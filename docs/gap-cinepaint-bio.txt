The Lord of the Rings films, the Harry Potter films, Too Fast Too Furious and The Last Samurai are some of the many Hollywood motion pictures that have used CinePaint as a post-production tool for image retouching and visual effects. Set up interview with Computer Graphics World editor that resulted in a feature story there, traveled to England with the CinePaint project manager for interview in person. Coverage in many computer magazines. Linux magazine CinePaint cover story.


Game producer. Direct creative process at a startup game and media company. Use critical insights, analytics, and data, ensuring cross functional alignment of goals, while driving strategic partnerships and new initiatives forward. Profit & Loss responsibility. Set project timelines and cross-project dependencies to ensure acceptable delivery by all stakeholders. Innovate and optimize digital workflows, working with engineering team. Develop games and comic books for female demographic audience. Produce PowerPoint pitch decks and game prototypes. Edit comic books. Write scripts. Responsible for managing studio artists and editors. Provide financial oversight and reporting throughout projects by creating and managing financial budgets in Excel.

Game producer. Direct creative process at a startup game and media company. Use critical insights, analytics, and data, ensuring cross functional alignment of goals, while driving strategic partnerships and new initiatives forward. Profit & Loss responsibility. Set project timelines and cross-project dependencies to ensure acceptable delivery by all stakeholders. Innovate and optimize digital workflows, working with engineering team. Develop games and comic books for female demographic audience. Produce PowerPoint pitch decks and game prototypes. Edit comic books. Write scripts. Responsible for managing studio artists and editors. Provide financial oversight and reporting throughout projects by creating and managing financial budgets in Excel.

Produce mobile games for fashion, wellness and entertainment.

Manage and mentor international editorial team. Edit articles and write headlines. 

Cover entertainment news as part of Hollywood press corps. Interview television and film stars, producers and celebrities for feature articles and for social media videos. Report on awards shows, red carpets and gift suites.

Write feature articles about entertainment business, primarily television series reviews. 