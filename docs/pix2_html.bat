@echo off
rem Robin.Rowe@MovieEditor.com 2015/10/28
rem License OSI MIT

set "OUT=index.html"
set "DIR=thumbnail"
set WIDTH=600
mkdir %DIR%
rem set exe="gm.exe convert"
set exe="C:\\Program Files\\ImageMagick-7.1.1-Q16-HDRI\\magick"

echo "<HTML><BODY><H3>CinePaint pix2_html</H3><P><P>" > %OUT% 
echo "<style>images{    text-align:center;    margin:50px auto; } images a{    margin:0px 20px;   display:inline-block;    text-decoration:none;    color:black; } <div id="images"></style>" > %OUT% 
for %%f in (*.HEIC) do (
	echo %%f -resize x%WIDTH% %DIR%\\%%f.jpg
	%exe% %%f -resize x%WIDTH% %DIR%\\%%f.jpg
rem	echo "<A HREF="%%f"><IMG SRC="%DIR%/%%f.jpg" ALT=\""%%f"\">%%f.jpg</A>" >> %OUT% 
echo "<a href="%%f"><img src="%DIR%/%%f.jpg" ALT=\""%%f"\"><div class="caption">%%f</div></a>" >> %OUT%
	)

echo "</div> </BODY></HTML>" >> %OUT%
echo done!
pause

