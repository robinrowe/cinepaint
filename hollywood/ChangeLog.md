# ChangeLog
*Robin.Rowe@cinepaint.org 2024/11/16*

## Next 1.4.8

* Fixed: splash screen display 2024/11/25
* Color palette bar
* Flipbook open range
* ToDo build
* Post bug report
https://reqbin.com/req/c-sma2qrvp/curl-post-form-example
* Darkroom and Thumbs build (image browser like Lightroom or Darktable)
* Scrubby Sweep build
* Wheelbarrow capture
https://stackoverflow.com/questions/3970066/creating-a-transparent-window-in-c-win32
Use the SetLayeredWindowAttributesarchive function, this allows you to set a mask color that will become transparent, thus allowing the background to show through.
You will also need to configure your window with the layered flag, e.g.:
SetWindowLong(hwnd, GWL_EXSTYLE, GetWindowLong(hwnd, GWL_EXSTYLE) | WS_EX_LAYERED);
After that it's fairly simple:
// Make red pixels transparent:
SetLayeredWindowAttributes(hwnd, RGB(255,0,0), 0, LWA_COLORKEY);
https://www.codeproject.com/KB/dialog/SemiTranDlgWithCtrls.aspx
https://stackoverflow.com/questions/3970066/creating-a-transparent-window-in-c-win32
SetWindowLong(hWnd, GWL_EXSTYLE, GetWindowLong(hWnd, GWL_EXSTYLE) | WS_EX_LAYERED); from the linked answer will make the entire window transparent. subsequent SetLayeredWindowAttributes(hWnd, 0, 128, LWA_ALPHA); call will make it 50% transparent. 
https://learn.microsoft.com/en-us/windows/win32/winmsg/window-features#layered-windows

## Notes regarding Color palette bar:
GdkColor color;
gdk_color_parse ("red", &color);
gtk_widget_modify_bg ( GTK_WIDGET(button), GTK_STATE_NORMAL, &color);
Notice that we are modifying bg instead of fg.
Colors are 16-bit per channel and have no alpha.

## Later

* Fix WinSnap
* Generative AI integration:
The web site likely uses cookies to store your session information. When you run
curl --user user:pass https://xyz.example/a  #works ok
curl https://xyz.example/b #doesn't work
curl is run twice, in two separate sessions. Thus when the second command runs, the cookies set by the 1st command are not available; it's just as if you logged in to page a in one browser session, and tried to access page b in a different one.
What you need to do is save the cookies created by the first command:
curl --user user:pass --cookie-jar ./somefile https://xyz.example/a
and then read them back in when running the second:
curl --cookie ./somefile https://xyz.example/b
* Timeline window
* File Save as Crash
* Bucket Fill Error Message
* Bucket Fill - Non-Sloppy Fill
* Save strokes, recreate in higher res
* Custom UI, expose PDB
* Red eye plugin
* * Multi-select for gtk_entry_get_chars() and struct _GtkEntry
* Chapter 12. List Widgets
The GtkList widget is designed to act as a vertical container for widgets that should be of the type GtkListItem.
A GtkList widget has its own window to receive events and it's own background color which is usualy white. As it is directly derived from a GtkContainer it can be treated as such by using the GTK_CONTAINER(List) macro, see the GtkContainer widget for more on this. One should already be familar whith the usage of a GList and its related functions g_list_*() to be able to use the GtkList widget to its fully extends.
There is one field inside the structure definition of the GtkList widget that will be of greater interest to us, this is:
    struct _GtkList
    {
      [...]
      GList *selection;
      guint selection_mode;
      [...]
    }; 
The selection field of a GtkList points to a linked list of all items that are cureently selected, or `NULL' if the selection is empty. So to learn about the current selection we read the GTK_LIST()->selection field, but do not modify it since the internal fields are maintained by the gtk_list_*() functions.
The selection_mode of the GtkList determines the selection facilities of a GtkList and therefore the contents of the GTK_LIST()->selection field:
The selection_mode may be one of the following:
    GTK_SELECTION_SINGLE The selection is either `NULL' or contains a GList* pointer for a single selected item.
    GTK_SELECTION_BROWSE The selection is `NULL' if the list contains no widgets or insensitive ones only, otherwise it contains a GList pointer for one GList structure, and therefore exactly one list item.
    GTK_SELECTION_MULTIPLE The selection is `NULL' if no list items are selected or a GList pointer for the first selected item. That in turn points to a GList structure for the second selected item and so on.
    GTK_SELECTION_EXTENDED The selection is always `NULL'.
The default is GTK_SELECTION_MULTIPLE. 
6.1 How do I find out about the selection of a GtkList?
Get the selection something like this:
    GList *sel;
    sel = GTK_LIST(list)->selection;
This is how GList is defined (quoting glist.h):
    typedef struct _GList GList;
    struct _GList
    {
      gpointer data;
      GList *next;
      GList *prev;
    };
A GList structure is just a simple structure for doubly linked lists. there exist several g_list_*() functions to modify a linked list in glib.h. However the GTK_LIST(MyGtkList)->selection is maintained by the gtk_list_*() functions and should not be modified.
The selection_mode of the GtkList determines the selection facilities of a GtkList and therefore the contents of GTK_LIST(AnyGtkList)->selection:
selection_mode          GTK_LIST()->selection contents
------------------------------------------------------
GTK_SELECTION_SINGLE    selection is either NULL
                        or contains a GList* pointer
                        for a single selected item.
GTK_SELECTION_BROWSE    selection is NULL if the list
                        contains no widgets, otherwise
                        it contains a GList* pointer
                        for one GList structure.
GTK_SELECTION_MULTIPLE  selection is NULL if no listitems
                        are selected or a a GList* pointer
                        for the first selected item. that
                        in turn points to a GList structure
                        for the second selected item and so
                        on.
GTK_SELECTION_EXTENDED  selection is NULL.
The data field of the GList structure GTK_LIST(MyGtkList)->selection points to the first GtkListItem that is selected. So if you would like to determine which listitems are selected you should go like this:
Upon Initialization:
    {
            gchar           *list_items[]={
                                    "Item0",
                                    "Item1",
                                    "foo",
                                    "last Item",
                            };
            guint           nlist_items=sizeof(list_items)/sizeof(list_items[0]);
            GtkWidget       *list_item;
            guint           i;
            list=gtk_list_new();
            gtk_list_set_selection_mode(GTK_LIST(list), GTK_SELECTION_MULTIPLE);
            gtk_container_add(GTK_CONTAINER(AnyGtkContainer), list);
            gtk_widget_show (list);
            for (i = 0; i < nlist_items; i++)
            {
                    list_item=gtk_list_item_new_with_label(list_items[i]);
                    gtk_object_set_user_data(GTK_OBJECT(list_item), (gpointer)i);
                    gtk_container_add(GTK_CONTAINER(list), list_item);
                    gtk_widget_show(list_item);
            }
    }
To get known about the selection:
    {
            GList   *items;

            items=GTK_LIST(list)->selection;

            printf("Selected Items: ");
            while (items) {
                    if (GTK_IS_LIST_ITEM(items->data))
                            printf("%d ", (guint) 
                    gtk_object_get_user_data(items->data));
                    items=items->next;
            }
            printf("\n");
    }

* * Co-working: https://magma.com/studio-plan

## Summary

Next: Wacom, Text, cmaker_generate, tracing, Gtk1Examples, SVG, Linux32

v1.4.8:
v1.4.7: GTK1 dark-mode, canvas automatic shrink-wrap (2023/2/20)
v1.4.6: Toolbox statusbar, fixed Measure tool (2022/7/8)
v1.4.5: Remove assert/exit(), add {file}{open}<Desktop>
v1.4.4: JPEG, Flipbook, Screenshot, Impressionist, broken: Text
v1.4.3: RAW, PSD, Gfig vector layers, Cinenote, many more plugins
v1.4.2: Fixed layer crash, fixed FileSave, single-threaded plugins
v1.4.1: Plugins PPM and PNG open files, broken: FileSave
v1.4.0: New cmake build system, Windows builds, broken: plugins

Dark mode: #define DARK in gtkstyle.c

## Coming Features
 
* tracing mode
* chat
* ftp
* file manager
* SVG
* bug tracker
* crash reporter
* timeline
* game engine
* TTS
* JavaScript 
* Forth
* EXIF
* audio
* 2D vector layers
* 3D layers# Cinepaint1 CHANGES.md

## Done

2021/04/22 - git tag v1.4.3
2021/04/22 - Add PDF plugin, but not compiled yet
2021/04/22 - Gfig works, plugins run in main event space, not as threads
2021/04/18 - Notes feature added
2021/04/18 - git tag v1.4.2
2021/04/17 - Add CineNotes program
2021/04/17 - Fix layer new crash (added layer_ref())
2021/04/17 - Add plugins and microperl (not building them yet):
	plugin/FractalExplorer/
	plugin/Lighting/
	plugin/MapObject/
	plugin/flame/
	plugin/fp/
	plugin/gap/
	plugin/gdyntext/
	plugin/gflare/
	plugin/gfli/
	plugin/gimpressionist/
	plugin/helpbrowser/
	plugin/ifscompose/
	plugin/imagemap/
	plugin/libgck/
	plugin/maze/
	plugin/mosaic/
	plugin/pagecurl/
	plugin/perl/
	plugin/rcm/
	plugin/sel2path/
	plugin/webbrowser/
	plugin/xjt/
2021/04/08 - PPM file saves, but contains zebra bars, not image
2021/04/08 - PNG file opens
2021/04/08 - PPM file opens
2020/03/01 - Lots of legacy bug fixes
2020/11/16 - New cmake build system, replaced automake
2020/11/19 - Toolbar selects wrong tools after changed order, fixed enum
2021/01/17 - Splash PPM load fixed
2021/02/02 - Added install.sh (Windows)

## Done

* 1.7.3 Automatic Shrink-Wrap 

## Tracker

* Install Bugzilla
* Create new website
* Create installer.
* Tracing Mode
* EXIF Suppor
* 3D Layer Support
* JavaScript Support

## Broken Plugins

* FractalExplorer
* Lightning
* MapObject
* apng
* bracketing_to_hdr
* collect
* dbbrowser
* dicom
* flame
* fp
* gap
* gdyntext
* gflare
* gfli
* guash
* helpbrowser
* icc_examin
* ico
* ifscompose
* imagemap
* libgck
* maze
* median
* minimum
* mosaic
* openexr
* pagecurl
* pdf
* print
* rcm
* retinex
* screenshot
* script-fu
* sel2path
* tiff
* twain
* userfilter
* webbrowser
* webp
* winprint
* xjt

## CinePaint 1.4.1

Expect PPM and PNG to work. Maybe other types will work. Needs much more testing. 

*** BUG LIST ***

- Erasing crashes the software
 Doesn't happen to me, although erasure looks funky
- It doesn't save preferences (I'll double check to see if this is global)
 ???
- Cannot load custom brushes AKA GrutBrush library - what is the brush format?
 CinePaint brushes are in GBR format. Any image can be made into a brush by saving it as a GBR and placing it in the brushes directory.
- FILL tool crashes software
 Works for me
- <CTRL Z> does not undo
 Broken
- EYEDROPPER tool does not update in COLOR SELECTION box
 Works for me
- Cannot QUIT or EXIT program
 Fixed

*** WISH LIST ***
- Transparency for Tracing
 Noted
- Glossary of terms (aka image_id Screen)
 ???
- Color Palette under the color switcher
 As a row of 15 color boxes added to the bottom of the toolbar?
- Easy way to change brush sizes ( PhotoShop [ ] )
 ???
- PREFERENCES > DISPLAY choose default background options
 ???

 ## Notes

- To output commit.log:
	git log --graph --pretty=format:"%C(cyan)%C(bold)%ad%Creset  %C(green)%Creset %s" --date=short > commit.log
	
## Matt's Bug List (from Slack)