/*	hollywood/platform_id.h
	Created 2021/4/8 robin.rowe@cinepaint.org
	Copyright 2020 CinePaint
	License MIT 
 */
                                   
#ifndef platform_id_h
#define platform_id_h

#if defined(_WIN64)
    #define PLATFORM_BITS "64"
    #define SIZEOF_INT 64
#elif defined(_WIN32)
    #define PLATFORM_BITS "32"
    #define SIZEOF_INT 32
#elif defined(__GNUC__)
    #if defined(__x86_64__) || defined(__ppc64__)
        #define PLATFORM_BITS "64"
        #define SIZEOF_INT 64
    #else
        #define PLATFORM_BITS "32"
        #define SIZEOF_INT 32
    #endif
#elif UINTPTR_MAX > UINT_MAX
    #define PLATFORM_BITS "64"
    #define SIZEOF_INT 64
#else
    #define PLATFORM_BITS "32"
    #define SIZEOF_INT 32
#endif

#if defined(_WIN64)
    #define PLATFORM_NAME "Win" 
//    #define PLATFORM_BITS "64"
#elif defined(_WIN32)
    #define PLATFORM_NAME "Win" 
//    #define PLATFORM_BITS "32"
#elif defined(__linux__)
    #define PLATFORM_NAME "Linux" 
/*
#elif defined(__APPLE__) && defined(__MACH__) // Apple OSX and iOS (Darwin)
    #include <TargetConditionals.h>
    #if TARGET_IPHONE_SIMULATOR == 1
        #define PLATFORM_NAME "ios" // Apple iOS
    #elif TARGET_OS_IPHONE == 1
        #define PLATFORM_NAME "ios" // Apple iOS
    #elif TARGET_OS_MAC == 1
        #define PLATFORM_NAME "osx" // Apple OSX
    #endif
*/
#elif defined(__APPLE__) 
    #define PLATFORM_NAME "MacOS"
#elif defined(BSD)
    #define PLATFORM_NAME "BSD" // FreeBSD, NetBSD, OpenBSD, DragonFly BSD
#elif defined(__hpux)
    #define PLATFORM_NAME "HP-UX" // HP-UX
#elif defined(_AIX)
    #define PLATFORM_NAME "AIX" // IBM AIX
#elif defined(__sun) && defined(__SVR4)
    #define PLATFORM_NAME "Solaris" // Oracle Solaris, Open Indiana
#elif defined(__ANDROID__)
    #define PLATFORM_NAME "Android" // Android (implies Linux, so it must come first)
#elif defined(__CYGWIN__) && !defined(_WIN32)
    #define PLATFORM_NAME "Cygwin" // Windows (Cygwin POSIX under Microsoft Window)
#else
    #define PLATFORM_NAME "Unknown"
#endif

#endif
