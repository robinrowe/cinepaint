# Cinepaint1 ROADMAP.md

*Robin.Rowe@cinepaint.org 2020/11/20*

RR = Robin
MR = Matt

Items with dates are completed.

## Plan

2021/04/22-RR: Builds on Win32 and runs as MVP 
RR: Build on Linux 32-bit
RR: Build 64-bit 
RR: MacOS build (64-bit only)
RR: FreeBSD

## Feature Requests - ASAP

RR: Redo install_help() to do HTTP registration 
RR: MOTD (Message of the Day) with billboard
RR: Live bug reporting over HTTP with screen capture option
RR: Live update of tips.txt with HTTP
RR: Call stack trace
MR: Wacom
RR: Gftp
RR: Scrubby audio editor

## Matt's Wish List (from Slack)

MR: Transparency for Tracing
	RR:	Noted
MR: Glossary of terms (aka Image Screen)
	RR: ???
MR: Color Palette under the color switcher
	RR:	As a row of 15 color boxes added to the bottom of the toolbar?
MR: Easy way to change brush sizes ( PhotoShop [ ] )
	RR:	???
MR: PREFERENCES > DISPLAY choose default background options
	RR:	???

## Feature Requests - Sooner

RR: vector layers
RR: animation
RR: asio chat
RR: Flipbook as PowerPoint-style deck presentation
RR: Comic strip mask
RR: Palette bar, not just 2 colors on toolbar
RR: Library of images
RR: Make messagebox selectable

## Feature Requests - Later

RR: Kinetic type 
RR: Dicom
RR: RAW
RR: AtlastForth
RR: QuickJS
RR: Pico C
RR: Android support
RR: iOS support
RR: Share session P2P
RR: 3D render import
RR: AI
RR: Deep fakes
RR: Colorization
RR: Film restoration
RR: Macro recorder
RR: Photo lightbox
RR: Cartoonize
RR: Video export/import
RR: Roto
RR: Tracking
RR: Cyclorama
RR: Change variargs code to use iostream
RR: UE4 integration
RR: JPEG2000

## Debug Verbosity

These #defines are commented out for normal operation:

	app\scale.c(29): VERBOSE_GDISP
	gtk1\gtk1win\gtk\gtkpixmap.c(35): VERBOSE_PIXMAP_SET
	gtk1\gtk1win\gtk\gtkpreview.c(40): VERBOSE_BITBLT
	gtk1\gtk1win\gtk\gtkobject.c VERBOSE_REF
	gtk1\gtk1win\gtk\gtkobject.c VERBOSE_REF2
	pdb\pixelrgn.c(29): VERBOSE_TILE_FILL
	pdb\plugin.c(77): VERBOSE_SCANLINE
	pdb\plugin.c(78): VERBOSE_ADD_PLUGIN
	pdb\plugin.c(79): VERBOSE_BUFFER_COPY
	pdb\plugin.c(80): VERBOSE_FILE_HANDLER
	pdb\plugin.c(81): VERBOSE_WIRE_TABLES
	pdb\plugin_main.c(57): DEBUG_RUN_PROC
	pdb\procedural_db.c(35): VERBOSE_PDB_LOOKUP
	pdb\procedural_db.c(36): VERBOSE_PDB_EXECUTE
	wire\protocol.c(25): VERBOSE_PROC_INSTALL
	wire\protocol.c(26): VERBOSE_PROC_RUN
	wire\protocol.c(27): VERBOSE_WIRE_HANDLERS
	wire\wire.c(41): VERBOSE_WIRE
	wire\wire.c(42): VERBOSE_WIRE_HANDLER
	wire\wire.c(45): VERBOSE_WRITE_STRING
	wire\wire.c(46): VERBOSE_WIRE_BYTES 
