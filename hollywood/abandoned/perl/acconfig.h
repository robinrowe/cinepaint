/* Define if you have the ANSI C header files.  */
#undef STDC_HEADERS

/* Define if you have the vsnprintf function.  */
#undef HAVE_VSNPRINTF

/* Define if you have the <libgimp/gimp.h> header file.  */
#undef HAVE_LIBGIMP_GIMP_H

/* Define if you have the glib library (-lglib).  */
#undef HAVE_LIBGLIB

/* Define if you don't have gimp_get_data_size.  */
#undef HAVE_GET_DATA_SIZE

/* Define if we have DIVIDE_MODE.  */
#undef HAVE_DIVIDE_MODE

/* Define if we have _exit(2).  */
#undef HAVE__EXIT

