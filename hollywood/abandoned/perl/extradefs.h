/*
 * extra constants for use by the plugin
 */

#define TRACE_NONE	0x00
#define TRACE_CALL	0x01
#define TRACE_TYPE	0x11
#define TRACE_NAME	0x21
#define TRACE_DESC	0x41
#define TRACE_ALL	0xff

