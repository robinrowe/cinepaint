/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __JGIMP_DATA_CONVERSIONS_H__
#define __JGIMP_DATA_CONVERSIONS_H__

#include <libgimp/gimp.h>
#include <jni.h>

struct ConvertedGimpParam
{
	GimpParam  param;
	int        array_len;
};

/* Allocates a new JGimpData object and sets its value to that passed in by the GimpParam
 *
 * Params:
 * - env        - the JNI environment
 * - param      - the GimpParam to convert
 * - array_len  - the size of the array (if the GimpParam refers to an array)
 */
jobject GimpParam_to_JGimpData(JNIEnv* env, GimpParam *param, int array_len);

/* Allocates a new GimpParam via g_new and sets it to the value of the JGimpData provided. You must deallocate the GimpParam when finished */
struct ConvertedGimpParam* JGimpData_to_GimpParam(JNIEnv* env, jobject jgimp_data);

void convert_jgimp_param_descriptor(JNIEnv* env, jobject param_descriptor_object, GimpParamDef* param_def);

/* Allocates a new c-string and copies the jstring into the new string. You must deallocate the c-string when finished */
char* jstring_to_c_string(JNIEnv* env, jstring in_string);

/* Deallocates all memory allocated for this param and data held by it */
void free_ConvertedGimpParam(struct ConvertedGimpParam *param);
#endif /* __JGIMP_DATA_CONVERSIONS_H__ */

/* Stub: Mike asks: What's this for? */
#ifdef DMALLOC
#include "dmalloc.h"
#endif
