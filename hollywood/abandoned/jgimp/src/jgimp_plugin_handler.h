/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __JGIMP_PLUGIN_HANDLER_H__
#define __JGIMP_PLUGIN_HANDLER_H__

#include <libgimp/gimp.h>
/*
 * Marshalls data to the Java side to call a Java-based plug-in
 */
void handle_java_plug_in(
     gchar      *name, 
     gint        n_params, 
     GimpParam  *param, 
     gint       *nreturn_vals,
     GimpParam **return_vals);

#endif /* __JGIMP_PLUGIN_HANDLER_H__ */
