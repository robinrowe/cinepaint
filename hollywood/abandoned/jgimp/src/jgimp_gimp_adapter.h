/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * This is a set of wrapper functions so that we can
 * either embed jgimp directly in the GIMP's process (for
 * efficiency and speed) or as a normal plug-in.
 */

#ifndef __JGIMP_GIMP_ADAPTER_H__
#define __JGIMP_GIMP_ADAPTER_H__

#include <jni.h>
#include <glib.h>

#ifdef JGIMP_COMPILE_AS_PLUGIN
#include <libgimp/gimp.h>
#else  /* JGIMP_COMPILE_AS_PLUGIN */
#include <libgimp/gimpprotocol.h>
#endif /* JGIMP_COMPILE_AS_PLUGIN */

#ifdef JGIMP_COMPILE_FOR_FILMGIMP
#define GimpRunProc GRunProc
#endif /* JGIMP_COMPILE_FOR_FILMGIMP */

typedef struct _JGimpDrawableInfo
{
	gint32   id;
	guint    width;
	guint    height;
	guint    bpp;
	gboolean has_alpha;
	gboolean is_rgb;
	gboolean is_gray;
	gboolean is_indexed;
	gboolean is_valid_drawable;
} JGimpDrawableInfo;

void        adapter_install_temp_proc   (gchar        *name,
				           gchar        *blurb,
				           gchar        *help,
				           gchar        *author,
				           gchar        *copyright,
				           gchar        *date,
				           gchar        *menu_path,
				           gchar        *image_types,
				           gint          type,
				           gint          nparams,
				           gint          nreturn_vals,
#ifdef JGIMP_COMPILE_AS_PLUGIN
				           GimpParamDef *params,
				           GimpParamDef *return_vals,
				           GimpRunProc   run_proc);
#else
				           GPParamDef   *params,
				           GPParamDef   *return_vals,
				           void         *run_proc); /* should be of type GimpRunProc but we can't include gimp.h for embedded compilation */
#endif /* JGIMP_COMPILE_AS_PLUGIN */

#ifdef JGIMP_COMPILE_AS_PLUGIN
GimpParam* adapter_run_pdb_procedure(gchar *name, gint *nreturn_vals, gint nparams, GimpParam *params);
void       adapter_destroy_pdb_procedure_return_vals(GimpParam *params, gint nparams);
#else
GPParam* adapter_run_pdb_procedure(gchar *name, gint *nreturn_vals, gint nparams, GPParam *params);
void     adapter_destroy_pdb_procedure_return_vals(GPParam *params, gint nparams);
#endif /* JGIMP_COMPILE_AS_PLUGIN */

JGimpDrawableInfo adapter_get_drawable_info(gint32 drawable_ID);
jlong             adapter_get_pixel_rgn(JGimpDrawableInfo* drawable,
										gint x,
										gint y,
										gint width,
										gint height,
										guchar* buffer,
										gint buffer_offset, /* in bytes */
										gint buffer_length); /* in bytes */
jlong             adapter_set_pixel_rgn(JGimpDrawableInfo* drawable,
										gint x,
										gint y,
										gint width,
										gint height,
										guchar* buffer, /* in bytes */
										gint buffer_offset, /* in bytes */
										gint buffer_length,
										gboolean flush_drawable,
										gboolean merge_shadow,
										gboolean update_drawable);

#ifndef JGIMP_COMPILE_AS_PLUGIN
guint jgimp_install_g_main_callback_hooks();
#endif /* JGIMP_COMPILE_AS_PLUGIN */

#endif /* __JGIMP_GIMP_ADAPTER_H__ */
