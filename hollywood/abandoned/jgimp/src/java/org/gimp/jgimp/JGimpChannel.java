/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp;

import java.awt.Color;

import org.gimp.jgimp.proxy.JGimpProxy;


/**
 * A Java-friendly wrapper for a GIMP channel. Note that
 * a JGimpChannel is a child of {@link org.gimp.jgimp.JGimpDrawable},
 * and thus shares all the properties and methods of a
 * {@link org.gimp.jgimp.JGimpDrawable}.
 * It can also be used in any place a drawable is needed.
 * <p>
 * Channels are associated with images, and must be created
 * by a {@link org.gimp.jgimp.JGimpImage} object
 * @see org.gimp.jgimp.JGimpImage#insertNewChannel(int, int, String, double, Color, int) 
 * @see org.gimp.jgimp.JGimpImage#appendNewChannel(int, int, String, double, Color) 
 */
public class JGimpChannel extends JGimpDrawable {
    private GimpApp m_App = null;

    JGimpChannel(GimpApp app, int ID) {
        super(app, ID);
        m_App = app;
    }
    JGimpChannel(JGimpProxy proxy, int ID) {
        super(new GimpApp(proxy), ID);
        m_App = new GimpApp(proxy);
    }

    public String toString() {
        return "JGimpChannel, value: " + this.getID();
    }
    
    /**
     * @see org.gimp.jgimp.JGimpData#getGimpPDBArgType()
     */
    public int getGimpPDBArgType() {
        return JGimpPDBArgTypeConstants.PDB_CHANNEL;
    }

    /**
     * Duplicates the channel and returns a copy
     * of the channel.
     * @return Returns a copy of this channel
     * @throws JGimpException
     * @pdb gimp-channel-copy
     */
    public JGimpChannel duplicate() throws JGimpException {
        return (JGimpChannel)m_App.callProcedure("gimp_channel_copy", this)[0];
    }
    /**
     * Returns the color of this channel.
     * @throws JGimpException
     * @pdb gimp-channel-get-color
     */
    public Color getColor() throws JGimpException {
        return m_App.callProcedure("gimp_channel_get_color", this)[0].convertToColor();
    }
    /**
     * Sets the color of this channel.
     * @param color The color to set for this channel.
     * @throws JGimpException
     * @pdb gimp-channel-set-color
     */
    public void setColor(Color color) throws JGimpException {
        m_App.callProcedure("gimp_channel_set_color", this, color);
    }
    /**
     * Returns this channel's name.
     * @throws JGimpException
     * @pdb gimp-channel-get-name
     */
    public String getName() throws JGimpException {
        return m_App.callProcedure("gimp_channel_get_name", this)[0].convertToString();
    }
    /**
     * Sets this channel's name.
     * @param name The new name for this channel.
     * @throws JGimpException
     * @pdb gimp-channel-set-name
     */
    public void setName(String name) throws JGimpException {
        m_App.callProcedure("gimp_channel_set_name", this, name);
    }
    /**
     * Returns the opacity of this channel.
     * @throws JGimpException
     * @pdb gimp-channel-get-opacity
     */
    public double getOpacity() throws JGimpException {
        return m_App.callProcedure("gimp_channel_get_opacity", this)[0].convertToDouble();
    }
    /**
     * Sets this channel's opacity.
     * @param opacity The new opacity of this channel.
     * @throws JGimpException
     * @pdb gimp-channel-set-opacity
     */
    public void setOpacity(double opacity) throws JGimpException {
        m_App.callProcedure("gimp_channel_set_opacity", this, new Double(opacity));
    }
    /**
     * Indicates whether this channel is composited (applied)
     * to the image.
     * @return True if this channel is applied to the image.
     * @throws JGimpException
     * @pdb gimp-channel-get-show-masked
     */
    public boolean isComposited() throws JGimpException {
        return m_App.callProcedure("gimp_channel_get_show_masked", this)[0].convertToBoolean();
    }
    /**
     * Applies or removes this channel to the image.
     * @param composited If true, applies this channel to the image.
     * @throws JGimpException
     * @pdb gimp-channel-set-show-masked
     */
    public void setComposited(boolean composited) throws JGimpException {
        m_App.callProcedure("gimp_channel_set_show_masked", this, new Boolean(composited));
    }
    /**
     * Indicates whether this channel is currently visible.
     * @return True if this channel is visible.
     * @throws JGimpException
     * @pdb gimp-channel-get-visible
     */
    public boolean isVisible() throws JGimpException {
        return m_App.callProcedure("gimp_channel_get_visible", this)[0].convertToBoolean();
    }
    /**
     * Sets the visibility of this channel.
     * @param visible Set to true to make this channel visible.
     * @throws JGimpException
     * @pdb gimp-channel-set-visible
     */
    public void setVisible(boolean visible) throws JGimpException {
        m_App.callProcedure("gimp_channel_set_visible", this, new Boolean(visible));
    }

}
