/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp.plugin;

import java.util.Vector;


/**
 * Convenience/utility class to more easily construct
 * lists/arrays of JGimpParamDescriptor values
 */
public class JGimpParamDescriptorListBuilder
{
	private Vector m_ParamList = new Vector();

	/**
	 * Appends the given parameter description to this list
	 */
	public void append(int inArgType, String inName, String inDescription)
	{
		m_ParamList.add(new JGimpParamDescriptor(inArgType, inName, inDescription));
	}
	/**
	 * Sets the parameter description for the given index
	 */
	public void set(int index, int inArgType, String inName, String inDescription)
	{
		m_ParamList.set(index, new JGimpParamDescriptor(inArgType, inName, inDescription));
	}
	/**
	 * Returns the parameter description at the given index
	 */
	public JGimpParamDescriptor get(int index)
	{
		return (JGimpParamDescriptor)m_ParamList.get(index);
	}
	/**
	 * Removes the parameter description at the given index
	 */
	public JGimpParamDescriptor remove(int index)
	{
		return (JGimpParamDescriptor)m_ParamList.remove(index);
	}
	/**
	 * Clears all parameter descriptions in this list
	 */
	public void clear()
	{
		m_ParamList.clear();
	}
	/**
	 * Returns the size of the list
	 */
	public int getSize()
	{
		return m_ParamList.size();
	}
	/**
	 * Transforms the list of parameters into an array
	 */
	public JGimpParamDescriptor[] toArray()
	{
		return (JGimpParamDescriptor[])m_ParamList.toArray(new JGimpParamDescriptor[0]);
	}
}
