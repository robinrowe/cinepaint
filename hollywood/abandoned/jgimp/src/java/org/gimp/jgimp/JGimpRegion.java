/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp;

import java.awt.Rectangle;

/**
 * Java-friendly object encapsulating data identifying a region within the GIMP.
 */
public class JGimpRegion extends JGimpData {
    private int m_X = -1;
    private int m_Y = -1;
    private int m_Width = -1;
    private int m_Height = -1;

    public JGimpRegion(int x, int y, int width, int height) {
        m_X = x;
        m_Y = y;
        m_Width = width;
        m_Height = height;
    }
    public JGimpRegion(Rectangle r) {
        m_X = r.x;
        m_Y = r.y;
        m_Width = r.width;
        m_Height = r.height;
    }

    public java.awt.Rectangle getRegion() {
        return new java.awt.Rectangle(m_X, m_Y, m_Width, m_Height);
    }

    /**
     * @see org.gimp.jgimp.JGimpData#convertToRectangle()
     */
    public Rectangle convertToRectangle() throws JGimpInvalidDataCoercionException {
        return this.getRegion();
    }

    /**
     * Returns the x-coordinate of the top-left corner of this region.
     */
    public int getX() {
        return m_X;
    }
    /**
     * Returns the y-coordinate of the top-left corner of this region.
     */
    public int getY() {
        return m_Y;
    }
    /**
     * Returns the width of this region.
     */
    public int getWidth() {
        return m_Width;
    }
    /**
     * Returns the height of this region.
     */
    public int getHeight() {
        return m_Height;
    }

    public String toString() {
        return "JGimpRegion, x: " + m_X + " y: " + m_Y + " width: " + m_Width + " height: " + m_Height;
    }

    public boolean equals(Object inRight) {
        if (inRight == null) {
            return false;
        }

        if (inRight instanceof JGimpRegion) {
            JGimpRegion rightRegion = (JGimpRegion) inRight;
            return (
                (rightRegion.getX() == this.getX())
                    && (rightRegion.getY() == this.getY())
                    && (rightRegion.getWidth() == this.getWidth())
                    && (rightRegion.getHeight() == this.getHeight()));
        }
        return false;
    }

    /**
     * @see org.gimp.jgimp.JGimpData#getGimpPDBArgType()
     */
    public int getGimpPDBArgType() {
        return JGimpPDBArgTypeConstants.PDB_REGION;
    }

}
