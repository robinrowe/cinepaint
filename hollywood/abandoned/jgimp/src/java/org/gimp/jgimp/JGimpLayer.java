/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp;

import org.gimp.jgimp.nativewrappers.JGimpInt32;
import org.gimp.jgimp.proxy.JGimpProxy;



/**
 * A Java-friendly wrapper for GIMP layers.
 * @see org.gimp.jgimp.JGimpDrawable
 */
public class JGimpLayer extends JGimpDrawable {

    private GimpApp m_App = null;
    
    JGimpLayer(GimpApp app, int ID) {
        super(app, ID);
        m_App = app;
    }
    JGimpLayer(JGimpProxy proxy, int ID) {
        super(new GimpApp(proxy), ID);
        m_App = new GimpApp(proxy);
    }
    
    /**
     * @see org.gimp.jgimp.JGimpData#getGimpPDBArgType()
     */
    public int getGimpPDBArgType() {
        return JGimpPDBArgTypeConstants.PDB_LAYER;
    }


    public String toString() {
        return "JGimpLayer, value: " + this.getID();
    }

    /**
     * Adds an alpha channel to this layer.
     * @throws JGimpException
     * @pdb gimp-layer-add-alpha
     */
    public void addAlphaChannel() throws JGimpException {
        m_App.callProcedure("gimp_layer_add_alpha", this);
    }
    /**
     * Creates a layer mask for this layer, making the layer
     * fully visible.
     * @return A reference to the new layer mask.
     * @throws JGimpException
     * @pdb gimp-layer-create-mask
     */
    public JGimpChannel createLayerMaskMakeFullyVisible() throws JGimpException {
        return (JGimpChannel)m_App.callProcedure("gimp_layer_create_mask", this, new JGimpInt32(0))[0];
    }
    /**
     * Creates a layer mask for this layer, making the layer fully transparent.
     * @return A reference to the new layer mask.
     * @throws JGimpException
     * @pdb gimp-layer-create-mask
     */
    public JGimpChannel createLayerMaskMakeFullyTransparent() throws JGimpException {
        return (JGimpChannel)m_App.callProcedure("gimp_layer_create_mask", this, new JGimpInt32(1))[0];
    }
    /**
     * Creates a layer mask for this layer, using the alpha channel.
     * @return A reference to the new layer mask.
     * @throws JGimpException
     * @pdb gimp-layer-create-mask
     */
    public JGimpChannel createLayerMaskUseAlphaChannel() throws JGimpException {
        return (JGimpChannel)m_App.callProcedure("gimp_layer_create_mask", this, new JGimpInt32(2))[0];
    }
    /**
     * Returns whether the layer mask is currently applied to this layer.
     * @return True if the layer mask is applied, false if not.
     * @throws JGimpException
     * @pdb gimp-layer-get-apply-mask
     */
    public boolean isLayerMaskApplied() throws JGimpException {
        return m_App.callProcedure("gimp_layer_get_apply_mask", this)[0].convertToBoolean();
    }
    /**
     * Returns whether the GIMP is currently showing the layer mask for editing
     * purposes.
     * @return True if the layer mask is shown for editing, false if not.
     * @throws JGimpException
     * @pdb gimp-layer-get-edit-mask
     */
    public boolean isLayerMaskShownForEditing() throws JGimpException {
        return m_App.callProcedure("gimp_layer_get_edit_mask", this)[0].convertToBoolean();
    }
    /**
     * Returns whether the layer is currently linked.
     * @return True if the layer is linked, false if not.
     * @throws JGimpException
     * @pdb gimp-layer-get-linked
     */
    public boolean isLinked() throws JGimpException {
        return m_App.callProcedure("gimp_layer_get_linked", this)[0].convertToBoolean();
    }
    /*
     * What is this?
    public int getCombinationMode() throws JGimpException {
    }
    */
    /**
     * Returns the name of this layer.
     * @pdb gimp-layer-get-name
     */
    public String getName() throws JGimpException {
        return m_App.callProcedure("gimp_layer_get_name", this)[0].convertToString();
    }
    /**
     * Returns the opacity level of this layer.
     * @return A value between 0.0 and 1.0.
     * @throws JGimpException
     * @pdb gimp-layer-get-opacity
     */
    public double getOpacity() throws JGimpException {
        return m_App.callProcedure("gimp_layer_get_opacity", this)[0].convertToDouble();
    }
    /**
     * Returns whether the layer preserves transparency.
     * @throws JGimpException
     * @pdb gimp-layer-get-preserve-trans
     */
    public boolean getPreserveTransparency() throws JGimpException {
        return m_App.callProcedure("gimp_layer_get_preserve_trans", this)[0].convertToBoolean();
    }
    /**
     * Returns whether the layer mask is currently shown in the GIMP,
     * rather than the layer.
     * @return True if the layer mask is shown instead of the layer.
     * @throws JGimpException
     * @pdb gimp-layer-get-show-mask
     */
    public boolean isLayerMaskShownInsteadOfLayer() throws JGimpException {
        return m_App.callProcedure("gimp_layer_get_show_mask", this)[0].convertToBoolean();
    }
    /**
     * Returns whether the layer is currently visible in the GIMP.
     * @throws JGimpException
     * @pdb gimp-layer-get-visible
     */
    public boolean isVisible() throws JGimpException {
        return m_App.callProcedure("gimp_layer_get_visible", this)[0].convertToBoolean();
    }
    /**
     * Returns the layer's mask, null if no layer mask exists.
     * @pdb gimp-layer-mask
     */
    public JGimpChannel getLayerMask() throws JGimpException {
        JGimpChannel channel = (JGimpChannel)m_App.callProcedure("gimp_layer_mask", this)[0];
        if (channel.getID() == -1) {
            return null;
        }
        return channel;
    }
    /**
     * Resizes the layer to the new dimensions and offset.
     * @param newWidth The new layer width.
     * @param newHeight The new layer height.
     * @param x_offset The new x offset of the layer from its old offset (old x + x_offset).
     * @param y_offset The new y offset of the layer from its old offset (old y + y_offset).
     * @throws JGimpException
     * @pdb gimp-layer-resize
     */
    public void resize(int newWidth, int newHeight, int x_offset, int y_offset) throws JGimpException {
        m_App.callProcedure("gimp_layer_resize", this, new JGimpInt32(newWidth), new JGimpInt32(newHeight), new JGimpInt32(x_offset), new JGimpInt32(y_offset));
    }
    /**
     * Scales the layer to the new dimensions.
     * @param newWidth The new width of the layer.
     * @param newHeight The new height of the layer.
     * @param useLocalOrigin Set to true to scale about the layer's origin, false to scale about the image's origin.
     * @throws JGimpException
     * @pdb gimp-layer-scale
     */
    public void scale(int newWidth, int newHeight, boolean useLocalOrigin) throws JGimpException {
        m_App.callProcedure("gimp_layer_scale", this, new JGimpInt32(newWidth), new JGimpInt32(newHeight), new Boolean(useLocalOrigin));
    }
    /**
     * Applies or removes the layer mask to this layer.
     * @param apply Set to true to apply the layer mask, false to remove the layer mask.
     * @throws JGimpException
     * @pdb gimp-layer-set-apply-mask
     */
    public void setLayerMaskApplied(boolean apply) throws JGimpException {
        m_App.callProcedure("gimp_layer_set_apply_mask", this, new Boolean(apply));
    }
    /**
     * Toggles the layer mask's visibility for editing.
     * @param visible Set to true to show the layer mask for editing, false to hide it.
     * @throws JGimpException
     * @pdb gimp-layer-set-edit-mask
     */
    public void setLayerMaskShownForEditing(boolean visible) throws JGimpException {
        m_App.callProcedure("gimp_layer_set_edit_mask", this, new Boolean(visible));
    }
    /**
     * Links or unlinks the layer.
     * @param linked Set to true to link the layer, false to unlink.
     * @throws JGimpException
     * @pdb gimp-layer-get-linked
     */
    public void setLinked(boolean linked) throws JGimpException {
        m_App.callProcedure("gimp_layer_get_linked", this, new Boolean(linked));
    }
    /*
     * What is this?
    public void setCombinationMode(int mode) throws JGimpException {
    }
    */
    /**
     * Sets the name of this layer.
     * @pdb gimp-layer-set-name
     */
    public void setName(String name) throws JGimpException {
        m_App.callProcedure("gimp_layer_set_name", this, name);
    }
    /**
     * Sets the offset of the layer relative to the image it's in.
     * @param x The x-offset of the layer.
     * @param y The y-offset of the layer.
     * @throws JGimpException
     * @pdb gimp-layer-set-offsets
     */
    public void setOffset(int x, int y) throws JGimpException {
        m_App.callProcedure("gimp_layer_set_offsets", this, new JGimpInt32(x), new JGimpInt32(y));
    }
    /**
     * Sets the opacity of this layer.
     * @param opacity A number between 0.0 and 1.0.
     * @throws JGimpException
     * @pdb gimp-layer-set-opacity
     */
    public void setOpacity(double opacity) throws JGimpException {
        m_App.callProcedure("gimp_layer_set_opacity", this, new Double(opacity));
    }
    /**
     * Sets whether to preserve the transparency of the layer.
     * @param preserve Set to true to preserve the transparency of the layer.
     * @throws JGimpException
     * @pdb gimp-layer-set-preserve-trans
     */
    public void setPreserveTransparency(boolean preserve) throws JGimpException {
        m_App.callProcedure("gimp_layer_set_preserve_trans", this, new Boolean(preserve));
    }
    /**
     * Toggles the visibility of the layer mask.
     * @param show Set to true to show the mask.
     * @throws JGimpException
     * @pdb gimp-layer-set-show-mask
     */
    public void setShowMask(boolean show) throws JGimpException {
        m_App.callProcedure("gimp_layer_set_show_mask", this, new Boolean(show));
    }
    /**
     * Sets the visibility of this layer.
     * @param visible Set to true to show the layer, false to hide it.
     * @throws JGimpException
     * @pdb gimp-layer-set-visible
     */
    public void setVisible(boolean visible) throws JGimpException {
        m_App.callProcedure("gimp_layer_set_visible", this, new Boolean(visible));
    }
    /**
     * Translate the layer by the given amount.
     * @param x_amount Amount to translate in the x-axis.
     * @param y_amount Amount to translate in the y-axis.
     * @throws JGimpException
     * @pdb gimp-layer-translate
     */
    public void translate(int x_amount, int y_amount) throws JGimpException {
        m_App.callProcedure("gimp_layer_translate", this, new JGimpInt32(x_amount), new JGimpInt32(y_amount));
    }
}
