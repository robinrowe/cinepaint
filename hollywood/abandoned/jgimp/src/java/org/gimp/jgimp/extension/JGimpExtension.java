/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp.extension;

import org.gimp.jgimp.GimpApp;

/**
 * JGimpExtensions are Java programs that use the GIMP as an image-processing
 * back-end. Unlike plug-ins, they do not add functionality to the
 * GIMP, but rather use the GIMP as an image manipulation service or engine.
 * <p>
 * Extensions in JGimp are loaded once and asked to shutdown when the GIMP is closing.
 * <p>
 * To build and install an extension, simply create a class that implements this interface,
 * compile it, and place it (all class files, or a jar file containing everything)
 * within JGimp's "javafiles" directory (gimpbasedir/lib/gimp/1.2/javafiles).
 * <p>
 * Currently, this interface does not let extensions install themselves in the GIMP's
 * menu.
 */
public interface JGimpExtension
{
    //TODO: Add ability to install in a menu 
	/**
	 * Called by JGimp when the app is starting up to start
	 * the extension.
	 */
	public void run(GimpApp app);
	/**
	 * Called by JGimp when the app is shutting down or the extensions
     * are being reloaded.
	 */
	public void stop(GimpApp app);
}
