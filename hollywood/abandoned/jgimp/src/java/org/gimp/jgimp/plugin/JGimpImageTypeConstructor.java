/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp.plugin;

/**
 * A class used to construct an object representing the types of
 * images a plug-in can operate on. This same information can be
 * represented as a String, if you are familiar with the GIMP's
 * image naming conventions (that is, you only really need to use
 * this if you want to be completely sure you have the image
 * types in a format the GIMP can understand).
 * @see org.gimp.jgimp.plugin.JGimpProcedureDescriptor
 */
public class JGimpImageTypeConstructor
{
	public interface Type
	{
		public final static String RGB           = "RGB";
		public final static String RGBA          = "RGBA";
		public final static String RGB_ANY       = "RGB*";

		public final static String GRAY          = "GRAY";
		public final static String GRAYA         = "GRAYA";
		public final static String GRAY_ANY      = "GRAY*";

		public final static String INDEXED       = "INDEXED";
		public final static String INDEXEDA      = "INDEXEDA";
		public final static String INDEXED_ANY   = "INDEXED*";

		public final static String ANY_IMAGE     = "*";
	}

	private String m_Type = null;

    public String getType() {
        return m_Type;
    }
    public String toString() {
        return this.getType();
    }
    private JGimpImageTypeConstructor(String inType) {
        m_Type = inType;
    }

    /**
     * Returns a new object that indicates a plug-in can operate on images 
     * represented by this object, and the object passed in (does not modify this object).
     */
    public JGimpImageTypeConstructor concatImageType(JGimpImageTypeConstructor inType) {
        if (inType == null) {
            return this;
        }
        return new JGimpImageTypeConstructor(this.getType() + ", " + inType.getType());
    }
    /**
     * Creates an object indicating that a plug-in can operate on RGB images
     */
    public static JGimpImageTypeConstructor createRGBImageType() {
        return new JGimpImageTypeConstructor(Type.RGB);
    }
    /**
     * Creates an object indicating that a plug-in can operate on RGB images with an alpha channel
     */
    public static JGimpImageTypeConstructor createAlphaRGBImageType() {
        return new JGimpImageTypeConstructor(Type.RGBA);
    }
    /**
     * Creates an object indicating that a plug-in can operate on any RGB image
     */
    public static JGimpImageTypeConstructor createAnyRGBImageType() {
        return new JGimpImageTypeConstructor(Type.RGB_ANY);
    }
    /**
     * Creates an object indicating that a plug-in can operate on grayscale images
     */
    public static JGimpImageTypeConstructor createGrayscaleImageType() {
        return new JGimpImageTypeConstructor(Type.GRAY);
    }
    /**
     * Creates an object indicating that a plug-in can operate on grayscale images with an alpha channel
     */
    public static JGimpImageTypeConstructor createAlphaGrayscaleImageType() {
        return new JGimpImageTypeConstructor(Type.GRAYA);
    }
    /**
     * Creates an object indicating that a plug-in can operate on any grayscale images
     */
    public static JGimpImageTypeConstructor createAnyGrayscaleImageType() {
        return new JGimpImageTypeConstructor(Type.GRAY_ANY);
    }
    /**
     * Creates an object indicating that a plug-in can operate on indexed images
     */
    public static JGimpImageTypeConstructor createIndexedImageType() {
        return new JGimpImageTypeConstructor(Type.INDEXED);
    }
    /**
     * Creates an object indicating that a plug-in can operate on indexed images with an alpha channel
     */
    public static JGimpImageTypeConstructor createAlphaIndexedImageType() {
        return new JGimpImageTypeConstructor(Type.INDEXEDA);
    }
    /**
     * Creates an object indicating that a plug-in can operate on any indexed image
     */
    public static JGimpImageTypeConstructor createAnyIndexedImageType() {
        return new JGimpImageTypeConstructor(Type.INDEXED_ANY);
    }
    /**
     * Creates an object indicating that a plug-in can operate on any image type
     */
    public static JGimpImageTypeConstructor createAnyImageType() {
        return new JGimpImageTypeConstructor(Type.ANY_IMAGE);
    }
}
