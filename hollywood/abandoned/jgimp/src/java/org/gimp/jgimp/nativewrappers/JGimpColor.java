/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp.nativewrappers;

import java.awt.Color;

import org.gimp.jgimp.*;

/**
 * Encapsulates data refering to a color in the GIMP.
 */
public class JGimpColor extends JGimpData {
    
    private int m_Red = -1;
    private int m_Blue = -1;
    private int m_Green = -1;

    public JGimpColor(java.awt.Color inColor) {
        m_Red = inColor.getRed();
        m_Green = inColor.getGreen();
        m_Blue = inColor.getBlue();
    }

    public JGimpColor(int red, int green, int blue) {
        m_Red = red;
        m_Green = green;
        m_Blue = blue;
    }

    public int getRed() {
        return m_Red;
    }

    public int getGreen() {
        return m_Green;
    }

    public int getBlue() {
        return m_Blue;
    }

    public Color getColor() {
        return new Color(m_Red, m_Green, m_Blue);
    }

    public String toString() {
        return "JGimpColor, values: r: " + m_Red + " g: " + m_Green + " b: " + m_Blue;
    }

    public boolean equals(Object inRight) {
        if (inRight == null) {
            return false;
        }
        if (inRight instanceof JGimpColor) {
            JGimpColor rightColor = (JGimpColor) inRight;
            return ((rightColor.getRed() == this.getRed()) && (rightColor.getGreen() == this.getGreen()) && (rightColor.getBlue() == this.getBlue()));
        }
        return false;
    }

    /**
     * @see org.gimp.jgimp.JGimpData#convertToColor()
     */
    public Color convertToColor() throws JGimpInvalidDataCoercionException {
        return this.getColor();
    }

    /**
     * @see org.gimp.jgimp.JGimpData#getGimpPDBArgType()
     */
    public int getGimpPDBArgType() {
        return JGimpPDBArgTypeConstants.PDB_COLOR;
    }
    
}
