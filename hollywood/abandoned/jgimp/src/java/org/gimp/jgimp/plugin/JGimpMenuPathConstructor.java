/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp.plugin;

/**
 * Constructs a valid menu path for installing a plug-in into
 * the Gimp's menu structure. This same data can be represented
 * as a String if you know the GIMP's convention (so you only really
 * need to use this if you are unfamiliar with how the GIMP organizes
 * its menus).
 */
public class JGimpMenuPathConstructor
{
	public interface TopLevelPath
	{
		public final static String TOOLBOX   = "<Toolbox>/";
		public final static String IMAGE     = "<Image>/";
		public final static String LOAD      = "<Load>/";
		public final static String SAVE      = "<Save>/";
	}

    /**
     * Constructs a menu path for a toolbox menu item.
     * The top-level Toolbox menu path is automatically added,
     * so you only need to add the rest of the path to your plug-in.
     * The path separator is a forward slash (/).
     * Example: createToolboxMenuPath("Filters/MyPlugIn")
     */
    public static String createToolboxMenuPath(String inPathToPlugIn) {
        return TopLevelPath.TOOLBOX + inPathToPlugIn;
    }

    /**
     * Constructs a menu path for an image menu item.
     * The top-level Image menu path is automatically added,
     * so you only need to add the rest of the path to your plug-in.
     * The path separator is a forward slash (/).
     * Example: createImageMenuPath("Filters/MyPlugIn")
     */
    public static String createImageMenuPath(String inPathToPlugIn) {
        return TopLevelPath.IMAGE + inPathToPlugIn;
    }

    /**
     * Constructs a menu path for a load menu item.
     * The top-level Load menu path is automatically added,
     * so you only need to add the rest of the path to your plug-in.
     * The path separator is a forward slash (/).
     * Example: createLoadMenuPath("Filters/MyPlugIn")
     */
    public static String createLoadMenuPath(String inPathToPlugIn) {
        return TopLevelPath.LOAD + inPathToPlugIn;
    }

    /**
     * Constructs a menu path for a save menu item.
     * The top-level Save menu path is automatically added,
     * so you only need to add the rest of the path to your plug-in.
     * The path separator is a forward slash (/).
     * Example: createSaveMenuPath("Filters/MyPlugIn")
     */
    public static String createSaveMenuPath(String inPathToPlugIn) {
        return TopLevelPath.SAVE + inPathToPlugIn;
    }
}
