/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp.plugin;

import org.gimp.jgimp.GimpApp;
import org.gimp.jgimp.JGimpData;
/**
 * The interface for any Java plug-in for the GIMP.
 * <p>
 * Requirements for building your own plug-in:
 * <ul>
 * <li> If the plug-in is automatically loaded by the GIMP
 *      (as opposed to you manually installing the plug-in),
 *      then the class should provide a constructor with no arguments.
 *      Otherwise, you can pass in a reference to a JGimpPlugIn
 *      object to either
 *      {@link org.gimp.jgimp.proxy.JGimpProxy} or 
 *      {@link org.gimp.jgimp.GimpApp}'s method for installing a plug-in.
 *      However, in this case, your plug-in needs to remain resident
 *      (i.e., it is not instantiated every time the plug-in is called).
 * </ul>
 * <p>
 * This interface lets users install multiple plug-ins from the same class,
 * provided each provides a unique name for each JGimpProcedureDescriptor.
 * <p>
 * For those familiar with coding C-style plug-ins for the GIMP,
 * the standard set of methods to implement is slightly different.
 *       <p>
 *       Differences:
 *       <ul>
 *       <li>There is no "init" function. This is taken care of by your class's constructor.
 *       <li>The query interface is replaced with a call to getPlugInInfo. This method
 *           returns an array describing each individual plug-in available from this class.
 *       <li>The plug-in can remain resident, in which case the same instance of
 *           the object is always used, or it can be instantiated every time it is called.
 *           If it remains resident, you should ensure that the method calls are thread-safe.
 *           Override the remainResident() method to indicate which behavior your plug-in
 *           implements.
 *       <li>The "quit" method is only called if the object remains resident.
 *       </ul>
 */
public interface JGimpPlugIn
{
	/**
	 * Returns "true" if this plug-in is instantiated only once, with multiple
	 * calls to run() going to the same object. The alternative is that a new object
	 * is created each time "run" is called.
	 * This method should only return true if the plug-in is thread-safe.
	 */
	public boolean remainResident(GimpApp app);

	/**
	 * Returns an array of JGimpProcedureDescriptor's that will be used
	 * to install the plug-in(s) in the GIMP's PDB.
	 * A plug-in can install multiple procedures, but each name in each
	 * descriptor must be unique.
	 */
    public JGimpProcedureDescriptor[] getPlugInInfo(GimpApp app);

	/**
	 * Executes the plug-in of the following name with the following data.
	 * If the plug-in can remain resident, this method must be thread-safe.
	 */
    public JGimpPlugInReturnValues run(GimpApp app, String inName, JGimpData[] inParams);

	/**
	 * Called when the application is quitting or the plug-ins are being reloaded.
	 */
    public void stop(GimpApp app);
}
