/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp.plugin;

/**
 * Describes a plug-in (its name, the parameters it takes, where to install it in
 * the GIMP's menus, etc.) so it can be installed in the GIMP's PDB.
 */
public class JGimpProcedureDescriptor {
    private String m_Name = "";
    private String m_Blurb = "";
    private String m_Help = "";
    private String m_Author = "";
    private String m_Copyright = "";
    private String m_Date = "";
    private String m_MenuPath = "";
    private String m_ImageTypes = "";
    private JGimpParamDescriptor[] m_ParamDescriptions = null;
    private JGimpParamDescriptor[] m_ReturnValueDescriptions = null;

    /**
     * Creates a new procedure descriptor.
     * @param inName The name of the plug-in
     * @param inBlurb The blurb describing the plug-in
     * @param inHelp Any helpful information for using the plug-in
     * @param inAuthor The author of the plug-in
     * @param inCopyright Copyright information
     * @param inDate The date the plug-in was created
     * @param inMenuPath The place in the GIMP's menu hierarchy to install the plug-in
     * @param inImageTypes The types of images the plug-in can operate on
     * @param inParamDescriptions Descriptions of the individual parameters. Must be non-null and include
     *                            an int32, image, and drawable as the first three parameters. This may change in
     *                            the future so that these are automatically added for you 
     * @param inReturnValueDescriptions Descriptions of the return values. Can be null if no data are returned
     * @see JGimpMenuPathConstructor
     * @see JGimpImageTypeConstructor
     */
    public JGimpProcedureDescriptor(
        String inName,
        String inBlurb,
        String inHelp,
        String inAuthor,
        String inCopyright,
        String inDate,
        String inMenuPath,
        String inImageTypes,
        JGimpParamDescriptor[] inParamDescriptions,
        JGimpParamDescriptor[] inReturnValueDescriptions) {
        //TODO: Automatically add the first three params for plug-ins, since they are required, right?
        m_Name = inName;
        m_Blurb = inBlurb;
        m_Help = inHelp;
        m_Author = inAuthor;
        m_Copyright = inCopyright;
        m_Date = inDate;
        if (inMenuPath != null) {
            m_MenuPath = inMenuPath;
        } else {
            m_MenuPath = "";
        }
        if (inImageTypes != null) {
            m_ImageTypes = inImageTypes;
        } else {
            m_ImageTypes = "";
        }

        if (inParamDescriptions == null) {
            m_ParamDescriptions = new JGimpParamDescriptor[0];
        } else {
            m_ParamDescriptions = new JGimpParamDescriptor[inParamDescriptions.length];
            System.arraycopy(inParamDescriptions, 0, m_ParamDescriptions, 0, inParamDescriptions.length);
        }

        if (inReturnValueDescriptions == null) {
            m_ReturnValueDescriptions = new JGimpParamDescriptor[0];
        } else {
            m_ReturnValueDescriptions = new JGimpParamDescriptor[inReturnValueDescriptions.length];
            System.arraycopy(inReturnValueDescriptions, 0, m_ReturnValueDescriptions, 0, inReturnValueDescriptions.length);
        }
    }

    public String getName() {
        return m_Name;
    }

    public String getBlurb() {
        return m_Blurb;
    }

    public String getHelp() {
        return m_Help;
    }

    public String getAuthor() {
        return m_Author;
    }

    public String getCopyright() {
        return m_Copyright;
    }

    public String getDate() {
        return m_Date;
    }

    public String getMenuPath() {
        return m_MenuPath;
    }

    public String getImageTypes() {
        return m_ImageTypes;
    }

    public JGimpParamDescriptor[] getParameterDescriptions() {
        JGimpParamDescriptor[] returnArray = new JGimpParamDescriptor[m_ParamDescriptions.length];
        System.arraycopy(m_ParamDescriptions, 0, returnArray, 0, m_ParamDescriptions.length);
        return returnArray;
    }

    public JGimpParamDescriptor[] getReturnValueDescriptions() {
        JGimpParamDescriptor[] returnArray = new JGimpParamDescriptor[m_ReturnValueDescriptions.length];
        System.arraycopy(m_ReturnValueDescriptions, 0, returnArray, 0, m_ReturnValueDescriptions.length);
        return returnArray;
    }

    public String toString() {
        String returnString = "Procedure name: " + this.getName() + " blurb: " + this.getBlurb();
        returnString = returnString + " help: " + this.getHelp() + " author: " + this.getAuthor();
        returnString = returnString + " copyright: " + this.getCopyright() + " date: " + this.getDate();
        returnString = returnString + " menu path: " + this.getMenuPath() + " image types: " + this.getImageTypes();
        for (int i = 0; i < m_ParamDescriptions.length; i++) {
            returnString = returnString + " param num " + i + ": " + m_ParamDescriptions[i].toString();
        }
        for (int i = 0; i < m_ReturnValueDescriptions.length; i++) {
            returnString = returnString + " return val num " + i + ": " + m_ReturnValueDescriptions[i].toString();
        }
        return returnString;
    }

}
