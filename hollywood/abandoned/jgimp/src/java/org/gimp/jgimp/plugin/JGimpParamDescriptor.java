/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp.plugin;



/**
 * Encapsulates information describing a parameter in a plug-in's function.
*/
public class JGimpParamDescriptor
{
    private int m_GimpPDBArgType;
    private String m_Name = "";
    private String m_Description = "";

    /**
     * The PDBArgType is one from org.gimp.jgimp.objects.datawrappers.JGimpPDBArgTypeConstants.
     */
    public JGimpParamDescriptor(int inGimpPDBArgType, String inName, String inDescription)
    {
		m_GimpPDBArgType = inGimpPDBArgType;
        m_Name = inName;
        m_Description = inDescription;
    }
	public int getGimpPDBArgType()
    {
        return m_GimpPDBArgType;
    }
    public String getName()
    {
        return m_Name;
    }
    public String getDescription()
    {
        return m_Description;
    }

	public String toString()
	{
		return m_Name + " : " + m_Description;
	}
}
