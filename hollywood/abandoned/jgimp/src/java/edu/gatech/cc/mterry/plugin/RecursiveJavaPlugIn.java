/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package edu.gatech.cc.mterry.plugin;

import org.gimp.jgimp.GimpApp;
import org.gimp.jgimp.JGimpData;
import org.gimp.jgimp.JGimpPDBArgTypeConstants;
import org.gimp.jgimp.plugin.JGimpImageTypeConstructor;
import org.gimp.jgimp.plugin.JGimpMenuPathConstructor;
import org.gimp.jgimp.plugin.JGimpParamDescriptor;
import org.gimp.jgimp.plugin.JGimpPlugIn;
import org.gimp.jgimp.plugin.JGimpPlugInReturnValues;
import org.gimp.jgimp.plugin.JGimpProcedureDescriptor;


/**
 * @author Michael Terry
 * This class is simply a test class for testing recursive calls
 * to the library
 */
public class RecursiveJavaPlugIn implements JGimpPlugIn {

    /**
     * Returns "true" if this plug-in is instantiated only once, with multiple
     * calls to run() going to the same object. The alternative is that a new object
     * is created each time "run" is called.
     */
    public boolean remainResident(GimpApp inApp) {
        // We are instantiated every time (no need to remain resident)
        return false;
    }

    /**
     * Returns an array of JGimpProcedureDescriptor's that will be used
     * to install the plug-in in the GIMP's PDB.
     */
    public JGimpProcedureDescriptor[] getPlugInInfo(GimpApp inApp) {
        /*
         * Make an array of one parameter descriptor corresponding to our single plug-in
         */
        JGimpProcedureDescriptor[] plugInInfoArray = new JGimpProcedureDescriptor[1];

        /*
         * Build a list of the parameters of this plug-in
         */
        JGimpParamDescriptor[] pdbParams = new JGimpParamDescriptor[3];
        pdbParams[0] = new JGimpParamDescriptor( JGimpPDBArgTypeConstants.PDB_INT32,
                                                 "run_mode",
                                                 "Interactive, non-interactive" ); // Can run interactively, or non-interactively
        pdbParams[1] = new JGimpParamDescriptor( JGimpPDBArgTypeConstants.PDB_IMAGE,
                                                 "image",
                                                 "Input image" ); // The image we're going to work on
        pdbParams[2] = new JGimpParamDescriptor( JGimpPDBArgTypeConstants.PDB_DRAWABLE,
                                                 "drawable",
                                                 "Input drawable"); // The drawable we're going to work on

        /*
         * Create the complete descriptor for our plug-in
         */
        plugInInfoArray[0] = new JGimpProcedureDescriptor(
                /* name of the plug-in as installed in the PDB */ "recursive_plug_in_image_divider",
                /* description of the plug-in                  */ "Simple Java Plug-In example for 4x4 digital images",
                /* the "help" message for this plug-in         */ "This plug-in was built specifically for Nikon Coolpix images that place 16 images in a 4x4 grid on one image. The plug-in will extract the 16 images and place them in layers in a new image.",
                /* Author                                      */ "Michael Terry",
                /* Copyright information                       */ "Copyright 2002, Michael Terry",
                /* Date of writing                             */ "23 June, 2002",
                /* Menu path where it is installed             */ JGimpMenuPathConstructor.createImageMenuPath("Filters/JGimp Test/Recursive Plug In"),
                /* Types of images this plug-in can work on    */ JGimpImageTypeConstructor.createAnyImageType().toString(),
                /* The parameters we accept                    */ pdbParams,
                /* Return value descriptions (there are none)  */ null);

        return plugInInfoArray;
    }

    /**
     * Executes the plug-in of the following name with the following data.
     * If the plug-in can remain resident, this method must be thread-safe.
     */
    public JGimpPlugInReturnValues run(GimpApp inApp, String inName, JGimpData[] inParams) {
        try {
            inApp.callProcedure("plug_in_image_divider", inParams);

        } catch (Exception e) {
            System.err.println("Error caught in TESTER: " + e.getMessage());
            e.printStackTrace();
            JGimpPlugInReturnValues.returnExecutionError();
        }

        return JGimpPlugInReturnValues.returnSuccess();
    }

    /**
     * Called when the application is quitting
     */
    public void stop(GimpApp inApp) {
    }
}
