/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * This is a set of wrapper functions so that we can
 * either embed jgimp directly in the GIMP's process (for
 * efficiency and speed) or as a normal plug-in.
 */

#include "jgimp_gimp_adapter.h"


void        adapter_install_temp_proc   (gchar        *name,
				           gchar        *blurb,
				           gchar        *help,
				           gchar        *author,
				           gchar        *copyright,
				           gchar        *date,
				           gchar        *menu_path,
				           gchar        *image_types,
				           gint          type,
				           gint          nparams,
				           gint          nreturn_vals,
				           GimpParamDef *params,
				           GimpParamDef *return_vals,
				           GimpRunProc   run_proc)
{
	gimp_install_temp_proc(name,
			blurb,
			help,
			author,
			copyright,
			date,
			menu_path,
			image_types,
			type,
			nparams,
			nreturn_vals,
			params,
			return_vals,
			run_proc);
}

GimpParam * adapter_run_pdb_procedure(gchar *name, gint *nreturn_vals, gint nparams, GimpParam *params)
{
	if ( (name == NULL) || (nreturn_vals == NULL) || ((params == NULL) && (nparams > 0)))
	{
		fprintf(stderr, "JGimp internal error: no name, nreturn_vals, or params in adapter_run_pdb_procedure\n");
		return NULL;
	}
	return gimp_run_procedure2(name, nreturn_vals, nparams, params);
}

void        adapter_destroy_pdb_procedure_return_vals(GimpParam *params, gint nparams)
{
	if (params == NULL)
	{
		return;
	}
	gimp_destroy_params(params, nparams);
}

JGimpDrawableInfo adapter_get_drawable_info(gint32 drawable_ID)
{
	JGimpDrawableInfo return_info;
#ifdef JGIMP_COMPILE_FOR_FILMGIMP
	GDrawableType drawable_type;
#endif

	return_info.id                = drawable_ID;
	return_info.width             = gimp_drawable_width(drawable_ID);
	return_info.height            = gimp_drawable_height(drawable_ID);
	return_info.bpp               = gimp_drawable_bpp(drawable_ID);
	return_info.has_alpha         = gimp_drawable_has_alpha(drawable_ID);
#ifndef JGIMP_COMPILE_FOR_FILMGIMP
	return_info.is_rgb            = gimp_drawable_is_rgb(drawable_ID);
	return_info.is_gray           = gimp_drawable_is_gray(drawable_ID);
	return_info.is_indexed        = gimp_drawable_is_indexed(drawable_ID);
#else
	drawable_type = gimp_drawable_type(drawable_ID);
	return_info.is_rgb            = ( (drawable_type == RGB_IMAGE)
                                   || (drawable_type == RGBA_IMAGE)
								   || (drawable_type == U16_RGB_IMAGE)
								   || (drawable_type == U16_RGBA_IMAGE)
								   || (drawable_type == FLOAT_RGB_IMAGE)
								   || (drawable_type == FLOAT_RGBA_IMAGE)
								   || (drawable_type == FLOAT16_RGB_IMAGE)
								   || (drawable_type == FLOAT16_RGBA_IMAGE)
			                       );
	return_info.is_gray           = ( (drawable_type == GRAY_IMAGE)
                                   || (drawable_type == GRAYA_IMAGE)
								   || (drawable_type == U16_GRAY_IMAGE)
								   || (drawable_type == U16_GRAYA_IMAGE)
								   || (drawable_type == FLOAT_GRAY_IMAGE)
								   || (drawable_type == FLOAT_GRAYA_IMAGE)
								   || (drawable_type == FLOAT16_GRAY_IMAGE)
								   || (drawable_type == FLOAT16_GRAYA_IMAGE)
			                       );
	return_info.is_indexed        = ( (drawable_type == INDEXED_IMAGE)
                                   || (drawable_type == INDEXEDA_IMAGE)
								   || (drawable_type == U16_INDEXED_IMAGE)
								   || (drawable_type == U16_INDEXEDA_IMAGE)
			                       );
#endif
	return_info.is_valid_drawable = ((return_info.width < 0) ? 0 : 1);

	return return_info;
}
jlong             adapter_get_pixel_rgn(JGimpDrawableInfo* drawable,
										gint x,
										gint y,
										gint width,
										gint height,
										guchar* buffer,
										gint buffer_offset,
										gint buffer_length)
{
	GimpPixelRgn  pixel_rgn;
	GimpDrawable* gimp_drawable;

	if (buffer == NULL)
	{
		fprintf(stderr, "Internal jgimp error: NULL buffer passed to adapter_get_pixel_rgn\n");
		return -1;
	}
	if (drawable == NULL)
	{
		fprintf(stderr, "Internal jgimp error: NULL drawable info passed to adapter_get_pixel_rgn\n");
		return -1;
	}
	if (!(drawable->is_valid_drawable))
	{
		fprintf(stderr, "Internal jgimp error: Invalid drawable info passed to adapter_get_pixel_rgn\n");
		return -1;
	}
	if ((drawable->bpp * width * height) > (buffer_length - buffer_offset))
	{
		fprintf(stderr, "Internal jgimp error: buffer underflow error in adapter_get_pixel_rgn\n");
		return -1;
	}

	gimp_drawable = gimp_drawable_get(drawable->id);
	if (gimp_drawable == NULL)
	{
		fprintf(stderr, "Internal jgimp error: could not get drawable in adapter_get_pixel_rgn\n");
		return -1;
	}
	
	gimp_pixel_rgn_init(&pixel_rgn, gimp_drawable, x, y, width, height, FALSE, FALSE); /* Last two params: no dirty, no shadow */
	gimp_pixel_rgn_get_rect(&pixel_rgn, buffer + buffer_offset, x, y, width, height);
	gimp_drawable_detach(gimp_drawable);
	gimp_drawable = NULL;

	return (width * height * drawable->bpp);
}
jlong             adapter_set_pixel_rgn(JGimpDrawableInfo* drawable,
										gint x,
										gint y,
										gint width,
										gint height,
										guchar* buffer,
										gint buffer_offset,
										gint buffer_length,
										gboolean flush_drawable,
										gboolean merge_shadow,
										gboolean update_drawable)
{
	GimpPixelRgn  pixel_rgn;
	GimpDrawable* gimp_drawable;

	if (buffer == NULL)
	{
		fprintf(stderr, "Internal jgimp error: NULL buffer passed to adapter_set_pixel_rgn\n");
		return -1;
	}
	if (drawable == NULL)
	{
		fprintf(stderr, "Internal jgimp error: NULL drawable info passed to adapter_set_pixel_rgn\n");
		return -1;
	}
	if (!(drawable->is_valid_drawable))
	{
		fprintf(stderr, "Internal jgimp error: Invalid drawable info passed to adapter_set_pixel_rgn\n");
		return -1;
	}
	if ((drawable->bpp * width * height) > (buffer_length - buffer_offset))
	{
		fprintf(stderr, "Internal jgimp error: buffer underflow error in adapter_set_pixel_rgn\n");
		return -1;
	}

	gimp_drawable = gimp_drawable_get(drawable->id);
	if (gimp_drawable == NULL)
	{
		fprintf(stderr, "Internal jgimp error: could not get drawable in adapter_set_pixel_rgn\n");
		return -1;
	}
	
	gimp_pixel_rgn_init(&pixel_rgn, gimp_drawable, x, y, width, height, TRUE, TRUE); /* Last two params: yes dirty, yes shadow */
	gimp_pixel_rgn_set_rect(&pixel_rgn, buffer + buffer_offset, x, y, width, height);
	if (flush_drawable)
	{
		gimp_drawable_flush(gimp_drawable);
	}
	if (merge_shadow)
	{
		gimp_drawable_merge_shadow(gimp_drawable->id, TRUE);
	}
	if (update_drawable)
	{
		gimp_drawable_update(gimp_drawable->id, x, y, width, height);
	}
	gimp_drawable_detach(gimp_drawable);
	gimp_drawable = NULL;
	return (width * height * drawable->bpp);
}
