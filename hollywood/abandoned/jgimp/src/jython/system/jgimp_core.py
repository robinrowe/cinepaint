# JGimp - A Java extension for the GIMP enabling users to write scripts in Java and Python/Jython
# Copyright (C) 2003  Georgia Tech Research Corp.
# Written by Michael Terry, mterry@cc.gatech.edu
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

"""
This module defines core Jython operations for interacting with
the GIMP and JGimp
"""

from org.gimp.jgimp.plugin import *
from org.gimp.jgimp.jython import *
from org.gimp.jgimp.nativewrappers import *
import org.python.core
import os
import sys

# Sets the gimpApp global variable
gimpApp = JGimpJythonInterpreter.getInstance().getGimpApp()

"""
A Jython-wrapper for JGimp plug-ins.
The wrapper adds a parameter to specify a plug-in
function to call when a plug-in is called. This
function must take three parameters: gimpApp, plug-in name,
and paramList. gimpApp is a org.gimp.jgimp.GimpApp object,
plug-in name is the name of the plug-in being called,
and paramList is a list of at least three parameters: runInteractive,
image, and drawable.
"""
class JythonPlugIn(JGimpPlugIn):
	def __init__(self,
				 inName,
				 inBlurb,
				 inHelp,
				 inAuthor,
                 inCopyright,
				 inDate,
				 inMenuPath,
				 inImageTypes,
				 inParams,
				 inReturnVals,
				 inPluginFunction):
		self.name = inName
		self.blurb = inBlurb
		self.help = inHelp
		self.author = inAuthor
		self.copyright = inCopyright
		self.date = inDate
		self.menu_path = inMenuPath
		self.image_types = inImageTypes
		self.params = convert_descriptors(inParams)
		self.return_vals = convert_descriptors(inReturnVals)
		self.plugin_function = inPluginFunction
	def remainResident(self, app):
		return 1
	def getPlugInInfo(self, app):
		return [JGimpProcedureDescriptor(
			self.name, self.blurb, self.help, self.author, self.copyright,
			self.date, self.menu_path, self.image_types, self.params,
			self.return_vals)]
	def run(self, app, plugin_name, params):
		result = self.plugin_function(app, plugin_name, params)
		if (result == None): result = (PDB_SUCCESS, )
		if (not isinstance(result, org.python.core.PyTuple)): result = (result, )
		return JGimpPlugInReturnValues(JGimpPDBStatus(result[0]), result[1:])
	def stop(self, app):
		pass

"""
Installs a Jython-based plug-in for the GIMP, where the
plug-in is defined as a Jython function
"""
def install_plugin(name,
				   blurb,
				   help,
				   author,
                   copyright,
				   date,
				   menu_path,
				   image_types,
				   params,
				   ret_vals,
				   plugin_function):
   if (gimpApp.proxy.canInstallPlugIns()):
        gimpApp.installPlugIn(JythonPlugIn(name, blurb, help, author, copyright,
                                               date, menu_path, image_types,
                                               params, ret_vals, plugin_function))

def convert_descriptors(params):
	returnParams = []
	for thisParam in params:
		(type, name, description) = thisParam
		returnParams.append(JGimpParamDescriptor(type, name, description))
	return returnParams

def load_script_directory(script_directory):
	for file in os.listdir(script_directory):
		if (file == "jgimp_core.py"):
			continue
		if (not file.lower().endswith(".py")):
			continue
		__import__(file[:file.rfind(".py")])
		