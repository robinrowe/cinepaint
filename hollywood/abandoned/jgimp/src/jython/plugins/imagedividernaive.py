# JGimp - A Java extension for the GIMP enabling users to write
# scripts in Java and Python/Jython
# Copyright (C) 2003  Georgia Tech Research Corp.
# Written by Michael Terry, mterry@cc.gatech.edu
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

from jgimp_core import install_plugin
from gimpenums import *

"""
Divides an image into a 4x4 grid, and copies each
cell into a new layer in a new image. It must take
the following three parameters: the gimpApp object,
the name of the plug-in being called, and a list of
parameters passed to the plug-in. The paramList contains
at least the following three variables: runInteractive,
image, and drawable, which indicate whether the plug-in
is being run interactively, and the image and drawable
on which to operate.

This particular implementation will not handle large
images, since it reads an entire tile in at once.
"""
def jython_image_divider(gimpApp, pluginName, paramList):

    (runInteractive, image, drawable) = paramList
    NUM_TILES = 4 # The number of tiles to divide this drawable int

	# Calculate the size of the new image
    newWidth = drawable.size.width / NUM_TILES
    newHeight = drawable.size.height / NUM_TILES

	# Create a new image of the same type as the image passed in
    newImage = gimpApp.createImage(newWidth, newHeight, image.type)

	# Now copy each tile into a new layer in the new image
    pixelBuf = None
    for y_tile in range(NUM_TILES):
        for x_tile in range(NUM_TILES):
            layerNum = y_tile * NUM_TILES + x_tile
            newLayer = newImage.appendNewLayer(newWidth,
                                               newHeight,
                                               drawable.hasAlpha(),
                                               "Layer number " + str(layerNum + 1),
                                               100,
                                               NORMAL_MODE)
			# The pixelBuf is a buffer used to read the pixel data. We save and reuse it
			# so we're not constantly allocating a new buffer
            if (pixelBuf == None):
                pixelBuf = drawable.readPixelRegionInNativeByteFormat(x_tile * newWidth,
																	  y_tile * newHeight,
																	  newWidth,
																	  newHeight)
            else:
                drawable.readPixelRegionInNativeByteFormat(x_tile * newWidth,
                                                           y_tile * newHeight,
                                                           newWidth,
                                                           newHeight,
                                                           pixelBuf,
                                                           0)
			# Write the pixels to the new image/drawable
            newLayer.writePixelRegionInNativeByteFormat(0, 0, newWidth, newHeight, pixelBuf, 0)
	
	# Display the new image and force an update
    newImage.display()
    gimpApp.flushDisplays()
    return PDB_SUCCESS # Could also be returned as a list of data (PDB_SUCCESS,)
                       # PDB_SUCCESS is defined in gimpenums.py

# Install the plug-in in the GIMP
install_plugin("jython-image-divider-naive",                                         # The plug-in's unique name
               "Divides image into a 4x4 grid, pastes each tile into a new image",   # Its description
               "Help goes here",                                                     # A help message
               "Michael Terry",                                                      # The author
               "Copyright 2003, GTRC and Michael Terry",                             # Copyright information
               "April 2003",                                                         # Date it was made
               "<Image>/Filters/Digital Cameras/Nikon Image Divider (Jython Naive)", # Where to install it in the menu
               "*",                                                                  # The image types it can work on
               [                                                                     # The parameters this plug-in takes
                   (PDB_INT32, "Interactive", "Run interactive or not?"),
                   (PDB_IMAGE, "image", "The source image"),
                   (PDB_DRAWABLE, "drawable", "The drawable to divide up")
                ],
               [],                                                                   # The return values of this plug-in
               jython_image_divider)                                                 # The function to call
