[Setup]
AppName=Gimp
AppID=Gimp-1.2.4
AppVerName=Gimp
AppPublisher=JGimp
AppPublisherURL=http://jgimp.sourceforge.net/
AppSupportURL=http://jgimp.sourceforge.net/
AppUpdatesURL=http://jgimp.sourgeforge.net/
DefaultDirName={pf}\JGimp
DefaultGroupName=JGimp
AllowNoIcons=true
;AlwaysCreateUninstallIcon=true
LicenseFile=full_license.rtf
;InfoBeforeFile=D:\other\gimp.txt
ChangesAssociations=true
WizardImageFile=GTKLOGO.bmp
;WizardSmallImageFile=gpsmall.bmp
UseSetupLdr=no
OutputDir=Output\Installer
;bzip/9 creates about 500kB smaller file that zip/9 (and compresses faster ;)
Compression=bzip/9
;Compression=none
InternalCompressLevel=0
;AdminPrivilegesRequired=true
MinVersion=4,4
;WizardDebug=yes

[Tasks]
Name: desktopicon; Description: "Create a &desktop icon"; GroupDescription: "Recommended:"; MinVersion: 4,4
Name: gtkrc; Description: "The Gimp uses your Windows system color scheme"; GroupDescription: "Recommended:"

[Components]
Name: base; Description: "Required Files"; Types: full compact custom normal; Flags: fixed
Name: locgood; Description: "Non-English Translations"; Types: full custom normal

[Types]
Name: normal; Description: "Typical installation"
Name: full; Description: "Full installation"
Name: compact; Description: "Compact installation"
Name: custom; Description: "Custom installation"; Flags: iscustom

[Files]
Source: ".\_gtk\*.*"; DestDir: "{app}"; CopyMode: alwaysoverwrite; Components: base; Flags: recursesubdirs
;To prepare files for installation, move Help folder to the root, and move bad localizations to localebad folder in lib folder
;GIMP base
Source: ".\_gimp\_main\*.*"; DestDir: "{app}"; CopyMode: alwaysoverwrite; Components: base; Flags: recursesubdirs
;GIMP help
;Source: ".\_gimp\_help\*.*"; DestDir: "{app}"; Components: help; CopyMode: alwaysoverwrite; Flags: recursesubdirs
;GIMP localization
Source: ".\_gimp\_local\*.*"; DestDir: "{app}"; Components: locgood; CopyMode: alwaysoverwrite; Flags: recursesubdirs
Source: ".\_local\*.*"; DestDir: "{app}"; Components: locgood; CopyMode: alwaysoverwrite; Flags: recursesubdirs
;required libraries
Source: ".\_gimp\_lib\*.*"; DestDir: "{app}"; Components: base; CopyMode: alwaysoverwrite; Flags: recursesubdirs

[INI]
;URL to Gimp-Win homepage
Filename: "{app}\gimp.url"; Section: "InternetShortcut"; Key: "URL"; String: "http://www.gimp.org/win32/"

[Registry]
;file associations main key
Root: "HKCR"; Subkey: "TheGIMP"; ValueType: string; ValueName: ; ValueData: "GIMP image"; Flags: uninsdeletekey
Root: "HKCR"; Subkey: "TheGIMP\DefaultIcon"; ValueType: string; ValueName: ; ValueData: "{app}\bin\gimp.exe,0"
Root: "HKCR"; Subkey: "TheGIMP\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\bin\gimp.exe"" ""%1"""
;file types
Root: "HKCR"; Subkey: ".XCF"; ValueType: string; ValueName: ""; ValueData: "TheGIMP"; Flags: uninsdeletevalue
Root: "HKCR"; Subkey: ".PAT"; ValueType: string; ValueName: ""; ValueData: "TheGIMP"; Flags: uninsdeletevalue
Root: "HKCR"; Subkey: ".GIH"; ValueType: string; ValueName: ""; ValueData: "TheGIMP"; Flags: uninsdeletevalue
Root: "HKCR"; Subkey: ".GBR"; ValueType: string; ValueName: ""; ValueData: "TheGIMP"; Flags: uninsdeletevalue
;check the [Code] section!!
Root: "HKCR"; Subkey: ".BMP"; ValueType: string; ValueName: ""; ValueData: "TheGIMP"; Flags: uninsdeletevalue; Check: AssocCheck(BMP)
Root: "HKCR"; Subkey: ".JPG"; ValueType: string; ValueName: ""; ValueData: "TheGIMP"; Flags: uninsdeletevalue; Check: AssocCheck(JPG)
Root: "HKCR"; Subkey: ".PNG"; ValueType: string; ValueName: ""; ValueData: "TheGIMP"; Flags: uninsdeletevalue; Check: AssocCheck(PNG)
Root: "HKCR"; Subkey: ".TIF"; ValueType: string; ValueName: ""; ValueData: "TheGIMP"; Flags: uninsdeletevalue; Check: AssocCheck(TIFg)

;path
Root: "HKLM"; SubKey: "Software\Microsoft\Windows\CurrentVersion\App Paths\gimp.exe"; ValueType: string; ValueName: ""; ValueData: "{app}\bin\gimp.exe"
Root: "HKLM"; SubKey: "Software\Microsoft\Windows\CurrentVersion\App Paths\gimp.exe"; ValueType: string; ValueName: "Path"; ValueData: "{app}\lib"

;GTK+ installation path -- not needed anymore
;Root: HKLM; SubKey: Software\GNU\GTk+; ValueType: string; ValueName: InstallationDirectory; ValueData: {code:GetGTKdir|{win}\gtk+}

[Icons]
Name: "{group}\Gimp"; Filename: "{app}\bin\gimp.exe"; WorkingDir: "{app}\lib"
Name: "{group}\Gimp Website"; Filename: "{app}\gimp.url"
Name: "{userdesktop}\Gimp"; Filename: "{app}\bin\gimp.exe"; WorkingDir: "{app}\lib"; MinVersion: 4,4; Tasks: desktopicon

[Run]
;Filename: "{app}\bin\gimp.exe"; Description: "Launch The GIMP"; Flags: postinstall nowait skipifsilent
Filename: "{app}\bin\gimp.exe"; Description: "Launch Gimp"; Flags: postinstall nowait skipifsilent shellexec

[UninstallDelete]
Type: "files"; Name: "{app}\gimp.url"
Type: "files"; Name: "{app}\lib\gimp\1.2\plug-ins\gif.exe"

[Messages]
WelcomeLabel1=Welcome to Gimp Setup (now with JGimp!)

[Code]
program Setup;

  //functions to get BPP & resolution
  function DeleteDC(hDC: Integer): Integer; external 'DeleteDC@GDI32 stdcall';
  function CreateDC(lpDriverName, lpDeviceName, lpOutput: String; lpInitData: Integer): Integer; external 'CreateDCA@GDI32 stdcall';
  function GetDeviceCaps(hDC, nIndex: Integer): Integer; external 'GetDeviceCaps@GDI32 stdcall';

  function GetSysColor(nIndex: Integer): Integer; external 'GetSysColor@user32 stdcall';

  Const
    OldGimpRegPath = 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\WinGimp_is1';
    GTkRegPath = 'SOFTWARE\GTk\1.3';
    RegKey = 'Inno Setup: App Path';

    HORZRES = 8;
    BITSPIXEL = 12;
    PLANES = 14;

    NumAssoc = 3; // number of file associations

  var
    AssocPrompts, AssocValues: TArrayOfString;
    Key: String;
    DataDir: String;
    i: Integer;
    ext: String;
    Warnings: Integer;
    GTkDir: String;


  function InitializeSetup(): Boolean;
  var xres, bpp, pl, tmp: Integer;
      hDC: Integer;
  begin

    If not RegQueryStringValue(HKLM, GTkRegPath, 'Path', GTkDir) then //find GTk+
      RegQueryStringValue(HKCU, GTkRegPath, 'Path', GTkDir);          //install dir

    //File associations
    SetArrayLength(AssocPrompts, NumAssoc+1);
    SetArrayLength(AssocValues, NumAssoc+1);
    //to change number of file associations, set the NumAssoc constant.
    AssocPrompts[0] := 'BMP Files (Old Windows Backgrounds)';
    AssocPrompts[1] := 'JPG Files (Digital Cameras and Scanned Photos)';
    AssocPrompts[2] := 'PNG Files (New Web Graphics)';
    AssocPrompts[3] := 'TIF Files (Old Digital Cameras)';

    // Try to find the settings that were stored last time
    for i := 0 to NumAssoc do
    begin
      ext:='Ext' + IntToStr(i);
      AssocValues[i] := GetPreviousData(ext, AssocValues[i]);
    end;

    //get resolution & BPP
    hDC := CreateDC('DISPLAY', '', '', 0);
    pl := GetDeviceCaps(hDC, PLANES);
    bpp := GetDeviceCaps(hDC, BITSPIXEL);
    xres := GetDeviceCaps(hDC, HORZRES);
    tmp := DeleteDC(hDC);
    tmp := pl * bpp;

    //variable Warnings holds differens errors detected at beginning of installation; if bit 1 is set, the installer
    //won't start

    if tmp<16 then //color depth
      Warnings:=Warnings Or 2;

    if xres<1024 then //resolution
      Warnings:=Warnings Or 4;

    if IsAdminLoggedOn=False then //admin
      Warnings:=Warnings Or 8;


    If not RegValueExists(HKLM, OldGimpRegPath, RegKey) then //previous version
    begin                                                    //of The Gimp (has to
      if RegValueExists(HKCU, OldGimpRegPath, RegKey) then   //be uninstalled first)
      begin
        Warnings:=Warnings Or 32;
        Warnings:=Warnings Or 1;
      end;
    end else
    begin
      Warnings:=Warnings Or 32;
      Warnings:=Warnings Or 1;
    end;

//    Warnings:=16;

    Result := True;
  end;


  procedure RegisterPreviousData(PreviousDataKey: Integer);
  var i: integer;
      CurSubPage: Integer;
      ext: string;
  begin
    //Save informations about selected associations
    for i := 0 to NumAssoc do
    begin
      ext:='Ext' + IntToStr(i);
      SetPreviousData(PreviousDataKey, ext, AssocValues[i]);
    end;
  end;


  (* convert RGB color to BGR HEX value *)
  function RGBBGR(q: Integer): String;
  var a,b,c: String;
      d,e: Integer;
  begin

    while q>0 do
    begin
      e := (q and 15) + 1;
      a := copy('0123456789ABCDEF', e, 1);
      q := q / 16;
      c := a + b;
      b := c;
    end;

    d:=6-Length(b);
    if d>0 then
    begin
      c := StringOfChar('0',d) + b;
      b := c;
    end;

    a := copy(b,1,2);
    c := copy(b,5,2);
    b := copy(b,3,2);

    Result := c + b + a;
  end;


  function ScriptDlgPages(CurPage: Integer; BackClicked: Boolean): Boolean;
  var CurSubPage,i: Integer;
      Next: Boolean;
      Text, GTKRC: String;
      S: String;
      clBtnFace,clBtnShadow,clBtnLight,clText,clGrayText,clHilight,clHilightText,
      clTooltip,clTooltipText: String;
      clHex: String;
  begin

    if (not BackClicked and (CurPage = wpSelectProgramGroup)) or (BackClicked and (CurPage = wpSelectTasks)) then
    begin
      ///////////////////////////
      // File Association page //
      ///////////////////////////
      ScriptDlgPageOpen();

      if not BackClicked then
        CurSubPage := 0
      else
        CurSubPage := 1;

      while (CurSubPage>=0) and (CurSubPage<=0) and not Terminated do
      begin
        case CurSubPage of
          0:
          begin

            if IsAdminLoggedOn=False then //no associations for non-admins :(
            begin

              ScriptDlgPageSetCaption('Select file associations');
              ScriptDlgPageSetSubCaption1('Which file types should be associated with the GIMP?');

              Text := 'You must run this setup as administrator to be able to select file associations.';

              Next := OutputMsg(Text,True);

            end else begin

              ScriptDlgPageSetCaption('Select file associations');
              ScriptDlgPageSetSubCaption1('Which types of files should be opened by The Gimp?');
              ScriptDlgPageSetSubCaption2('');
              Next := InputOptionArray(AssocPrompts, AssocValues, False, True);

            end;

          end;
          1:
          begin

            ScriptDlgPageSetCaption('Notes on supported file formats');
            ScriptDlgPageSetSubCaption1('Please read the following important information before continuing.');

            Text := 'GIF format uses LZW compression, which is covered with the Unisys patent, so The Gimp can only load GIF files.'#13#13;
            //Text := Text+'TIFF format may also use LZW compression. Such files can be loaded by The Gimp, but only uncompressed TIFF files can be saved.'#13#13;
            Text := Text+'If you wish to be able to save GIF files, make sure that either you are in a country where Unisys doesn''t have a patent on the LZW algorithm, or that you have a license from Unisys, and download the LZW library.'#13#13;
            Text := Text+'The Gimp requires GhostScript to be able to read PostScript files. You can download it from <http://ghostscript.sourceforge.net/>'#13#13;

            Next := OutputMsg(Text,True);

          end;
        end;

        if Next then
          CurSubPage := CurSubPage + 1
        else
          CurSubPage := CurSubPage - 1;
      end;

      if not BackClicked then
        Result := Next
      else
        Result := not Next;

      ScriptDlgPageClose(not Result);

(*    end else if (not BackClicked and (CurPage = wpSelectDir)) or (BackClicked and (CurPage = wpSelectComponents)) then begin
      ///////////////////////////
      // select GTK+ directory // (not needed anymore)
      ///////////////////////////
      ScriptDlgPageOpen();
      ScriptDlgPageSetCaption('Select GTK+ Installation directory');
      ScriptDlgPageSetSubCaption1('Where should GTK+ support files be installed?');
      ScriptDlgPageSetSubCaption2('Select the folder you would like Setup to install GTK+ support files to and click Next.');
      // Initialize the GTKdir if necessary
      if GTKdir = '' then
        GTKdir := 'C:\'+ ChangeDirConst('{win}\GTK+');
      // Ask for a dir until the user has entered one or click Back or Cancel
      Next := InputDir('GTK+', GTKdir);
      while Next and (GTKdir = '') do begin
        MsgBox(SetupMessage(msgInvalidPath), mbError, MB_OK);
        Next := InputDir('GTK+', GTKdir);
      end;
      if not BackClicked then
        Result := Next
      else
        Result := not Next;
      ScriptDlgPageClose(not Result); *)
    end else if (not BackClicked and (CurPage = wpWelcome)) or (BackClicked and (CurPage = wpLicense)) then
    begin
      ////////////////////////////////////////
      // Warnings (color depth, resolution) //
      ////////////////////////////////////////
      If Warnings > 0 then begin //display the page only if necessary
        ScriptDlgPageOpen();
        ScriptDlgPageSetCaption('Warnings');
        ScriptDlgPageSetSubCaption1('There may be problems with your current configuration');

        Text := '';
        if (Warnings and 32)=32 then
          Text := Text+'-'#9'The Gimp 1.2.3-20020310 or older was found on your computer. Please uninstall it before installing Gimp 1.2.4.'#13#13;
        if (Warnings and 64)=64 then
          Text := Text+'-'#9'GTK+ 1.3 was installed by Administrator, but you don''t have Administrator privileges. If GTk+ was installed by Administrator, only he can install The Gimp.'#13#13;
        if (Warnings and 16)=16 then
          Text := Text+'-'#9'GTK+ 1.3 was not found on your computer. Please install it before The Gimp.'#13#13;
        If (Warnings and 2)=2 then
          Text := Text+'-'#9'At least 16-bit color mode is recommended to run The Gimp.'#13#13;
        If (Warnings and 4)=4 then
          Text := Text+'-'#9'1024x768 or larger resolution is recommended to run The Gimp.'#13#13;
        if (Warnings and 8)=8 then
          Text := Text+'-'#9'You should run this installation as Administrator, otherwise the installer won''t be able to set the registry entries for library paths and file associations. If The Gimp doesn''t work after the installation is complete, please uninstall it, and run the Setup again as Administrator.';

        if (Warnings and 1)=1 then
          Text := Text+'The Setup cannot continue.';

        if (Warnings and 1)<>1 then
          Next := OutputMsg(Text,True)
        else
        begin
          Next := OutputMsg(Text,False);
          exit;
        end;

        if not BackClicked then
          Result := Next
        else
          Result := not Next;

        ScriptDlgPageClose(not Result);

      end else begin
        Result := True;
      end;

    end else if CurPage = wpFinished then
    ////////////////////////////////
    // save color scheme to gtkrc //
    ////////////////////////////////
    begin

      GTKRC := ExpandConstant('{app}\etc\gimp\1.2\gtkrc_user');
      //GTKRC := GetGTkDir('')+'\etc\gimp\1.2\gtkrc_user';

      if ShouldProcessEntry('base','gtkrc') then
      begin
        if not FileExists(GTKRC) then
          MsgBox('Error: Windows color scheme could not be saved.',0,0)
        else
        begin

          //get the windows' colors
          clBtnFace := RGBBGR(GetSysColor(15));
          clBtnShadow := RGBBGR(GetSysColor(16));
          clBtnLight := RGBBGR(GetSysColor(20));
          clText := RGBBGR(GetSysColor(18));
          clGrayText := RGBBGR(GetSysColor(17));
          clHilight := RGBBGR(GetSysColor(13));
          clHilightText := RGBBGR(GetSysColor(14));
          clTooltip := RGBBGR(GetSysColor(24));
          clTooltipText := RGBBGR(GetSysColor(23));


          //generate the gtkrc file
          S:='style "default"';
          S:=S+#13'{';
          S:=S+#13'  bg[NORMAL] = "#'+clBtnFace+'"';
          S:=S+#13'  bg[PRELIGHT] = "#'+clHilight+'"';
          S:=S+#13'  bg[ACTIVE] = "#'+clBtnFace+'"';
          S:=S+#13'  bg[INSENSITIVE] = "#'+clBtnFace+'"';
          S:=S+#13'  bg[SELECTED] = "#'+clHilight+'"';
          S:=S+#13'';
          S:=S+#13'  fg[NORMAL] = "#'+clText+'"';
          S:=S+#13'  fg[PRELIGHT] = "#'+clHilightText+'"';
          S:=S+#13'  fg[ACTIVE] = "#'+clText+'"';
          S:=S+#13'  fg[INSENSITIVE] = "#'+clGrayText+'"';
          S:=S+#13'  fg[SELECTED] = "#'+clHilightText+'"';
          S:=S+#13'';
          S:=S+#13'  base[PRELIGHT] = "#'+clBtnFace+'"';
          S:=S+#13'  base[INSENSITIVE] = "#'+clBtnFace+'"';
          S:=S+#13'';
          S:=S+#13'}';
          S:=S+#13'widget "*" style "default"';
          S:=S+#13'style "tooltip"';
          S:=S+#13'{';
          S:=S+#13'  bg[NORMAL] = "#'+clTooltip+'"';
          S:=S+#13'  fg[NORMAL] = "#'+clTooltipText+'"';
          S:=S+#13'}';
          S:=S+#13'widget "*gtk-tooltip*" style "tooltip"';
          S:=S+#13'style "button" {';
          S:=S+#13'  bg[NORMAL]    = "#'+clBtnFace+'"';
          S:=S+#13'  bg[PRELIGHT]  = "#'+clBtnFace+'"';
          S:=S+#13'  bg[INSENSITIVE] = "#'+clBtnFace+'"';
          S:=S+#13'  fg[PRELIGHT] = "#'+clHilight+'"';
          S:=S+#13'}';
          S:=S+#13'widget "*Gtk*Button*" style "button"';
          S:=S+#13'';

          if not SaveStringToFile(GTKRC,S,False) then
            MsgBox('Error: could not update color scheme file.',0,0);

        end;
      end;

      Result := True;

    end else begin
      Result := True;
    end;
  end;


  function UpdateReadyMemo(Space, NewLine, MemoUserInfo, MemoDirInfo, MemoTypeInfo, MemoComponentsInfo, MemoGroupInfo, MemoTasksInfo: String): String;
  var S: String;
      ShowAssoc: Boolean;
  begin

    (* Fill the 'Ready Memo' with setup information *)

    S := MemoDirInfo + NewLine + NewLine;                //installation directory

    S := S + MemoTypeInfo + NewLine + NewLine;           //installation type (full/compact/custom)

    S := S + MemoComponentsInfo + NewLine + NewLine;     //selected components

    for i := 0 to NumAssoc do
      if AssocValues[i]='1' then
        ShowAssoc:=True;

    if ShowAssoc then
    begin
      S := S + 'File types to associate with The Gimp:'; //which file types to associate

      for i := 0 to NumAssoc do
        if AssocValues[i]='1' then
          S := S + NewLine + Space + AssocPrompts[i];

      S := S + NewLine + NewLine;
    end;

    S := S + MemoGroupInfo;                              //program group

    If MemoTasksInfo<>'' then
      S := S + NewLine + NewLine + MemoTasksInfo;        //additional icons

    Result := S;
  end;


  function NextButtonClick(CurPage: Integer): Boolean;
  begin
    Result := ScriptDlgPages(CurPage, False);
  end;


  function BackButtonClick(CurPage: Integer): Boolean;
  begin
    Result := ScriptDlgPages(CurPage, True);
  end;


  //GTK+ installation directory
  function GetGTkDir(S: String): String;
  begin
    Result := GTKdir;
  end;


  //Check, if file association should be registered.
  function AssocCheck(assoc: String): Boolean;
  var an: Integer;
  begin

    if IsAdminLoggedOn=false then //don't trigger warning dialogs if admin isn't logged on
      Result:=False
    else
    begin

      for i := 0 to NumAssoc do
      begin
        If Pos(assoc,AssocPrompts[i])>0 then
        begin
          If AssocValues[i]='1' then
            Result := True
          else
            Result := False;
        end;
      end;

    end;

  end;


begin
end.

