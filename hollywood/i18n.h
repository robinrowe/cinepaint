// i18n.h
// Copyright 2023/3/2 Robin.Rowe@CinePaint.org
// License MIT Open Source

#ifndef i18n_h
#define i18n_h

#include <libintl.h>
#include "dll_api.h"

#define PACKAGE "English"

#if 0
DLL_API const char* _(const char* s);
#else
inline
const char* _(const char* s)
{	return s;
}
#endif

#define N_(String) (String)
#define gettext(String)
#define gettext_noop(String) (String)
#define dgettext(Domain,Message) (char *) (Message)
#define dcgettext(Domain,Message,Type) (char *) (Message)
#define bindtextdomain(Domain,Directory) (Domain)
//#    define _(String) (String)
//#    define N_(String) (String)
#define SET_LC_NUMERIC
#define INIT_LOCALE( domain )	

#endif 
