/* hollywood/config.h.  */
/* Robin.Rowe@MovieEditor.com
   This file is in the public domain.

*/

#ifndef config_h
#define config_h

#include <stdlib.h>
#include <stddef.h>

#define RETSIGTYPE void

//#ifdef _WIN32
#if 0
#define HAVE_DIRENT_H
#define HAVE_IPC_H 1
#define HAVE_SYS_TIME_H 1
#define HAVE_UNISTD_H
#define HAVE_VPRINTF 1
#define IPC_RMID_DEFERRED_RELEASE 1
#define TIME_WITH_SYS_TIME 1
#define HAVE_DIRENT_H
#define HAVE_DLFCN_H 1
#define HAVE_INTTYPES_H 1
#define HAVE_LIBTIFF 1
#define HAVE_MEMORY_H 1
#define HAVE_STDINT_H 1
#define HAVE_STDLIB_H 1
#define HAVE_STRINGS_H 1
#define HAVE_STRING_H 1
#define HAVE_SYS_STAT_H 1
#define HAVE_SYS_TYPES_H 1
#define HAVE_SYS_WAIT_H 1
#define HAVE_UNISTD_H
#define HAVE_VPRINTF 1
#define RETSIGTYPE void
#define STDC_HEADERS 1
// #pragma warning( error : 4013)
// #pragma warning( error : 4047) 
       
#endif

#endif