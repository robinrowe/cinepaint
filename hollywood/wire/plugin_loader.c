/* plug-ins/plugin_loader.c
// Loads plug-in dll in same process thread
// Copyright Dec 28, 2002, Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)
*/

#include <stdio.h>
#include <dlfcn.h>
#include "../wire/wire.h"
#include "../wire/wirebuffer.h"
#include "../wire/taskswitch.h"
#include "../wire/protocol.h"
#include "../wire/enums.h"
#include "plugin_loader.h"
#include "../app/app.h"

PluginMain GetPluginMain(PluginLoader* plugin)
{	const char* filename = plugin->internal->filename;//args[0];
	plugin->internal->library = dlopen(filename, RTLD_LAZY);
	if(!plugin->internal->library)
	{	d_printf("ERROR: couldn't dlopen %s\n",filename);
		return 0;
	}
	dlerror();    /* Clear any existing error */
	void* f = dlsym(plugin->internal->library,"plugin_main");
	if(!f)
	{	d_printf("ERROR: couldn't find plugin_main  %s\n", filename);
		return 0;
	}
	plugin->internal->plugin_main=f;
	return f;
}

#if 0
DWORD WINAPI WireMsgHandlerThread(WireBuffer* wire_buffer)
{	WireMessage msg; 
	WireMsgSetZero(msg);
/* Already checked wire_buffer != 0 */
/* ?	plugin_push (plugin);*/
#if 0
	EventWaitFor(wire_buffer->event_wire);
#endif
//	d_puts("<PluginDoWireMsg>");
	for(;;)
	{	if(!wire_read_msg(&msg))
		{	wire_buffer->plugin=0;
			puts("WIRE: WireMsgHandlerThread wire_read_msg failed");
			return 1;
		}
	//	d_puts("WIRE: handle message");
		/* Set breakpoint here to trace into messages received from plug-in */
		plugin_handle_message(&msg);
		TaskSwitchToPlugin();
}	}
#if 0
int WireMsgHandlerRun(WireBuffer* wire_buffer)
{	DWORD thread_id;
	if(!wire_buffer)
	{	d_printf("WireMsgHandlerRun invalid arguments!\n");
		return 0;
	}
	thread_id=0;
	wire_buffer->thread_wire=CreateThread(0,0,WireMsgHandlerThread,wire_buffer,0,&thread_id);
	d_printf("CreatedThread wire #%i\n",(int) wire_buffer->thread_wire);
	return 0!=wire_buffer->thread_wire;
}
#endif
#endif

int LoadPlugIn(PluginLoader* loader)
{	if(!loader || !GetApp()->wire_buffer->buffer)
	{	e_puts("ERROR: PlugInLoader");
		return 0;
	}
	PluginMain plugin_main = GetPluginMain(loader);
	if(!plugin_main)
	{	e_puts("ERROR: plugin_main=<null>");
		return 0;
	}
//	loader->wire_buffer=GetApp()->wire_buffer;
	/* Set breakpoint here to trace into plug-in at program startup */
	if(!plugin_main(loader))
	{	e_puts("ERROR: plugin_main failed");
		return 0;
	} 
	return 1;
}

int PlugInRun(PluginLoader* loader)
{
#if 0
	if(!loader ||!loader->wire_buffer)
	{	d_printf("ERROR: PlugInRun invalid args!\n");
		return 0;
	}
	wire_buffer=loader->wire_buffer;
	if(wire_buffer->thread_plugin)
	{	d_printf("ERROR: Another loader is not done!\n");
		return 0;
	}
#endif
	PluginMain plugin_main = 0;//loader->plugin_main;
	if(!plugin_main)
	{	plugin_main=GetPluginMain(loader);
	}
	if(!plugin_main)
	{	return 0;
	}
	/* Set breakpoint here to trace into plug-in at run-time --rsr */
	if(!plugin_main(loader))/* plugin_main returns errorlevel! */
	{	puts("bad loader run");
		return FALSE;
	}
	return TRUE;
/*	CreateThread(0,0,plugin_main,loader,0,&thread_id);
*/	
}

void GetWireBuffer(WireMessage* msg)
{
	d_puts("GetWireBuffer()");
	msg->msg_type=GP_ERROR;
	if(GetApp()->wire_buffer->buffer_count)
	{	if(!wire_read_msg(msg))
		{	GetApp()->wire_buffer->plugin=0;
			e_puts("wire_read_msg failed");
			msg->msg_type=GP_ERROR;
}	}	}


/*
		if (wire_buffer->current_return_vals && wire_buffer->current_return_nvals == nargs)
		
		if(!wire_read_msg(0,&msg))
		{	puts("ERROR: plug-in recurse 1");
			return 0;
		}
		if(GP_PROC_RETURN!=msg.type)
		{	puts("ERROR: plug-in recurse 2");
			return 0;
		}
	     plugin_set_proc_return (msg.data);
*/
