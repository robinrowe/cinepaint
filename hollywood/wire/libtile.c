/* lib/libtile.c
// Tile and tile cache support in the wire and appcinepaint
// Copyright May 25, 2003, Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)
//
// Similar to functions in app/tile_cache.c.
// Total rewrite by rsr to eliminate tile memory corruption.
*/

#include <string.h>
#include "../pdb//plugin_main.h"
#include "../wire/protocol.h"
#include "../wire/wire.h"
#include "../wire/taskswitch.h"
#include "../wire/iodebug.h"
#include "../wire/wire_types.h"
#include "../app/app.h"
#include "libtile.h"

#ifdef _WIN32
#include "../wire/event.h"
#define _writefd 0
#define _readfd 0
#define _shm_addr 0
#else
  extern int _writefd;
  extern int _readfd;
  extern guchar* _shm_addr;
#endif

#if 1

/*  This is the percentage of the maximum cache size that should be cleared
 *   from the cache when an eviction is necessary
 */
#define FREE_QUANTUM 0.1

static void  wire_tile_get(GTile* tile);
static void  wire_tile_put(GTile* tile);
static void  wire_tile_cache_insert(GTile* tile);
static void  wire_tile_cache_flush(GTile* tile);
static void  wire_tile_cache_zorch(void);
static uintptr_t wire_tile_hash(GTile* tile);

#define _wire_tile_width LIB_TILE_WIDTH
#define _wire_tile_height LIB_TILE_HEIGHT

void
wire_tile_ref(GTile* tile)
{
	if (tile)
	{
		tile->ref_count += 1;

		if (tile->ref_count == 1)
		{
			wire_tile_get(tile);
			tile->dirty = FALSE;
		}

		wire_tile_cache_insert(tile);
	}
}

void
wire_tile_ref_zero(GTile* tile)
{
	if (tile)
	{
		tile->ref_count += 1;

		if (tile->ref_count == 1)
		{
			tile->data = g_new(guchar, tile->ewidth * tile->eheight * tile->bpp);
			memset(tile->data, 0, tile->ewidth * tile->eheight * tile->bpp);
		}

		wire_tile_cache_insert(tile);
	}
}

void
wire_tile_unref(GTile* tile,
	int   dirty)
{
	if (tile)
	{
		tile->ref_count -= 1;
		tile->dirty |= dirty;

		if (tile->ref_count == 0)
		{
			wire_tile_flush(tile);
			g_free(tile->data);
			tile->data = NULL;
		}
	}
}

void
wire_tile_flush(GTile* tile)
{
	if (tile && tile->data && tile->dirty)
	{
		wire_tile_put(tile);
		tile->dirty = FALSE;
	}
}

void wire_tile_unref_free(GTile* tile, int dirty)
{
	if (!tile)
	{
		return;
	}
	d_assert(1 == tile->is_allocated);
	tile->ref_count -= 1;
	tile->dirty |= dirty;
	if (tile->ref_count)
	{
		return;
	}
	wire_tile_flush(tile);
	g_free(tile->data);
	tile->data = NULL;
	d_assert(1 == tile->is_allocated);
}

static void wire_tile_cache_detach(GTile* tile)
{	wire_tile_unref(tile,FALSE);
}

void wire_tile_cache_purge(GTile* tile, gsize ntiles)
{
	int i;
	for (i = 0; i < ntiles; i++)
	{
		wire_tile_cache_detach(tile);
		wire_tile_flush(tile);
		g_free(tile->data);
		tile->data = NULL;
		tile++;
	}
}

void
get_wire_tile_cache_size(gulong kilobytes)
{	App* app = GetApp();
	app->tile_cache.max_cache_size = kilobytes * 1024;
}


void get_wire_tile_cache_ntiles(gulong ntiles)
{
	get_wire_tile_cache_size((ntiles * _wire_tile_width * _wire_tile_height * 4) / 1024);
}

guint get_wire_tile_width(void)
{
	return LIB_TILE_WIDTH;
}

guint get_wire_tile_height(void)
{
	return LIB_TILE_HEIGHT;
}

static void
wire_tile_get(GTile* tile)
{
#if 0
	extern int _writefd;
	extern int _readfd;
	extern guchar* _shm_addr;
#endif
	GPTileReq tile_req;
	GPTileData* tile_data;
	WireMessage msg;
	WireMsgSetZero(msg);

	tile_req.drawable_ID = tile->drawable->id;// Setting to -1 will result in fetch canvas fail.
	tile_req.tile_num = tile->tile_num;
	tile_req.shadow = tile->shadow;
	if (!gp_tile_req_write(&tile_req))
		cine_quit();

	if (!wire_read_msg(&msg))
		cine_quit();

	if (msg.msg_type != GP_TILE_DATA)
	{
		g_message("unexpected message: %d\n", msg.msg_type);
		cine_quit();
	}

	tile_data = msg.msg_data;
	if ((tile_data->drawable_ID != tile->drawable->id) ||
		(tile_data->tile_num != tile->tile_num) ||
		(tile_data->shadow != tile->shadow) ||
		(tile_data->width != tile->ewidth) ||
		(tile_data->height != tile->eheight) ||
		(tile_data->bpp != tile->bpp))
	{
		g_message("received tile info did not match computed tile info\n");
		cine_quit();
	}
#if 0
	if (tile_data->use_shm)
	{
		tile->data = g_new(guchar, tile->ewidth * tile->eheight * tile->bpp);
		memcpy(tile->data, _shm_addr, tile->ewidth * tile->eheight * tile->bpp);
	}
	else
#endif
	{
		tile->data = tile_data->data;
		tile_data->data = NULL;
	}

	if (!gp_tile_ack_write(_writefd))
		cine_quit();

	wire_destroy(&msg);
}

static void
wire_tile_put(GTile* tile)
{
#if 0
	extern int _writefd;
	extern int _readfd;
	extern guchar* _shm_addr;
#endif
	GPTileReq tile_req;
	GPTileData tile_data;
	GPTileData* tile_info;
	WireMessage msg;
	WireMsgSetZero(msg);
	tile_req.drawable_ID = -1;// Not tile->drawable->id
	tile_req.tile_num = 0;
	tile_req.shadow = 0;
	if (!gp_tile_req_write(&tile_req))
		cine_quit();

	if (!wire_read_msg(&msg))
		cine_quit();

	if (msg.msg_type != GP_TILE_DATA)
	{
		g_message("unexpected message: %d\n", msg.msg_type);
		cine_quit();
	}

	tile_info = msg.msg_data;

	tile_data.drawable_ID = tile->drawable->id;
	tile_data.tile_num = tile->tile_num;
	tile_data.shadow = tile->shadow;
	tile_data.bpp = tile->bpp;
	tile_data.width = tile->ewidth;
	tile_data.height = tile->eheight;
	tile_data.data = NULL;
#if 0
	tile_data.use_shm = tile_info->use_shm;

	if (tile_info->use_shm)
		memcpy(_shm_addr, tile->data, tile->ewidth * tile->eheight * tile->bpp);
	else
#endif
		tile_data.data = tile->data;

	if (!gp_tile_data_write(&tile_data))
		cine_quit();

	if (!wire_read_msg(&msg))
		cine_quit();
#if 0
	if (msg.type != GP_TILE_ACK)
	{
		g_message("unexpected message: %d\n", msg.type);
		cine_quit();
	}
	wire_destroy(&msg);
#endif
}

/* This function is nearly identical to the function 'tile_cache_insert'
 *  in the file 'tile_cache.c' which is part of the main cine application.
 */
static void
wire_tile_cache_insert(GTile* tile)
{
	GList* tmp;
	App* app = GetApp();
	if (!app->tile_cache.tile_hash_table)
	{
		app->tile_cache.tile_hash_table = g_hash_table_new((GHashFunc)wire_tile_hash, NULL);
		app->tile_cache.max_tile_size = _wire_tile_width * _wire_tile_height * 4;
	}

	/* First check and see if the tile is already
	 *  in the cache. In that case we will simply place
	 *  it at the end of the tile list to indicate that
	 *  it was the most recently accessed tile.
	 */
	tmp = g_hash_table_lookup(app->tile_cache.tile_hash_table, tile);

	if (tmp)
	{
		/* The tile was already in the cache. Place it at
		 *  the end of the tile list.
		 */
		if (tmp == app->tile_cache.tile_list_tail)
			app->tile_cache.tile_list_tail = app->tile_cache.tile_list_tail->prev;
		app->tile_cache.tile_list_head = g_list_remove_link(app->tile_cache.tile_list_head, tmp);
		if (!app->tile_cache.tile_list_head)
			app->tile_cache.tile_list_tail = NULL;
		g_list_free(tmp);

		/* Remove the old reference to the tiles list node
		 *  in the tile hash table.
		 */
		g_hash_table_remove(app->tile_cache.tile_hash_table, tile);

		app->tile_cache.tile_list_tail = g_list_append(app->tile_cache.tile_list_tail, tile);
		if (!app->tile_cache.tile_list_head)
			app->tile_cache.tile_list_head = app->tile_cache.tile_list_tail;
		app->tile_cache.tile_list_tail = g_list_last(app->tile_cache.tile_list_tail);

		/* Add the tiles list node to the tile hash table. The
		 *  list node is indexed by the tile itself. This makes
		 *  for a quick lookup of which list node the tile is in.
		 */
		g_hash_table_insert(app->tile_cache.tile_hash_table, tile, app->tile_cache.tile_list_tail);
	}
	else
	{
		/* The tile was not in the cache. First check and see
		 *  if there is room in the cache. If not then we'll have
		 *  to make room first. Note: it might be the case that the
		 *  cache is smaller than the size of a tile in which case
		 *  it won't be possible to put it in the cache.
		 */
		if ((app->tile_cache.cur_cache_size + app->tile_cache.max_tile_size) > app->tile_cache.max_cache_size)
		{
			while (app->tile_cache.tile_list_head && (app->tile_cache.cur_cache_size + app->tile_cache.max_cache_size * FREE_QUANTUM) > app->tile_cache.max_cache_size)
				wire_tile_cache_zorch();

			if ((app->tile_cache.cur_cache_size + app->tile_cache.max_tile_size) > app->tile_cache.max_cache_size)
				return;
		}

		/* Place the tile at the end of the tile list.
		 */
		app->tile_cache.tile_list_tail = g_list_append(app->tile_cache.tile_list_tail, tile);
		if (!app->tile_cache.tile_list_head)
			app->tile_cache.tile_list_head = app->tile_cache.tile_list_tail;
		app->tile_cache.tile_list_tail = g_list_last(app->tile_cache.tile_list_tail);

		/* Add the tiles list node to the tile hash table.
		 */
		g_hash_table_insert(app->tile_cache.tile_hash_table, tile, app->tile_cache.tile_list_tail);

		/* Note the increase in the number of bytes the cache
		 *  is referencing.
		 */
		app->tile_cache.cur_cache_size += app->tile_cache.max_tile_size;

		/* Reference the tile so that it won't be returned to
		 *  the main cine application immediately.
		 */
		tile->ref_count += 1;
		if (tile->ref_count == 1)
		{
			wire_tile_get(tile);
			tile->dirty = FALSE;
		}
	}
}

static void
wire_tile_cache_flush(GTile* tile)
{
	GList* tmp;
	App* app = GetApp();
	if (!app->tile_cache.tile_hash_table)
	{
		app->tile_cache.tile_hash_table = g_hash_table_new((GHashFunc)wire_tile_hash, NULL);
		app->tile_cache.max_tile_size = _wire_tile_width * _wire_tile_height * 4;
	}

	/* Find where the tile is in the cache.
	 */
	tmp = g_hash_table_lookup(app->tile_cache.tile_hash_table, tile);

	if (tmp)
	{
		/* If the tile is in the cache, then remove it from the
		 *  tile list.
		 */
		if (tmp == app->tile_cache.tile_list_tail)
			app->tile_cache.tile_list_tail = app->tile_cache.tile_list_tail->prev;
		app->tile_cache.tile_list_head = g_list_remove_link(app->tile_cache.tile_list_head, tmp);
		if (!app->tile_cache.tile_list_head)
			app->tile_cache.tile_list_tail = NULL;


		/* Remove the tile from the tile hash table.
		 */
		g_hash_table_remove(app->tile_cache.tile_hash_table, tile);

		/* Note the decrease in the number of bytes the cache
		 *  is referencing.
		 */
		app->tile_cache.cur_cache_size -= app->tile_cache.max_tile_size;

		/* Unreference the tile.
		 */
		wire_tile_unref(tile, FALSE);
		g_list_free(tmp);
	}
}

static void
wire_tile_cache_zorch()
{
	App* app = GetApp();
	if (app->tile_cache.tile_list_head)
		wire_tile_cache_flush((GTile*)app->tile_cache.tile_list_head->data);
}

static uintptr_t
wire_tile_hash(GTile* tile)
{
	return (uintptr_t)tile;
}


#endif