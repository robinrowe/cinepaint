/* wirebuffer.h
// In-memory buffer used instead of pipe to plug-ins
// Copyright Mar 6, 2003, Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)
*/

#ifndef WIRE_BUFFER_H
#define WIRE_BUFFER_H

#include "wire.h"
#include "../dll_api.h"

/* Tile buffer uses 64*64 tiles:
	8-bit: 12k
	16-bit: 24k */

//#define WIRE_BUFFER_SIZE  2*64*1024
#define WIRE_BUFFER_SIZE 256

struct WireBuffer
{	WireMessage buffer[WIRE_BUFFER_SIZE];
	guint buffer_write_index;
	guint buffer_read_index;
	guint buffer_count;
	struct PluginLoader* plugin;
#if 0
	HANDLE thread_wire;
	HANDLE thread_plugin;
	Event event_wire;
	Event event_plugin;
#endif
	GSList* plugin_internal_list;//PluginInternal
	GSList* plugin_external_list;//PluginInternal
	GSList* load_procs_list;//PluginProc
	GSList* save_procs_list;//PluginProc
	GSList* plugin_proc_list;//PluginProc
//	GSList* plugin_proc;// PluginProc
#ifdef HEAVY
/* old stuff: */
	GSList *open_plugins;//PluginLoader (empty)
	GSList *blocked_plugins;//PlugInBlocked
	GSList *plugin_stack;//PluginLoader
#endif
	struct PluginLoader *current_plugin;
	int current_readfd;
	int current_writefd;
	int current_buffer_index;
	WireMessage *current_buffer;
	Argument *current_return_vals;
	int current_return_nvals;

	ProcData *last_plugin;
#if 0
	int shm_ID;
	guchar *shm_addr;
#endif
	int write_pluginrc;
	struct PluginProc* load_file_proc;
	struct PluginProc* save_file_proc;
	int image_ID;
};

DLL_API int WireBufferOpen();
DLL_API void WireBufferClose();
DLL_API void DumpWireTables();

#endif
