/* wire/taskswitch.c
// Task switches between threads when running plug-ins
// Copyright March 5, 2003, Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)
*/

#include <stdio.h>
#include "wire.h"
#include "taskswitch.h"
#include "wirebuffer.h"
#include "iodebug.h"
#include "../app/app.h"
#include "../pdb/plugin.h"

int PluginDoWireMsg()
{//	puts("PluginDoWireMsg");
	int _readfd = 0;
	WireMessage msg; 
	WireMsgSetZero(msg); 
	if (!wire_read_msg(&msg))
	{
		g_error("ERROR: wire_read_msg failed");
//		cine_quit();
		return FALSE;
	}
	plugin_handle_message(&msg);
	if(msg.msg_data)
	{	
#ifdef _DEBUG
		memset(msg.msg_data,0,msg.msg_size);
#endif
		free(msg.msg_data);
	}
	return TRUE;
}

void TaskSwitchToPlugin()
{
	puts("TaskSwitchToPlugin");
#if 0
	int _readfd = 0;
	WireMessage msg; 
	WireMsgSetZero(msg);
	if (!wire_read_msg(_readfd, &msg))
	{
		g_error("ERROR: wire_read_msg failed");
		//		cine_quit();
		return;
	}
	plugin_handle_message(&msg);
#endif
#if 0
	WireMessage msg; 
	WireMsgSetZero(msg);
	App* app = GetApp();
	if (!wire_read_msg(app->wire_buffer->current_readfd, &msg))
	{
//		g_message(_("plugin_handle_tile_req: ERROR"));
//		ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
		return;
	}
#endif
}

#if 0
void PluginDoWireMsg()
{
#ifdef _WIN32
	if(!GetApp()->wire_buffer->buffer || !GetApp()->wire_buffer->plugin)
	{	e_printf("Thread (GetApp()->wire_buffer->plugin error!");
		return;
	}
//	d_puts("<Sleep Plugin>");
	EventSignal(GetApp()->wire_buffer->event_wire);
	EventWaitFor(GetApp()->wire_buffer->event_plugin);
//	d_puts("<TaskSwitchToPlugin>");
#endif
}

void TaskSwitchToPlugin()
{
#ifdef _WIN32
	if(!GetApp()->wire_buffer->buffer || !GetApp()->wire_buffer->plugin)
	{	e_printf("Thread (GetApp()->wire_buffer->plugin error!");
		return;
	}
//	d_puts("<Sleep Wire>");
	EventSignal(GetApp()->wire_buffer->event_plugin);
	EventWaitFor(GetApp()->wire_buffer->event_wire);
//	d_puts("<PluginDoWireMsg>");
#endif
}
#endif
