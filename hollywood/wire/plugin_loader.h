// plug-ins/plugin_loader.h
// Launches plug-in as a thread
// Copyrith Dec 28, 2002, by Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)

#ifndef PLUGIN_LOADER_H
#define PLUGIN_LOADER_H

#include <unistd.h>
#include "../pdb/plugin.h"
#include "../wire/wire.h"
#include "../wire/c_typedefs.h"
#include "../dll_api.h"

//DWORD WINAPI PlugInRunThread(PluginLoader* plugin);
int LoadPlugIn(PluginLoader* loader);
int PlugInRun(PluginLoader* plugin);
//int WireMsgHandlerRun(WireBuffer* wire_buffer);
void GetWireBuffer(WireMessage* msg);
PluginMain GetPluginMain(PluginLoader* plugin);

typedef int (*Plugin_reader)(int fd, guint8 *buf, gulong count);
typedef int (*PluginMain)(void* data);
//DWORD WINAPI WireMsgHandlerThread(WireBuffer* wire_buffer);


#endif

