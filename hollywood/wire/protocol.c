/* LIBGIMP - CinePaint Library                                                   
 * Copyright (C) 1995-1997 Peter Mattis and Spencer Kimball                
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.             
 *                                                                              
 * This library is distributed in the hope that it will be useful,              
 * but WITHOUT ANY WARRANTY; without even the implied warranty of               
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU            
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */                                                                             

#include "protocol.h"
#include "enums.h"
#include "wire.h"
#include "../pdb/plugin.h"
#include "wire_types.h"

//#define VERBOSE_PROC_INSTALL
//#define VERBOSE_PROC_RUN
//#define VERBOSE_WIRE_HANDLERS

static void _gp_quit_read                (WireMessage *msg);
static void _gp_quit_write               (WireMessage *msg);
static void _gp_quit_destroy             (WireMessage *msg);
static void _gp_config_read              (WireMessage *msg);
static void _gp_config_write             (WireMessage *msg);
static void _gp_config_destroy           (WireMessage *msg);
static void _gp_tile_req_read            (WireMessage *msg);
static void _gp_tile_req_write           (WireMessage *msg);
static void _gp_tile_req_destroy         (WireMessage *msg);
static void _gp_tile_ack_read            (WireMessage *msg);
static void _gp_tile_ack_write           (WireMessage *msg);
static void _gp_tile_ack_destroy         (WireMessage *msg);
static void _gp_tile_data_read           (WireMessage *msg);
static void _gp_tile_data_write          (WireMessage *msg);
static void _gp_tile_data_destroy        (WireMessage *msg);
static void _gp_proc_run_read            (WireMessage *msg);
static void _gp_proc_run_write           (WireMessage *msg);
static void _gp_proc_run_destroy         (WireMessage *msg);
static void _gp_proc_return_read         (WireMessage *msg);
static void _gp_proc_return_write        (WireMessage *msg);
static void _gp_proc_return_destroy      (WireMessage *msg);
static void _gp_temp_proc_run_read       (WireMessage *msg);
static void _gp_temp_proc_run_write      (WireMessage *msg);
static void _gp_temp_proc_run_destroy    (WireMessage *msg);
static void _gp_temp_proc_return_read    (WireMessage *msg);
static void _gp_temp_proc_return_write   (WireMessage *msg);
static void _gp_temp_proc_return_destroy (WireMessage *msg);
static void _gp_proc_install_read        (WireMessage *msg);
static void _gp_proc_install_write       (WireMessage *msg);
static void _gp_proc_install_destroy     (WireMessage *msg);
static void _gp_proc_uninstall_read      (WireMessage *msg);
static void _gp_proc_uninstall_write     (WireMessage *msg);
static void _gp_proc_uninstall_destroy   (WireMessage *msg);
static void _gp_extension_ack_read       (WireMessage *msg);
static void _gp_extension_ack_write      (WireMessage *msg);
static void _gp_extension_ack_destroy    (WireMessage *msg);
static void _gp_params_read              (GParam **params, guint *nparams);
static void _gp_params_write             (GParam *params, int nparams);

#define SAY(x) #x,x

void
gp_init ()
{
  wire_register(SAY(GP_QUIT),
		 _gp_quit_read,
		 _gp_quit_write,
		 _gp_quit_destroy);
  wire_register(SAY(GP_CONFIG),
		 _gp_config_read,
		 _gp_config_write,
		 _gp_config_destroy);
  wire_register(SAY(GP_TILE_REQ),
		 _gp_tile_req_read,
		 _gp_tile_req_write,
		 _gp_tile_req_destroy);
  wire_register(SAY(GP_TILE_ACK),
		 _gp_tile_ack_read,
		 _gp_tile_ack_write,
		 _gp_tile_ack_destroy);
  wire_register(SAY(GP_TILE_DATA),
		 _gp_tile_data_read,
		 _gp_tile_data_write,
		 _gp_tile_data_destroy);
  wire_register(SAY(GP_PROC_RUN),
		 _gp_proc_run_read,
		 _gp_proc_run_write,
		 _gp_proc_run_destroy);
  wire_register(SAY(GP_PROC_RETURN),
		 _gp_proc_return_read,
		 _gp_proc_return_write,
		 _gp_proc_return_destroy);
  wire_register(SAY(GP_TEMP_PROC_RUN),
		 _gp_temp_proc_run_read,
		 _gp_temp_proc_run_write,
		 _gp_temp_proc_run_destroy);
  wire_register(SAY(GP_TEMP_PROC_RETURN),
		 _gp_temp_proc_return_read,
		 _gp_temp_proc_return_write,
		 _gp_temp_proc_return_destroy);
  wire_register(SAY(GP_PROC_INSTALL),
		 _gp_proc_install_read,
		 _gp_proc_install_write,
		 _gp_proc_install_destroy);
  wire_register(SAY(GP_PROC_UNINSTALL),
		 _gp_proc_uninstall_read,
		 _gp_proc_uninstall_write,
		 _gp_proc_uninstall_destroy);
  wire_register(SAY(GP_EXTENSION_ACK),
		 _gp_extension_ack_read,
		 _gp_extension_ack_write,
		 _gp_extension_ack_destroy);
#ifdef VERBOSE_WIRE_HANDLERS
	DumpWire();
#endif
}

int
gp_quit_write ()
{
  WireMessage msg; 
  WireMsgSetZero(msg);
  msg.msg_type = GP_QUIT;
  if (!wire_write_msg (&msg))
    return FALSE;
  if (!wire_flush())
    return FALSE;
  return TRUE;
}

int
gp_config_write (GPConfig *config)
{
  WireMessage msg; 
  WireMsgSet(msg,GP_CONFIG,config);
  if (!wire_write_msg (&msg))
    return FALSE;
  if (!wire_flush())
    return FALSE;
  return TRUE;
}

int
gp_tile_req_write (GPTileReq *tile_req)
{
  WireMessage msg; 
  WireMsgSet(msg,GP_TILE_REQ,tile_req);
  if (!wire_write_msg (&msg))
    return FALSE;
  if (!wire_flush())
    return FALSE;
  return TRUE;
}

int
gp_tile_ack_write ()
{
  WireMessage msg; 
  WireMsgSetZero(msg);
  msg.msg_type = GP_TILE_ACK;
  if (!wire_write_msg (&msg))
    return FALSE;
  if (!wire_flush())
    return FALSE;
  return TRUE;
}

int
gp_tile_data_write (GPTileData *tile_data)
{
  WireMessage msg; 
  WireMsgSet(msg,GP_TILE_DATA,tile_data);
#if 0 
if(!gp_tile_ack_write(fd))
	return FALSE;
#endif
  if (!wire_write_msg (&msg))
    return FALSE;
#if 0
  if (!wire_flush())
    return FALSE;
#endif
  return TRUE;
}

int
gp_proc_run_write (GPProcRun *proc_run, gboolean is_execute)
{
  WireMessage msg; 
  WireMsgSet(msg,GP_PROC_RUN,proc_run);
#ifdef VERBOSE_PROC_RUN
  printf("PROC INSTALL: %s\n", proc_run->name);
#endif
  if (!wire_write_msg (&msg))
    return FALSE;
	if(is_execute)
	{  if (!wire_flush())
		return FALSE;
	}
  return TRUE;
}

int
gp_proc_return_write (
		      GPProcReturn *proc_return)
{
  WireMessage msg; 
  WireMsgSet(msg,GP_PROC_RETURN,proc_return);
  if (!wire_write_msg (&msg))
    return FALSE;
#if 0
  if (!wire_flush())
    return FALSE;
#endif
  plugin_set_proc_return(msg.msg_data);
  return TRUE;
}

int
gp_temp_proc_run_write (GPProcRun *proc_run)
{
  WireMessage msg; 
  WireMsgSet(msg,GP_TEMP_PROC_RUN,proc_run);
  if (!wire_write_msg (&msg))
    return FALSE;
  if (!wire_flush())
    return FALSE;
  return TRUE;
}

int
gp_temp_proc_return_write (
			   GPProcReturn *proc_return)
{
  WireMessage msg; 
  WireMsgSet(msg,GP_TEMP_PROC_RETURN,proc_return);
  if (!wire_write_msg (&msg))
    return FALSE;
  if (!wire_flush())
    return FALSE;
  return TRUE;
}

#if 0
#define STRDUP(var) var = strdup(var)
	
gp_proc_instuall_dup( GPProcInstall *proc_install)
{	STRDUP(proc_install->name);
	STRDUP(proc_install->blurb);
	STRDUP(proc_install->help);
	STRDUP(proc_install->author);
	STRDUP(proc_install->copyright);
	STRDUP(proc_install->date);
	STRDUP(proc_install->menu_path);
	STRDUP(proc_install->image_types);
#if 0
	int       type 
	int       nparams,
	int       nreturn_params,
	GParamDef *params,
	GParamDef *return_params)
#endif
}
#endif

int
gp_proc_install_write (GPProcInstall *proc_install)
{//	gp_proc_instuall_dup(proc_install);
  WireMessage msg; 
  WireMsgSet(msg,GP_PROC_INSTALL,proc_install);
#ifdef VERBOSE_PROC_INSTALL
	printf("PROC INSTALL: %s\n",proc_install->name);
#endif
  if (!wire_write_msg (&msg))
    return FALSE;
  if (!wire_flush())
    return FALSE;
#if 0	
// try this:
  if (!wire_read_msg(&msg))
	  return FALSE;
  plugin_handle_message(&msg);
#endif
  return TRUE;
}

int
gp_proc_uninstall_write (GPProcUninstall *proc_uninstall)
{
  WireMessage msg; 
  WireMsgSet(msg,GP_PROC_UNINSTALL,proc_uninstall);
  if (!wire_write_msg (&msg))
    return FALSE;
  if (!wire_flush())
    return FALSE;
  return TRUE;
}

int
gp_extension_ack_write (int fd)
{
  WireMessage msg; 
  WireMsgSetZero(msg);
  msg.msg_type = GP_EXTENSION_ACK;
  if (!wire_write_msg (&msg))
    return FALSE;
  if (!wire_flush())
    return FALSE;
  return TRUE;
}


static void
_gp_quit_read (WireMessage *msg)
{
}

static void
_gp_quit_write (WireMessage *msg)
{
}

static void
_gp_quit_destroy (WireMessage *msg)
{
}

static void
_gp_config_read (WireMessage *msg)
{
  GPConfig *config;
  config = g_new (GPConfig, 1);
  memset(config, 0, sizeof(*config));
#if 0
  if (!wire_read_int32 (&config->version, 1))
    return;
  if (!wire_read_int32 (&config->tile_width, 1))
    return;
  if (!wire_read_int32 (&config->tile_height, 1))
    return;
  if (!wire_read_int32 ((guint32*) &config->shm_ID, 1))
    return;
  if (!wire_read_double (&config->gamma, 1))
    return;
  if (!wire_read_int8 ((guint8*) &config->install_cmap, 1))
    return;
  if (!wire_read_int8 ((guint8*) config->color_cube, 3))
    return;
#endif
  msg->msg_data = config;
}

static void
_gp_config_write (WireMessage *msg)
{
  GPConfig *config;

  config = msg->msg_data;
#if 0
  if (!wire_write_int32 (&config->version, 1))
    return;
  if (!wire_write_int32 (&config->tile_width, 1))
    return;
  if (!wire_write_int32 (&config->tile_height, 1))
    return;
  if (!wire_write_int32 ((guint32*) &config->shm_ID, 1))
    return;
  if (!wire_write_double (&config->gamma, 1))
    return;
  if (!wire_write_int8 ((guint8*) &config->install_cmap, 1))
    return;
  if (!wire_write_int8 ((guint8*) config->color_cube, 3))
    return;
#endif
}


static void
_gp_config_destroy (WireMessage *msg)
{
  g_free (msg->msg_data);
}

static void
_gp_tile_req_read (WireMessage *msg)
{
  GPTileReq *tile_req;

  tile_req = g_new (GPTileReq, 1);
#if 0
  if (!wire_read_int32 ((guint32*) &tile_req->drawable_ID, 1))
    return;
  if (!wire_read_int32 (&tile_req->tile_num, 1))
    return;
  if (!wire_read_int32 (&tile_req->shadow, 1))
    return;
#endif
  msg->msg_data = tile_req;
}

static void
_gp_tile_req_write (WireMessage *msg)
{
  GPTileReq *tile_req;

  tile_req = msg->msg_data;
#if 0
  if (!wire_write_int32 ((guint32*) &tile_req->drawable_ID, 1))
    return;
  if (!wire_write_int32 (&tile_req->tile_num, 1))
    return;
  if (!wire_write_int32 (&tile_req->shadow, 1))
    return;
#endif
}

static void
_gp_tile_req_destroy (WireMessage *msg)
{
  g_free (msg->msg_data);
}

static void
_gp_tile_ack_read(WireMessage *msg)
{
}

static void
_gp_tile_ack_write (WireMessage *msg)
{
}

static void
_gp_tile_ack_destroy (WireMessage *msg)
{
}

static void
_gp_tile_data_read (WireMessage *msg)
{
  GPTileData *tile_data;
 // guint length;

  tile_data = g_new (GPTileData, 1);
#if 0
  if (!wire_read_int32 ((guint32*) &tile_data->drawable_ID, 1))
    return;
  if (!wire_read_int32 (&tile_data->tile_num, 1))
    return;
  if (!wire_read_int32 (&tile_data->shadow, 1))
    return;
  if (!wire_read_int32 (&tile_data->bpp, 1))
    return;
  if (!wire_read_int32 (&tile_data->width, 1))
    return;
  if (!wire_read_int32 (&tile_data->height, 1))
    return;
#ifdef BUILD_SHM
  if (!wire_read_int32 (&tile_data->use_shm, 1))
    return;
#endif
#endif
#if 0
  tile_data->data = NULL;
#ifdef BUILD_SHM
  if (!tile_data->use_shm)
#endif
    {
      length = tile_data->width * tile_data->height * tile_data->bpp;
      tile_data->data = g_new (guchar, length);
      if (!wire_read_int8 ((guint8*) tile_data->data, length))
	return;
    }
#endif
#if 0
#ifdef _WIN32
	uintptr_t p = 0;
  if (!wire_read_int(&p,1))// ok for 64-bit?
	return;
  tile_data->data = (guint8*) p;
  msg->msg_data = tile_data;
#else
	#error
#endif
#endif
}

static void
_gp_tile_data_write (WireMessage *msg)
{
  GPTileData *tile_data;
//  guint length;

  tile_data = msg->msg_data;
#if 0
  if (!wire_write_int32 ((guint32*) &tile_data->drawable_ID, 1))
    return;
  if (!wire_write_int32 (&tile_data->tile_num, 1))
    return;
  if (!wire_write_int32 (&tile_data->shadow, 1))
    return;
  if (!wire_write_int32 (&tile_data->bpp, 1))
    return;
  if (!wire_write_int32 (&tile_data->width, 1))
    return;
  if (!wire_write_int32 (&tile_data->height, 1))
    return;
#if 0
#ifdef BUILD_SHM
  if (!wire_write_int32 (&tile_data->use_shm, 1))
    return;

  if (!tile_data->use_shm)
#endif
    {
      length = tile_data->width * tile_data->height * tile_data->bpp;
      if (!wire_write_int8 ((guint8*) tile_data->data, length))
	return;
    }
#endif
#ifdef _WIN32
  intptr_t p = (intptr_t) tile_data->data;
  if (!wire_write_int(&p, 1))//ok for 64-bit?
	  return;
#else
	#error
#endif	
#endif
}

static void
_gp_tile_data_destroy (WireMessage *msg)
{
  GPTileData *tile_data;

  tile_data = msg->msg_data;
  if (tile_data->data)
    g_free (tile_data->data);
  g_free (tile_data);
}

static void
_gp_proc_run_read (WireMessage *msg)
{
  GPProcRun *proc_run;

  proc_run = g_new (GPProcRun, 1);
#if 0
  if (!wire_read_string (&proc_run->name, 1))
    {
      g_free (proc_run);
      return;
    }
  _gp_params_read (&proc_run->params, (guint*) &proc_run->nparams);
#endif
  msg->msg_data = proc_run;
}

static void
_gp_proc_run_write (WireMessage *msg)
{
  GPProcRun *proc_run;

  proc_run = msg->msg_data;
#if 0
  if (!wire_write_string (&proc_run->name, 1))
    return;
#endif
  _gp_params_write (proc_run->params, proc_run->nparams);
}

static void
_gp_proc_run_destroy (WireMessage *msg)
{
  GPProcRun *proc_run;

  proc_run = msg->msg_data;
  _gp_params_destroy (proc_run->params, proc_run->nparams);
  g_free (proc_run->name);
  g_free (proc_run);
}

static void
_gp_proc_return_read (WireMessage *msg)
{
  GPProcReturn *proc_return;

  proc_return = g_new (GPProcReturn, 1);
#if 0
  if (!wire_read_string (&proc_return->name, 1))
    return;
#endif
  _gp_params_read (&proc_return->params, (guint*) &proc_return->nparams);

  msg->msg_data = proc_return;
}

static void
_gp_proc_return_write (WireMessage *msg)
{
  GPProcReturn *proc_return;

  proc_return = msg->msg_data;
#if 0
  if (!wire_write_string (&proc_return->name, 1))
    return;
#endif
  _gp_params_write (proc_return->params, proc_return->nparams);
}

static void
_gp_proc_return_destroy (WireMessage *msg)
{
  GPProcReturn *proc_return;

  proc_return = msg->msg_data;
  _gp_params_destroy (proc_return->params, proc_return->nparams);
  g_free (proc_return);
}

static void
_gp_temp_proc_run_read (WireMessage *msg)
{
  _gp_proc_run_read (msg);
}

static void
_gp_temp_proc_run_write (WireMessage *msg)
{
  _gp_proc_run_write (msg);
}

static void
_gp_temp_proc_run_destroy (WireMessage *msg)
{
  _gp_proc_run_destroy (msg);
}

static void
_gp_temp_proc_return_read (WireMessage *msg)
{
  _gp_proc_return_read (msg);
}

static void
_gp_temp_proc_return_write (WireMessage *msg)
{
  _gp_proc_return_write (msg);
}

static void
_gp_temp_proc_return_destroy (WireMessage *msg)
{
  _gp_proc_return_destroy (msg);
}

static void
_gp_proc_install_read (WireMessage *msg)
{
  GPProcInstall *proc_install;
//  unsigned i;

  proc_install = g_new (GPProcInstall, 1);
#if 0
  if (!wire_read_string (&proc_install->name, 1))
    return;
  if (!wire_read_string (&proc_install->blurb, 1))
    return;
  if (!wire_read_string (&proc_install->help, 1))
    return;
  if (!wire_read_string (&proc_install->author, 1))
    return;
  if (!wire_read_string (&proc_install->copyright, 1))
    return;
  if (!wire_read_string (&proc_install->date, 1))
    return;
  if (!wire_read_string (&proc_install->menu_path, 1))
    return;
  if (!wire_read_string (&proc_install->image_types, 1))
    return;

  if (!wire_read_int32 (&proc_install->type, 1))
    return;
  if (!wire_read_int32 (&proc_install->nparams, 1))
    return;
  if (!wire_read_int32 (&proc_install->nreturn_vals, 1))
    return;

  proc_install->params = g_new (GPParamDef, proc_install->nparams);
  proc_install->return_vals = g_new (GPParamDef, proc_install->nreturn_vals);

  for (i = 0; i < proc_install->nparams; i++)
    {
      if (!wire_read_int32 ((guint32*) &proc_install->params[i].type, 1))
	return;
      if (!wire_read_string (&proc_install->params[i].name, 1))
	return;
      if (!wire_read_string (&proc_install->params[i].description, 1))
	return;
    }

  for (i = 0; i < proc_install->nreturn_vals; i++)
    {
      if (!wire_read_int32 ((guint32*) &proc_install->return_vals[i].type, 1))
	return;
      if (!wire_read_string (&proc_install->return_vals[i].name, 1))
	return;
      if (!wire_read_string (&proc_install->return_vals[i].description, 1))
	return;
    }
#endif
  msg->msg_data = proc_install;
}

static void
_gp_proc_install_write (WireMessage *msg)
{
  GPProcInstall *proc_install;
//  unsigned i;

  proc_install = msg->msg_data;
#if 0
  if (!wire_write_string (&proc_install->name, 1))
    return;
  if (!wire_write_string (&proc_install->blurb, 1))
    return;
  if (!wire_write_string (&proc_install->help, 1))
    return;
  if (!wire_write_string (&proc_install->author, 1))
    return;
  if (!wire_write_string (&proc_install->copyright, 1))
    return;
  if (!wire_write_string (&proc_install->date, 1))
    return;
  if (!wire_write_string (&proc_install->menu_path, 1))
    return;
  if (!wire_write_string (&proc_install->image_types, 1))
    return;

  if (!wire_write_int32 (&proc_install->type, 1))
    return;
  if (!wire_write_int32 (&proc_install->nparams, 1))
    return;
  if (!wire_write_int32 (&proc_install->nreturn_vals, 1))
    return;

  for (i = 0; i < proc_install->nparams; i++)
    {
      if (!wire_write_int32 ((guint32*) &proc_install->params[i].type, 1))
	return;
      if (!wire_write_string (&proc_install->params[i].name, 1))
	return;
      if (!wire_write_string (&proc_install->params[i].description, 1))
	return;
    }

  for (i = 0; i < proc_install->nreturn_vals; i++)
    {
      if (!wire_write_int32 ((guint32*) &proc_install->return_vals[i].type, 1))
	return;
      if (!wire_write_string (&proc_install->return_vals[i].name, 1))
	return;
      if (!wire_write_string (&proc_install->return_vals[i].description, 1))
	return;
    }
#endif
}

static void
_gp_proc_install_destroy (WireMessage *msg)
{
  GPProcInstall *proc_install;
  guint32 i;

  proc_install = msg->msg_data;

  g_free (proc_install->name);
  g_free (proc_install->blurb);
  g_free (proc_install->help);
  g_free (proc_install->author);
  g_free (proc_install->copyright);
  g_free (proc_install->date);
  g_free (proc_install->menu_path);
  g_free (proc_install->image_types);

  for (i = 0; i < proc_install->nparams; i++)
    {
      g_free (proc_install->params[i].name);
      g_free (proc_install->params[i].description);
    }

  for (i = 0; i < proc_install->nreturn_params; i++)
    {
      g_free (proc_install->return_params[i].name);
      g_free (proc_install->return_params[i].description);
    }

  g_free (proc_install->params);
  g_free (proc_install->return_params);
  g_free (proc_install);
}

static void
_gp_proc_uninstall_read (WireMessage *msg)
{
  GPProcUninstall *proc_uninstall;

  proc_uninstall = g_new (GPProcUninstall, 1);
#if 0
  if (!wire_read_string (&proc_uninstall->name, 1))
    return;
#endif
  msg->msg_data = proc_uninstall;
}

static void
_gp_proc_uninstall_write (WireMessage *msg)
{
  GPProcUninstall *proc_uninstall;

  proc_uninstall = msg->msg_data;
#if 0
  if (!wire_write_string (&proc_uninstall->name, 1))
    return;
#endif
}

static void
_gp_proc_uninstall_destroy (WireMessage *msg)
{
  GPProcUninstall *proc_uninstall;

  proc_uninstall = msg->msg_data;

  g_free (proc_uninstall->name);
  g_free (proc_uninstall);
}

static void
_gp_params_read (GParam **params, guint *nparams)
{
//  guint i;
#if 0
  if (!wire_read_int32 ((guint32*) nparams, 1))
    return;

  if (*nparams == 0)
    {
      *params = NULL;
      return;
    }

  *params = g_new (GPParam, *nparams);

  for (i = 0; i < *nparams; i++)
    {
      if (!wire_read_int32 ((guint32*) &(*params)[i].type, 1))
	return;

      switch ((*params)[i].type)
	{
	case PARAM_INT32:
	  if (!wire_read_int32 ((guint32*) &(*params)[i].data.d_int32, 1))
	    return;
	  break;
	case PARAM_INT16:
	  if (!wire_read_int16 ((guint16*) &(*params)[i].data.d_int16, 1))
	    return;
	  break;
	case PARAM_INT8:
	  if (!wire_read_int8 ((guint8*) &(*params)[i].data.d_int8, 1))
	    return;
	  break;
        case PARAM_FLOAT:
	  if (!wire_read_double (&(*params)[i].data.d_float, 1))
	    return;
          break;
        case PARAM_STRING:
	  if (!wire_read_string (&(*params)[i].data.d_string, 1))
	    return;
          break;
        case PARAM_INT32ARRAY:
	  (*params)[i].data.d_int32array = g_new (gint32, (*params)[i-1].data.d_int32);
	  if (!wire_read_int32 ((guint32*) (*params)[i].data.d_int32array,
				(*params)[i-1].data.d_int32))
	    return;
          break;
        case PARAM_INT16ARRAY:
	  (*params)[i].data.d_int16array = g_new (gint16, (*params)[i-1].data.d_int32);
	  if (!wire_read_int16 ((guint16*) (*params)[i].data.d_int16array,
				(*params)[i-1].data.d_int32))
	    return;
          break;
        case PARAM_INT8ARRAY:
	  (*params)[i].data.d_int8array = g_new (gint8, (*params)[i-1].data.d_int32);
	  if (!wire_read_int8 ((guint8*) (*params)[i].data.d_int8array,
			       (*params)[i-1].data.d_int32))
	    return;
          break;
        case PARAM_FLOATARRAY:
	  (*params)[i].data.d_floatarray = g_new (gdouble, (*params)[i-1].data.d_int32);
	  if (!wire_read_double ((*params)[i].data.d_floatarray,
				 (*params)[i-1].data.d_int32))
	    return;
          break;
        case PARAM_STRINGARRAY:
	  (*params)[i].data.d_stringarray = g_new (gchar*, (*params)[i-1].data.d_int32);
	  if (!wire_read_string ((*params)[i].data.d_stringarray,
				 (*params)[i-1].data.d_int32))
	    return;
          break;
        case PARAM_COLOR:
	  if (!wire_read_int8 ((guint8*) &(*params)[i].data.d_color.red, 1))
	    return;
	  if (!wire_read_int8 ((guint8*) &(*params)[i].data.d_color.green, 1))
	    return;
	  if (!wire_read_int8 ((guint8*) &(*params)[i].data.d_color.blue, 1))
	    return;
          break;
        case PARAM_REGION:
          break;
        case PARAM_DISPLAY:
	  if (!wire_read_int32 ((guint32*) &(*params)[i].data.d_display, 1))
	    return;
          break;
        case PARAM_IMAGE:
	  if (!wire_read_int32 ((guint32*) &(*params)[i].data.d_image, 1))
	    return;
          break;
        case PARAM_LAYER:
	  if (!wire_read_int32 ((guint32*) &(*params)[i].data.d_layer, 1))
	    return;
          break;
        case PARAM_CHANNEL:
	  if (!wire_read_int32 ((guint32*) &(*params)[i].data.d_channel, 1))
	    return;
          break;
        case PARAM_DRAWABLE:
	  if (!wire_read_int32 ((guint32*) &(*params)[i].data.d_drawable, 1))
	    return;
          break;
        case PARAM_SELECTION:
	  if (!wire_read_int32 ((guint32*) &(*params)[i].data.d_selection, 1))
	    return;
          break;
        case PARAM_BOUNDARY:
	  if (!wire_read_int32 ((guint32*) &(*params)[i].data.d_boundary, 1))
	    return;
          break;
        case PARAM_PATH:
	  if (!wire_read_int32 ((guint32*) &(*params)[i].data.d_path, 1))
	    return;
          break;
        case PARAM_STATUS:
	  if (!wire_read_int32 ((guint32*) &(*params)[i].data.d_status, 1))
	    return;
          break;
	case PARAM_END:
	  break;
	}
    }
#endif
}

static void
_gp_params_write (GParam *params, int nparams)
{
//  int i;
#if 0
  if (!wire_write_int32 ((guint32*) &nparams, 1))
    return;

  for (i = 0; i < nparams; i++)
    {
      if (!wire_write_int32 ((guint32*) &params[i].type, 1))
	return;

      switch (params[i].type)
	{
	case PARAM_INT32:
	  if (!wire_write_int32 ((guint32*) &params[i].data.d_int32, 1))
	    return;
	  break;
	case PARAM_INT16:
	  if (!wire_write_int16 ((guint16*) &params[i].data.d_int16, 1))
	    return;
	  break;
	case PARAM_INT8:
	  if (!wire_write_int8 ((guint8*) &params[i].data.d_int8, 1))
	    return;
	  break;
        case PARAM_FLOAT:
	  if (!wire_write_double (&params[i].data.d_float, 1))
	    return;
          break;
        case PARAM_STRING:
	  if (!wire_write_string (&params[i].data.d_string, 1))
	    return;
          break;
        case PARAM_INT32ARRAY:
	  if (!wire_write_int32 ((guint32*) params[i].data.d_int32array,
				 params[i-1].data.d_int32))
	    return;
          break;
        case PARAM_INT16ARRAY:
	  if (!wire_write_int16 ((guint16*) params[i].data.d_int16array,
				 params[i-1].data.d_int32))
	    return;
          break;
        case PARAM_INT8ARRAY:
	  if (!wire_write_int8 ((guint8*) params[i].data.d_int8array,
				params[i-1].data.d_int32))
	    return;
          break;
        case PARAM_FLOATARRAY:
	  if (!wire_write_double (params[i].data.d_floatarray,
				  params[i-1].data.d_int32))
	    return;
          break;
        case PARAM_STRINGARRAY:
	  if (!wire_write_string (params[i].data.d_stringarray,
				  params[i-1].data.d_int32))
	    return;
          break;
        case PARAM_COLOR:
	  if (!wire_write_int8 ((guint8*) &params[i].data.d_color.red, 1))
	    return;
	  if (!wire_write_int8 ((guint8*) &params[i].data.d_color.green, 1))
	    return;
	  if (!wire_write_int8 ((guint8*) &params[i].data.d_color.blue, 1))
	    return;
          break;
        case PARAM_REGION:
          break;
        case PARAM_DISPLAY:
	  if (!wire_write_int32 ((guint32*) &params[i].data.d_display, 1))
	    return;
          break;
        case PARAM_IMAGE:
	  if (!wire_write_int32 ((guint32*) &params[i].data.d_image, 1))
	    return;
          break;
        case PARAM_LAYER:
	  if (!wire_write_int32 ((guint32*) &params[i].data.d_layer, 1))
	    return;
          break;
        case PARAM_CHANNEL:
	  if (!wire_write_int32 ((guint32*) &params[i].data.d_channel, 1))
	    return;
          break;
        case PARAM_DRAWABLE:
	  if (!wire_write_int32 ((guint32*) &params[i].data.d_drawable, 1))
	    return;
          break;
        case PARAM_SELECTION:
	  if (!wire_write_int32 ((guint32*) &params[i].data.d_selection, 1))
	    return;
          break;
        case PARAM_BOUNDARY:
	  if (!wire_write_int32 ((guint32*) &params[i].data.d_boundary, 1))
	    return;
          break;
        case PARAM_PATH:
	  if (!wire_write_int32 ((guint32*) &params[i].data.d_path, 1))
	    return;
          break;
        case PARAM_STATUS:
	  if (!wire_write_int32 ((guint32*) &params[i].data.d_status, 1))
	    return;
          break;
	case PARAM_END:
	  break;
	}
    }
#endif
}

static void
_gp_extension_ack_read (WireMessage *msg)
{
}

static void
_gp_extension_ack_write (WireMessage *msg)
{
}

static void
_gp_extension_ack_destroy (WireMessage *msg)
{
}

void
_gp_params_destroy (GParam *params, int nparams)
{
  int count;
  int i, j;

  for (i = 0; i < nparams; i++)
    {
      switch (params[i].type)
	{
	case PARAM_INT32:
	case PARAM_INT16:
	case PARAM_INT8:
	case PARAM_FLOAT:
	case PARAM_COLOR:
	case PARAM_REGION:
	case PARAM_DISPLAY:
	case PARAM_IMAGE:
	case PARAM_LAYER:
	case PARAM_CHANNEL:
	case PARAM_DRAWABLE:
	case PARAM_SELECTION:
	case PARAM_BOUNDARY:
	case PARAM_PATH:
	case PARAM_STATUS:
	  break;

	case PARAM_STRING:
	  g_free (params[i].data.d_string);
	  break;
	case PARAM_INT32ARRAY:
	  g_free (params[i].data.d_int32array);
	  break;
	case PARAM_INT16ARRAY:
	  g_free (params[i].data.d_int16array);
	  break;
	case PARAM_INT8ARRAY:
	  g_free (params[i].data.d_int8array);
	  break;
	case PARAM_FLOATARRAY:
	  g_free (params[i].data.d_floatarray);
	  break;
	case PARAM_STRINGARRAY:
	  if ((i > 0) && (params[i-1].type == PARAM_INT32))
	    {
	      count = params[i-1].data.d_int32;
	      for (j = 0; j < count; j++)
		g_free (params[i].data.d_stringarray[j]);
	      g_free (params[i].data.d_stringarray);
	    }
	  break;
	case PARAM_END:
	  break;
	}
    }
#ifdef MEMLEAK
  g_free (params);
#endif
}
