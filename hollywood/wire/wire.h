/* LIBGIMP - CinePaint Library                                                   
 * Copyright (C) 1995-1997 Peter Mattis and Spencer Kimball                
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.             
 *                                                                              
 * This library is distributed in the hope that it will be useful,              
 * but WITHOUT ANY WARRANTY; without even the implied warranty of               
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU            
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */                                                                             
#ifndef __WIRE_H__
#define __WIRE_H__

#include <unistd.h>
#include <glib.h>
#include <memory.h>
#ifdef _WIN32
//#include "event.h"
#endif
#include "../dll_api.h"
#include "../pdb/procedural_db.h"
#include "c_typedefs.h"
#include "../platform_id.h"

typedef void (* WireReadFunc)    (WireMessage* msg);
typedef void (* WireWriteFunc)   (WireMessage* msg);
typedef void (* WireDestroyFunc) (WireMessage* msg);
typedef int  (* WireIOFunc)      (WireMessage* msg, unsigned count);
typedef int  (* WireFlushFunc)   ();

DLL_API const char* Get_gp_name(int param);

struct WireMessage
{	guint32 msg_size;
	guint32 msg_type;
	gpointer msg_data;
};

#define WireMsgSet(msg,type,data) \
	msg.msg_type = type; \
	msg.msg_data = data; \
	msg.msg_size = sizeof(*data)

#define WireMsgSetError(msg) \
	msg.msg_type = GP_ERROR; \
	msg.msg_data = 0; \
	msg.msg_size = 0

#define WireMsgSetZero(msg) \
	memset(&msg,0,sizeof(WireMessage))

void DumpWire();
void wire_register    (const char* name, guint32          type,
		        WireReadFunc     read_func,
		        WireWriteFunc    write_func,
		        WireDestroyFunc  destroy_func);
DLL_API void wire_set_reader   (WireIOFunc       read_func);
DLL_API void wire_set_writer   (WireIOFunc       write_func);
DLL_API void wire_set_flusher  (WireFlushFunc    flush_func);
int  wire_read         (
			guint8          *buf,
			gulong           count);
int  wire_write        (
			guint8          *buf,
			gulong           count);
DLL_API int  wire_flush        ();
int  wire_error        (void);
DLL_API void wire_clear_error  (void);
DLL_API int  wire_read_msg     (WireMessage     *msg);


void wire_get_buffer(PluginLoader* plugin);
	 

DLL_API int  wire_write_msg    (
		        WireMessage     *msg);
DLL_API void wire_destroy      (WireMessage     *msg);
#if SIZEOF_INT == 64
#define wire_read_int wire_read_int64
#define wire_write_int wire_write_int64
#else
#define wire_read_int wire_read_int32
#define wire_write_int wire_write_int32
#endif

int  wire_read_int64(
	guint64* data,
	gint             count);
int  wire_read_int32   (
		        guint32         *data,
		        gint             count);

int  wire_read_int16   (
		        guint16         *data,
		        gint             count);
int  wire_read_int8    (
		        guint8          *data,
		        gint             count);
int  wire_read_double  (
		        gdouble         *data,
		        gint             count);
int  wire_read_string  (
			gchar          **data,
			gint             count);
int  wire_write_int64(
	guint64* data,
	gint             count);
int  wire_write_int32  (
		        guint32         *data,
		        gint             count);
int  wire_write_int16  (
		        guint16         *data,
		        gint             count);
int  wire_write_int8   (
		        guint8          *data,
		        gint             count);
int  wire_write_double (
			gdouble         *data,
			gint             count);
int  wire_write_string (
			gchar          **data,
			gint             count);

DLL_API int wire_memory_read(WireMessage* buf,unsigned count);
DLL_API int wire_memory_write(WireMessage* buf,unsigned count);
DLL_API int wire_memory_flush();

#endif /* __WIRE_H__ */
