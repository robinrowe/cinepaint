/* wirebuffer.c
// In-memory buffer used instead of pipe to plug-ins
// Copyright Mar 6, 2003, Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)
*/

#include <stdio.h>
#include "wire.h"
#include "wirebuffer.h"
#include "iodebug.h"
#include "plugin_loader.h"
#include "../app/app.h"

//extern WireBuffer* wire_buffer;

/* These Wire functions have to go here or plugins won't link! */

int WireMsgHandlerRun(WireBuffer* wire_buffer)
{//	DWORD thread_id;
	if(!wire_buffer)
	{	d_printf("WireMsgHandlerRun invalid arguments!\n");
		return 0;
	}
#ifdef HEAVY
	thread_id=0;
	wire_buffer->thread_wire=CreateThread(0,0,WireMsgHandlerThread,wire_buffer,0,&thread_id);
//	d_printf("CreatedThread wire #%i\n",(int) wire_buffer->thread_wire);
	return 0!=wire_buffer->thread_wire;
#else
	return 0;
#endif
}

void WireBufferReset(WireBuffer* wire_buffer)
{	if(!wire_buffer)
	{	return;
	}
	memset(wire_buffer,0,sizeof(WireBuffer));
#ifdef HEAVY
	wire_buffer->thread_wire = 0;
	wire_buffer->thread_plugin = 0;
	wire_buffer->event_wire = INVALID_HANDLE_VALUE;
	wire_buffer->event_plugin = INVALID_HANDLE_VALUE;
	wire_buffer->open_plugins = NULL;
	wire_buffer->blocked_plugins = NULL;
	wire_buffer->plugin_stack = NULL;
	wire_buffer->shm_ID = -1;
#endif
}

int WireBufferOpen()
{
	LogoProgress(_("Wire comms"), _(""));
	App* app = GetApp();
	if(!app->wire_buffer)
	{	app->wire_buffer = (WireBuffer*) malloc(sizeof(WireBuffer));
	}
	if(!app->wire_buffer)
	{	return FALSE;
	}
	WireBufferReset(app->wire_buffer);
#ifdef HEAVY
	app->wire_buffer->event_wire=EventOpen();
	if(INVALID_HANDLE_VALUE==app->wire_buffer->event_wire)
	{	e_puts("ERROR: EventOpen event_wire");
		return FALSE;
	}
	app->wire_buffer->event_plugin=EventOpen();
	if(INVALID_HANDLE_VALUE==app->wire_buffer->event_plugin)
	{	e_puts("ERROR: EventOpen event_plugin");
		return FALSE;
	}
	if(!WireMsgHandlerRun(app->wire_buffer))
	{	e_printf("WireMsgHandlerRun failed!\n");
		return FALSE;
	}
#endif
	return TRUE;
}

void WireBufferClose(WireBuffer* wire_buffer)
{
#ifdef HEAVY
	EventClose(wire_buffer->event_wire);
	EventClose(wire_buffer->event_plugin);
	/* kill thread ... */
#endif
	WireBufferReset(wire_buffer);
}

void DumpPluginList(GSList* plugin_list,const char* msg)
{	puts(msg);
	while (plugin_list)
	{	PluginInternal* plugin = plugin_list->data;
		plugin_list = plugin_list->next;
		const char* plugin_name = plugin->plugin_name;
		if(!plugin_name)
		{	plugin_name = "(empty)";
		}
		printf("  plugin_external_list: %s\n",plugin->plugin_name);
}	}

void DumpPluginProc(PluginProc* procedure, const char* msg)
{
	printf("  procs: %s\n", procedure->plugin_name);
}

void DumpPluginProcs(GSList* procs, const char* msg)
{	puts(msg);
	while (procs)
	{	PluginProc* procedure = procs->data;
		procs = procs->next;
		DumpPluginProc(procedure,msg);
	}
}

void DumpPluginsOpen(GSList* open_plugins, const char* msg)
{	puts(msg);
	while (open_plugins)
	{	PluginLoader* plugin = open_plugins->data;
		open_plugins = open_plugins->next;
		printf("  plugin_external_list: %s\n", plugin->name);
	}
} 
#if 0

void DumpPluginsInternal(GSList* plugin_external_list, const char* msg)
{
	puts("DumpPluginsInternal:");
	while (plugin_external_list)
	{
		GSList* procs = plugin_external_list->data;
		plugin_external_list = plugin_external_list->next;
		DumpPluginProcs(procs);
	}
}
void DumpPluginsBlocked(GSList* blocked_plugins, const char* msg)
{	puts("DumpPluginsBlocked:");
	while (blocked_plugins)
	{	PlugInBlocked* blocked = blocked_plugins->data;
		blocked_plugins = blocked_plugins->next;
		printf("plugin_external_list: %s\n", procedure->name);
	}
}
#endif

void DumpPluginStack(GSList* plugin_stack, const char* msg)
{	puts(msg);
	while (plugin_stack)
	{	PluginInternal* procedure = plugin_stack->data;
		plugin_stack = plugin_stack->next;
		printf("  plugin_external_list: %s\n", procedure->plugin_name);
}	}

#define DUMP(f,data) f(data,"DUMP " #data ":")

void DumpWireTables()
{
	App* app = GetApp();
	DUMP(DumpPluginList,app->wire_buffer->plugin_external_list);
	DUMP(DumpPluginList,app->wire_buffer->plugin_internal_list);
	DUMP(DumpPluginProcs, app->wire_buffer->load_procs_list);
	DUMP(DumpPluginProcs, app->wire_buffer->save_procs_list);
	DUMP(DumpPluginProcs, app->wire_buffer->plugin_proc_list);
#ifdef HEAVY
	DUMP(DumpPluginsOpen,app->wire_buffer->open_plugins);
	DUMP(DumpPluginStack,app->wire_buffer->plugin_stack);
//	GSList* blocked_plugins;//PlugInBlocked
#endif
}

/*
	GSList* plugin_internal_list;//PluginInternal
	GSList* plugin_external_list;//Plugin
	GSList* load_procs_list;//PluginProc
	GSList* save_procs_list;//PluginProc
	GSList* plugin_proc_list;//PluginProc
*/