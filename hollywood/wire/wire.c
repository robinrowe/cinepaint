/* LIBGIMP - CinePaint Library                                                   
 * Copyright (C) 1995-1997 Peter Mattis and Spencer Kimball                
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.             
 *                                                                              
 * This library is distributed in the hope that it will be useful,              
 * but WITHOUT ANY WARRANTY; without even the implied warranty of               
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU            
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */                                                                             
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if 0
#include "event.h"

#include <sys/param.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <poll.h>
#endif

#include <unistd.h>
#include "wire.h"
#include "protocol.h"
#include "wirebuffer.h"
#include "../pdb/plugin.h"
#include "../app/app.h"
#include "iodebug.h"
#include "taskswitch.h"

//#define DEBUG_WIRE
//#define DEBUG_WIRE_HANDLER

#ifdef DEBUG_WIRE
//#define VERBOSE_WRITE_STRING
//#define DEBUG_WIRE_BYTES 
#endif

const char* gp_name[] = {
	"GP_ERROR",
	"GP_QUIT",
	"GP_CONFIG",
	"GP_TILE_REQ",
	"GP_TILE_ACK",
	"GP_TILE_DATA",
	"GP_PROC_RUN",
	"GP_PROC_RETURN",
	"GP_TEMP_PROC_RUN",
	"GP_TEMP_PROC_RETURN",
	"GP_PROC_INSTALL",
	"GP_PROC_UNINSTALL",
	"GP_EXTENSION_ACK"
};

const char* Get_gp_name(int param)
{	if(param<0)
	{	return "Garbage in Param Type";
	}
	if(param>12)
	{	return "Unknown Param Type";
	}
	return gp_name[param];
}

typedef struct WireHandler  WireHandler;

struct WireHandler
{ const char* name;
  guint32 type;
  WireReadFunc read_func;
  WireWriteFunc write_func;
  WireDestroyFunc destroy_func;
};


static void  wire_init    (void);
static guint wire_hash    (guint32 *key);
static gint  wire_compare (guint32 *a,
			   guint32 *b);


static GHashTable *wire_ht = NULL;
static WireIOFunc wire_read_func = NULL;
static WireIOFunc wire_write_func = NULL;
static WireFlushFunc wire_flush_func = NULL;
static int wire_error_val = FALSE;

void
wire_register(const char* name,guint32         type,
	       WireReadFunc    read_func,
	       WireWriteFunc   write_func,
	       WireDestroyFunc destroy_func)
{
  WireHandler *handler;

  if (!wire_ht)
    wire_init ();

  handler = g_hash_table_lookup (wire_ht, &type);
  if (!handler)
    handler = g_new (WireHandler, 1);
  handler->name = name;
  handler->type = type;
  handler->read_func = read_func;
  handler->write_func = write_func;
  handler->destroy_func = destroy_func;

  g_hash_table_insert (wire_ht, &handler->type, handler);
}

void PrintWire(gpointer key,gpointer value,gpointer user_data)
{	WireHandler* handler = (WireHandler*) value;
	printf("Wire type %i: %s\n",handler->type,handler->name);
}

void DumpWire()
{	if(!wire_ht)
	{	puts("Bad wire table");
		return;
	}
	g_hash_table_foreach(wire_ht,PrintWire,0);
}

void wire_set_reader(WireIOFunc read_func)
{	wire_read_func = read_func;
}

void wire_set_writer(WireIOFunc write_func)
{	wire_write_func = write_func;
}

void wire_set_flusher(WireFlushFunc flush_func)
{	wire_flush_func = flush_func;
}

int wire_memory_read(WireMessage* msg,unsigned bytes)
{	App* app = GetApp();
	unsigned buffer_count=app->wire_buffer->buffer_count;
#ifdef DEBUG_WIRE_BYTES
	d_printf("DEBUG_WIRE_BYTES wire_memory_read: %i - %i = %i\n",buffer_bytes,count,buffer_bytes-count);
#endif
	if(!buffer_count)
	{	return FALSE;
	}
	WireMessage* buffer = app->wire_buffer->buffer + app->wire_buffer->buffer_read_index;
	msg = buffer;
	app->wire_buffer->buffer_count--;
	app->wire_buffer->buffer_read_index++;
	if(!app->wire_buffer->buffer_count)
	{	app->wire_buffer->buffer_read_index = 0;
		app->wire_buffer->buffer_write_index = 0;
	}
	return TRUE;
}

int wire_memory_write(WireMessage* msg,unsigned bytes)
{	App* app = GetApp();
	if(!app->wire_buffer->buffer)
	{	e_printf("wire_memory_write: error no app->wire_buffer\n");
		return FALSE;
	}
	unsigned buffer_count=app->wire_buffer->buffer_count;
#ifdef DEBUG_WIRE_BYTES
	d_printf("DEBUG_WIRE_BYTES wire_memory_write: %i + %i = %i (%i max)\n",buffer_bytes,count,buffer_bytes+count,WIRE_BUFFER_SIZE);
#endif
	if((buffer_count + 1) >= WIRE_BUFFER_SIZE)
	{	e_printf("wire_memory_write: error WIRE_BUFFER_SIZE %i\n",buffer_count);
		return FALSE;
	}
	WireMessage* p = app->wire_buffer->buffer + app->wire_buffer->buffer_write_index;
	WireMessage* buffer = app->wire_buffer->buffer + app->wire_buffer->buffer_write_index;
	*buffer = *msg;
	app->wire_buffer->buffer_write_index++;
	app->wire_buffer->buffer_count++;
	return TRUE;
}

int wire_memory_flush(int fd)
{
#if 0
	App* app = GetApp();
	if (!app->wire_buffer || app->wire_buffer->buffer_count <= 0)
	{	return TRUE;
	}
//	wire_memory_write(app->wire_buffer->buffer, app->wire_buffer->buffer_count);
#endif
	return PluginDoWireMsg();
//	app->wire_buffer->buffer_count = 0;// Don't, would purge all
//	return TRUE;
}

#if 0

int
wire_read (guint8 *buf, gulong count) 
{
	if(!wire_read_func)
	{	return FALSE;
	}
	if(wire_read_func(buf, count))
	{	return TRUE;
	}
	g_print ("wire_read: error\n");
	wire_error_val = TRUE;
	return FALSE;
}

int wire_write_ptr(guint8* buf)
{	if(!wire_write_func)
	{	return FALSE;
	}
	gulong ptr_size = sizeof(buf);
	if (wire_write_func((guint8*) &buf, ptr_size))
	{	return TRUE;
	}
	g_print ("wire_write: write error 1\n");
	wire_error_val = TRUE;
	return FALSE;
}

int
wire_write (
	    guint8 *buf,
	    gulong  count)
{
	if(!wire_write_func)
	{	return FALSE;
	}
	/* calls cine_write */
	if (wire_write_func(buf, count))
	{	return TRUE;
	}
	g_print ("wire_write: write error 1\n");
	wire_error_val = TRUE;
	return FALSE;
}
#endif

int wire_flush()
{/* calls cine_flush */
  if (wire_flush_func)
    return (* wire_flush_func)();
  return FALSE;
}


int
wire_error ()
{
  return wire_error_val;
}

void
wire_clear_error ()
{
  wire_error_val = FALSE;
}

WireHandler* GetHandler(const int* type,const char* msg)
{
	WireHandler* handler = g_hash_table_lookup(wire_ht, type);
	if (!handler)
	{
		g_error("could not find handler %s for message: %s\n", msg, Get_gp_name(*type));
		return 0;
	}
#ifdef DEBUG_WIRE_HANDLER
	d_printf("wire handler %s %s\n",msg, handler->name);
#endif
	return handler;
}

void PrintWireMessage(WireMessage* msg)
{	WireHandler *handler = GetHandler(&msg->msg_type,"<<");
	const char* name = "Unknown";
	if(handler)
	{	name = handler->name;
	}
	printf("*WireCheck msg: %s type = %u, size = %u, %p\n",name, msg->msg_type,msg->msg_size,msg->msg_data);
}

void PrintWireCheck()
{	App* app = GetApp();
	unsigned buffer_count=app->wire_buffer->buffer_count;
	WireMessage* msg = app->wire_buffer->buffer + app->wire_buffer->buffer_read_index;
	PrintWireMessage(msg);
}

#ifdef DEBUG_WIRE
#define PRINT_PTR(msg) PrintWireMessage(msg)
#else
#define PRINT_PTR(msg) 
#endif

void* payload_dup(void* msg_data,unsigned payload_size)
{	if(!msg_data)
	{	return 0;
	}
	void* payload = malloc(payload_size);
	if(!payload)
	{	return 0;
	}
	memcpy(payload,msg_data,payload_size);
//	PRINT_PTR(payload);
	return payload;
}

int wire_write_msg (WireMessage *msg)
{	const int max_msg_size = 1000;
	if(!msg || msg->msg_size > max_msg_size || wire_error_val)
	{	return !wire_error_val;
	}
#if 1
	if(msg->msg_size) // put on heap, like strdup("hello");
	{	msg->msg_data = payload_dup(msg->msg_data,msg->msg_size);
		if(!msg->msg_data)
		{	return FALSE;
	}	}
#endif
#ifdef DEBUG_WIRE
	d_printf("wire_write_msg: type %i %s\n",msg->type, Get_gp_name(msg->type));
#endif
	if(!wire_write_func)
	{	return FALSE;
	}
	PRINT_PTR(msg);
	gulong payload_size = sizeof(WireMessage);
	if (wire_write_func(msg,payload_size))
	{	return TRUE;
	}
	g_print ("wire_write: write error 1\n");
	wire_error_val = TRUE;
	return FALSE;
}

#if 0
guint8* wire_read_ptr()
{	const int ptr_size = sizeof(ptr);
	App* app = GetApp();
//	guint buffer_bytes=app->wire_buffer->buffer_count;
	ptr = app->wire_buffer->buffer + app->wire_buffer->buffer_read_index;
	app->wire_buffer->buffer_count -= ptr_size;
	app->wire_buffer->buffer_read_index += ptr_size;
	if(!app->wire_buffer->buffer_count)
	{	app->wire_buffer->buffer_read_index = 0;
		app->wire_buffer->buffer_write_index = 0;
	}
	PRINT_PTR(ptr);
	return ptr;
}
#endif

int wire_read_msg (WireMessage *msg)
{	if(!msg || wire_error_val)
	{	return !wire_error_val;
	}
	App* app = GetApp();
	if(!app->wire_buffer->buffer_count)
	{	puts("bad wire_read_msg: empty");
		wire_error_val = TRUE;
		return FALSE;
	}
	WireMessage* buffer = app->wire_buffer->buffer + app->wire_buffer->buffer_read_index;
	*msg = *buffer;
	app->wire_buffer->buffer_count--;
	app->wire_buffer->buffer_read_index++;
	if(!app->wire_buffer->buffer_count)
	{	app->wire_buffer->buffer_read_index = 0;
		app->wire_buffer->buffer_write_index = 0;
	}
	return TRUE;
}
#if 0
  WireHandler *handler = GetHandler(&msg->msg_type,"<<");
  if (!handler)
  {  return FALSE;
	}
  (* handler->read_func) (fd, msg);// Sets msg->msg_data = payload.,.
#endif
#ifdef DEBUG_WIRE
	d_printf("wire_read_msg[%i]: type %i %s\n",app->wire_buffer->buffer_count,msg->type,Get_gp_name(msg->type));
#endif
#if 0
	switch(msg->type)
	{
		default:
			d_printf("<GP_DEFAULT (%i)\n", app->wire_buffer->buffer_count);
			break;
		case GP_PROC_RETURN:
		{	GPProcReturn *proc=(GPProcReturn*) msg->data;
			d_printf("<GP_PROC_RETURN: %s (%i)\n",proc->name, app->wire_buffer->buffer_count);
			break;
		}
		case GP_TILE_DATA:
		{	d_printf("<GP_TILE_DATA (%i)\n", app->wire_buffer->buffer_count);
/*	unsigned i;
			unsigned long* ptr;
			GPTileData* tile=(GPTileData*) msg->data;
			d_printf("\nTILE_DATA: ");
			d_printf("id=%i,tile_num=%i,width=%i,height=%i,bpp=%i
 tile->drawable->id) ||
  tile->tile_num) ||
 tile->shadow) ||
tile->width) ||
tile->height) ||
tile->bpp))*/
/*			ptr=(unsigned long*) tile->data;
			for(i=0;i<3*8;i++)
			{	d_printf("%x ",ptr[i]);
			}
*/		
			break;
		}
		case GP_PROC_INSTALL:
		{	GPProcInstall* proc = (GPProcInstall*)msg->data;
			d_printf("<GP_PROC_INSTALL: %s (%i)\n", proc->name, app->wire_buffer->buffer_count);
			break;
		}
		case GP_PROC_RUN:
		{	GPProcRun* proc = (GPProcRun*)msg->data;
			d_printf("<GP_PROC_RUN: %s (%i)\n", proc->name, app->wire_buffer->buffer_count);
			break;
		}
	}
	d_puts("");
#endif


void
wire_destroy (WireMessage *msg)
{	if(!msg || !msg->msg_type)
	{	return;
	}
	WireHandler *handler;
	handler = g_hash_table_lookup (wire_ht, &msg->msg_type);
	if (!handler)
	{	g_error ("could not find handler for message: %d\n", msg->msg_type);
		return;
	}
	(* handler->destroy_func) (msg);
}
#if 0
int
wire_read_int64(
	guint64* data,
	gint     count)
{
	if (count > 0)
	{
		if (!wire_read_int8((guint8*)data, count * sizeof(guint64)))
			return FALSE;

		while (count--)
		{
			*data = ntohl(*data);
			data++;
		}
	}

	return TRUE;
}


int
wire_read_int32 (
		 guint32 *data,
		 gint     count)
{
  if (count > 0)
    {
      if (!wire_read_int8 ((guint8*) data, count * 4))
	return FALSE;

      while (count--)
	{
	  *data = ntohl (*data);
	  data++;
	}
    }

  return TRUE;
}

int
wire_read_int16 (
		 guint16 *data,
		 gint     count)
{
  if (count > 0)
    {
      if (!wire_read_int8 ((guint8*) data, count * 2))
	return FALSE;

      while (count--)
	{
	  *data = ntohs (*data);
	  data++;
	}
    }

  return TRUE;
}

int
wire_read_int8 (
		guint8 *data,
		gint    count)
{
  return wire_read (data, count);
}

int
wire_read_double (
		  gdouble *data,
		  gint     count)
{
  char *str;
  int i;

  for (i = 0; i < count; i++)
    {
      if (!wire_read_string (&str, 1))
	return FALSE;
      sscanf (str, "%le", &data[i]);
      g_free (str);
    }

  return TRUE;
}

int
wire_read_string (
		  gchar **data,
		  gint    count)
{
  guint32 tmp;
  int i;

  for (i = 0; i < count; i++)
    {
      if (!wire_read_int32 (&tmp, 1))
	return FALSE;

      if (tmp > 0)
	{
	  data[i] = g_new (gchar, tmp);
	  if (!wire_read_int8 ((guint8*) data[i], tmp))
	    {
	      g_free (data[i]);
	      return FALSE;
	    }
	}
      else
	{
	  data[i] = NULL;
	}
    }

  return TRUE;
}

int
wire_write_int64 (
		  guint64 *data,
		  gint     count)
{
  guint64 tmp;
  int i;

  if (count > 0)
    {
      for (i = 0; i < count; i++)
	{
	  tmp = htonl (data[i]);
          //d_printf("%u/%u  %ld %ld %ld \n", tmp, (guint32)data, sizeof(long), sizeof(int), sizeof(short));
	  if (!wire_write_int8 ((guint8*) &tmp, sizeof(guint64)))
	    return FALSE;
	}
    }

  return TRUE;
}


int
wire_write_int32(
	guint32* data,
	gint     count)
{
	guint32 tmp;
	int i;

	if (count > 0)
	{
		for (i = 0; i < count; i++)
		{
			tmp = htonl(data[i]);
			//d_printf("%u/%u  %ld %ld %ld \n", tmp, (guint32)data, sizeof(long), sizeof(int), sizeof(short));
			if (!wire_write_int8((guint8*)&tmp, 4))
				return FALSE;
		}
	}

	return TRUE;
}

int
wire_write_int16 (
		  guint16 *data,
		  gint     count)
{
  guint16 tmp;
  int i;

  if (count > 0)
    {
      for (i = 0; i < count; i++)
	{
	  tmp = htons (data[i]);
	  if (!wire_write_int8 ((guint8*) &tmp, 2))
	    return FALSE;
	}
    }

  return TRUE;
}

int
wire_write_int8 (
		 guint8 *data,
		 gint    count)
{
  return wire_write (data, count);
}

int
wire_write_double (
		   gdouble *data,
		   gint     count)
{
  gchar *t, buf[128];
  int i;

  t = buf;
  for (i = 0; i < count; i++)
    {
      sprintf (buf, "%0.50e", data[i]);
      if (!wire_write_string (&t, 1))
	return FALSE;
    }

  return TRUE;
}

int
wire_write_string (gchar **data,
		   gint    count)
{
  guint32 tmp;
  int i;

  for (i = 0; i < count; i++)
    {
      if (data[i])
	tmp = strlen (data[i]) + 1;
      else
	tmp = 0;
#ifdef DEBUG_WIRE_WRITE_STRING
	if(tmp)
	{	d_printf("wire_write_string: %s\n",data[i]);
	}
#endif
      if (!wire_write_int32 (&tmp, 1))
	return FALSE;
      if (tmp > 0)
	if (!wire_write_int8 ((guint8*) data[i], tmp))
	  return FALSE;
    }

  return TRUE;
}
#endif

static void
wire_init ()
{
  if (!wire_ht)
    {
      wire_ht = g_hash_table_new ((GHashFunc) wire_hash,
				  (GCompareFunc) wire_compare);
    }
}

static guint
wire_hash (guint32 *key)
{
  return *key;
}

static gint
wire_compare (guint32 *a,
	      guint32 *b)
{
  return (*a == *b);
}

int GetMsgDataSize(int msg_type)
{	switch(msg_type)
	{	case GP_ERROR:
			return 0;
		case GP_QUIT:
			return 0;
		case GP_CONFIG:
			return sizeof(GPConfig);
		case GP_TILE_REQ:
			return sizeof(GPTileReq);
		case GP_TILE_ACK:
			return -1;//sizeof(GPTileAck);
		case GP_TILE_DATA:
			return sizeof(GPTileData);
		case GP_PROC_RUN:
			return sizeof(GPProcRun);
		case GP_PROC_RETURN:
			return sizeof(GPProcReturn);
		case GP_TEMP_PROC_RUN:
			return -1;
		case GP_TEMP_PROC_RETURN:
			return -1;
		case GP_PROC_INSTALL:
			return sizeof(GPProcInstall);
		case GP_PROC_UNINSTALL:
			return sizeof(GPProcUninstall);
		case GP_EXTENSION_ACK:
			return -1;
	}
	return -1;
}
