/* LIBGIMP - CinePaint Library                                                   
 * Copyright (C) 1995-1997 Peter Mattis and Spencer Kimball                
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.             
 *                                                                              
 * This library is distributed in the hope that it will be useful,              
 * but WITHOUT ANY WARRANTY; without even the implied warranty of               
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU            
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */                                                                             
#ifndef __ENUMS_H__
#define __ENUMS_H__

#include "iodebug.h"
#include "c_typedefs.h"
#include "precision.h"

typedef enum 
{
  RGB         = 0,
  GRAY        = 1,
  INDEXED     = 2,
  U16_RGB     = 3,
  U16_GRAY    = 4,
  U16_INDEXED = 5,
  FLOAT_RGB   = 6,
  FLOAT_GRAY  = 7,
  FLOAT16_RGB   = 8,
  FLOAT16_GRAY  = 9, 
  BFP_RGB       = 10,
  BFP_GRAY      = 11 
} GImageType;

typedef enum 
{ RGB_IMAGE          = 0,
  RGBA_IMAGE         = 1,
  GRAY_IMAGE         = 2,
  GRAYA_IMAGE        = 3,
  INDEXED_IMAGE      = 4,
  INDEXEDA_IMAGE     = 5,
  U16_RGB_IMAGE      = 6,
  U16_RGBA_IMAGE     = 7,
  U16_GRAY_IMAGE     = 8,
  U16_GRAYA_IMAGE    = 9,
  U16_INDEXED_IMAGE  = 10,
  U16_INDEXEDA_IMAGE = 11, 
  FLOAT_RGB_IMAGE    = 12,
  FLOAT_RGBA_IMAGE   = 13,
  FLOAT_GRAY_IMAGE   = 14,
  FLOAT_GRAYA_IMAGE  = 15,
  FLOAT16_RGB_IMAGE    = 16,
  FLOAT16_RGBA_IMAGE   = 17,
  FLOAT16_GRAY_IMAGE   = 18,
  FLOAT16_GRAYA_IMAGE  = 19,
  BFP_RGB_IMAGE    = 20,
  BFP_RGBA_IMAGE   = 21,
  BFP_GRAY_IMAGE   = 22,
  BFP_GRAYA_IMAGE  = 23 
} GDrawableType;

typedef enum 
{
  NORMAL_MODE       = 0,
  DISSOLVE_MODE     = 1,
  MULTIPLY_MODE     = 3,
  SCREEN_MODE       = 4,
  OVERLAY_MODE      = 5,
  DIFFERENCE_MODE   = 6,
  ADDITION_MODE     = 7,
  SUBTRACT_MODE     = 8,
  DARKEN_ONLY_MODE  = 9,
  LIGHTEN_ONLY_MODE = 10,
  HUE_MODE          = 11,
  SATURATION_MODE   = 12,
  COLOR_MODE        = 13,
  VALUE_MODE        = 14
} GLayerMode;

typedef enum
{
  BG_IMAGE_FILL,
  WHITE_IMAGE_FILL,
  TRANS_IMAGE_FILL
} GFillType;

typedef enum
{
  PROC_PLUG_IN = 1,
  PROC_EXTENSION = 2,
  PROC_TEMPORARY = 3
} GProcedureType;

/* This enum is mirrored in "app/plugin.c", make sure
 *  they are identical or bad things will happen.
 */
typedef enum
{
  RUN_INTERACTIVE    = 0x0,
  RUN_NONINTERACTIVE = 0x1,
  RUN_WITH_LAST_VALS = 0x2
} GRunModeType;

typedef enum
{
  GIMP_PDB_EXECUTION_ERROR,
  GIMP_PDB_CALLING_ERROR,
  GIMP_PDB_PASS_THROUGH,
  GIMP_PDB_SUCCESS,
  GIMP_PDB_CANCEL
} GStatusType;

typedef enum
{
  GIMP_INTERNAL,
  GIMP_PLUGIN,
  GIMP_EXTENSION,
  GIMP_TEMPORARY
} GimpPDBProcType;

typedef enum
{
  GIMP_HORIZONTAL,
  GIMP_VERTICAL,
  GIMP_UNKNOWN
} GimpOrientationType;

typedef enum
{
  GIMP_PDB_INT32,
  GIMP_PDB_INT16,
  GIMP_PDB_INT8,
  GIMP_PDB_FLOAT,
  GIMP_PDB_STRING,
  GIMP_PDB_INT32ARRAY,
  GIMP_PDB_INT16ARRAY,
  GIMP_PDB_INT8ARRAY,
  GIMP_PDB_FLOATARRAY,
  GIMP_PDB_STRINGARRAY,
  GIMP_PDB_COLOR,
  GIMP_PDB_REGION,
  GIMP_PDB_DISPLAY,
  GIMP_PDB_IMAGE,
  GIMP_PDB_LAYER,
  GIMP_PDB_CHANNEL,
  GIMP_PDB_DRAWABLE,
  GIMP_PDB_SELECTION,
  GIMP_PDB_BOUNDARY,
  GIMP_PDB_PATH,
  /*  GIMP_PDB_PARASITE, */
  GIMP_PDB_STATUS,
  GIMP_PDB_END
} GimpPDBArgType;

typedef enum
{
  GIMP_APPLY,
  GIMP_DISCARD
} GimpMaskApplyMode;

typedef enum
{
  GIMP_EXPAND_AS_NECESSARY,
  GIMP_CLIP_TO_IMAGE,
  GIMP_CLIP_TO_BOTTOM_LAYER,
  GIMP_FLATTEN_IMAGE
} GimpMergeType;

typedef enum
{
  GIMP_RED_CHANNEL,
  GIMP_GREEN_CHANNEL,
  GIMP_BLUE_CHANNEL,
  GIMP_GRAY_CHANNEL,
  GIMP_INDEXED_CHANNEL,
  GIMP_AUXILLARY_CHANNEL
} GimpChannelType;


typedef enum
{
  GIMP_NO_DITHER,
  GIMP_FS_DITHER,
  GIMP_FSLOWBLEED_DITHER,
  GIMP_FIXED_DITHER,
  GIMP_NODESTRUCT_DITHER
} GimpConvertDitherType;

typedef enum
{
  GIMP_MAKE_PALETTE,
  GIMP_REUSE_PALETTE,
  GIMP_WEB_PALETTE,
  GIMP_MONO_PALETTE,
  GIMP_CUSTOM_PALETTE
} GimpConvertPaletteType;


typedef enum
{
  GIMP_OFFSET_BACKGROUND,
  GIMP_OFFSET_TRANSPARENT
} GimpChannelOffsetType;

typedef enum
{
  ICC_IMAGE_PROFILE,
  ICC_PROOF_PROFILE
} CMSProfileType;

#define PARAM_INT32 GIMP_PDB_INT32
#define PARAM_INT16 GIMP_PDB_INT16
#define PARAM_INT8 GIMP_PDB_INT8
#define PARAM_FLOAT GIMP_PDB_FLOAT
#define PARAM_STRING GIMP_PDB_STRING
#define PARAM_INT32ARRAY GIMP_PDB_INT32ARRAY
#define PARAM_INT16ARRAY GIMP_PDB_INT16ARRAY
#define PARAM_INT8ARRAY GIMP_PDB_INT8ARRAY
#define PARAM_FLOATARRAY GIMP_PDB_FLOATARRAY
#define PARAM_STRINGARRAY GIMP_PDB_STRINGARRAY
#define PARAM_COLOR GIMP_PDB_COLOR
#define PARAM_REGION GIMP_PDB_REGION
#define PARAM_DISPLAY GIMP_PDB_DISPLAY
#define PARAM_IMAGE GIMP_PDB_IMAGE
#define PARAM_LAYER GIMP_PDB_LAYER
#define PARAM_CHANNEL GIMP_PDB_CHANNEL
#define PARAM_DRAWABLE GIMP_PDB_DRAWABLE
#define PARAM_SELECTION GIMP_PDB_SELECTION
#define PARAM_BOUNDARY GIMP_PDB_BOUNDARY
#define PARAM_PATH GIMP_PDB_PATH
#define PARAM_PARASITE GIMP_PDB_PARASITE
#define PARAM_STATUS GIMP_PDB_STATUS
#define PARAM_END GIMP_PDB_END

#define STATUS_EXECUTION_ERROR GIMP_PDB_EXECUTION_ERROR
#define STATUS_CALLING_ERROR GIMP_PDB_CALLING_ERROR
#define STATUS_PASS_THROUGH GIMP_PDB_PASS_THROUGH
#define STATUS_SUCCESS GIMP_PDB_SUCCESS
#define STATUS_CANCEL GIMP_PDB_CANCEL

typedef void   (* GRunProc) (char    *name,
			     int      nparams,
			     GParam  *param,
			     int     *nreturn_vals,
			     GParam **return_vals);

typedef void (*plugin_proc) (void);

struct GPlugInInfo
{	
  /* called when CinePaint application initially starts up */
  plugin_proc init_proc;

  /* called when CinePaint application exits */
  plugin_proc quit_proc;

  /* called by CinePaint so that the plug-in can inform the
   *  CinePaint of what it does. (ie. installing a procedure database
   *  procedure).
   */
  plugin_proc query_proc;

  /* called to run a procedure the plug-in installed in the
   *  procedure database.
   */
  GRunProc run_proc;
};

// ------------------- more -------------

enum
{
	GIMP_WHITE_MASK,
	GIMP_BLACK_MASK,
	GIMP_ALPHA_MASK
} GimpAddMaskType;

enum
{
	GIMP_FG_BG_RGB,
	GIMP_FG_BG_HSV,
	GIMP_FG_TRANS,
	GIMP_CUSTOM
} GimpBlendMode;

enum
{
	GIMP_HARD,
	GIMP_SOFT,
	GIMP_PRESSURE
} GimpBrushApplicationMode;

enum
{
	GIMP_FG_BUCKET_FILL,
	GIMP_BG_BUCKET_FILL,
	GIMP_PATTERN_BUCKET_FILL
} GimpBucketFillMode;

enum
{
	GIMP_VALUE_LUT,
	GIMP_RED_LUT,
	GIMP_GREEN_LUT,
	GIMP_BLUE_LUT,
	GIMP_ALPHA_LUT
} GimpChannelLutType;

enum
{
	GIMP_ADD,
	GIMP_SUB,
	GIMP_REPLACE,
	GIMP_INTERSECT
} GimpChannelOps;

enum
{
	GIMP_IMAGE_CLONE,
	GIMP_PATTERN_CLONE
} GimpCloneType;

enum
{
	GIMP_NORMAL_CONVOL,
	GIMP_ABSOLUTE_CONVOL,
	GIMP_NEGATIVE_CONVOL
} GimpConvolutionType;

enum
{
	GIMP_BLUR_CONVOLVE,
	GIMP_SHARPEN_CONVOLVE,
	GIMP_CUSTOM_CONVOLVE
} GimpConvolveType;

enum
{
	GIMP_DODGEBURN_HIGHLIGHTS,
	GIMP_DODGEBURN_MIDTONES,
	GIMP_DODGEBURN_SHADOWS
} GimpDodgeBurnMode;

enum
{
	GIMP_DODGE,
	GIMP_BURN
} GimpDodgeBurnType;

#if 0
enum
{
	GIMP_FG_IMAGE_FILL,
	GIMP_BG_IMAGE_FILL,
	GIMP_WHITE_IMAGE_FILL,
	GIMP_TRANS_IMAGE_FILL,
	GIMP_NO_IMAGE_FILL
} GimpFillType;
#endif

enum
{
	GIMP_RGB,
	GIMP_GRAY,
	GIMP_INDEXED
} GimpImageBaseType2;

enum
{
	GIMP_RGB_IMAGE,
	GIMP_RGBA_IMAGE,
	GIMP_GRAY_IMAGE,
	GIMP_GRAYA_IMAGE,
	GIMP_INDEXED_IMAGE,
	GIMP_INDEXEDA_IMAGE
} GimpImageType2;

enum
{
	GIMP_ONCE_FORWARD,
	GIMP_ONCE_BACKWARDS,
	GIMP_LOOP_SAWTOOTH,
	GIMP_LOOP_TRIANGLE,
	GIMP_ONCE_END_COLOR
} GimpGradientPaintMode;

enum
{
	GIMP_LINEAR,
	GIMP_BILINEAR,
	GIMP_RADIAL,
	GIMP_SQUARE,
	GIMP_CONICAL_SYMMETRIC,
	GIMP_CONICAL_ASYMMETRIC,
	GIMP_SHAPEBURST_ANGULAR,
	GIMP_SHAPEBURST_SPHERICAL,
	GIMP_SHAPEBURST_DIMPLED,
	GIMP_SPIRAL_CLOCKWISE,
	GIMP_SPIRAL_ANTICLOCKWISE
} GimpGradientType;

enum
{
	GIMP_ALL_HUES,
	GIMP_RED_HUES,
	GIMP_YELLOW_HUES,
	GIMP_GREEN_HUES,
	GIMP_CYAN_HUES,
	GIMP_BLUE_HUES,
	GIMP_MAGENTA_HUES
} GimpHueRange;

enum
{
	GIMP_LINEAR_INTERPOLATION,
	GIMP_CUBIC_INTERPOLATION,
	GIMP_NEAREST_NEIGHBOR_INTERPOLATION
} GimpInterpolationType;

enum
{
	GIMP_NORMAL_MODE,
	GIMP_DISSOLVE_MODE,
	GIMP_BEHIND_MODE,
	GIMP_MULTIPLY_MODE,
	GIMP_SCREEN_MODE,
	GIMP_OVERLAY_MODE,
	GIMP_DIFFERENCE_MODE,
	GIMP_ADDITION_MODE,
	GIMP_SUBTRACT_MODE,
	GIMP_DARKEN_ONLY_MODE,
	GIMP_LIGHTEN_ONLY_MODE,
	GIMP_HUE_MODE,
	GIMP_SATURATION_MODE,
	GIMP_COLOR_MODE,
	GIMP_VALUE_MODE,
	GIMP_DIVIDE_MODE
} GimpLayerModeEffects2;

enum
{
	GIMP_MESSAGE_BOX,
	GIMP_CONSOLE,
	GIMP_ERROR_CONSOLE
} GimpMessageHandlerType;

typedef enum GimpPaintApplicationMode
{
	GIMP_CONTINUOUS,
	GIMP_INCREMENTAL
} GimpPaintApplicationMode;

enum
{
	GIMP_REPEAT_NONE,
	GIMP_REPEAT_SAWTOOTH,
	GIMP_REPEAT_TRIANGULAR
} GimpRepeatMode;

enum
{
	GIMP_PIXELS,
	GIMP_POINTS
} GimpSizeType;

enum
{
	GIMP_STACK_TRACE_NEVER,
	GIMP_STACK_TRACE_QUERY,
	GIMP_STACK_TRACE_ALWAYS
} GimpStackTraceMode;

enum
{
	GIMP_SHADOWS,
	GIMP_MIDTONES,
	GIMP_HIGHLIGHTS
} GimpTransferMode;

#endif /* __ENUMS_H__ */


