# CinePaint README.md

*By Robin.Rowe@cinepaint.org, revised 2020/11/20*

## Intro

CinePaint is a deep paint image retouching tool that supports higher 
color fidelity than ordinary painting tools. CinePaint opens popular
image formats such as JPEG and PNG. CinePaint opens exotic image file 
formats such as DPX, 16-bit TIFF, and OpenEXR. CinePaint has features 
comparable to other raster-based paint programs that draw images and 
manipulate photos. 

CinePaint was created by the Hollywood film industry for retouching 
motion pictures frame-by-frame. Used in making the HARRY POTTER and 
LORD OF THE RINGS films.

## Press

Motion Picture Academy https://www.aswf.io/bts/robin-rowe/ 

## HDR Deep Paint

Pro deep paint tools, such as CinePaint and Adobe Photoshop, offer 
additional capabilities. Support for HDR, that is, high Dynamic 
Ranged. HDR image formats that are 8, 16 or 32 bits per channel deep. 
One RGBA 32-bit pixel contains 128 bits (4x32). Images common on the 
Internet are 8-bit. Motion picture film and digital cinema projectors 
are capable of displaying greater color fidelity. HDR is used for 
making major motion pictures, in high fashion photography and 
among pro photographers making art gallery prints. Also, in B&W pro 
photography due to there only being one channel of image data.

## Flipbook

CinePaint goes beyond other pro paint tools in that it contains a 
flipbook. The flipbook handles playback of a numbered series of 
images as a movie. Using a flipbook interface to advance through 
images is how motion picture frame-by-frame retouching by hand is 
done rapidly. The flipbook has onion-skinning, where the current
frame can be semi-transparent so the previous frame bleeds through 
and can be referenced or traced from.

## Status

CinePaint version 1.4 runs on Windows and is in QA. A lot has been 
happening with the CinePaint 1.x codebase. A new cmake build system,
taking over maintaining GTK1, and fixing lots of legacy bugs. Version
1.4 has been years in the making, a huge code clean-up. Linux, FreeBSD 
and MacOS versions are coming next. Doing the hardest first, that is,
building on Windows using a Linux codebase. CinePaint1 uses GTK1, which 
CinePaint is maintaining although it is quite old. Expect CinePaint 1.4 
release Xmas 2020 or sooner. 

CinePaint version 2.0 was running before but not entirely complete.
Currently broken pending cmake build system improvements. Rather than 
migrating to a newer version of GTK, CinePaint2 uses FLTK. 

## Versions

There are two versions of Cinepaint. Both are being actively developed.

CinePaint1 (Hollywood) forked from CinePaint in 2000. It is a GTK1-based 
C application. CinePaint1 was originally created for Linux then ported
to Windows and other operating systems. Version 1.4 currently runs only
on Windows 32-bit. The Linux, FreeBSD, MacOS and 64-bit versions are 
coming soon. 

CinePaint2 (Glasgow) is a rewrite that was funded by an EU research 
grant, implemented at the University of Glasgow. It is an FLTK-based 
C++ app. CinePaint2 runs but is not complete. Releasing next after 
version 1.4. 

## Film Industry History

CinePaint was used in making the HARRY POTTER and LORD OF THE RINGS films 
and many others. The Linux version of CinePaint1 was created by the film 
industry, both with in-house developers and by funding CinePaint project 
maintainers. Robin Rowe created the port for Windows. A student created 
the MacOS port before getting a job at Apple. 

The film industry started CinePaint1 development in 1998. Silicon Grail,
later acquired by Apple, and motion picture studio Rhythm & Hues led the 
development. Their goal was to create a deep paint alternative to the 
recently discontined SGI IRIX version of Adobe Photoshop to run on the 
emerging Linux studio platform. Although continuously in use in the film 
industry, CinePaint was not well known in the open source community.
CinePaint was used in making THE LAST SAMURAI and TOO FAST, TOO FURIOUS.

Robin Rowe, a columnist at Linux Journal, encountered CinePaint while 
writing an article about Linux being used at Rhythm & Hues. Robin 
observed CinePaint in the wild, being used the studio in making the 
film SCOOBY-DOO. There's a scene in the film where the cartoon character 
Scooby-Doo has to run behind columns in a live action scene in a 
warehouse. The filmmakers were rotoscoping out Scooby-Doo frame by 
frame, whenever the animation layer character passed in front of an 
object that Scooby-Doo should have passed behind. 

After the article was published, LJ readers wrote asking for the source 
code and then started sending back patches. CinePaint had no project 
website.

On July 4th, 2002, Robin Rowe released CinePaint as a SourceForge 
project and became project leader. RnH had used CinePaint on STUART 
LITTLE and Sony was using CinePaint to make the sequel. Sony contributed 
a number of patches and provided an employee as part-time release 
manager for a year to help. ILM contributed the OpenEXR plugin they 
created for CinePaint. 

The legacy Linux/automake build system made maintaining CinePaint 
increasingly difficult. Making any small change could result in hours
of build time. Eventually, CinePaint wouldn't build on any operating 
system reliably. Robin Rowe created a new cmake CinePaint build system 
to solve that. Also created Cmaker, a build automation system that 
writes cmake files, plus C++ code frameworks and unit tests.

The CinePaint source code uses Linux APIs. For Windows it uses the open 
source libunistd library, created by Robin Rowe. Libunistd is a Windows
implementation of the POSIX BSD and System V library calls. Used to 
compile Linux-based C/C++ code on Windows without visible Windows APIs.

## Build

### Windows

	$ mkdir build
	$ cd build
	$ cmake .. -A Win32

Open sln file in Visual Studio and build. Plugins require zlib, jpeg, 
png, tiff and openexr libraries. Windows build requires libunistd.

## Contact

http://cinepaint.org

CinePaint website has not been updated yet. Is outdated. Working on it.

Join on Slack at: https://shorl.com/pugafudebyve

Twitter: @cinepaint

Robin Rowe
CinePaint Project Manager
Beverly Hills, California
imdb.me/robinrowe
323-535-0952

###
