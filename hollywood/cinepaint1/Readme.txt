Readme.txt

Notes on Win32 port
Robin.Rowe@MovieEditor.com 11/16/02

1. shmbuf.c removed from project
2. Fake unistd.h fixes many problems
3. #include <gdk/win32/gdkwin32.h>
4. {Project}{Settings}{C/C++}{Preprocessor}: 
../../win32/,../../,../../../glib-dev/src/glib/,../../../gtk/src/gtk+/,../../../gtk/src/gtk+/gdk
5. libgimp library
6. filmgimp app
7. Link:
../../../glib-dev/src/glib/glib-1.3.lib ../../../gtk/src/gtk+/gdk/gdk-1.3.lib ../../../gtk/src/gtk+/gtk/gtk-1.3.lib  
8. pkg_plug_in creates VC++ dsp project files for nix plug-ins
9. jpeg plug-in requires libjpeg (ver 6b)
10. tiff plug-in requires libtiff
11. png plug-in requires libpng and zlib
12. gz, bz2, and script-fu plug-ins not supported in Windows
13. Text and regex broken.
14. Windows plug-ins are dll libraries, not executables.

Moved files up a level:
gimpenum.h
gimpfeatures.h
gimpprotocol.h
gimpwire.h
gimpprotocol.c
gimpwire.c


To share memory that is not associated with a file, a process must use 
the 

CreateFileMapping specify (HANDLE)0xFFFFFFFF as the hfile parameter 
The corresponding file-mapping object accesses memory backed by the 
system paging file. You must specify a size greater than zero when 
you specify an hfile of (HANDLE)0xFFFFFFFF in a call to CreateFileMapping. 

The easiest way for other processes to obtain a handle of the file-mapping object created by the first process is to use the OpenFileMapping function and specify the object's name. This is referred to as named shared memory. If the file-mapping object does not have a name, the process must obtain a handle to it through inheritance or duplication. For more information on inheritance and duplication, see Inheritance.

../libgimp/debug/libgimp.lib

../../../glib-dev/src/glib/

../../../glib-dev/src/glib/glib-1.3.lib
../../../glib-dev/src/glib/gspawn-win32-helper.lib
../../../glib-dev/src/glib/testgdate.lib
../../../glib-dev/src/glib/testgdateparser.lib
../../../glib-dev/src/glib/testglib.lib

../../../glib-dev/src/glib/gmodule/

../../../glib-dev/src/glib/gmodule/gmodule-1.3.lib
../../../glib-dev/src/glib/gmodule/lipgplugin_a.lib
../../../glib-dev/src/glib/gmodule/libgplugin_b.lib
../../../glib-dev/src/glib/gmodule/testgmodule.lib

../../../glib-dev/src/glib/gobject/

../../../glib-dev/src/glib/gobject/gobject-1.3.lib
../../../glib-dev/src/glib/gobject/gthread-1.3.lib

../../../gtk/src/gtk+/gdk/gdk-1.3.lib
../../../gtk/src/gtk+/gtk/gtk-1.3.lib
../../../gtk/src/intl/gnu-intl.lib



Searching for 'getenv'...
D:\MAXTOR.DRIVE.FAIL\CODE.OSS\FILMGIMP-PROJECT\FILMGIMP-0.7\app\datafiles.c(71):  home = getenv("HOME");
D:\MAXTOR.DRIVE.FAIL\CODE.OSS\FILMGIMP-PROJECT\FILMGIMP-0.7\app\displaylut.c(45):          if(getenv("WHITETURKEY"))
D:\MAXTOR.DRIVE.FAIL\CODE.OSS\FILMGIMP-PROJECT\FILMGIMP-0.7\app\displaylut.c(47):	  if ((filmVar = getenv("FILM")))
D:\MAXTOR.DRIVE.FAIL\CODE.OSS\FILMGIMP-PROJECT\FILMGIMP-0.7\app\gimpbrushlist.c(211):		  home = getenv("HOME");
D:\MAXTOR.DRIVE.FAIL\CODE.OSS\FILMGIMP-PROJECT\FILMGIMP-0.7\app\gimprc.c(312):  env_gimp_dir = getenv ("GIMP_DIRECTORY");
D:\MAXTOR.DRIVE.FAIL\CODE.OSS\FILMGIMP-PROJECT\FILMGIMP-0.7\app\gimprc.c(313):  env_home_dir = getenv ("HOME");
D:\MAXTOR.DRIVE.FAIL\CODE.OSS\FILMGIMP-PROJECT\FILMGIMP-0.7\app\gimprc.c(392):	home_dir = g_strdup (getenv ("HOME"));
D:\MAXTOR.DRIVE.FAIL\CODE.OSS\FILMGIMP-PROJECT\FILMGIMP-0.7\app\gimprc.c(1433):  home = getenv ("HOME");
D:\MAXTOR.DRIVE.FAIL\CODE.OSS\FILMGIMP-PROJECT\FILMGIMP-0.7\app\gimprc.c(1466):	      tmp2 = getenv (token);
D:\MAXTOR.DRIVE.FAIL\CODE.OSS\FILMGIMP-PROJECT\FILMGIMP-0.7\app\gradient.c(5941):	home        = getenv("HOME");
D:\MAXTOR.DRIVE.FAIL\CODE.OSS\FILMGIMP-PROJECT\FILMGIMP-0.7\app\palette.c(1080):	  home = getenv("HOME");
D:\MAXTOR.DRIVE.FAIL\CODE.OSS\FILMGIMP-PROJECT\FILMGIMP-0.7\libgimp\gimp.c(817):  home_dir = getenv ("HOME");
D:\MAXTOR.DRIVE.FAIL\CODE.OSS\FILMGIMP-PROJECT\FILMGIMP-0.7\plug-ins\script-fu\interp_slib.c(186):      if (getenv ("SIOD_LIB"))
D:\MAXTOR.DRIVE.FAIL\CODE.OSS\FILMGIMP-PROJECT\FILMGIMP-0.7\plug-ins\script-fu\interp_slib.c(188):	  siod_lib = getenv ("SIOD_LIB");
D:\MAXTOR.DRIVE.FAIL\CODE.OSS\FILMGIMP-PROJECT\FILMGIMP-0.7\plug-ins\script-fu\interp_slib.c(965):    tmpstr = getenv (get_c_string (str));
D:\MAXTOR.DRIVE.FAIL\CODE.OSS\FILMGIMP-PROJECT\FILMGIMP-0.7\plug-ins\script-fu\interp_slib.c(3838):  init_subr_1 ("getenv", lgetenv);
D:\MAXTOR.DRIVE.FAIL\CODE.OSS\FILMGIMP-PROJECT\FILMGIMP-0.7\plug-ins\script-fu\script-fu-scripts.c(195):      home = getenv ("HOME");
17 occurrence(s) have been found.



/*
PaintFunc16     paint_func;
PaintFunc16     cursor_func;

*/

/*
#define pipe(x) _pipe(x,256, O_BINARY)

void unsetenv(char* var)
{	char* equalsign=strchr(var,'=');
	if(!equalsign)
	{	return;
	}
	*(equalsign+1)=0;
	putenv(var);
}

#define snprintf _snprintf 



#ifdef WIN32
#include "../win32/unistd.h"
#else

#ifdef WIN32
#include "config.h"
#endif

From GTK+ UNIX To Win32 Porting Tutorial:
	http://wolfpack.twu.net/docs/gtkwin32/

"The short story is that if you #include <gdk/gdkx.h> 
then you need to change that to #include <gdk/gdkwin32.h>."

#ifdef WIN32
# include <gdk/win32/gdkwin32.h>
#endif

Works:
GdkEventButton *bevent,
bevent->state & GDK_SHIFT_MASK

struct _GdkEventButton
{
  GdkEventType type;
  GdkWindow *window;
  gint8 send_event;
  guint32 time;
  gdouble x;
  gdouble y;
  gdouble pressure;
  gdouble xtilt;
  gdouble ytilt;
  guint state;
  guint button;
  GdkInputSource source;
  guint32 deviceid;
  gdouble x_root, y_root;
};

Doesn't work:
TransformCore * transform_core;
transform_core->state & ControlMask
 int             state;         state of buttons and keys   





       The  following  POSIX macros are defined to check the file
       type:

              S_ISREG(m)  is it a regular file?

              S_ISDIR(m)  directory?

              S_ISCHR(m)  character device?

              S_ISBLK(m)  block device?

              S_ISFIFO(m) fifo?

              S_ISLNK(m)  symbolic link? (Not in POSIX.1-1996.)

              S_ISSOCK(m) socket? (Not in POSIX.1-1996.)

       The following flags are defined for the st_mode field:

       S_IFMT     0170000   bitmask for the file type bitfields
       S_IFSOCK   0140000   socket
       S_IFLNK    0120000   symbolic link
       S_IFREG    0100000   regular file
       S_IFBLK    0060000   block device
       S_IFDIR    0040000   directory
       S_IFCHR    0020000   character device
       S_IFIFO    0010000   fifo
       S_ISUID    0004000   set UID bit
       S_ISGID    0002000   set GID bit (see below)
       S_ISVTX    0001000   sticky bit (see below)
       S_IRWXU    00700     mask for file owner permissions
       S_IRUSR    00400     owner has read permission
       S_IWUSR    00200     owner has write permission
       S_IXUSR    00100     owner has execute permission
       S_IRWXG    00070     mask for group permissions
       S_IRGRP    00040     group has read permission
       S_IWGRP    00020     group has write permission
       S_IXGRP    00010     group has execute permission
       S_IRWXO    00007     mask for permissions for others (not in group)
       
*/

/*

XListFonts
Syntax
char **XListFonts(display, pattern, maxnames, actual_count_return)
      Display *display;
      char *pattern;
      int maxnames;
      int *actual_count_return;

Arguments
display  Specifies the connection to the X server.  
pattern  Specifies the null-terminated pattern string that can contain wildcard characters.  
maxnames  Specifies the maximum number of names to be returned.  
actual_count_return  Returns the actual number of font names.  

Description
The XListFonts() function returns an array of available font names 
(as controlled by the font search path; see XSetFontPath()) that match 
the string you passed to the pattern argument. The pattern string can 
contain any characters, but each asterisk (*) is a wildcard for any 
number of characters, and each question mark (?) is a wildcard for a 
single character. If the pattern string is not in the Host Portable 
Character Encoding, the result is implementation dependent. Use of 
uppercase or lowercase does not matter. Each returned string is 
null-terminated. If the data returned by the server is in the Latin 
Portable Character Encoding, then the returned strings are in the Host 
Portable Character Encoding. Otherwise, the result is implementation 
dependent. If there are no matching font names, XListFonts() returns 
NULL. The client should call XFreeFontNames() when finished with the 
result to free the memory. 

*/

/*#error Remove_this_line_when_done_testing
*/
/* gtk+\gdk\gdktypes.h:
typedef enum
{
  GDK_SHIFT_MASK    = 1 << 0,
  GDK_LOCK_MASK	    = 1 << 1,
  GDK_CONTROL_MASK  = 1 << 2,
  GDK_MOD1_MASK	    = 1 << 3,
  GDK_MOD2_MASK	    = 1 << 4,
  GDK_MOD3_MASK	    = 1 << 5,
  GDK_MOD4_MASK	    = 1 << 6,
  GDK_MOD5_MASK	    = 1 << 7,
  GDK_BUTTON1_MASK  = 1 << 8,
  GDK_BUTTON2_MASK  = 1 << 9,
  GDK_BUTTON3_MASK  = 1 << 10,
  GDK_BUTTON4_MASK  = 1 << 11,
  GDK_BUTTON5_MASK  = 1 << 12,
  GDK_RELEASE_MASK  = 1 << 13,
  GDK_MODIFIER_MASK = 0x3fff
} GdkModifierType;


*/
/* filmgimp vcpp project C++ include setting:
../../,../../../glib-dev/src/glib/,../../../gtk/src/gtk+/,../../../gtk/src/gtk+/gdk
*/