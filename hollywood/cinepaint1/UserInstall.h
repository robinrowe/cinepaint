/* UserInstall.h
// Only called if cine_directory() doesn't exist, e.g., rower/.filmgimp 
// Copyright Dec 1, 2002, Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)
*/

#ifndef USER_INSTALL_H
#define USER_INSTALL_H

const char* UserInstall(const char* gimpdir);
#define RC_NAME "cinepaintrc"

#endif