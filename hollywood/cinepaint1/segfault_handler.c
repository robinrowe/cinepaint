// segfault_handler.c
// From https://spin.atomicobject.com/2013/01/13/exceptions-stack-traces-c/

#include "segfault_handler.h"

#ifdef _WIN32

#include <unistd.h>
#include <windows.h>
#include <stdio.h>

#define CASE(x) case x: s = #x; break

LONG WINAPI windows_exception_handler(EXCEPTION_POINTERS * ExceptionInfo)
{   const char* s = 0;
  switch(ExceptionInfo->ExceptionRecord->ExceptionCode)
  {
    CASE(EXCEPTION_ACCESS_VIOLATION);
    CASE(EXCEPTION_ARRAY_BOUNDS_EXCEEDED);
    CASE(EXCEPTION_BREAKPOINT);
    CASE(EXCEPTION_DATATYPE_MISALIGNMENT);
    CASE(EXCEPTION_FLT_DENORMAL_OPERAND);
    CASE(EXCEPTION_FLT_DIVIDE_BY_ZERO);
    CASE(EXCEPTION_FLT_INEXACT_RESULT);
    CASE(EXCEPTION_FLT_INVALID_OPERATION);
    CASE(EXCEPTION_FLT_OVERFLOW);
    CASE(EXCEPTION_FLT_STACK_CHECK);
    CASE(EXCEPTION_FLT_UNDERFLOW);
    CASE(EXCEPTION_ILLEGAL_INSTRUCTION);
    CASE(EXCEPTION_IN_PAGE_ERROR);
    CASE(EXCEPTION_INT_DIVIDE_BY_ZERO);
    CASE(EXCEPTION_INT_OVERFLOW);
    CASE(EXCEPTION_INVALID_DISPOSITION);
    CASE(EXCEPTION_NONCONTINUABLE_EXCEPTION);
    CASE(EXCEPTION_PRIV_INSTRUCTION);
    CASE(EXCEPTION_SINGLE_STEP);
    CASE(EXCEPTION_STACK_OVERFLOW);
    default:
      s = "Unrecognized Exception";
      break;
  }
  printf("Signal %s\n", s);
#if 0
  /* If this is a stack overflow then we can't walk the stack, so just show
    where the error happened */
  if (EXCEPTION_STACK_OVERFLOW != ExceptionInfo->ExceptionRecord->ExceptionCode)
  {
      windows_print_stacktrace(ExceptionInfo->ContextRecord);
  }
  else
  {
      addr2line(icky_global_program_name, (void*)ExceptionInfo->ContextRecord->Eip);
  }
#endif 
  return EXCEPTION_EXECUTE_HANDLER;
}

LONG WINAPI TopLevelExceptionHandler(PEXCEPTION_POINTERS pExceptionInfo)
{   char s[80];
    sprintf(s,"Fatal: Unhandled exception %0x",pExceptionInfo->ExceptionRecord->ExceptionCode);
    MessageBoxA(NULL, s, "CinePaint1", MB_OK | MB_ICONSTOP);
    exit(1);
    return EXCEPTION_CONTINUE_SEARCH;
}

void set_signal_handler()
{
  SetUnhandledExceptionFilter(windows_exception_handler);
}

#else

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <execinfo.h>
 
void almost_c99_signal_handler(int sig)
{
  switch(sig)
  {
    case SIGABRT:
      fputs("Caught SIGABRT: usually caused by an abort() or assert()\n", stderr);
      break;
    case SIGFPE:
      fputs("Caught SIGFPE: arithmetic exception, such as divide by zero\n",
            stderr);
      break;
    case SIGILL:
      fputs("Caught SIGILL: illegal instruction\n", stderr);
      break;
    case SIGINT:
      fputs("Caught SIGINT: interactive attention signal, probably a ctrl+c\n",
            stderr);
      break;
    case SIGSEGV:
      fputs("Caught SIGSEGV: segfault\n", stderr);
      break;
    case SIGTERM:
    default:
      fputs("Caught SIGTERM: a termination request was sent to the program\n",
            stderr);
      break;
  }
  _Exit(1);
}
 
void signalHandler(int signal)
{   void* addrBuffer[100];
    backtrace(addrBuffer, 100);
    // print addresses to log
    exit(1);
}

void set_signal_handler()
{
  signal(SIGABRT, almost_c99_signal_handler);
  signal(SIGFPE,  almost_c99_signal_handler);
  signal(SIGILL,  almost_c99_signal_handler);
  signal(SIGINT,  almost_c99_signal_handler);
  signal(SIGSEGV, almost_c99_signal_handler);
  signal(SIGTERM, almost_c99_signal_handler);
  signal(SIGSEGV, signalHandler);
}
 
#endif