/* win32/extra.c
// Windows emulation of some extra *nix and GTK+ functions
// In-memory buffer used instead of pipe to plug-ins
// Copyright Nov 24, 2002, Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)
*/

#include "extra.h"

#ifndef _WIN32
#error
#endif

#include <sys/timeb.h>
#include <sys/types.h>

#if 0
void gettimeofday(struct timeval* t,void* timezone)
{	struct _timeb timebuffer;
	_ftime( &timebuffer );
	t->tv_sec=timebuffer.time;
	t->tv_usec=1000*timebuffer.millitm;
}

/*#define kill(pid,signum) exit(1)*/

int kill(pid_t pid,int signum)
{	TerminateProcess((HANDLE)pid,signum);
	return 0;
}

pid_t waitpid(pid_t pid,int* retval,int options)
{	WaitForSingleObject((HANDLE)pid,INFINITE);
	return GetExitCodeProcess((HANDLE)pid,(LPDWORD) retval)? pid:(pid_t)-1;
}

/* between 0.0 and 1.0 */
double drand48(void)
{	double r=(double)rand();
	r/=RAND_MAX;
	return r > 1.0? 1.0:r;
}

/*double srand48(time_t);*/
void srand48(long int seedval)
{	srand(seedval);
}

long int random(void)
{	return rand();
}

#endif
