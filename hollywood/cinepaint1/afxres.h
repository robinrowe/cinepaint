/* win32/afxres.h
// Typical Windows include stuff
// Copyright November 6, 2002, Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)
*/

#include <winnt.h>
#include <winuser.h>
#include <winver.h> // w/o this can't see properties tab!
