/* win32/extra.h 
// Windows emulation of some extra *nix and GTK+ functions
// Copyright Nov 12, 2002, Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)
*/

#ifndef EXTRA_H
#define EXTRA_H

#include <unistd.h>
#include <stdlib.h>

#if 0
// #define MAXPATHLEN MAX_PATH
#define ControlMask GDK_CONTROL_MASK
#define ShiftMask GDK_SHIFT_MASK
#define Button1Mask GDK_BUTTON1_MASK
#endif

#define M_PI	3.14159265358979323846 /* pi */  
#if 0
int readlink(const char *path, char *buf, size_t bufsiz); 
int symlink(const char *oldpath, const char *newpath); 

typedef int pid_t;

int kill(pid_t pid,int sig);
pid_t waitpid(pid_t pid,int* retval,int options);

double drand48(void);
void srand48(long int seedval);
long int random(void);
#endif
/*extern int __cdecl mkdir2(const char *dir,int per);
*/


#endif