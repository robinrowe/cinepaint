// segfault_handler.h
// Copyright 2021/02/03 Robin.Rowe@CinePaint.org
// License open source MIT

#ifndef segfault_handler_h
#define segfault_handler_h

void set_signal_handler();

#endif