/* CinePaint -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <locale.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include "config.h"
#include "version.h"
#include "../app/tag.h"
#include "../app/rc.h"
#include "i18n.h"
#include "../app/datadir.h"
#include "../app/app.h"
#include "segfault_handler.h"

#if 0
#ifdef _DEBUG
#define HEAP_DEBUG
#ifdef HEAP_DEBUG
#include <crtdbg.h>
#endif
#endif
#endif
#if 0
#include <crtdbg.h>
#endif

#ifndef  WAIT_ANY
#define  WAIT_ANY -1
#endif   /*  WAIT_ANY  */

#ifdef HAVE_OY
#include <oyranos/oyranos.h>
#include <oyranos/oyranos_config.h>
#include <oyranos/oyranos_version.h>
char *oyranos_temp_path = NULL;
void *myAlloc(size_t n) { return calloc(sizeof(char), n); }
#endif

#define  CP_CHANGE_CODESET 0
#ifdef __APPLE__
#include <CoreFoundation/CoreFoundation.h>
#endif

#include "../app/appenv.h"
#include "../app/app_procs.h"
#include "../app/errors.h"
#include "../app/install.h"
#include "../app/gdisplay.h"

static RETSIGTYPE on_signal (int);
static RETSIGTYPE on_sig_child (int);
static RETSIGTYPE on_sig_refresh (int);
static void       init (void);

char **batch_cmds;
int parentPID = 0;		/* If invoked using share memory intf from another process.. who parent is */


/* LOCAL data */
static int cine_argc;
static char **cine_argv;
static int useSharedMem = FALSE;



/* added by IMAGEWORKS (02/21/02) */
static int serverPort;
static char *serverLog;


/*
 *  argv processing:
 *      Arguments are either switches, their associated
 *      values, or image files.  As switches and their
 *      associated values are processed, those slots in
 *      the argv[] array are NULLed. We do this because
 *      unparsed args are treated as images to load on
 *      startup.
 *
 *      The GTK switches are processed first (X switches are
 *      processed here, not by any X routines).  Then the
 *      general CinePaint switches are processed.  Any args
 *      left are assumed to be image files CinePaint should
 *      display.
 *
 *      The exception is the batch switch.  When this is
 *      encountered, all remaining args are treated as batch
 *      commands.
 */

int main (int argc, char **argv)
{
  InitApp();
  int show_version;
  int show_help;
  int i, j;
#ifdef HAVE_PUTENV
  gchar *display_name, *display_env;
#endif
#ifdef _WIN32
	_fmode = _O_BINARY; /* doesn't seem to work, had to reset open calls */
#if 0
/* This creates a console window and joins it to a GUI process: */
	AllocConsole();
	freopen("CONIN$","rb",stdin);
	freopen("CONOUT$","wb",stdout);
	freopen("CONOUT$","wb",stderr);
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF|_CRTDBG_CHECK_ALWAYS_DF|_CRTDBG_LEAK_CHECK_DF);
#endif
#endif
  /* ATEXIT (g_mem_profile); */

  /* Initialize variables */
  SetPathData(argv[0]);

#if 0
  /* Where are we? */
  {
    int len = strlen(prog_name) * 2 + 128;
    char *text = GetDirAbsoluteExec( prog_name );

    /* remove the expected bin/ dir */
    char *tmp = strrchr(text, DIR_SEPARATOR_C);
    if(tmp)
      *tmp = 0;

    /* look for something known */
    {
      char *t = malloc((strlen(text)+1024)*2);
      FILE *fp;

      sprintf( t,"%s%slib%scinepaint%s%s%splug-ins%sblur",
               text, DIR_SEPARATOR, DIR_SEPARATOR,
               DIR_SEPARATOR, PROGRAM_VERSION, DIR_SEPARATOR, DIR_SEPARATOR);
      fp = fopen(t,"r");
      if(fp)
      {
#ifdef HAVE_OY
        /* set a profile temporary path */
        /* this is obsolete with the XDG paths being supported since v0.1.8 */
#if (OYRANOS_VERSION < 108)
        if(strcmp(text, GetDirPrefix()) != 0)
        {
          int n = oyPathsCount(), i, has_path = 0;

          oyranos_temp_path = myAlloc (strlen(GetDirPrefix()) +
                     sizeof(OY_ICCDIRNAME) + strlen(text) + 64);
          sprintf(oyranos_temp_path, "%s%sshare%scolor%s%s",
              text, DIR_SEPARATOR, DIR_SEPARATOR, DIR_SEPARATOR, OY_ICCDIRNAME);
          for(i = 0; i < n; ++i)
          {
            char *temp = oyPathName(i, myAlloc);
            if(strcmp(oyranos_temp_path, temp) == 0)
              has_path = 1;
            if(temp) free(temp);
          }
          if(!has_path)
          {
            struct stat status;
            int r = 0;

            status.st_mode = 0;
            r = stat (oyranos_temp_path, &status);
            r = !r &&
              ((status.st_mode & S_IFMT) & S_IFDIR);
            if(!r)
              oyPathAdd( oyranos_temp_path );
          } else {
            free( oyranos_temp_path );
            oyranos_temp_path = NULL;
          }
        }
#endif
#endif
        SetDirPrefix( text );
        fclose(fp);
      }
      free(t);
    }

    path = (char*) malloc( len + strlen(text) + 1024 );
    sprintf (path, "%s%s%s", GetDirPrefix(), DIR_SEPARATOR, "share/locale");
  }
#endif

  /* Initialize Gtk toolkit */
  gtk_init (&argc, &argv);

#ifdef HAVE_PUTENV
  display_name = gdk_get_display ();
  display_env = g_new (gchar, strlen (display_name) + 9);
  *display_env = 0;
  strcat (display_env, "DISPLAY=");
  strcat (display_env, display_name);
  putenv (display_env);
#endif
  batch_cmds = g_new (char*, argc);
  batch_cmds[0] = NULL;

  show_version = FALSE;
  show_help = FALSE;
    App* app = GetApp();

  for (i = 1; i < argc; i++)
    {
      /* added by IMAGEWORKS (02/21/02) */
      if( strcmp( argv[i], "-server" ) == 0 ) {
          serverPort = atoi( argv[++i] );
          serverLog = argv[++i];
      }
      else if ((strcmp (argv[i], "--no-interface") == 0) ||
	  (strcmp (argv[i], "-n") == 0))
	{
	  app->no_interface = TRUE;
	}
      else if ((strcmp (argv[i], "--batch") == 0) ||
	       (strcmp (argv[i], "-b") == 0))
	{
	  for (j = 0, i++ ; i < argc; j++, i++)
	      batch_cmds[j] = argv[i];
	  batch_cmds[j] = NULL;
	  if (batch_cmds[0] == NULL)	/* We need at least one batch command */
		 show_help = TRUE;
	  if (argv[i-1][0] != '-')		/* Did loop end due to a new argument? */
		 --i;						/* Ensure new argument gets processed */
	}
      else if (strcmp (argv[i], "-seq") == 0)
      {
	app->start_with_sfm = TRUE;
	if(argv[i+1]!=NULL)
	  {
	    app->initial_frames_loaded = atoi(argv[i+1]);
	  }
	if(app->initial_frames_loaded<1)
	  {
	    show_help = TRUE;
	    argv[i] = NULL;
	  }
      }
      else if ((strcmp (argv[i], "--help") == 0) ||
	       (strcmp (argv[i], "-h") == 0))
	{
	  show_help = TRUE;
	  argv[i] = NULL;
	}
      else if (strcmp (argv[i], "--version") == 0 ||
	       strcmp (argv[i], "-v") == 0)
	{
	  show_version = TRUE;
	  argv[i] = NULL;
	}
      else if (strcmp (argv[i], "--no-data") == 0)
	{
	  app->no_data = TRUE;
	  argv[i] = NULL;
	}
      else if (strcmp (argv[i], "--no-splash") == 0)
	{
	  app->no_splash = TRUE;
	  argv[i] = NULL;
	}
      else if (strcmp (argv[i], "--no-splash-image") == 0)
	{
	  app->no_splash_image = TRUE;
	  argv[i] = NULL;
	}
      else if (strcmp (argv[i], "--verbose") == 0)
	{
	  app->be_verbose = TRUE;
	  argv[i] = NULL;
	}
#ifdef BUILD_SHM
      else if (strcmp (argv[i], "--no-shm") == 0)
	{
	  use_shm = FALSE;
	  argv[i] = NULL;
	}
#endif
      else if (strcmp (argv[i], "--debug-handlers") == 0)
	{
	  app->use_debug_handler = TRUE;
	  argv[i] = NULL;
	}
      else if (strcmp (argv[i], "--console-messages") == 0)
        {
          app->console_messages = TRUE;
	  argv[i] = NULL;
        }
      else if( strcmp( argv[i], "--sharedmem" ) == 0 )
	{
#ifdef BUILD_SHM
	  if( i+6 >= argc )
		  show_help = TRUE;
	  else
	  {
		  useSharedMem = TRUE;
		  parentPID = atoi( argv[++i] );
		  shmid = atoi( argv[++i] );
		  offset = atoi( argv[++i] );
		  chans = atoi( argv[++i] );
		  xSize = atoi( argv[++i] );
		  ySize = atoi( argv[++i] );
		  /*
		  d_printf( "Parent PID: %d, shmid: %d, off: %d, chans: %d, (%d,%d)\n", 
				  parentPID, shmid, offset, chans, xSize, ySize );
		  */
	  }
#else
		d_puts("Built without shared memory -- can't enable!");
#endif
	}
#ifdef __APPLE__
      else if (strstr (argv[i], "-psn_") != NULL)
        {
	  argv[i] = NULL;
        }
#endif
/*
 *    ANYTHING ELSE starting with a '-' is an error.
 */

      else if (argv[i][0] == '-')
	{
	  show_help = TRUE;
	}
    }

  if (show_version)
    g_print (PROGRAM_TITLE "\n");

  if (show_help)
    {
      g_print ("\007Usage: %s [option ...] [files ...]\n", argv[0]);
      g_print ("Valid options are:\n");
      g_print ("  -h --help              Output this help.\n");
      g_print ("  -v --version           Output version info.\n");
      g_print ("  -b --batch <commands>  Run in batch mode.\n");
      g_print ("  -n --no-interface      Run without a user interface.\n");
      g_print ("  --no-data              Do not load patterns, gradients, palettes, brushes.\n");
      g_print ("  --verbose              Show startup messages.\n");
      g_print ("  --no-splash            Do not show the startup window.\n");
      g_print ("  --no-splash-image      Do not add an image to the startup window.\n");
      g_print ("  --no-shm               Do not use shared memory between CinePaint and its plugins.\n");
      g_print ("  --no-xshm              Do not use the X Shared Memory extension.\n");
      g_print ("  --console-messages     Display warnings to console instead of a dialog box.\n");
      g_print ("  --debug-handlers       Enable debugging signal handlers.\n");
      g_print ("  --display <display>    Use the designated X display.\n");
      g_print ("  -seq <number>          Open frame manager and load a number of frames into\n");
      g_print ("                         a film sequence.\n\n");

    }

  if (show_version || show_help)
    return 0;//exit (0);

#if GTK_MAJOR_VERSION >= 1
  g_log_set_handler(NULL,
                    (GLogLevelFlags) (G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),
                    message_func_cb, NULL);
#else
  g_set_message_handler (&message_func);
#endif

# ifndef _DEBUG
  set_signal_handler();
#endif

  /* Keep the command line arguments--for use in cine_init */
  cine_argc = argc - 1;
  cine_argv = argv + 1;

  /* Check the installation */
#ifdef BUILD_SHM
  if( useSharedMem )
	install_verify( init_shmem );
  else
#endif
  install_verify (init);


  /* Main application loop */
//  if (!app_exit_finish_done ())
    gtk_main ();

  {
#if 0
    extern int ref_ro, ref_rw,ref_un, ref_fa , ref_uf;
    
    g_warning ("Refs:   %d+%d = %d", ref_ro, ref_rw, (ref_ro+ref_rw));
    g_warning ("Unrefs: %d", ref_un);
    
    g_warning ("Frefs:   %d", ref_fa);
    g_warning ("Funrefs: %d", ref_uf);
#endif
  }

#ifdef HAVE_OY
  /* remove a profile temporary path */
  if(oyranos_temp_path)
  {
#if (OYRANOS_VERSION < 108)
    oyPathRemove( oyranos_temp_path );
#endif
    free( oyranos_temp_path );
  }
#endif

  return 0;
}

static void
init ()
{
  /*  Continue initializing  */

  /* Initialize the application */
  app_init ();

#if GTK_MAJOR_VERSION > 1
  /* Why does gtk change LC_NUMERIC with the first window in gtk-2.4 ? beku */
  {
    char settxt[64];
    if(setlocale (LC_NUMERIC, NULL))
    {
      snprintf( settxt,63, "export LC_NUMERIC=%s", setlocale(LC_NUMERIC, NULL));
      putenv( settxt );
      setenv("LC_NUMERIC", setlocale (LC_NUMERIC, NULL), 1);
    } else
      printf("%s:%d %s() no LC_NUMERIC set\n",__FILE__,__LINE__,__func__);
  }
#endif

  cine_init (cine_argc, cine_argv);
}

static int caught_fatal_sig = 0;

static RETSIGTYPE
on_signal (int sig_num)
{
  if (caught_fatal_sig)
/*    raise (sig_num);*/
    kill (getpid (), sig_num);
  caught_fatal_sig = 1;

  switch (sig_num)
    {
    case SIGHUP:
      terminate ("sighup caught");
      break;
    case SIGINT:
      terminate ("sigint caught");
      break;
    case SIGQUIT:
      terminate ("sigquit caught");
      break;
    case SIGABRT:
      terminate ("sigabrt caught");
      break;
    case SIGBUS:
      fatal_error ("sigbus caught");
      break;
    case SIGSEGV:
      fatal_error ("sigsegv caught");
      break;
    case SIGPIPE:
      /*terminate*/printf ("sigpipe caught\n");
      break;
    case SIGTERM:
      terminate ("sigterm caught");
      break;
    case SIGFPE:
      fatal_error ("sigfpe caught");
      break;
    default:
      fatal_error ("unknown signal");
      break;
    }
}

static RETSIGTYPE
on_sig_child (int sig_num)
{
  int pid;
  int status;

  while (1)
    {
      pid = waitpid (WAIT_ANY, &status, WNOHANG);
      if (pid <= 0)
	break;
    }
}

