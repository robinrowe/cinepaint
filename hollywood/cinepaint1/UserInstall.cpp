/* win32/UserInstall.c
// Perform the default installation steps
// Copyright Dec 1, 2002, Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)

#  Perform the default installation steps for the
#  specified user home directory:
#
#  1) Create ~/.filmgimp if it doesn't exist
#  2) Copy system gimprc file to ~/.filmgimp
#  3) Create brushes, gradients, palettes, patterns, plug-ins subdirs
#  4) Create the tmp subdirectory for disk-swapping undo buffers
#  5) Copy the palette files in system palette directory
#  6) Copy the vbr brush files in system vbr to ~/.filmgimp/brushes directory
#  */

#ifndef _WIN32
#error This file only used in Windows
#endif

#include <gwin32.h>
#include <errno.h>
#include "unistd.h"
#include "../app/datadir.h"
#include "UserInstall.h"
#include "../win32/extra.h"

#define MKDIR(dir) \
	name=dir;\
	strcpy(buffer+len,name);\
	status=mkdir(buffer,755);\
	if(-1==status && EEXIST!=errno)\
		return name
	
/* returns error message or 0 */
const char* UserInstall(const char* gimpdir)
{	char buffer[MAXPATHLEN];
	size_t len=0;
	int status;
	const char* name;
	const char* homedir=GetPathHome();
	MKDIR(homedir);
	MKDIR(gimpdir);
	len=strlen(gimpdir);
	strcpy(buffer,gimpdir);
	name="/" RC_NAME;
	strcpy(buffer+len,name);
	if(!CopyFile(RC_NAME,buffer,FALSE))
	{	return name+1;
	}
	name="/gtkrc";
	strcpy(buffer+len,name);
	if(!CopyFile(name+1,buffer,FALSE))
	{	return name+1;
	}
//	len=strlen(homedir);
//	strcpy(buffer,homedir);
	MKDIR("/brushes");
	MKDIR("/gradients");
	MKDIR("/palettes");
	MKDIR("/patterns");
	MKDIR("/plug-ins");
	MKDIR("/gfig");
	MKDIR("/tmp");
	MKDIR("/scripts");
	MKDIR("/gflares");
	return 0;
}