# Cinepaint1 BUGS.md

*Robin.Rowe@cinepaint.org 2020/11/20*

## Bugs

RR = Robin
MR = Matt

2021/04/22-RR: Win32 MVP, works enough for alpha Windows release
2021/04/22-RR: Gfig background draw crashes
2021/04/22-RR: Gfig X-close exits program
2021/04/22-RR: Cinenotes screen doesn't quite refresh completely after edit
2021/01/01-RR: Linux broken
2021/01/01-RR: MacOS broken
2021/01/01-RR: Win64 broken
2021/01/01-RR: TIFF broken, lib issues
2021/01/01-RR: OpenEXR broken, lib issues
2021/01/01-RR: Twain broken
2021/01/01-RR: #ifdef _WIN32 need to be removed 
2021/01/01-RR: int pointer casts break 64-bit builds
2021/02/04-MR: Wacom broken
2021/02/04-MR: All the dialog boxes show up on the wrong (non-wacom cintiq) screen in a 2 monitor setup
2021/02/04-MR: Erasing crashes the software
RR: Doesn't happen to me, although erasure looks funky
2021/02/04-MR: FILL tool crashes software
2021/02/04-MR: Doesn't save preferences (I'll double check to see if this is global)
RR: Details???
2021/02/04-MR: Cannot load custom brushes AKA GrutBrush library - what is the brush format?
RR: CinePaint brushes are in GBR format. Any image can be made into a brush by saving it as a GBR and placing it in the brushes directory.
2021/02/04-MR: <CTRL Z> does not undo
RR: Was broken due to layers being broken, please retest
2021/02/04-MR: EYEDROPPER tool does not update in COLOR SELECTION box
RR: Works for me
2021/02/04-MR: Cannot QUIT or EXIT program
RR: Fixed

## Debug Verbosity

These #defines are commented out for normal operation:

	app\scale.c(29): VERBOSE_GDISP
	gtk1\gtk1win\gtk\gtkpixmap.c(35): VERBOSE_PIXMAP_SET
	gtk1\gtk1win\gtk\gtkpreview.c(40): VERBOSE_BITBLT
	gtk1\gtk1win\gtk\gtkobject.c VERBOSE_REF
	gtk1\gtk1win\gtk\gtkobject.c VERBOSE_REF2
	pdb\pixelrgn.c(29): VERBOSE_TILE_FILL
	pdb\plugin.c(77): VERBOSE_SCANLINE
	pdb\plugin.c(78): VERBOSE_ADD_PLUGIN
	pdb\plugin.c(79): VERBOSE_BUFFER_COPY
	pdb\plugin.c(80): VERBOSE_FILE_HANDLER
	pdb\plugin.c(81): VERBOSE_WIRE_TABLES
	pdb\plugin_main.c(57): DEBUG_RUN_PROC
	pdb\procedural_db.c(35): VERBOSE_PDB_LOOKUP
	pdb\procedural_db.c(36): VERBOSE_PDB_EXECUTE
	wire\protocol.c(25): VERBOSE_PROC_INSTALL
	wire\protocol.c(26): VERBOSE_PROC_RUN
	wire\protocol.c(27): VERBOSE_WIRE_HANDLERS
	wire\wire.c(41): VERBOSE_WIRE
	wire\wire.c(42): VERBOSE_WIRE_HANDLER
	wire\wire.c(45): VERBOSE_WRITE_STRING
	wire\wire.c(46): VERBOSE_WIRE_BYTES 
