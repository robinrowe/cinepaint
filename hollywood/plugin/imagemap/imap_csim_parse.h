#ifndef BISON_IMAP_CSIM_TAB_H
# define BISON_IMAP_CSIM_TAB_H

#ifndef YYSTYPE
typedef union {
   int val;
   double value;
   char id[256];
} yystype;
# define YYSTYPE yystype
# define YYSTYPE_IS_TRIVIAL 1
#endif
# define	IMG	257
# define	SRC	258
# define	WIDTH	259
# define	HEIGHT	260
# define	BORDER	261
# define	USEMAP	262
# define	START_MAP	263
# define	END_MAP	264
# define	NAME	265
# define	AREA	266
# define	SHAPE	267
# define	COORDS	268
# define	ALT	269
# define	HREF	270
# define	NOHREF	271
# define	TARGET	272
# define	ONMOUSEOVER	273
# define	ONMOUSEOUT	274
# define	ONFOCUS	275
# define	ONBLUR	276
# define	AUTHOR	277
# define	DESCRIPTION	278
# define	BEGIN_COMMENT	279
# define	END_COMMENT	280
# define	FLOAT	281
# define	STRING	282


extern YYSTYPE csim_lval;

#endif /* not BISON_IMAP_CSIM_TAB_H */
