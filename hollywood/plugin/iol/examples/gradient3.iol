
# A gradient example
#
# $Id: gradient3.iol,v 1.2 2008/12/24 06:28:52 robinrowe Exp $

size a b ;

alpha = ( 1.0 ) ;

v = 1.0 / b ;

initend ;

pos x y;

y *= v ;
y !-= 1.0 ;

output y y y alpha;

end ;

