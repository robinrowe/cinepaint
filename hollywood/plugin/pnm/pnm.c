/*
   pnm.c - is a pnm/pfm converter for CinePaint

   Copyright (c) 2005-2007 Kai-Uwe Behrmann

   Author: Kai-Uwe Behrmann <ku.b at gmx.de>

   Permission to use, copy, modify, and distribute this software and its
   documentation for any purpose and without fee is hereby granted,
   provided that the above copyright notice appear in all copies and that
   both that copyright notice and this permission notice appear in
   supporting documentation.

   This file is provided AS IS with no warranties of any kind.  The author
   shall have no liability with respect to the infringement of copyrights,
   trade secrets or any patents by this file or any part thereof.  In no
   event will the author be liable for any lost revenue or profits or
   other special, indirect and consequential damages.

   * initial version - ku.b 06.11.2005 : v0.1
   * bug fix - ku.b 08.11.2005
   * add saving - ku.b 30.3.2006 : v0.2
   * support alpha images, pam spec: http://netpbm.sourceforge.net/doc/pam.html
   * 8-bit save bugfix - ku.b 07.06.2007 : v0.3

*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include "../app/gdisplay.h"
#include "libgimp/gimp.h"

/***   some configuration switches   ***/

/*** macros ***/

/* --- Helpers --- */

/* m_free (void*) */
#define m_free(x) {                                         \
  if (x != NULL) {                                          \
    free (x); x = NULL;                                     \
  } else {                                                  \
    g_print ("%s:%d %s() nothing to delete " #x "\n",       \
    __FILE__,__LINE__,__func__);                            \
  }                                                         \
}

/* m_new (void*, type, size_t, action) */
#define m_new(ptr,type,size,action) {                       \
  if (ptr != NULL)                                          \
    m_free (ptr)                                            \
  if ((size) <= 0) {                                        \
    g_print ("%s:%d %s() nothing to allocate %d\n",         \
    __FILE__,__LINE__,__func__, (int)size);                 \
  } else {                                                  \
    ptr = calloc (sizeof (type), (size_t)size);             \
  }                                                         \
  if (ptr == NULL) {                                        \
    g_message ("%s:%d %s() %s %d %s %s .",__FILE__,__LINE__,\
         __func__, _("Can not allocate"),(int)size,         \
         _("bytes of  memory for"), #ptr);                  \
    action;                                                 \
  }                                                         \
}



/***   versioning   ***/

static char plugin_version_[64];
static char Version_[] = "v0.3";


/***   include files   ***/

#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/*** struct definitions ***/

/*** declaration of local functions ***/

/* --- plug-in API specific functions --- */
void   query(void);
void   run(gchar* name,
	gint             nparams,
	GimpParam* param,
	gint* nreturn_vals,
	GimpParam** return_vals);

/* --- pnm functions --- */
gint32 load_image(gchar* filename);


gint   save_image(gchar* filename,
	gint32           image_ID,
	gint32           drawable_ID);


/* --- helpers --- */
int wread(unsigned char* data,      /* read a word */
	size_t  pos,
	size_t  max,
	size_t* start,
	size_t* length);

/*** global variables ***/


GimpPlugInInfo plugin_info =
{ NULL,    /* init_proc */
	NULL,    /* quit_proc */
	query,   /* query_proc */
	run,     /* run_proc */
};


/*** functions ***/

// MAIN ()

/* Version_ */
static char* version(void)
{
	sprintf(plugin_version_, "%s %s %s", "PNM plug-in for",
		"CinePaint",
		Version_);
	return &plugin_version_[0];
}

void
query(void)
{
	char* date = NULL;
	static GimpParamDef load_args[] =
	{ { GIMP_PDB_INT32, "run_mode", "Interactive, non-interactive" },
		{ GIMP_PDB_STRING, "filename", "The name of the file to load" },
		{ GIMP_PDB_STRING, "short_filename", "The name of the file to load." },
	};
	static GimpParamDef load_return_vals[] =
	{ { GIMP_PDB_IMAGE, "image", "Output image" },
	};
	static int nload_args = (int)sizeof(load_args) / (int)sizeof(load_args[0]);
	static int nload_return_vals = (int)sizeof(load_return_vals) / (int)sizeof(load_return_vals[0]);
	static GimpParamDef save_args[] =
	{ { GIMP_PDB_INT32, "run_mode", "Interactive, non-interactive" },
		{ GIMP_PDB_IMAGE, "image", "Input image" },
		{ GIMP_PDB_DRAWABLE, "drawable", "Drawable to save" },
		{ GIMP_PDB_STRING, "filename", "The name of the file to save the image in" },
		{ GIMP_PDB_STRING, "short_filename", "The name of the file to save the image in" },
	};
	static int nsave_args = (int)sizeof(save_args) / (int)sizeof(save_args[0]);
	m_new(date, char, (200 + strlen(version())), cine_quit())
		sprintf(date, "%s\n2005-2007", version());
	cine_install_procedure("file_pnm_load",
		"loads files of the pnm file format",
		"This plug-in loades PNM files. "
		"The RAW_WIDTH, RAW_HEIGHT, RAW_TYPE and RAW_MAXVAL "
		"variables can be used to load raw data ending with "
		"\".raw\" .",
		"Kai-Uwe Behrmann",
		"2005-2007 Kai-Uwe Behrmann",
		date,
		"<Load>/PNM",
		NULL,
		GIMP_PLUGIN,
		nload_args, nload_return_vals,
		load_args, load_return_vals);
	cine_register_load_handler("file_pnm_load", "pgm,pnm,ppm,pfm,raw", "");
	cine_install_procedure("file_pnm_save",
		"saves files in the pnm file format",
		"This plug-in saves PNM files.",
		"Kai-Uwe Behrmann",
		"2005-2007 Kai-Uwe Behrmann",
		date,
		"<Save>/PNM",
		"RGB*, GRAY*, U16_RGB*, U16_GRAY*",
		GIMP_PLUGIN,
		nsave_args, 0,
		save_args, NULL);
	cine_register_save_handler("file_pnm_save", "pgm,pnm,ppm", "");
	cine_install_procedure("file_pfm_save",
		"saves files in the pfm file format",
		"This plug-in saves PFM files.",
		"Kai-Uwe Behrmann",
		"2005-2007 Kai-Uwe Behrmann",
		date,
		"<Save>/PFM",
		"FLOAT_RGB*, FLOAT_GRAY*",
		GIMP_PLUGIN,
		nsave_args, 0,
		save_args, NULL);
	cine_register_save_handler("file_pfm_save", "pfm", "");
	m_free(date)
}

void
run(gchar* name,
	gint             nparams,
	GimpParam* param,
	gint* nreturn_vals,
	GimpParam** return_vals)
{
	static GimpParam values[2];
	GimpRunModeType run_mode;
	GimpPDBStatusType status = GIMP_PDB_SUCCESS;
	gint32 image_ID;
	run_mode = param[0].data.d_int32;
	*nreturn_vals = 1;
	*return_vals = values;
	values[0].type = GIMP_PDB_STATUS;
	values[0].data.d_status = GIMP_PDB_EXECUTION_ERROR;
	if (strcmp(name, "file_pnm_load") == 0)
	{
		image_ID = load_image(param[1].data.d_string);
		if (image_ID != -1)
		{
			*nreturn_vals = 2;
			values[0].data.d_status = GIMP_PDB_SUCCESS;
			values[1].type = GIMP_PDB_IMAGE;
			values[1].data.d_image = image_ID;
		}
		else
		{
			values[0].data.d_status = GIMP_PDB_EXECUTION_ERROR;
		}
	}
	else if (strcmp(name, "file_pnm_save") == 0)
	{
		switch (run_mode)
		{
		case GIMP_RUN_INTERACTIVE:
		case GIMP_RUN_NONINTERACTIVE:
			/*  Make sure all the arguments are there! */
			if (nparams != 5)
			{
				status = GIMP_PDB_CALLING_ERROR;
			}
			break;
		case GIMP_RUN_WITH_LAST_VALS:
			break;
		default:
			break;
		}
		*nreturn_vals = 1;
		if (save_image(param[3].data.d_string, param[1].data.d_int32,
			param[2].data.d_int32) == 1)
		{
			values[0].data.d_status = GIMP_PDB_SUCCESS;
		}
		else
		{
			values[0].data.d_status = GIMP_PDB_EXECUTION_ERROR;
		}
	}
}

int wread(unsigned char* data, size_t pos, size_t max, size_t* start, size_t* end)
{
	int end_found = 0;
	if (max <= 1) { return 0; }
	while (pos < max && isspace(data[pos])) { ++pos; }
	*start = pos;
	while (pos < max && !end_found)
	{
		if (isspace(data[pos]))
		{
			end_found = 1;
			break;
		}
		else
		{
			++pos;
		}
	}
	*end = pos;
	return end_found;
}

typedef struct PpmData
{
	GImageType    image_type;
	GDrawableType layer_type;
	gchar* name;
	unsigned char* data;
	size_t  mem_n;  /* needed memory in bytes */
	int info_good;
	int type;       /* PNM type */
	int width;
	int height;
	int depth;        /* samples per pixel */
	int byteps;        /* byte per sample */
	double maxval;
	size_t start, end;
	int     fsize;
	size_t  fpos;
	const char* filename;
} PpmData;

int OpenPpm(PpmData* ppmData)
{
	FILE* fp;
	ppmData->name = g_strdup_printf(_("Opening '%s'..."), ppmData->filename);
	cine_progress_init(ppmData->name);
	m_free(ppmData->name);
	fp = fopen(ppmData->filename, "r");
	if (!fp)
	{
		g_message(_("Could not open '%s' for reading: %s"),
			ppmData->filename, g_strerror(errno));
		return FALSE;
	}
	fseek(fp, 0L, SEEK_END);
	ppmData->fsize = ftell(fp);
	rewind(fp);
	m_new(ppmData->data, char, ppmData->fsize, return FALSE)
		ppmData->fpos = fread(ppmData->data, sizeof(char), ppmData->fsize, fp);
	if (ppmData->fpos < ppmData->fsize)
	{
		g_message(_("Could not read '%s' : %s %d/%d"),
			ppmData->filename, g_strerror(errno), ppmData->fsize, (int)ppmData->fpos);
		m_free(ppmData->data)
			fclose(fp);
		return FALSE;
	}
	ppmData->fpos = 0;
	fclose(fp);
	fp = NULL;
	return TRUE;
}
const char* IsInvalidPpm(int type,int is_too_big)
{	switch (type)
	{	default:
			return "PPM contains unknown format";
		case 5:
		case 6:
		case -5:
		case -6:
		case 7:
			if (is_too_big)
			{	return "PPM storage size of is too small";
				return FALSE;
			}
			break;
		case 2:
		case 3:
			return "PPM contains ascii data, which are not handled by this pnm reader";
		case 1:
		case 4:
			return "PPM contains bitmap data, which are not handled by this pnm reader";
	}
	return 0;
}

int ParseHeader(PpmData* ppmData)
{	/* parse Infos */
	ppmData->info_good = 1;
	ppmData->byteps = 1;        /* byte per sample */
	if (ppmData->data[ppmData->fpos] == 'P')
	{
		if (isdigit(ppmData->data[++ppmData->fpos]))
		{
			char tmp[2] = { 0, 0 };
			tmp[0] = ppmData->data[ppmData->fpos];
			ppmData->type = atoi(tmp);
		}
		else if (!isspace(ppmData->data[ppmData->fpos]))
		{
			if (ppmData->data[ppmData->fpos] == 'F') /* PFM rgb */
			{
				ppmData->type = -6;
			}
			else if (ppmData->data[ppmData->fpos] == 'f')  /* PFM gray */
			{
				ppmData->type = -5;
			}
			else
			{
				ppmData->info_good = 0;
			}
		}
		else
		{
			ppmData->info_good = 0;
		}
	}
	ppmData->fpos++;
	/* parse variables */
	{	int in_c = 0;    /* within comment */
	int v_read = 0;  /* number of variables allready read */
	int v_need = 3;  /* number of needed variable; start with three */
	int l_end = 0;   /* line end position */
	int l_pos = 0;   /* line position */
	int l_rdg = 1;   /* line reading */
	if (ppmData->type == 1 || ppmData->type == 4)
	{
		v_need = 2;
	}
	if (ppmData->type == 7) /* pam  */
	{
		v_need = 12;
	}
	while (v_read < v_need && ppmData->info_good)
	{
		l_pos = l_end = ppmData->fpos;
		l_rdg = 1;
		/* read line */
		while (ppmData->fpos < ppmData->fsize && l_rdg)
		{
			if (ppmData->data[ppmData->fpos] == '#')
			{
				in_c = 1;
				l_end = ppmData->fpos - 1;
			}
			else if (ppmData->data[ppmData->fpos] == 10 || ppmData->data[ppmData->fpos] == 13)     /* line break */
			{
				l_rdg = 0;
			}
			else if (ppmData->data[ppmData->fpos] != 0)
			{
				if (!in_c)
				{
					++l_end;
				}
			}
			else
			{
				l_rdg = 0;
			}
			if (!l_rdg)
			{
				in_c = 0;
			}
			++ppmData->fpos;
		}
		/* parse line */
		while (ppmData->info_good &&
			v_read < v_need &&
			l_pos < l_end)
		{
			if (ppmData->info_good)
			{
				double var = -2;
				char var_s[64];
				int l = 0;
				wread(ppmData->data, l_pos, l_end, &ppmData->start, &ppmData->end);
				l = ppmData->end - ppmData->start;
				if (l < 63)
				{
					memcpy(var_s, &ppmData->data[ppmData->start], l);
					var_s[l] = 0;
					var = atof(var_s);
#           ifdef DEBUG
					printf("var = \"%s\"  %d\n", var_s, l);
#           endif
				}
				l_pos = ppmData->end + 1;
				if (ppmData->type == 7)
				{
					if (ppmData->height == -1)
					{
						ppmData->height = (int)var;
					}
					if (ppmData->width == -1)
					{
						ppmData->width = (int)var;
					}
					if (ppmData->depth == -1)
					{
						ppmData->depth = (int)var;
					}
					if (ppmData->maxval == -0.5)
					{
						ppmData->maxval = var;
					}
					if (strcmp(var_s, "HEIGHT") == 0)
					{
						ppmData->height = -1;    /* expecting the next token is the val */
					}
					if (strcmp(var_s, "WIDTH") == 0)
					{
						ppmData->width = -1;
					}
					if (strcmp(var_s, "DEPTH") == 0)
					{
						ppmData->depth = -1;
					}
					if (strcmp(var_s, "MAXVAL") == 0)
					{
						ppmData->maxval = -0.5;
					}
					if (strcmp(var_s, "TUPLTYPE") == 0)
						; /* ?? */
					if (strcmp(var_s, "ENDHDR") == 0)
					{
						v_need = v_read;
					}
				}
				else
				{
					if (!var)
					{
						ppmData->info_good = 0;
					}
					if (v_read == 0)
					{
						ppmData->width = (int)var;
					}
					else if (v_read == 1)
					{
						ppmData->height = (int)var;
					}
					else if (v_read == 2)
					{
						ppmData->maxval = var;
					}
				}
				++v_read;
			}
		}
	}
	}
	if (strstr(strrchr(ppmData->filename, '.') + 1, "raw"))
	{
		ppmData->info_good = 1;
		ppmData->width = atoi(getenv("RAW_WIDTH"));
		ppmData->height = atoi(getenv("RAW_HEIGHT"));
		ppmData->type = atoi(getenv("RAW_TYPE"));
		ppmData->fpos = 0;
		ppmData->maxval = atoi(getenv("RAW_MAXVAL"));
	}
	if (ppmData->info_good)
		switch (ppmData->type)
		{
		case 1:
		case 4:
			ppmData->image_type = GIMP_GRAY;
			ppmData->layer_type = GRAY_IMAGE;
			ppmData->depth = 1;
			ppmData->info_good = 0;
			break;
		case 2:
		case 5:
			if (ppmData->maxval <= 255)
			{
				ppmData->image_type = GIMP_GRAY;
				ppmData->layer_type = GRAY_IMAGE;
				ppmData->byteps = 1;
			}
			else if (ppmData->maxval <= 65535)
			{
				ppmData->image_type = U16_GRAY;
				ppmData->layer_type = U16_GRAY_IMAGE;
				ppmData->byteps = 2;
			}
			ppmData->depth = 1;
			break;
		case 3:
		case 6:
			if (ppmData->maxval <= 255)
			{
				ppmData->image_type = GIMP_RGB;
				ppmData->layer_type = RGB_IMAGE;
				ppmData->byteps = 1;
			}
			else if (ppmData->maxval <= 65535)
			{
				ppmData->image_type = U16_RGB;
				ppmData->layer_type = U16_RGB_IMAGE;
				ppmData->byteps = 2;
			}
			ppmData->depth = 3;
			break;
		case -5:
			ppmData->image_type = FLOAT_GRAY;
			ppmData->layer_type = FLOAT_GRAY_IMAGE;
			ppmData->byteps = 4;
			ppmData->depth = 1;
			break;
		case -6:
			ppmData->byteps = 4;
			ppmData->depth = 3;
			ppmData->image_type = FLOAT_RGB;
			ppmData->layer_type = FLOAT_RGB_IMAGE;
			break;
		case 7: /* pam */
			if (ppmData->maxval == 1.0 || ppmData->maxval == -1.0)
			{
				ppmData->byteps = 4;
				switch (ppmData->depth)
				{
				case 1:
				{	ppmData->image_type = FLOAT_GRAY;
					ppmData->layer_type = FLOAT_GRAY_IMAGE;
				}
				break;
				case 2:
				{	ppmData->image_type = FLOAT_GRAY;
				ppmData->layer_type = FLOAT_GRAYA_IMAGE;
				}
				break;
				case 3:
				{	ppmData->image_type = FLOAT_RGB;
				ppmData->layer_type = FLOAT_RGB_IMAGE;
				}
				break;
				case 4:
				{	ppmData->image_type = FLOAT_RGB;
				ppmData->layer_type = FLOAT_RGBA_IMAGE;
				}
				break;
				}
			}
			else if (ppmData->maxval <= 255)
			{
				ppmData->byteps = 1;
				switch (ppmData->depth)
				{
				case 1:
				{	ppmData->image_type = GIMP_GRAY;
				ppmData->layer_type = GRAY_IMAGE;
				}
				break;
				case 2:
				{	ppmData->image_type = GIMP_GRAY;
				ppmData->layer_type = GRAYA_IMAGE;
				}
				break;
				case 3:
				{	ppmData->image_type = GIMP_RGB;
				ppmData->layer_type = RGB_IMAGE;
				}
				break;
				case 4:
				{	ppmData->image_type = GIMP_RGB;
				ppmData->layer_type = RGBA_IMAGE;
				}
				break;
				}
			}
			else if (ppmData->maxval <= 65535)
			{
				ppmData->byteps = 2;
				switch (ppmData->depth)
				{
				case 1:
				{	ppmData->image_type = U16_GRAY;
				ppmData->layer_type = U16_GRAY_IMAGE;
				}
				break;
				case 2:
				{	ppmData->image_type = U16_GRAY;
				ppmData->layer_type = U16_GRAYA_IMAGE;
				}
				break;
				case 3:
				{	ppmData->image_type = U16_RGB;
				ppmData->layer_type = U16_RGB_IMAGE;
				}
				break;
				case 4:
				{	ppmData->image_type = U16_RGB;
				ppmData->layer_type = U16_RGBA_IMAGE;
				}
				break;
				}
			}
			break;
		default:
			ppmData->info_good = 0;
		}
# ifdef DEBUG
	printf("wxh %dx%d  type: %d|%d|%d   bytes/samples %d %d \n", width, height, ppmData->image_type, ppmData->layer_type, type,
		byteps, depth);
# endif
	if (!ppmData->info_good)
	{
		g_message("%s:%d failed to get info of %s", __FILE__, __LINE__, ppmData->filename);
		m_free(ppmData->data)
			return FALSE;
	}
	/* check if the file can hold the expected data (for raw only) */
	ppmData->mem_n = ppmData->width * ppmData->height * ppmData->byteps * ppmData->depth;
	const char* msg = IsInvalidPpm(ppmData->type, ppmData->mem_n > ppmData->fsize - ppmData->fpos);
	if(msg)
	{	puts(msg);
		m_free(ppmData->data)
		return FALSE;
	}
	return TRUE;
}
gint32
load_image(gchar* filename)
{	GimpDrawable* drawable = NULL;
	gint32  image_ID = 0;
	PpmData ppmData;
	memset(&ppmData, 0, sizeof(ppmData));
	ppmData.filename = filename;
# ifdef DEBUG
	printf("%s:%d %s() 0x%x\n", __FILE__, __LINE__, __func__, (intptr_t)filename);
#endif
	if (!OpenPpm(&ppmData))
	{	return FALSE;
	}
	if (!ParseHeader(&ppmData))
	{
		return FALSE;
	}
	if ((image_ID = cine_image_new((guint)ppmData.width, (guint)ppmData.height,
		ppmData.image_type)) == -1)
	{
		g_message("PNM can't create a new image\n%dx%d %d",
			ppmData.width, ppmData.height, ppmData.image_type);
		m_free(ppmData.data)
			return FALSE;
	}
	cine_image_set_filename(image_ID, filename);
	{	int layer = 0;
	layer = cine_layer_new(image_ID, _("background"),
		(guint)ppmData.width,
		(guint)ppmData.height,
		(guint)ppmData.layer_type,
		100.0, NORMAL_MODE);
	cine_image_add_layer(image_ID, layer, 0);
	drawable = cine_drawable_get(layer);
	}
	{	int   h, p, n_samples, n_bytes;
	int   j_h,         /* jump_height */
		c_h;         /* current_height */
	unsigned char* buf = 0,
		* d_8 = 0;
	unsigned char* src;
	guint16* d_16;
	gfloat* d_f;
	GimpPixelRgn pixel_rgn;
	int byte_swap = 0;
	size_t sn = ppmData.width * get_wire_tile_height() * ppmData.byteps * ppmData.depth;
	cine_pixel_rgn_init(&pixel_rgn, drawable, 0, 0, ppmData.width, ppmData.height, TRUE, FALSE);
	m_new(buf, char, sn, return FALSE)
#   ifdef DEBUG
		printf("wxh %dx%d  bytes|samples: %d|%d buf size: %d\n", width, height, byteps, depth, sn);
#   endif
#if G_BYTE_ORDER == G_LITTLE_ENDIAN
	if ((ppmData.byteps == 2) ||
		(ppmData.maxval > 0 && ppmData.byteps == 4))
	{
		byte_swap = 1;
	}
#else
	if (maxval < 0 && byteps == 4)
	{
		byte_swap = 1;
	}
#endif
	src = &ppmData.data[ppmData.fpos];
	ppmData.maxval = abs(ppmData.maxval);
	c_h = get_wire_tile_height();
	for (j_h = 0; j_h < ppmData.height; j_h += get_wire_tile_height())
	{
		if (j_h + get_wire_tile_height() > ppmData.height)
		{
			c_h = ppmData.height - j_h;
		}
		for (h = 0; h < c_h; ++h)
		{
			n_samples = 1 * ppmData.width * ppmData.depth;
			n_bytes = n_samples * ppmData.byteps;
			d_8 = buf;
			d_16 = (guint16*)buf;
			d_f = (gfloat*)buf;
			/*  TODO 1 bit raw and ascii */
			if (ppmData.type == 1 || ppmData.type == 4)
			{	/*  TODO ascii  */
			}
			else if (ppmData.type == 2 || ppmData.type == 3)
			{	/*  raw and floats */
			}
			else if (ppmData.type == 5 || ppmData.type == 6 ||
				ppmData.type == -5 || ppmData.type == -6 ||
				ppmData.type == 7)
			{
				if (ppmData.byteps == 1)
				{
					d_8 = &src[h * ppmData.width * ppmData.depth * ppmData.byteps];
				}
				else if (ppmData.byteps == 2)
				{
					d_16 = (guint16*)&src[h * ppmData.width * ppmData.depth * ppmData.byteps];
				}
				else if (ppmData.byteps == 4)
				{
					d_f = (gfloat*)&src[h * ppmData.width * ppmData.depth * ppmData.byteps];
				}
				memcpy(&buf[h * ppmData.width * ppmData.depth * ppmData.byteps],
					&src[(j_h + h) * ppmData.width * ppmData.depth * ppmData.byteps],
					1 * ppmData.width * ppmData.depth * ppmData.byteps);
			}
			/* normalise and byteswap */
			if (byte_swap)
			{
				unsigned char* c_buf = &buf[h * ppmData.width * ppmData.depth * ppmData.byteps];
				char  tmp;
				if (ppmData.byteps == 2)            /* 16 bit */
				{
					for (p = 0; p < n_bytes; p += 2)
					{
						tmp = c_buf[p];
						c_buf[p] = c_buf[p + 1];
						c_buf[p + 1] = tmp;
					}
				}
				else if (ppmData.byteps == 4)       /* float */
				{
					for (p = 0; p < n_bytes; p += 4)
					{
						tmp = c_buf[p];
						c_buf[p] = c_buf[p + 3];
						c_buf[p + 3] = tmp;
						tmp = c_buf[p + 1];
						c_buf[p + 1] = c_buf[p + 2];
						c_buf[p + 2] = tmp;
					}
				}
			}
			if (ppmData.byteps == 1 && ppmData.maxval < 255)            /*  8 bit */
			{
				for (p = 0; p < n_samples; ++p)
				{
					d_8[p] = (d_8[p] * 255) / ppmData.maxval;
				}
			}
			else if (ppmData.byteps == 2 && ppmData.maxval < 65535)     /* 16 bit */
			{
				for (p = 0; p < n_samples; ++p)
				{
					d_16[p] = (d_16[p] * 65535) / ppmData.maxval;
				}
			}
			else if (ppmData.byteps == 4 && ppmData.maxval != 1.0)       /* float */
			{
				for (p = 0; p < n_samples; ++p)
				{
					d_f[p] = d_f[p] * ppmData.maxval;
				}
			}
			cine_progress_update((double)(j_h + h) / (double)ppmData.height);
		}
		cine_pixel_rgn_set_rect(&pixel_rgn,
			buf, 0, j_h, ppmData.width, c_h);
	}
	m_free(buf)
	}
	cine_drawable_update(drawable->id, 0, 0, drawable->width, drawable->height);
#if 1
//def FLUSH_DRAWABLE
	cine_drawable_flush(drawable);
	cine_drawable_detach(drawable);
//?	gdisplays_flush();
	//  Crashes, don't:	LogoProgress(NULL, "flush", 100);
#endif
	m_free(ppmData.data);
	return image_ID;
}

gint
	save_image(gchar * filename,
		gint32           image_ID,
		gint32           drawable_ID)
{
	guint    width, height;	/* view sizes */
	gint32* layers;
	gint nlayers;
	FILE* fp = NULL;
# ifdef DEBUG
	printf("%s:%d %s() image_ID: %d\n", __FILE__, __LINE__, __func__, image_ID);
# endif
	layers = cine_image_get_layers(image_ID, &nlayers);
	if (nlayers == -1)
	{
		g_message("no image data to save");
		cine_quit();
	}
	{	int byte_swap = 0;
	int drawable_type;
	int byteps = 1;        /* byte per sample */
	int bps = 8;
	int depth;
	int alpha;
	size_t size;
	char* colourspacename = 0;
	unsigned char* src = NULL;
	GimpPixelRgn pixel_rgn;
	GDrawable* drawable;
	drawable = cine_drawable_get(drawable_ID);
	drawable_type = cine_drawable_type(drawable_ID);
	width = drawable->width;
	height = drawable->height;
	switch (drawable_type)
	{
	case RGB_IMAGE:
		depth = 3;
		bps = 8;
		alpha = 0;
		break;
	case GRAY_IMAGE:
		depth = 1;
		bps = 8;
		alpha = 0;
		break;
	case RGBA_IMAGE:
		depth = 4;
		bps = 8;
		alpha = 1;
		break;
	case GRAYA_IMAGE:
		depth = 2;
		bps = 8;
		alpha = 1;
		break;
	case U16_RGB_IMAGE:
		depth = 3;
		bps = 16;
		alpha = 0;
		break;
	case U16_GRAY_IMAGE:
		depth = 1;
		bps = 16;
		alpha = 0;
		break;
	case U16_RGBA_IMAGE:
		depth = 4;
		bps = 16;
		alpha = 1;
		break;
	case U16_GRAYA_IMAGE:
		depth = 2;
		bps = 16;
		alpha = 1;
		break;
	case FLOAT_RGB_IMAGE:
		depth = 3;
		bps = 32;
		alpha = 0;
		break;
	case FLOAT_GRAY_IMAGE:
		depth = 1;
		bps = 32;
		alpha = 0;
		break;
	case FLOAT_RGBA_IMAGE:
		depth = 4;
		bps = 32;
		alpha = 1;
		break;
	case FLOAT_GRAYA_IMAGE:
		depth = 2;
		bps = 32;
		alpha = 1;
		break;
	default:
		g_print("Can't save this image type\n");
		m_free(drawable)
			return 0;
	}
	byteps = bps / 8;
#if G_BYTE_ORDER == G_LITTLE_ENDIAN
	if ((byteps == 2) ||
		(byteps == 4))
	{
		byte_swap = 1;
	}
#endif
#if 0
	if (alpha)
	{
		g_message(_("Alpha is not supported\n"));
		m_free(drawable)
			return 0;
	}
#endif
	if (cine_image_has_icc_profile(image_ID, ICC_IMAGE_PROFILE))
	{	/*if (strcmp (cine_image_get_icc_profile_color_space_name(image_ID, ICC_IMAGE_PROFILE) , "Rgb")
			!= 0)
		{
			g_message (_("This Colourspace is not supported\n"));
			m_free (drawable)
			return 0;
		}*/
		colourspacename = cine_image_get_icc_profile_description(image_ID, ICC_IMAGE_PROFILE);
	}
	size = width * height * depth * byteps;
	m_new(src, char, size, return FALSE)
		cine_pixel_rgn_init(&pixel_rgn, drawable, 0, 0, width, height, TRUE, FALSE);
	cine_pixel_rgn_get_rect(&pixel_rgn, src, 0, 0, width, height);
	if ((fp = fopen(filename, "wb")) != NULL &&
		src && size)
	{
		size_t pt = 0;
		char text[128];
		int  len = 0;
		int  i = 0;
		char max_val[48];
		fputc('P', fp);
		if (alpha)
		{
			fputc('7', fp);
		}
		else
		{
			if (byteps == 1 ||
				byteps == 2)
			{
				if (depth == 1)
				{
					fputc('5', fp);
				}
				else
				{
					fputc('6', fp);
				}
			}
			else if (byteps == 4)
			{
				if (depth == 1)
				{
					fputc('f', fp);    /* PFM gray */
				}
				else
				{
					fputc('F', fp);    /* PFM rgb */
				}
			}
		}
		fputc('\n', fp);
		{	time_t	cutime;         /* Time since epoch */
		struct tm* gmt;
		gchar time_str[24];
		cutime = time(NULL); /* time right NOW */
		gmt = gmtime(&cutime);
		strftime(time_str, 24, "%Y/%m/%d %H:%M:%S", gmt);
		snprintf(text, 84, "# CREATOR: %s : %s\n",
			version(), time_str);
		}
		len = strlen(text);
		do
		{
			fputc(text[pt++], fp);
		} while (--len);
		pt = 0;
		snprintf(text, 84, "# COLORSPACE: %s\n", colourspacename ?
			colourspacename : "--");
		len = strlen(text);
		do
		{
			fputc(text[pt++], fp);
		} while (--len);
		pt = 0;
		if (byteps == 1)
		{
			snprintf(max_val, 84, "255");
		}
		else if (byteps == 2)
		{
			snprintf(max_val, 84, "65535");
		}
		else if (byteps == 4)
		{
			if (G_BYTE_ORDER == G_LITTLE_ENDIAN)
			{
				snprintf(max_val, 84, "-1.0");
			}
			else
			{
				snprintf(max_val, 84, "1.0");
			}
		}
		else
		{
			g_message("Error: byteps: %d", byteps);
		}
		if (alpha)
		{
			const char* tupl = "RGB_ALPHA";
			if (depth == 2)
			{
				tupl = "GRAYSCALE_ALPHA";
			}
			snprintf(text, 128, "WIDTH %u\nHEIGHT %d\nDEPTH %u\nMAXVAL %s\nTUPLTYPE %s\nENDHDR\n", width, height, depth, max_val, tupl);
			len = strlen(text);
			do
			{
				fputc(text[pt++], fp);
			} while (--len);
			pt = 0;
		}
		else
		{
			snprintf(text, 84, "%d %d\n", width, height);
			len = strlen(text);
			do
			{
				fputc(text[pt++], fp);
			} while (--len);
			pt = 0;
			snprintf(text, 84, "%s\n", max_val);
			len = strlen(text);
			do
			{
				fputc(text[pt++], fp);
			} while (--len);
			pt = 0;
		}
#     ifdef DEBUG
		printf("%s:%d %s() size: %d\n", __FILE__, __LINE__, __func__, (int)size);
#     endif
		do
		{
			if (byte_swap && (byteps == 2))
			{
				if (size % 2)
				{
					fputc(src[pt++ - 1], fp);
				}
				else
				{
					fputc(src[pt++ + 1], fp);
				}
			}
			else
			{
				fputc(src[pt++], fp);
			}
			++i;
		} while (--size);
		pt = 0;
#     ifdef DEBUG
		printf("%s:%d %s() size: %d\n", __FILE__, __LINE__, __func__, i);
#     endif
	}
	if (fp)
	{
		fflush(fp);
		fclose(fp);
	}
	cine_drawable_detach(drawable);
	m_free(src)
	}
	return TRUE;
}

