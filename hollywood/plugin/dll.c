/* dll.c
// Makes plug-in into dll
// Copyright Dec 26, 2002, Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)
*/

#ifdef _WIN32

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include "../pdb/plugin_main.h"
#include "../wire/wire.h"
#include "../pdb/plugin.h"

extern GPlugInInfo plugin_info;

BOOL APIENTRY 
DllMain(HANDLE hModule,DWORD ul_reason_for_call,LPVOID lpReserved)
{    return TRUE;
}

int plugin_main(PluginLoader* plugin) 
{	plugin->plugin_info=&plugin_info; // Points us to plugin functions to load
	return cine_main(plugin);
}

#endif
