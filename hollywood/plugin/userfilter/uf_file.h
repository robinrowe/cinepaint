/*
 * Written 1997,2003 Jens Ch. Restemeier <jrestemeier@currantbun.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifndef _UF_FILE_H
#define _UF_FILE_H

typedef struct _filter_data {
	char title[256];
	char category[256];
	char copyright[256];
	char author[256];
	int map_enable[4];
	char map_label[4][256];
	int control_enable[8];
	char control_label[8][256];
	int control_value[8];
	char source[4][8192];
} s_filter_data;

int load_filter(char *s, s_filter_data *data);
void save_filter(char *s, s_filter_data *data);
void save_filter_afs(char *s, s_filter_data *data);
void save_filter_source(char *s, s_filter_data *data);
void default_filter(s_filter_data *data);

#endif
