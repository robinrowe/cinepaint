# Generic CinePaint plug-in CMakeLists.txt
# Created by Robin Rowe 2020-03-10
# License: Open Source MIT

get_filename_component(dir ${CMAKE_CURRENT_SOURCE_DIR} NAME)
project(plugin-${dir})
message("Configuring ${PROJECT_NAME}...")
set(SOURCES ${dir}.c)
add_library(${PROJECT_NAME} SHARED ${SOURCES})
