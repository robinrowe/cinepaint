/* cine_plugin.h
// Helps CinePaint plug-ins build in CinePaint
// Copyright May 12, 2003, Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)
*/

#ifndef GIMP_PLUGIN_H
#define GIMP_PLUGIN_H

#ifdef _WIN32
#include "../pdb/regex.h"
#endif
#include "../i18n.h"
#include "../pdb/plugin_main.h"
#include "../wire/enums.h"
#include "gimp1.2.h"
#include "../pdb/ui.h"
#include "../i18n.h"
#include "../pdb/dialog.h"
#include "../libgimp/stdplugins-intl.h"
#include "../pdb/float16.h"
#include "../wire/iodebug.h"

#define MAIN() 
#define PLUG_IN_INFO plugin_info
#define gimp_run_procedure2 cine_run_procedure2
#define gimp_query_database cine_query_database
#define gimp_query_procedure cine_query_procedure
#define gimp_run_procedure cine_run_procedure
#define gimp_destroy_params cine_destroy_params
#define gimp_install_temp_proc cine_install_temp_proc
#define gimp_drawable_get cine_drawable_get
#define gimp_drawable_mask_bounds cine_drawable_mask_bounds
#define gimp_drawable_detach cine_drawable_detach
#define gimp_drawable_color cine_drawable_color
#define gimp_drawable_gray cine_drawable_gray
#define gimp_displays_flush cine_displays_flush
#define gimp_pixel_rgn_init cine_pixel_rgn_init
#define gimp_drawable_bpp cine_drawable_bpp
#define gimp_drawable_has_alpha cine_drawable_has_alpha
#define gimp_drawable_type cine_drawable_type
#define gimp_pixel_rgn_get_row cine_pixel_rgn_get_row
#define gimp_color_cube cine_color_cube
#define gimp_drawable_channel cine_drawable_channel
#define gimp_drawable_fill cine_drawable_fill
#define gimp_drawable_width cine_drawable_width
#define gimp_drawable_height cine_drawable_height
#define gimp_gamma cine_gamma
#define gimp_gtkrc cine_gtkrc
#define gimp_image_add_layer cine_image_add_layer
#define gimp_image_delete cine_image_delete
#define gimp_image_new cine_image_new
#define gimp_image_resize cine_image_resize
#define gimp_install_cmap cine_install_cmap
#define gimp_layer_new cine_layer_new
#define gimp_layer_resize cine_layer_resize
#define gimp_layer_set_name cine_layer_set_name
#define gimp_palette_get_background cine_palette_get_background
#define gimp_palette_get_foreground cine_palette_get_foreground
#define gimp_palette_set_background cine_palette_set_background
#define gimp_palette_set_foreground cine_palette_set_foreground
#define gimp_pixel_rgn_get_rect cine_pixel_rgn_get_rect
#define gimp_tile_width get_wire_tile_width
#define gimp_tile_height get_wire_tile_height
//#define gimp_tile_width get_wire_tile_width
//#define gimp_tile_height get_wire_tile_height
//#define gimp_tile_cache_ntiles cine_tile_cache_ntiles
//#define get_wire_tile_width get_wire_tile_width
//#define get_wire_tile_height get_wire_tile_height
#define cine_tile_cache_ntiles get_wire_tile_cache_ntiles
#define cine_tile_ref wire_tile_ref
#define cine_tile_unref wire_tile_unref
#define GetDirHome GetPathHome
#define gimp_uninstall_temp_proc cine_uninstall_temp_proc
#define gimp_uninstall_temp_proc cine_uninstall_temp_proc
#define gimp_image_menu_new cine_image_menu_new
#define gimp_drawable_menu_new cine_drawable_menu_new
#define gimp_layer_menu_new cine_layer_menu_new
#define gimp_channel_menu_new cine_channel_menu_new
#define gimp_use_xshm cine_use_xshm
#define gimp_quit cine_quit
#define gimp_get_data cine_get_data
#define gimp_set_data cine_set_data
#define gimp_layer_get_name cine_layer_get_name
#define gimp_strcompress cine_strcompress
#define gimp_strescape cine_strescape
#define gimp_progress_init cine_progress_init
#define gimp_undo_push_group_start cine_undo_push_group_start
#define gimp_drawable_precision cine_drawable_precision
#define gimp_pixel_rgns_register cine_pixel_rgns_register
#define gimp_pixel_rgns_process cine_pixel_rgns_process
#define gimp_drawable_flush cine_drawable_flush
#define gimp_drawable_merge_shadow cine_drawable_merge_shadow
#define gimp_drawable_update cine_drawable_update
#define gimp_pixel_rgn_set_row cine_pixel_rgn_set_row
#define gimp_progress_update cine_progress_update
#define gimp_query_boolean_box cine_query_boolean_box
#define gimp_standard_help_func cine_standard_help_func
#define gimp_path_free cine_path_free
#define gimp_gimprc_query cinerc_query
#define gimp_drawable_is_indexed cine_drawable_indexed
#define gimp_image_get_selection cine_image_get_selection
#define gimp_drawable_image_id cine_drawable_image_id
#define gimp_drawable_offsets cine_drawable_offsets
#define gimp_pixel_rgn_get_col cine_pixel_rgn_get_col
#define gimp_register_magic_load_handler cine_register_magic_load_handler
#define gimp_register_save_handler cine_register_save_handler
#define gimp_ui_init cine_ui_init
#define gimp_export_image cine_export_image
#define gimp_image_set_cmap cine_image_set_cmap
#define gimp_image_set_filename cine_image_set_filename
#define gimp_pixel_rgn_set_rect cine_pixel_rgn_set_rect
#define gimp_image_get_cmap cine_image_get_cmap
#define gimp_help_init cine_help_init
#define gimp_path_parse cine_path_parse
#define gimp_main cine_main
#define gimp_dialog_new cine_dialog_new
#define gimp_table_attach_aligned cine_table_attach_aligned
#define gimp_toggle_button_update cine_toggle_button_update
#define gimp_random_seed_new cine_random_seed_new
#define gimp_radio_group_new cine_radio_group_new
#define gimp_procedural_db_proc_info procedural_db_proc_info
#define gimp_radio_button_update cine_radio_button_update
#define GimpImage GImage
#define GimpValueArray GimpParam 
#define GType int
#define GimpPlugIn GtkObject
#define GimpPlugInClass GtkObjectClass
#define GimpRunMode GRunModeType
#define GimpProcedure PluginProc

typedef const char* GFile;

#ifdef GFIG_CPP_STRINGS
#define gimp_airbrush cine_airbrush
#define gimp_brushes_get_brush cine_brushes_get_brush
#define gimp_brushes_list cine_brushes_list
#define gimp_brushes_set_brush cine_brushes_set_brush
#define gimp_bucket_fill cine_bucket_fill
#define gimp_clone cine_clone
#define gimp_edit_clear cine_edit_clear
#define gimp_edit_stroke cine_edit_stroke
#define gimp_ellipse_select cine_ellipse_select
#define gimp_free_select cine_free_select
#define gimp_gimprc_query cine_cinerc_query
#define gimp_layer_copy cine_layer_copy
#define gimp_paintbrush cine_paintbrush
#define gimp_pencil cine_pencil
#define gimp_selection_clear cine_selection_clear
#endif

/*
#define gimp_quit
gimp_install_procedure
gimp_register_load_handler
gimp_register_save_handler
gimp_get_data
gimp_set_data
gimp_progress_init
gimp_image_new
gimp_image_set_filename
gimp_image_set_icc_profile_by_mem
gimp_image_set_lab_profile
gimp_image_resize
gimp_image_set_cmap
gimp_layer_new
gimp_image_add_layer
gimp_drawable_get
gimp_channel_new
gimp_image_add_channel
gimp_layer_set_offsets
gimp_layer_set_visible
gimp_drawable_flush
gimp_drawable_detach
gimp_pixel_rgn_init
gimp_pixel_rgn_set_rect
gimp_progress_update
gimp_tile_height
gimp_pixel_rgn_get_rect
gimp_image_get_layers
gimp_drawable_type
gimp_image_width
gimp_image_height
*/

/* Don't include this file, but use it as a guide.
Do the OPPOSITE of what it does to rename back to older interface we use.
#include "gimpcompat.h"
*/
#endif

