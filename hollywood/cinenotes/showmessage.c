/*
 * File: showmessage.c
 * Auth: Eric Harlow
 */

#include <gtk/gtk.h>



/*
 * --- CloseDialog
 *
 * Routine to close the about dialog window.
 */
void CloseShowMessage (GtkWidget *widget, gpointer data)
{
    GtkWidget *dialog_widget = (GtkWidget *) data;

    /* --- Close the widget --- */
    gtk_widget_destroy (dialog_widget);
}



/*
 * ClearShowMessage
 *
 * Release the window "grab" 
 * Clear out the global dialog_window since that
 * is checked when the dialog is brought up.
 */
void ClearShowMessage (GtkWidget *widget, gpointer data)
{
    gtk_grab_remove (widget);
}

#if 0
/**
 * gtk_label_set_selectable: (attributes org.gtk.Method.set_property=selectable)
 * @self: a `GtkLabel`
 * @setting: %TRUE to allow selecting text in the label
 *
 * Makes text in the label selectable.
 *
 * Selectable labels allow the user to select text from the label,
 * for copy-and-paste.
 */
void
gtk_label_set_selectable(GtkLabel* self,
    gboolean  setting)
{
    gboolean old_setting;

    g_return_if_fail(GTK_IS_LABEL(self));

    setting = setting != FALSE;
    old_setting = self->select_info && self->select_info->selectable;

    if (setting)
    {
        gtk_label_ensure_select_info(self);
        self->select_info->selectable = TRUE;
        gtk_label_update_cursor(self);
    }
    else
    {
        if (old_setting)
        {
            /* unselect, to give up the selection */
            gtk_label_select_region(self, 0, 0);

            self->select_info->selectable = FALSE;
            gtk_label_clear_select_info(self);
        }
    }
    if (setting != old_setting)
    {
        g_object_freeze_notify(G_OBJECT(self));
        g_object_notify_by_pspec(G_OBJECT(self), label_props[PROP_SELECTABLE]);
        g_object_thaw_notify(G_OBJECT(self));
        gtk_widget_queue_draw(GTK_WIDGET(self));
    }
}

static void set_label_selectable(gpointer data, gpointer user_data)
{
    GtkWidget* widget = GTK_WIDGET(data);

    if (GTK_IS_LABEL(widget))
    {
        gtk_label_set_selectable(GTK_LABEL(widget), TRUE);
    }
}

static void set_lables_selectable(GtkWidget* dialog)
{
    GtkWidget* area = gtk_message_dialog_get_message_area(
        GTK_MESSAGE_DIALOG(dialog));
    GtkContainer* box = (GtkContainer*)area;

    GList* children = gtk_container_get_children(box);
    g_list_foreach(children, set_label_selectable, NULL);
    g_list_free(children);
}

#endif
/*
 * ShowMessage
 *
 * Show a popup message to the user.
 */
void ShowMessage (char *szTitle, char *szMessage)
{
    GtkWidget *label;
    GtkWidget *button;
    GtkWidget *dialog_window;

    /* --- Create a dialog window --- */
    dialog_window = gtk_dialog_new ();

    gtk_signal_connect (GTK_OBJECT (dialog_window), "destroy",
              GTK_SIGNAL_FUNC (ClearShowMessage),
              NULL);

    /* --- Set the title and add a border --- */
    gtk_window_set_title (GTK_WINDOW (dialog_window), szTitle);
    gtk_container_border_width (GTK_CONTAINER (dialog_window), 0);

    /* --- Create an "Ok" button with the focus --- */
    button = gtk_button_new_with_label ("OK");

    gtk_signal_connect (GTK_OBJECT (button), "clicked",
              GTK_SIGNAL_FUNC (CloseShowMessage),
              dialog_window);

    /* --- Default the "Ok" button --- */
    GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog_window)->action_area), 
              button, TRUE, TRUE, 0);
    gtk_widget_grab_default (button);
    gtk_widget_show (button);

    /* --- Create a descriptive label --- */
    label = gtk_label_new (szMessage);

    /* --- Put some room around the label text --- */
    gtk_misc_set_padding (GTK_MISC (label), 10, 10);

    /* --- Add label to designated area on dialog --- */
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog_window)->vbox), 
              label, TRUE, TRUE, 0);
//    set_labels_selectable(dialog_window);
    /* --- Show the label --- */
    gtk_widget_show (label);


    /* --- Show the dialog --- */
    gtk_widget_show (dialog_window);

    /* --- Only this window can have actions done. --- */
    gtk_grab_add (dialog_window);
}

