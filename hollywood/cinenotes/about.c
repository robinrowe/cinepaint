/*
 * About.c
 */

#include <gtk/gtk.h>
#include "notepad.h"
#include "version.h"


/*
 * ShowAbout
 *
 * Show the about dialog.  Reuse existing code.
 */
void ShowAbout ()
{
#if 1
    ShowMessage ("About...", NOTES_PROGRAM_TITLE "\n");
#else
    ShowMessage ("About...",
                  "GtkNotepad v.07\n - "
                  "Eric Harlow\n");
#endif
}
