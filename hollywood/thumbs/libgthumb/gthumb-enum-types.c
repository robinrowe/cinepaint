
/* Generated data (by glib-mkenums) */

#include <glib-object.h>


/* enumerations from "image-list.h" */
#include "image-list.h"
GType
gthumb_cursor_movement_get_type (void)
{
  static GType etype = 0;
  if (etype == 0) {
    static const GEnumValue values[] = {
      { GTHUMB_CURSOR_MOVE_UP, "GTHUMB_CURSOR_MOVE_UP", "up" },
      { GTHUMB_CURSOR_MOVE_DOWN, "GTHUMB_CURSOR_MOVE_DOWN", "down" },
      { GTHUMB_CURSOR_MOVE_RIGHT, "GTHUMB_CURSOR_MOVE_RIGHT", "right" },
      { GTHUMB_CURSOR_MOVE_LEFT, "GTHUMB_CURSOR_MOVE_LEFT", "left" },
      { GTHUMB_CURSOR_MOVE_PAGE_UP, "GTHUMB_CURSOR_MOVE_PAGE_UP", "page-up" },
      { GTHUMB_CURSOR_MOVE_PAGE_DOWN, "GTHUMB_CURSOR_MOVE_PAGE_DOWN", "page-down" },
      { GTHUMB_CURSOR_MOVE_BEGIN, "GTHUMB_CURSOR_MOVE_BEGIN", "begin" },
      { GTHUMB_CURSOR_MOVE_END, "GTHUMB_CURSOR_MOVE_END", "end" },
      { 0, NULL, NULL }
    };
    etype = g_enum_register_static ("GthumbCursorMovement", values);
  }
  return etype;
}

GType
gthumb_selection_change_get_type (void)
{
  static GType etype = 0;
  if (etype == 0) {
    static const GEnumValue values[] = {
      { GTHUMB_SELCHANGE_NONE, "GTHUMB_SELCHANGE_NONE", "none" },
      { GTHUMB_SELCHANGE_SET, "GTHUMB_SELCHANGE_SET", "set" },
      { GTHUMB_SELCHANGE_SET_RANGE, "GTHUMB_SELCHANGE_SET_RANGE", "set-range" },
      { 0, NULL, NULL }
    };
    etype = g_enum_register_static ("GthumbSelectionChange", values);
  }
  return etype;
}


/* Generated data ends here */

