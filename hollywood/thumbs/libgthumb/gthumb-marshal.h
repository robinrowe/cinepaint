
#ifndef __gthumb_marshal_MARSHAL_H__
#define __gthumb_marshal_MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS

/* VOID:FLOAT (gthumb-marshal.list:1) */
#define gthumb_marshal_VOID__FLOAT	g_cclosure_marshal_VOID__FLOAT

/* VOID:INT,BOXED (gthumb-marshal.list:2) */
extern void gthumb_marshal_VOID__INT_BOXED (GClosure     *closure,
                                            GValue       *return_value,
                                            guint         n_param_values,
                                            const GValue *param_values,
                                            gpointer      invocation_hint,
                                            gpointer      marshal_data);

/* VOID:ENUM,BOOLEAN (gthumb-marshal.list:3) */
extern void gthumb_marshal_VOID__ENUM_BOOLEAN (GClosure     *closure,
                                               GValue       *return_value,
                                               guint         n_param_values,
                                               const GValue *param_values,
                                               gpointer      invocation_hint,
                                               gpointer      marshal_data);

/* VOID:ENUM,ENUM (gthumb-marshal.list:4) */
extern void gthumb_marshal_VOID__ENUM_ENUM (GClosure     *closure,
                                            GValue       *return_value,
                                            guint         n_param_values,
                                            const GValue *param_values,
                                            gpointer      invocation_hint,
                                            gpointer      marshal_data);

/* VOID:POINTER,POINTER (gthumb-marshal.list:5) */
extern void gthumb_marshal_VOID__POINTER_POINTER (GClosure     *closure,
                                                  GValue       *return_value,
                                                  guint         n_param_values,
                                                  const GValue *param_values,
                                                  gpointer      invocation_hint,
                                                  gpointer      marshal_data);

/* VOID:STRING (gthumb-marshal.list:6) */
#define gthumb_marshal_VOID__STRING	g_cclosure_marshal_VOID__STRING

/* VOID:VOID (gthumb-marshal.list:7) */
#define gthumb_marshal_VOID__VOID	g_cclosure_marshal_VOID__VOID

/* BOOL:BOOL (gthumb-marshal.list:8) */
extern void gthumb_marshal_BOOLEAN__BOOLEAN (GClosure     *closure,
                                             GValue       *return_value,
                                             guint         n_param_values,
                                             const GValue *param_values,
                                             gpointer      invocation_hint,
                                             gpointer      marshal_data);
#define gthumb_marshal_BOOL__BOOL	gthumb_marshal_BOOLEAN__BOOLEAN

/* BOOL:ENUM (gthumb-marshal.list:9) */
extern void gthumb_marshal_BOOLEAN__ENUM (GClosure     *closure,
                                          GValue       *return_value,
                                          guint         n_param_values,
                                          const GValue *param_values,
                                          gpointer      invocation_hint,
                                          gpointer      marshal_data);
#define gthumb_marshal_BOOL__ENUM	gthumb_marshal_BOOLEAN__ENUM

/* BOOL:INT,POINTER (gthumb-marshal.list:10) */
extern void gthumb_marshal_BOOLEAN__INT_POINTER (GClosure     *closure,
                                                 GValue       *return_value,
                                                 guint         n_param_values,
                                                 const GValue *param_values,
                                                 gpointer      invocation_hint,
                                                 gpointer      marshal_data);
#define gthumb_marshal_BOOL__INT_POINTER	gthumb_marshal_BOOLEAN__INT_POINTER

/* BOOL:VOID (gthumb-marshal.list:11) */
extern void gthumb_marshal_BOOLEAN__VOID (GClosure     *closure,
                                          GValue       *return_value,
                                          guint         n_param_values,
                                          const GValue *param_values,
                                          gpointer      invocation_hint,
                                          gpointer      marshal_data);
#define gthumb_marshal_BOOL__VOID	gthumb_marshal_BOOLEAN__VOID

G_END_DECLS

#endif /* __gthumb_marshal_MARSHAL_H__ */

