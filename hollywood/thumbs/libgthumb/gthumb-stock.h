/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  GThumb
 *
 *  Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#ifndef GTHUMB_STOCK_H
#define GTHUMB_STOCK_H

#define GTHUMB_STOCK_ADD_COMMENT          "gthumb-add-comment"
#define GTHUMB_STOCK_BRIGHTNESS_CONTRAST  "gthumb-brightness-contrast"
#define GTHUMB_STOCK_COLOR_BALANCE        "gthumb-color-balance"
#define GTHUMB_STOCK_DESATURATE           "gthumb-desaturate"
#define GTHUMB_STOCK_FLIP                 "gthumb-flip"
#define GTHUMB_STOCK_FULLSCREEN           "gthumb-fullscreen"
#define GTHUMB_STOCK_HUE_SATURATION       "gthumb-hue-saturation"
#define GTHUMB_STOCK_INVERT               "gthumb-invert"
#define GTHUMB_STOCK_MIRROR               "gthumb-mirror"
#define GTHUMB_STOCK_NORMAL_VIEW          "gthumb-normal-view"
#define GTHUMB_STOCK_POSTERIZE            "gthumb-posterize"
#define GTHUMB_STOCK_PROPERTIES           "gthumb-properties"
#define GTHUMB_STOCK_RESIZE               "gthumb-resize"
#define GTHUMB_STOCK_ROTATE_90            "gthumb-rotate-90"
#define GTHUMB_STOCK_ROTATE_90_CC         "gthumb-rotate-90-cc"
#define GTHUMB_STOCK_SLIDESHOW            "gthumb-slideshow"
#define GTHUMB_STOCK_TRANSFORM            "gthumb-transform"


void gthumb_stock_init (void);


#endif /* GTHUMB_STOCK_H */
