
/* Generated data (by glib-mkenums) */

#ifndef GTHUMB_ENUM__TYPES_H
#define GTHUMB_ENUM_TYPES_H

#include <glib-object.h>

G_BEGIN_DECLS

/* enumerations from "image-list.h" */

GType gthumb_cursor_movement_get_type (void);
#define GTHUMB_TYPE_CURSOR_MOVEMENT (gthumb_cursor_movement_get_type())

GType gthumb_selection_change_get_type (void);
#define GTHUMB_TYPE_SELECTION_CHANGE (gthumb_selection_change_get_type())

G_END_DECLS

#endif /* GTHUMB_ENUM_TYPES_H */

/* Generated data ends here */

