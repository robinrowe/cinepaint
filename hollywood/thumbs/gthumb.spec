%define release 7
%define prefix  /usr
%define name	gthumb
%define version 1.105

Summary:	An image viewer and browser for GNOME.
Name:		%{name}
Version:    	%{version}
Release:	%{release}
Copyright:	GPL
Vendor:		GNOME
URL:		http://gthumb.sourceforge.net/
Group:		Applications/Multimedia
Source0:	%{name}-%{version}.tar.gz
Packager:       Paolo Bacchilega <paolo.bacch@tin.it>	
BuildRoot:	%{_builddir}/%{name}-%{version}-root
Requires:	libpng
Requires:       glib2 >= 2.0.0
Requires:       gtk2 >= 2.0.0
Requires:	libxml2 >= 2.4.0
Requires:	libgnome >= 2.0.0
Requires:	libgnomeui >= 2.0.0
Requires:	gnome-vfs2 >= 2.0.0
Requires:	libglade2 >= 2.0.0
Requires:	libgnomeprint >= 1.110
Requires:	libgnomeprintui >= 1.110
Requires:	bonobo-activation >= 1.0.0
Requires:	libbonobo >= 2.0.0
Requires:	libbonoboui >= 2.0.0
BuildRequires:	libpng-devel
BuildRequires:	glib2-devel >= 2.0.0
BuildRequires:	gtk2-devel >= 2.0.0
BuildRequires:	libxml2-devel >= 2.4.0
BuildRequires:	libgnome-devel >= 2.0.0
BuildRequires:	libgnomeui-devel >= 2.0.0
BuildRequires:	gnome-vfs2-devel >= 2.0.0
BuildRequires:	libglade2-devel >= 2.0.0
BuildRequires:	libgnomeprint-devel >= 1.110
BuildRequires:	libgnomeprintui-devel >= 1.110
BuildRequires:	bonobo-activation-devel >= 1.0.0
BuildRequires:	libbonobo-devel >= 2.0.0
BuildRequires:	libbonoboui-devel >= 2.0.0
Docdir:         %{prefix}/share/doc

%description
gThumb lets you browse your hard disk, showing you thumbnails of image files. 
It also lets you view single files (including GIF animations), add comments to
images, organize images in catalogs, print images, view slideshows, set your
desktop background, and more. 

%prep
%setup 

%build
%configure
make

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_bindir}/gthumb
%{_libexecdir}/gthumb-image-viewer
%{_libexecdir}/gthumb-catalog-view
%{_datadir}/applications/gthumb.desktop
%{_datadir}/gnome-2.0/ui/*.xml
%{_datadir}/gthumb/glade/*.glade
%{_datadir}/gthumb/icons/*.xpm
%{_datadir}/locale/*/LC_MESSAGES/gthumb-2.0.mo
%{_datadir}/application-registry/gthumb.applications
%{_libdir}/bonobo/servers/*.server
%{_datadir}/pixmaps/gthumb.png
%doc AUTHORS NEWS README COPYING
%doc %{_datadir}/man/man1/gthumb.1*
%doc %{_datadir}/gnome/help/gthumb
