#ifndef CINEPAINT_HALF_H
#define CINEPAINT_HALF_H

#include "../dll_api.h"

typedef unsigned short ImfHalf;

#ifdef __cplusplus
extern "C" {
#endif

DLL_API ImfHalf	ImfFloat2Half (float f);
DLL_API float ImfHalf2Float (ImfHalf h);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* CINEPAINT_HALF_H */
