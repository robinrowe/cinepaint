#!/bin/bash
# CinePaint/Hollywood/install_src.sh
# Copyright 2021/05/05 Robin.Rowe@cinepaint.org
# License open source MIT

VERSION=$(<version.txt)
srcpath=./install/CinePaint-${VERSION}-win32
srcdir=${srcpath}-src
copy=cp

dirs=(app cinenotes cinepaint1 data depth libgimp libhalf liblcms pdb plugin po po-plug-ins win32 wire)

echo mkdir ${srcdir} 
mkdir ${srcdir} 

echo ${copy} LICENSE ${srcdir} 
${copy} LICENSE ${srcdir} 
echo ${copy} *.md ${srcdir} 
${copy} *.md ${srcdir} 
echo ${copy} *.txt ${srcdir} 
${copy} *.txt ${srcdir} 

for dir in "${dirs[@]}"
do
	echo ${copy} -R ${dir}/ ${srcdir}
	${copy} -R ${dir}/ ${srcdir}
done

echo du ${srcdir} -sh
du ${srcdir} -sh

echo scp ${srcpath}*.zip robinrowe@frs.sourceforge.net:/home/frs/project/cinepaint/CinePaint
