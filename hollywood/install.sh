#!/bin/bash
# CinePaint/Hollywood/install.sh
# Copyright 2021/02/02 Robin.Rowe@cinepaint.org
# License open source MIT

#BUILD=Debug
BUILD=Release
BUILD_PATH=./build/Win32/${BUILD}
REDIST_PATH=./install/x86/Microsoft.VC142.CRT
# From "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Redist\MSVC\14.28.29325\x86\Microsoft.VC142.CRT"

VERSION=$(<version.txt)
INSTALL_PATH=./install/CinePaint-${VERSION}-Win32

echo "CinePaint VERSION = ${VERSION}"
echo "BUILD_PATH = ${BUILD_PATH}"
echo "INSTALL_PATH = ${INSTALL_PATH}"
mkdir -p ${INSTALL_PATH}
echo "Copying CinePaint1 to ${INSTALL_PATH}"
cp ${BUILD_PATH}/cinepaint*.exe ${INSTALL_PATH}
echo "Copying plugins to ${INSTALL_PATH}"
cp ${BUILD_PATH}/*.dll ${INSTALL_PATH}
echo "Copying redist to ${INSTALL_PATH}"
cp ${REDIST_PATH}/* ${INSTALL_PATH}
echo "Copying data to ${INSTALL_PATH}"
cp data/cinepaintrc ${INSTALL_PATH}
cp plugin/plugins.cmake ${INSTALL_PATH}
cp *.md ${INSTALL_PATH}
cp -r data ${INSTALL_PATH}
cp -r po ${INSTALL_PATH}
ls ${INSTALL_PATH}
 


#powershell Compress-Archive D:\Build\FolderName D:\Build\FolderName.zip
