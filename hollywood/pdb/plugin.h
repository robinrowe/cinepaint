/* CinePaint -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __PLUG_IN_H__
#define __PLUG_IN_H__

#include <time.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <stdbool.h>
#include "../pdb/procedural_db.h"
#include "../app/tag.h"
#include "../wire/enums.h"
#include "../wire/wirebuffer.h"
#include "../wire/c_typedefs.h"
#include "../dll_api.h"
#include "../wire/protocol.h"

typedef
struct PluginProc
{	char* plugin_name;
	char* menu_path;
	char* accelerator;
	char* extensions;
	char* prefixes;
	char* magics;
	char* image_types;
	int   image_types_val;
	ProcData proc_data;
	GSList* extensions_list;
	GSList* prefixes_list;
	GSList* magics_list;
} PluginProc;

typedef
struct PluginInternal
{	char* plugin_name;
	char* filename;
	GSList* proc_list;//PluginProc*
	time_t mtime;
	int query;
	PluginMain plugin_main;
	void* library;
} PluginInternal;

#define PLUG_IN_ARGS 7

typedef
struct PluginLoader
{
  unsigned int is_open : 1;                 /* Is the plug-in open */
  unsigned int is_destroy : 1;              /* Should the plug-in by destroyed */
  unsigned int is_query : 1;                /* Are we querying the plug-in? */
  unsigned int is_synchronous : 1;          /* Is the plug-in running synchronously or not */
  unsigned int is_recurse : 1;              /* Have we called 'gtk_main' recursively? */
  unsigned int is_busy : 1;                 /* Is the plug-in busy with a temp proc? */
  PluginInternal* internal;
  const char* name;
  char *args[PLUG_IN_ARGS];              /* Plug-ins command line arguments */
  const char* function_name;
  void* function_proc;
  GPlugInInfo* plugin_info;
//  WireBuffer* wire_buffer;
/*  GSList *temp_proc_defs; */        
  GtkWidget *progress;              
  GtkWidget *progress_label;
  GtkWidget *progress_bar;
} PluginLoader;

DLL_API void plugin_handle_message(WireMessage* msg);

/* Initialize the plug-ins */
void plugin_init (void);
#ifdef _WIN32
DWORD WINAPI plugin_init_thread(void*);
#endif
/* Kill all running plug-ins */
void plugin_kill (void);

/* Add a plug-in to the list of valid plug-ins
 *  and query the plug-in for information if
 *  necessary.
 */
void plugin_add (char *name,
		  char *menu_path,
		  char *accelerator);

/* Get the "image_types" the plug-in works on.
 */
char* plugin_image_types (char *name);

/* Add in the file load/save handler fields procedure.
 */
struct PluginProc* plugin_file_handler (char *name,
				     char *extensions,
				     char *prefixes,
				     char *magics);

/* Add a plug-in definition.
 */
void plugin_def_add (struct PluginInternal *internal);

/* Retrieve a plug-ins menu path
 */
char* plugin_menu_path (char *name);

/* Create a new plug-in structure */
int CreatePlugin(char *name);

/* Destroy a plug-in structure. This will close the plug-in
 *  first if necessary.
 */
void DestroyPluginExternal (struct PluginLoader *plugin);

/* Open a plug-in. This cause the plug-in to run.
 * If returns 1, you must destroy the plugin.  If returns 0 you
 * may not destroy the plugin.
 */
int OpenPluginExternal (struct PluginLoader *plugin);

/* Close a plug-in. This kills the plug-in and releases
 *  its resources.
 */
void ClosePluginExternal (struct PluginLoader *plugin,
		    int     kill_it);

/* Run a plug-in as if it were a procedure database procedure */
Argument* plugin_run (ProcData *proc_rec,
		       Argument   *args,
		       int         synchronous,
		       int         destroy_values);

/* Run the last plug-in again with the same arguments. Extensions
 *  are exempt from this "privelege".
 */
void plugin_repeat (int with_interface);

/* Set the sensitivity for plug-in menu items based on the image
 *  base type.
 */
void plugin_set_menu_sensitivity (Tag t);

/* Register an internal plug-in.  This is for file load-save
 * handlers, which are organized around the plug-in data structure.
 * This could all be done a little better, but oh well.  -josh
 */
void plugin_add_internal (struct PluginProc* procs);
GSList* plugin_extensions_parse  (char     *extensions);
int     plugin_image_types_parse (char     *image_types);

gint  tag_to_plugin_image_type ( Tag t );
void  plugin_proc_def_print (struct PluginProc* procs); 
PluginInternal* GetPlugin(const char* plugin_name);
void FreeProcDef(struct PluginProc* pluginProcedure);
DLL_API void plugin_handle_proc_install(GPProcInstall* proc_install);
DLL_API bool plugin_set_proc_return(GPProcReturn* proc_return);
void PrintScanline(const unsigned char* p, int width, int bpp,int is_verbose);
void  ZebraStripe(guchar* data, int height, int width, int bpp);
DLL_API void plugin_copyarea(PixelArea* pixelArea,guchar* buf,gint direction);

#if 0
#define RGB_IMAGE       0x01
#define GRAY_IMAGE      0x02
#define INDEXED_IMAGE   0x04
#define RGBA_IMAGE      0x08
#define GRAYA_IMAGE     0x10
#define INDEXEDA_IMAGE  0x20
#define U16_RGB_IMAGE       0x40
#define U16_GRAY_IMAGE      0x80
#define U16_RGBA_IMAGE      0x100 
#define U16_GRAYA_IMAGE     0x200
#define FLOAT_RGB_IMAGE       0x400
#define FLOAT_GRAY_IMAGE      0x800
#define FLOAT_RGBA_IMAGE      0x1000 
#define FLOAT_GRAYA_IMAGE     0x2000
#define FLOAT16_RGB_IMAGE       0x4000
#define FLOAT16_GRAY_IMAGE      0x8000
#define FLOAT16_RGBA_IMAGE      0x10000 
#define FLOAT16_GRAYA_IMAGE     0x20000
#define BFP_RGB_IMAGE       0x40000
#define BFP_GRAY_IMAGE      0x80000
#define BFP_RGBA_IMAGE      0x100000 
#define BFP_GRAYA_IMAGE     0x200000
#endif
#endif /* __PLUG_IN_H__ */
