/* LIBGIMP - CinePaint Library
 * Copyright (C) 1995-2000 Peter Mattis and Spencer Kimball
 *
 * gimpimage.h
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __IMAGE_H__
#define __IMAGE_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "../dll_api.h"

/* For information look into the C source or the html documentation */

#define cine_image_convert_rgb        cine_convert_rgb
#define cine_image_convert_grayscale  cine_convert_grayscale
#define cine_image_convert_indexed    cine_convert_indexed
#define cine_image_duplicate          cine_channel_ops_duplicate

/*
guchar   * cine_image_get_cmap           (gint32  image_ID,
					  gint   *num_colors);
gboolean   cine_image_set_cmap           (gint32  image_ID,
					  guchar *cmap,
					  gint    num_colors);
*/
DLL_API guchar   * cine_image_get_thumbnail_data (gint32  image_ID,
					  gint   *width,
					  gint   *height,
					  gint   *bpp);
  

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __IMAGE_H__ */
