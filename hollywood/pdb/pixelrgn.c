/* LIBGIMP - CinePaint Library                                                   
 * Copyright (C) 1995-1997 Peter Mattis and Spencer Kimball                
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.             
 *                                                                              
 * This library is distributed in the hope that it will be useful,              
 * but WITHOUT ANY WARRANTY; without even the implied warranty of               
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU            
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */                                                                             

#include <string.h>
#include <stdarg.h>
#include "../pdb//plugin_main.h"
#include "../app/app.h"
#include "../app/drawable.h"
#include "../app/gdisplay.h"
#include "../app/pixelarea.h"
#include "../pdb/plugin.h"

// #define VERBOSE_TILE_FILL
#define BOUNDS(a,x,y)  ((a < x) ? x : ((a > y) ? y : a))
#define MEMCPY_IS_FAST

typedef struct GPixelRgnHolder    GPixelRgnHolder;
typedef struct GPixelRgnIterator  GPixelRgnIterator;

struct GPixelRgnHolder
{
  GPixelRgn *pr;
  guchar *original_data;
  int startx;
  int starty;
  int count;
};

struct GPixelRgnIterator
{
  GSList *pixel_regions;
  int region_width;
  int region_height;
  int portion_width;
  int portion_height;
  int process_count;
};


static int      cine_get_portion_width    (GPixelRgnIterator *pri);
static int      cine_get_portion_height   (GPixelRgnIterator *pri);
static gpointer cine_pixel_rgns_configure (GPixelRgnIterator *pri);
static void     cine_pixel_rgn_configure  (GPixelRgnHolder   *prh,
					   GPixelRgnIterator *pri);

void
cine_pixel_rgn_init (GPixelRgn *pr,
		     TileDrawable *drawable,
		     int       x,
		     int       y,
		     int       width,
		     int       height,
		     int       dirty,
		     int       shadow)
{
  pr->data = NULL;
  pr->drawable = drawable;
  pr->bpp = drawable->bpp;
  pr->rowstride = pr->bpp * LIB_TILE_WIDTH;
  pr->x = x;
  pr->y = y;
  pr->w = width;
  pr->h = height;
  pr->dirty = dirty;
  pr->shadow = shadow;
}

void
cine_pixel_rgn_resize (GPixelRgn *pr,
		       int       x,
		       int       y,
		       int       width,
		       int       height)
{
  if (pr->data != NULL)
    pr->data += ((y - pr->y) * pr->rowstride +
		 (x - pr->x) * pr->bpp);

  pr->x = x;
  pr->y = y;
  pr->w = width;
  pr->h = height;
}

void
cine_pixel_rgn_get_pixel (GPixelRgn *pr,
			  guchar   *buf,
			  int       x,
			  int       y)
{
  GTile *tile;
  guchar *tile_data;
  unsigned b;

  tile = cine_drawable_get_tile2 (pr->drawable, pr->shadow, x, y);
  wire_tile_ref (tile);

  tile_data = tile->data + tile->bpp * (tile->ewidth * (y % LIB_TILE_HEIGHT) + (x % LIB_TILE_WIDTH));

  for (b = 0; b < tile->bpp; b++)
    *buf++ = *tile_data++;

  wire_tile_unref_free(tile, FALSE);
}

void
cine_pixel_rgn_get_row (GPixelRgn *pr,
			guchar   *buf,
			int       x,
			int       y,
			int       width)
{
  GTile *tile;
  guchar *tile_data;
  int bpp, inc, min;
  int end;
  int boundary;
#ifndef MEMCPY_IS_FAST
  int b;
#endif

  end = x + width;

  while (x < end)
    {
      tile = cine_drawable_get_tile2 (pr->drawable, pr->shadow, x, y);
      wire_tile_ref (tile);

      tile_data = tile->data + (int)tile->bpp * (int)(tile->ewidth * (int)(y % LIB_TILE_HEIGHT) + (x % LIB_TILE_WIDTH));
      boundary = x + (tile->ewidth - (x % LIB_TILE_WIDTH));
      bpp = tile->bpp;

#ifdef MEMCPY_IS_FAST
      memcpy ((void *)buf,
	      (const void *)tile_data,
	      inc = (bpp *
		     ( (min = MIN(end,boundary)) -x) ) );
      x = min;;
      buf += inc;
#else
      for ( ; x < end && x < boundary; x++)
	{
	  for (b = 0; b < tile->bpp; b++)
	    *buf++ = tile_data[b];
	  tile_data += bpp;
	}
#endif /* MEMCPY_IS_FAST */

      wire_tile_unref_free(tile, FALSE);
    }
}

void
cine_pixel_rgn_get_col (GPixelRgn *pr,
			guchar   *buf,
			int       x,
			int       y,
			int       height)
{
  GTile *tile;
  guchar *tile_data;
  int inc;
  int end;
  int boundary;
  unsigned b;

  end = y + height;

  while (y < end)
    {
      tile = cine_drawable_get_tile2 (pr->drawable, pr->shadow, x, y);
      wire_tile_ref (tile);

      tile_data = tile->data + tile->bpp * (tile->ewidth * (y % LIB_TILE_HEIGHT) + (x % LIB_TILE_WIDTH));
      boundary = y + (tile->eheight - (y % LIB_TILE_HEIGHT));
      inc = tile->bpp * tile->ewidth;

      for ( ; y < end && y < boundary; y++)
	{
	  for (b = 0; b < tile->bpp; b++)
	    *buf++ = tile_data[b];
	  tile_data += inc;
	}

      wire_tile_unref_free (tile, FALSE);
    }
}

void
cine_pixel_rgn_get_rect (GPixelRgn *pr,
			 guchar   *buf,
			 int       x,
			 int       y,
			 int       width,
			 int       height)
{
  GTile *tile;
  guchar *src, *dest;
  gulong bufstride;
  int xstart, ystart;
  int xend, yend;
  int xboundary;
  int yboundary;
  int xstep, ystep;
  int ty, bpp;
#ifndef MEMCPY_IS_FAST
  int b, tx;
#endif

  bpp = pr->bpp;
  bufstride = bpp * width;

  xstart = x;
  ystart = y;
  xend = x + width;
  yend = y + height;
  ystep = 0;

  while (y < yend)
    {
      x = xstart;
      while (x < xend)
	{
	  tile = cine_drawable_get_tile2 (pr->drawable, pr->shadow, x, y);
	  wire_tile_ref (tile);

	  xstep = tile->ewidth - (x % LIB_TILE_WIDTH);
	  ystep = tile->eheight - (y % LIB_TILE_HEIGHT);
	  xboundary = x + xstep;
	  yboundary = y + ystep;
	  xboundary = MIN (xboundary, xend);
	  yboundary = MIN (yboundary, yend);

	  for (ty = y; ty < yboundary; ty++)
	    {
	      src = tile->data + tile->bpp * (tile->ewidth * (ty % LIB_TILE_HEIGHT) + (x % LIB_TILE_WIDTH));
	      dest = buf + bufstride * (ty - ystart) + bpp * (x - xstart);

#ifdef MEMCPY_IS_FAST
	      memcpy ((void *)dest, (const void *)src, (xboundary-x)*bpp);  
#else   
	      for (tx = x; tx < xboundary; tx++)
		{
		  for (b = 0; b < bpp; b++)
		    *dest++ = *src++;
		}
#endif /* MEMCPY_IS_FAST */

	    }

	  wire_tile_unref_free (tile, FALSE);
	  x += xstep;
	}

      y += ystep;
    }
}

void
cine_pixel_rgn_set_pixel (GPixelRgn *pr,
			  guchar   *buf,
			  int       x,
			  int       y)
{
  GTile *tile;
  guchar *tile_data;
  unsigned b;

  tile = cine_drawable_get_tile2 (pr->drawable, pr->shadow, x, y);
  wire_tile_ref (tile);

  tile_data = tile->data + tile->bpp * (tile->ewidth * (y % LIB_TILE_HEIGHT) + (x % LIB_TILE_WIDTH));

  for (b = 0; b < tile->bpp; b++)
    *tile_data++ = *buf++;

  wire_tile_unref_free (tile, TRUE);
}

void
cine_pixel_rgn_set_row (GPixelRgn *pr,
			guchar   *buf,
			int       x,
			int       y,
			int       width)
{
  GTile *tile;
  guchar *tile_data;
  int inc, min;
  int end;
  int boundary;
#ifndef MEMCPY_IS_FAST
  int b;
#endif

  end = x + width;

  while (x < end)
    {
      tile = cine_drawable_get_tile2 (pr->drawable, pr->shadow, x, y);
      wire_tile_ref (tile);

      tile_data = tile->data + tile->bpp * (tile->ewidth * (y % LIB_TILE_HEIGHT) + (x % LIB_TILE_WIDTH));
      boundary = x + (tile->ewidth - (x % LIB_TILE_WIDTH));

#ifdef MEMCPY_IS_FAST
      memcpy ((void *)tile_data,
	      (const void *)buf,
	      inc = (tile->bpp *
		     ( (min = MIN(end,boundary)) -x) ) );
      x = min;
      buf += inc;
#else
      for ( ; x < end && x < boundary; x++)
	{
	  for (b = 0; b < tile->bpp; b++)
	    *tile_data++ = *buf++;
	}
#endif /* MEMCPY_IS_FAST */

      wire_tile_unref_free (tile, TRUE);
    }
}

void
cine_pixel_rgn_set_col (GPixelRgn *pr,
			guchar   *buf,
			int       x,
			int       y,
			int       height)
{
  GTile *tile;
  guchar *tile_data;
  int inc;
  int end;
  int boundary;
  unsigned b;

  end = y + height;

  while (y < end)
    {
      tile = cine_drawable_get_tile2 (pr->drawable, pr->shadow, x, y);
      wire_tile_ref (tile);

      tile_data = tile->data + tile->bpp * (tile->ewidth * (y % LIB_TILE_HEIGHT) + (x % LIB_TILE_WIDTH));
      boundary = y + (tile->eheight - (y % LIB_TILE_HEIGHT));
      inc = tile->bpp * tile->ewidth;

      for ( ; y < end && y < boundary; y++)
	{
	  for (b = 0; b < tile->bpp; b++)
	    tile_data[b] = *buf++;
	  tile_data += inc;
	}

      wire_tile_unref_free (tile, TRUE);
    }
}

void
cine_pixel_rgn_set_rect (GPixelRgn *pr,
			 guchar   *buf,
			 int       x,
			 int       y,
			 int       width,
			 int       height)
{
  GTile *tile;
  guchar *src, *dest;
  gulong bufstride;
  int xstart, ystart;
  int xend, yend;
  int xboundary;
  int yboundary;
  int xstep, ystep;
  int ty, bpp;
#ifndef MEMCPY_IS_FAST
  int b, tx;
#endif

  bpp = pr->bpp;
  bufstride = bpp * width;


  xstart = x;
  ystart = y;
  xend = x + width;
  yend = y + height;
  ystep = 0;

  while (y < yend)
    {
//	  PrintScanline(buf + bufstride * y, bufstride, bpp);
//	  puts("raw buffer");
      x = xstart;
      while (x < xend)
	{
	  tile = cine_drawable_get_tile2 (pr->drawable, pr->shadow, x, y);// tiles created, tile->data = NULL
#if 0
	  tile->data = g_malloc(tile->eheight * tile->ewidth * tile->bpp);
	  ZebraStripe(tile->data, tile->eheight, tile->ewidth, tile->bpp);
#endif
	  wire_tile_ref (tile);// Allocates tile->data, but blank

	  xstep = tile->ewidth - (x % LIB_TILE_WIDTH);
	  ystep = tile->eheight - (y % LIB_TILE_HEIGHT);
	  xboundary = x + xstep;
	  yboundary = y + ystep;
	  xboundary = MIN (xboundary, xend);
	  yboundary = MIN (yboundary, yend);

	  for (ty = y; ty < yboundary; ty++)
	    { int offset = bufstride * (ty - ystart) + bpp * (x - xstart);
	      src = buf + offset;
			offset = tile->bpp * (tile->ewidth * (ty % LIB_TILE_HEIGHT) + (x % LIB_TILE_WIDTH));
	      dest = tile->data + offset;

#ifdef MEMCPY_IS_FAST
	      memcpy ((void *)dest, (const void *)src, (xboundary-x)*bpp); 
#else
	      for (tx = x; tx < xboundary; tx++)
		{
		  for (b = 0; b < bpp; b++)
		    *dest++ = *src++;
		}
#endif /* MEMCPY_IS_FAST */
	    }
#ifdef VERBOSE_TILE_FILL
	  d_printf("Filled tile #%i (%i)\n", tile->tile_num, tile->ref_count);
#endif
#define CANVAS_REPAINT
#ifdef CANVAS_REPAINT
	  CanvasDrawable* drawable = drawable_get_ID(tile->drawable->id);
	  Canvas* canvas = drawable_data(drawable);
	  if (!canvas)
	  {
		  return;
	  }
	  canvas_portion_refro(canvas, x, y);
	  if (!canvas_portion_data(canvas, x, y))
	  {
		  return;
	  }
#ifdef VERBOSE_TILE_FILL
	  d_printf(" x = %d, y = %d, width = %d, height = %d \n", x,y,width,height);
#endif
	  PixelArea pixelArea;
	  memset(&pixelArea, 0, sizeof(pixelArea));
	  pixelarea_init(&pixelArea, canvas, x, y, tile->ewidth, tile->eheight, FALSE);
	  plugin_copyarea(&pixelArea,tile->data, COPY_BUFFER_TO_AREA);
	  canvas_portion_unref(canvas, x, y);
//	  drawable_invalidate_preview(drawable);
#endif
#define NO_UNREF
#ifndef NO_UNREF
	  wire_tile_unref_free(tile, TRUE);
#endif
	  x += xstep;
	}

      y += ystep;
    }
#ifdef VERBOSE_TILE_FILL
	d_printf("Filled tiles\n");
#endif
}

gpointer
cine_pixel_rgns_register(int nrgns,
			  ...)
{
  GPixelRgn *pr;
  GPixelRgnHolder *prh;
  GPixelRgnIterator *pri;
  va_list ap;
  int found;

  pri = g_new (GPixelRgnIterator, 1);
  pri->pixel_regions = NULL;
  pri->process_count = 0;

  if (nrgns < 1)
    return NULL;

  va_start (ap, nrgns);

  found = FALSE;
  while (nrgns --)
    {
      pr = va_arg (ap, GPixelRgn *);
      prh = g_new (GPixelRgnHolder, 1);
      prh->pr = pr;

      if (pr != NULL)
	{
	  /*  If there is a defined value for data, make sure tiles is NULL  */
	  if (pr->data)
	    pr->drawable = NULL;
	  prh->original_data = pr->data;
	  prh->startx = pr->x;
	  prh->starty = pr->y;
	  prh->pr->process_count = 0;

	  if (!found)
	    {
	      found = TRUE;
	      pri->region_width = pr->w;
	      pri->region_height = pr->h;
	    }
	}

      /*  Add the pixel Rgn holder to the list  */
      pri->pixel_regions = g_slist_prepend (pri->pixel_regions, prh);
    }

  va_end (ap);

  return cine_pixel_rgns_configure (pri);
}

gpointer
cine_pixel_rgns_process (gpointer pri_ptr)
{
  GSList *list;
  GPixelRgnHolder *prh;
  GPixelRgnIterator *pri;

  pri = (GPixelRgnIterator*) pri_ptr;
  pri->process_count++;

  /*  Unref all referenced tiles and increment the offsets  */

  list = pri->pixel_regions;
  while (list)
    {
      prh = (GPixelRgnHolder*) list->data;
      list = list->next;

      if ((prh->pr != NULL) && ((gint) prh->pr->process_count != pri->process_count))
	{
	  /*  This eliminates the possibility of incrementing the
	   *  same region twice
	   */
	  prh->pr->process_count++;

	  /*  Unref the last referenced tile if the underlying region is a tile manager  */
	  if (prh->pr->drawable)
	    {
	      GTile *tile = cine_drawable_get_tile2 (prh->pr->drawable, prh->pr->shadow,
						    prh->pr->x, prh->pr->y);
	      wire_tile_unref_free (tile, prh->pr->dirty);
	    }

	  prh->pr->x += pri->portion_width;

	  if ((gint)(prh->pr->x - prh->startx) >= pri->region_width)
	    {
	      prh->pr->x = prh->startx;
	      prh->pr->y += pri->portion_height;
	    }
	}
    }

  return cine_pixel_rgns_configure (pri);
}


static int
cine_get_portion_width (GPixelRgnIterator *pri)
{
  GSList *list;
  GPixelRgnHolder *prh;
  int min_width = G_MAXINT;
  int width;

  /* Find the minimum width to the next vertical tile (in the case of a tile manager)
   * or to the end of the pixel region (in the case of no tile manager)
   */

  list = pri->pixel_regions;
  while (list)
    {
      prh = (GPixelRgnHolder *) list->data;

      if (prh->pr)
        {
          /* Check if we're past the point of no return  */
          if ((gint)(prh->pr->x - prh->startx) >= pri->region_width)
            return 0;

          if (prh->pr->drawable)
            {
              width = LIB_TILE_WIDTH - (prh->pr->x % LIB_TILE_WIDTH);
              width = BOUNDS (width, 0,(gint) (pri->region_width - (prh->pr->x - prh->startx)));
            }
          else
            width = (pri->region_width - (prh->pr->x - prh->startx));

          if (width < min_width)
            min_width = width;
        }

      list = list->next;
    }

  return min_width;
}

static int
cine_get_portion_height (GPixelRgnIterator *pri)
{
  GSList *list;
  GPixelRgnHolder *prh;
  int min_height = G_MAXINT;
  int height;

  /* Find the minimum height to the next vertical tile (in the case of a tile manager)
   * or to the end of the pixel region (in the case of no tile manager)
   */

  list = pri->pixel_regions;
  while (list)
    {
      prh = (GPixelRgnHolder *) list->data;

      if (prh->pr)
        {
          /* Check if we're past the point of no return  */
          if ((gint)(prh->pr->y - prh->starty) >= pri->region_height)
            return 0;

          if (prh->pr->drawable)
            {
              height = LIB_TILE_HEIGHT - (prh->pr->y % LIB_TILE_HEIGHT);
              height = BOUNDS (height, 0,(gint) (pri->region_height - (prh->pr->y - prh->starty)));
            }
          else
            height = (pri->region_height - (prh->pr->y - prh->starty));

          if (height < min_height)
            min_height = height;
        }

      list = list->next;
    }

  return min_height;
}

static gpointer
cine_pixel_rgns_configure (GPixelRgnIterator *pri)
{
  GPixelRgnHolder *prh;
  GSList *list;

  /*  Determine the portion width and height  */
  pri->portion_width = cine_get_portion_width (pri);
  pri->portion_height = cine_get_portion_height (pri);

  if ((pri->portion_width == 0) ||
      (pri->portion_height == 0))
    {
      /*  free the pixel regions list  */
      if (pri->pixel_regions)
        {
          list = pri->pixel_regions;
          while (list)
            {
              g_free (list->data);
              list = list->next;
            }
          g_slist_free (pri->pixel_regions);
          g_free (pri);
        }

      return NULL;
    }

  pri->process_count++;

  list = pri->pixel_regions;
  while (list)
    {
      prh = (GPixelRgnHolder *) list->data;

      if ((prh->pr != NULL) && ((gint) prh->pr->process_count != pri->process_count))
        {
          prh->pr->process_count++;
          cine_pixel_rgn_configure (prh, pri);
        }

      list = list->next;
    }

  return pri;
}

static void
cine_pixel_rgn_configure (GPixelRgnHolder   *prh,
			  GPixelRgnIterator *pri)
{
  /* Configure the rowstride and data pointer for the pixel region
   * based on the current offsets into the region and whether the
   * region is represented by a tile manager or not
   */
  if (prh->pr->drawable)
    {
      GTile *tile;
      int offx, offy;

      tile = cine_drawable_get_tile2 (prh->pr->drawable, prh->pr->shadow, prh->pr->x, prh->pr->y);
      wire_tile_ref (tile);

      offx = prh->pr->x % LIB_TILE_WIDTH;
      offy = prh->pr->y % LIB_TILE_HEIGHT;

      prh->pr->rowstride = tile->ewidth * prh->pr->bpp;
      prh->pr->data = tile->data + offy * prh->pr->rowstride + offx * prh->pr->bpp;
    }
  else
    {
      prh->pr->data = (prh->original_data +
		       (prh->pr->y - prh->starty) * prh->pr->rowstride +
		       (prh->pr->x - prh->startx) * prh->pr->bpp);
    }

  prh->pr->w = pri->portion_width;
  prh->pr->h = pri->portion_height;
}
