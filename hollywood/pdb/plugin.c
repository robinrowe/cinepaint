/* CinePaint -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "config.h"
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <dlfcn.h>
#include <stdbool.h>
#include "../app/tile_manager.h"
#include "../wire/libtile.h"
#ifdef _WIN32
#include "../cinepaint1/extra.h"
#else
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>
#endif
#ifdef HAVE_IPC_H
#include <sys/ipc.h>
#endif
#ifdef BUILD_SHM
#include <sys/shm.h>
extern int use_shm;
#endif

#include "../wire/protocol.h"
#include "../wire/wire.h"
#include "../wire/enums.h"
#include "../app/app_procs.h"
#include "../app/appenv.h"
#include "../app/canvas.h"
#include "../app/drawable.h"
#include "../app/datafiles.h"
#include "../app/errors.h"
#include "../app/gdisplay.h"
#include "../app/general.h"
#include "../app/gimage.h"
#include "../app/rc.h"
#include "../app/interface.h"
#include "../app/menus.h"
#include "../app/pixelarea.h"
#include "plugin.h"
#include "../app/tag.h"
#include "../app/layout.h"
#include "../app/minimize.h"
#include "../wire/plugin_loader.h"
#include "../wire/wirebuffer.h"
#include "../wire/taskswitch.h"
#include "../wire/event.h"
#include "version.h"
#include "i18n.h"
#include "../pdb/regex.h"
#include "../pdb/float16.h"
#include "../app/app.h"
#include "../pdb/plugin_main.h"

//#define DEBUG_SCANLINE
//#define DEBUG_ADD_PLUGIN
#define DEBUG_PLUGIN_BLURB
//#define DEBUG_BUFFER_COPY
//#define DEBUG_FILE_HANDLER
//#define DEBUG_WIRE_TABLES

//#define DEBUG_PROCRUN
#define FIXME
#define index strchr
/* rsr: can't run sep progress bar -- event starvation */
//#define SEPARATE_PROGRESS_BAR
typedef struct PlugInBlocked  PlugInBlocked;

/*typedef enum
{
  RUN_INTERACTIVE    = 0x0,
  RUN_NONINTERACTIVE = 0x1,
  RUN_WITH_LAST_VALS = 0x2
} RunModeType;*/

struct PlugInBlocked
{	PluginLoader *plugin;
	char *proc_name;
};

void plugin_copyarea(PixelArea         *pixelArea,
                             guchar            *buf,
                             gint               direction);
static void plugin_push(PluginLoader            *plugin);
static void plugin_pop(void);
static void plugin_recv_message(gpointer           data,
                                 gint               id,
                                 GdkInputCondition  cond);

static void plugin_handle_quit(void);
static void plugin_handle_tile_req(GPTileReq         *tile_req);
bool plugin_handle_proc_run(GPProcRun         *proc_run);
bool plugin_set_proc_return(GPProcReturn      *proc_return);


static void plugin_handle_proc_uninstall(GPProcUninstall   *proc_uninstall);
static int plugin_write_rc(const char              *filename);
int CreatePlugin(char              *filename);
char* CreatePluginName(const char* filename);
static int plugin_query(char              *filename,
                          PluginInternal         *plugin);
static void plugin_add_to_db(void);
static void plugin_make_menu(void);
void plugin_make_image_menu(GtkItemFactory *fac, char *buf);
static void plugin_callback(GtkWidget         *widget,
                             gpointer           client_data);
static void plugin_proc_def_insert(PluginProc     *proc);
static void plugin_proc_def_remove(PluginProc     *proc);
static void plugin_proc_def_destroy(PluginProc     *proc,
                                     int                data_only);

static Argument* plugin_temp_run(ProcData *proc_rec,
                                  Argument   *args);
static Argument* plugin_params_to_args(GParam   *params,
                                        int        nparams,
                                        int        full_copy);
static GParam*  plugin_args_to_params(Argument  *args,
                                        int        nargs,
                                        int        full_copy);
static void      plugin_params_destroy(GParam   *params,
                                        int        nparams,
                                        int        full_destroy);
static void      plugin_args_destroy(Argument  *args,
                                      int        nargs,
                                      int        full_destroy);

static Argument* progress_init_invoker(Argument *args);
static Argument* progress_update_invoker(Argument *args);

static Argument* message_invoker(Argument *args);

static Argument* message_handler_get_invoker(Argument *args);
static Argument* message_handler_set_invoker(Argument *args);

static ProcArg progress_init_args[] =
{	{	PDB_STRING,
		"message",
		"Message to use in the progress dialog."
	}
};

static ProcData progress_init_proc =
{	"cine_progress_init",
	"Initializes the progress bar for the current plug-in",
	"Initializes the progress bar for the current plug-in. It is only valid to call this procedure from a plug-in.",
	"Spencer Kimball & Peter Mattis",
	"Spencer Kimball & Peter Mattis",
	"1995-1996",
	PDB_INTERNAL,
	1,
	progress_init_args,
	0,
	NULL,
	{ { progress_init_invoker } },
};

static ProcArg progress_update_args[] =
{	{	PDB_FLOAT,
		"percentage",
		"Percentage of progress completed"
	}
};

static ProcData progress_update_proc =
{	"cine_progress_update",
	"Updates the progress bar for the current plug-in",
	"Updates the progress bar for the current plug-in. It is only valid to call this procedure from a plug-in.",
	"Spencer Kimball & Peter Mattis",
	"Spencer Kimball & Peter Mattis",
	"1995-1996",
	PDB_INTERNAL,
	1,
	progress_update_args,
	0,
	NULL,
	{ { progress_update_invoker } },
};


static ProcArg message_args[] =
{	{	PDB_STRING,
		"message",
		"Message to display in the dialog."
	}
};

static ProcData message_proc =
{	"cine_message",
	"Displays a dialog box with a message",
	"Displays a dialog box with a message. Useful for status or error reporting.",
	"Spencer Kimball & Peter Mattis",
	"Spencer Kimball & Peter Mattis",
	"1995-1996",
	PDB_INTERNAL,
	1,
	message_args,
	0,
	NULL,
	{ { message_invoker } },
};


static ProcArg message_handler_get_out_args[] =
{	{	PDB_INT32,
		"handler",
		"the current handler type: { MESSAGE_BOX (0), CONSOLE (1) }"
	}
};

static ProcData message_handler_get_proc =
{	"cine_message_handler_get",
	"Returns the current state of where warning messages are displayed.",
	"This procedure returns the way g_message warnings are displayed. They can be shown in a dialog box or printed on the console where CinePaint was started.",
	"Manish Singh",
	"Manish Singh",
	"1998",
	PDB_INTERNAL,
	0,
	NULL,
	1,
	message_handler_get_out_args,
	{ { message_handler_get_invoker } },
};

static ProcArg message_handler_set_args[] =
{	{	PDB_INT32,
		"handler",
		"the new handler type: { MESSAGE_BOX (0), CONSOLE (1) }"
	}
};

static ProcData message_handler_set_proc =
{	"cine_message_handler_set",
	"Controls where warning messages are displayed.",
	"This procedure controls how g_message warnings are displayed. They can be shown in a dialog box or printed on the console where CinePaint was started.",
	"Manish Singh",
	"Manish Singh",
	"1998",
	PDB_INTERNAL,
	1,
	message_handler_set_args,
	0,
	NULL,
	{ { message_handler_set_invoker } },
};


static int
match_strings(regex_t *preg,
              gchar   *a)
{	return regexec(preg, a, 0, NULL, 0);
}

static ProcData temp_PDB_name_proc;
static ProcData plugins_query_proc;
static ProcData plugin_domain_register_proc;
static ProcData plugin_help_register_proc;

static Argument *
temp_PDB_name_invoker(Argument *args)
{	Argument *return_args;
	gchar *temp_name;
	static gint proc_number = 0;
	temp_name = g_strdup_printf("temp_plugin_number_%d", proc_number++);
	return_args = procedural_db_return_args(&temp_PDB_name_proc, TRUE);
	return_args[1].value.pdb_pointer = temp_name;
	return return_args;
}

static ProcArg temp_PDB_name_outargs[] =
{	{	PDB_STRING,
		"temp_name",
		"A unique temporary name for a temporary PDB entry"
	}
};

static ProcData temp_PDB_name_proc =
{	"cine_temp_PDB_name",
	"Generates a unique temporary PDB name.",
	"This procedure generates a temporary PDB entry name that is guaranteed to be unique. It is many used by the interactive popup dialogs to generate a PDB entry name.",
	"Andy Thomas",
	"Andy Thomas",
	"1998",
	PDB_INTERNAL,
	0,
	NULL,
	1,
	temp_PDB_name_outargs,
	{ { temp_PDB_name_invoker } }
};

static Argument *
plugins_query_invoker(Argument *args)
{	App* app = GetApp();
	GSList *plugin_list = GetInternalPlugins();
	Argument *return_args;
	gchar *search_str;
	gint32 num_plugins = 0;
	gchar **menu_strs;
	gchar **accel_strs;
	gchar **prog_strs;
	gchar **types_strs;
	gint32 *time_ints;
	gchar **realname_strs;
	PluginProc *plugin_proc;
	GSList *proc_list = NULL;
	gint i = 0;
	regex_t sregex;
	memset(&sregex,0,sizeof(sregex));
	search_str = (gchar *) args[0].value.pdb_pointer;
	if(search_str && strlen(search_str))
	{ regcomp(&sregex, search_str, REG_ICASE); }
	else
	{ search_str = NULL; }
	/* count number of plugin entries, then allocate 4 arrays of correct size
	 * where we can store the strings.
	 */
	proc_list = plugin_list;
	while(proc_list)
	{	plugin_proc = proc_list->data;
		proc_list = proc_list->next;
		if(plugin_proc->plugin_name && plugin_proc->menu_path)
		{	gchar *name = strrchr(plugin_proc->menu_path, '/');
			if(name)
			{ name = name + 1; }
			else
			{ name = plugin_proc->menu_path; }
			if(search_str && match_strings(&sregex, name))
			{ continue; }
			num_plugins++;
		}
	}
	menu_strs     = g_new(gchar *, num_plugins);
	accel_strs    = g_new(gchar *, num_plugins);
	prog_strs     = g_new(gchar *, num_plugins);
	types_strs    = g_new(gchar *, num_plugins);
	realname_strs = g_new(gchar *, num_plugins);
	time_ints     = g_new(gint   , num_plugins);
	proc_list = plugin_list;
	while(proc_list)
	{	if(i > num_plugins)
		{ g_error("Internal error counting plugins"); }
		plugin_proc = proc_list->data;
		proc_list = proc_list->next;
		if(plugin_proc->plugin_name && plugin_proc->menu_path)
		{	ProcData *pr = &plugin_proc->proc_data;
			gchar *name = strrchr(plugin_proc->menu_path, '/');
			if(name)
			{ name = name + 1; }
			else
			{ name = plugin_proc->menu_path; }
			if(search_str && match_strings(&sregex,name))
			{ continue; }
			menu_strs[i]     = g_strdup(plugin_proc->menu_path);
			accel_strs[i]    = g_strdup(plugin_proc->accelerator);
			prog_strs[i]     = g_strdup(plugin_proc->plugin_name);//bug?
			types_strs[i]    = g_strdup(plugin_proc->image_types);
			realname_strs[i] = g_strdup(pr->name);
#if 0
			time_ints[i]     = plugin_proc->mtime;
#endif
			i++;
		}
	}
	/* This I hope frees up internal stuff */
	if(search_str)
	{ free(sregex.buffer); }
	return_args = procedural_db_return_args(&plugins_query_proc, TRUE);
	return_args[1].value.pdb_int = num_plugins;
	return_args[2].value.pdb_pointer = menu_strs;
	return_args[3].value.pdb_int = num_plugins;
	return_args[4].value.pdb_pointer = accel_strs;
	return_args[5].value.pdb_int = num_plugins;
	return_args[6].value.pdb_pointer = prog_strs;
	return_args[7].value.pdb_int = num_plugins;
	return_args[8].value.pdb_pointer = types_strs;
	return_args[9].value.pdb_int = num_plugins;
	return_args[10].value.pdb_pointer = time_ints;
	return_args[11].value.pdb_int = num_plugins;
	return_args[12].value.pdb_pointer = realname_strs;
	return return_args;
}

static ProcArg plugins_query_inargs[] =
{	{	PDB_STRING,
		"search_string",
		"If not an empty string then use this as a search pattern"
	}
};

static ProcArg plugins_query_outargs[] =
{	{	PDB_INT32,
		"num_plugins",
		"The number of plugins"
	},
	{	PDB_STRINGARRAY,
		"menu_path",
		"The menu path of the plugin"
	},
	{	PDB_INT32,
		"num_plugins",
		"The number of plugins"
	},
	{	PDB_STRINGARRAY,
		"plugin_accelerator",
		"String representing keyboard accelerator (could be empty string)"
	},
	{	PDB_INT32,
		"num_plugins",
		"The number of plugins"
	},
	{	PDB_STRINGARRAY,
		"plugin_location",
		"Location of the plugin program"
	},
	{	PDB_INT32,
		"num_plugins",
		"The number of plugins"
	},
	{	PDB_STRINGARRAY,
		"plugin_image_type",
		"Type of image that this plugin will work on"
	},
	{	PDB_INT32,
		"num_plugins",
		"The number of plugins"
	},
	{	PDB_INT32ARRAY,
		"plugin_install_time",
		"Time that the plugin was installed"
	},
	{	PDB_INT32,
		"num_plugins",
		"The number of plugins"
	},
	{	PDB_STRINGARRAY,
		"plugin_real_name",
		"The internal name of the plugin"
	}
};

static ProcData plugins_query_proc =
{	"cine_plugins_query",
	"Queries the plugin database for its contents.",
	"This procedure queries the contents of the plugin database.",
	"Andy Thomas",
	"Andy Thomas",
	"1998",
	PDB_INTERNAL,
	1,
	plugins_query_inargs,
	12,
	plugins_query_outargs,
	{ { plugins_query_invoker } }
};

static Argument *
plugin_domain_register_invoker(Argument *args)
{	gboolean success = TRUE;
	gchar *domain_name;
	gchar *domain_path;
	PluginInternal *plugin;
	domain_name = (gchar *) args[0].value.pdb_pointer;
	if(domain_name == NULL)
	{ success = FALSE; }
	domain_path = (gchar *) args[1].value.pdb_pointer;
	App* app = GetApp();
	if(success)
	{	if(app->wire_buffer->current_plugin && app->wire_buffer->current_plugin->is_query)
		{	plugin = app->wire_buffer->current_plugin->internal;
#if 0
			if(proc_list->locale_domain)
			{ g_free(proc_list->locale_domain); }
			proc_list->locale_domain = g_strdup(domain_name);
			if(proc_list->locale_path);
			g_free(proc_list->locale_path);
			proc_list->locale_path = domain_path ? g_strdup(domain_path) : NULL;
#endif
		}
	}
	return procedural_db_return_args(&plugin_domain_register_proc, success);
}

static ProcArg plugin_domain_register_inargs[] =
{	{	PDB_STRING,
		"domain_name",
		"The name of the textdomain (must be unique)."
	},
	{	PDB_STRING,
		"domain_path",
		"The absolute path to the compiled message catalog (may be NULL)."
	}
};

static ProcData plugin_domain_register_proc =
{	"cine_plugin_domain_register",
	"Registers a textdomain for localisation.",
	"This procedure adds a textdomain to the list of domains CinePaint searches for strings when translating its menu entries. There is no need to call this function for plug-ins that have their strings included in CinePaint-std-plugins domain as that is used by default. If the compiled message catalog is not in the standard location, you may specify an absolute path to another location. This procedure can only be called in the query function of a plug-in and it has to be called before any procedure is installed.",
	"Sven Neumann",
	"Sven Neumann",
	"2000",
	PDB_INTERNAL,
	2,
	plugin_domain_register_inargs,
	0,
	NULL,
	{ { plugin_domain_register_invoker } }
};

static Argument *
plugin_help_register_invoker(Argument *args)
{	gboolean success = TRUE;
	gchar *help_path;
	PluginInternal *plugin;
	help_path = (gchar *) args[0].value.pdb_pointer;
	if(help_path == NULL)
	{ success = FALSE; 
	}
	App* app = GetApp();
	if(success)
	{	if(app->wire_buffer->current_plugin && app->wire_buffer->current_plugin->is_query)
		{	plugin = app->wire_buffer->current_plugin->internal;
#if 0
			if(plugin->help_path)
			{ g_free(plugin->help_path); }
			plugin->help_path = g_strdup(help_path);
#endif
		}
	}
	return procedural_db_return_args(&plugin_help_register_proc, success);
}

static ProcArg plugin_help_register_inargs[] =
{	{	PDB_STRING,
		"help_path",
		"The rootdir of the plug-in's help pages"
	}
};

static ProcData plugin_help_register_proc =
{	"cine_plugin_help_register",
	"Register a help path for a plug-in.",
	"This procedure changes the help rootdir for the plug-in which calls it. All subsequent calls of cine_help from this plug-in will be interpreted relative to this rootdir. This procedure can only be called in the query function of a plug-in and it has to be called before any procedure is installed.",
	"Michael Natterer <mitch@gimp.org>",
	"Michael Natterer <mitch@gimp.org>",
	"2000",
	PDB_INTERNAL,
	1,
	plugin_help_register_inargs,
	0,
	NULL,
	{ { plugin_help_register_invoker } }
};

void InitPlugins()
{
#if 0
	/*	WireBufferOpen(app->wire_buffer);*/
	if (!WireMsgHandlerRun(app->wire_buffer))
	{
		e_printf("WireMsgHandlerRun failed!\n");
	}
#endif
	/* initialize the progress init and update procedure db calls. */
	procedural_db_register(&progress_init_proc);
	procedural_db_register(&progress_update_proc);
	procedural_db_register(&temp_PDB_name_proc);
	procedural_db_register(&plugins_query_proc);
	procedural_db_register(&plugin_domain_register_proc);
	procedural_db_register(&plugin_help_register_proc);
	/* initialize the message box procedural db calls */
	procedural_db_register(&message_proc);
	procedural_db_register(&message_handler_get_proc);
	procedural_db_register(&message_handler_set_proc);
	/* initialize CinePaint protocol library and set the read and
	 *  write handlers.
	 */
	gp_init();
	wire_set_reader(wire_memory_read);
	wire_set_writer(wire_memory_write);
	wire_set_flusher(wire_memory_flush);
#ifdef BUILD_SHM
	/* allocate a piece of shared memory for use in transporting tiles
	 *  to plug-ins. if we can't allocate a piece of shared memory then
	 *  we'll fall back on sending the data over the pipe.
	 */
	if (use_shm)
	{
#define PLUG_IN_C_3_cw
		shm_ID = shmget(IPC_PRIVATE, TILE_WIDTH * TILE_HEIGHT * TAG_MAX_BYTES, IPC_CREAT | 0777);
		if (shm_ID == -1)
		{
			g_message(_("shmget failed...disabling shared memory tile transport\n"));
		}
		else
		{
			shm_addr = (guchar*)shmat(shm_ID, 0, 0);
			if (shm_addr == (guchar*)-1)
			{
				g_message(_("shmat failed...disabling shared memory tile transport\n"));
				shm_ID = -1;
			}
			/* I dont think this will work, as it makes the shmat of the plugin fail*/
# ifdef	IPC_RMID_DEFERRED_RELEASE
			if (shm_addr != (guchar*)-1)
			{
				shmctl(shm_ID, IPC_RMID, 0);
			}
# endif
		}
	}
#endif
}

void QueryPlugins()
{	unsigned nplugins;
	unsigned nth; 
	App* app = GetApp();
	GSList* plugins = GetExternalPlugins();
	LogoProgress(_("Plug-ins"), "");
	nplugins = g_slist_length(plugins);
	s_printf(_("plugins count = %i\n"),(int) g_slist_length(GetExternalPlugins()));
	nth = 0;
	while (plugins)
	{	PluginInternal* plugin = plugins->data;
		plugins = plugins->next;
		if (plugin->query)
		{	app->wire_buffer->write_pluginrc = TRUE;
			if ((app->be_verbose == TRUE) || (app->no_splash == TRUE))
			{	g_print(_("query plug-in: \"%s\"\n"), plugin->filename);
			}
			plugin_query(plugin->plugin_name, plugin);
		}
		nth++;
		LogoProgress(NULL, plugin->plugin_name);//, nth / nplugins);
	}
}

static void
plugin_add_to_db()
{	Argument args[4];
	Argument* return_vals;
	App* app = GetApp();
	GSList* plugins;
	for (plugins = GetExternalPlugins(); plugins; plugins = plugins->next)
	{	PluginInternal* plugin = plugins->data;
		GSList* procs = plugin->proc_list;
		while (procs)
		{	PluginProc* proc = procs->data;
			procs = procs->next;
			if (proc->plugin_name && (proc->proc_data.proc_type == PDB_INTERNAL))
			{	continue;
			}
			proc->proc_data.exec_method.plugin.plugin_main = 0;
			proc->proc_data.exec_method.plugin.filename = proc->plugin_name;//bug?
			procedural_db_register(&proc->proc_data);
			//plugin_proc_def_insert(proc);// try this
		}
	}
	for (plugins = GetInternalPlugins(); plugins; plugins = plugins->next)
	{	PluginInternal* plugin = plugins->data;
		GSList* procs = plugin->proc_list;
		while (procs)
		{	PluginProc* proc = procs->data;
			procs = procs->next;
			if (proc->extensions || proc->prefixes || proc->magics)
			{	args[0].arg_type = PDB_STRING;
				args[0].value.pdb_pointer = proc->proc_data.name;
				args[1].arg_type = PDB_STRING;
				args[1].value.pdb_pointer = proc->extensions;
				args[2].arg_type = PDB_STRING;
				args[2].value.pdb_pointer = proc->prefixes;
				args[3].arg_type = PDB_STRING;
				args[3].value.pdb_pointer = proc->magics;
				if (proc->image_types)
				{	d_printf("procedural_db_execute cine_register_save_handler: %s\n", proc->proc_data.name);
					return_vals = procedural_db_execute("cine_register_save_handler", args);
					g_free(return_vals);
				}
				else
				{	d_printf("procedural_db_execute cine_register_magic_load_handler: %s\n", proc->proc_data.name);
					return_vals = procedural_db_execute("cine_register_magic_load_handler", args);
					g_free(return_vals);
				}
			}
		}
	}
}

void DefPlugins()
{	App* app = GetApp();
	/* insert the proc defs */
	GSList* plugin_proc = app->wire_buffer->plugin_proc_list;
	while (plugin_proc)
	{	PluginProc* proc = g_new(PluginProc, 1);
		memset(proc, 0, sizeof(proc));
		*proc = *((PluginProc*)plugin_proc->data);
		plugin_proc_def_insert(proc);
		plugin_proc = plugin_proc->next;
	}
	//#ifdef DOES_NOTHING
	GSList* plugin_external_list = GetExternalPlugins();
	while (plugin_external_list)
	{	PluginInternal* plugin = plugin_external_list->data;
		plugin_external_list = plugin_external_list->next;
		GSList* procedures = plugin->proc_list;
		while (procedures)
		{	PluginProc* proc = procedures->data;
			procedures = procedures->next;
			plugin_proc_def_insert(proc);
		}
	}
}

void CreatePlugins(char* filename)
{	InitPlugins();
	char* plugin_path = GetRC()->plugin_path;
	printf(_("Scanning plug-ins in path: %s\n"), plugin_path);
	int count = datafiles_read_directories(plugin_path, CreatePlugin, MODE_EXECUTABLE, ".dll");
//	LogoProgress(_("Resource configuration"), filename, -1);
	QueryPlugins();
	plugin_add_to_db();
//	DefPlugins();
}

void  RunPluginExtensions()
{/* run the available extensions */
	App* app = GetApp();
	GSList* plugins = GetExternalPlugins();
	if ((app->be_verbose == TRUE) || (app->no_splash == TRUE))
	{
		g_print("Starting extensions: ");
	}
	LogoProgress(_("Extensions"), "");
	unsigned nplugins = g_slist_length(plugins);
	unsigned nth = 0;
	while (plugins)
	{	PluginInternal* plugin = plugins->data;
		plugins = plugins->next;
		GSList* procedures = plugin->proc_list;
		while (procedures)
		{	PluginProc* proc = procedures->data; 
			procedures = procedures->next;
			if (proc->plugin_name &&
				(proc->proc_data.num_args == 0) &&
				(proc->proc_data.proc_type == PDB_EXTENSION))
			{
				if ((app->be_verbose == TRUE) || (app->no_splash == TRUE))
				{
					g_print("%s ", proc->proc_data.name);
				}
				LogoProgress(NULL, proc->proc_data.name);
//					nth / nplugins);
				plugin_run(&proc->proc_data, NULL, FALSE, TRUE);
			}
		}
	}
}

void PluginsFree()
{
	App* app = GetApp();
	if ((app->be_verbose == TRUE) || (app->no_splash == TRUE))
	{
		g_print("\n");
	}
	/* free up stuff */
	GSList* plugin_external_list = GetExternalPlugins();
	while (plugin_external_list)
	{
		PluginInternal* plugin = plugin_external_list->data;
		plugin_external_list = plugin_external_list->next;
		g_slist_free(plugin->proc_list);
		g_free(plugin->filename);
		g_free(plugin);
	}
	g_slist_free(GetExternalPlugins());
#ifdef WIN32_EXPERIMENTAL
	return 1;
#endif
}

void WritePluginRC(const char* filename)
{	/* write the pluginrc file if necessary */
	App* app = GetApp();
	if (app->wire_buffer->write_pluginrc)
	{
		if ((app->be_verbose == TRUE) || (app->no_splash == TRUE))
		{
			g_print("writing \"%s\"\n", filename);
		}
		plugin_write_rc(filename);
	}
}

void
plugin_init()
{
	LogoProgress(_("Plugins"), _(""));

char filename[PATH_MAX];
	const char* pluginrc_path = GetRC()->pluginrc_path;
	if (pluginrc_path)
	{	if (*pluginrc_path == '/')
		{	strcpy(filename, pluginrc_path);
		}
		else
		{	sprintf(filename, "%s/%s", cine_directory(), pluginrc_path);
		}
	}
	else
	{	sprintf(filename, "%s/pluginrc", cine_directory());
	}
	CreatePlugins(filename);
	plugin_make_menu();
	RunPluginExtensions();
#ifdef PLUGINRC
	parse_cinerc_file(filename);// query any plug-ins that have changed since last wrote pluginrc 
	WritePluginRC(filename);
#endif
#ifdef DEBUG_WIRE_TABLES
	DumpWireTables();
#endif
#if 0
	PluginsFree();
#endif
}

void
plugin_kill()
{//	GSList *plugins;
//	PluginLoader *plugin;
#ifdef BUILD_SHM
# ifndef	IPC_RMID_DEFERRED_RELEASE
	if(shm_ID != -1)
	{	shmdt((char*) shm_addr);
		shmctl(shm_ID, IPC_RMID, 0);
	}
# else	/* IPC_RMID_DEFERRED_RELEASE */
	if(shm_ID != -1)
	{ shmdt((char*) shm_addr); }
# endif
#endif
#if 0
	App* app = GetApp();
	plugins = app->wire_buffer->open_plugins;
	while(plugins)
	{	plugin = plugins->data;
		plugins = plugins->next;
		DestroyPluginExternal(plugin);
	}
#endif

}

void
plugin_add(char *filename,
            char *menu_path,
            char *accelerator)
{	PluginProc *proc;
	GSList *list;
	if(strncmp("plugin_", filename, 8) != 0)
	{	char *t = g_new(char, strlen(filename) + 9);
		sprintf(t, "plugin_%s", filename);
		g_free(filename);
		filename = t;
	}
	App* app = GetApp();
	list = app->wire_buffer->plugin_proc_list;
	while(list)
	{	proc = list->data;
		list = list->next;
		if(strcmp(proc->proc_data.name, filename) == 0)
		{	FreeProcDef(proc);
			proc->proc_data.name = filename;
			proc->menu_path = menu_path;
			proc->accelerator = accelerator;
			break;
		}
	}
	proc = g_new0(PluginProc, 1);
	if(!proc)
	{	return;
	}
	memset(proc,0,sizeof(*proc));
	proc->proc_data.name = filename;
	proc->menu_path = menu_path;
	proc->accelerator = accelerator;
	app->wire_buffer->plugin_proc_list = g_slist_prepend(app->wire_buffer->plugin_proc_list, proc);
}

char*
plugin_image_types(char *name)
{	PluginInternal *plugin;
	PluginProc *proc;
	GSList *plugin_list;
	App* app = GetApp();
	if(app->wire_buffer->current_plugin)
	{	plugin = app->wire_buffer->current_plugin->internal;
		plugin_list = plugin->proc_list;
	}
	else
	{	plugin_list = GetInternalPlugins();
	}
	while(plugin_list)
	{	proc = plugin_list->data;
		plugin_list = plugin_list->next;
		if(strcmp(proc->proc_data.name, name) == 0)
		{ return proc->image_types; }
	}
	return NULL;
}

GSList*
plugin_extensions_parse(char *extensions)
{	GSList *list;
	char *extension;
	list = NULL;
	/* EXTENSIONS can be NULL.  Avoid calling strtok if it is.  */
	if(extensions)
	{	extensions = g_strdup(extensions);
		extension = strtok(extensions, " \t,");
		while(extension)
		{	list = g_slist_prepend(list, g_strdup(extension));
			extension = strtok(NULL, " \t,");
		}
		g_free(extensions);
	}
	return g_slist_reverse(list);
}

void
plugin_add_internal(PluginProc *proc)
{	App* app = GetApp();
	if(!app || !app->wire_buffer)
	{	return;
	}
	d_printf("plugin_add_internal: %s\n",proc->plugin_name);
	GSList *plugin_list = GetInternalPlugins();
	plugin_list = g_slist_prepend(plugin_list,proc);
	app->wire_buffer->plugin_internal_list = plugin_list;
//	DumpWireTables();
}

PluginProc*
plugin_file_handler(char *name,
                     char *extensions,
                     char *prefixes,
                     char *magics)
{	PluginInternal *plugin;
	PluginProc *plugin_proc;
	GSList * proc_list;
	App* app = GetApp();
//DumpPDB();
//DumpWireTables();
	if(app->wire_buffer->current_plugin)
	{	plugin = app->wire_buffer->current_plugin->internal;
		proc_list = plugin->proc_list;
		//proc_list = app->wire_buffer->plugin->procedures;
	}
	else
	{	proc_list = GetInternalPlugins();
	}
//	GHashTable* hash = GetPDB();
	plugin_proc = FindProcDef(proc_list,name);
	if(!plugin_proc)
	{//	DumpPDB();
	//	DumpWireTables();
		d_printf("FindProcDef %s not found\n",name);
		return NULL;
	}
#ifdef DEBUG_FILE_HANDLER
	d_printf("plugin_file_handler %s\n",name);
#endif
	/* EXTENSIONS can be plugin_proc->extensions  */
	if(plugin_proc->extensions != extensions)
	{	if(plugin_proc->extensions)
		{ g_free(plugin_proc->extensions); }
		plugin_proc->extensions = g_strdup(extensions);
	}
	plugin_proc->extensions_list = plugin_extensions_parse(plugin_proc->extensions);
	/* PREFIXES can be plugin_proc->prefixes  */
	if(plugin_proc->prefixes != prefixes)
	{	if(plugin_proc->prefixes)
		{ g_free(plugin_proc->prefixes); }
		plugin_proc->prefixes = g_strdup(prefixes);
	}
	plugin_proc->prefixes_list = plugin_extensions_parse(plugin_proc->prefixes);
	/* MAGICS can be plugin_proc->magics  */
	if(plugin_proc->magics != magics)
	{	if(plugin_proc->magics)
		{ g_free(plugin_proc->magics); }
		plugin_proc->magics = g_strdup(magics);
	}
	plugin_proc->magics_list = plugin_extensions_parse(plugin_proc->magics);
//	plugin_proc->plugin_main = 0;
	return plugin_proc;
}

void
plugin_def_add(PluginInternal *plugin)
{	GSList *plugins;
	PluginInternal *t;
	char *t1, *t2;
	t1 = strrchr(plugin->filename, '/');
	if(t1)
	{ t1 = t1 + 1; }
	else
	{ t1 = plugin->filename; }
	App* app = GetApp();
	plugins = GetExternalPlugins();
	while(plugins)
	{	t = plugins->data;
		t2 = strrchr(t->filename, '/');
		if(t2)
		{ t2 = t2 + 1; }
		else
		{ t2 = t->filename; }
		if(strcmp(t1, t2) == 0)
		{	if((strcmp(plugin->filename, t->filename) == 0) &&
			        (plugin->mtime == t->mtime))
			{	/* Use cached plug-in entry */
				plugins->data = plugin;
				g_free(t->filename);
				g_free(t);
			}
			else
			{	g_free(plugin->filename);
				g_free(plugin);
			}
			return;
		}
		plugins = plugins->next;
	}
	app->wire_buffer->write_pluginrc = TRUE;
	g_print("\"%s\" executable not found\n", plugin->filename);
	g_free(plugin->filename);
	g_free(plugin);
}

char*
plugin_menu_path(char *name)
{	PluginInternal *plugin;
	PluginProc *proc;
	GSList *plugins, *tmp2;
	App* app = GetApp();
	plugins = GetExternalPlugins();
	while(plugins)
	{	plugin = plugins->data;
		plugins = plugins->next;
		tmp2 = plugin->proc_list;
		while(tmp2)
		{	proc = tmp2->data;
			tmp2 = tmp2->next;
			if(strcmp(proc->proc_data.name, name) == 0)
			{ return proc->menu_path; }
		}
	}
	plugins = GetInternalPlugins();
	while(plugins)
	{	proc = plugins->data;
		plugins = plugins->next;
		if(strcmp(proc->proc_data.name, name) == 0)
		{ return proc->menu_path; }
	}
	return NULL;
}

PluginLoader*
CreatePluginLoader(char * plugin_name)
{	PluginLoader *loader;
	PluginInternal* internal = GetPlugin(plugin_name);
	if(!internal)
	{	return 0;
	}
	loader = g_new(PluginLoader, 1);
	memset(loader,0,sizeof(*loader));
	loader->internal = internal;
	loader->args[0] = g_strdup(plugin_name);
	loader->args[1] = g_strdup("-gimp");
	loader->args[2] = g_new(char, 16);
	loader->args[2][0] = 0;
	loader->args[3] = g_new(char, 16);
	loader->args[3][0] = 0;
	loader->name = g_strdup(plugin_name);
#if 0
	App* app = GetApp();
	app->wire_buffer->loader = loader;
	app->wire_buffer->current_plugin = loader;
#endif
	return loader;
}

void
DestroyPluginExternal(PluginLoader *loader)
{	if(!loader)
	{	return;
	}
	ClosePluginExternal(loader, TRUE);
		if(loader->args[0])
		{ g_free(loader->args[0]); }
		if(loader->args[1])
		{ g_free(loader->args[1]); }
		if(loader->args[2])
		{ g_free(loader->args[2]); }
		if(loader->args[3])
		{ g_free(loader->args[3]); }
		if(loader->args[4])
		{ g_free(loader->args[4]); }
		if(loader->args[5])
		{ g_free(loader->args[5]); }
		App* app = GetApp();
		if (app)
		{	if(loader == app->wire_buffer->current_plugin)
			{ plugin_pop(); 
		}	}
		if(!loader->is_destroy)
		{ g_free(loader); 
	}
}

int
OpenPluginExternal(PluginLoader *loader)
{	if(!loader)
	{	return 0;
	}
/* Set the rest of the command line arguments.
		*/
	if(loader->is_query)
	{	loader->args[4] = g_strdup("-query");
	}
	else
	{	loader->args[4] = g_new(char, 16);
		loader->args[5] = g_new(char, 16);
		sprintf(loader->args[4], "%d", TILE_WIDTH);
		sprintf(loader->args[5], "%d", TILE_WIDTH);
	}
	/*	This is how it would be if plug-ins were Windows executables
		    loader->pid = spawnv (_P_NOWAIT, loader->args[0], loader->args);
		    if (loader->pid == -1)*/
	App* app = GetApp();
//	loader->wire_buffer=app->wire_buffer;
	if(!loader->is_query)
	{	return 1;
	}
	/*	if(!PlugInRun(loader))
		Run plug-in in main thread so we don't need to
		explicitly wait on it to complete */
	if(!PlugInRun(loader))
	{	e_printf(_("PlugInRun failed! %s\n"),loader->args[0]);
		/*          DestroyPluginExternal(loader); -- don't, causes double delete!*/
		return 0;
	}
	/*	WaitForPlugin(plug-in);*/
	loader->is_open = TRUE;
	return 1;
}

void
ClosePluginExternal(PluginLoader *loader,
              int     kill_it)
{	//int status;
	//struct timeval tv;
	puts("ClosePluginExternal");
#if 0
	if(loader && loader->open && loader->internal)
	{	loader->open = FALSE;
		dlclose(loader->internal->library);
		loader->internal->plugin_main = 0; //INVALID_HANDLE_VALUE;
		//loader->plugin_main=0;
		loader->plugin_info=0;
		wire_clear_error();
		/* Destroy the progress dialog if it exists
		 */
#ifdef SEPARATE_PROGRESS_BAR
		if(loader->progress)
		{	gtk_signal_disconnect_by_data(GTK_OBJECT(loader->progress), loader);
			gtk_widget_destroy(loader->progress);
			loader->progress = NULL;
			loader->progress_label = NULL;
			loader->progress_bar = NULL;
		}
#else
		if(loader->progress)
		{	progress_end();
			loader->progress = NULL;
		}
#endif
		/* Set the fields to null values.
		 */
		loader->synchronous = FALSE;
		loader->recurse = FALSE;
		/* rsr: this was killing xcf handlers */
#ifdef UNREGISTER_TEMP_PROCEDURES
		/* Un.cany temporary procedures
		 */
		/*      if (loader->temp_proc_defs)
		*/	if(GetExternalPlugins())
		{	GSList *proc_list;
			PluginProc *proc;
			/*	  proc_list = loader->temp_proc_defs;
			*/
			proc_list=GetExternalPlugins();
			while(proc_list)
			{	proc = (PluginProc *) proc_list->data;
				plugin_proc_def_remove(proc);
				proc_list = proc_list->next;
			}
			/*	  g_slist_free (loader->temp_proc_defs);
			*/	  g_slist_free(GetExternalPlugins());
			/*	loader->temp_proc_defs = NULL;*/
			app->wire_buffer->plugin_external_list = 0;
		}
		App* app = GetApp();
		app->wire_buffer->open_plugins = g_slist_remove(app->wire_buffer->open_plugins,loader);
#endif
	}
#endif
}

static Argument* plugin_get_current_return_vals(ProcData *proc_rec)
{	Argument *return_vals;
	int nargs;
	/* Return the status code plus the current return values. */
	nargs = proc_rec->num_values + 1;
	App* app = GetApp();
	if(app->wire_buffer->current_return_vals && app->wire_buffer->current_return_nvals == nargs)
	{ return_vals = app->wire_buffer->current_return_vals; }
	else if(app->wire_buffer->current_return_vals)
	{	/* Allocate new return values of the correct size. */
		return_vals = procedural_db_return_args(proc_rec, FALSE);
		/* Copy all of the arguments we can. */
		memcpy(return_vals, app->wire_buffer->current_return_vals,
		       sizeof(Argument) * MIN(app->wire_buffer->current_return_nvals, nargs));
		/* Free the old argument pointer.  This will cause a memory leak
		only if there were more values returned than we need (which
		 shouldn't ever happen). */
		g_free(app->wire_buffer->current_return_vals);
	}
	else
	{	/* Just return a dummy set of values. */
		return_vals = procedural_db_return_args(proc_rec, FALSE);
	}
	/* We have consumed any saved values, so clear them. */
	app->wire_buffer->current_return_nvals = 0;
	app->wire_buffer->current_return_vals = NULL;
	return return_vals;
}

Argument*
plugin_run(ProcData *proc_rec,
            Argument   *args,
            int         synchronous,
            int         destroy_values)
{	GPConfig config;
	memset(&config,0,sizeof(config));
	GPProcRun proc_run;
	memset(&proc_run,0,sizeof(proc_run));
	Argument *return_vals;
	PluginLoader *loader = 0;
	WireMessage msg; 
	WireMsgSetZero(msg);
	return_vals = NULL;
	if(proc_rec->proc_type == PDB_TEMPORARY)
	{	return_vals = plugin_temp_run(proc_rec, args);
		goto done;
	}
	loader = CreatePluginLoader(proc_rec->exec_method.plugin.filename);
	if(!loader || !loader->internal)
	{	goto done;
	}
	if(!OpenPluginExternal(loader))
	{	goto done;
	}
	App* app = GetApp();
	loader->is_open = TRUE;
//			loader->wire_buffer=app->wire_buffer;
//			app->wire_buffer->plugin_external_list = loader;
	loader->is_synchronous=TRUE;
	loader->is_recurse = synchronous;
	plugin_push(loader);
	config.version = GP_VERSION;
	config.tile_width = TILE_WIDTH;
	config.tile_height = TILE_HEIGHT;
//			config.shm_ID = app->wire_buffer->shm_ID;
	config.gamma = GetRC()->gamma_val;
	config.install_cmap = GetRC()->install_cmap;
	config.use_xshm = gdk_get_use_xshm();
	config.color_cube[0] = GetRC()->color_cube_shades[0];
	config.color_cube[1] = GetRC()->color_cube_shades[1];
	config.color_cube[2] = GetRC()->color_cube_shades[2];
	config.color_cube[3] = GetRC()->color_cube_shades[3];
	proc_run.name = proc_rec->name;
	proc_run.nparams = proc_rec->num_args;
	proc_run.params = plugin_args_to_params(args, proc_rec->num_args, FALSE);
	if(!gp_config_write(&config))
	{	return_vals = procedural_db_return_args(proc_rec, FALSE);
		goto done;
	}
#if 1
	if (!gp_proc_run_write(&proc_run,FALSE))
		//bug?  ||			!wire_flush(app->wire_buffer->current_writefd))
	{	return_vals = procedural_db_return_args(proc_rec, FALSE);
		goto done;
	}
#endif
	if(!PlugInRun(loader))
	{	e_printf("PlugInRun failed!\n");
	}
	/*
		*  If this is an automatically installed extension, wait for an
		*   installation-confirmation message
		*/
#if 0
	if((proc_rec->proc_type == PDB_EXTENSION) && (proc_rec->num_args == 0))
		e_puts("ERROR: PDB_EXTENSION not implemented");
	msg.msg_type=GP_ERROR;
	while(GP_QUIT!=msg.msg_type)
	{	GetWireBuffer(&msg);
		if(GP_QUIT==msg.msg_type)
		{	break;
		}
		plugin_handle_message(&msg);
		if(GP_PROC_RETURN==msg.msg_type)
		{	return_vals = plugin_get_current_return_vals(proc_rec);
		}
	}
	plugin_pop();
	if(loader->is_recurse)
	{
		/*		DrainWireBuffer();*/
		return_vals = plugin_get_current_return_vals(proc_rec);
	}
#endif
done:
	return_vals = plugin_get_current_return_vals(proc_rec);
	plugin_params_destroy(proc_run.params, proc_run.nparams, FALSE);
	if(return_vals && destroy_values)
	{	procedural_db_destroy_args(return_vals, proc_rec->num_values);
		return_vals = NULL;
	}
	return return_vals;
}

void
plugin_repeat(int with_interface)
{	GDisplay *gdisplay;
	Argument *args;
	int i;
	App* app = GetApp();
	if(app->wire_buffer->last_plugin)
	{	gdisplay = gdisplay_active();
		/* construct the procedures arguments */
		args = g_new(Argument, app->wire_buffer->last_plugin->num_args);
		memset(args, 0, (sizeof(Argument) * app->wire_buffer->last_plugin->num_args));
		/* initialize the argument types */
		for(i = 0; i < app->wire_buffer->last_plugin->num_args; i++)
		{ args[i].arg_type = app->wire_buffer->last_plugin->args[i].arg_type; }
		/* initialize the first 3 plug-in arguments  */
		args[0].value.pdb_int = (with_interface ? RUN_INTERACTIVE : RUN_WITH_LAST_VALS);
		args[1].value.pdb_int = gdisplay->gimage->ID;
		args[2].value.pdb_int = drawable_ID(gimage_active_drawable(gdisplay->gimage));
		/* run the plug-in loader */
		plugin_run(app->wire_buffer->last_plugin, args, FALSE, TRUE);
		g_free(args);
	}
}

void
plugin_set_menu_sensitivity(Tag t)
{	PluginProc *proc;
	GSList *plugins;
	int sensitive = FALSE;
	int base_type = tag_to_drawable_type(t);
	App* app = GetApp();
	plugins = GetInternalPlugins();
	while(plugins)
	{	proc = plugins->data;
		plugins = plugins->next;
		if(proc->image_types_val && proc->menu_path)
		{	switch(base_type)
			{	case -1:
					sensitive = FALSE;
					break;
				case RGB_GIMAGE:
					sensitive = proc->image_types_val & RGB_IMAGE;
					break;
				case RGBA_GIMAGE:
					sensitive = proc->image_types_val & RGBA_IMAGE;
					break;
				case GRAY_GIMAGE:
					sensitive = proc->image_types_val & GRAY_IMAGE;
					break;
				case GRAYA_GIMAGE:
					sensitive = proc->image_types_val & GRAYA_IMAGE;
					break;
				case INDEXED_GIMAGE:
					sensitive = proc->image_types_val & INDEXED_IMAGE;
					break;
				case INDEXEDA_GIMAGE:
					sensitive = proc->image_types_val & INDEXEDA_IMAGE;
					break;
				case U16_RGB_GIMAGE:
					sensitive = proc->image_types_val & U16_RGB_IMAGE;
					break;
				case U16_RGBA_GIMAGE:
					sensitive = proc->image_types_val & U16_RGBA_IMAGE;
					break;
				case U16_GRAY_GIMAGE:
					sensitive = proc->image_types_val & U16_GRAY_IMAGE;
					break;
				case U16_GRAYA_GIMAGE:
					sensitive = proc->image_types_val & U16_GRAYA_IMAGE;
					break;
				case FLOAT_RGB_GIMAGE:
					sensitive = proc->image_types_val & FLOAT_RGB_IMAGE;
					break;
				case FLOAT_RGBA_GIMAGE:
					sensitive = proc->image_types_val & FLOAT_RGBA_IMAGE;
					break;
				case FLOAT_GRAY_GIMAGE:
					sensitive = proc->image_types_val & FLOAT_GRAY_IMAGE;
					break;
				case FLOAT_GRAYA_GIMAGE:
					sensitive = proc->image_types_val & FLOAT_GRAYA_IMAGE;
					break;
				case FLOAT16_RGB_GIMAGE:
					sensitive = proc->image_types_val & FLOAT16_RGB_IMAGE;
					break;
				case FLOAT16_RGBA_GIMAGE:
					sensitive = proc->image_types_val & FLOAT16_RGBA_IMAGE;
					break;
				case FLOAT16_GRAY_GIMAGE:
					sensitive = proc->image_types_val & FLOAT16_GRAY_IMAGE;
					break;
				case FLOAT16_GRAYA_GIMAGE:
					sensitive = proc->image_types_val & FLOAT16_GRAYA_IMAGE;
					break;
			}
			menus_set_sensitive(proc->menu_path, sensitive);
			if(app->wire_buffer->last_plugin && (app->wire_buffer->last_plugin == &(proc->proc_data)))
			{	menus_set_sensitive("<Image>/Filters/Repeat last", sensitive);
				menus_set_sensitive("<Image>/Filters/Re-show last", sensitive);
			}
		}
	}
}

static void
plugin_recv_message(gpointer          data,
                     gint              id,
                     GdkInputCondition cond)
{	WireMessage msg; 
	WireMsgSetZero(msg);
	plugin_push((PluginLoader*) data);
	App* app = GetApp();
	if(!wire_read_msg(&msg))
	{ ClosePluginExternal(app->wire_buffer->current_plugin, TRUE); }
	else
	{	plugin_handle_message(&msg);
		wire_destroy(&msg);
	}
	if(!app->wire_buffer->current_plugin->is_open)
	{ DestroyPluginExternal(app->wire_buffer->current_plugin); }
	else
	{ plugin_pop(); }
}

// static bool plugin_set_proc_return(GPProcReturn *proc_return);

void plugin_handle_message(WireMessage *msg)
{
	App* app = GetApp();
	switch(msg->msg_type)
	{	default:
			printf("Unknown wire message %d",msg->msg_type);
			break;
		case GP_QUIT:
			plugin_handle_quit();
			break;
		case GP_CONFIG:
//			g_message(_("plugin_handle_message(): received a GP_CONFIG message (should not happen)\n"));
//			ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
			cine_config(msg->msg_data);
			break;
		case GP_TILE_REQ:
			plugin_handle_tile_req(msg->msg_data);
			break;
		case GP_TILE_ACK:
#ifdef IGNORE_ACK
			g_message(_("plugin_handle_message(): received a GP_TILE_ACK message (should not happen)\n"));
			ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
#endif
			break;
		case GP_TILE_DATA:
			gp_tile_ack_write(0);
//			g_message(_("plugin_handle_message(): received a GP_TILE_DATA message (should not happen)\n"));
//			ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
			break;
		case GP_PROC_RUN:
			plugin_handle_proc_run(msg->msg_data);
			break;
		case GP_PROC_RETURN:
			plugin_set_proc_return(msg->msg_data);
			//ClosePluginExternal(app->wire_buffer->current_plugin, FALSE);
			break;
		case GP_TEMP_PROC_RUN:
			g_message(_("plugin_handle_message(): received a GP_TEMP_PROC_RUN message (should not happen)\n"));
			//ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
			break;
		case GP_TEMP_PROC_RETURN:
			plugin_set_proc_return(msg->msg_data);
			gtk_main_quit();
			break;
		case GP_PROC_INSTALL:
			plugin_handle_proc_install(msg->msg_data);
			break;
		case GP_PROC_UNINSTALL:
			plugin_handle_proc_uninstall(msg->msg_data);
			break;
		case GP_EXTENSION_ACK:
			gtk_main_quit();
			break;
	}
}

static void
plugin_handle_quit()
{
	App* app = GetApp();
	ClosePluginExternal(app->wire_buffer->current_plugin, FALSE);
}

int GetTileData(GPTileData* tile_data)
{	WireMessage msg; 
	memset(&msg, 0, sizeof(msg)); 
	App* app = GetApp();
	if (!gp_tile_data_write(tile_data))
	{	g_message(_("plugin_handle_tile_req: ERROR"));
		return FALSE;
	}
	if (!wire_read_msg(&msg))
	{	g_message(_("plugin_handle_tile_req: ERROR"));
		return FALSE;
	}
	if (msg.msg_type != GP_TILE_DATA)
	{	g_message(_("expected tile data and received: %d\n"), msg.msg_type);
		return FALSE;
	}
	GPTileData* data = msg.msg_data;
	tile_data->data = data->data;
	return TRUE;
}

void CreateTile(GPTileReq* tile_req)
{
	GPTileData tile_data; memset(&tile_data, 0, sizeof(tile_data));
	GPTileData* tile_info = 0;
	WireMessage msg; memset(&msg, 0, sizeof(msg));
	PixelArea a; memset(&a, 0, sizeof(a));
	Canvas* canvas = 0;
	CanvasDrawable* drawable = 0;
	gint ntile_cols, ntile_rows;
	gint ewidth, eheight;
	gint i, j;
	gint x, y;
	App* app = GetApp();

	tile_data.drawable_ID = -1;
	tile_data.tile_num = 0;
	tile_data.shadow = 0;
	tile_data.bpp = 0;
	tile_data.width = 0;
	tile_data.height = 0;
#ifdef BUILD_SHM
	tile_data.use_shm = (app->wire_buffer->shm_ID == -1) ? FALSE : TRUE;
#endif
	tile_data.data = NULL;
	if (!gp_tile_data_write(&tile_data))
	{
		g_message(_("plugin_handle_tile_req: ERROR"));
		ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
		return;
	}
//	TaskSwitchToPlugin();
#ifdef EXTRA_ACK
	if (!wire_read_msg(app->wire_buffer->current_readfd, &msg))
	{
		g_message(_("plugin_handle_tile_req: ERROR"));
		ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
		return;
	}
	if (msg.msg_type != GP_TILE_ACK)
	{
		g_message(_("expected tile data and received: %d\n"), msg.msg_type);
		ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
		return;
	}
#endif
	if (!wire_read_msg(&msg))
	{
		g_message(_("plugin_handle_tile_req: ERROR"));
		ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
		return;
	}
	if (msg.msg_type != GP_TILE_DATA)
	{
		g_message(_("expected tile data and received: %d\n"), msg.msg_type);
		ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
		return;
	}
	tile_info = msg.msg_data;
	drawable = drawable_get_ID(tile_info->drawable_ID);
	if (tile_info->shadow)
	{
		canvas = drawable_shadow(drawable);
	}
	else
	{
		canvas = drawable_data(drawable);
	}
	if (!canvas)
	{
		g_message(_("plug-in requested invalid drawable (killing)\n"));
		ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
		return;
	}
	/* Find the upper left corner (x,y) of the plugins tile with index tile_num. */
	ntile_cols = (drawable_width(drawable) + TILE_WIDTH - 1) / TILE_WIDTH;
	ntile_rows = (drawable_height(drawable) + TILE_HEIGHT - 1) / TILE_HEIGHT;
	i = tile_info->tile_num % ntile_cols;
	j = tile_info->tile_num / ntile_cols;
	x = TILE_WIDTH * i;
	y = TILE_HEIGHT * j;
	/*d_printf ("Get tilenum: %d, (i,j)=(%d,%d) (x,y)=(%d,%d) (rows,cols)=(%d,%d)\n",
		tile_info->tile_num, i, j, x, y, ntile_rows, ntile_cols);*/
		/* Make sure there is some valid canvas data there on CinePaint side */
	canvas_portion_refrw(canvas, x, y);
	if (!canvas_portion_data(canvas, x, y))
	{
		g_message(_("plug-in requested invalid tile (killing)\n"));
		ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
		canvas_portion_unref(canvas, x, y);
		return;
	}
	/* Now find the plugins tile ewidth and eheight */
	if (i == (ntile_cols - 1))
	{
		ewidth = drawable_width(drawable) - ((ntile_cols - 1) * TILE_WIDTH);
	}
	else
	{
		ewidth = TILE_WIDTH;
	}
	if (j == (ntile_rows - 1))
	{
		eheight = drawable_height(drawable) - ((ntile_rows - 1) * TILE_HEIGHT);
	}
	else
	{
		eheight = TILE_HEIGHT;
	}
	/* Now copy the data from plugins tile to CinePaint's canvas */
	pixelarea_init(&a, canvas, x, y, ewidth, eheight, TRUE);
#ifdef BUILD_SHM
	if (tile_data.use_shm)
	{
		plugin_copyarea(&a, app->wire_buffer->shm_addr, COPY_BUFFER_TO_AREA);
	}   /* shm to CinePaint */
	else
#endif
		plugin_copyarea(&a, tile_info->data, COPY_BUFFER_TO_AREA);  /* pipe buffer to CinePaint */
	canvas_portion_unref(canvas, x, y);
	wire_destroy(&msg);
	if (!gp_tile_ack_write(app->wire_buffer->current_writefd))
	{
		g_message(_("plugin_handle_tile_req: ERROR"));
		ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
		return;
	}
}

void  ZebraStripe(guchar* data, int height, int width, int bpp)
{	guchar color = 250;
	width *= bpp;
	while(height--)
	{	memset(data,color,width);
		if(250 == color)
		{	color = 50;
		}
		else
		{	color = 250;
		}
		data += width;
}	}

void AllocateTile(GPTileData* tile_data,GPTileReq* tile_req,int bpp)
{
	gint ntile_cols, ntile_rows;
	gint ewidth, eheight;
	gint i, j;
	CanvasDrawable* drawable = 0;
	drawable = drawable_get_ID(tile_req->drawable_ID);
	/* Find the lower left corner (x,y) of the plugins tile with index tile_num. */
	ntile_cols = (drawable_width(drawable) + TILE_WIDTH - 1) / TILE_WIDTH;
	ntile_rows = (drawable_height(drawable) + TILE_HEIGHT - 1) / TILE_HEIGHT;
	i = tile_req->tile_num % ntile_cols;
	j = tile_req->tile_num / ntile_cols;
	tile_data->x = TILE_WIDTH * i;
	tile_data->y = TILE_HEIGHT * j;
	/*d_printf ("Put tilenum: %d, (i,j)=(%d,%d) (x,y)=(%d,%d) (rows,cols)=(%d,%d)\n",
		tile_req->tile_num, i, j, x, y, ntile_rows, ntile_cols);*/
	tile_data->drawable_ID = tile_req->drawable_ID;
	tile_data->tile_num = tile_req->tile_num;
	tile_data->shadow = tile_req->shadow;
	tile_data->bpp = bpp;
	/* Find the plugins notion of ewidth and eheight of the tile */
	if (i == (ntile_cols - 1))
	{
		ewidth = drawable_width(drawable) - ((ntile_cols - 1) * TILE_WIDTH);
	}
	else
	{
		ewidth = TILE_WIDTH;
	}
	if (j == (ntile_rows - 1))
	{
		eheight = drawable_height(drawable) - ((ntile_rows - 1) * TILE_HEIGHT);
	}
	else
	{
		eheight = TILE_HEIGHT;
	}
	tile_data->height = eheight;
	tile_data->width = ewidth;
	tile_data->data = g_malloc(tile_data->height * tile_data->width * tile_data->bpp);
	ZebraStripe(tile_data->data, tile_data->height, tile_data->width, tile_data->bpp);
}

void FetchTile(GPTileReq * tile_req)
{	GPTileData tile_data; 
	memset(&tile_data, 0, sizeof(tile_data));
	WireMessage msg; 
	memset(&msg, 0, sizeof(msg));
	PixelArea pixelArea; 
	memset(&pixelArea, 0, sizeof(pixelArea));
	Canvas* canvas = 0;
	App* app = GetApp();
	if (tile_req->shadow)
	{
		canvas = drawable_shadow(drawable_get_ID(tile_req->drawable_ID));
	}
	else
	{
		canvas = drawable_data(drawable_get_ID(tile_req->drawable_ID));
	}
	if (!canvas)
	{
		g_message(_("plug-in requested invalid drawable (killing)\n"));
		ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
		return;
	}
	AllocateTile(&tile_data, tile_req,tag_bytes(canvas_tag(canvas)));
	canvas_portion_refro(canvas, tile_data.x, tile_data.y);
	if (!canvas_portion_data(canvas, tile_data.x, tile_data.y))
	{
		g_message(_("plug-in requested invalid tile (killing)\n"));
		ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
		canvas_portion_unref(canvas, tile_data.x, tile_data.y);
		return;
	}
	/*d_printf(" ewidth = %d, eheight = %d \n", ewidth, eheight);*/
	pixelarea_init(&pixelArea, canvas, tile_data.x, tile_data.y, tile_data.width, tile_data.height, FALSE);
#if 1
// DEBUG_ZEBRA
		/* rsr: g_malloc, not malloc!!! */
//puts("skip copyarea");
#endif
#if 0
	if(!GetTileData(&tile_data))
	{	ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
		return;
	}
#endif
	plugin_copyarea(&pixelArea, tile_data.data, COPY_AREA_TO_BUFFER); // Get image from screen
	canvas_portion_unref(canvas, tile_data.x, tile_data.y);
	if (!gp_tile_data_write(&tile_data))
	{
		g_message(_("plugin_handle_tile_req: ERROR"));
		ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
		return;
	}
#if 0
	//		TaskSwitchToPlugin();
	//		PluginDoWireMsg();
			//gp_tile_ack_write(app->wire_buffer->current_writefd);
	if (!wire_read_msg(app->wire_buffer->current_readfd, &msg))
	{
		g_message(_("plugin_handle_tile_req: ERROR"));
		ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
		return;
	}
	if (msg.msg_type != GP_TILE_ACK)
		//if (msg.msg_type != GP_TILE_DATA)
	{
		g_message("expected tile ack and received: %d\n", msg.msg_type);
		ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
		return;
	}
#endif
#if 0
	if (!wire_write_msg(app->wire_buffer->current_writefd, &msg))
	{
		g_message(_("plugin_handle_tile_req: ERROR"));
		ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
		return;
	}
#endif
#ifdef BUILD_SHM
	if (!tile_data.use_shm)
#endif
#if 0
		if (tile_data.data)
		{
			g_free(tile_data.data);
		}
	wire_destroy(&msg);
#endif
}

static void
plugin_handle_tile_req(GPTileReq *tile_req)
{	if(tile_req->drawable_ID == -1)
	{	CreateTile(tile_req);
		return;
	}
	FetchTile(tile_req);
}


void PrintScanline(const unsigned char* p,int width,int bpp,int is_verbose)
{	if(3 != bpp)
	{	return;
	}
	puts("PrintScanline: ");
	int is_black = TRUE;
	while(width--)
	{	unsigned r = p[0];
		unsigned b = p[1];
		unsigned g = p[2];
		if(r || b || g)
		{	is_black = FALSE;
		}
		if(is_verbose)
		{	printf("%u:%u:%u ",r,g,b);
		}
		p += bpp;
	}
	printf("scanline is_black = %i\n",is_black);
}

void
plugin_copyarea(PixelArea *pixelArea, guchar *buf, gint direction)
{	void *pag;
	Tag a_tag = pixelarea_tag(pixelArea);
	gint bpp = tag_bytes(a_tag);
	guchar *d, *s;
	gint d_rowstride, s_rowstride;
	gint width, height;
	for(pag = pixelarea_register(1, pixelArea) ;
	        pag != NULL;
	        pag = pixelarea_process(pag))
	{	height = pixelarea_height(pixelArea);
		width = pixelarea_width(pixelArea);
		if(direction == COPY_AREA_TO_BUFFER)
		{	/* copy from pixelArea to buf */
			s = pixelarea_data(pixelArea);
			s_rowstride = pixelarea_rowstride(pixelArea);
			d = buf;
			d_rowstride = width * bpp;
#ifdef DEBUG_BUFFER_COPY
	        d_printf("putting data : s_rowstride: %d, d_rowstride: %d (data = %i-%i-%i)\n",s_rowstride, d_rowstride,s[0],s[1],s[2]);
			d_printf("pixelarea width : %d, height: %d\n", width, height);
#endif
		}
		else 
		{	/* copy from buf to pixelArea */
			s = buf;
			s_rowstride = width * bpp;
			d = pixelarea_data(pixelArea);
			d_rowstride = pixelarea_rowstride(pixelArea);
#ifdef DEBUG_BUFFER_COPY
			 d_printf("getting data : s_rowstride: %d, d_rowstride: %d (data = %i-%i-%i)\n",s_rowstride, d_rowstride, s[0], s[1], s[2]);
			 d_printf("pixelarea width : %d, height: %d\n", width, height);
#endif
		}
// This is what copies the data to the screen:
//		puts("Scanline copy");
		while(height --)
		{	/* rsr: scanline-based region copy */
			memcpy(d, s, width * bpp);
			d += d_rowstride;
			s += s_rowstride;
#ifdef DEBUG_SCANLINE
			if(32 == height)
			{	PrintScanline(d, width, bpp,FALSE);
			}
#endif
		}
	}
}

bool
plugin_handle_proc_run(GPProcRun *proc_run)
{	GPProcReturn proc_return;
	ProcData *proc_rec;
	Argument *args;
	Argument *return_vals;
//	PlugInBlocked *blocked;
	args = plugin_params_to_args(proc_run->params, proc_run->nparams, FALSE);
	proc_rec = procedural_db_lookup(proc_run->name);
	if(!proc_rec)
	{	/* THIS IS PROBABLY NOT CORRECT -josh */
		printf("ERROR: PDB lookup failed on %s\n", proc_run->name);
		plugin_args_destroy(args, proc_run->nparams, FALSE);
		return FALSE;
	}
//#define CRASHES_UNSHARP_PLUGIN
#ifdef DEBUG_PROCRUN
	d_printf("plugin_handle_proc_run: %s %s\n",proc_run->name,proc_run->params->data.d_string);
#endif
	return_vals = procedural_db_execute(proc_run->name, args);
	if(return_vals)
	{	proc_return.name = proc_run->name;
		if(proc_rec)
		{	proc_return.nparams = proc_rec->num_values + 1;
			proc_return.params = plugin_args_to_params(return_vals, proc_rec->num_values + 1, FALSE);
		}
		else
		{	proc_return.nparams = 1;
			proc_return.params = plugin_args_to_params(return_vals, 1, FALSE);
		}
		App* app = GetApp();
		if(!gp_proc_return_write(&proc_return))
		{	g_message("plugin_handle_proc_run: ERROR");
			ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
			return FALSE;
		}
		plugin_args_destroy(args, proc_run->nparams, FALSE);
		plugin_args_destroy(return_vals, (proc_rec ? (proc_rec->num_values + 1) : 1), TRUE);
#ifdef BAD_FREE_STILL_IN_USE
		plugin_params_destroy(proc_return.params, proc_return.nparams, FALSE);
#endif
	}
#ifdef HEAVY 
	else
	{	blocked = g_new(PlugInBlocked, 1);
	App* app = GetApp();
		blocked->plugin = app->wire_buffer->current_plugin;
		blocked->proc_name = g_strdup(proc_run->name);
		app->wire_buffer->blocked_plugins = g_slist_prepend(app->wire_buffer->blocked_plugins, blocked);
	}
#endif
	return TRUE;
}

bool
plugin_set_proc_return(GPProcReturn *proc_return)
{//	PlugInBlocked *blocked;
	App* app = GetApp();
	if(!app->wire_buffer->buffer || !app->wire_buffer->current_plugin)
	{	e_puts("ERROR: plugin_set_proc_return <null>");
		return FALSE;
	}
//	if(app->wire_buffer->current_plugin->is_recurse)
	{	app->wire_buffer->current_return_vals = plugin_params_to_args(proc_return->params,
		                                  proc_return->nparams,
		                                  TRUE);
		app->wire_buffer->current_return_nvals = proc_return->nparams;
		return TRUE;
	}
#ifdef HEAVY
		GSList* proc_list; 
		proc_list = app->wire_buffer->blocked_plugins;
		while(proc_list)
		{	blocked = proc_list->data;
			proc_list = proc_list->next;
			if(strcmp(blocked->proc_name, proc_return->name) == 0)
			{	plugin_push(blocked->plugin);
				if(!gp_proc_return_write(app->wire_buffer->current_writefd, proc_return))
				{	g_message(_("plugin_handle_proc_run: ERROR"));
					ClosePluginExternal(app->wire_buffer->current_plugin, TRUE);
					return;
				}
				plugin_pop();
				app->wire_buffer->blocked_plugins = g_slist_remove(app->wire_buffer->blocked_plugins, blocked);
				g_free(blocked->proc_name);
				g_free(blocked);
				break;
			}
		}
	}
#endif
	return TRUE;
}

void
plugin_handle_proc_install(GPProcInstall *proc_install)
{	PluginInternal *plugin = NULL;
	PluginProc *proc;
	ProcData *proc_record = NULL;
	GSList *plugins = NULL;
	GtkMenuEntry entry;
	char *filename = NULL;
	int add_proc_def;
	guint32 i;
#ifdef DEBUG_PLUGIN_BLURB
	printf("%s: %s %s\n",proc_install->name,proc_install->blurb,proc_install->menu_path);
#endif
	App* app = GetApp();
	/*
	 *  Argument checking
	 *   --only sanity check arguments when the plugin requests a menu path
	 */
	if(proc_install->menu_path)
	{	if(strncmp(proc_install->menu_path, "<Toolbox>", 9) == 0)
		{	if((proc_install->nparams < 1) ||
			        (proc_install->params[0].type != PDB_INT32))
			{	g_message(_("plug-in \"%s\" attempted to install pluginProcedures \"%s\" which "
				            "does not take the standard plug-in args"),
				          app->wire_buffer->current_plugin->args[0], proc_install->name);
				return;
			}
		}
		else if(strncmp(proc_install->menu_path, "<Image>", 7) == 0)
		{	if((proc_install->nparams < 3) ||
			        (proc_install->params[0].type != PDB_INT32) ||
			        (proc_install->params[1].type != PDB_IMAGE) ||
			        (proc_install->params[2].type != PDB_DRAWABLE))
			{	g_message(_("plug-in \"%s\" attempted to install pluginProcedures \"%s\" which "
				            "does not take the standard plug-in args"),
				          app->wire_buffer->current_plugin->args[0], proc_install->name);
				return;
			}
		}
		else if(strncmp(proc_install->menu_path, "<sfm_menu>", 10) == 0)
		{	if(((proc_install->nparams < 1) ||
			        (proc_install->params[0].type != PDB_DISPLAY)) &&
			        ((proc_install->nparams < 3) ||
			         (proc_install->params[0].type != PDB_INT32) ||
			         (proc_install->params[1].type != PDB_IMAGE) ||
			         (proc_install->params[2].type != PDB_DRAWABLE)))
			{	g_message(_("plug-in \"%s\" attempted to install pluginProcedures \"%s\" which "
				            "does not take the standard plug-in args"),
				          app->wire_buffer->current_plugin->args[0], proc_install->name);
				return;
			}
		}
		else if(strncmp(proc_install->menu_path, "<Load>", 6) == 0)
		{	if((proc_install->nparams < 3) ||
			        (proc_install->params[0].type != PDB_INT32) ||
			        (proc_install->params[1].type != PDB_STRING) ||
			        (proc_install->params[2].type != PDB_STRING))
			{	g_message(_("plug-in \"%s\" attempted to install pluginProcedures \"%s\" which "
				            "does not take the standard plug-in args"),
				          app->wire_buffer->current_plugin->args[0], proc_install->name);
				return;
			}
		}
		else if(strncmp(proc_install->menu_path, "<Save>", 6) == 0)
		{	if((proc_install->nparams < 5) ||
			        (proc_install->params[0].type != PDB_INT32) ||
			        (proc_install->params[1].type != PDB_IMAGE) ||
			        (proc_install->params[2].type != PDB_DRAWABLE) ||
			        (proc_install->params[3].type != PDB_STRING) ||
			        (proc_install->params[4].type != PDB_STRING))
			{	g_message(_("plug-in \"%s\" attempted to install pluginProcedures \"%s\" which "
				            "does not take the standard plug-in args"),
				          app->wire_buffer->current_plugin->args[0], proc_install->name);
				return;
			}
		}
		else
		{	g_message(_("plug-in \"%s\" attempted to install pluginProcedures \"%s\" in "
			            "an invalid menu location.  Use either \"<Toolbox>\", \"<Image>\", "
			            "\"<Load>\", or \"<Save>\"."),
			          app->wire_buffer->current_plugin->args[0], proc_install->name);
			return;
		}
	}
	/*
	 *  Sanity check for array arguments
	 */
	for(i = 1; i < proc_install->nparams; i++)
	{	if((proc_install->params[i].type == PDB_INT32ARRAY ||
		        proc_install->params[i].type == PDB_INT8ARRAY ||
		        proc_install->params[i].type == PDB_FLOATARRAY ||
		        proc_install->params[i].type == PDB_STRINGARRAY) &&
		        proc_install->params[i-1].type != PDB_INT32)
		{	g_message(_("plugin \"%s\" attempted to install pluginProcedures \"%s\" "
			            "which fails to comply with the array parameter "
			            "passing standard.  Argument %d is noncompliant."),
			          app->wire_buffer->current_plugin->args[0], proc_install->name, i);
			return;
		}
	}
	/*
	 *  Initialization
	 */
	proc = NULL;
	switch(proc_install->type)
	{	case PDB_PLUGIN:
		case PDB_EXTENSION:
			if(!app->wire_buffer || !app->wire_buffer->current_plugin )
			{	puts("Invalid PDB_EXTENSION");
				return;
			}
			plugin = app->wire_buffer->current_plugin->internal;
			filename = plugin->filename;
			plugins = plugin->proc_list;
			break;
		case PDB_TEMPORARY:
			filename = "none";
			/*      plugins = app->wire_buffer->current_plugin->temp_proc_defs;
			*/
			plugins=GetExternalPlugins();
			break;
	}
	while(plugins)
	{	proc = plugins->data;
		plugins = plugins->next;
		if(strcmp(proc->proc_data.name, proc_install->name) == 0)
		{	if(proc_install->type == PDB_TEMPORARY)
			{ plugin_proc_def_remove(proc); }
			else
			{ plugin_proc_def_destroy(proc, TRUE); }
			break;
		}
		proc = NULL;
	}
	add_proc_def = FALSE;
	if(!proc)
	{	add_proc_def = TRUE;
		proc = g_new(PluginProc, 1);
		memset(proc,0,sizeof(*proc));// plugin data created
	}
	proc->plugin_name = CreatePluginName(filename);
	proc->menu_path = g_strdup(proc_install->menu_path);
	proc->accelerator = NULL;
	proc->extensions = NULL;
	proc->prefixes = NULL;
	proc->magics = NULL;
	proc->image_types = g_strdup(proc_install->image_types);
	proc->image_types_val = plugin_image_types_parse(proc->image_types);
	proc_record = &proc->proc_data;
	/*
	 *  The procedural database plugin
	 */
	proc_record->name = g_strdup(proc_install->name);//bug?
	proc_record->blurb = g_strdup(proc_install->blurb);
	proc_record->help = g_strdup(proc_install->help);
	proc_record->author = g_strdup(proc_install->author);
	proc_record->copyright = g_strdup(proc_install->copyright);
	proc_record->date = g_strdup(proc_install->date);
	proc_record->proc_type = proc_install->type;
	proc_record->num_args = proc_install->nparams;
	proc_record->num_values = proc_install->nreturn_params;
	proc_record->args = g_new(ProcArg, proc_record->num_args);
	proc_record->values = g_new(ProcArg, proc_record->num_values);
	for(i = 0; i < proc_record->num_args; i++)
	{	proc_record->args[i].arg_type = proc_install->params[i].type;
		proc_record->args[i].name = g_strdup(proc_install->params[i].name);
		proc_record->args[i].description = g_strdup(proc_install->params[i].description);
	}
	for(i = 0; i < proc_record->num_values; i++)
	{	proc_record->values[i].arg_type = proc_install->return_params[i].type;
		proc_record->values[i].name = g_strdup(proc_install->return_params[i].name);
		proc_record->values[i].description = g_strdup(proc_install->return_params[i].description);
	}
	switch(proc_install->type)
	{	case PDB_PLUGIN:
		case PDB_EXTENSION:
			if(add_proc_def && plugin)
			{	plugin->proc_list = g_slist_prepend(plugin->proc_list, proc);
//				app->wire_buffer->plugin_external_list = g_slist_prepend(GetExternalPlugins(), proc);//bug?
			}

			/*  If there is a menu path specified, create a menu entry  */
			if (proc_install->menu_path)
			{
				entry.path = menu_plugins_translate(proc_install->menu_path);
				entry.accelerator = NULL;
				entry.callback = plugin_callback;
				entry.callback_data = proc_record;
				menus_create(&entry, 1);
			}
			break;
		case PDB_TEMPORARY:
			if(add_proc_def)
			{	/*app->wire_buffer->current_plugin->temp_proc_defs = g_slist_prepend (app->wire_buffer->current_plugin->temp_proc_defs, proc);
				*/
				app->wire_buffer->plugin_external_list = g_slist_prepend(GetExternalPlugins(), proc);
				/*      app->wire_buffer->plugin_external_list = g_slist_append (GetExternalPlugins(), proc);*/
			}
			proc_record->exec_method.temporary.plugin = (void *) app->wire_buffer->current_plugin;
			procedural_db_register(proc_record);
			/*  If there is a menu path specified, create a menu entry  */
			if(proc_install->menu_path)
			{	entry.path = menu_plugins_translate(proc_install->menu_path);
				entry.accelerator = NULL;
				entry.callback = plugin_callback;
				entry.callback_data = proc_record;
				menus_create(&entry, 1);
			}
			break;
	}
}

static void
plugin_handle_proc_uninstall(GPProcUninstall *proc_uninstall)
{	PluginProc *proc;
	GSList *plugins;
	/*  plugins = app->wire_buffer->current_plugin->temp_proc_defs;
	*/
	App* app = GetApp();
	plugins=GetExternalPlugins();
	while(plugins)
	{	proc = plugins->data;
		plugins = plugins->next;
		if(strcmp(proc->proc_data.name, proc_uninstall->name) == 0)
		{	/*	  app->wire_buffer->current_plugin->temp_proc_defs = g_slist_remove (app->wire_buffer->current_plugin->temp_proc_defs, proc);
			*/
			app->wire_buffer->plugin_external_list = g_slist_remove(GetExternalPlugins(), proc);
			plugin_proc_def_remove(proc);
			break;
		}
	}
}

static void
plugin_push(PluginLoader *plugin)
{
	if (!plugin)
	{	return;
	}
	App* app = GetApp();
	app->wire_buffer->current_plugin = plugin;
#ifdef HEAVY 
	app->wire_buffer->plugin_stack = g_slist_prepend(app->wire_buffer->plugin_stack, app->wire_buffer->current_plugin);
#endif
#if 1
	app->wire_buffer->current_buffer_index = app->wire_buffer->buffer_write_index;
	app->wire_buffer->current_buffer = app->wire_buffer->buffer;
#else
	app->wire_buffer->current_readfd = app->wire_buffer->current_plugin->my_read;
	app->wire_buffer->current_writefd = app->wire_buffer->current_plugin->my_write;
	app->wire_buffer->current_buffer_index = app->wire_buffer->current_plugin->write_buffer_index;
	app->wire_buffer->current_buffer = app->wire_buffer->current_plugin->write_buffer;
#endif
}

#if 0
	else
	{	app->wire_buffer->current_readfd = 0;
		app->wire_buffer->current_writefd = 0;
		app->wire_buffer->current_buffer_index = 0;
		app->wire_buffer->current_buffer = NULL;
	}
}
#endif

static void
plugin_pop()
{
#ifdef HEAVY
	GSList *plugins;
App* app = GetApp();
	if(app->wire_buffer->current_plugin)
	{
		plugins = app->wire_buffer->plugin_stack;
		if(plugins)
		{	app->wire_buffer->plugin_stack = app->wire_buffer->plugin_stack->next;
			plugins->next = NULL;
			g_slist_free(plugins);
		}
	}
	if(app->wire_buffer->plugin_stack)
	{	app->wire_buffer->current_plugin = app->wire_buffer->plugin_stack->data;
		app->wire_buffer->current_buffer_index = app->wire_buffer->buffer_write_index;
		app->wire_buffer->current_buffer = app->wire_buffer->buffer;
	}
	else
	{	app->wire_buffer->current_plugin = NULL;
		app->wire_buffer->current_readfd = 0;
		app->wire_buffer->current_writefd = 0;
		app->wire_buffer->current_buffer_index = 0;
		app->wire_buffer->current_buffer = NULL;
	}
#endif
}

static void
plugin_write_rc_string(FILE *fp,
                        char *str)
{	fputc('"', fp);
	if(str)
		while(*str)
		{	if((*str == '"') || (*str == '\\'))
			{ fputc('\\', fp); }
			fputc(*str, fp);
			str += 1;
		}
	fputc('"', fp);
}

static int
plugin_write_rc(const char *filename)
{
	FILE *fp;
	PluginInternal *plugin;
	PluginProc *proc;
	GSList *plugins, *tmp2;
	int i;
	fp = fopen(filename, "wb");
	if(!fp)
	{ return 0; 
	}
	App* app = GetApp();
	plugins = GetExternalPlugins();
	while(plugins)
	{	plugin = plugins->data;
		plugins = plugins->next;
		if(plugin->proc_list)
		{	fprintf(fp, "(plug-in-def \"%s\" %ld",
			        plugin->filename,
			        (long) plugin->mtime);
			tmp2 = plugin->proc_list;
			if(tmp2)
			{ fprintf(fp, "\n"); }
			while(tmp2)
			{	proc = tmp2->data;
				tmp2 = tmp2->next;
				fprintf(fp, "             (proc-def \"%s\" %d\n",
				        proc->proc_data.name, proc->proc_data.proc_type);
				fprintf(fp, "                       ");
				plugin_write_rc_string(fp, proc->proc_data.blurb);
				fprintf(fp, "\n                       ");
				plugin_write_rc_string(fp, proc->proc_data.help);
				fprintf(fp, "\n                       ");
				plugin_write_rc_string(fp, proc->proc_data.author);
				fprintf(fp, "\n                       ");
				plugin_write_rc_string(fp, proc->proc_data.copyright);
				fprintf(fp, "\n                       ");
				plugin_write_rc_string(fp, proc->proc_data.date);
				fprintf(fp, "\n                       ");
				plugin_write_rc_string(fp, proc->menu_path);
				fprintf(fp, "\n                       ");
				plugin_write_rc_string(fp, proc->extensions);
				fprintf(fp, "\n                       ");
				plugin_write_rc_string(fp, proc->prefixes);
				fprintf(fp, "\n                       ");
				plugin_write_rc_string(fp, proc->magics);
				fprintf(fp, "\n                       ");
				plugin_write_rc_string(fp, proc->image_types);
				fprintf(fp, "\n                       %d %d\n",
				        proc->proc_data.num_args, proc->proc_data.num_values);
				for(i = 0; i < proc->proc_data.num_args; i++)
				{	fprintf(fp, "                       (proc-arg %d ",
					        proc->proc_data.args[i].arg_type);
					plugin_write_rc_string(fp, proc->proc_data.args[i].name);
					fprintf(fp, " ");
					plugin_write_rc_string(fp, proc->proc_data.args[i].description);
					fprintf(fp, ")%s",
					        (proc->proc_data.num_values ||
					         (i < (proc->proc_data.num_args - 1))) ? "\n" : "");
				}
				for(i = 0; i < proc->proc_data.num_values; i++)
				{	fprintf(fp, "                       (proc-arg %d ",
					        proc->proc_data.values[i].arg_type);
					plugin_write_rc_string(fp, proc->proc_data.values[i].name);
					fprintf(fp, " ");
					plugin_write_rc_string(fp, proc->proc_data.values[i].description);
					fprintf(fp, ")%s", (i < (proc->proc_data.num_values - 1)) ? "\n" : "");
				}
				fprintf(fp, ")");
				if(tmp2)
				{ fprintf(fp, "\n"); }
			}
			fprintf(fp, ")\n");
			if(plugins)
			{ fprintf(fp, "\n"); }
		}
	}
	fclose(fp);
	return 1;
}

PluginInternal* GetPlugin(const char* plugin_name)
{	PluginInternal* plugin = 0;
	if(!plugin_name)
	{	puts("bad GetPlugin");
		return 0;
	}
	App* app = GetApp();
	GSList* plugins = GetExternalPlugins();
	while (plugins) 
	{	plugin = plugins->data;
		if(!plugin->plugin_name)
		{	puts("bad plugin");
			return 0;
		}
		if (strcmp(plugin->plugin_name, plugin_name) == 0)
		{	return plugin;
		}
		plugins = plugins->next;
	}
	return 0;
}

char* CreatePluginName(const char* filename)
{
	const char* name = strrchr(filename, '/');
	if (name)
	{
		name++;
	}
	else
	{
		name = filename;
	}
	const int length = 7;
	if (strncmp(name, "plugin-", length) != 0)
	{
		return 0;
	}
	name += length;
	char* plugin_name = strdup(name);
	char* dot = strrchr(plugin_name, '.');
	if (dot)
	{
		*dot = 0;
	}
	return plugin_name;
}

void
plugin_add_external(PluginInternal* plugin)
{	App* app = GetApp();
#ifdef DEBUG_ADD_PLUGIN
	printf("Load plugin %s\n",plugin->plugin_name);
#endif
	app->wire_buffer->plugin_external_list = g_slist_append(GetExternalPlugins(), plugin);
}


int CreatePlugin(char *filename)
{	char *plugin_name = CreatePluginName(filename);
	if(!plugin_name)
	{	return 0; // isn't a plugin, not "plugin-name.dll"
	}
//	s_printf(_("Loading plugin: %s\n"), plugin_name);
	PluginInternal* plugin = GetPlugin(plugin_name);
	if(plugin)
	{	g_print("duplicate plug-in: \"%s\" (skipping)\n", plugin_name);
		return 0;
	}
	plugin = g_new(PluginInternal, 1);
	memset(plugin,0,sizeof(*plugin));
	plugin->plugin_name = plugin_name;
	plugin->filename = filename;// vampiring pointer here, not g_strdup(filename);
	plugin->mtime = datafile_mtime(plugin->filename);
	plugin->query = TRUE;
	plugin_add_external(plugin);
	return 1;
}

static int
plugin_query(char * plugin_name,PluginInternal *plugin)
{	PluginLoader *loader;
	WireMessage msg; 
	WireMsgSetZero(msg);
//	const char* filename = loader->args[0];
	loader = CreatePluginLoader(plugin_name);
	if (!loader)
	{	return 0;
	}
	loader->name = plugin->plugin_name;//GetPluginName(filename);
	loader->is_query = TRUE;
	loader->is_synchronous = TRUE;
	loader->internal = plugin;
	if (!OpenPluginExternal(loader))
	{	return 0;
	}
#if 1
	App* app = GetApp();
	/* This is a hack because _WIN32 has no pipes. Pass the wire_buffer itself. Robin */
	app->wire_buffer->current_readfd=(intptr_t) app->wire_buffer;
#endif
//	plugin_push(loader);
	if(loader->is_open)
	{	return 1;
	}
	if(!wire_read_msg(&msg))
	{	ClosePluginExternal(app->wire_buffer->current_plugin, TRUE); }
	else
	{	plugin_handle_message(&msg);
		wire_destroy(&msg);
	}
//		return 1;
//	}
//	plugin_pop();
	DestroyPluginExternal(loader);
	return 0;
}

static void
plugin_make_menu()
{	GtkMenuEntry entry;
	PluginProc *proc;
	GSList *plugins;
	App* app = GetApp();
	plugins = GetInternalPlugins();
	while(plugins)
	{	proc = plugins->data;
		plugins = plugins->next;
		if(proc->plugin_name && proc->menu_path && (!proc->extensions &&
		        !proc->prefixes &&
		        !proc->magics))
		{	entry.path = menu_plugins_translate(proc->menu_path);
			entry.accelerator = proc->accelerator;
			entry.callback = plugin_callback;
			entry.callback_data = &proc->proc_data;
			menus_create(&entry, 1);
		}
	}
	menus_propagate_plugins_all();
}



void
plugin_make_image_menu(GtkItemFactory *fac, gchar *buff)
{	GtkItemFactoryEntry entry;
	PluginProc *proc;
	GSList *plugins;
	App* app = GetApp();
	plugins = GetInternalPlugins();
	while(plugins)
	{	proc = plugins->data;
		plugins = plugins->next;
		if(proc->plugin_name && proc->menu_path && (!proc->extensions &&
		        !proc->prefixes &&
		        !proc->magics && proc->image_types))
		{	if(strncmp(proc->menu_path, "<Image>", 6) == 0)
			{	entry.path = menu_plugins_translate(index(proc->menu_path, '/'));
				entry.accelerator = proc->accelerator;
				entry.callback = plugin_callback;
				entry.item_type = "<Item>";
#           if 0
				menus_create(&entry, 1);
#           else
				entry.path = menu_entry_translate(entry.path);
#           ifdef DEBUG_
				printf("%s:%d: %s | %s\n", __FILE__,__LINE__, _(entry.path), entry.path);
#           endif
				gtk_item_factory_create_items_ac(fac , 1, &entry, (gpointer)&proc->proc_data, 2);
#           endif
			}
			//gtk_item_factory_create_item (fac , &entry, NULL, 2);
		}
	}
}

static void
plugin_callback(GtkWidget *widget,
                 gpointer   client_data)
{	if(!client_data)
	{	return;
	}
	GDisplay *gdisplay;
	ProcData *proc_rec;
	Argument *args;
	int i;
	if(!client_data)
	{ g_warning("%s:%d %s() No ProcData deliverd.",__FILE__,__LINE__,__func__); }
	/* get the active gdisplay */
	gdisplay = gdisplay_active();
	proc_rec = (ProcData*) client_data;
	/* construct the procedures arguments */
	args = g_new(Argument, proc_rec->num_args);
	memset(args, 0, (sizeof(Argument) * proc_rec->num_args));
	/* initialize the argument types */
	for(i = 0; i < proc_rec->num_args; i++)
	{ args[i].arg_type = proc_rec->args[i].arg_type; }
	switch(proc_rec->proc_type)
	{	case PDB_EXTENSION:
			/* initialize the first argument  */
			args[0].value.pdb_int = RUN_INTERACTIVE;
			break;
		case PDB_PLUGIN:
			if(proc_rec->num_args && args[0].arg_type == PDB_DISPLAY)
			{	args[0].value.pdb_int = gdisplay->ID;
			}
			else if(gdisplay)
			{	/* initialize the first 3 plug-in arguments  */
				args[0].value.pdb_int = RUN_INTERACTIVE;
				args[1].value.pdb_int = gdisplay->gimage->ID;
				args[2].value.pdb_int = drawable_ID(gimage_active_drawable(gdisplay->gimage));
			}
			else
			{	g_message(_("Uh-oh, no active gdisplay for the plug-in!"));
				g_free(args);
				return;
			}
			break;
		case PDB_TEMPORARY:
			args[0].value.pdb_int = RUN_INTERACTIVE;
			if(proc_rec->num_args >= 3 &&
			        proc_rec->args[1].arg_type == PDB_IMAGE &&
			        proc_rec->args[2].arg_type == PDB_DRAWABLE)
			{	if(gdisplay)
				{	args[1].value.pdb_int = gdisplay->gimage->ID;
					args[2].value.pdb_int = drawable_ID(gimage_active_drawable(gdisplay->gimage));
				}
				else
				{	g_message(_("Uh-oh, no active gdisplay for the temporary procedure!"));
					g_free(args);
					return;
				}
			}
			break;
		default:
			g_error("Unknown procedure type.");
			break;
	}
	/* run the plug-in procedure */
	plugin_run(proc_rec, args, FALSE, TRUE);
	if(proc_rec->proc_type == PDB_PLUGIN)
	{
		App* app = GetApp();
		app->wire_buffer->last_plugin = proc_rec; 
	}
	g_free(args);
}

static void
plugin_proc_def_insert(PluginProc *procedure)
{	PluginProc *proc;
	GSList *plugins, *prev;
	GSList* proc_list;
	prev = NULL;
	App* app = GetApp();
	plugins = GetExternalPlugins();
	while(plugins)
	{	proc = plugins->data;
		if(strcmp(proc->proc_data.name, procedure->proc_data.name) != 0)
		{	prev = plugins;
			plugins = plugins->next;
			continue;
		}
		plugins->data = procedure;
		if(proc->menu_path)
		{	g_free(proc->menu_path); 
		}
		if(proc->accelerator)
		{ g_free(proc->accelerator); 
		}
		proc->menu_path = proc->menu_path;
		proc->accelerator = proc->accelerator;
		proc->menu_path = NULL;
		proc->accelerator = NULL;
		plugin_proc_def_destroy(proc, FALSE);
		if(!proc->menu_path || (proc->menu_path && (strcmp(proc->menu_path, proc->menu_path) < 0)))
		{	proc_list = g_slist_alloc();
			proc_list->data = proc;
			proc_list->next = plugins;
			if(prev)
			{	prev->next = proc_list; 
			}
			else
			{	app->wire_buffer->plugin_external_list = proc_list; 
			}
		}
		break;
	}
//	app->wire_buffer->plugin_external_list = g_slist_append(GetExternalPlugins(), proc);
}

static void
plugin_proc_def_remove(PluginProc *proc)
{	if(!proc)
	{	return;
	}
	/*  Destroy the menu item  */
	if(proc->menu_path)
	{ menus_destroy(proc->menu_path); }
	/*  Un.cthe procedural database entry  */
	procedural_db_unregister(proc->proc_data.name);
	/*  Remove the defintion from the global list  */
	App* app = GetApp();
	app->wire_buffer->plugin_external_list = g_slist_remove(GetExternalPlugins(), proc);
	/*  Destroy the definition  */
	plugin_proc_def_destroy(proc, TRUE);
}

static void
plugin_proc_def_destroy(PluginProc *proc,
                         int            data_only)
{	int i;
	if(proc->plugin_name)
	{ g_free(proc->plugin_name); }
	if(proc->menu_path)
	{ g_free(proc->menu_path); }
	if(proc->accelerator)
	{ g_free(proc->accelerator); }
	if(proc->extensions)
	{ g_free(proc->extensions); }
	if(proc->prefixes)
	{ g_free(proc->prefixes); }
	if(proc->magics)
	{ g_free(proc->magics); }
	if(proc->image_types)
	{ g_free(proc->image_types); }
	if(proc->proc_data.name)
	{ g_free(proc->proc_data.name); }
	if(proc->proc_data.blurb)
	{ g_free(proc->proc_data.blurb); }
	if(proc->proc_data.help)
	{ g_free(proc->proc_data.help); }
	if(proc->proc_data.author)
	{ g_free(proc->proc_data.author); }
	if(proc->proc_data.copyright)
	{ g_free(proc->proc_data.copyright); }
	if(proc->proc_data.date)
	{ g_free(proc->proc_data.date); }
	for(i = 0; i < proc->proc_data.num_args; i++)
	{	if(proc->proc_data.args[i].name)
		{ g_free(proc->proc_data.args[i].name); }
		if(proc->proc_data.args[i].description)
		{ g_free(proc->proc_data.args[i].description); }
	}
	for(i = 0; i < proc->proc_data.num_values; i++)
	{	if(proc->proc_data.values[i].name)
		{ g_free(proc->proc_data.values[i].name); }
		if(proc->proc_data.values[i].description)
		{ g_free(proc->proc_data.values[i].description); }
	}
	if(proc->proc_data.args)
	{ g_free(proc->proc_data.args); }
	if(proc->proc_data.values)
	{ g_free(proc->proc_data.values); }
	if(!data_only)
	{ g_free(proc); }
}

static Argument *
plugin_temp_run(ProcData *proc_rec,
                 Argument   *args)
{	Argument *return_vals;
	PluginLoader *plugin;
	GPProcRun proc_run;
	gint old_recurse;
	return_vals = NULL;
	plugin = (PluginLoader *) proc_rec->exec_method.temporary.plugin;
	App* app = GetApp();
	if(plugin)
	{	if(plugin->is_busy)
		{	return_vals = procedural_db_return_args(proc_rec, FALSE);
			goto done;
		}
		plugin->is_busy = TRUE;
		plugin_push(plugin);
		proc_run.name = proc_rec->name;
		proc_run.nparams = proc_rec->num_args;
		proc_run.params = plugin_args_to_params(args, proc_rec->num_args, FALSE);
		if(!gp_temp_proc_run_write(&proc_run) ||
		        !wire_flush(app->wire_buffer->current_writefd))
		{	return_vals = procedural_db_return_args(proc_rec, FALSE);
			goto done;
		}
		plugin_pop();
		plugin_params_destroy(proc_run.params, proc_run.nparams, FALSE);
		old_recurse = plugin->is_recurse;
		plugin->is_recurse = TRUE;
		gtk_main();
		return_vals = plugin_get_current_return_vals(proc_rec);
		plugin->is_recurse = old_recurse;
		plugin->is_busy = FALSE;
	}
done:
	return return_vals;
}

static Argument*
plugin_params_to_args(GParam *params,
                       int      nparams,
                       int      full_copy)
{	Argument *args;
	gchar **stringarray;
	guchar *colorarray;
	int count;
	int i, j;
	if(nparams == 0)
	{ return NULL; }
	args = g_new(Argument, nparams);
	for(i = 0; i < nparams; i++)
	{	args[i].arg_type = params[i].type;
		switch(args[i].arg_type)
		{	case PDB_INT32:
				args[i].value.pdb_int = params[i].data.d_int32;
				break;
			case PDB_INT16:
				args[i].value.pdb_int = params[i].data.d_int16;
				break;
			case PDB_INT8:
				args[i].value.pdb_int = params[i].data.d_int8;
				break;
			case PDB_FLOAT:
				args[i].value.pdb_float = params[i].data.d_float;
				break;
			case PDB_STRING:
				if(full_copy)
				{ args[i].value.pdb_pointer = g_strdup(params[i].data.d_string); }
				else
				{ args[i].value.pdb_pointer = params[i].data.d_string; }
				break;
			case PDB_INT32ARRAY:
				if(full_copy)
				{	count = args[i-1].value.pdb_int;
					args[i].value.pdb_pointer = g_new(gint32, count);
					memcpy(args[i].value.pdb_pointer, params[i].data.d_int32array, count * 4);
				}
				else
				{	args[i].value.pdb_pointer = params[i].data.d_int32array;
				}
				break;
			case PDB_INT16ARRAY:
				if(full_copy)
				{	count = args[i-1].value.pdb_int;
					args[i].value.pdb_pointer = g_new(gint16, count);
					memcpy(args[i].value.pdb_pointer, params[i].data.d_int16array, count * 2);
				}
				else
				{	args[i].value.pdb_pointer = params[i].data.d_int16array;
				}
				break;
			case PDB_INT8ARRAY:
				if(full_copy)
				{	count = args[i-1].value.pdb_int;
					args[i].value.pdb_pointer = g_new(gint8, count);
					memcpy(args[i].value.pdb_pointer, params[i].data.d_int8array, count);
				}
				else
				{	args[i].value.pdb_pointer = params[i].data.d_int8array;
				}
				break;
			case PDB_FLOATARRAY:
				if(full_copy)
				{	count = args[i-1].value.pdb_int;
					args[i].value.pdb_pointer = g_new(gdouble, count);
					memcpy(args[i].value.pdb_pointer, params[i].data.d_floatarray, count * 8);
				}
				else
				{	args[i].value.pdb_pointer = params[i].data.d_floatarray;
				}
				break;
			case PDB_STRINGARRAY:
				if(full_copy)
				{	args[i].value.pdb_pointer = g_new(gchar*, args[i-1].value.pdb_int);
					stringarray = args[i].value.pdb_pointer;
					for(j = 0; j < args[i-1].value.pdb_int; j++)
					{ stringarray[j] = g_strdup(params[i].data.d_stringarray[j]); }
				}
				else
				{	args[i].value.pdb_pointer = params[i].data.d_stringarray;
				}
				break;
			case PDB_COLOR:
				args[i].value.pdb_pointer = g_new(guchar, 3);
				colorarray = args[i].value.pdb_pointer;
				colorarray[0] = params[i].data.d_color.red;
				colorarray[1] = params[i].data.d_color.green;
				colorarray[2] = params[i].data.d_color.blue;
				break;
			case PDB_REGION:
				g_message(_("the \"region\" arg type is not currently supported"));
				break;
			case PDB_DISPLAY:
				args[i].value.pdb_int = params[i].data.d_display;
				break;
			case PDB_IMAGE:
				args[i].value.pdb_int = params[i].data.d_image;
				break;
			case PDB_LAYER:
				args[i].value.pdb_int = params[i].data.d_layer;
				break;
			case PDB_CHANNEL:
				args[i].value.pdb_int = params[i].data.d_channel;
				break;
			case PDB_DRAWABLE:
				args[i].value.pdb_int = params[i].data.d_drawable;
				break;
			case PDB_SELECTION:
				args[i].value.pdb_int = params[i].data.d_selection;
				break;
			case PDB_BOUNDARY:
				args[i].value.pdb_int = params[i].data.d_boundary;
				break;
			case PDB_PATH:
				args[i].value.pdb_int = params[i].data.d_path;
				break;
			case PDB_STATUS:
				args[i].value.pdb_int = params[i].data.d_status;
				break;
			case PDB_END:
				break;
		}
	}
	return args;
}

static GParam*
plugin_args_to_params(Argument *args,
                       int       nargs,
                       int       full_copy)
{	GParam *params;
	gchar **stringarray;
	guchar *colorarray;
	int i, j;
	if(nargs == 0)
	{ return NULL; }
	params = g_new(GParam, nargs);
	for(i = 0; i < nargs; i++)
	{	params[i].type = args[i].arg_type;
		switch(args[i].arg_type)
		{	case PDB_INT32:
				params[i].data.d_int32 = args[i].value.pdb_int;
				break;
			case PDB_INT16:
				params[i].data.d_int16 = args[i].value.pdb_int;
				break;
			case PDB_INT8:
				params[i].data.d_int8 = args[i].value.pdb_int;
				break;
			case PDB_FLOAT:
				params[i].data.d_float = args[i].value.pdb_float;
				break;
			case PDB_STRING:
				if(full_copy)
				{ params[i].data.d_string = g_strdup(args[i].value.pdb_pointer); }
				else
				{ params[i].data.d_string = args[i].value.pdb_pointer; }
				break;
			case PDB_INT32ARRAY:
				if(full_copy)
				{	params[i].data.d_int32array = g_new(gint32, params[i-1].data.d_int32);
					memcpy(params[i].data.d_int32array,
					       args[i].value.pdb_pointer,
					       params[i-1].data.d_int32 * 4);
				}
				else
				{	params[i].data.d_int32array = args[i].value.pdb_pointer;
				}
				break;
			case PDB_INT16ARRAY:
				if(full_copy)
				{	params[i].data.d_int16array = g_new(gint16, params[i-1].data.d_int32);
					memcpy(params[i].data.d_int16array,
					       args[i].value.pdb_pointer,
					       params[i-1].data.d_int32 * 2);
				}
				else
				{	params[i].data.d_int16array = args[i].value.pdb_pointer;
				}
				break;
			case PDB_INT8ARRAY:
				if(full_copy)
				{	params[i].data.d_int8array = g_new(gint8, params[i-1].data.d_int32);
					memcpy(params[i].data.d_int8array,
					       args[i].value.pdb_pointer,
					       params[i-1].data.d_int32);
				}
				else
				{	params[i].data.d_int8array = args[i].value.pdb_pointer;
				}
				break;
			case PDB_FLOATARRAY:
				if(full_copy)
				{	params[i].data.d_floatarray = g_new(gdouble, params[i-1].data.d_int32);
					memcpy(params[i].data.d_floatarray,
					       args[i].value.pdb_pointer,
					       params[i-1].data.d_int32 * 8);
				}
				else
				{	params[i].data.d_floatarray = args[i].value.pdb_pointer;
				}
				break;
			case PDB_STRINGARRAY:
				if(full_copy)
				{	params[i].data.d_stringarray = g_new(gchar*, params[i-1].data.d_int32);
					stringarray = args[i].value.pdb_pointer;
					for(j = 0; j < params[i-1].data.d_int32; j++)
					{ params[i].data.d_stringarray[j] = g_strdup(stringarray[j]); }
				}
				else
				{	params[i].data.d_stringarray = args[i].value.pdb_pointer;
				}
				break;
			case PDB_COLOR:
				colorarray = args[i].value.pdb_pointer;
				if(colorarray)
				{	params[i].data.d_color.red = colorarray[0];
					params[i].data.d_color.green = colorarray[1];
					params[i].data.d_color.blue = colorarray[2];
				}
				else
				{	params[i].data.d_color.red = 0;
					params[i].data.d_color.green = 0;
					params[i].data.d_color.blue = 0;
				}
				break;
			case PDB_REGION:
				g_message(_("the \"region\" arg type is not currently supported"));
				break;
			case PDB_DISPLAY:
				params[i].data.d_display = args[i].value.pdb_int;
				break;
			case PDB_IMAGE:
				params[i].data.d_image = args[i].value.pdb_int;
				break;
			case PDB_LAYER:
				params[i].data.d_layer = args[i].value.pdb_int;
				break;
			case PDB_CHANNEL:
				params[i].data.d_channel = args[i].value.pdb_int;
				break;
			case PDB_DRAWABLE:
				params[i].data.d_drawable = args[i].value.pdb_int;
				break;
			case PDB_SELECTION:
				params[i].data.d_selection = args[i].value.pdb_int;
				break;
			case PDB_BOUNDARY:
				params[i].data.d_boundary = args[i].value.pdb_int;
				break;
			case PDB_PATH:
				params[i].data.d_path = args[i].value.pdb_int;
				break;
			case PDB_STATUS:
				params[i].data.d_status = args[i].value.pdb_int;
				break;
			case PDB_END:
				break;
		}
	}
	return params;
}

static void
plugin_params_destroy(GParam *params,
                       int      nparams,
                       int      full_destroy)
{	int i, j;
	for(i = 0; i < nparams; i++)
	{	switch(params[i].type)
		{	case PDB_INT32:
			case PDB_INT16:
			case PDB_INT8:
			case PDB_FLOAT:
				break;
			case PDB_STRING:
				if(full_destroy)
				{ g_free(params[i].data.d_string); }
				break;
			case PDB_INT32ARRAY:
				if(full_destroy)
				{ g_free(params[i].data.d_int32array); }
				break;
			case PDB_INT16ARRAY:
				if(full_destroy)
				{ g_free(params[i].data.d_int16array); }
				break;
			case PDB_INT8ARRAY:
				if(full_destroy)
				{ g_free(params[i].data.d_int8array); }
				break;
			case PDB_FLOATARRAY:
				if(full_destroy)
				{ g_free(params[i].data.d_floatarray); }
				break;
			case PDB_STRINGARRAY:
				if(full_destroy)
				{	for(j = 0; j < params[i-1].data.d_int32; j++)
					{ g_free(params[i].data.d_stringarray[j]); }
					g_free(params[i].data.d_stringarray);
				}
				break;
			case PDB_COLOR:
				break;
			case PDB_REGION:
				g_message(_("the \"region\" arg type is not currently supported"));
				break;
			case PDB_DISPLAY:
			case PDB_IMAGE:
			case PDB_LAYER:
			case PDB_CHANNEL:
			case PDB_DRAWABLE:
			case PDB_SELECTION:
			case PDB_BOUNDARY:
			case PDB_PATH:
			case PDB_STATUS:
				break;
			case PDB_END:
				break;
		}
	}
	g_free(params);
}

static void
plugin_args_destroy(Argument *args,
                     int       nargs,
                     int       full_destroy)
{	gchar **stringarray;
	int count;
	int i, j;
	for(i = 0; i < nargs; i++)
	{	switch(args[i].arg_type)
		{	case PDB_INT32:
			case PDB_INT16:
			case PDB_INT8:
			case PDB_FLOAT:
				break;
			case PDB_STRING:
				if(full_destroy)
				{   const char* s = args[i].value.pdb_pointer;
					g_free(args[i].value.pdb_pointer); }
				break;
			case PDB_INT32ARRAY:
				if(full_destroy)
				{ g_free(args[i].value.pdb_pointer); }
				break;
			case PDB_INT16ARRAY:
				if(full_destroy)
				{ g_free(args[i].value.pdb_pointer); }
				break;
			case PDB_INT8ARRAY:
				if(full_destroy)
				{ g_free(args[i].value.pdb_pointer); }
				break;
			case PDB_FLOATARRAY:
				if(full_destroy)
				{ g_free(args[i].value.pdb_pointer); }
				break;
			case PDB_STRINGARRAY:
				if(full_destroy)
				{	count = args[i-1].value.pdb_int;
					stringarray = args[i].value.pdb_pointer;
					for(j = 0; j < count; j++)
					{ g_free(stringarray[j]); }
					g_free(args[i].value.pdb_pointer);
				}
				break;
			case PDB_COLOR:
				g_free(args[i].value.pdb_pointer);
				break;
			case PDB_REGION:
				g_message(_("the \"region\" arg type is not currently supported"));
				break;
			case PDB_DISPLAY:
			case PDB_IMAGE:
			case PDB_LAYER:
			case PDB_CHANNEL:
			case PDB_DRAWABLE:
			case PDB_SELECTION:
			case PDB_BOUNDARY:
			case PDB_PATH:
			case PDB_STATUS:
				break;
			case PDB_END:
				break;
		}
		if(full_destroy)
		{	args[i].value.pdb_pointer = 0;
		}
	}
	g_free(args);
}

int
plugin_image_types_parse(char *image_types)
{	int types;
	if(!image_types)
		return (RGB_IMAGE | GRAY_IMAGE | INDEXED_IMAGE |
		        U16_RGB_IMAGE | U16_GRAY_IMAGE |
		        FLOAT_RGB_IMAGE | FLOAT_GRAY_IMAGE |
		        FLOAT16_RGB_IMAGE | FLOAT16_GRAY_IMAGE);
	types = 0;
	while(*image_types)
	{	while(*image_types &&
		        ((*image_types == ' ') ||
		         (*image_types == '\t') ||
		         (*image_types == ',')))
		{ image_types++; }
		if(*image_types)
		{	if(strncmp(image_types, "RGBA", 4) == 0)
			{	types |= RGBA_IMAGE;
				image_types += 4;
			}
			else if(strncmp(image_types, "RGB*", 4) == 0)
			{	types |= RGB_IMAGE | RGBA_IMAGE;
				image_types += 4;
			}
			else if(strncmp(image_types, "RGB", 3) == 0)
			{	types |= RGB_IMAGE;
				image_types += 3;
			}
			else if(strncmp(image_types, "GRAYA", 5) == 0)
			{	types |= GRAYA_IMAGE;
				image_types += 5;
			}
			else if(strncmp(image_types, "GRAY*", 5) == 0)
			{	types |= GRAY_IMAGE | GRAYA_IMAGE;
				image_types += 5;
			}
			else if(strncmp(image_types, "GRAY", 4) == 0)
			{	types |= GRAY_IMAGE;
				image_types += 4;
			}
			else if(strncmp(image_types, "INDEXEDA", 8) == 0)
			{	types |= INDEXEDA_IMAGE;
				image_types += 8;
			}
			else if(strncmp(image_types, "INDEXED*", 8) == 0)
			{	types |= INDEXED_IMAGE | INDEXEDA_IMAGE;
				image_types += 8;
			}
			else if(strncmp(image_types, "INDEXED", 7) == 0)
			{	types |= INDEXED_IMAGE;
				image_types += 7;
			}
			else if(strncmp(image_types, "U16_RGBA", 8) == 0)
			{	types |= U16_RGBA_IMAGE;
				image_types += 8;
			}
			else if(strncmp(image_types, "U16_RGB*", 8) == 0)
			{	types |= U16_RGB_IMAGE | U16_RGBA_IMAGE;
				image_types += 8;
			}
			else if(strncmp(image_types, "U16_RGB", 7) == 0)
			{	types |= U16_RGB_IMAGE;
				image_types += 7;
			}
			else if(strncmp(image_types, "U16_GRAYA", 9) == 0)
			{	types |= U16_GRAYA_IMAGE;
				image_types += 9;
			}
			else if(strncmp(image_types, "U16_GRAY*", 9) == 0)
			{	types |= U16_GRAY_IMAGE | U16_GRAYA_IMAGE;
				image_types += 9;
			}
			else if(strncmp(image_types, "U16_GRAY", 8) == 0)
			{	types |= U16_GRAY_IMAGE;
				image_types += 8;
			}
			else if(strncmp(image_types, "FLOAT_RGBA", 10) == 0)
			{	types |= FLOAT_RGBA_IMAGE;
				image_types += 10;
			}
			else if(strncmp(image_types, "FLOAT_RGB*", 10) == 0)
			{	types |= FLOAT_RGB_IMAGE | FLOAT_RGBA_IMAGE;
				image_types += 10;
			}
			else if(strncmp(image_types, "FLOAT_RGB", 9) == 0)
			{	types |= FLOAT_RGB_IMAGE;
				image_types += 9;
			}
			else if(strncmp(image_types, "FLOAT_GRAYA", 11) == 0)
			{	types |= FLOAT_GRAYA_IMAGE;
				image_types += 11;
			}
			else if(strncmp(image_types, "FLOAT_GRAY*", 11) == 0)
			{	types |= FLOAT_GRAY_IMAGE | FLOAT_GRAYA_IMAGE;
				image_types += 11;
			}
			else if(strncmp(image_types, "FLOAT_GRAY", 10) == 0)
			{	types |= FLOAT_GRAY_IMAGE;
				image_types += 10;
			}
			else if(strncmp(image_types, "FLOAT16_RGBA", 12) == 0)
			{	types |= FLOAT16_RGBA_IMAGE;
				image_types += 12;
			}
			else if(strncmp(image_types, "FLOAT16_RGB*", 12) == 0)
			{	types |= FLOAT16_RGB_IMAGE | FLOAT16_RGBA_IMAGE;
				image_types += 12;
			}
			else if(strncmp(image_types, "FLOAT16_RGB", 11) == 0)
			{	types |= FLOAT16_RGB_IMAGE;
				image_types += 11;
			}
			else if(strncmp(image_types, "FLOAT16_GRAYA", 13) == 0)
			{	types |= FLOAT16_GRAYA_IMAGE;
				image_types += 13;
			}
			else if(strncmp(image_types, "FLOAT16_GRAY*", 13) == 0)
			{	types |= FLOAT16_GRAY_IMAGE | FLOAT16_GRAYA_IMAGE;
				image_types += 13;
			}
			else if(strncmp(image_types, "FLOAT16_GRAY", 12) == 0)
			{	types |= FLOAT16_GRAY_IMAGE;
				image_types += 12;
			}
			else
			{	while(*image_types &&
				        (*image_types != 'R') &&
				        (*image_types != 'G') &&
				        (*image_types != 'I') &&
				        (*image_types != 'U') &&
				        (*image_types != 'F'))
				{ image_types++; }
			}
		}
	}
	return types;
}

static void
plugin_progress_cancel(GtkWidget *widget,
                        PluginLoader    *plugin)
{	plugin->progress = NULL;
#if 1
#else
	DestroyPluginExternal(plugin);
#endif
}

static void
plugin_progress_init(PluginLoader *plugin,
                      char   *message)
{	//GtkWidget *vbox;
	//GtkWidget *button;
	if(!message)
	{ message = plugin->args[0]; }
#ifdef SEPARATE_PROGRESS_BAR
	if(!plugin->progress)
	{	plugin->progress = gtk_dialog_new();
		gtk_window_set_wmclass(GTK_WINDOW(plugin->progress), "plugin_progress", PROGRAM_NAME);
		gtk_window_set_title(GTK_WINDOW(plugin->progress), prune_filename(plugin->args[0]));
		gtk_widget_set_uposition(plugin->progress, progress_x, progress_y);
		layout_connect_window_position(plugin->progress, &progress_x, &progress_y, TRUE);
		minimize_register(plugin->progress);
		gtk_signal_connect(GTK_OBJECT(plugin->progress), "destroy",
		                   (GtkSignalFunc) plugin_progress_cancel,
		                   plugin);
		gtk_container_border_width(GTK_CONTAINER(GTK_DIALOG(plugin->progress)->action_area), 2);
		vbox = gtk_vbox_new(FALSE, 2);
		gtk_container_border_width(GTK_CONTAINER(vbox), 2);
		gtk_box_pack_start(GTK_BOX(GTK_DIALOG(plugin->progress)->vbox), vbox, TRUE, TRUE, 0);
		gtk_widget_show(vbox);
		plugin->progress_label = gtk_label_new(message);
		gtk_misc_set_alignment(GTK_MISC(plugin->progress_label), 0.0, 0.5);
		gtk_box_pack_start(GTK_BOX(vbox), plugin->progress_label, FALSE, TRUE, 0);
		gtk_widget_show(plugin->progress_label);
		plugin->progress_bar = gtk_progress_bar_new();
		gtk_widget_set_usize(plugin->progress_bar, 150, 20);
		gtk_box_pack_start(GTK_BOX(vbox), plugin->progress_bar, TRUE, TRUE, 0);
		gtk_widget_show(plugin->progress_bar);
		button = gtk_button_new_with_label(_("Cancel"));
		gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
		                          (GtkSignalFunc) gtk_widget_destroy,
		                          GTK_OBJECT(plugin->progress));
		GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
		gtk_box_pack_start(GTK_BOX(GTK_DIALOG(plugin->progress)->action_area), button, TRUE, TRUE, 0);
		gtk_widget_grab_default(button);
		gtk_widget_show(button);
		gtk_widget_show(plugin->progress);
	}
	else
	{	gtk_label_set(GTK_LABEL(plugin->progress_label), message);
	}
#else
	if(!plugin->progress)
	{	plugin->progress = (GtkWidget*) 0x1;
		progress_update(message,0.0);
		progress_start();
	}
#endif
}

static void
plugin_progress_update(PluginLoader *plugin,
                        double  percentage)
{
#ifdef SEPARATE_PROGRESS_BAR
	if(!(percentage >= 0.0 && percentage <= 1.0))
	{ return; }
	if(!plugin->progress)
	{ plugin_progress_init(plugin, NULL); }
	gtk_progress_bar_update(GTK_PROGRESS_BAR(plugin->progress_bar), percentage);
#else
	progress_update(plugin->function_name,percentage);
#endif
}

static Argument*
progress_init_invoker(Argument *args)
{	int success = FALSE;
	App* app = GetApp();
	if(app->wire_buffer->current_plugin && app->wire_buffer->current_plugin->is_open)
	{	success = TRUE;
		if(app->no_interface == FALSE)
		{ plugin_progress_init(app->wire_buffer->current_plugin, args[0].value.pdb_pointer); }
	}
	return procedural_db_return_args(&progress_init_proc, success);
}

static Argument*
progress_update_invoker(Argument *args)
{	int success = FALSE;
	App* app = GetApp();
	if(app->wire_buffer->current_plugin && app->wire_buffer->current_plugin->is_open)
	{	success = TRUE;
		if(app->no_interface == FALSE)
		{ plugin_progress_update(app->wire_buffer->current_plugin, args[0].value.pdb_float); }
	}
	return procedural_db_return_args(&progress_update_proc, success);
}

static Argument*
message_invoker(Argument *args)
{	g_message(args[0].value.pdb_pointer, NULL, NULL);
	return procedural_db_return_args(&message_proc, TRUE);
}

static Argument*
message_handler_get_invoker(Argument *args)
{	Argument *return_args;
	return_args = procedural_db_return_args(&message_handler_get_proc, TRUE);
	App* app = GetApp();
	return_args[1].value.pdb_int = app->message_handler;
	return return_args;
}

static Argument*
message_handler_set_invoker(Argument *args)
{	int success = TRUE;
	if((args[0].value.pdb_int >= MESSAGE_BOX) &&
	        (args[0].value.pdb_int <= CONSOLE))
	{
		App* app = GetApp();
		app->message_handler = args[0].value.pdb_int; 
	}
	else
	{ success = FALSE; }
	return procedural_db_return_args(&message_handler_set_proc, success);
}

gint
tag_to_plugin_image_type(
    Tag t
)
{	Alpha alpha = tag_alpha(t);
	Precision p = tag_precision(t);
	Format f = tag_format(t);
	if(!tag_valid(t))
	{ return -1; }
	switch(p)
	{	case PRECISION_U8:
			switch(f)
			{	case FORMAT_RGB:
					return (alpha == ALPHA_YES)?  RGBA_IMAGE: RGB_IMAGE;
				case FORMAT_GRAY:
					return (alpha == ALPHA_YES)?  GRAYA_IMAGE: GRAY_IMAGE;
				case FORMAT_INDEXED:
					return (alpha == ALPHA_YES)?  INDEXEDA_IMAGE: INDEXED_IMAGE;
				default:
					break;
			}
			break;
		case PRECISION_U16:
			switch(f)
			{	case FORMAT_RGB:
					return (alpha == ALPHA_YES)?  U16_RGBA_IMAGE: U16_RGB_IMAGE;
				case FORMAT_GRAY:
					return (alpha == ALPHA_YES)?  U16_GRAYA_IMAGE: U16_GRAY_IMAGE;
				default:
					break;
			}
			break;
		case PRECISION_FLOAT:
			switch(f)
			{	case FORMAT_RGB:
					return (alpha == ALPHA_YES)?  FLOAT_RGBA_IMAGE: FLOAT_RGB_IMAGE;
				case FORMAT_GRAY:
					return (alpha == ALPHA_YES)?  FLOAT_GRAYA_IMAGE: FLOAT_GRAY_IMAGE;
				default:
					break;
			}
			break;
		case PRECISION_FLOAT16:
			switch(f)
			{	case FORMAT_RGB:
					return (alpha == ALPHA_YES)?  FLOAT16_RGBA_IMAGE: FLOAT16_RGB_IMAGE;
				case FORMAT_GRAY:
					return (alpha == ALPHA_YES)?  FLOAT16_GRAYA_IMAGE: FLOAT16_GRAY_IMAGE;
				default:
					break;
			}
			break;
		case PRECISION_BFP:
			switch(f)
			{	case FORMAT_RGB:
					return (alpha == ALPHA_YES)?  BFP_RGBA_IMAGE: BFP_RGB_IMAGE;
				case FORMAT_GRAY:
					return (alpha == ALPHA_YES)?  BFP_GRAYA_IMAGE: BFP_GRAY_IMAGE;
				default:
					break;
			}
			break;
		default:
			break;
	}
	return -1;
}

void FreeProcDef(PluginProc* proc)
{	if(!proc)
	{	return;
	}
	if (proc->proc_data.name)
	{
		g_free(proc->proc_data.name);
	}
	if (proc->menu_path)
	{
		g_free(proc->menu_path);
	}
	if (proc->accelerator)
	{
		g_free(proc->accelerator);
	}
	if (proc->extensions)
	{
		g_free(proc->extensions);
	}
	if (proc->prefixes)
	{
		g_free(proc->prefixes);
	}
	if (proc->magics)
	{
		g_free(proc->magics);
	}
	if (proc->image_types)
	{
		g_free(proc->image_types);
	}
	memset(proc,0,sizeof(*proc));
}
