/* LIBGIMP - CinePaint Library                                                   
 * Copyright (C) 1995-1997 Peter Mattis and Spencer Kimball                
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.             
 *                                                                              
 * This library is distributed in the hope that it will be useful,              
 * but WITHOUT ANY WARRANTY; without even the implied warranty of               
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU            
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */                                                                             
#ifndef __H__
#define __H__

#include <glib.h>
#include "version.h"
#include "../pdb/compat.h"
#include "../wire/wire_types.h"
#include "../wire/libtile.h"
#include "../pdb/float16.h"
#include "../app/unit.h"
#include "../pdb/plugin_pdb.h"
#include "../pdb/unit_pdb.h"
#include "libgimp/gimp.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern const guint cine_major_version;
extern const guint cine_minor_version;
extern const guint cine_micro_version;

DLL_API int cine_main(PluginLoader *plugin);
//DLL_API void set_cine_PLUG_IN_INFO (const GPlugInInfo *p);

extern int   argc;
extern const char **argv;

#if 0
#define // MAIN () int main (int argc, char *argv[]) { printf("%s:%d\n",__FILE__,__LINE__); return plugin_main(argc, argv, &plugin_info); }
#endif

/* The main procedure that should be called with the
 *  'argc' and 'argv' that are passed to "main".
 */
//DLL_API int plugin_main(int argc, char **argv, const GPlugInInfo *p);
//DLL_API int plugin_main(const GPlugInInfo *p);
DLL_API int plugin_main(PluginLoader *plugin);

/* Forcefully causes CinePaint library to exit and
 *  close down its connection to main CinePaint application.
 */
DLL_API void cine_quit();

/* Specify a range of data to be associated with 'id'.
 *  The data will exist for as long as the main gimp
 *  application is running.
 */
DLL_API void cine_set_data (gchar *  id,
		    gpointer data,
		    guint32  length);

/* Retrieve the piece of data stored within the main
 *  CinePaint application specified by 'id'. The data is
 *  stored in the supplied buffer.  Make sure enough
 *  space is allocated.
 */
DLL_API void cine_get_data (gchar *  id,
		    gpointer data);


/* Pops up a dialog box with "message". Useful for status and
 * error reports. If "message" is NULL, do nothing.
 */
DLL_API void cine_message (const char *message);

/* Query CinePaint application's procedural database.
 *  The arguments are regular expressions which select
 *  which procedure names will be returned in 'proc_names'.
 */
DLL_API void cine_query_database (char   *name_regexp,
			  char   *blurb_regexp,
			  char   *help_regexp,
			  char   *author_regexp,
			  char   *copyright_regexp,
			  char   *date_regexp,
			  char   *proc_type_regexp,
			  int    *nprocs,
			  char ***proc_names);

/* Query CinePaint application's procedural database
 *  regarding a particular procedure.
 */
DLL_API gint cine_query_procedure  (char       *proc_name,
			    char      **proc_blurb,
			    char      **proc_help,
			    char      **proc_author,
			    char      **proc_copyright,
			    char      **proc_date,
			    int        *proc_type,
			    int        *nparams,
			    int        *nreturn_vals,
			    GParamDef  **params,
			    GParamDef  **return_vals);

/* Query CinePaint application regarding all open images.
 *  The list of open image id's is returned in 'image_ids'.
 */
gint32* cine_query_images (int *nimages);


/* Install a procedure in the procedure database.
 */
DLL_API gboolean cine_install_procedure (char      *name,
			     char      *blurb,
			     char      *help,
			     char      *author,
			     char      *copyright,
			     char      *date,
			     char      *menu_path,
			     char      *image_types,
			     int        type,
			     int        nparams,
			     int        nreturn_vals,
			     GParamDef *params,
			     GParamDef *return_vals);

/* Install a temporary procedure in the procedure database.
 */
DLL_API void cine_install_temp_proc (char      *name,
			     char      *blurb,
			     char      *help,
			     char      *author,
			     char      *copyright,
			     char      *date,
			     char      *menu_path,
			     char      *image_types,
			     int        type,
			     int        nparams,
			     int        nreturn_vals,
			     GParamDef *params,
			     GParamDef *return_vals,
			     GRunProc   run_proc);

/* Uninstall a temporary procedure
 */
DLL_API void cine_uninstall_temp_proc (char *name);

/* Install a load file format handler in the procedure database.
 */
DLL_API void cine_register_magic_load_handler (char *name,
				       char *extensions,
				       char *prefixes,
				       char *magics);

/* Install a load file format handler in the procedure database.
 */
DLL_API void cine_register_load_handler (char *name,
				 char *extensions,
				 char *prefixes);

/* Install a save file format handler in the procedure database.
 */
DLL_API void cine_register_save_handler (char *name,
				 char *extensions,
				 char *prefixes);

/* Run a procedure in the procedure database. The parameters are
 *  specified via the variable length argument list. The return
 *  values are returned in the 'GParam*' array.
 */
DLL_API GParam* cine_run_procedure (char *name,
			    gint  *nreturn_vals,
			    ...);

/* Run a procedure in the procedure database. The parameters are
 *  specified as an array of GParam.  The return
 *  values are returned in the 'GParam*' array.
 */
DLL_API GParam* cine_run_procedure2 (char   *name,
			     int    *nreturn_vals,
			     int     nparams,
			     GParam *params);

/* Destroy the an array of parameters. This is useful for
 *  destroying the return values returned by a call to
 *  'cine_run_procedure'.
 */
DLL_API void cine_destroy_params (GParam *params,
			  int     nparams);

DLL_API gdouble  cine_gamma        (void);
DLL_API gint     cine_install_cmap (void);
DLL_API gint     cine_use_xshm     (void);
DLL_API guchar*  cine_color_cube   (void);
DLL_API gchar*   cine_gtkrc        (void);

char*      cine_get_default_monitor_profile   ();
char*      cine_get_default_image_profile     ();
char*      cine_get_default_workspace_profile ();
gint32     cine_get_default_intent_profile    ();
gint32     cine_get_default_flags_profile     ();


/****************************************
 *              Images                  *
 ****************************************/

DLL_API gint32     cine_image_new                   (guint      width,
					     guint      height,
					     GImageType type);
DLL_API gboolean       cine_image_delete                (gint32     image_ID);
DLL_API guint      cine_image_width                 (gint32     image_ID);
DLL_API guint      cine_image_height                (gint32     image_ID);
DLL_API GImageType cine_image_base_type             (gint32     image_ID);
gint32     cine_image_floating_selection    (gint32     image_ID);
DLL_API gboolean       cine_image_add_channel           (gint32     image_ID,
                                                     gint32     channel_ID,
                                                     gint       position);
DLL_API gboolean       cine_image_add_layer             (gint32     image_ID,
                                                     gint32     layer_ID,
                                                     gint       position);
DLL_API gboolean       cine_image_add_layer_mask        (gint32     image_ID,
                                                     gint32     layer_ID,
                                                     gint32     mask_ID);
DLL_API gboolean       cine_image_clean_all             (gint32     image_ID);
DLL_API void       cine_image_disable_undo          (gint32     image_ID);
DLL_API void       cine_image_enable_undo           (gint32     image_ID);
DLL_API gint32     cine_image_flatten               (gint32     image_ID);
gint       cine_image_is_layered   	    (gint32     image_ID);
gint       cine_image_dirty_flag   	    (gint32     image_ID);
gboolean       cine_image_lower_channel         (gint32     image_ID,
                                             gint32     channel_ID);
gboolean       cine_image_lower_layer           (gint32     image_ID,
                                             gint32     layer_ID);
gint32     cine_image_merge_visible_layers  (gint32     image_ID,
                                             GimpMergeType       merge_type);
gint32     cine_image_pick_correlate_layer  (gint32     image_ID,
                                             gint       x,
                                             gint       y);
gboolean       cine_image_raise_channel         (gint32     image_ID,
                                             gint32     channel_ID);
gboolean       cine_image_raise_layer           (gint32     image_ID,
                                             gint32     layer_ID);
gboolean       cine_image_remove_channel        (gint32     image_ID,
                                             gint32     channel_ID);
gboolean       cine_image_remove_layer          (gint32     image_ID,
                                             gint32     layer_ID);
gboolean       cine_image_remove_layer_mask     (gint32     image_ID,
                                             gint32     layer_ID,
	GimpMaskApplyMode       mode);
DLL_API gboolean       cine_image_resize        (gint32     image_ID,
                                             guint      new_width,
                                             guint      new_height,
                                             gint       offset_x,
                                             gint       offset_y);
gint32     cine_image_get_active_channel    (gint32     image_ID);
DLL_API gint32     cine_image_get_active_layer      (gint32     image_ID);
DLL_API gint*    cine_image_get_channels          (gint32     image_ID,
                                                     gint      *nchannels);
DLL_API guchar*    cine_image_get_cmap              (gint32     image_ID,
                                                     gint      *ncolors);
gboolean       cine_image_get_component_active  (gint32     image_ID,
                                             GimpChannelType       component);
gboolean       cine_image_get_component_visible (gint32     image_ID,
                                             GimpChannelType       component);
gboolean   cine_image_get_resolution        (gint32     image_ID,
                                             gdouble    *xresolution,
                                             gdouble    *yresolution);

DLL_API char*      cine_image_get_filename          (gint32     image_ID);
DLL_API gint*    cine_image_get_layers            (gint32     image_ID,
                                                     gint      *nlayers);
DLL_API gint32     cine_image_get_selection         (gint32     image_ID);
gboolean       cine_image_set_active_channel    (gint32     image_ID,
                                             gint32     channel_ID);
gboolean       cine_image_set_active_layer      (gint32     image_ID,
                                             gint32     layer_ID);
DLL_API void       cine_image_set_cmap      (gint32     image_ID,
                                             guchar    *cmap,
                                             gint       ncolors);
gboolean       cine_image_set_component_active  (gint32     image_ID,
	GimpChannelType       component,
                                             gboolean       active);
gboolean       cine_image_set_component_visible (gint32     image_ID,
	GimpChannelType       component,
                                             gboolean       visible);
DLL_API gboolean       cine_image_set_filename  (gint32     image_ID,
                                             gchar      *name);
void cine_bfm_set_dir_src    (gint32     disp_ID, char *filename);
void cine_bfm_set_dir_dest    (gint32     disp_ID, char *filename);

void       cine_image_set_icc_profile_by_name (gint32   image_ID,
                                               gchar   *name,
                                               CMSProfileType type);
DLL_API void       cine_image_set_icc_profile_by_mem  (gint32   image_ID,
                                               gint     size,
                                               gchar   *data,
                                               CMSProfileType type);
DLL_API void       cine_image_set_lab_profile         (gint32   image_ID);
void       cine_image_set_xyz_profile         (gint32   image_ID);
void       cine_image_set_srgb_profile        (gint32   image_ID);
DLL_API gboolean   cine_image_has_icc_profile         (gint32   image_ID,
                                               CMSProfileType type);
DLL_API char*      cine_image_get_icc_profile_by_mem  (gint32   image_ID,
                                               gint    *size,
                                               CMSProfileType type);
DLL_API char*      cine_image_get_icc_profile_description (gint32  image_ID,
                                                   CMSProfileType type);
DLL_API char*      cine_image_get_icc_profile_info    (gint32   image_ID,
                                               CMSProfileType type);
char*      cine_image_get_icc_profile_pcs     (gint32   image_ID,
                                               CMSProfileType type);
DLL_API char*      cine_image_get_icc_profile_color_space_name  (gint32   image_ID,
                                                         CMSProfileType type);
char*      cine_image_get_icc_profile_device_class_name (gint32   image_ID,
                                                         CMSProfileType type);

/****************************************
 *             Displays                 *
 ****************************************/

DLL_API gint32 cine_display_new    (gint32 image_ID);
gint32 cine_display_fm    (gint32 image_ID, gint32 disp_ID);
gint32 cine_display_active  ();
void   cine_display_delete (gint32 display_ID);
void   cine_displays_delete_image (gint32 image_ID);
DLL_API void   cine_displays_flush (void);
int        cine_display_get_image_id         ( gint32   display_ID );
gint32     cine_display_get_cms_intent        (gint32   display_ID,
                                               CMSProfileType type);
void       cine_display_set_cms_intent        (gint32   display_ID,
                                               gint32   intent,
                                               CMSProfileType type);
gint32     cine_display_get_cms_flags         (gint32   display_ID);
void       cine_display_set_cms_flags         (gint32   display_ID,
                                               gint32   flags);
gint32     cine_display_is_colormanaged       (gint32   display_ID,
                                               CMSProfileType type);
void       cine_display_set_colormanaged      (gint32   display_ID,
                                               gboolean True_False,
                                               CMSProfileType type);
void       cine_display_all_set_colormanaged  (gboolean True_False);
void       cine_display_image_set_colormanaged(gint32   display_ID,
                                               gboolean True_False);

/****************************************
 *              Layers                  *
 ****************************************/

DLL_API gint32        cine_layer_new                       (gint32        image_ID,
						    const char         *name,
						    guint         width,
						    guint         height,
						    GDrawableType  type,
						    gdouble       opacity,
						    GLayerMode    mode);
gint32        cine_layer_copy                      (gint32        layer_ID);
DLL_API void          cine_layer_delete                    (gint32        layer_ID);
guint         cine_layer_width                     (gint32        layer_ID);
guint         cine_layer_height                    (gint32        layer_ID);
guint         cine_layer_bpp                       (gint32        layer_ID);
GDrawableType cine_layer_type                      (gint32       layer_ID);
DLL_API void          cine_layer_add_alpha                 (gint32        layer_ID);
DLL_API gint32        cine_layer_create_mask               (gint32        layer_ID,
						    gint          mask_type);
DLL_API void          cine_layer_resize                    (gint32        layer_ID,
						    guint         new_width,
						    guint         new_height,
						    gint          offset_x,
						    gint          offset_y);
void          cine_layer_scale                     (gint32        layer_ID,
						    guint         new_width,
						    guint         new_height,
						    gint          local_origin);
DLL_API void          cine_layer_translate                 (gint32        layer_ID,
						    gint          offset_x,
						    gint          offset_y);
DLL_API gint          cine_layer_is_floating_selection     (gint32        layer_ID);
gint32        cine_layer_get_image_id              (gint32        layer_ID);
gint32        cine_layer_get_mask_id               (gint32        layer_ID);
gint          cine_layer_get_apply_mask            (gint32        layer_ID);
gint          cine_layer_get_edit_mask             (gint32        layer_ID);
DLL_API GLayerMode    cine_layer_get_mode                  (gint32        layer_ID);
DLL_API char*         cine_layer_get_name                  (gint32        layer_ID);
DLL_API gdouble       cine_layer_get_opacity               (gint32        layer_ID);
DLL_API gint          cine_layer_get_preserve_transparency (gint32        layer_ID);
gint          cine_layer_get_show_mask             (gint32        layer_ID);
DLL_API gint          cine_layer_get_visible               (gint32        layer_ID);
void          cine_layer_set_apply_mask            (gint32        layer_ID,
						    gint          apply_mask);
void          cine_layer_set_edit_mask             (gint32        layer_ID,
						    gint          edit_mask);
DLL_API void          cine_layer_set_mode                  (gint32        layer_ID,
						    GLayerMode    mode);
DLL_API void          cine_layer_set_name                  (gint32        layer_ID,
						    char         *name);
DLL_API void          cine_layer_set_offsets               (gint32        layer_ID,
						    gint          offset_x,
						    gint          offset_y);
void          cine_layer_set_opacity               (gint32        layer_ID,
						    gdouble       opacity);
DLL_API void          cine_layer_set_preserve_transparency (gint32        layer_ID,
						    gint          preserve_transparency);
void          cine_layer_set_show_mask             (gint32        layer_ID,
						    gint          show_mask);
DLL_API void          cine_layer_set_visible               (gint32        layer_ID,
						    gint          visible);


/****************************************
 *             Channels                 *
 ****************************************/

DLL_API gint32  cine_channel_new             (gint32   image_ID,
				      const char    *name,
				      guint    width,
				      guint    height,
				      gdouble  opacity,
				      guchar  *color);
gint32  cine_channel_copy            (gint32   channel_ID);
void    cine_channel_delete          (gint32   channel_ID);
guint   cine_channel_width           (gint32   channel_ID);
guint   cine_channel_height          (gint32   channel_ID);
gint32  cine_channel_get_image_id    (gint32   channel_ID);
gint32  cine_channel_get_layer_id    (gint32   channel_ID);
void    cine_channel_get_color       (gint32   channel_ID,
				      guchar  *red,
				      guchar  *green,
				      guchar  *blue);
DLL_API char*   cine_channel_get_name        (gint32   channel_ID);
gdouble cine_channel_get_opacity     (gint32   channel_ID);
gint    cine_channel_get_show_masked (gint32   channel_ID);
gint    cine_channel_get_visible     (gint32   channel_ID);
void    cine_channel_set_color       (gint32   channel_ID,
				      guchar   red,
				      guchar   green,
				      guchar   blue);
void    cine_channel_set_name        (gint32   channel_ID,
				      char    *name);
void    cine_channel_set_opacity     (gint32   channel_ID,
				      gdouble  opacity);
void    cine_channel_set_show_masked (gint32   channel_ID,
				      gint     show_masked);
DLL_API void    cine_channel_set_visible     (gint32   channel_ID,
				      gint     visible);


/****************************************
 *             GDrawables                *
 ****************************************/

DLL_API TileDrawable*    cine_drawable_get          (gint32     drawable_ID);
DLL_API void          cine_drawable_detach       (TileDrawable *drawable);
DLL_API void          cine_drawable_flush        (TileDrawable *drawable);
void          cine_drawable_delete       (TileDrawable *drawable);
DLL_API gboolean          cine_drawable_update       (gint32     drawable_ID,
					  gint       x,
					  gint       y,
					  guint      width,
					  guint      height);
DLL_API gboolean          cine_drawable_merge_shadow (gint32     drawable_ID,
					  gint       undoable);
DLL_API gint32        cine_drawable_image_id     (gint32     drawable_ID);
char*         cine_drawable_name         (gint32     drawable_ID);
DLL_API guint         cine_drawable_width        (gint32     drawable_ID);
DLL_API guint         cine_drawable_height       (gint32     drawable_ID);
DLL_API guint         cine_drawable_bpp          (gint32     drawable_ID);
DLL_API guint         cine_drawable_num_channels (gint32     drawable_ID);
DLL_API GDrawableType cine_drawable_type         (gint32     drawable_ID);
DLL_API GPrecisionType cine_drawable_precision    (gint32     drawable_ID);
gint          cine_drawable_visible      (gint32     drawable_ID);
DLL_API gint          cine_drawable_channel      (gint32     drawable_ID);
DLL_API gint          cine_drawable_color        (gint32     drawable_ID);
DLL_API gint          cine_drawable_gray         (gint32     drawable_ID);
DLL_API gint          cine_drawable_has_alpha    (gint32     drawable_ID);
DLL_API gint          cine_drawable_indexed      (gint32     drawable_ID);
DLL_API gint          cine_drawable_layer        (gint32     drawable_ID);
gint          cine_drawable_layer_mask   (gint32     drawable_ID);
DLL_API gint          cine_drawable_mask_bounds  (gint32     drawable_ID,
					  gint      *x1,
					  gint      *y1,
					  gint      *x2,
					  gint      *y2);
DLL_API gboolean          cine_drawable_offsets      (gint32     drawable_ID,
					  gint      *offset_x,
					  gint      *offset_y);
DLL_API gboolean          cine_drawable_fill         (gint32     drawable_ID,
	GimpFillType       fill_type);
void          cine_drawable_set_name     (gint32     drawable_ID,
					  char      *name);
void          cine_drawable_set_visible  (gint32     drawable_ID,
					  gint       visible);
DLL_API GTile*        cine_drawable_get_tile     (TileDrawable *drawable,
					  gint       shadow,
					  gint       row,
					  gint       col);
GTile*        cine_drawable_get_tile2    (TileDrawable *drawable,
					  gint       shadow,
					  gint       x,
					  gint       y);

/****************************************
 *           Pixel Regions              *
 ****************************************/

DLL_API void     cine_pixel_rgn_init      (GPixelRgn *pr,
				   TileDrawable *drawable,
				   int        x,
				   int        y,
				   int        width,
				   int        height,
				   int        dirty,
				   int        shadow);
void     cine_pixel_rgn_resize    (GPixelRgn *pr,
				   int        x,
				   int        y,
				   int        width,
				   int        height);
void     cine_pixel_rgn_get_pixel (GPixelRgn *pr,
				   guchar    *buf,
				   int        x,
				   int        y);
DLL_API void     cine_pixel_rgn_get_row   (GPixelRgn *pr,
				   guchar    *buf,
				   int        x,
				   int        y,
				   int        width);
DLL_API void     cine_pixel_rgn_get_col   (GPixelRgn *pr,
				   guchar    *buf,
				   int        x,
				   int        y,
				   int        height);
DLL_API void     cine_pixel_rgn_get_rect  (GPixelRgn *pr,
				   guchar    *buf,
				   int        x,
				   int        y,
				   int        width,
				   int        height);
void     cine_pixel_rgn_set_pixel (GPixelRgn *pr,
				   guchar    *buf,
				   int        x,
				   int        y);
DLL_API void     cine_pixel_rgn_set_row   (GPixelRgn *pr,
				   guchar    *buf,
				   int        x,
				   int        y,
				   int        width);
DLL_API void     cine_pixel_rgn_set_col   (GPixelRgn *pr,
				   guchar    *buf,
				   int        x,
				   int        y,
				   int        height);
DLL_API void     cine_pixel_rgn_set_rect  (GPixelRgn *pr,
				   guchar    *buf,
				   int        x,
				   int        y,
				   int        width,
				   int        height);
DLL_API gpointer cine_pixel_rgns_register(int        nrgns,
				   ...);
DLL_API gpointer cine_pixel_rgns_process  (gpointer   pri_ptr);


/****************************************
 *            The Palette               *
 ****************************************/

DLL_API gboolean cine_palette_get_background (guchar *red,
				  guchar *green,
				  guchar *blue);
DLL_API gboolean cine_palette_get_foreground (guchar *red,
				  guchar *green,
				  guchar *blue);
DLL_API gboolean cine_palette_set_background (guchar  red,
				  guchar  green,
				  guchar  blue);
DLL_API gboolean cine_palette_set_foreground (guchar  red,
				  guchar  green,
				  guchar  blue);

/****************************************
 *            Gradients                 *
 ****************************************/

char**   cine_gradients_get_list       (gint    *num_gradients);
char*    cine_gradients_get_active     (void);
void     cine_gradients_set_active     (char    *name);
gdouble* cine_gradients_sample_uniform (gint     num_samples);
gdouble* cine_gradients_sample_custom  (gint     num_samples,
					gdouble *positions);

void cine_path_free (GList *path);
DLL_API void cine_ui_init (const gchar *prog_name,gboolean preview);
int cine_run();
DLL_API void   query(void);
DLL_API void   run(gchar* name,
	gint             nparams,
	GimpParam* param,
	gint* nreturn_vals,
	GimpParam** return_vals);
/* --- pnm functions --- */
DLL_API gint32 load_image(gchar* filename);
DLL_API gint   save_image(gchar* filename,
	gint32           image_ID,
	gint32           drawable_ID);
struct GPConfig;
DLL_API void cine_config(struct GPConfig* config);
DLL_API void cine_extension_process(guint timeout);
DLL_API void cine_extension_ack(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __H__ */
