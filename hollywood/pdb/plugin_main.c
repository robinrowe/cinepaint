/* LIBGIMP - CinePaint Library
 * Copyright (C) 1995-1997 Peter Mattis and Spencer Kimball
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <unistd.h>
#include <glib.h>
#include <errno.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if 0
#include <crtdbg.h>
#endif
#include <sys/types.h>
#include <sys/time.h>
#include <dlfcn.h>
#include "config.h"
#include "../app/datadir.h"
#include "../cinepaint1/extra.h"
#include "../wire/protocol.h"
#ifdef HAVE_IPC_H
#include <sys/ipc.h>
#endif
#ifdef HAVE_SHM_H
#include <sys/shm.h>
#endif
#ifdef HAVE_SYS_SELECT_H
#include <sys/select.h>
#endif
#include "plugin_main.h"
#include "../wire/protocol.h"
#include "../wire/wire.h"
#include "../wire/plugin_loader.h"
#include "../pdb/plugin.h"
#include "../wire/wirebuffer.h"
#include "../wire/taskswitch.h"
#include "../wire/iodebug.h"
#include "version.h"
#include "../app/app.h"

//#define DEBUG_RUN_PROC
//#define DEBUG_PROC_INSTALL
//#define DEBUG_PLUGIN_LOADING

/* Rowe: workaround for config.h clash when making win32 and Linux simultaneously. 
*/
#if 0
#undef HAVE_SHM_H
#endif

static RETSIGTYPE cine_signal        (int signum);
//static void       cine_loop          (void);
void       cine_config        (GPConfig *config);
static int       cine_proc_run      (GPProcRun *proc_run);
static void       cine_temp_proc_run (GPProcRun *proc_run);
#if GTK_MAJOR_VERSION >= 1
void      cine_message_func  (const gchar   *log_domain,
                                                 GLogLevelFlags log_level,
                                                 const gchar   *message,
                                                 gpointer       user_data);
#else
void      cine_message_func  (char *str);
#endif

#if 0
extern int _fmode;
#define _readfd 0
#define _writefd 0
#define _shm_ID 1
#define _shm_addr 0
#else
int _readfd = 0;
int _writefd = 0;
int _shm_ID = -1;
guchar *_shm_addr = NULL;
#endif

const guint cine_major_version = MAJOR_VERSION;
const guint cine_minor_version = MINOR_VERSION;
const guint cine_micro_version = MICRO_VERSION;

int   argc = 0;
const char* blank = "";
const char **argv = &blank;

static gdouble _gamma_val;
static gint _install_cmap;
static gint _use_xshm;
static guchar _color_cube[4];

static const char *progname = NULL;
static guint8 write_buffer[WIRE_BUFFER_SIZE];
static guint write_buffer_index = 0;

static GHashTable *temp_proc_ht = NULL;
static int is_quitting;
static GPlugInInfo* plugin_info;

void set_cine_log()
{
#if GTK_MAJOR_VERSION >= 1
  g_log_set_handler("Gdk",
		    G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION,
		    cine_message_func, NULL);

  g_log_set_handler("Gtk",
		    G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION,
		    cine_message_func, NULL);
		    
  g_log_set_handler("GLib",
		    G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION,
		    cine_message_func, NULL);
		    
  g_log_set_handler("GLib-GObject",
		    G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION,
		    cine_message_func, NULL);
		    
  g_log_set_handler("GModule",
		    G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION,
		    cine_message_func, NULL);
		    
  g_log_set_handler("GThread",
		    G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION,
		    cine_message_func, NULL);
  g_log_set_handler(NULL,
		    G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION,
		    cine_message_func, NULL);
#else
  g_set_message_handler ((GPrintFunc) cine_message_func);
#endif
}

int cine_main(PluginLoader *plugin)
{	if (!plugin )
	{	e_puts("ERROR: plugin is <null>");
		return 0;
	}
	if(plugin->function_name)
	{	dlerror();    /* Clear any existing error */
		plugin->function_proc = dlsym(plugin->internal->library, plugin->function_name);
		if(!plugin->function_proc)
		{	printf("dlerror %s:%s: %s\n",plugin->internal->filename,plugin->function_name,dlerror());
			return FALSE;
		}
		return TRUE;
	}
	if (!plugin->plugin_info)
	{	e_puts("ERROR: plugin_info is <null>");
		return 0;
	}
	plugin_info = plugin->plugin_info;
	App* app = GetApp();
#ifdef DEBUG_PLUGIN_LOADING
	printf("Loading plugin: %s\n", plugin->name);
#endif
	if(!app->wire_buffer)
	{	printf("Error: %s invalid wire\n",plugin->name);
		return 0;
	}
	app->wire_buffer->plugin = plugin;
	app->wire_buffer->current_plugin = plugin;
//	_fmode = _O_BINARY;
#ifdef DUPE  
	gp_init ();
#endif
#if 0
  wire_set_reader(wire_memory_read);
  wire_set_writer(wire_memory_write);
  wire_set_flusher(wire_memory_flush);
#endif
	if(plugin->is_open)
	{	cine_run();
		return TRUE;
	}
	plugin_proc query_proc = plugin->plugin_info->query_proc;
	if (!query_proc)
	{	e_puts("ERROR: plugin_info.query_proc is <null>");
		return 0;
	}
	(*query_proc)();
	plugin->is_open = TRUE;
	//	is_quitting=0;
#if 0
	cine_quit (plugin->plugin_info);
  set_cine_log();
  temp_proc_ht = g_hash_table_new (&g_str_hash, &g_str_equal);
	WireBufferClose(GetApp()->wire_buffer);// don't!
// bug: Reset wire offsets here?
#endif
  return TRUE;
}

void 
cine_quit ()
{	if(!plugin_info)
	{	return;
	}
	plugin_proc quit_proc = plugin_info->quit_proc;
  if (quit_proc)
    (*quit_proc) ();

#ifdef HAVE_SHM_H
  if ((_shm_ID != -1) && _shm_addr)
    shmdt ((char*) _shm_addr);
#endif
//	d_puts(" cine_quit");
//  gp_quit_write (_writefd);
///	PluginDoWireMsg();
  is_quitting=1;
}

void
cine_set_data (gchar *  id,
	       gpointer data,
	       guint32  length)
{
  GParam *return_params;
  int nreturn_params;

  return_params = cine_run_procedure ("cine_procedural_db_set_data",
				    &nreturn_params,
				    PARAM_STRING, id,
				    PARAM_INT32, length,
				    PARAM_INT8ARRAY, data,
				    PARAM_END);

  cine_destroy_params (return_params, nreturn_params);
}

void
cine_get_data (gchar *  id,
	       gpointer data)
{
  GParam *return_params;
  int nreturn_params;
  int length;
  gchar *returned_data;

  return_params = cine_run_procedure ("cine_procedural_db_get_data",
				    &nreturn_params,
				    PARAM_STRING, id,
				    PARAM_END);

  if (return_params[0].data.d_status == STATUS_SUCCESS)
    {
      length = return_params[1].data.d_int32;
      returned_data = (gchar *) return_params[2].data.d_int8array;

      memcpy (data, returned_data, length);
    }

  cine_destroy_params (return_params, nreturn_params);
}

void
cine_message (const char *message)
{
  GParam *return_params;
  int nreturn_params;

  return_params = cine_run_procedure ("cine_message",
				    &nreturn_params,
				    PARAM_STRING, message,
				    PARAM_END);

  cine_destroy_params (return_params, nreturn_params);
}

#if GTK_MAJOR_VERSION >= 1
void
cine_message_func (const gchar    *log_domain,
		   GLogLevelFlags  log_level,
		   const gchar    *str,
		   gpointer        data)
#else
void
cine_message_func (const gchar  *log_domain)
#endif
{
  cine_message (log_domain);
}

void
cine_query_database (char   *name_regexp,
		     char   *blurb_regexp,
		     char   *help_regexp,
		     char   *author_regexp,
		     char   *copyright_regexp,
		     char   *date_regexp,
		     char   *proc_type_regexp,
		     int    *nprocs,
		     char ***proc_names)
{
  GParam *return_params;
  int nreturn_params;
  int i;

  return_params = cine_run_procedure ("cine_procedural_db_query",
				    &nreturn_params,
				    PARAM_STRING, name_regexp,
				    PARAM_STRING, blurb_regexp,
				    PARAM_STRING, help_regexp,
				    PARAM_STRING, author_regexp,
				    PARAM_STRING, copyright_regexp,
				    PARAM_STRING, date_regexp,
				    PARAM_STRING, proc_type_regexp,
				    PARAM_END);

  *nprocs = 0;
  *proc_names = NULL;

  if (return_params[0].data.d_status == STATUS_SUCCESS)
    {
      *nprocs = return_params[1].data.d_int32;
      *proc_names = g_new (gchar*, *nprocs);

      for (i = 0; i < *nprocs; i++)
	(*proc_names)[i] = g_strdup (return_params[2].data.d_stringarray[i]);
    }

  cine_destroy_params (return_params, nreturn_params);
}

gint
cine_query_procedure (char      *proc_name,
		      char     **proc_blurb,
		      char     **proc_help,
		      char     **proc_author,
		      char     **proc_copyright,
		      char     **proc_date,
		      int       *proc_type,
		      int       *nparams,
		      int       *nreturn_params,
		      GParamDef **params,
		      GParamDef **return_params)
{
  GParam *ret_vals;
  int nret_vals;
  int i;
  int success = TRUE;

  ret_vals = cine_run_procedure ("cine_procedural_db_proc_info",
				 &nret_vals,
				 PARAM_STRING, proc_name,
				 PARAM_END);

  if (ret_vals[0].data.d_status == STATUS_SUCCESS)
    {
      *proc_blurb = g_strdup (ret_vals[1].data.d_string);
      *proc_help = g_strdup (ret_vals[2].data.d_string);
      *proc_author = g_strdup (ret_vals[3].data.d_string);
      *proc_copyright = g_strdup (ret_vals[4].data.d_string);
      *proc_date = g_strdup (ret_vals[5].data.d_string);
      *proc_type = ret_vals[6].data.d_int32;
      *nparams = ret_vals[7].data.d_int32;
      *nreturn_params = ret_vals[8].data.d_int32;
      *params = g_new (GParamDef, *nparams);
      *return_params = g_new (GParamDef, *nreturn_params);

      for (i = 0; i < *nparams; i++)
	{
	  GParam *rvals;
	  int nrvals;

	  rvals = cine_run_procedure ("cine_procedural_db_proc_arg",
				      &nrvals,
				      PARAM_STRING, proc_name,
				      PARAM_INT32, i,
				      PARAM_END);

	  if (rvals[0].data.d_status == STATUS_SUCCESS)
	    {
	      (*params)[i].type = rvals[1].data.d_int32;
	      (*params)[i].name = g_strdup (rvals[2].data.d_string);
	      (*params)[i].description = g_strdup (rvals[3].data.d_string);
	    }
	  else
	    {
	      g_free (*params);
	      g_free (*return_params);
	      cine_destroy_params (rvals, nrvals);
	      return FALSE;
	    }

	  cine_destroy_params (rvals, nrvals);
	}

      for (i = 0; i < *nreturn_params; i++)
	{
	  GParam *rvals;
	  int nrvals;

	  rvals = cine_run_procedure ("cine_procedural_db_proc_val",
				      &nrvals,
				      PARAM_STRING, proc_name,
				      PARAM_INT32, i,
				      PARAM_END);

	  if (rvals[0].data.d_status == STATUS_SUCCESS)
	    {
	      (*return_params)[i].type = rvals[1].data.d_int32;
	      (*return_params)[i].name = g_strdup (rvals[2].data.d_string);
	      (*return_params)[i].description = g_strdup (rvals[3].data.d_string);
	    }
	  else
	    {
	      g_free (*params);
	      g_free (*return_params);
	      cine_destroy_params (rvals, nrvals);
	      return FALSE;
	    }

	  cine_destroy_params (rvals, nrvals);
	}
    }
  else
    success = FALSE;

  cine_destroy_params (ret_vals, nret_vals);

  return success;
}

gint32*
cine_query_images (int *nimages)
{
  GParam *return_params;
  int nreturn_params;
  gint32 *images;

  return_params = cine_run_procedure ("cine_list_images",
				    &nreturn_params,
				    PARAM_END);

  images = NULL;
  if (return_params[0].data.d_status == STATUS_SUCCESS)
    {
      *nimages = return_params[1].data.d_int32;
      images = g_new (gint32, *nimages);
      memcpy (images, return_params[2].data.d_int32array, *nimages * 4);
    }

  cine_destroy_params (return_params, nreturn_params);

  return images;
}

gboolean cine_install_procedure (char     *name,
			char     *blurb,
			char     *help,
			char     *author,
			char     *copyright,
			char     *date,
			char     *menu_path,
			char     *image_types,
			int       type,
			int       nparams,
			int       nreturn_params,
			GParamDef *params,
			GParamDef *return_params)
{
#ifdef DEBUG_PROC_INSTALL
	printf("proc_install: %s menu: %s\n",name,menu_path?menu_path:"<none>");
#endif
  GPProcInstall proc_install;
  proc_install.name = name;
  proc_install.blurb = blurb;
  proc_install.help = help;
  proc_install.author = author;
  proc_install.copyright = copyright;
  proc_install.date = date;
  proc_install.menu_path = menu_path;
  proc_install.image_types = image_types;
  proc_install.type = type;
  proc_install.nparams = nparams;
  proc_install.nreturn_params = nreturn_params;
  proc_install.params = (GPParamDef*) params;
  proc_install.return_params = (GPParamDef*) return_params;
//  PluginInternal* internatl = GetCurrentPlugin();
  if (!gp_proc_install_write (&proc_install))
  {	cine_quit();
	return false;
	}
///  PluginDoWireMsg();
  return true;
}

void
cine_install_temp_proc (char     *name,
			char     *blurb,
			char     *help,
			char     *author,
			char     *copyright,
			char     *date,
			char     *menu_path,
			char     *image_types,
			int       type,
			int       nparams,
			int       nreturn_params,
			GParamDef *params,
			GParamDef *return_params,
			GRunProc   run_proc)
{
  cine_install_procedure (name, blurb, help, author, copyright, date,
			  menu_path, image_types, type,
			  nparams, nreturn_params, params, return_params);

  /*  Insert the temp proc run function into the hash table  */
  g_hash_table_insert (temp_proc_ht, (gpointer) name, (gpointer) run_proc);
}

void
cine_uninstall_temp_proc (char *name)
{
  GPProcUninstall proc_uninstall;

  proc_uninstall.name = name;
  if (!gp_proc_uninstall_write (&proc_uninstall))
    cine_quit ();
  g_hash_table_remove (temp_proc_ht, (gpointer) name);
}

void
cine_register_magic_load_handler (char *name,
				  char *extensions,
				  char *prefixes,
				  char *magics)
{
  GParam *return_params;
  int nreturn_params;

  return_params = cine_run_procedure ("cine_register_magic_load_handler",
				    &nreturn_params,
				    PARAM_STRING, name,
				    PARAM_STRING, extensions,
				    PARAM_STRING, prefixes,
				    PARAM_STRING, magics,
				    PARAM_END);

  cine_destroy_params (return_params, nreturn_params);
}

void
cine_register_load_handler (char *name,
			    char *extensions,
			    char *prefixes)
{
  GParam *return_params;
  int nreturn_params;

  return_params = cine_run_procedure ("cine_register_load_handler",
				    &nreturn_params,
				    PARAM_STRING, name,
				    PARAM_STRING, extensions,
				    PARAM_STRING, prefixes,
				    PARAM_END);

  cine_destroy_params (return_params, nreturn_params);
}

void
cine_register_save_handler (char *name,
			    char *extensions,
			    char *prefixes)
{
  GParam *return_params;
  int nreturn_params;

  return_params = cine_run_procedure ("cine_register_save_handler",
				    &nreturn_params,
				    PARAM_STRING, name,
				    PARAM_STRING, extensions,
				    PARAM_STRING, prefixes,
				    PARAM_END);

  cine_destroy_params (return_params, nreturn_params);
}

GParam*
cine_run_procedure (char *name,
		    gint  *nreturn_params,
		    ...)
{
  GPProcRun proc_run;
  GPProcReturn *proc_return = 0;
  WireMessage msg; 
  WireMsgSetError(msg);
  GParamType param_type = 0;
  GParam *return_params = 0;
  va_list args;
  guchar *color = 0;
  unsigned i = 0;
#ifdef DEBUG_RUN_PROC
  d_printf("run_procedure: %s\n",name);
#endif
  proc_run.name = name;
  proc_run.nparams = 0;
  proc_run.params = NULL;

  va_start (args, nreturn_params);
  param_type = va_arg (args, GParamType);

  while (param_type != PARAM_END)
    {
      switch (param_type)
	{
	case PARAM_INT32:
        case PARAM_DISPLAY:
        case PARAM_IMAGE:
        case PARAM_LAYER:
        case PARAM_CHANNEL:
        case PARAM_DRAWABLE:
        case PARAM_SELECTION:
        case PARAM_BOUNDARY:
        case PARAM_PATH:
        case PARAM_STATUS:
	  (void) va_arg (args, int);
	  break;
	case PARAM_INT16:
	  (void) va_arg (args, int);
	  break;
	case PARAM_INT8:
	  (void) va_arg (args, int);
	  break;
        case PARAM_FLOAT:
          (void) va_arg (args, double);
          break;
        case PARAM_STRING:
          (void) va_arg (args, gchar*);
          break;
        case PARAM_INT32ARRAY:
          (void) va_arg (args, gint32*);
          break;
        case PARAM_INT16ARRAY:
          (void) va_arg (args, gint16*);
          break;
        case PARAM_INT8ARRAY:
          (void) va_arg (args, gint8*);
          break;
        case PARAM_FLOATARRAY:
          (void) va_arg (args, gdouble*);
          break;
        case PARAM_STRINGARRAY:
          (void) va_arg (args, gchar**);
          break;
        case PARAM_COLOR:
          (void) va_arg (args, guchar*);
          break;
        case PARAM_REGION:
          break;
	case PARAM_END:
	  break;
	}

      proc_run.nparams += 1;
      param_type = va_arg (args, GParamType);
    }

  va_end (args);

  proc_run.params = g_new (GParam, proc_run.nparams);

  va_start (args, nreturn_params);

  for (i = 0; i < proc_run.nparams; i++)
    {
      proc_run.params[i].type = va_arg (args, GParamType);

      switch (proc_run.params[i].type)
	{
	case PARAM_INT32:
	  proc_run.params[i].data.d_int32 = (gint32) va_arg (args, int);
	  break;
	case PARAM_INT16:
	  proc_run.params[i].data.d_int16 = (gint16) va_arg (args, int);
	  break;
	case PARAM_INT8:
	  proc_run.params[i].data.d_int8 = (gint8) va_arg (args, int);
	  break;
        case PARAM_FLOAT:
          proc_run.params[i].data.d_float = (gdouble) va_arg (args, double);
          break;
        case PARAM_STRING:
          proc_run.params[i].data.d_string = va_arg (args, gchar*);
          break;
        case PARAM_INT32ARRAY:
          proc_run.params[i].data.d_int32array = va_arg (args, gint32*);
          break;
        case PARAM_INT16ARRAY:
          proc_run.params[i].data.d_int16array = va_arg (args, gint16*);
          break;
        case PARAM_INT8ARRAY:
          proc_run.params[i].data.d_int8array = va_arg (args, gint8*);
          break;
        case PARAM_FLOATARRAY:
          proc_run.params[i].data.d_floatarray = va_arg (args, gdouble*);
          break;
        case PARAM_STRINGARRAY:
          proc_run.params[i].data.d_stringarray = va_arg (args, gchar**);
          break;
        case PARAM_COLOR:
	  color = va_arg (args, guchar*);
          proc_run.params[i].data.d_color.red = color[0];
          proc_run.params[i].data.d_color.green = color[1];
          proc_run.params[i].data.d_color.blue = color[2];
          break;
        case PARAM_REGION:
          break;
        case PARAM_DISPLAY:
	  proc_run.params[i].data.d_display = va_arg (args, gint32);
          break;
        case PARAM_IMAGE:
	  proc_run.params[i].data.d_image = va_arg (args, gint32);
          break;
        case PARAM_LAYER:
	  proc_run.params[i].data.d_layer = va_arg (args, gint32);
          break;
        case PARAM_CHANNEL:
	  proc_run.params[i].data.d_channel = va_arg (args, gint32);
          break;
        case PARAM_DRAWABLE:
	  proc_run.params[i].data.d_drawable = va_arg (args, gint32);
          break;
        case PARAM_SELECTION:
	  proc_run.params[i].data.d_selection = va_arg (args, gint32);
          break;
        case PARAM_BOUNDARY:
	  proc_run.params[i].data.d_boundary = va_arg (args, gint32);
          break;
        case PARAM_PATH:
	  proc_run.params[i].data.d_path = va_arg (args, gint32);
          break;
        case PARAM_STATUS:
	  proc_run.params[i].data.d_status = va_arg (args, gint32);
          break;
	case PARAM_END:
	  break;
	}
    }

  va_end (args);

  if (!gp_proc_run_write (&proc_run,TRUE))
  {  g_error("ERROR: gp_proc_run_write failed");
	cine_quit ();
	return 0; //BUG: mem leak
  }
#if 1
///    PluginDoWireMsg();
#else
  if (!wire_read_msg(&msg))
  { g_error("ERROR: wire_read_msg failed");
    cine_quit ();
	return 0;
  }
  plugin_handle_message(&msg);
#endif
#if 1
  if (!wire_read_msg(&msg))
  {
	  g_error("ERROR: wire_read_msg failed");
	  cine_quit();
	  return 0;
  }
  if (msg.msg_type != GP_PROC_RETURN)
  {  g_error ("unexpected message[1]: %d %s\n", msg.msg_type,Get_gp_name(msg.msg_type));
	return 0;
  }
  proc_return = msg.msg_data;
  *nreturn_params = proc_return->nparams;
  return_params = proc_return->params;

  switch (return_params[0].data.d_status)
    {
    case STATUS_EXECUTION_ERROR:
      /*g_warning ("an execution error occured while trying to run: \"%s\"", name);*/
      break;
    case STATUS_CALLING_ERROR:
      g_warning ("a calling error occured while trying to run: \"%s\"", name);
      break;
    default:
      break;
    }
#endif
//  return_params = proc_return->params;
#if 1
  g_free (proc_run.params);
//  g_free (proc_return->name);
  g_free (proc_return);
#endif
  return return_params;
}

GParam*
cine_run_procedure2 (char   *name,
		     int    *nreturn_params,
		     int     nparams,
		     GParam *params)
{
  GPProcRun proc_run;
  GPProcReturn *proc_return;
  WireMessage msg; 
  WireMsgSetZero(msg);
  GParam *return_params;
  proc_run.name = name;
  proc_run.nparams = nparams;
  proc_run.params = params;

  if (!gp_proc_run_write (&proc_run,TRUE))
    cine_quit ();
///  PluginDoWireMsg();
  if(!wire_read_msg(&msg))
    cine_quit ();

  if (msg.msg_type != GP_PROC_RETURN)
  {  g_error ("unexpected message[2]: %d %s\n", msg.msg_type,Get_gp_name(msg.msg_type));
  }
  proc_return = msg.msg_data;
  *nreturn_params = proc_return->nparams;
  return_params = proc_return->params;

  switch (return_params[0].data.d_status)
    {
    case STATUS_EXECUTION_ERROR:

      /*g_warning ("an execution error occured while trying to run: \"%s\"", name);*/
      break;
    case STATUS_CALLING_ERROR:
      g_warning ("a calling error occured while trying to run: \"%s\"", name);
      break;
    default:
      break;
    }

  g_free (proc_return);

  return return_params;
}

void
cine_destroy_params (GParam *params,
		     int    nparams)
{
  //extern void _gp_params_destroy (GPParam *params, int nparams);

  _gp_params_destroy (params, nparams);
}

gdouble
cine_gamma ()
{
  return _gamma_val;
}

gint
cine_install_cmap ()
{
  return _install_cmap;
}

gint
cine_use_xshm ()
{
  return _use_xshm;
}

guchar*
cine_color_cube ()
{
  return _color_cube;
}

gchar*
cine_gtkrc ()
{
  static char filename[PATH_MAX];
  const char *home_dir;

  home_dir = GetPathHome();
  if (!home_dir)
    return NULL;

  sprintf (filename, "%s/%s/gtkrc", home_dir, GetPathDot());

  return filename;
}

void
cine_extension_process (guint timeout)
{
  WireMessage msg; 
  WireMsgSetZero(msg);
  fd_set readfds;
  int select_val;
  struct timeval tv;
  struct timeval *tvp;
  if (timeout)
    {
      tv.tv_sec = timeout / 1000;
      tv.tv_usec = timeout % 1000;
      tvp = &tv;
    }
  else
    tvp = NULL;

  FD_ZERO (&readfds);
#if 0
  FD_SET (&readfds);
#endif
  if ((select_val = select (FD_SETSIZE, &readfds, NULL, NULL, tvp)) > 0)
    {	///PluginDoWireMsg();
    if (!wire_read_msg(&msg))
	cine_quit ();
      switch (msg.msg_type)
	{
	case GP_QUIT:
	  cine_quit ();
	  break;
	case GP_CONFIG:
	  cine_config (msg.msg_data);
	  break;
	case GP_TILE_REQ:
	case GP_TILE_ACK:
	case GP_TILE_DATA:
	  g_warning ("unexpected tile message received (should not happen)\n");
	  break;
	case GP_PROC_RUN:
	  g_warning ("unexpected proc run message received (should not happen)\n");
	  break;
	case GP_PROC_RETURN:
	  g_warning ("unexpected proc return message received (should not happen)\n");
	  break;
	case GP_TEMP_PROC_RUN:
	  cine_temp_proc_run (msg.msg_data);
	  break;
	case GP_TEMP_PROC_RETURN:
	  g_warning ("unexpected temp proc return message received (should not happen)\n");
	  break;
	case GP_PROC_INSTALL:
	  g_warning ("unexpected proc install message received (should not happen)\n");
	  plugin_handle_proc_install(msg.msg_data);
	  break;
	}

      wire_destroy (&msg);
    }
  else if (select_val == -1)
    {
      perror ("cine_process");
      cine_quit ();
    }
}

void
cine_extension_ack ()
{
  /*  Send an extension initialization acknowledgement  */
  if (! gp_extension_ack_write (_writefd))
    cine_quit ();
}

static RETSIGTYPE
cine_signal (int signum)
{
  static int caught_fatal_sig = 0;

  if (caught_fatal_sig)
    kill (getpid (), signum);
  caught_fatal_sig = 1;

  fprintf (stderr, "\n%s: %s caught\n", progname, g_strsignal (signum));

  switch (signum)
    {
    case SIGBUS:
    case SIGSEGV:
    case SIGFPE:
      g_on_error_query (progname);
      break;
    default:
      break;
    }

  cine_quit ();
}

int cine_run()
{
	WireMessage msg; 
	WireMsgSetZero(msg);
	if (!wire_read_msg(&msg))
	{	return FALSE;
	}
	switch (msg.msg_type)
	{
	case GP_QUIT:
		cine_quit();
		break;
	case GP_CONFIG:
		cine_config(msg.msg_data);
		return cine_run();
	case GP_TILE_REQ:
	case GP_TILE_ACK:
	case GP_TILE_DATA:
		g_warning("unexpected tile message received (should not happen)\n");
		break;
	case GP_PROC_RUN:
		if(!cine_proc_run(msg.msg_data))
		{	return FALSE;
		}
//		cine_quit();
		break;
	case GP_PROC_RETURN:
//		g_warning("unexpected proc return message received (should not happen)\n");
		plugin_set_proc_return(msg.msg_data);
		ClosePluginExternal(GetCurrentPlugin(), FALSE);
		break;
	case GP_TEMP_PROC_RUN:
		g_warning("unexpected temp proc run message received (should not happen\n");
		break;
	case GP_TEMP_PROC_RETURN:
		g_warning("unexpected temp proc return message received (should not happen\n");
		break;
	case GP_PROC_INSTALL:
//		g_warning("unexpected proc install message received (should not happen)\n");
		plugin_handle_proc_install(msg.msg_data);
		break;
	}
	return TRUE;
}
#if 0
static void
cine_loop ()
{  /* listening to a plug-in */
#ifndef CINE_LOOP
	cine_run();
#else
  while(cine_run()){}
#endif
}
#endif

void
cine_config (GPConfig *config)
{
  if (config->version < GP_VERSION)
    {
      g_message ("%s: CinePaint is using an older version of the "
		 "plug-in protocol than this plug-in\n", progname);
      cine_quit ();
    }
  else if (config->version > GP_VERSION)
    {
      g_message ("%s: CinePaint is using a newer version of the "
		 "plug-in protocol than this plug-in\n", progname);
      cine_quit ();
    }

//  LIB_TILE_WIDTH = config->tile_width;
//  LIB_TILE_HEIGHT = config->tile_height;
#if 0
  _shm_ID = config->shm_ID;
#endif
  _gamma_val = config->gamma;
  _install_cmap = config->install_cmap;
  _use_xshm = config->use_xshm;
  _color_cube[0] = config->color_cube[0];
  _color_cube[1] = config->color_cube[1];
  _color_cube[2] = config->color_cube[2];
  _color_cube[3] = config->color_cube[3];

#ifdef HAVE_SHM_H
  if (_shm_ID != -1)
    {
      _shm_addr = (guchar*) shmat (_shm_ID, 0, 0);

      if (_shm_addr == (guchar*) -1)
	g_error ("could not attach to CinePaint shared memory segment\n");
    }
#endif
}

static int
cine_proc_run (GPProcRun *proc_run)
{
//  GPProcReturn proc_return;
  GParam *return_params;
  int nreturn_params;
	if(!plugin_info)
	{	return FALSE;
	}
	GRunProc run_proc = plugin_info->run_proc;
  if (!run_proc)
    {	return FALSE;
	}
// Set breakpoint here to trace into plugin:
      (* run_proc) (proc_run->name,
				 proc_run->nparams,
				 (GParam*) proc_run->params,
				 &nreturn_params,
				 &return_params);
#if 1
	GPProcReturn proc_return;
      proc_return.name = proc_run->name;
      proc_return.nparams = nreturn_params;
      proc_return.params = return_params;
      if (!gp_proc_return_write (&proc_return))
	{
		cine_quit ();
		return FALSE;
	}
#endif
// Don't, are Params, not Args:    PrintArgs(proc_run->name,return_params,nreturn_params);
	return TRUE;
}

static void
cine_temp_proc_run (GPProcRun *proc_run)
{
  GPProcReturn proc_return;
  GParam *return_params;
  int nreturn_params;
  GRunProc run_proc;

  run_proc = (GRunProc)g_hash_table_lookup (temp_proc_ht, (gpointer) proc_run->name);

  if (run_proc)
    {
      (* run_proc) (proc_run->name,
		    proc_run->nparams,
		    (GParam*) proc_run->params,
		    &nreturn_params,
		    &return_params);

      proc_return.name = proc_run->name;
      proc_return.nparams = nreturn_params;
      proc_return.params = return_params;
      if (!gp_temp_proc_return_write (&proc_return))
		cine_quit ();
    }
}

/** Rowe moved this from png.c so other plug-ins can use:
 * cine_ui_init:
 * @prog_name: The name of the plug-in which will be passed as argv[0] to
 *             gtk_init(). It's a convention to use the name of the
 *             executable and _not_ the PDB procedure name or something.
 * @preview: #TRUE if the plug-in has some kind of preview in it's UI.
 *           Note that passing #TRUE is recommended also if one of the
 *           used CinePaint Library widgets contains a preview (like the image
 *           menu returned by cine_image_menu_new()).
 *
 * This function initializes GTK+ with gtk_init(), instructs GDK not to
 * use X shared memory if CinePaint was invoked with the --no-xshm command
 * line option and initializes GDK's image rendering subsystem (GdkRGB) to
 * follow CinePaint main program's colormap allocation/installation policy.
 *
 * CinePaint's colormap policy can be determinded by the user with the
 * gimprc variables @min_colors and @install_cmap.
 **/
void
cine_ui_init (const gchar *prog_name,
	      gboolean     preview)
{
  gint    argc;
  gchar **argv;

  static gboolean initialized = FALSE;

  g_return_if_fail (prog_name != NULL);

  if (initialized)
    return;

  argc    = 1;
  argv    = g_new (gchar *, 1);
  argv[0] = g_strdup (prog_name);

  gtk_init (&argc, &argv);
  gtk_rc_parse (cine_gtkrc ());

  /*  It's only safe to switch Gdk SHM usage off  */
#ifdef BUILD_SHM
  if (! cine_use_xshm ())
    gdk_set_use_xshm (FALSE);
#endif

  /*
  gdk_rgb_set_min_colors (cine_min_colors ());
  gdk_rgb_set_install (cine_install_cmap ());
  gtk_widget_set_default_visual (gdk_rgb_get_visual ());
  gtk_widget_set_default_colormap (gdk_rgb_get_cmap ());*/ //FIXME Not sure what this is for

  /*  Set the gamma after installing the colormap because
   *  gtk_preview_set_gamma() initializes GdkRGB if not already done
   */
  if (preview)
    gtk_preview_set_gamma (cine_gamma ());

  initialized = TRUE;
}

