/* CinePaint -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "../app/appenv.h"
#include "color_panel.h"
#include "../app/color_select.h"
#include "../app/colormaps.h"
#include "../app/paint_funcs_area.h"
#include "../app/pixelrow.h"
#include "../app/app.h"

#define EVENT_MASK  GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK

typedef struct ColorPanelPrivate ColorPanelPrivate;

struct ColorPanelPrivate
{
  GtkWidget *drawing_area;
  GdkGC *gc;

  ColorSelectP color_select;
  int color_select_active;
};

static void color_panel_draw (ColorPanel *);
static gint color_panel_events (GtkWidget *area, GdkEvent *event);
static void color_panel_select_callback (PixelRow *, ColorSelectState, void *);


ColorPanel *
color_panel_new (PixelRow * initial,
		 int        width,
		 int        height)
{
  ColorPanel *color_panel;
  ColorPanelPrivate *privateData;

  color_panel = g_new (ColorPanel, 1);
  privateData = g_new (ColorPanelPrivate, 1);
  privateData->color_select = NULL;
  privateData->color_select_active = 0;
  privateData->gc = NULL;
  color_panel->privateData_part = privateData;

  /*  set the initial color  */
  pixelrow_init (&color_panel->color, pixelrow_tag (initial), color_panel->color_data, 1);
  copy_row (initial, &color_panel->color);

  color_panel->color_panel_widget = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (color_panel->color_panel_widget), GTK_SHADOW_IN);

  /*  drawing area  */
  privateData->drawing_area = gtk_drawing_area_new ();
  gtk_drawing_area_size (GTK_DRAWING_AREA (privateData->drawing_area), width, height);
  gtk_widget_set_events (privateData->drawing_area, EVENT_MASK);
  gtk_signal_connect (GTK_OBJECT (privateData->drawing_area), "event",
		      (GtkSignalFunc) color_panel_events,
		      color_panel);
  gtk_object_set_user_data (GTK_OBJECT (privateData->drawing_area), color_panel);
  gtk_container_add (GTK_CONTAINER (color_panel->color_panel_widget), privateData->drawing_area);
  gtk_widget_show (privateData->drawing_area);

  return color_panel;
}

void
color_panel_free (ColorPanel *color_panel)
{
  ColorPanelPrivate *privateData;

  privateData = (ColorPanelPrivate *) color_panel->privateData_part;
  
  /* make sure we hide and free color_select */
  if (privateData->color_select)
    {
      color_select_hide (privateData->color_select);
      color_select_free (privateData->color_select);
    }
  
  if (privateData->gc)
    gdk_gc_destroy (privateData->gc);
  g_free (color_panel->privateData_part);
  g_free (color_panel);
}

static void
color_panel_draw (ColorPanel *color_panel)
{
  GtkWidget *widget;
  ColorPanelPrivate *privateData;
  GdkColor fg;

  privateData = (ColorPanelPrivate *) color_panel->privateData_part;
  widget = privateData->drawing_area;

  fg.pixel = GetColorData()->old_color_pixel;
  store_color (&fg.pixel,
	       &color_panel->color);

  gdk_gc_set_foreground (privateData->gc, &fg);
  gdk_draw_rectangle (widget->window, privateData->gc, 1, 0, 0,
		      widget->allocation.width, widget->allocation.height);
}

static gint
color_panel_events (GtkWidget *widget,
		    GdkEvent  *event)
{
  GdkEventButton *bevent;
  ColorPanel *color_panel;
  ColorPanelPrivate *privateData;
	if(!widget)
	{	return FALSE;
	}

  color_panel = (ColorPanel *) gtk_object_get_user_data (GTK_OBJECT (widget));
  privateData = (ColorPanelPrivate *) color_panel->privateData_part;

  switch (event->type)
    {
    case GDK_EXPOSE:
      if (!privateData->gc)
	privateData->gc = gdk_gc_new (widget->window);

      color_panel_draw (color_panel);
      break;

    case GDK_BUTTON_PRESS:
      bevent = (GdkEventButton *) event;

      if (bevent->button == 1)
	{
	  if (! privateData->color_select)
	    {
	      privateData->color_select = color_select_new (&color_panel->color,
							color_panel_select_callback,
							color_panel,
							FALSE);
	      privateData->color_select_active = 1;
	    }
	  else
	    {
	      if (! privateData->color_select_active)
		color_select_show (privateData->color_select);
	      color_select_set_color (privateData->color_select,
				      &color_panel->color, 1);
	    }
	}
      break;

    default:
      break;
    }

  return FALSE;
}

static void
color_panel_select_callback (PixelRow * col,
			     ColorSelectState state,
			     void *client_data)
{
  ColorPanel *color_panel;
  ColorPanelPrivate *privateData;

  color_panel = (ColorPanel *) client_data;
  privateData = (ColorPanelPrivate *) color_panel->privateData_part;

  if (privateData->color_select)
    {
      switch (state) {
      case COLOR_SELECT_UPDATE:
	break;
      case COLOR_SELECT_OK:
        copy_row (col, &color_panel->color);
	color_panel_draw (color_panel);
	/* Fallthrough */
      case COLOR_SELECT_CANCEL:
	color_select_hide (privateData->color_select);
	privateData->color_select_active = 0;
      }
    }
}
