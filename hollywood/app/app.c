/* CinePaint -- an image manipulation program
 * Copyright (C) 2020 Robin S. Rowe
 * License GPL
 */

#include <gtk/gtk.h>
#include "../liblcms/lcms.h"
#include "cms.h"
#include "datadir.h"
#include "interface.h"
#include "app.h"

#define DEFAULT_WIDTH 1920
#define DEFAULT_HEIGHT 1080
#define GENERIC_WINDOW_X 20
#define GENERIC_WINDOW_Y 20

App app;
ColorData colorData;
RcData rcData;

App* GetApp()
{//	printf("app = Ox%p\n",&app);
	return &app;
}

ColorData* GetColorData()
{	return &colorData;
}

RcData* GetRC()
{	return &rcData;
}


static uintptr_t
procedural_db_hash_func(gconstpointer key)
{
	gchar* string;
	guint result;
	int c;

	/*
	 * I tried a zillion different hash functions and asked many other
	 * people for advice.  Many people had their own favorite functions,
	 * all different, but no-one had much idea why they were good ones.
	 * I chose the one below (multiply by 9 and add new character)
	 * because of the following reasons:
	 *
	 * 1. Multiplying by 10 is perfect for keys that are decimal strings,
	 *    and multiplying by 9 is just about as good.
	 * 2. Times-9 is (shift-left-3) plus (old).  This means that each
	 *    character's bits hang around in the low-order bits of the
	 *    hash value for ever, plus they spread fairly rapidly up to
	 *    the high-order bits to fill out the hash value.  This seems
	 *    works well both for decimal and non-decimal strings.
	 *
	 * tclHashregister --
	 *
	 *      Implementation of in-memory hash tables for Tcl and Tcl-based
	 *      applications.
	 *
	 * Copyright (c) 1991-1993 The Regents of the University of California.
	 * Copyright (c) 1994 Sun Microsystems, Inc.
	 */

	string = (gchar*)key;
	result = 0;
	while (1)
	{
		c = *string;
		string++;
		if (c == 0)
			break;
		result += (result << 3) + c;
	}

	return result;
}

GHashTable* GetPDB()
{	if (!app.pdb)
		app.pdb = g_hash_table_new(procedural_db_hash_func,
			g_str_equal);
//	printf("pdb = Ox%p\n",pdb);
	return app.pdb;
}

void InitRcData();

void InitApp()
{	memset(&app,sizeof(app),0);
	app.max_label_length = PATH_MAX;
	app.dot_path = DOT_PATH;
	app.tips = TIPS_FILE;
	app.console_messages = 1;
	app.initial_frames_loaded = -1;
	app.message_handler = CONSOLE;
	/* Global data */
#if GTK_MAJOR_VERSION > 1
	GdkDevice* current_device = NULL;
#else
	app.current_device = GDK_CORE_POINTER;
#endif
//	app.wire_tile_width = -1;
//	app.wire_tile_height = -1;
//	app.is_app_exit_finish_done = TRUE;
	InitRcData();
	memset(&colorData,0,sizeof(colorData));
};

void InitRcData()
{	/*  global " RC_FILENAME " variables  */
	rcData.plugin_path = NULL;
	rcData.temp_path = NULL;
	rcData.swap_path = NULL;
	rcData.brush_path = NULL;
	rcData.brush_vbr_path = NULL;
	rcData.default_brush = NULL;
	rcData.pattern_path = NULL;
	rcData.default_pattern = NULL;
	rcData.palette_path = NULL;
	rcData.default_palette = NULL;
	rcData.frame_manager_path = NULL;
	rcData.default_frame_manager = NULL;
	rcData.gradient_path = NULL;
	rcData.default_gradient = NULL;
	rcData.pluginrc_path = NULL;
	rcData.cms_profile_path = NULL;
	rcData.look_profile_path = NULL;
	rcData.tile_cache_size = 4194304;  /* 4 MB */
	rcData.marching_speed = 150;   /* 150 ms */
	rcData.gamma_val = 1.0;
	rcData.transparency_type = 1;  /* Mid-Tone Checks */
	rcData.transparency_size = 1;  /* Medium sized */
	rcData.levels_of_undo = 1;     /* 1 level of undo default */
	rcData.color_cube_shades[0] = 6;
	rcData.color_cube_shades[1] = 7;
	rcData.color_cube_shades[2] = 4;
	rcData.color_cube_shades[3] = 24;
	rcData.install_cmap = 0;
	rcData.cycled_marching_ants = 0;
	rcData.default_threshold = .05;
	rcData.stingy_memory_use = 0;
	rcData.allow_resize_windows = 0;
	rcData.no_cursor_updating = 0;
	rcData.preview_size = 64;
	rcData.show_rulers = TRUE;
	rcData.ruler_units = GTK_PIXELS;
	rcData.auto_save = TRUE;
	rcData.cubic_interpolation = FALSE;
	rcData.toolbox_x = 20; 
	rcData.toolbox_y = 20;
	rcData.tips_x = 20; 
	rcData.tips_y = 240;
	rcData.progress_x = 170; rcData.progress_y = 5;
	rcData.info_x = 165; rcData.info_y = 0;
	rcData.color_select_x = 140; rcData.color_select_y = 120;
	rcData.tool_options_x = 0; rcData.tool_options_y = 345;
	rcData.confirm_on_close = TRUE;
	rcData.default_width = DEFAULT_WIDTH;
	rcData.default_height = DEFAULT_HEIGHT;
	rcData.default_format = FORMAT_RGB;
	rcData.default_precision = PRECISION_FLOAT16;
	rcData.show_tips = TRUE;
	rcData.last_tip = -1;
	rcData.show_tool_tips = TRUE;
	rcData.gamut_alarm_red = 255;
	rcData.gamut_alarm_green = 0;
	rcData.gamut_alarm_blue = 255;
	rcData.monitor_xres = 72.0;
	rcData.monitor_yres = 72.0;
	rcData.using_xserver_resolution = FALSE;
	rcData.enable_rgbm_painting = FALSE;
	rcData.enable_brush_dialog = FALSE;
	rcData.enable_layer_dialog = FALSE;
	rcData.enable_brush_display = FALSE;
	rcData.enable_layer_display = FALSE;
	rcData.enable_paste_c_disp = FALSE;
	rcData.enable_tmp_saving = TRUE;
	rcData.enable_channel_revert = FALSE;
	rcData.zoom_window_x = 500;
	rcData.zoom_window_y = 500;
	rcData.frame_manager_x = 600;
	rcData.frame_manager_y = 600;
	rcData.brush_select_x = 700;
	rcData.brush_select_y = 700;
	rcData.brush_edit_x = 400;
	rcData.brush_edit_y = 400;
	rcData.layer_channel_x = 300;
	rcData.layer_channel_y = 300;
	rcData.palette_x = 200;
	rcData.palette_y = 200;
	rcData.gradient_x = 100;
	rcData.gradient_y = 100;
	rcData.generic_window_x = GENERIC_WINDOW_X;
	rcData.generic_window_y = GENERIC_WINDOW_Y;
	rcData.image_x = 100; rcData.image_y = 100;
	rcData.tool_options_visible = FALSE;
	rcData.zoom_window_visible = FALSE;
	rcData.brush_select_visible = FALSE;
	rcData.brush_edit_visible = FALSE;
	rcData.layer_channel_visible = FALSE;
	rcData.color_visible = FALSE;
	rcData.palette_visible = FALSE;
	rcData.gradient_visible = FALSE;
	rcData.cms_default_image_profile_name = NULL;
	rcData.cms_default_proof_profile_name = NULL;
	rcData.cms_workspace_profile_name = NULL;
	rcData.cms_display_profile_name = NULL;
	rcData.cms_default_intent = INTENT_PERCEPTUAL;
	rcData.cms_bpc_by_default = FALSE;
	rcData.cms_default_proof_intent = INTENT_RELATIVE_COLORIMETRIC;
	rcData.cms_default_flags = 0;
	rcData.cms_open_action = 0;
	rcData.cms_mismatch_action = CMS_MISMATCH_AUTO_CONVERT;
	rcData.cms_manage_by_default = FALSE;
#ifdef HAVE_OY
	rcData.cms_oyranos = TRUE;
#else
	rcData.cms_oyranos = FALSE;
#endif
	rcData.enable_rulers_on_start = FALSE;
	tile_cache_init();
}

int
app_exit_finish_done(void)
{
	return app.is_app_exit_finish_done;
}

GSList* GetInternalPlugins()
{	if(!app.wire_buffer)
	{	return 0;
	}
	return app.wire_buffer->plugin_internal_list;
}

GSList* GetExternalPlugins()
{	if (!app.wire_buffer)
	{	return 0;
	}
	return app.wire_buffer->plugin_external_list;
}

struct PluginLoader* GetCurrentPlugin()
{	if (!app.wire_buffer)
	{	return 0;
	}
	return app.wire_buffer->current_plugin;
}


void update_statusbar(const char* msg)
{   App* app = GetApp();
//    gtk_statusbar_push (GTK_STATUSBAR (app->statusbar),1, msg);
    gtk_label_set_text (GTK_LABEL(app->statusbar),msg);
}

void update_statusbar_progress(const char* msg,unsigned percent)
{   if(percent > 100)
    {   return;
    }
    App* app = GetApp();
    if(strlen(msg)>20)
    {   msg = "(Msg too long)";
    }
    char buffer[30];
    sprintf(buffer,"%s: %i%%",msg,percent);
//    itoa (percent,msg,10);
//    strcat(msg,"%");
    update_statusbar(buffer);
}

void create_statusbar(GtkWidget* parent)
{ App* app = GetApp();
  app->statusbar = gtk_label_new("Welcome to CinePaint..."); //gtk_statusbar_new();
	app->statusbar->name = "statusbar";
//  gtk_container_border_width (GTK_CONTAINER (app->statusbar), 1);
  gtk_misc_set_alignment (GTK_MISC(app->statusbar), 0.0, 0.5);
  gtk_misc_set_padding (GTK_MISC(app->statusbar), 2, 2);
  //gtk_label_set_selectable(app->statusbar,TRUE);
  gtk_box_pack_start (GTK_BOX (parent), app->statusbar, TRUE, TRUE, 0);
//  gtk_widget_realize (app->statusbar);
  gtk_widget_show(app->statusbar);
//  update_statusbar("Welcome to CinePaint...");
}

