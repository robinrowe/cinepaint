/* CinePaint -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <glib.h>
#include <string.h>
#include "config.h"
#include "i18n.h"
#include "app_procs.h"
#include "appenv.h"
#include "devices.h"
#include "errors.h"
#include "fileops.h"
#include "general.h"
#include "rc.h"
#include "menus.h"
#include "../pdb/plugin.h"
#include "../app/gimage.h"
#include "tools.h"
#include "../app/app.h"
#include "user_install.h"

enum 
{	RC_ERROR,
	RC_DONE,
	RC_OK,
	RC_EOF
};

int ReturnRC_ERROR()
{
	return RC_ERROR;
}

extern RcData rcData;

#if 0	
#define RC_ERROR  0
#define RC_DONE   1
#define RC_OK     2
#endif 

#define MAX_GIMPDIR_LEN 500

static ParseInfo parse_info;

static int get_next_token (void);
static int peek_next_token (void);
static int parse_statement (void);

static int parse_string (gpointer val1p, gpointer val2p);
static int parse_path (gpointer val1p, gpointer val2p);
static int parse_double (gpointer val1p, gpointer val2p);
static int parse_int (gpointer val1p, gpointer val2p);
static int parse_boolean (gpointer val1p, gpointer val2p);
static int parse_position (gpointer val1p, gpointer val2p);
static int parse_mem_size (gpointer val1p, gpointer val2p);
static int parse_image_type (gpointer val1p, gpointer val2p);
static int parse_color_cube (gpointer val1p, gpointer val2p);
static int parse_preview_size (gpointer val1p, gpointer val2p);
static int parse_ruler_units (gpointer val1p, gpointer val2p);
static int parse_plugin (gpointer val1p, gpointer val2p);
static int parse_plugin_def (gpointer val1p, gpointer val2p);
static int parse_device (gpointer val1p, gpointer val2p);
static int parse_menu_path (gpointer val1p, gpointer val2p);

static int parse_proc_def (PluginProc **proc);
static int parse_proc_arg (ProcArg *arg);
static int parse_unknown (char *token_sym);

static char* value_to_str (char *name);

static char* string_to_str (gpointer val1p, gpointer val2p);
static char* path_to_str (gpointer val1p, gpointer val2p);
static char* double_to_str (gpointer val1p, gpointer val2p);
static char* int_to_str (gpointer val1p, gpointer val2p);
static char* boolean_to_str (gpointer val1p, gpointer val2p);
static char* position_to_str (gpointer val1p, gpointer val2p);
static char* mem_size_to_str (gpointer val1p, gpointer val2p);
static char* image_type_to_str (gpointer val1p, gpointer val2p);
static char* color_cube_to_str (gpointer val1p, gpointer val2p);
static char* preview_size_to_str (gpointer val1p, gpointer val2p);
static char* ruler_units_to_str (gpointer val1p, gpointer val2p);

static char* transform_path (char *path, int destroy);
static char* cinerc_find_token(const char *token);
void cinerc_set_token (char *token, char *value);
Argument * cinerc_query (Argument *args);
static void add_directory_token(const char* name, const char* value);
static char* open_backup_file (char *filename, FILE **fp_new, FILE **fp_old);

static ParseFunc funcs[] =
{
	{ "temp-path", TT_PATH, &rcData.temp_path, NULL },
		{ "swap-path", TT_PATH, &rcData.swap_path, NULL },
		{ "brush-path", TT_PATH, &rcData.brush_path, NULL },
		{ "brush-vbr-path", TT_PATH, &rcData.brush_vbr_path, NULL },
		{ "pattern-path", TT_PATH, &rcData.pattern_path, NULL },
		{ "plug-in-path", TT_PATH, &rcData.plugin_path, NULL },
		{ "palette-path", TT_PATH, &rcData.palette_path, NULL },
		{ "gradient-path", TT_PATH, &rcData.gradient_path, NULL },
		{ "pluginrc-path", TT_PATH, &rcData.pluginrc_path, NULL },
		{ "cms-profile-path", TT_PATH, &rcData.cms_profile_path, NULL },
		{ "default-brush", TT_STRING, &rcData.default_brush, NULL },
		{ "cms-default-image-profile-name", TT_STRING, &rcData.cms_default_image_profile_name, NULL },
		{ "cms-default-proof-profile-name", TT_STRING, &rcData.cms_default_proof_profile_name, NULL },
		{ "cms-workspace-profile-name", TT_STRING, &rcData.cms_workspace_profile_name, NULL },
		{ "cms-display-profile-name", TT_STRING, &rcData.cms_display_profile_name, NULL },
		{ "cms-default-intent", TT_INT, &rcData.cms_default_intent, NULL },
		{ "cms-default-flags", TT_INT, &rcData.cms_default_flags, NULL },
		{ "cms-open-action", TT_INT, &rcData.cms_open_action, NULL },
		{ "cms-mismatch-action", TT_INT, &rcData.cms_mismatch_action, NULL },
		{ "cms-bpc-by-default", TT_BOOLEAN, &rcData.cms_bpc_by_default, NULL },
		{ "cms-manage-by-default", TT_BOOLEAN, &rcData.cms_manage_by_default, NULL },
#ifdef HAVE_OY
		{ "cms-oyranos", TT_INT, &rcData.cms_oyranos, NULL },
#endif
		{ "look-profile-path", TT_STRING, &rcData.look_profile_path, NULL },
		{ "default-pattern", TT_STRING, &rcData.default_pattern, NULL },
		{ "default-palette", TT_STRING, &rcData.default_palette, NULL },
		{ "default-gradient", TT_STRING, &rcData.default_gradient, NULL },
		{ "gamma-correction", TT_DOUBLE, &rcData.gamma_val, NULL },
		{ "color-cube", TT_XCOLORCUBE, NULL, NULL },
		{ "tile-cache-size", TT_MEMSIZE, &rcData.tile_cache_size, NULL },
		{ "marching-ants-speed", TT_INT, &rcData.marching_speed, NULL },
		{ "undo-levels", TT_INT, &rcData.levels_of_undo, NULL },
		{ "transparency-type", TT_INT, &rcData.transparency_type, NULL },
		{ "transparency-size", TT_INT, &rcData.transparency_size, NULL },
		{ "install-colormap", TT_BOOLEAN, &rcData.install_cmap, NULL },
		{ "colormap-cycling", TT_BOOLEAN, &rcData.cycled_marching_ants, NULL },
		{ "default-threshold", TT_DOUBLE, &rcData.default_threshold, NULL },
		{ "stingy-memory-use", TT_BOOLEAN, &rcData.stingy_memory_use, NULL },
		{ "allow-resize-windows", TT_BOOLEAN, &rcData.allow_resize_windows, NULL },
		{ "tooloptions-visible", TT_BOOLEAN, &rcData.tool_options_visible, NULL },
		{ "zoomwindow-visible", TT_BOOLEAN, &rcData.zoom_window_visible, NULL },
		{ "brushselect-visible", TT_BOOLEAN, &rcData.brush_select_visible, NULL },
		{ "brushedit-visible", TT_BOOLEAN, &rcData.brush_edit_visible, NULL },
		{ "layerchannel-visible", TT_BOOLEAN, &rcData.layer_channel_visible, NULL },
		{ "color-visible", TT_BOOLEAN, &rcData.color_visible, NULL },
		{ "palette-visible", TT_BOOLEAN, &rcData.palette_visible, NULL },
		{ "gradient-visible", TT_BOOLEAN, &rcData.gradient_visible, NULL },
		{ "cursor-updating", TT_BOOLEAN, NULL, &rcData.no_cursor_updating },
		{ "no-cursor-updating", TT_BOOLEAN, &rcData.no_cursor_updating, NULL },
		{ "preview-size", TT_XPREVSIZE, NULL, NULL },
		{ "show-rulers", TT_BOOLEAN, &rcData.show_rulers, NULL },
		{ "dont-show-rulers", TT_BOOLEAN, NULL, &rcData.show_rulers },
		{ "ruler-units", TT_XRULERUNIT, NULL, NULL },
		{ "auto-save", TT_BOOLEAN, &rcData.auto_save, NULL },
		{ "dont-auto-save", TT_BOOLEAN, NULL, &rcData.auto_save },
		{ "cubic-interpolation", TT_BOOLEAN, &rcData.cubic_interpolation, NULL },
		{ "framemanager-position", TT_POSITION, &rcData.frame_manager_x, &rcData.frame_manager_y },
		{ "palette-position", TT_POSITION, &rcData.palette_x, &rcData.palette_y },
		{ "zoom-position", TT_POSITION, &rcData.zoom_window_x, &rcData.zoom_window_y},
		{ "brusheditor-position", TT_POSITION, &rcData.brush_edit_x, &rcData.brush_edit_y},
		{ "brushselect-position", TT_POSITION, &rcData.brush_select_x, &rcData.brush_select_y},
		{ "layerchannel-position", TT_POSITION, &rcData.layer_channel_x, &rcData.layer_channel_y}, { "framemanager-position", TT_POSITION,   &rcData.frame_manager_x, &rcData.frame_manager_y },
		{ "palette-position", TT_POSITION, &rcData.palette_x, &rcData.palette_y },
		{ "zoom-position", TT_POSITION, &rcData.zoom_window_x, &rcData.zoom_window_y},
		{ "brusheditor-position", TT_POSITION, &rcData.brush_edit_x, &rcData.brush_edit_y},
		{ "brushselect-position", TT_POSITION, &rcData.brush_select_x, &rcData.brush_select_y},
		{ "layerchannel-position", TT_POSITION, &rcData.layer_channel_x, &rcData.layer_channel_y},
		{ "toolbox-position", TT_POSITION, &rcData.toolbox_x, &rcData.toolbox_y },
		{ "tips-position", TT_POSITION, &rcData.tips_x, &rcData.tips_y },
		{ "progress-position", TT_POSITION, &rcData.progress_x, &rcData.progress_y },
		{ "generic-position", TT_POSITION, &rcData.generic_window_x, &rcData.generic_window_y},
		{ "gradient-position", TT_POSITION, &rcData.gradient_x, &rcData.gradient_y },
		{ "info-position", TT_POSITION, &rcData.info_x, &rcData.info_y },
		{ "image-position", TT_POSITION, &rcData.image_x, &rcData.image_y },
		{ "color-select-position", TT_POSITION, &rcData.color_select_x, &rcData.color_select_y },
		{ "tool-options-position", TT_POSITION, &rcData.tool_options_x, &rcData.tool_options_y },
		{ "confirm-on-close", TT_BOOLEAN, &rcData.confirm_on_close, NULL },
		{ "dont-confirm-on-close", TT_BOOLEAN, NULL, &rcData.confirm_on_close },
		{ "show-tips", TT_BOOLEAN, &rcData.show_tips, NULL },
		{ "dont-show-tips", TT_BOOLEAN, NULL, &rcData.show_tips },
		{ "enable-rgbm-painting", TT_BOOLEAN, &rcData.enable_rgbm_painting, NULL },
		{ "enable-rulers-on-start", TT_BOOLEAN, &rcData.enable_rulers_on_start, NULL },
		{ "dont-enable-rgbm-painting", TT_BOOLEAN, NULL, &rcData.enable_rgbm_painting },
		{ "enable-brush-dialog", TT_BOOLEAN, &rcData.enable_brush_dialog, NULL },
		{ "dont-enable-brush-dialog", TT_BOOLEAN, NULL, &rcData.enable_brush_dialog },
		{ "enable-layer-dialog", TT_BOOLEAN, &rcData.enable_layer_dialog, NULL },
		{ "dont-enable-layer-dialog", TT_BOOLEAN, NULL, &rcData.enable_layer_dialog },
		{ "enable-paste-c-disp", TT_BOOLEAN, &rcData.enable_paste_c_disp, NULL },
		{ "dont-enable-paste-c-disp", TT_BOOLEAN, NULL, &rcData.enable_paste_c_disp },
		{ "enable-tmp-saving", TT_BOOLEAN, &rcData.enable_tmp_saving, NULL },
		{ "dont-enable-tmp-saving", TT_BOOLEAN, NULL, &rcData.enable_tmp_saving },
		{ "enable-channel-revert", TT_BOOLEAN, &rcData.enable_channel_revert, NULL },
		{ "dont-enable-channel-revert", TT_BOOLEAN, NULL, &rcData.enable_channel_revert },
		{ "last-tip-shown", TT_INT, &rcData.last_tip, NULL },
		{ "default-image-size", TT_POSITION, &rcData.default_width, &rcData.default_height },
		{ "default-image-type", TT_IMAGETYPE, &rcData.default_format, NULL },
		{ "default-image-precision", TT_INT, &rcData.default_precision, NULL },
		{ "plug-in", TT_XPLUGIN, NULL, NULL },
		{ "plug-in-def", TT_XPLUGINDEF, NULL, NULL },
		{ "menu-path", TT_XMENUPATH, NULL, NULL },
		{ "device", TT_XDEVICE, NULL, NULL },
		{ "show-tool-tips", TT_BOOLEAN, &rcData.show_tool_tips, NULL },
		{ "dont-show-tool-tips", TT_BOOLEAN, NULL, &rcData.show_tool_tips },
		{ "gamut-alarm-red", TT_INT, &rcData.gamut_alarm_red, NULL },
		{ "gamut-alarm-green", TT_INT, &rcData.gamut_alarm_green, NULL },
		{ "gamut-alarm-blue", TT_INT, &rcData.gamut_alarm_blue, NULL }
};
static int nfuncs = sizeof(funcs) / sizeof(funcs[0]);

char *
cine_directory ()
{
  static char cine_dir[MAX_GIMPDIR_LEN + 1] = "";
  const char *env_cine_dir;
  const char *env_home_dir;
  size_t len_env_home_dir = 0;

  if ('\000' != cine_dir[0])
    return cine_dir;

  env_cine_dir = GetPathDot();
  env_home_dir = GetPathHome();
  if (NULL != env_home_dir)
    len_env_home_dir = strlen (env_home_dir);

  if (NULL != env_cine_dir)
    {
      if ('/' == env_cine_dir[0])
	  strncpy (cine_dir, env_cine_dir, MAX_GIMPDIR_LEN);
      else
	{
	  if (NULL != env_home_dir)
	    {
	      strncpy (cine_dir, env_home_dir, MAX_GIMPDIR_LEN);
	      cine_dir[len_env_home_dir] = '/';
	    }
	  else
	    g_message (_("warning: no home directory."));

	  strncpy (&cine_dir[len_env_home_dir+1],
		   env_cine_dir,
		   MAX_GIMPDIR_LEN - len_env_home_dir - 1);
	}
    }
  else
    {
      if (NULL != env_home_dir)
	{
	  strncpy (cine_dir, env_home_dir, MAX_GIMPDIR_LEN);
	  cine_dir[len_env_home_dir] = '/';
	}
      else
	g_message (_("warning: no home directory."));

      strncpy (&cine_dir[len_env_home_dir+1],
	       GetPathDot(),
	       MAX_GIMPDIR_LEN - len_env_home_dir - 1);
    }

  cine_dir[MAX_GIMPDIR_LEN] = '\000';
  return cine_dir;
}

static void
add_directory_token(const char* name, const char* value)
{
	UnknownToken* ut;

	/*
	  The token holds data from a static buffer which is initialized
	  once.  There should be no need to change an already-existing
	  value.
	*/
	if (NULL != cinerc_find_token(name))
		return;

	ut = g_new(UnknownToken, 1);
	ut->token = g_strdup(name);
	ut->value = g_strdup(value);

	/* Similarly, transforming the path should be silly. */
	parse_info.unknown_tokens = g_list_append(parse_info.unknown_tokens, ut);
	d_printf("rc: %s = \"%s\"\n", ut->token, ut->value);
}

void
parse_cinerc ()
{
	LogoProgress(_("Parse RC"), _(""));

  char libfilename[512];
  char filename[512];
  char *cine_dir;

  parse_info.buffer = g_new (char, 4096);
  parse_info.tokenbuf = parse_info.buffer + 2048;
  parse_info.buffer_size = 2048;
  parse_info.tokenbuf_size = 2048;

  cine_dir = cine_directory ();
  add_directory_token("cine_dir",cine_dir);
  add_directory_token("exe_dir",GetPathData());
  sprintf (libfilename, "%s/" RC_FILENAME, GetPathData());// parse built-in RC
//  LogoProgress("Resource configuration", libfilename, -1);
  parse_cinerc_file (libfilename);

  sprintf (filename, "%s/" RC_FILENAME, cine_dir);
  if (strcmp (filename, libfilename) != 0)
    {
//      LogoProgress(NULL, filename, -1);// parse personal RC
      parse_cinerc_file (filename);
    }
}

static int
peek_next_token()
{
	if (parse_info.next_token == -1)
		parse_info.next_token = get_token(&parse_info);

	return parse_info.next_token;
}

static int
parse_statement()
{
	int token;
	int i;

	token = peek_next_token();
	if (!token)
		return RC_DONE;
	if (token != TOKEN_LEFT_PAREN)
		return ReturnRC_ERROR();
	token = get_next_token();

	token = peek_next_token();
	if (!token || (token != TOKEN_SYMBOL))
		return ReturnRC_ERROR();
	token = get_next_token();

	for (i = 0; i < nfuncs; i++)
	{
		if (strcmp(funcs[i].name, token_sym) != 0)
		{
			continue;
		}
		switch (funcs[i].type)
		{
		case TT_STRING:
			return parse_string(funcs[i].val1p, funcs[i].val2p);
		case TT_PATH:
			return parse_path(funcs[i].val1p, funcs[i].val2p);
		case TT_DOUBLE:
			return parse_double(funcs[i].val1p, funcs[i].val2p);
		case TT_INT:
			return parse_int(funcs[i].val1p, funcs[i].val2p);
		case TT_BOOLEAN:
			return parse_boolean(funcs[i].val1p, funcs[i].val2p);
		case TT_POSITION:
			return parse_position(funcs[i].val1p, funcs[i].val2p);
		case TT_MEMSIZE:
			return parse_mem_size(funcs[i].val1p, funcs[i].val2p);
		case TT_IMAGETYPE:
			return parse_image_type(funcs[i].val1p, funcs[i].val2p);
		case TT_XCOLORCUBE:
			return parse_color_cube(funcs[i].val1p, funcs[i].val2p);
		case TT_XPREVSIZE:
			return parse_preview_size(funcs[i].val1p, funcs[i].val2p);
		case TT_XRULERUNIT:
			return parse_ruler_units(funcs[i].val1p, funcs[i].val2p);
		case TT_XPLUGIN:
			return parse_plugin(funcs[i].val1p, funcs[i].val2p);
		case TT_XPLUGINDEF:
			return parse_plugin_def(funcs[i].val1p, funcs[i].val2p);
		case TT_XMENUPATH:
			return parse_menu_path(funcs[i].val1p, funcs[i].val2p);
		case TT_XDEVICE:
			return parse_device(funcs[i].val1p, funcs[i].val2p);
		}
	}
	return parse_unknown(token_sym);
}

void
parse_cinerc_file (char *filename)
{	parse_info.unknown_tokens = NULL;
    parse_info.fp = fopen (filename, "rt");
    if (!parse_info.fp)
      return;
  if ((GetApp()->be_verbose == TRUE) || (GetApp()->no_splash == TRUE))
    g_print ("parsing \"%s\"\n", filename);
  parse_info.cur_token = -1;
  parse_info.next_token = -1;
  parse_info.position = -1;
  parse_info.linenum = 1;
  parse_info.charnum = 1;
  parse_info.inc_linenum = FALSE;
  parse_info.inc_charnum = FALSE;
  parse_info.done = FALSE;
  while ((parse_info.status = parse_statement(/*&parse_info*/)) == RC_OK);
  fclose (parse_info.fp);
  if (parse_info.status == RC_ERROR)
    {
      g_print ("RC_ERROR parsing: \"%s\"\n", filename);
      g_print ("  at line %d column %d\n",parse_info.linenum,parse_info.charnum);
      g_print ("  unexpected token: %s\n", token_sym);
    }
}

static GList *
g_list_findstr (GList *list,
		char *str)
{
  while (list)
    {
      if (! strcmp ((char *) list->data, str))
        break;
      list = list->next;
    }

  return list;
}

void
save_cinerc (GList **updated_options,
	     GList **conflicting_options)
{
  char timestamp[40];
  char *cine_dir;
  char name[512];
  FILE *fp_new;
  FILE *fp_old;
  GList *option;
  char *cur_line;
  char *prev_line;
  char *str;
  char *RC_ERROR_msg;

  if(!updated_options  || !conflicting_options)
	{	return;
	}

  cine_dir = cine_directory ();
  sprintf (name, "%s/" RC_FILENAME, cine_dir);

  RC_ERROR_msg = open_backup_file (name, &fp_new, &fp_old);
  if (RC_ERROR_msg != NULL)
    {
      g_message (RC_ERROR_msg);
      return;
    }

  strcpy (timestamp, "by CinePaint on ");
  iso_8601_date_format (timestamp + strlen (timestamp), FALSE);

  /* copy the old .cinerc into the new one, modifying it as needed */
  prev_line = NULL;
  cur_line = g_new (char, 1024);
  while (!feof (fp_old))
    {
      if (!fgets (cur_line, 1024, fp_old))
	continue;
      
      /* special case: save lines starting with '#-' (added by CinePaint) */
      if ((cur_line[0] == '#') && (cur_line[1] == '-'))
	{
	  if (prev_line != NULL)
	    {
	      fputs (prev_line, fp_new);
	      g_free (prev_line);
	    }
	  prev_line = g_strdup (cur_line);
	  continue;
	}

      /* see if the line contains something that we can use */
      if (find_token (cur_line, name, 50))
	{
	  /* check if that entry should be updated */
	  option = g_list_findstr (*updated_options, name);
	  if (option != NULL)
	    {
	      if (prev_line == NULL)
		{
		  fprintf (fp_new, "#- Next line commented out %s\n",
			   timestamp);
		  fprintf (fp_new, "# %s\n", cur_line);
		  fprintf (fp_new, "#- Next line added %s\n",
			   timestamp);
		}
	      else
		{
		  g_free (prev_line);
		  prev_line = NULL;
		  fprintf (fp_new, "#- Next line modified %s\n",
			   timestamp);
		}
	      str = value_to_str (name);
	      fprintf (fp_new, "(%s %s)\n", name, str);
	      g_free (str);

	      *updated_options = g_list_remove_link (*updated_options, option);
	      *conflicting_options = g_list_append (*conflicting_options,
						    (gpointer) option->data);
	      g_list_free_1 (option);
	      continue;
	    }

	  /* check if that entry should be commented out */
	  option = g_list_findstr (*conflicting_options, name);
	  if (option != NULL)
	    {
	      if (prev_line != NULL)
		{
		  g_free (prev_line);
		  prev_line = NULL;
		}
	      fprintf (fp_new, "#- Next line commented out %s\n",
		       timestamp);
	      fprintf (fp_new, "# %s\n", cur_line);
	      continue;
	    }
	}

      /* all lines that did not match the tests above are simply copied */
      if (prev_line != NULL)
	{
	  fputs (prev_line, fp_new);
	  g_free (prev_line);
	  prev_line = NULL;
	}
      fputs (cur_line, fp_new);

    }

  g_free (cur_line);
  if (prev_line != NULL)
    g_free (prev_line);
  fclose (fp_old);

  /* append the options that were not in the old .cinerc */
  option = *updated_options;
  while (option)
    {
      fprintf (fp_new, "#- Next line added %s\n",
	       timestamp);
      str = value_to_str ((char *) option->data);
      fprintf (fp_new, "(%s %s)\n\n", (char *) option->data, str);
      g_free (str);
      option = option->next;
    }
  fclose (fp_new);
}

static int
get_next_token ()
{
  if (parse_info.next_token != -1)
    {
	  parse_info.cur_token = parse_info.next_token;
	  parse_info.next_token = -1;
    }
  else
    {
	  parse_info.cur_token = get_token (&parse_info);
    }

  return parse_info.cur_token;
}


static int
parse_path (gpointer val1p,
	    gpointer val2p)
{
  int token;
  char **pathp;

  if(!val1p)
	{	return ReturnRC_ERROR();
	}
  pathp = (char **)val1p;

  token = peek_next_token ();
  if (!token || (token != TOKEN_STRING))
    return ReturnRC_ERROR();
  token = get_next_token ();

  if (*pathp)
    g_free (*pathp);
  *pathp = g_strdup (token_str);
/* Example: *pathp = "${cine_dir)/plugin/:$(cine_plugin_dir)/plug-ins" */
  token = peek_next_token();
  if (!token || (token != TOKEN_RIGHT_PAREN))
    {
      g_free (*pathp);
      *pathp = NULL;
      return ReturnRC_ERROR();
    }
  token = get_next_token();

  *pathp = transform_path (*pathp, TRUE);
/* Example: "rower/.filmgimp/plugin/:/usr/local/bin/plug-ins" */
	d_printf("rc parse path: \"%s\"\n",*pathp);
  return RC_OK;
}


static int
parse_string (gpointer val1p,gpointer val2p)
{
  int token;
  char **strp;

    if (!val1p)
	{	return ReturnRC_ERROR();
	}
  strp = (char **)val1p;

  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    return ReturnRC_ERROR();
  token = get_next_token ();

  if (*strp)
    g_free (*strp);
  *strp = g_strdup (token_str);

  token = peek_next_token ();
  if (!token || (token != TOKEN_RIGHT_PAREN))
    {
      g_free (*strp);
      *strp = NULL;
      return ReturnRC_ERROR();
    }
  token = get_next_token ();

  return RC_OK;
}

static int
parse_double (gpointer val1p,
	      gpointer val2p)
{
  int token;
  double *nump;

    if (!val1p)
	{	return ReturnRC_ERROR();
	}
  nump = (double *)val1p;

  token = peek_next_token();
  if (!token || (token != TOKEN_NUMBER))
    return ReturnRC_ERROR();
  token = get_next_token();

  *nump = token_num;

  token = peek_next_token ();
  if (!token || (token != TOKEN_RIGHT_PAREN))
    return ReturnRC_ERROR();
  token = get_next_token ();

  return RC_OK;
}

static int
parse_int (gpointer val1p,
	   gpointer val2p)
{
  int token;
  int *nump;

    if (!val1p)
	{	return ReturnRC_ERROR();
	}
  nump = (int *)val1p;

  token = peek_next_token ();
  if (!token || (token != TOKEN_NUMBER))
    return ReturnRC_ERROR();
  token = get_next_token ();

  *nump = token_num;

  token = peek_next_token ();
  if (!token || (token != TOKEN_RIGHT_PAREN))
    return ReturnRC_ERROR();
  token = get_next_token();

  return RC_OK;
}

static int
parse_boolean (gpointer val1p,
	       gpointer val2p)
{
  int token;
  int *boolp;

  /* The variable to be set should be passed in the first or second
   * pointer.  If the pointer is in val2p, then the opposite value is
   * stored in the pointer.  This is useful for "dont-xxx" or "no-xxx"
   * type of options.
   * If the expression to be parsed is written as "(option)" instead
   * of "(option yes)" or "(option no)", then the default value is
   * TRUE if the variable is passed in val1p, or FALSE if in val2p.
   */
    if(!val1p || !val2p)
	{	return ReturnRC_ERROR();
	}
  if (val1p != NULL)
    boolp = (int *)val1p;
  else
    boolp = (int *)val2p;

  token = peek_next_token();
  if (!token)
    return ReturnRC_ERROR();
  switch (token)
    {
    case TOKEN_RIGHT_PAREN:
      *boolp = TRUE;
      break;
    case TOKEN_NUMBER:
      token = get_next_token ();
      *boolp = token_num;
      token = peek_next_token ();
      if (!token || (token != TOKEN_RIGHT_PAREN))
	return ReturnRC_ERROR();
      break;
    case TOKEN_SYMBOL:
      token = get_next_token ();
      if (!strcmp (token_sym, "true") || !strcmp (token_sym, "on")
	  || !strcmp (token_sym, "yes"))
	*boolp = TRUE;
      else if (!strcmp (token_sym, "false") || !strcmp (token_sym, "off")
	       || !strcmp (token_sym, "no"))
	*boolp = FALSE;
      else
	return ReturnRC_ERROR();
      token = peek_next_token();
      if (!token || (token != TOKEN_RIGHT_PAREN))
	return ReturnRC_ERROR();
      break;
    default:
      return ReturnRC_ERROR();
    }

  if (val1p == NULL)
    *boolp = !*boolp;

  token = get_next_token();

  return RC_OK;
}

static int
parse_position (gpointer val1p,
		gpointer val2p)
{
  int token;
  int *xp;
  int *yp;

    if(!val1p || !val2p)
	{	return ReturnRC_ERROR();
	}
  xp = (int *)val1p;
  yp = (int *)val2p;

  token = peek_next_token();
  if (!token || (token != TOKEN_NUMBER))
    return ReturnRC_ERROR();
  token = get_next_token ();

  *xp = token_num;

  token = peek_next_token ();
  if (!token || (token != TOKEN_NUMBER))
    return ReturnRC_ERROR();
  token = get_next_token();

  *yp = token_num;

  token = peek_next_token ();
  if (!token || (token != TOKEN_RIGHT_PAREN))
    return ReturnRC_ERROR();
  token = get_next_token ();

  return RC_OK;
}

static int
parse_mem_size (gpointer val1p,
		gpointer val2p)
{
  int mult;
  int suffix;
  int token;
  int *sizep;

    if(!val1p)
	{	return ReturnRC_ERROR();
	}
  sizep = (int *)val1p;

  token = peek_next_token ();
  if (!token || ((token != TOKEN_NUMBER) &&
		 (token != TOKEN_SYMBOL)))
    return ReturnRC_ERROR();
  token = get_next_token();

  if (token == TOKEN_NUMBER)
    {
      *sizep = token_num * 1024;
    }
  else
    {
      *sizep = atoi (token_sym);

      suffix = token_sym[strlen (token_sym) - 1];
      if ((suffix == 'm') || (suffix == 'M'))
	mult = 1024 * 1024;
      else if ((suffix == 'k') || (suffix == 'K'))
	mult = 1024;
      else if ((suffix == 'b') || (suffix == 'B'))
	mult = 1;
      else
	return FALSE;

      *sizep *= mult;
    }

  token = peek_next_token();
  if (!token || (token != TOKEN_RIGHT_PAREN))
    return ReturnRC_ERROR();
  token = get_next_token ();

  return RC_OK;
}

static int
parse_image_type (gpointer val1p,
		  gpointer val2p)
{
  int token;
  Format *typep;
    if(!val1p)
	{	return ReturnRC_ERROR();
	}
  typep = (Format *)val1p;

  token = peek_next_token();
  if (!token || (token != TOKEN_SYMBOL))
    return ReturnRC_ERROR();
  token = get_next_token ();
 
  if (!strcmp (token_sym, "rgb"))
    *typep = FORMAT_RGB;
  else if ((!strcmp (token_sym, "gray")) || (!strcmp (token_sym, "grey")))
    *typep = FORMAT_GRAY;
  else
    return ReturnRC_ERROR();

  token = peek_next_token ();
  if (!token || (token != TOKEN_RIGHT_PAREN))
    return ReturnRC_ERROR();
  token = get_next_token ();

  return RC_OK;
}

static int
parse_color_cube (gpointer val1p,
		  gpointer val2p)
{
  int token;

  token = peek_next_token();
  if (!token || (token != TOKEN_NUMBER))
    return ReturnRC_ERROR();
  token = get_next_token();

  GetRC()->color_cube_shades[0] = token_num;

  token = peek_next_token ();
  if (!token || (token != TOKEN_NUMBER))
    return ReturnRC_ERROR();
  token = get_next_token();

  GetRC()->color_cube_shades[1] = token_num;

  token = peek_next_token();
  if (!token || (token != TOKEN_NUMBER))
    return ReturnRC_ERROR();
  token = get_next_token();

  GetRC()->color_cube_shades[2] = token_num;

  token = peek_next_token();
  if (!token || (token != TOKEN_NUMBER))
    return ReturnRC_ERROR();
  token = get_next_token();

  GetRC()->color_cube_shades[3] = token_num;

  token = peek_next_token();
  if (!token || (token != TOKEN_RIGHT_PAREN))
    return ReturnRC_ERROR();
  token = get_next_token();

  return RC_OK;
}

static int
parse_preview_size (gpointer val1p,
		    gpointer val2p)
{
  int token;

  token = peek_next_token();
  if (!token || (token != TOKEN_SYMBOL && token != TOKEN_NUMBER))
    return ReturnRC_ERROR();
  token = get_next_token();

  if (token == TOKEN_SYMBOL)
    {
      if (strcmp (token_sym, "none") == 0)
		  GetRC()->preview_size = 0;
      else if (strcmp (token_sym, "small") == 0)
		  GetRC()->preview_size = 32;
      else if (strcmp (token_sym, "medium") == 0)
		  GetRC()->preview_size = 64;
      else if (strcmp (token_sym, "large") == 0)
		  GetRC()->preview_size = 128;
      else
		  GetRC()->preview_size = 0;
    }
  else if (token == TOKEN_NUMBER)
	  GetRC()->preview_size = token_num;

  token = peek_next_token();
  if (!token || (token != TOKEN_RIGHT_PAREN))
    return ReturnRC_ERROR();
  token = get_next_token();

  return RC_OK;
}

static int
parse_ruler_units (gpointer val1p,
		   gpointer val2p)
{
  int token;

  token = peek_next_token();
  if (!token || (token != TOKEN_SYMBOL))
    return ReturnRC_ERROR();
  token = get_next_token();

  if (strcmp (token_sym, "pixels") == 0)
	  GetRC()->ruler_units = GTK_PIXELS;
  else if (strcmp (token_sym, "inches") == 0)
	  GetRC()->ruler_units = GTK_INCHES;
  else if (strcmp (token_sym, "centimeters") == 0)
	  GetRC()->ruler_units = GTK_CENTIMETERS;
  else
	  GetRC()->ruler_units = GTK_PIXELS;

  token = peek_next_token();
  if (!token || (token != TOKEN_RIGHT_PAREN))
    return ReturnRC_ERROR();
  token = get_next_token();

  return RC_OK;
}

static int
parse_plugin (gpointer val1p,
	       gpointer val2p)
{
  char *name;
  char *menu_path;
  char *accelerator;
  int token;

  name = NULL;
  menu_path = NULL;
  accelerator = NULL;

  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    return ReturnRC_ERROR();
  token = get_next_token();

  name = g_strdup (token_str);

  token = peek_next_token();
  if (token == TOKEN_STRING)
    {
      menu_path = g_strdup (token_str);
      token = get_next_token();
    }

  token = peek_next_token();
  if (token == TOKEN_STRING)
    {
      accelerator = g_strdup (token_str);
      token = get_next_token();
    }

  token = peek_next_token();
  if (!token || (token != TOKEN_RIGHT_PAREN))
    {
      g_free (name);
      g_free (menu_path);
      g_free (accelerator);
      return ReturnRC_ERROR();
    }
  token = get_next_token();

  if (name && menu_path)
    plugin_add (name, menu_path, accelerator);

  return RC_OK;
}

int AddPPluginProc(PluginInternal* internal, PluginProc* proc)
{	if(!internal || !proc)
	{	puts("bad Add proc");
		return FALSE;
	}
	//		d_printf("Parsing %s\n", proc->proc_data.name);
	proc->plugin_name = g_strdup(internal->plugin_name);
	internal->proc_list = g_slist_append(internal->proc_list, proc);
	return TRUE;
}
static int
parse_plugin_def (gpointer val1p,
		   gpointer val2p)
{
	PluginInternal* internal = 0;
  PluginProc *proc = 0;
  GSList *tmp_list;
  int token;

  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    return ReturnRC_ERROR();
  token = get_next_token();

  internal = g_new (PluginInternal, 1);
	memset(internal,0,sizeof(*internal));
  internal->filename = g_strdup (token_str);
  token = peek_next_token();
  if (!token || (token != TOKEN_NUMBER))
    goto RC_ERROR;
  token = get_next_token();

  internal->mtime = token_int;
  int status = RC_OK;
  while(RC_OK == status)
	{	status = parse_proc_def (&proc);
    	if(RC_ERROR == status)
		{	printf("RC_ERROR parsing %s\n", proc->proc_data.name);
			break;
		}
		else if(RC_EOF == status)
		{	break;
		}
		AddPPluginProc(internal,proc);
    }
#if 0
  token = peek_next_token();
  if (!token || (token != TOKEN_RIGHT_PAREN))
    goto RC_ERROR;
  token = get_next_token();
#endif
  plugin_def_add (internal);

  return RC_OK;

RC_ERROR:
  g_message ("RC_ERROR parsing pluginrc");
  tmp_list = internal->proc_list;
  while (tmp_list)
    {
      g_free (tmp_list->data);
      tmp_list = tmp_list->next;
    }
  g_slist_free (internal->proc_list);
  g_free (internal->filename);
  g_free (internal);

  return ReturnRC_ERROR();
}

static int
parse_proc_def (PluginProc **proc)
{
  PluginProc *pd;
  int token;
  int i;

  token = peek_next_token();
  if (!token || (token != TOKEN_LEFT_PAREN))
    return ReturnRC_ERROR();
  token = get_next_token();

  token = peek_next_token();
  if (!token || (token != TOKEN_SYMBOL) ||
      (strcmp ("proc-def", token_sym) != 0))
    return ReturnRC_ERROR();
  token = get_next_token();

  pd = g_new0 (PluginProc, 1);
	memset(pd,0,sizeof(*pd));

  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    goto RC_ERROR;
  token = get_next_token();

  pd->proc_data.name = g_strdup (token_str);

  token = peek_next_token();
  if (!token || (token != TOKEN_NUMBER))
    goto RC_ERROR;
  token = get_next_token();

  pd->proc_data.proc_type = token_int;

  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    goto RC_ERROR;
  token = get_next_token();

  pd->proc_data.blurb = g_strdup (token_str);

  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    goto RC_ERROR;
  token = get_next_token();

  pd->proc_data.help = g_strdup (token_str);

  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    goto RC_ERROR;
  token = get_next_token();

  pd->proc_data.author = g_strdup (token_str);

  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    goto RC_ERROR;
  token = get_next_token();

  pd->proc_data.copyright = g_strdup (token_str);

  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    goto RC_ERROR;
  token = get_next_token();

  pd->proc_data.date = g_strdup (token_str);

  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    goto RC_ERROR;
  token = get_next_token();

  if (token_str[0] != '\0')
    pd->menu_path = g_strdup (token_str);

  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    goto RC_ERROR;
  token = get_next_token();

  if (token_str[0] != '\0')
    pd->extensions = g_strdup (token_str);
  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    goto RC_ERROR;
  token = get_next_token();
  if (token_str[0] != '\0')
    pd->prefixes = g_strdup (token_str);
  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
//    goto RC_ERROR;
	return RC_EOF;//Eof
  token = get_next_token();
  if (token_str[0] != '\0')
    pd->magics = g_strdup (token_str);

  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    goto RC_ERROR;
  token = get_next_token();
  if(' ' == *token_str)
	{	token_str++;
	}
  if (token_str[0] != '\0')
    {
      pd->image_types = g_strdup (token_str);
      pd->image_types_val = plugin_image_types_parse (token_str);
    }

  token = peek_next_token();
  if (!token || (token != TOKEN_NUMBER))
    goto RC_ERROR;
  token = get_next_token();

  pd->proc_data.num_args = token_int;
  pd->proc_data.args = g_new0 (ProcArg, pd->proc_data.num_args);

  token = peek_next_token();
  if (!token || (token != TOKEN_NUMBER))
    goto RC_ERROR;
  token = get_next_token();

  pd->proc_data.num_values = token_int;
  pd->proc_data.values = NULL;
  if (pd->proc_data.num_values > 0)
    pd->proc_data.values = g_new (ProcArg, pd->proc_data.num_values);

  for (i = 0; i < pd->proc_data.num_args; i++)
    if (!parse_proc_arg (&pd->proc_data.args[i]))
      goto RC_ERROR;

  for (i = 0; i < pd->proc_data.num_values; i++)
    if (!parse_proc_arg (&pd->proc_data.values[i]))
      goto RC_ERROR;

  token = peek_next_token();
  if (!token || (token != TOKEN_RIGHT_PAREN))
    goto RC_ERROR;
  *proc = pd;
  token = get_next_token();
  if (!token || (token != TOKEN_RIGHT_PAREN))
  {	  ReturnRC_ERROR();
  }
  token = peek_next_token();
  if (token == TOKEN_LEFT_PAREN)
  {	  return RC_OK;
  }
  token = get_next_token();// last TOKEN_RIGHT_PAREN
  if (!token || (token != TOKEN_RIGHT_PAREN))
  {	  ReturnRC_ERROR();
  }
  token = peek_next_token();
  if (!token || (token != TOKEN_RIGHT_PAREN))
	{  return RC_DONE;
	}
  return RC_OK;

RC_ERROR:
  g_free (pd->proc_data.name);
  g_free (pd->proc_data.blurb);
  g_free (pd->proc_data.help);
  g_free (pd->proc_data.author);
  g_free (pd->proc_data.copyright);
  g_free (pd->proc_data.date);
  g_free (pd->menu_path);
  g_free (pd->extensions);
  g_free (pd->prefixes);
  g_free (pd->magics);
  g_free (pd->image_types);

  for (i = 0; i < pd->proc_data.num_args; i++)
    {
      g_free (pd->proc_data.args[i].name);
      g_free (pd->proc_data.args[i].description);
    }

  for (i = 0; i < pd->proc_data.num_values; i++)
    {
      g_free (pd->proc_data.values[i].name);
      g_free (pd->proc_data.values[i].description);
    }

  g_free (pd->proc_data.args);
  g_free (pd->proc_data.values);
  g_free (pd);
  return ReturnRC_ERROR();
}

static int
parse_proc_arg (ProcArg *arg)
{
  int token;

  arg->arg_type = -1;
  arg->name = NULL;
  arg->description = NULL;

  token = peek_next_token();
  if (!token || (token != TOKEN_LEFT_PAREN))
    return ReturnRC_ERROR();
  token = get_next_token();

  token = peek_next_token();
  if (!token || (token != TOKEN_SYMBOL) ||
      (strcmp ("proc-arg", token_sym) != 0))
    return ReturnRC_ERROR();
  token = get_next_token();

  token = peek_next_token();
  if (!token || (token != TOKEN_NUMBER))
    return ReturnRC_ERROR();
  token = get_next_token();

  arg->arg_type = token_int;

  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    return ReturnRC_ERROR();
  token = get_next_token();

  arg->name = g_strdup (token_str);

  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    goto RC_ERROR;
  token = get_next_token();

  arg->description = g_strdup (token_str);

  token = peek_next_token();
  if (!token || (token != TOKEN_RIGHT_PAREN))
    goto RC_ERROR;
  token = get_next_token();

  return RC_OK;

RC_ERROR:
  g_free (arg->name);
  g_free (arg->description);

  return ReturnRC_ERROR();
}

static int
parse_menu_path (gpointer val1p,
		 gpointer val2p)
{
  char *menu_path;
  char *accelerator;
  int token;

  menu_path = NULL;
  accelerator = NULL;

  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    goto RC_ERROR;
  token = get_next_token();

  menu_path = g_strdup (token_str);

  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    goto RC_ERROR;
  token = get_next_token();

  accelerator = g_strdup (token_str);

  token = peek_next_token();
  if (!token || (token != TOKEN_RIGHT_PAREN))
    goto RC_ERROR;
  token = get_next_token();

  /* Sort of silly, we do nothing here - same as 1.1 */

  return RC_OK;

RC_ERROR:
  g_free (menu_path);
  g_free (accelerator);

  return ReturnRC_ERROR();
}

static char*
transform_path (char *path,
		int   destroy)
{
  guint length;
  char *new_path;
  const char *home;
  char *token;
  char *tmp;
  char *tmp2;
  int  substituted;
  int  is_env;
  UnknownToken *ut;
  home = GetPathHome();
  length = 0;
  substituted = FALSE;
  is_env = FALSE;

  tmp = path;
  while (*tmp)
    {
      if (*tmp == '~')
	{
	  length += strlen (home);
	  tmp += 1;
	}
      else if (*tmp == '$')
	{
	  tmp += 1;
	  if (!*tmp || (*tmp != '{'))
	    return path;
	  tmp += 1;

	  token = tmp;
	  while (*tmp && (*tmp != '}'))
	    tmp += 1;

	  if (!*tmp)
	    return path;

	  *tmp = '\0';

	  tmp2 = cinerc_find_token (token);
	  if (tmp2 == NULL) 
	    {
	      /* maybe token is an environment variable */
	      tmp2 = getenv (token);
	      if (tmp2 != NULL)
		{
		  is_env = TRUE;
		}
	      else
		{
		  printf("rc token referenced but not defined: %s", token);
			continue;
		}
	    }
	  tmp2 = transform_path (tmp2, FALSE);
	  if (is_env)
	    {
	      /* then add to list of unknown tokens */
	      /* but only if it isn't already in list */
	      if (cinerc_find_token (token) == NULL)
		{
		  ut = g_new (UnknownToken, 1);
		  ut->token = g_strdup (token);
		  ut->value = g_strdup (tmp2);
		  parse_info.unknown_tokens = g_list_append (parse_info.unknown_tokens, ut);

			d_printf("rc: %s = \"%s\"\n",ut->token,ut->value);
		}
	    }
	  else
	    {
	      cinerc_set_token (token, tmp2);
	    }
	  length += strlen (tmp2);

	  *tmp = '}';
	  tmp += 1;

	  substituted = TRUE;
	}
      else
	{
	  length += 1;
	  tmp += 1;
	}
    }

  if ((length == strlen (path)) && !substituted)
    return path;

  new_path = g_new (char, length + 1);

  tmp = path;
  tmp2 = new_path;

  while (*tmp)
    {
      if (*tmp == '~')
	{
	  *tmp2 = '\0';
	  strcat (tmp2, home);
	  tmp2 += strlen (home);
	  tmp += 1;
	}
      else if (*tmp == '$')
	{
	  tmp += 1;
	  if (!*tmp || (*tmp != '{'))
	    {
	      g_free (new_path);
	      return path;
	    }
	  tmp += 1;

	  token = tmp;
	  while (*tmp && (*tmp != '}'))
	    tmp += 1;

	  if (!*tmp)
	    {
	      g_free (new_path);
	      return path;
	    }

	  *tmp = '\0';
	  token = cinerc_find_token (token);
	  *tmp = '}';

	  *tmp2 = '\0';
	  strcat (tmp2, token);
	  tmp2 += strlen (token);
	  tmp += 1;
	}
      else
	{
	  *tmp2++ = *tmp++;
	}
    }

  *tmp2 = '\0';

  if (destroy)
    g_free (path);

  return new_path;
}

/* Copied from gtk_menu_factory_parse_accelerator() */
static void
parse_device_accelerator (const char   *accelerator,
			  GdkDeviceKey *key)
{
  int done;

  g_return_if_fail (accelerator != NULL);
  g_return_if_fail (key != NULL);

  key->modifiers = 0;

  done = FALSE;
  while (!done)
    {
      if (strncmp (accelerator, "<shift>", 7) == 0)
        {
          accelerator += 7;
          key->modifiers |= GDK_SHIFT_MASK;
        }
      else if (strncmp (accelerator, "<alt>", 5) == 0)
        {
          accelerator += 5;
          key->modifiers |= GDK_MOD1_MASK;
        }
      else if (strncmp (accelerator, "<control>", 9) == 0)
        {
          accelerator += 9;
          key->modifiers |= GDK_CONTROL_MASK;
        }
      else
        {
          done = TRUE;
	  /* Tricky, but works... ("" => keyval = 0, or no translation) */
          key->keyval = accelerator[0];
        }
    }
}

static int 
parse_device (gpointer val1p, 
	      gpointer val2p)
{
  DeviceValues values = 0;
  int i;
  int token;
  
  /* The initialized values here are meaningless */
  gchar *name = NULL;
  GdkInputMode mode = GDK_MODE_DISABLED;
  gint num_axes = 0;
  GdkAxisUse *axes = NULL;
  gint num_keys = 0;
  GdkDeviceKey *keys = NULL;
  gchar *brush_name = NULL;
  ToolType tool = RECT_SELECT;
  guchar foreground[3] = { 0, 0, 0 };

  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    goto RC_ERROR;
  token = get_next_token();

  name = g_strdup (token_str);

  /* Parse options for device */

  while ( peek_next_token() == TOKEN_LEFT_PAREN )
    {
      token = get_next_token();

      token = peek_next_token();
      if (!token || (token != TOKEN_SYMBOL))
	goto RC_ERROR;
      token = get_next_token();

      if (!strcmp ("mode", token_sym))
	{
	  values |= DEVICE_MODE;
	  
	  token = peek_next_token();
	  if (!token || (token != TOKEN_SYMBOL))
	    goto RC_ERROR;
	  token = get_next_token();

	  if (!strcmp ("disabled", token_sym))
	    mode = GDK_MODE_DISABLED;
	  else if (!strcmp ("window", token_sym))
	    mode = GDK_MODE_WINDOW;
	  else if (!strcmp ("screen", token_sym))
	    mode = GDK_MODE_SCREEN;
	  else
	    goto RC_ERROR;
	}
      else if (!strcmp ("axes", token_sym))
	{
	  values |= DEVICE_AXES;
	  
	  token = peek_next_token();
	  if (!token || (token != TOKEN_NUMBER))
	    goto RC_ERROR;
	  token = get_next_token();

	  num_axes = token_int;
	  axes = g_new (GdkAxisUse, num_axes);
	  
	  for (i=0; i<num_axes; i++)
	    {
	      token = peek_next_token();
	      if (!token || (token != TOKEN_SYMBOL))
		goto RC_ERROR;
	      token = get_next_token();
	      
	      if (!strcmp ("ignore", token_sym))
		axes[i] = GDK_AXIS_IGNORE;
	      else if (!strcmp ("x", token_sym))
		axes[i] = GDK_AXIS_X;
	      else if (!strcmp ("y", token_sym))
		axes[i] = GDK_AXIS_Y;
	      else if (!strcmp ("pressure", token_sym))
		axes[i] = GDK_AXIS_PRESSURE;
	      else if (!strcmp ("xtilt", token_sym))
		axes[i] = GDK_AXIS_XTILT;
	      else if (!strcmp ("ytilt", token_sym))
		axes[i] = GDK_AXIS_YTILT;
#if GTK_MAJOR_VERSION > 1
	      else if (!strcmp ("wheel", token_sym))
		axes[i] = GDK_AXIS_WHEEL;
#endif
	      else
		goto RC_ERROR;
	    }
	}
      else if (!strcmp ("keys", token_sym))
	{
	  values |= DEVICE_KEYS;
	  
	  token = peek_next_token();
	  if (!token || (token != TOKEN_NUMBER))
	    goto RC_ERROR;
	  token = get_next_token();

	  num_keys = token_int;
	  keys = g_new (GdkDeviceKey, num_keys);
	  
	  for (i=0; i<num_keys; i++)
	    {
	      token = peek_next_token();
	      if (!token || (token != TOKEN_STRING))
		goto RC_ERROR;
	      token = get_next_token();
	      
	      parse_device_accelerator (token_str, &keys[i]);
	    }
	}
      else if (!strcmp ("brush", token_sym))
	{
	  values |= DEVICE_BRUSH;
	  
	  token = peek_next_token();
	  if (!token || (token != TOKEN_STRING))
	    goto RC_ERROR;
	  token = get_next_token();

	  brush_name = g_strdup (token_str);
	}
      else if (!strcmp ("tool", token_sym))
	{
	  values |= DEVICE_TOOL;

	  token = peek_next_token();
	  if (!token || (token != TOKEN_STRING))
	    goto RC_ERROR;
	  token = get_next_token();

	  /* FIXME: this shouldn't be hard coded like this */
	  for (tool = RECT_SELECT; tool <= CONVOLVE; tool++)
	    {
	      if (!strcmp(tool_info[tool].tool_name, token_str))
		break;
	    }
	  if (tool > CONVOLVE)
	    goto RC_ERROR;
	  
	}
      else if (!strcmp ("foreground", token_sym))
	{
	  values |= DEVICE_FOREGROUND;
	  
	  for (i=0; i<3; i++)
	    {
	      token = peek_next_token();
	      if (!token || (token != TOKEN_NUMBER))
		goto RC_ERROR;
	      token = get_next_token();

	      foreground[i] = token_int;
	    }
	}
      else
	goto RC_ERROR;
      
      token = peek_next_token();
      if (!token || (token != TOKEN_RIGHT_PAREN))
	goto RC_ERROR;
      token = get_next_token();
    }

  if (!token || (token != TOKEN_RIGHT_PAREN))
    goto RC_ERROR;
  token = get_next_token();

  devices_rc_update (name, values, mode, num_axes, axes, num_keys, keys,
		     brush_name, tool, foreground);

  g_free (brush_name);
  g_free (name);
  g_free (axes);
  g_free (keys);

  return RC_OK;

RC_ERROR:
  g_free (brush_name);
  g_free (name);
  g_free (axes);
  g_free (keys);

  return ReturnRC_ERROR();
}

static int
parse_unknown (char *token_sym)
{
  int token;
  UnknownToken *ut, *tmp;
  GList *list;

  ut = g_new (UnknownToken, 1);
  ut->token = g_strdup (token_sym);

  token = peek_next_token();
  if (!token || (token != TOKEN_STRING))
    {
      g_free (ut->token);
      g_free (ut);
      return ReturnRC_ERROR();
    }
  token = get_next_token();

  ut->value = g_strdup (token_str);

  token = peek_next_token();
  if (!token || (token != TOKEN_RIGHT_PAREN))
    {
      g_free (ut->token);
      g_free (ut->value);
      g_free (ut);
      return ReturnRC_ERROR();
    }
  token = get_next_token();

  /*  search through the list of unknown tokens and replace an existing entry  */
  list = parse_info.unknown_tokens;
  while (list)
    {
      tmp = (UnknownToken *) list->data;
      list = list->next;

      if (strcmp (tmp->token, ut->token) == 0)
	{
		  parse_info.unknown_tokens = g_list_remove (parse_info.unknown_tokens, tmp);
	  g_free (tmp->token);
	  g_free (tmp->value);
	  g_free (tmp);
	}
    }

  ut->value = transform_path (ut->value, TRUE);
  parse_info.unknown_tokens = g_list_append (parse_info.unknown_tokens, ut);
	d_printf("rc: %s = \"%s\"\n",ut->token,ut->value);
  return RC_OK;
}

static char *
value_to_str (char *name)
{
  int i;

  for (i = 0; i < nfuncs; i++)
    if (! strcmp (funcs[i].name, name))
      switch (funcs[i].type)
	{
	case TT_STRING:
	  return string_to_str (funcs[i].val1p, funcs[i].val2p);
	case TT_PATH:
	  return path_to_str (funcs[i].val1p, funcs[i].val2p);
	case TT_DOUBLE:
	  return double_to_str (funcs[i].val1p, funcs[i].val2p);
	case TT_INT:
	  return int_to_str (funcs[i].val1p, funcs[i].val2p);
	case TT_BOOLEAN:
	  return boolean_to_str (funcs[i].val1p, funcs[i].val2p);
	case TT_POSITION:
	  return position_to_str (funcs[i].val1p, funcs[i].val2p);
	case TT_MEMSIZE:
	  return mem_size_to_str (funcs[i].val1p, funcs[i].val2p);
	case TT_IMAGETYPE:
	  return image_type_to_str (funcs[i].val1p, funcs[i].val2p);
	case TT_XCOLORCUBE:
	  return color_cube_to_str (funcs[i].val1p, funcs[i].val2p);
	case TT_XPREVSIZE:
	  return preview_size_to_str (funcs[i].val1p, funcs[i].val2p);
	case TT_XRULERUNIT:
	  return ruler_units_to_str (funcs[i].val1p, funcs[i].val2p);
	case TT_XPLUGIN:
	case TT_XPLUGINDEF:
	case TT_XMENUPATH:
	case TT_XDEVICE:
	  return NULL;
	}
  return NULL;
}

static char *
string_to_str (gpointer val1p,
	       gpointer val2p)
{
  char *str;

  if (*((char **)val1p) != NULL)
  {   str = g_malloc (strlen (*((char **)val1p)) + 3);
      sprintf (str, "%c%s%c", '"', *((char **)val1p), '"');
  } 
  else
  {  str = g_malloc (3);
     str[0]='"';
     str[1]='"';
     str[2]='\0';
  }

  return str;
}

static char *
path_to_str (gpointer val1p,
	     gpointer val2p)
{
  return string_to_str (val1p, val2p);
}

static char *
double_to_str (gpointer val1p,
	       gpointer val2p)
{
  char *str;

  str = g_malloc (20);
  sprintf (str, "%f", *((double *)val1p));
  return str;
}

static char *
int_to_str (gpointer val1p,
	    gpointer val2p)
{
  char *str;

  str = g_malloc (20);
  sprintf (str, "%d", *((int *)val1p));
  return str;
}

static char *
boolean_to_str (gpointer val1p,
		gpointer val2p)
{
  int v;

  if (val1p != NULL)
    v = *((int *)val1p);
  else
    v = !*((int *)val2p);
  if (v)
    return g_strdup ("yes");
  else
    return g_strdup ("no");
}

static char *
position_to_str (gpointer val1p,
		 gpointer val2p)
{
  char *str;

  str = g_malloc (40);
  sprintf (str, "%d %d", *((int *)val1p), *((int *)val2p));
  return str;
}

static char *
mem_size_to_str (gpointer val1p,
		 gpointer val2p)
{
  int size;
  char *str;

  size = *((int *)val1p);
  str = g_malloc (20);
  if (size % 1048576 == 0)
    sprintf (str, "%dM", size / 1048576);
  else if (size % 1024 == 0)
    sprintf (str, "%dK", size / 1024);
  else
    sprintf (str, "%dB", size);
  return str;
}

static char *
image_type_to_str (gpointer val1p,
		   gpointer val2p)
{
  Format type;

  type = *((Format *)val1p);
  if (type == FORMAT_GRAY)
    return g_strdup ("gray");
  else
    return g_strdup ("rgb");
}

static char *
color_cube_to_str (gpointer val1p,
		   gpointer val2p)
{
  char *str;

  str = g_malloc (40);
  sprintf (str, "%d %d %d  %d",
	  GetRC()->color_cube_shades[0], GetRC()->color_cube_shades[1],
	  GetRC()->color_cube_shades[2], GetRC()->color_cube_shades[3]);
  return str;
}

static char *
preview_size_to_str (gpointer val1p,
		     gpointer val2p)
{
  if (GetRC()->preview_size >= 128)
    return g_strdup ("large");
  else if (GetRC()->preview_size >= 64)
    return g_strdup ("medium");
  else if (GetRC()->preview_size >= 32)
    return g_strdup ("small");
  else
    return g_strdup ("none");
}

static char *
ruler_units_to_str (gpointer val1p,
		    gpointer val2p)
{
  switch (GetRC()->ruler_units)
    {
    case GTK_INCHES:
      return g_strdup ("inches");
    case GTK_CENTIMETERS:
      return g_strdup ("centimeters");
    case GTK_PIXELS:
      return g_strdup ("pixels");
    }
  return NULL;
}
/* Try to:

   1. Open cinerc for reading.

   2. Rename cinerc to cinerc.old.

   3. Open cinerc for writing.

   On success, return NULL. On failure, return a string in English
   explaining the problem.

   */
static char *
open_backup_file (char *filename,
                  FILE **fp_new,
                  FILE **fp_old)
{
  char *oldfilename;

  /*
    Rename the file to *.old, open it for reading and create the new file.
  */

  oldfilename = g_malloc (strlen (filename) + 5);
  sprintf (oldfilename, "%s.old", filename);
#ifdef _WIN32
  remove(oldfilename);
#endif
  if (rename (filename, oldfilename) < 0)
    {
		e_printf("rename %s %s errno #%i\n",filename,oldfilename,errno);
      g_free (oldfilename);
      if (errno == EACCES)
        return "Can't rename " RC_FILENAME " to " RC_FILENAME ".old; permission problems";
      if (errno == EISDIR)
        return "Can't rename " RC_FILENAME " to " RC_FILENAME ".old; " RC_FILENAME ".old is a directory";
      return "Can't rename " RC_FILENAME " to " RC_FILENAME ".old, reason unknown";
    }

  if ((*fp_old = fopen (oldfilename, "rt")) == NULL)
    {
      if (errno == EACCES)
        return "Can't open " RC_FILENAME "; permission problems";
      if (errno == ENOENT)
        return "Can't open " RC_FILENAME "; file does not exist";
      return "Can't open " RC_FILENAME ", reason unknown";
    }

  if ((*fp_new = fopen (filename, "wt")) == NULL)
    {
      (void) rename (oldfilename, filename);
      g_free (oldfilename);
      if (errno == EACCES)
        return "Can't write to " RC_FILENAME "; permission problems";
      return "Can't write to " RC_FILENAME ", reason unknown";
    }

  g_free (oldfilename);
  return NULL;
}

static char*
cinerc_find_token(const char *token)
{
  GList *list;
  UnknownToken *ut;

  list = parse_info.unknown_tokens;
  while (list)
    {
      ut = (UnknownToken *) list->data;
      if (strcmp (ut->token, token) == 0)
	return ut->value;
      list = list->next;
    }

  return NULL;
}

void
cinerc_set_token (char *token,
		  char *value)
{
  GList *list;
  UnknownToken *ut;

  list = parse_info.unknown_tokens;
  while (list)
    {
      ut = (UnknownToken *) list->data;
      if (strcmp (ut->token, token) == 0)
	{
	  if (ut->value != value)
	    {
	      if (ut->value)
		g_free (ut->value);
	      ut->value = value;
	    }
	  break;
	}
      list = list->next;
    }
}

/******************/
/*  GIMPRC_QUERY  */

Argument *
cinerc_query (Argument *args)
{
  Argument *return_args;
  int success = TRUE;
  char *token;
  char *value;

  token = (char *) args[0].value.pdb_pointer;

  success = ((value = cinerc_find_token (token)) != NULL);
  return_args = procedural_db_return_args (&cinerc_query_proc, success);

  if (success)
    return_args[1].value.pdb_pointer = g_strdup (value);

  return return_args;
}

static ProcArg cinerc_query_args[] =
{
  { PDB_STRING,
    "token",
    "the token to query for"
  }
};

static ProcArg cinerc_query_out_args[] =
{
  { PDB_STRING,
    "value",
    "the value associated with the queried token"
  }
};

ProcData cinerc_query_proc =
{
  "cine_cinerc_query",
  "Queries the " RC_FILENAME " file parser for information on a specified token",
  "This procedure is used to locate additional information contained in the " RC_FILENAME " file considered extraneous to the operation of CinePaint.  Plug-ins that need configuration information can expect it will be stored in the user's " RC_FILENAME " file and can use this procedure to retrieve it.  This query procedure will return the value associated with the specified token.  This corresponds _only_ to entries with the format: (<token> <value>).  The value must be a string.  Entries not corresponding to this format will cause warnings to be issued on " RC_FILENAME " parsing and will not be queryable.",
  "Spencer Kimball & Peter Mattis",
  "Spencer Kimball & Peter Mattis",
  "1997",
  PDB_INTERNAL,

  /*  Input arguments  */
  1,
  cinerc_query_args,

  /*  Output arguments  */
  1,
  cinerc_query_out_args,

  /*  Exec method  */
  { { cinerc_query } },
};
#if 1
/**
 * gimp_path_parse:
 * @path: A list of directories separated by #G_SEARCHPATH_SEPARATOR.
 * @max_paths: The maximum number of directories to return.
 * @check: #TRUE if you want the directories to be checked.
 * @check_failed: Returns a #GList of path elements for which the
 *                check failed. Each list element is guaranteed
 *		  to end with a #G_PATH_SEPARATOR.
 *
 * Returns: A #GList of all directories in @path. Each list element
 *	    is guaranteed to end with a #G_PATH_SEPARATOR.
 **/
GList* cine_path_parse(gchar* path,
	gint       max_paths,
	gboolean   check,
	GList** check_failed)
{
	const gchar* home;
	gchar** patharray;
	GList* list = NULL;
	GList* fail_list = NULL;
	gint    i;

	struct stat filestat;
	gint        err = FALSE;

	if (!path || !*path || max_paths < 1 || max_paths > 256)
		return NULL;

	home = g_get_home_dir();

	patharray = g_strsplit(path, G_SEARCHPATH_SEPARATOR_S, max_paths);

	for (i = 0; i < max_paths; i++)
	{
		GString* dir;

		if (!patharray[i])
			break;

#ifndef G_OS_WIN32
		if (*patharray[i] == '~')
		{
			dir = g_string_new(home);
			g_string_append(dir, patharray[i] + 1);
		}
		else
#endif
		{
			dir = g_string_new(patharray[i]);
		}

#ifdef __EMX__
		_fnslashify(dir);
#endif

		if (check)
		{
			/*  check if directory exists  */
			err = stat(dir->str, &filestat);

			if (!err && S_ISDIR(filestat.st_mode))
			{
				if (dir->str[dir->len - 1] != G_DIR_SEPARATOR)
					g_string_append_c(dir, G_DIR_SEPARATOR);
			}
		}

		if (!err)
			list = g_list_prepend(list, g_strdup(dir->str));
		else if (check_failed)
			fail_list = g_list_prepend(fail_list, g_strdup(dir->str));

		g_string_free(dir, TRUE);
	}

	g_strfreev(patharray);

	list = g_list_reverse(list);

	if (check && check_failed)
	{
		fail_list = g_list_reverse(fail_list);
		*check_failed = fail_list;
	}

	return list;
}
#endif