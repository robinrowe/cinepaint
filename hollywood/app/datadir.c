/* wire/datadir.c
// GetPathHome
// Copyright Dec 2, 2002, Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)
*/

#include <stdio.h>
#include <linux/limits.h>
#include "datadir.h"
#include "../wire/iodebug.h"
#include "version.h"
#include "../pdb/plugin.h"
#include "app.h"
#include "tile_swap.h"

#ifdef _WIN32
#include <Shlobj.h>  // need to include definitions of constants
#endif

# define  DIR_SEPARATOR "/"
# define  DIR_SEPARATOR_C '/'
extern App app;

void FlipSlashes(char* s)
{   char* sep = strchr(s, '\\');
    while (sep)
    {
        *sep = '/';
        sep = strchr(s, '\\');
    }
}

#ifdef _WIN32

const char* GetPathHome()
{	static char* path;
	if(path)
	{	return path;
	}
	path=(char*) malloc(sizeof(char)*PATH_MAX);
	if(!path)
	{	return "error";
	}
	path[0]=0;
	if(!SUCCEEDED(SHGetFolderPathA(NULL, 
                             CSIDL_APPDATA, 
                             NULL, 
                             0, 
                             path))) 
	{	return "error";
	}
	FlipSlashes(path);
	return path;
}

#else

const char* GetPathHome()
{   return getenv("HOME");
}

#endif

const char* GetPathPo()
{   return app.po_path;
}

const char* GetPathExe()
{    return app.exe_path;
}

const char* GetPathLast()
{    return app.last_path;
}

void SetPathLast(const char* last_path,bool isTrim)
{   if(!last_path)
    {   return;
    }
    if(app.last_path)
    {   if(!strcmp(last_path,app.last_path))
        {   return;
        }
        free(app.last_path);
    }
    app.last_path = strdup(last_path);
    FlipSlashes(app.last_path);
    if(!isTrim)
    {   return;
    }
    char* basename = strrchr(app.last_path,'/');// e.g., "/Users/rower/Desktop/female-face-pixabay2.bmp"
    if(!basename)
    {   return;
    }
    enum { trim_base_name = 0 };
    *basename = trim_base_name;
}

//#define LOCALEDIR GetApp()->po_path

int IsFile(const char* filename)
{   FILE* fp = fopen(filename, "r");
    if(!fp)
    {   return 0;
    }
    fclose(fp);
    return 1;
}

char* StrDupPath(const char* ext,int is_data)
{   char* s = strdup(app.arg0);
    if(is_data)
    {   s[app.data_offset] = 0;
    }
    else
    {   s[app.exe_offset] = 0;
    }
    if(ext)
    {    strcat(s,ext);
    }
    return s;
}

int FindTipsFile()
{   for(;;)
    {   char* end = strrchr(app.data_path,'/');
        if(!end)
        {   return 0;
        }
        if(!app.exe_offset)
        {   app.exe_offset = end - app.data_path;
        }
        *(end+1) = 0; // data_path = "C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug"
        strcat(app.data_path, app.tips);
        if(IsFile(app.data_path))
        {   *end = 0;
            app.data_offset = end - app.data_path;
            return 1;
        }
        *end = 0;
    }
    return 0;
}

void SetPathData(const char* arg)
{   if(app.data_path || !arg)
    {   return;
    }
    app.arg0 = strdup(arg);
    FlipSlashes(app.arg0);
    app.data_path = strdup(app.arg0);
    if(!FindTipsFile())
    {   return;
    }
}

const char* GetPathDot()
{	return GetApp()->dot_path;
}

const char* GetPathData()
{	return GetApp()->data_path;
}

const char* cine_personal_rc_file (const char* rcfile)
{
    static char* rc_file;
    rc_file = (char*)malloc(sizeof(char)*PATH_MAX);
    if(!rc_file)
    {       return "error";
    }
    sprintf( rc_file, "%s\\%s\\%s",GetPathHome(), GetPathDot(), rcfile );
    return rc_file;
}


const char* GetPathSeparator()
{ return DIR_SEPARATOR; 
}

const char* GetDirPluginSuffix()
{   static char* plugin_suffix = NULL;
  if(plugin_suffix)
    return plugin_suffix;

  plugin_suffix = malloc (strlen("lib" "cinepaint" PROGRAM_VERSION) * 2);
    if(!plugin_suffix)
    {   return 0;
    }
  sprintf( plugin_suffix,"%s%s%s%s%s", "lib", DIR_SEPARATOR, "cinepaint",
           DIR_SEPARATOR, PROGRAM_VERSION);
  d_printf("GetDirPluginSuffix: %s\n",plugin_suffix);
  return (const char*)plugin_suffix;
}


const char* GetDirDataSuffix()
{   static char* data_suffix_ = NULL;
  if(data_suffix_)
    return data_suffix_;

  data_suffix_ = malloc (strlen("share" "cinepaint" PROGRAM_VERSION) * 2);
    if(!data_suffix_)
    {   return 0;
    }
  sprintf( data_suffix_,"%s%s%s%s%s", "share", DIR_SEPARATOR, "cinepaint",
           DIR_SEPARATOR, PROGRAM_VERSION);
  d_printf("GetDirDataSuffix: %s\n",data_suffix_);
  return (const char*)data_suffix_;
}

#if 0
char *data_=NULL;
const char* GetPathData()
{
  if(data_)
    return data_;

  data_ = malloc (( strlen(GetDirPrefix()) + 512) * 2);
  sprintf( data_,"%s%s%s", GetDirPrefix(), DIR_SEPARATOR,
           GetDirDataSuffix());

  d_printf("GetPathData: %s\n",data_);
  return data_;
}
#endif

char* GetDirAbsoluteExec(const char *filename)
{
  char *path = NULL;

  /* Where are we? beku */
  if(filename)
  {
    size_t len = strlen(filename) * 2 + 1024;
    char *text = (char*) calloc( sizeof(char), len );
    text[0] = 0;
    /* whats the path for the executeable ? */
    snprintf (text, len-1, "%s", filename);

    if(text[0] == '~')
    {
      /* home directory */
      sprintf( text, "%s%s", GetPathHome(), &filename[0]+1 );
    }

    /* relative names - where the first sign is no directory separator */
    if (text[0] != DIR_SEPARATOR_C)
    {
      FILE *pp = NULL;

      if (text) free (text);
      text = (char*) malloc( 1024 );

      /* Suche das ausfuehrbare Programm
         TODO symbolische Verknuepfungen */
      snprintf( text, 1024, "which %s", filename);
      pp = popen( text, "r" );
      if (pp) {
        if (fscanf (pp, "%s", text) != 1)
        {
          pclose (pp);
          printf( "no executeable path found\n" );
        }
      } else {
        printf( "could not ask for executeable path\n" );
      }

      if(text[0] != DIR_SEPARATOR_C)
      {
        char* cn = (char*) malloc(2048 * sizeof(char));
        sprintf (cn, "%s%s%s", getenv("PWD"), DIR_SEPARATOR, filename);
        sprintf (text,"%s", cn);
        if(cn) free(cn);
      }
    }

    { /* remove the executable name */
      char *tmp = strrchr(text, DIR_SEPARATOR_C);
      if(tmp)
        *tmp = 0;
    }
    while (text[strlen(text)-1] == '.')
      text[strlen(text)-1] = 0;
    while (text[strlen(text)-1] == DIR_SEPARATOR_C)
      text[strlen(text)-1] = 0;

    path = text;
  }
  return path;
}

#include <dirent.h>
#include <sys/stat.h>
int
IsDir (const char* path)
{
  struct stat status;
  int r = 0;
  const char* name = path;
  status.st_mode = 0;
  r = stat (name, &status);
  r = !r &&
       ((status.st_mode & S_IFMT) & S_IFDIR);

  return r;
}

void AddSwapFile()
{
    LogoProgress(_("Tiles swap file"), _(""));
    const char* swap_path = GetPathDot();
    char* path = g_new(gchar, strlen(swap_path) + 32);
    sprintf(path, "%s/swapfile.%ld", swap_path, (long)getpid());
    tile_swap_add(path, NULL, NULL);
    g_free(path);
}


#if 0

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "iodebug.h"

/*
#define DATA_DIR "share"
#define GetPathData() DATA_DIR
#define GetPathDot() ".cinepaint"
 getenv("GIMP_DIRECTORY")
#define GetPathHome() getenv("HOME")
#define DOT_DIR ".cinepaint"


 -DLIBDIR=\""/opt/lib/filmgimp/0.16"\" -DDATADIR=\""/opt/share/filmgimp/0.16"\" -DGIMPDIR=\"".filmgimp"\"

programdatadir=$datadir/cinepaint/$VERSION
programplugindir=$libdir/cinepaint/$VERSION
programincludedir=${prefix}/include/cinepaint-$VERSION

*/

const char* GetPathHome()
{
    const char* home = getenv("HOME");
    d_printf("GetPathHome: %s\n", home);
    return home;
}

const char* GetPathDot()
{
    d_printf("GetPathDot: %s\n", DOTDIR);
    return DOTDIR;
}

const char* prefix_ = PREFIX;
const char* GetDirStaticPrefix()
{
    d_printf("GetDirStaticPrefix: %s\n", PREFIX);
    return PREFIX;
}

void SetDirPrefix(const char* prefix)
{
    d_printf("SetDirPrefix: %s\n", prefix);
    prefix_ = prefix;
}
const char* GetDirPrefix()
{
    d_printf("GetDirPrefix: %s\n", prefix_);
    return prefix_;
}

const char* cine_personal_rc_file(const char* rcfile)
{
    static char* rc_file;
    rc_file = (char*)malloc(sizeof(char) * 2048);
    if (!rc_file)
    {
        return "error";
    }
    sprintf(rc_file, "%s/%s/%s", GetPathHome(), GetPathDot(), rcfile);
    return rc_file;
}
#endif
