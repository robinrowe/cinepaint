#ifndef _LIST_H__
#define _LIST_H__

#include <glib.h>
#include "listF.h"


/* GimpList - a typed list of objects with signals for adding and
   removing of stuff. If it is weak, destroyed objects get removed
   automatically. If it is not, it refs them so they won't be freed
   till they are removed. (Though they can be destroyed, of course) */

#define GIMP_TYPE_LIST cine_list_get_type()
#define GIMP_LIST(obj) GTK_CHECK_CAST (obj, GIMP_TYPE_LIST, GimpList)
#define GIMP_IS_LIST(obj) GTK_CHECK_TYPE (obj, GIMP_TYPE_LIST)
     
/* Signals:
   add
   remove
*/


GtkType cine_list_get_type (void);

GimpList*	cine_list_new	 (GtkType type, gboolean weak);
GtkType		cine_list_type	 (GimpList* list);
gboolean       	cine_list_add	 (GimpList* gimplist, gpointer ob);
gboolean       	cine_list_remove (GimpList* gimplist, gpointer ob);
gboolean	cine_list_have	 (GimpList* gimplist, gpointer ob);
void		cine_list_foreach(GimpList* gimplist, GFunc func,
				  gpointer user_data);
gint		cine_list_size	 (GimpList* gimplist);

#endif
