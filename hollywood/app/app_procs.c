/* CinePaint -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <gdktablet.h>
#include "config.h"
#include "version.h"
#include "../app/datadir.h"
#include "i18n.h"
#include <time.h>
#include <unistd.h>
#include <gtk/gtk.h>
#include "../wire/enums.h"
#include "version.h"
#include "../app/datadir.h"
#include "appenv.h"
#include "app_procs.h"
#include "batch.h"
#include "brush.h"
#include "brushlist.h"
#include "color_transfer.h"
#include "curves.h"
#include "devices.h"
#include "../app/gdisplay.h"
#include "colormaps.h"
#include "fileops.h"
#include "rc.h"
#include "global_edit.h"
#include "gradient.h"
#include "gximage.h"
#include "hue_saturation.h"
#include "image_render.h"
#include "interface.h"
#include "internal_procs.h"
#include "layers_dialog.h"
#include "levels.h"
#include "menus.h"
#include "paint_funcs_area.h"
#include "palette.h"
#include "patterns.h"
#include "../pdb/plugin.h"
#include "../pdb/procedural_db.h"
#include "tile_swap.h"
#include "tips_dialog.h"
#include "tools.h"
#include "undo.h"
#include "xcf.h"
#include "xcf.h"
#include "errors.h"
#include "../app/actionarea.h"
#include "brush.h"
#include "layout.h"
#include "minimize.h"
#include "../depth/displaylut.h"
#include "config.h"
#include "../wire/wire.h"
#include "store_frame_manager.h"
#include "../app/app.h"
#include "about_dialog.h"
#include "libgimp/stdplugins-intl.h"
#include "app/text_tool.h"
#include "splash.h"

static void      really_quit_dialog (void);
static Argument* quit_invoker       (Argument *args);
static void server_start(int port, char *log);  /* added by IMAGEWORKS (doug creel 02/21/02) */
static void app_update_prefix (void);

static ProcArg quit_args[] =
{   {   PDB_INT32,
        "kill",
        "Flag specifying whether to kill CinePaint process or exit normally"
    },
};

static ProcData quit_proc =
{   "cine_quit",
    "Causes CinePaint to exit gracefully",
    "The internal procedure which can either be used to make CinePaint quit normally, or to have CinePaint clean up its resources and exit immediately. The normaly shutdown process allows for querying the user to save any dirty images.",
    "Spencer Kimball & Peter Mattis",
    "Spencer Kimball & Peter Mattis",
    "1995-1996",
    PDB_INTERNAL,
    1,
    quit_args,
    0,
    NULL,
    { { quit_invoker } },
};


/* added by IMAGEWORKS (doug creel 02/21/02) */
static void
server_start(int port, char *log)
{   Argument *args;
    Argument *vals;
    int i;
    ProcData *eval_proc;
    eval_proc = procedural_db_lookup ("extension_script_fu_server");
    if(!eval_proc)
    {   g_message (_("script-fu error: server mode not available\n"));
        return;
    }
    args = g_new0 (Argument, eval_proc->num_args);
    for (i = 0; i < eval_proc->num_args; i++)
    {   args[i].arg_type = eval_proc->args[i].arg_type;
    }
    args[0].value.pdb_int = 1;
    args[1].value.pdb_int = port;
    args[2].value.pdb_pointer = log;
    vals = procedural_db_execute (_("extension_script_fu_server"), args);
    switch (vals[0].value.pdb_int)
    {   case PDB_EXECUTION_ERROR:
            g_print ("server start: experienced an execution error.\n");
            break;
        case PDB_CALLING_ERROR:
            g_print ("server start: experienced a calling error.\n");
            break;
        case PDB_SUCCESS:
            g_print ("server start: executed successfully.\n");
            break;
        default:
            break;
    }
    procedural_db_destroy_args (vals, eval_proc->num_values);
    g_free(args);
    return;
}

void
cine_init (int    cine_argc,
           char **cine_argv)
{   int serverPort;
    char *serverLog;
    /* Parse the rest of the command line arguments as images to load */
    /* except for server arguments */
    if (cine_argc <= 0)
    {   return;
    }
    while (cine_argc-- > 0)
    {   if (*cine_argv == 0)
        {   cine_argv++;
            continue;
        }
        if( strcmp( *cine_argv, "-server" ) == 0 )
        {   cine_argc--;
            cine_argv++;
            serverPort = atoi( *cine_argv );
            cine_argc--;
            cine_argv++;
            serverLog = *cine_argv;
        }
        if (*cine_argv)
        {   if(!file_open (*cine_argv, *cine_argv))// /Users/rower/Desktop/who-covid-19.bmp
            {   puts("Cannot open file");
            }
            cine_argv++;
        }
    }
#if 0
    batch_init ();
#endif
    /* start server */
#if 0
    server_start(serverPort, serverLog);
#endif
    App* app = GetApp();
    /* Handle showing dialogs with gdk_quit_adds here */
    if (!app->no_interface && GetRC()->show_tips)
    {   tips_dialog_create ();
    }
    layout_restore();
    /*
    if (enable_brush_dialog)
            create_brush_dialog ();
    if (enable_layer_dialog)
            lc_dialog_create (-1);
    */
    if (app->start_with_sfm)
    {   sfm_dialog_create();
    }
}

void app_init (void)
{   char filename[PATH_MAX];
    char *cine_dir;
//  char *path;
    srand48 (time(NULL));
    App* app = GetApp();
    cine_dir = cine_directory ();
    if (cine_dir[0] != '\000')
    {   sprintf (filename, "%s/gtkrc", cine_dir);
        if ((app->be_verbose == TRUE) || (app->no_splash == TRUE))
        {   g_print ("gtk_rc_parse: \"%s\"\n", filename);
        }
        gtk_rc_parse (filename);
    }
    make_initialization_status_window();
#if 0
    if (app->no_interface == FALSE && app->no_splash == FALSE && app->win_initstatus)
    {   splash_text_draw (logo_area);
    }
#endif
    if (app->no_interface == FALSE && app->no_splash == FALSE /*&& show_logo */)
    {   if(load_splash_ppm(app->win_initstatus))
        {   Logo* logo = GetLogo();
            logo->show_logo = SHOW_NOW;
            splash_logo_draw(logo->logo_area);
        }
    }
    /*
     *  Initialize the procedural database
     *    We need to do this first because any of the init
     *    procedures might install or query it as needed.
     */
    text_get_fonts();
    procedural_db_init ();
    internal_procs_init ();
    procedural_db_register(&quit_proc);
    parse_cinerc ();         /*  parse the local CinePaint configuration file  */
    app_update_prefix();
    setlocale(LC_ALL, "");
    cinepaint_init_i18n();
    printf("Translation test: %s\n", _("About"));
    setlocale(LC_NUMERIC, "C");/* must use dot, not comma, as decimal separator */
    file_ops_pre_init ();    /*  pre-initialize the file types  */
    WireBufferOpen(); /* xcf uses wire buffer! */
    xcf_init ();             /*  initialize the xcf file format routines */
    cms_init();              /*  initialize colour management */
    brushes_init (app->no_data);         /*  initialize the list of CinePaint brushes  */
    patterns_init (app->no_data);        /*  initialize the list of CinePaint patterns  */
    palettes_init (app->no_data);        /*  initialize the list of CinePaint palettes  */
    gradients_init (app->no_data);       /*  initialize the list of CinePaint gradients  */
    plugin_init ();         /*  initialize the plug in structures  */
    file_ops_post_init ();   /*  post-initialize the file types  */
    AddSwapFile();
    /*  Things to do only if there is an interface  */
    if (app->no_interface == FALSE)
    {   get_standard_colormaps ();
        /* this has to be done after the colormaps are setup */
        palette_set_default_colors ();
        devices_init();
        create_toolbox ();
        display_u8_init();
        gximage_init ();
        render_setup (GetRC()->transparency_type, GetRC()->transparency_size);
        tools_options_dialog_new ();
        const int FIRST_TOOL_IN_TOOLBAR = 0;
        tools_select (FIRST_TOOL_IN_TOOLBAR);
        app->message_handler = MESSAGE_BOX;
    }
    color_transfer_init ();
    LogoProgress(_("Set brush and pattern"), _(""));
    get_active_brush ();
    get_active_pattern ();
    paint_funcs_area_setup ();
    LogoProgress(_("Tablet"), _(""));
    app->tablet = GdkTabletOpen();
    destroy_initialization_status_window();
}

void app_exit_finish (void)
{   App* app = GetApp();
    if (app_exit_finish_done ())
    {   return;
    }
    app->is_app_exit_finish_done = TRUE;
#ifdef LONG_SHUTDOWN
    app->message_handler = CONSOLE;
    // do this here so calls to destroy windows will be ignored from the layout's
    // point of view.
    layout_freeze_current_layout();
    lc_dialog_free ();
    gdisplays_delete ();
    global_edit_free ();
    named_buffers_free ();
    brushes_free ();
    patterns_free ();
    palettes_free ();
    gradients_free ();
    hue_saturation_free ();
#if 0
    curves_free ();
#endif
    levels_free ();
    brush_select_dialog_free ();
    file_temp_clear();
    pattern_select_dialog_free ();
    palette_free ();
    paint_funcs_area_free ();
#ifdef PLUGIN_KILL
    plugin_kill ();
    procedural_db_free ();
#endif
    menus_quit ();
    tile_swap_exit ();
    file_temp_clear();
    cms_free();
#endif
    /*  Things to do only if there is an interface  */
    if (app->no_interface == FALSE)
    {   gximage_free ();
        render_free ();
        tools_options_dialog_free ();
        layout_save();
    }
    GdkTabletClose(app->tablet);
    app->tablet = 0;
    /*  gtk_exit (0); */
    gtk_main_quit();
}
void
app_exit (int kill_it)
{   App* app = GetApp();
    /*  If it's the user's perogative, and there are dirty images  */
    if (kill_it == 0 && gdisplays_dirty () && app->no_interface == FALSE)
    {   really_quit_dialog ();
    }
    else if (app->no_interface == FALSE)
    {   toolbox_free ();
    }
    app_exit_finish ();
}
#define SHOW(name,path) printf("%s = %s\n",#name,path)
static void
app_update_prefix (void)
{   RcData* rc = GetRC();
    App* app = GetApp();
    rc->brush_path = StrDupPath(BRUSH_PATH,1);
    rc->pattern_path = StrDupPath(PATTERN_PATH, 1);
    rc->palette_path = StrDupPath(PALETTE_PATH, 1);
    rc->gradient_path = StrDupPath(GRADIENT_PATH, 1);
    rc->plugin_path = StrDupPath(PLUGIN_PATH, 0);
    app->po_path = StrDupPath(PO_PATH, 1);
    app->exe_path = StrDupPath(0, FALSE);
    //printf("PLUGIN_PATH = %s\n", rc->plugin_path);
    SHOW(BRUSH_PATH, rc->brush_path);
    SHOW(PLUGIN_PATH, rc->plugin_path);
    SHOW(PO_PATH, app->po_path);
}
/********************************************************
 *   Routines to query exiting the application          *
 ********************************************************/
static void
really_quit_callback (GtkWidget *button,
                      gpointer user_data)
{   GtkWidget *dialog;
    dialog = (GtkWidget *)user_data;
    gtk_widget_destroy (dialog);
    toolbox_free ();
}
static void
really_quit_cancel_callback (GtkWidget *widget,
                             gpointer user_data)
{   GtkWidget *dialog;
    dialog = (GtkWidget *)user_data;
    menus_set_sensitive ("<Toolbox>/File/Quit", TRUE);
    menus_set_sensitive ("<Image>/File/Quit", TRUE);
    gtk_widget_destroy (dialog);
}
static gint
really_quit_delete_callback (GtkWidget *widget,
                             GdkEvent  *event,
                             gpointer client_data)
{   really_quit_cancel_callback (widget, (GtkWidget *) client_data);
    return TRUE;
}
static GtkWidget *dialog;
static ActionAreaItem quit_action_items[] =
{   { "No", really_quit_cancel_callback, NULL, NULL },
    { "Yes", really_quit_callback, NULL, NULL }
};
static void
really_quit_dialog ()
{   GtkWidget *label;
    GtkWidget *vbox;
    menus_set_sensitive ("<Toolbox>/File/Quit", FALSE);
    menus_set_sensitive ("<Image>/File/Quit", FALSE);
    dialog = gtk_dialog_new ();
    dialog->name = "quit dialog";
    gtk_window_set_wmclass (GTK_WINDOW (dialog), "really_quit", PROGRAM_NAME);
    gtk_window_set_title (GTK_WINDOW (dialog), _("Really Quit?"));
    gtk_window_set_policy (GTK_WINDOW (dialog), FALSE, FALSE, FALSE);
    gtk_widget_set_uposition(dialog, GetRC()->generic_window_x, GetRC()->generic_window_y);
    layout_connect_window_position(dialog, &GetRC()->generic_window_x, &GetRC()->generic_window_y, FALSE);
    minimize_register(dialog);
    gtk_container_border_width (GTK_CONTAINER (GTK_DIALOG (dialog)->action_area), 2);
    gtk_signal_connect (GTK_OBJECT (dialog), "delete_event",
                        (GtkSignalFunc) really_quit_delete_callback,
                        dialog);
    vbox = gtk_vbox_new (FALSE, 1);
    vbox->name = "quit vbox";
    gtk_container_border_width (GTK_CONTAINER (vbox), 1);
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox),
                        vbox, TRUE, TRUE, 0);
    label = gtk_label_new (_("Really quit? -- Not all is saved."));
    label->name = "quit label";
    gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 4);
    gtk_misc_set_alignment (GTK_MISC (label), 0.5, 0.5);
    gtk_widget_show (label);
    quit_action_items[0].label = _(quit_action_items[0].label);
    quit_action_items[1].label = _(quit_action_items[1].label);
    quit_action_items[0].user_data = dialog;
    quit_action_items[1].user_data = dialog;
    build_action_area (GTK_DIALOG (dialog), quit_action_items, 2, 0);
    gtk_widget_show (vbox);
    gtk_widget_show (dialog);
}
static Argument*
quit_invoker (Argument *args)
{   Argument *return_args;
    int kill_it;
    kill_it = args[0].value.pdb_int;
    app_exit (kill_it);
    return_args = procedural_db_return_args (&quit_proc, TRUE);
    return return_args;
}
#define TEXTLEN 128
void
fl_set_codeset_(const char* lang, const char* codeset_,
                char* locale, char* codeset,
                int set_locale)
{   if (strlen(locale))
    {   char* pos = strstr(locale, lang);
        if (pos != 0)
        {   /* 1 a. select an appropriate charset (needed for non UTF-8 fltk/gtk1)*/
            sprintf(codeset,"%s", codeset_);
            /* merge charset with locale string */
            if (set_locale)
            {   if ((pos = strrchr(locale, '.')) != 0)
                {   *pos = 0;
                }
                else
                {   pos = &locale[strlen(locale)];
                }
                snprintf(pos, TEXTLEN - strlen(locale), ".%s", codeset);
            }
#if 0
            /* 1b. set correct environment variable LANG */
            {   char settxt[64];
                snprintf(settxt, 63, "LANG=%s", locale);
                (void)putenv(settxt);
            }
            setenv("LANG", locale, 1); /* setenv is not standard C */
#endif
            /* 1c. set the locale info after LANG */
            if (set_locale)
            {   char* ptr = setlocale(LC_ALL, "");
                if (ptr)
                {   snprintf(locale, TEXTLEN, "%s", ptr);
                }
            }
        }
    }
}
signed int
fl_search_locale_path(int n_places, const char** locale_paths,
                      const char* search_lang, const char* app_name)
{   int i;
    /* search in a given set of locale paths for a valid locale file */
    for (i = 0; i < n_places; ++i)
    {   if (locale_paths[i])
        {   char test[1024];
            FILE* fp = 0;
            /* construct the full path to a possibly valid locale file */
            snprintf(test, 1024, "%s%s%s%sLC_MESSAGES%s%s.mo",
                     locale_paths[i], DIR_SEPARATOR,
                     search_lang, DIR_SEPARATOR, DIR_SEPARATOR, app_name);
            /* test the file for existence */
            fp = fopen(test, "r");
            if (fp)
            {   fclose(fp);
                /* tell about the hit place */
                return i;
            }
        }
    }
    return -1;
}
void
fl_initialise_locale(const char* domain, const char* locale_path,
                     int set_locale)
{   char locale[TEXTLEN];
    char codeset[24] = "ISO-8859-1";
    const char* tmp = 0;
    const char* loc = NULL;
    const char* bdtd = 0;
# ifdef __APPLE__
    // 1. get the locale info
    CFLocaleRef userLocaleRef = CFLocaleCopyCurrent();
    CFStringRef cfstring = CFLocaleGetIdentifier(userLocaleRef);
    CFIndex gr = 36;
    char text[36];
    Boolean fehler = 0;
    CFShow(cfstring);
    // copy to a C buffer
    fehler = CFStringGetCString(cfstring, text, gr, kCFStringEncodingISOLatin1);
    if (fehler)
    {   d_printf("osX locale obtained: %s", text);
        snprintf(locale, TEXTLEN, text);
    }
    else
    {   d_printf("osX locale not obtained: %s", text);
    }
    // set the locale info
    if (strlen(locale) && set_locale)
    {
#if GTK_MAJOR_VERSION < 2
        setlocale(LC_MESSAGES, locale);
#else
        setlocale(LC_ALL, locale);
#endif
    }
    if (tmp)
    {   snprintf(locale, TEXTLEN, tmp);
    }
    set_locale = 0;
# else
    // 1. get default locale info ..
    // this is dangerous
    /*const char *tmp = setlocale (LC_MESSAGES, NULL);
    if(tmp) {
      snprintf(locale,TEXTLEN, tmp);
      DBG_PROG_V( locale )
    }*/
    // .. or take locale info from environment
    if (getenv("LANG"))
    {   snprintf(locale, TEXTLEN, "%s", getenv("LANG"));
    }
# endif
    // add more LINGUAS here
    // borrowed from http://czyborra.com/charsets/iso8859.html
    fl_set_codeset_("af", "ISO-8859-1", locale, codeset, set_locale); // Afrikaans
    fl_set_codeset_("ca", "ISO-8859-1", locale, codeset, set_locale); // Catalan
    fl_set_codeset_("da", "ISO-8859-1", locale, codeset, set_locale); // Danish
    fl_set_codeset_("de", "ISO-8859-1", locale, codeset, set_locale); // German
    fl_set_codeset_("en", "ISO-8859-1", locale, codeset, set_locale); // English
    fl_set_codeset_("es", "ISO-8859-1", locale, codeset, set_locale); // Spanish
    fl_set_codeset_("eu", "ISO-8859-1", locale, codeset, set_locale); // Basque
    fl_set_codeset_("fi", "ISO-8859-1", locale, codeset, set_locale); // Finnish
    fl_set_codeset_("fo", "ISO-8859-1", locale, codeset, set_locale); // Faroese
    fl_set_codeset_("fr", "ISO-8859-1", locale, codeset, set_locale); // French
    fl_set_codeset_("ga", "ISO-8859-1", locale, codeset, set_locale); // Irish
    fl_set_codeset_("gd", "ISO-8859-1", locale, codeset, set_locale); // Scottish
    fl_set_codeset_("is", "ISO-8859-1", locale, codeset, set_locale); // Icelandic
    fl_set_codeset_("it", "ISO-8859-1", locale, codeset, set_locale); // Italian
    fl_set_codeset_("nl", "ISO-8859-1", locale, codeset, set_locale); // Dutch
    fl_set_codeset_("no", "ISO-8859-1", locale, codeset, set_locale); // Norwegian
    fl_set_codeset_("pt", "ISO-8859-1", locale, codeset, set_locale); // Portuguese
    fl_set_codeset_("rm", "ISO-8859-1", locale, codeset, set_locale); // Rhaeto-Romanic
    fl_set_codeset_("sq", "ISO-8859-1", locale, codeset, set_locale); // Albanian
    fl_set_codeset_("sv", "ISO-8859-1", locale, codeset, set_locale); // Swedish
    fl_set_codeset_("sw", "ISO-8859-1", locale, codeset, set_locale); // Swahili
    fl_set_codeset_("cs", "ISO-8859-2", locale, codeset, set_locale); // Czech
    fl_set_codeset_("hr", "ISO-8859-2", locale, codeset, set_locale); // Croatian
    fl_set_codeset_("hu", "ISO-8859-2", locale, codeset, set_locale); // Hungarian
    fl_set_codeset_("pl", "ISO-8859-2", locale, codeset, set_locale); // Polish
    fl_set_codeset_("ro", "ISO-8859-2", locale, codeset, set_locale); // Romanian
    fl_set_codeset_("sk", "ISO-8859-2", locale, codeset, set_locale); // Slovak
    fl_set_codeset_("sl", "ISO-8859-2", locale, codeset, set_locale); // Slovenian
    fl_set_codeset_("eo", "ISO-8859-3", locale, codeset, set_locale); // Esperanto
    fl_set_codeset_("mt", "ISO-8859-3", locale, codeset, set_locale); // Maltese
    fl_set_codeset_("et", "ISO-8859-4", locale, codeset, set_locale); // Estonian
    fl_set_codeset_("lv", "ISO-8859-4", locale, codeset, set_locale); // Latvian
    fl_set_codeset_("lt", "ISO-8859-4", locale, codeset, set_locale); // Lithuanian
    fl_set_codeset_("kl", "ISO-8859-4", locale, codeset, set_locale); // Greenlandic
    fl_set_codeset_("be", "ISO-8859-5", locale, codeset, set_locale); // Byelorussian
    fl_set_codeset_("bg", "ISO-8859-5", locale, codeset, set_locale); // Bulgarian
    fl_set_codeset_("mk", "ISO-8859-5", locale, codeset, set_locale); // Macedonian
    fl_set_codeset_("ru", "ISO-8859-5", locale, codeset, set_locale); // Russian
    fl_set_codeset_("sr", "ISO-8859-5", locale, codeset, set_locale); // Serbian
    fl_set_codeset_("uk", "ISO-8859-5", locale, codeset, set_locale); // Ukrainian
    fl_set_codeset_("ar", "ISO-8859-6", locale, codeset, set_locale); // Arabic
    fl_set_codeset_("fa", "ISO-8859-6", locale, codeset, set_locale); // Persian
    fl_set_codeset_("ur", "ISO-8859-6", locale, codeset, set_locale); // Pakistani Urdu
    fl_set_codeset_("el", "ISO-8859-7", locale, codeset, set_locale); // Greek
    fl_set_codeset_("iw", "ISO-8859-8", locale, codeset, set_locale); // Hebrew
    fl_set_codeset_("ji", "ISO-8859-8", locale, codeset, set_locale); // Yiddish
    fl_set_codeset_("tr", "ISO-8859-9", locale, codeset, set_locale); // Turkish
    fl_set_codeset_("th", "ISO-8859-11", locale, codeset, set_locale); // Thai
    fl_set_codeset_("zh", "ISO-8859-15", locale, codeset, set_locale); // Chinese
    fl_set_codeset_("ja", "EUC", locale, codeset, set_locale); // Japan ; eucJP, ujis, EUC, PCK, jis7, SJIS
    fl_set_codeset_("hy", /*"UTF-8"*/"ARMSCII-8", locale, codeset, set_locale); // Armenisch
    // 2. for GNU _, the locale info is usually stored in the LANG variable
    loc = getenv("LANG");
    if (loc)
    {   // good
    }
    else
    {   // set LANG
#   ifdef __APPLE__
        if (strlen(locale))
        {   setenv("LANG", locale, 0);
        }
#   endif
        // good?
        if (getenv("LANG"))
        {   d_printf(getenv("LANG"));
        }
    }
    if (strlen(locale))
    {   d_printf(locale);
    }
    // 3. where to find the MO file? select an appropriate directory
    bdtd = bindtextdomain(domain, locale_path);
    // 4. set our charset
    //char* cs = bind_textdomain_codeset(domain, codeset);
    // 5. our translations
    textdomain(domain);
    // _ initialisation end
}
void
cinepaint_init_i18n()
{
    LogoProgress(_("Initialize i18n"), _(""));
   const char* locale_paths[2];
    const char* domain[] = { "cinepaint","cinepaint-std-plugins" };
    int         paths_n = 2, i;
    int path_nr = 0;
#if GTK_MAJOR_VERSION < 2
    int set_locale = 1;
#else
    int set_locale = 0;
#endif
    App* app = GetApp();
    locale_paths[0] = GetPathPo();
    locale_paths[1] = GetPathExe();
//    locale_paths[2] = 0;
    path_nr = fl_search_locale_path(paths_n,
                                    locale_paths,
                                    "de",
                                    domain[0]);
    if (path_nr >= 0)
    {   fl_initialise_locale(domain[0], locale_paths[path_nr], set_locale);
        fl_initialise_locale(domain[1], locale_paths[path_nr], set_locale);
        textdomain(domain[0]);
        printf("Locale found in %s\n", locale_paths[path_nr]);
        setenv("TEXTDOMAINDIR", locale_paths[path_nr], 1);
        return;
    }
    for (i = 0; i < paths_n; ++i)
    {   printf("Locale not found in %s\n", locale_paths[i]);
    }
}