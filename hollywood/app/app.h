/* CinePaint -- an image manipulation program
 * Copyright (C) 2020 Robin S. Rowe
 * License GPL
 */

#ifndef app_h
#define app_h

#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <glib/ghash.h>
#include "../pdb/procedural_db.h"
#include "../app/tag.h"
#include "../wire/enums.h"
#include "../wire/wirebuffer.h"
#include "../wire/c_typedefs.h"
#include "../dll_api.h"
#include "tile_cache.h"
#include "splash.h"
#include "i18n.h"

DLL_API void create_statusbar(GtkWidget* parent);
DLL_API void update_statusbar(const char* msg);
DLL_API void update_statusbar_progress(const char* msg,unsigned percent);
DLL_API void create_palettebar(GtkWidget* parent);

typedef enum {
	MESSAGE_BOX,
	CONSOLE
} MessageHandlerType;



typedef struct App
{	GtkWidget* win_initstatus;
	guint max_label_length;
	gint is_app_exit_finish_done;
	const char* dot_path;
	char* data_path;
	char* po_path;
	char* exe_path;
	char* last_path;
	const char* tips;
	char* arg0;
	unsigned data_offset;
	unsigned exe_offset;
	int no_interface;
	int no_data;
	int no_splash;
	int no_splash_image;
	int be_verbose;
	int use_debug_handler;
	int console_messages;
	int start_with_sfm;
	int initial_frames_loaded;
	WireBuffer* wire_buffer;
	MessageHandlerType message_handler;
	GtkTooltips* tool_tips;
	int active_color;
	gulong foreground_pixel;
	gulong background_pixel;
	GtkWidget* lc_shell;     /* declared in layers_dialogP.h */
	GtkWidget* lc_subshell;  /* seems local -hsbo */
	/*  Current device id  */
#if GTK_MAJOR_VERSION > 1
	GdkDevice* current_device;
#else
	gint current_device;
#endif
//	gint wire_tile_width;
//	gint wire_tile_height;
	GHashTable* pdb;
	TileCache tile_cache;
	GtkWidget* toolbox;
	GtkWidget* statusbar;
	struct GdkTablet* tablet;
} App;

typedef struct ColorData
{
	GdkVisual* g_visual;
	GdkColormap* g_cmap;

	gulong g_black_pixel;
	gulong g_gray_pixel;
	gulong g_white_pixel;
	gulong g_color_pixel;
	gulong g_normal_guide_pixel;
	gulong g_active_guide_pixel;

	gulong old_color_pixel;
	gulong new_color_pixel;

	gulong marching_ants_pixels[8];

	GtkDitherInfo* red_ordered_dither;
	GtkDitherInfo* green_ordered_dither;
	GtkDitherInfo* blue_ordered_dither;
	GtkDitherInfo* gray_ordered_dither;

	guchar*** ordered_dither_matrix;

	/*  These arrays are calculated for quick 24 bit to 16 color conversions  */
	gulong* g_lookup_red;
	gulong* g_lookup_green;
	gulong* g_lookup_blue;

	gulong* color_pixel_vals;
	gulong* gray_pixel_vals;
} ColorData;

typedef struct RcData
{
	char* plugin_path;
	char* temp_path;
	char* swap_path;
	char* brush_path;
	char* brush_vbr_path;
	char* default_brush;
	char* pattern_path;
	char* default_pattern;
	char* palette_path;
	char* frame_manager_path;
	char* default_palette;
	char* default_frame_manager;
	char* gradient_path;
	char* default_gradient;
	char* pluginrc_path;
	char* cms_profile_path;
	int       tile_cache_size;
	int       marching_speed;
	double    gamma_val;
	int       transparency_type;
	int       transparency_size;
	int       levels_of_undo;
	int       color_cube_shades[4];
	int       install_cmap;
	int       cycled_marching_ants;
	double    default_threshold;
	int       stingy_memory_use;
	int       allow_resize_windows;
	int       no_cursor_updating;
	int       preview_size;
	int       show_rulers;
	int       ruler_units;
	int       auto_save;
	int       cubic_interpolation;
	int       toolbox_x, toolbox_y;
	int       tips_x, tips_y;
	int       progress_x, progress_y;
	int       info_x, info_y;
	int       color_select_x, color_select_y;
	int       tool_options_x, tool_options_y;

	int       zoom_window_x, zoom_window_y;
	int       frame_manager_x, frame_manager_y;
	int       brush_select_x, brush_select_y;
	int       brush_edit_x, brush_edit_y;
	int       layer_channel_x, layer_channel_y;
	int       palette_x, palette_y;
	int       gradient_x, gradient_y;
	int       image_x, image_y;
	int       generic_window_x, generic_window_y;

	int       confirm_on_close;
	int       default_width, default_height;
	Format    default_format;
	Precision default_precision;
	int       show_tips;
	int       last_tip;
	int       show_tool_tips;
	int       gamut_alarm_red;
	int       gamut_alarm_green;
	int       gamut_alarm_blue;

	float     monitor_xres;
	float     monitor_yres;
	int       using_xserver_resolution;
	int       enable_rgbm_painting;
	int       enable_brush_dialog;
	int       enable_layer_dialog;
	int       enable_paste_c_disp;
	int       enable_tmp_saving;
	int       enable_channel_revert;

	int       tool_options_visible;
	int       zoom_window_visible;
	int       brush_select_visible;
	int       brush_edit_visible;
	int       layer_channel_visible;
	int       color_visible;
	int       palette_visible;
	int       gradient_visible;
	char* cms_default_image_profile_name;
	char* cms_default_proof_profile_name;
	char* cms_display_profile_name;
	char* cms_workspace_profile_name;
	int       cms_default_intent;
	int       cms_bpc_by_default;
	int       cms_default_proof_intent;
	int       cms_default_flags;
	int       cms_open_action;
	int       cms_mismatch_action;
	int       cms_manage_by_default;
	int       cms_oyranos;
	char* look_profile_path;

	int enable_rulers_on_start;
	int enable_brush_display;
	int enable_layer_display;
} RcData;

typedef enum {
	TT_STRING,
	TT_PATH,
	TT_DOUBLE,
	TT_INT,
	TT_BOOLEAN,
	TT_POSITION,
	TT_MEMSIZE,
	TT_IMAGETYPE,
	TT_XCOLORCUBE,
	TT_XPREVSIZE,
	TT_XRULERUNIT,
	TT_XPLUGIN,
	TT_XPLUGINDEF,
	TT_XMENUPATH,
	TT_XDEVICE
} RcTokenType;


typedef struct UnknownToken UnknownToken;

struct UnknownToken
{
	char* token;
	char* value;
};

typedef struct ParseFunc ParseFunc;

struct ParseFunc
{
	const char* name;
	RcTokenType type;
	gpointer val1p;
	gpointer val2p;
};

//extern DLL_API RC_data rc_data;

DLL_API int app_exit_finish_done(void);
DLL_API App* GetApp();
DLL_API void InitApp();
DLL_API RcData* GetRC();
DLL_API GHashTable* GetPDB();
DLL_API void InitRC();
DLL_API ColorData* GetColorData();
DLL_API void InitColorMap();
DLL_API struct PluginLoader* GetCurrentPlugin();

//#define LIB_TILE_WIDTH   GetApp()->wire_tile_width
//#define LIB_TILE_HEIGHT  GetApp()->wire_tile_height

DLL_API GSList* GetInternalPlugins();
DLL_API GSList* GetExternalPlugins();

inline
const char* GetBaseName(const char* long_filename)
{	const char* basename = strrchr(long_filename,'/');
	if(basename)
	{	return basename + 1;
	}
	basename = strrchr(long_filename, '\\');
	if (basename)
	{	return basename + 1;
	}
	return long_filename;
}

inline
char* RemoveBaseName(char* long_filename)
{	char* basename = strrchr(long_filename, '/');
	if (basename)
	{	*basename = 0;
		return long_filename;
	}
	basename = strrchr(long_filename, '\\');
	if (basename)
	{	*basename = 0;
		return long_filename;
	}
	return long_filename;
}
//#undef GET_SET

#endif 
