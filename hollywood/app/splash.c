// splash.c
// Robin Rowe 2024/11/25

#include "splash.h"
#include "app.h"
#include "../version.h"
#include "i18n.h"
#include "interface.h"

int splash_logo_load_size (GtkWidget *window)
{   char buf[1024];
    FILE *fp;
    Logo* logo = GetLogo();
    if (logo->logo_pixmap)
    {   return TRUE;
    }
    sprintf (buf, "%s/" SPLASH_FILE, GetPathData());
    fp = fopen (buf, "rb");
    if (!fp)
    {   return 0;
    }
    fgets (buf, 1024, fp);
    if (strcmp (buf, "P6\n") != 0)
    {   fclose (fp);
        return 0;
    }
    fgets (buf, 1024, fp);
    // buf += strlen(buf);
    fgets (buf, 1024, fp);
    (void) sscanf (buf, "%d %d", &logo->logo_width, &logo->logo_height);
    fclose (fp);
    return TRUE;
}

#if 0
int splash_logo_load (GtkWidget *window)
{   extern int load_splash_ppm(GtkWidget *window);
    return load_splash_ppm(window);
    GtkWidget *preview;
    GdkGC *gc;
    char buf[1024];
    unsigned char *pixelrow;
    FILE *fp;
    int count;
    int i;
    if (logo_pixmap)
    {   return TRUE;
    }
    sprintf (buf, "%s/" SPLASH_FILE, GetPathData());
    fp = fopen (buf, "rb");
    if (!fp)
    {   return 0;
    }
    fgets (buf, 1024, fp);
    if (strcmp (buf, "P6\n") != 0)
    {   fclose (fp);
        return 0;
    }
    fgets (buf, 1024, fp);
    fgets (buf, 1024, fp);
    sscanf (buf, "%d %d", &logo_width, &logo_height);
    fgets (buf, 1024, fp);
    if (strcmp (buf, "255\n") != 0)
    {   fclose (fp);
        return 0;
    }
    preview = gtk_preview_new (GTK_PREVIEW_COLOR);
    gtk_preview_size (GTK_PREVIEW (preview), logo_width, logo_height);
    pixelrow = g_new (guchar, logo_width * 3);
    for (i = 0; i < logo_height; i++)
    {   count = fread (pixelrow, sizeof (unsigned char), logo_width * 3, fp);
        if (count != (logo_width * 3))
        {   gtk_widget_destroy (preview);
            g_free (pixelrow);
            fclose (fp);
            return 0;
        }
        gtk_preview_draw_row (GTK_PREVIEW (preview), pixelrow, 0, i, logo_width);
    }
    gtk_widget_realize (window);
    logo_pixmap = gdk_pixmap_new (window->window, logo_width, logo_height,
                                  gtk_preview_get_visual ()->depth);
    gc = gdk_gc_new (logo_pixmap);
    gtk_preview_put (GTK_PREVIEW (preview),
                     logo_pixmap, gc,
                     0, 0, 0, 0, logo_width, logo_height);
    gdk_gc_destroy (gc);
    gtk_widget_unref (preview);
    g_free (pixelrow);
    fclose (fp);
    return TRUE;
}
#endif

void splash_text_draw (GtkWidget *widget)
{   GdkFont *font = NULL;
    font = gdk_font_load ("-Adobe-Helvetica-Bold-R-Normal--*-140-*-*-*-*-*-*");
    Logo* logo = GetLogo();
    gdk_draw_string (widget->window,
                     font,
                     widget->style->fg_gc[GTK_STATE_NORMAL],
                     ((logo->logo_area_width - gdk_string_width (font, PROGRAM_NAME)) / 2),
                     (gint) (0.25 * logo->logo_area_height),
                     PROGRAM_NAME);
    font = gdk_font_load ("-Adobe-Helvetica-Bold-R-Normal--*-120-*-*-*-*-*-*");
    gdk_draw_string (widget->window,
                     font,
                     widget->style->fg_gc[GTK_STATE_NORMAL],
                     ((logo->logo_area_width - gdk_string_width (font, PROGRAM_VERSION)) / 2),
                     (gint) (0.45 * logo->logo_area_height),
                     PROGRAM_VERSION);
#if 0
    gdk_draw_string (widget->window,
                     font,
                     widget->style->fg_gc[GTK_STATE_NORMAL],
                     ((logo_area_width - gdk_string_width (font, BROUGHT)) / 2),
                     (0.65 * logo_area_height),
                     BROUGHT);
    gdk_draw_string (widget->window,
                     font,
                     widget->style->fg_gc[GTK_STATE_NORMAL],
                     ((logo_area_width - gdk_string_width (font, AUTHORS)) / 2),
                     (0.80 * logo_area_height),
                     AUTHORS);
#endif
}
void splash_logo_draw (GtkWidget *widget)
{   Logo* logo = GetLogo();
    gdk_draw_pixmap (widget->window,
                     widget->style->black_gc,
                     logo->logo_pixmap,
                     0, 0,
                     ((logo->logo_area_width - logo->logo_width) / 2), ((logo->logo_area_height - logo->logo_height) / 2),
                     logo->logo_width, logo->logo_height);
//    splash_text_draw(widget);
//    splash_logo_expose(widget);
}

void splash_logo_expose (GtkWidget *widget)
{   Logo* logo = GetLogo();
    switch (logo->show_logo)
    {   case SHOW_NEVER:
        case SHOW_LATER:
            splash_text_draw (widget);
            break;
        case SHOW_NOW:
            splash_logo_draw (widget);
    }
}
#if 0
extern GtkWidget* label1;
extern GtkWidget* label2;
extern GtkWidget* win_initstatus;
extern GtkWidget* pbar;
extern guint max_label_length;
#endif

void destroy_initialization_status_window(void)
{   App* app = GetApp();
    GtkWidget* win_initstatus = app->win_initstatus;
    if(!win_initstatus)
    {   return;
    }
    gtk_widget_destroy(win_initstatus);
 //   gtk_widget_unref(win_initstatus);
    //   gtk_widget_unref(win_initstatus);
    //   gtk_widget_ref_sink(G_OBJECT(gadget))    //        g_object_unref(G_OBJECT(win_initstatus)); // decrease ref count to 0
    //gtk_close_window(win_initstatus);
#if 0
    Logo* logo = GetLogo();
    if (logo->logo_pixmap != NULL)
    {   gdk_pixmap_unref(logo->logo_pixmap);
    }
    //  InitApp();
    logo->logo_area = NULL;
    logo->logo_pixmap = NULL;
#endif
//    app->win_initstatus = 0;
}

void make_initialization_status_window(void)
{   App* app = GetApp();
    if (app->no_interface || app->no_splash )
    {   return;
    }
    Logo* logo = GetLogo();
    GtkWidget *vbox;
    GtkStyle *style;
#if GTK_MAJOR_VERSION > 1
    win_initstatus = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_type_hint (GTK_WINDOW(win_initstatus),
                              GDK_WINDOW_TYPE_HINT_DIALOG);
#else
    app->win_initstatus = gtk_window_new (GTK_WINDOW_DIALOG);
    app->win_initstatus->name = "win_initstatus";
//    g_object_ref_sink(G_OBJECT(app->win_initstatus)); // convert floating ref to standard ref
#endif
    gtk_signal_connect (GTK_OBJECT (app->win_initstatus), "delete_event",
                        GTK_SIGNAL_FUNC (gtk_true),
                        NULL);
    gtk_window_set_wmclass (GTK_WINDOW(app->win_initstatus), "startup", PROGRAM_NAME);
    gtk_window_set_title(GTK_WINDOW(app->win_initstatus),
                         _("CinePaint Startup"));
    if (app->no_splash == FALSE && splash_logo_load_size (app->win_initstatus))
    {   logo->show_logo = SHOW_LATER;
    }
    vbox = gtk_vbox_new(FALSE, 4);
    vbox->name = "app_initstatus vbox";
    gtk_container_add(GTK_CONTAINER(app->win_initstatus), vbox);
    gtk_widget_push_visual (gtk_preview_get_visual ());
    gtk_widget_push_colormap  (gtk_preview_get_cmap ());
    gtk_widget_pop_colormap ();
    gtk_widget_pop_visual ();
    gtk_drawing_area_size (GTK_DRAWING_AREA (logo->logo_area), logo->logo_area_width, logo->logo_area_height);
    gtk_box_pack_start_defaults(GTK_BOX(vbox), logo->logo_area);
    logo->label1 = gtk_label_new("");
    logo->label1->name = "logo label1";
    gtk_box_pack_start_defaults(GTK_BOX(vbox), logo->label1);
    logo->label2 = gtk_label_new("");
    logo->label2->name = "logo label2";
#if 1
    gtk_signal_connect(GTK_OBJECT(logo->logo_area), "expose_event",
        (GtkSignalFunc)splash_logo_expose, NULL);
#endif
    gtk_box_pack_start_defaults(GTK_BOX(vbox), logo->label2);
    logo->pbar = gtk_progress_bar_new();
    logo->pbar->name = "app progress bar";
    gtk_box_pack_start_defaults(GTK_BOX(vbox), logo->pbar);
    gtk_widget_show(vbox);
    gtk_widget_show (logo->logo_area);
    gtk_widget_show(logo->label1);
    gtk_widget_show(logo->label2);
    gtk_widget_show(logo->pbar);
    gtk_window_position(GTK_WINDOW(app->win_initstatus),
                        GTK_WIN_POS_CENTER);
    gtk_widget_show(app->win_initstatus);
    gtk_window_set_policy (GTK_WINDOW (app->win_initstatus), FALSE, TRUE, FALSE);
    /*
     *  This is a hack: we try to compute a good guess for the maximum
     *  number of charcters that will fit into the splash-screen using
     *  the default_font
     */
    style = gtk_widget_get_style (app->win_initstatus);
    app->max_label_length = MAX_LABEL_LEN;
}

#if 0
void LogoProgress(const char *label1val,
                       const char *label2val,
                       float pct_progress)
{   char *temp;
    App* app = GetApp();
    Logo* logo = GetLogo();
    if(app->no_interface == FALSE && app->no_splash == FALSE && app->win_initstatus)
    {   GdkRectangle area = {0, 0, -1, -1};
        if(label1val
                && strcmp(label1val, GTK_LABEL(logo->label1)->label))
        {   gtk_label_set(GTK_LABEL(logo->label1), label1val);
        }
        if(label2val
                && strcmp(label2val, GTK_LABEL(logo->label2)->label))
        {   while ( strlen (label2val) > app->max_label_length )
            {   temp = strchr (label2val, '/');
                if (temp == NULL)  /* for sanity */
                {   break;
                }
                temp++;
                label2val = temp;
            }
            gtk_label_set(GTK_LABEL(logo->label2), label2val);
        }
        if (pct_progress >= 0.0 && pct_progress <= 1.0 &&
                gtk_progress_get_current_percentage(&(GTK_PROGRESS_BAR(logo->pbar)->progress)) != pct_progress)
            /*
             GTK_PROGRESS_BAR(pbar)->percentage != pct_progress)
            */
        {   gtk_progress_bar_update(GTK_PROGRESS_BAR(logo->pbar), pct_progress);
        }
        gtk_widget_draw(app->win_initstatus, &area);
        while (gtk_events_pending())
        {   gtk_main_iteration();
        }
        /* We sync here to make sure things get drawn before continuing,
         * is the improved look worth the time? I'm not sure...
         */
        gdk_flush();
    }
}
#endif

static Logo* logo;

Logo* GetLogo()
{   if(logo)
    {   return logo;
    }
    logo = (Logo*)malloc(sizeof(Logo));
    if (!logo)
    {   return 0;
    }
    memset(logo, 0, sizeof(Logo));
    logo->logo_area = gtk_drawing_area_new();
    logo->logo_area->name = "logo area";
    logo->logo_area_width = (logo->logo_width > LOGO_WIDTH_MIN) ? logo->logo_width : LOGO_WIDTH_MIN;
    logo->logo_area_height = (logo->logo_height > LOGO_HEIGHT_MIN) ? logo->logo_height : LOGO_HEIGHT_MIN;
    return logo;
}

#define PROGRESS_STEPS 389.

void LogoProgress(const char* label1val,const char* label2val)
{   static float pct_progress;
    pct_progress += 1./PROGRESS_STEPS;
#if 0
    static int count;
    printf("count = %i\n",++count);
//    progress_update(label1val, pct_progress);
#endif
    char* temp;
    App* app = GetApp();
    Logo* logo = GetLogo();

    if (app->no_interface == FALSE && app->no_splash == FALSE && app->win_initstatus)
    {
        GdkRectangle area = { 0, 0, -1, -1 };
        if (label1val
            && strcmp(label1val, GTK_LABEL(logo->label1)->label))
        {
            gtk_label_set(GTK_LABEL(logo->label1), label1val);
        }
        if (label2val
            && strcmp(label2val, GTK_LABEL(logo->label2)->label))
        {
            while (strlen(label2val) > app->max_label_length)
            {
                temp = strchr(label2val, '/');
                if (temp == NULL)  /* for sanity */
                    break;
                temp++;
                label2val = temp;
            }
            gtk_label_set(GTK_LABEL(logo->label2), label2val);
        }
        gfloat progress = gtk_progress_get_current_percentage(&(GTK_PROGRESS_BAR(logo->pbar)->progress));
        if (pct_progress >= 0.0 && pct_progress <= 1.0 && progress != pct_progress)
        {   gtk_progress_bar_update(GTK_PROGRESS_BAR(logo->pbar), pct_progress);
        }
        gtk_widget_draw(app->win_initstatus, &area);
        while (gtk_events_pending())
            gtk_main_iteration();
        /* We sync here to make sure things get drawn before continuing,
         * is the improved look worth the time? I'm not sure...
         */
        gdk_flush();
    }
}
