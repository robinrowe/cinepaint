/* CinePaint -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef  __GIMAGE_CMDS_H__
#define  __GIMAGE_CMDS_H__

#include "../pdb/procedural_db.h"

void  channel_ops_duplicate (void *);

extern ProcData gimage_list_images_proc;
extern ProcData gimage_new_proc;
extern ProcData gimage_resize_proc;
extern ProcData gimage_scale_proc;
extern ProcData gimage_delete_proc;
extern ProcData gimage_free_shadow_proc;
extern ProcData gimage_is_layered_proc;
extern ProcData gimage_dirty_flag_proc;
extern ProcData gimage_get_layers_proc;
extern ProcData gimage_get_channels_proc;
extern ProcData gimage_get_active_layer_proc;
extern ProcData gimage_get_active_channel_proc;
extern ProcData gimage_get_selection_proc;
extern ProcData gimage_get_component_active_proc;
extern ProcData gimage_get_component_visible_proc;
extern ProcData gimage_set_active_layer_proc;
extern ProcData gimage_set_active_channel_proc;
extern ProcData gimage_unset_active_channel_proc;
extern ProcData gimage_set_component_active_proc;
extern ProcData gimage_set_component_visible_proc;
extern ProcData gimage_pick_correlate_layer_proc;
extern ProcData gimage_raise_layer_proc;
extern ProcData gimage_lower_layer_proc;
extern ProcData gimage_merge_visible_layers_proc;
extern ProcData gimage_flatten_proc;
extern ProcData gimage_add_layer_proc;
extern ProcData gimage_remove_layer_proc;
extern ProcData gimage_add_layer_mask_proc;
extern ProcData gimage_remove_layer_mask_proc;
extern ProcData gimage_raise_channel_proc;
extern ProcData gimage_lower_channel_proc;
extern ProcData gimage_add_channel_proc;
extern ProcData gimage_remove_channel_proc;
extern ProcData gimage_active_drawable_proc;
extern ProcData gimage_base_type_proc;
extern ProcData gimage_get_filename_proc;
extern ProcData gimage_set_filename_proc;
extern ProcData gimage_width_proc;
extern ProcData gimage_height_proc;
extern ProcData gimage_get_cmap_proc;
extern ProcData gimage_set_cmap_proc;
extern ProcData gimage_enable_undo_proc;
extern ProcData gimage_disable_undo_proc;
extern ProcData gimage_clean_all_proc;
extern ProcData gimage_floating_sel_proc;
extern ProcData channel_ops_duplicate_proc;
extern ProcData gimage_thumbnail_proc;
extern ProcData gimage_set_icc_profile_by_name_proc;
extern ProcData gimage_set_icc_profile_by_mem_proc;
extern ProcData gimage_set_lab_profile_proc;
extern ProcData gimage_set_xyz_profile_proc;
extern ProcData gimage_set_srgb_profile_proc;
extern ProcData gimage_has_icc_profile_proc;
extern ProcData gimage_get_icc_profile_by_mem_proc;
extern ProcData gimage_get_icc_profile_info_proc;
extern ProcData gimage_get_icc_profile_description_proc;
extern ProcData gimage_get_icc_profile_pcs_proc;
extern ProcData gimage_get_icc_profile_color_space_name_proc;
extern ProcData gimage_get_icc_profile_device_class_name_proc;

#endif  /*  __GIMAGE_CMDS_H__  */
