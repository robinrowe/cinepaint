// splash.h
// Robin Rowe 2024/11/25

#ifndef splash_h
#define splash_h

#include <gtk/gtk.h>
#include "datadir.h"
#include "dll_api.h"
#include "i18n.h"

DLL_API void LogoProgress(const char* label1val, const char* label2val);

typedef struct Logo
{
	GtkWidget* logo_area;
	GtkWidget* scroll_area;
	GdkPixmap* logo_pixmap;
	GdkPixmap* scroll_pixmap;
	GtkWidget* label1;
	GtkWidget* label2;
	GtkWidget* pbar;
	unsigned char* dissolve_map;
	int dissolve_width;
	int dissolve_height;
	int logo_width;
	int logo_height;
	int do_animation;
	int do_scrolling;
	int scroll_state;
	int frame;
	int offset;
	int timer;
	int logo_area_width;
	int logo_area_height;
	int show_logo;
} Logo;

Logo* GetLogo();
void make_initialization_status_window(void);
void destroy_initialization_status_window(void);
int splash_logo_load_size(GtkWidget* window);
void splash_logo_draw(GtkWidget* widget);
void splash_text_draw(GtkWidget* widget);
void splash_logo_expose(GtkWidget* widget);
int load_splash_ppm(GtkWidget* window);

#define SPLASH_FILE "spot.splash.ppm"

#define SHOW_NEVER 0
#define SHOW_LATER 1
#define SHOW_NOW 2

#endif