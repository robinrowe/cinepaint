/* CinePaint -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#include "tools.h"

/* typedefs */
typedef void (*QueryFunc) (GtkWidget *, gpointer, gpointer);

/* externed variables  */
extern GtkWidget *tool_widgets[];
extern GtkWidget *popup_shell;
extern gint       num_tools;

/* function declarations */
GtkWidget *  create_pixmap_widget (GdkWindow *, const char **, int, int, const char*);
GdkPixmap *  create_tool_pixmap (GtkWidget *, ToolType);
void         create_toolbox (void);
void	     toolbox_free (void);
void         toolbox_raise_callback (GtkWidget *, gpointer);
void         create_display_shell (int, int, int, char *, int);
void         position_dialog (GtkWidget *, gpointer, gpointer);
void         center_dialog (GtkWidget *, gpointer, gpointer);
DLL_API GtkWidget *  query_string_box (const char *,const char *,const char *, QueryFunc, gpointer);
GtkWidget *  message_box (const char *, GtkCallback, gpointer);

void tools_push_label (char *label);
void tools_pop_label (void);

void progress_start ();
void progress_update (const char* msg,float);
void progress_step (const char* msg);
void progress_end ();

typedef struct ToolButton
{
  const char **icon_data;
  const char *tango_icon_data;
  const char  *tool_desc;
  uintptr_t callback_data;
} ToolButton;

#define TOOLBOX_COUNT 26
extern ToolButton tool_data[TOOLBOX_COUNT];

inline
const char* GetToolDesc(int type)
{	if(type <0 || type >= TOOLBOX_COUNT)
	{	return 0;
	}
	return tool_data[type].tool_desc;
}

void color_to_palette_bar(gfloat* color);

#endif /* INTERFACE_H */
