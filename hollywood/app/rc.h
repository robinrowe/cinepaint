/* CinePaint -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef _RC_H__
#define _RC_H__

#include <glib.h>
#include "tag.h"
#include "../pdb/procedural_db.h"
#include "../app/datadir.h"
#include "../app/app.h"

/*  function prototypes  */
char *  cine_directory (void);
void    parse_cinerc (void);
void    parse_cinerc_file (char *filename);
void    save_cinerc (GList **updated_options, GList **conflicting_options);
DLL_API Argument* cinerc_query(Argument* args);

DLL_API GList* cine_path_parse(gchar* path,
	gint       max_paths,
	gboolean   check,
	GList** check_failed);

/*  procedural database procs  */
extern ProcData cinerc_query_proc;

#endif  /*  _RC_H__  */
