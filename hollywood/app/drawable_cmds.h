/* CinePaint -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __DRAWABLE_CMDS_H__
#define __DRAWABLE_CMDS_H__

#include "../pdb/procedural_db.h"

extern ProcData drawable_merge_shadow_proc;
extern ProcData drawable_fill_proc;
extern ProcData drawable_update_proc;
extern ProcData drawable_mask_bounds_proc;
extern ProcData drawable_gimage_proc;
extern ProcData drawable_type_proc;
extern ProcData drawable_has_alpha_proc;
extern ProcData drawable_type_with_alpha_proc;
extern ProcData drawable_color_proc;
extern ProcData drawable_gray_proc;
extern ProcData drawable_indexed_proc;
extern ProcData drawable_bytes_proc;
extern ProcData drawable_precision_proc;
extern ProcData drawable_num_channels_proc;
extern ProcData drawable_width_proc;
extern ProcData drawable_height_proc;
extern ProcData drawable_offsets_proc;
extern ProcData drawable_layer_proc;
extern ProcData drawable_layer_mask_proc;
extern ProcData drawable_channel_proc;
extern ProcData drawable_set_pixel_proc;
extern ProcData drawable_get_pixel_proc;

#endif /* __DRAWABLE_CMDS_H__ */
