/* CinePaint -- an image manipulation program
Copyright 2021/04/18 Robin.Rowe@CinePaint.org 
License open source MIT
 */

#ifndef notes_dialog_h
#define notes_dialog_h

#include <gtk/gtk.h>

char* strdup2(const char* first, const char* last);
const char* GetNotepad();
void LaunchNotepad(const char* docPath,const char* docName);
void notes_prod_dialog_cmd_callback(GtkWidget* widget,gpointer client_data);
void notes_tips_dialog_cmd_callback(GtkWidget* widget, gpointer client_data);
void notes_bugs_dialog_cmd_callback(GtkWidget* widget, gpointer client_data);
void notes_changes_dialog_cmd_callback(GtkWidget* widget, gpointer client_data);
void notes_roadmap_dialog_cmd_callback(GtkWidget* widget, gpointer client_data);
void notes_readme_dialog_cmd_callback(GtkWidget* widget, gpointer client_data);

#endif
