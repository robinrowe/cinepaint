
/* CinePaint -- an image manipulation program
Copyright 2021/04/18 Robin.Rowe@CinePaint.org
License open source MIT
 */

#include <unistd.h>
#include <string.h>
#include <memory.h>
#include <stdlib.h>
#include <stdio.h>
#ifdef _WIN32
#include <processthreadsapi.h>
#else
#include <sys/types.h>
#endif
#include "datadir.h"
#include "notes_dialog.h"

#define NOTES_PROD_FILENAME "cinenotes"
#define NOTES_DATA_FILENAME "notes.md"
#define NOTES_TIPS_FILENAME "data/tips.txt"
#define NOTES_BUGS_FILENAME "data/BUGS.md"
#define NOTES_CHANGES_FILENAME "data/CHANGES.md"
#define NOTES_ROADMAP_FILENAME "data/ROADMAP.md"
#define NOTES_README_FILENAME "README.md"

char* strdup2path(const char* path, const char* basename, bool isSpace)
{
    if (!path || !basename)
    {
        return 0;
    }
    size_t len1 = strlen(path);
    size_t len2 = strlen(basename);
    char* s = malloc(len1 + len2 + 3); // space, slash, terminator
    if(!s)
    {   return 0;
    }
//    *s = '\"';
//    char* p = s + 1;
    char* p = s;
    if (isSpace)
    {   *p = ' ';
        p++;
    }
    memcpy(p, path, len1);
    p += len1;
    if('/' != *p)
    {   *p = '/';
        p++;
    }
    memcpy(p, basename, len2);
    p += len2;
//    *p = '"';
//    p++;
    *p = 0;
    return s;
}

char* strdup2cmd(const char* appName, const char* args)
{   const size_t len1 = strlen(appName);
    const size_t len2 = strlen(args);
    char* cmd = malloc(1 + len1 + 1 + len2);
    char* p = cmd;
    memcpy(p, appName, len1);
    p += len1;
    *p = ' ';
    p += +1 + len1;
    memcpy(p, args, len2);
    p += len2 + 1;
    *p = 0;
    return cmd;
}

#ifdef _WIN32
#define APP_EXT ".exe"
#else
#define APP_EXT ""
#endif

const char* GetNotepad()
{
    static const char* appName;
    if (appName)
    {
        return appName;
    }
    appName = strdup2path(GetPathExe(),NOTES_PROD_FILENAME APP_EXT,FALSE);
    return appName;
}

void LaunchNotepad(const char* docPath, const char* docName)
{
    const char* appName = GetNotepad();
    if(!docPath)
    {   docPath = GetPathExe();
    }
    char* args = strdup2path(docPath, docName,TRUE);
    if(!args)
    {   puts("ERROR: invalid path");
        return;
    }
//    char* cmd = strdup2cmd(NOTES_PROD_FILENAME APP_EXT,args,FALSE);
#ifdef _WIN32
    STARTUPINFO siStartupInfo;
    PROCESS_INFORMATION piProcessInfo;
    memset(&siStartupInfo, 0, sizeof(siStartupInfo));
    memset(&piProcessInfo, 0, sizeof(piProcessInfo));
    siStartupInfo.cb = sizeof(siStartupInfo);
#if 0
    DWORD  dwCreationFlags = CREATE_NEW_CONSOLE;
#else
    DWORD  dwCreationFlags = CREATE_DEFAULT_ERROR_MODE;
#endif
    BOOL ok = CreateProcessA(appName,
        args,
        NULL,
        NULL,
        FALSE,
        dwCreationFlags,
        NULL,
        NULL,
        &siStartupInfo,
        &piProcessInfo);
        if (ok)
        {
            printf("Launched %s %s\n", appName, args);
        }
        else
        {
            printf("ERROR: Cannot launch %s %s\n", appName, args);
        }
#if 0
    // Wait until application has terminated
    WaitForSingleObject(piProcessInfo.hProcess, INFINITE);
    // Close process and thread handles
    ::CloseHandle(piProcessInfo.hThread);
    ::CloseHandle(piProcessInfo.hProcess);
#endif
#else
    pid_t p = fork();
    if(p==-1)
    {	return;
    }
    if(p!=0)
    {	return;
    }
    execv(appName,&args);
#endif
    free((void*)args);
//    free((void*) cmd);
}

void notes_prod_dialog_cmd_callback(GtkWidget* widget, gpointer client_data)
{
    LaunchNotepad(GetPathLast(), NOTES_DATA_FILENAME);
}

void notes_tips_dialog_cmd_callback(GtkWidget* widget, gpointer client_data)
{
    LaunchNotepad(GetPathData(), NOTES_TIPS_FILENAME);
}

void notes_bugs_dialog_cmd_callback(GtkWidget* widget, gpointer client_data)
{
    LaunchNotepad(GetPathData(), NOTES_BUGS_FILENAME);
}

void notes_changes_dialog_cmd_callback(GtkWidget* widget, gpointer client_data)
{
    LaunchNotepad(GetPathData(), NOTES_CHANGES_FILENAME);
}

void notes_roadmap_dialog_cmd_callback(GtkWidget* widget, gpointer client_data)
{
    LaunchNotepad(GetPathData(), NOTES_ROADMAP_FILENAME);
}

void notes_readme_dialog_cmd_callback(GtkWidget* widget, gpointer client_data)
{
    LaunchNotepad(GetPathData(), NOTES_README_FILENAME);
}