#ifndef __OBJECT_H__
#define __OBJECT_H__

#include <gtk/gtk.h>
#include <gtk/gtktypeutils.h>
#include "objectF.h"

#if GTK_MAJOR_VERSION > 1
#ifndef GTK_DISABLE_DEPRECATED
#define GTK_DISABLE_DEPRECATED
#endif
#endif

#define GIMP_TYPE_OBJECT cine_object_get_type()

#define GIMP_OBJECT(obj) GTK_CHECK_CAST (obj, GIMP_TYPE_OBJECT, GimpObject)

#define GIMP_IS_OBJECT(obj) GTK_CHECK_TYPE (obj, GIMP_TYPE_OBJECT)


GtkType cine_object_get_type(void);

/* hacks to fake a CinePaint object lib */
#define GIMP_CHECK_CAST GTK_CHECK_CAST
#define GIMP_CHECK_TYPE GTK_CHECK_TYPE
#define cine_type_new gtk_type_new
#define cine_object_destroy(obj) gtk_object_destroy(GTK_OBJECT(obj))
#define cine_object_ref(obj) gtk_object_ref(GTK_OBJECT(obj))
#define cine_object_unref(obj) gtk_object_unref(GTK_OBJECT(obj))

#endif




	
