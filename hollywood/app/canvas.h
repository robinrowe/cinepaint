/* CinePaint -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __CANVAS_H__
#define __CANVAS_H__

#include "tag.h"
#include "tile.h"
#include "../wire/c_typedefs.h"

typedef enum
{
  STORAGE_NONE   = 0,
  STORAGE_FLAT   = 1,
  STORAGE_TILED  = 2
#ifdef BUILD_SHM
  ,
  STORAGE_SHM    = 3
#endif
} StorageType;


/* should a ref of an unalloced portion automatically allocate the
   memory or simply fail */
typedef enum
{
  AUTOALLOC_NONE = 0,
  AUTOALLOC_OFF  = 1,
  AUTOALLOC_ON   = 2
} AutoAlloc;


/* ways to reference a portion */
typedef enum
{
  REFTYPE_NONE  = 0,
  REFTYPE_READ  = 1,
  REFTYPE_WRITE = 2
} RefType;


/* the result of referencing a portion.  a successful ref means the
   data pointer is non-null */
typedef enum
{
  REFRC_NONE   = 0,
  REFRC_OK     = 1,
  REFRC_FAIL   = 2
} RefRC;

struct Canvas;
struct TileManager;

typedef guint(*CanvasInitFunc) (struct Canvas*, int, int, int, int, void*);
typedef void (*TileValidateProc) (struct TileManager* tm,Tile* tile,int level);

typedef struct TileLevel
{
	int width;                       /* the width of the tiled area */
	int height;                      /* the height of the tiled area */
	int bpp;                         /* the bpp of each tile */

	int ntile_rows;                  /* the number of tiles in each row */
	int ntile_cols;                  /* the number of tiles in each columns */

	Tile* tiles;                     /* the tiles for this level */
} TileLevel;

#if 0
struct TileManagerPvt
{
	int x, y;                        /* tile manager offsets  */
	int nlevels;                     /* the number of tile levels in the hierarchy */
	TileLevel* levels;               /* the hierarchy */
	TileValidateProc validate_proc;  /* this proc is called when an attempt to get an
					  *  invalid tile is made.
					  */
	void* user_data;                 /* hook for hanging data off of */
};
#endif

typedef struct Canvas
{
	/* the image size */
	int   width;
	int   height;

	/* should a ref of a non-existent area allocate the memory? */
	AutoAlloc  autoalloc;

	/* the type of buffer holding the image
	STORAGE_NONE   = 0,
	STORAGE_FLAT   = 1,
	STORAGE_TILED  = 2,
	STORAGE_SHM    = 3 */

	StorageType    storage;
	void* rep;

	/* function and data for initializing new memory */
	CanvasInitFunc init_func;
	void* init_data;

	/* cached info about the physical rep */
	Tag   tag;
	gsize   bytes;
	int nlevels;                     /* the number of tile levels in the hierarchy */
	TileLevel* levels;               /* the hierarchy */
	TileValidateProc validate_proc;  /* this proc is called when an attempt to get an
					  *  invalid tile is made.
					  */
	void* user_data;                 /* hook for hanging data off of */
	/* this is so wrong */
	int x;
	int y;
} Canvas;

DLL_API Canvas *       canvas_new            (Tag, int w, int h, StorageType);
DLL_API void           canvas_delete         (Canvas *);
void           canvas_info           (Canvas *);

DLL_API Tag            canvas_tag            (Canvas *);
Precision      canvas_precision      (Canvas *);
Format         canvas_format         (Canvas *);
Alpha          canvas_alpha          (Canvas *);

StorageType        canvas_storage        (Canvas *);
AutoAlloc      canvas_autoalloc      (Canvas *);
AutoAlloc      canvas_set_autoalloc  (Canvas *, AutoAlloc);

DLL_API int            canvas_width          (Canvas *);
DLL_API int            canvas_height         (Canvas *);
int            canvas_bytes          (Canvas *);


/* a portion is a rectangular area of a Canvas that resides on a
   single underlying chunk of memory (eg: a tile) */

/* return the TOP LEFT coordinate of the tile this pixel lies on */
guint          canvas_portion_x         (Canvas *, int x, int y);
guint          canvas_portion_y         (Canvas *, int x, int y);

/* return the maximum width and height of the rectangle that has the
   specified TOP LEFT pixel AND is contained on a single tile */
guint          canvas_portion_width     (Canvas *, int x, int y);
guint          canvas_portion_height    (Canvas *, int x, int y);

/* get the data pointer and rowstride for the rectangle with the
   specified TOP LEFT corner */
DLL_API guchar *       canvas_portion_data      (Canvas *, int x, int y);
guint          canvas_portion_rowstride (Canvas *, int x, int y);

/* check if a pixel needs to have memory allocated, alloc and dealloc
   the memory */
guint          canvas_portion_alloced   (Canvas *, int x, int y);
guint          canvas_portion_alloc     (Canvas *, int x, int y);
guint          canvas_portion_unalloc   (Canvas *, int x, int y);

/* allocate and/or swap in the backing store for this pixel */
DLL_API RefRC          canvas_portion_refro     (Canvas *, int x, int y);
RefRC          canvas_portion_refrw     (Canvas *, int x, int y);
DLL_API RefRC          canvas_portion_unref     (Canvas *, int x, int y);

/* initialize the backing store for this pixel */
void           canvas_portion_init_setup (Canvas *, CanvasInitFunc, void *);
guint          canvas_portion_init       (Canvas *, int x, int y, int w, int h);



/* FIXME FIXME FIXME */
int canvas_fixme_getx (Canvas *);
int canvas_fixme_gety (Canvas *);
int canvas_fixme_setx (Canvas *, int);
int canvas_fixme_sety (Canvas *, int);
int canvas_fixme_width (Canvas *, int);
int canvas_fixme_height (Canvas *, int);

typedef Canvas TileManager;

#endif /* __CANVAS_H__ */
