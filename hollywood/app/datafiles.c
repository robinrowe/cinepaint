/* CinePaint -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * Datafiles module copyight (C) 1996 Federico Mena Quintero
 * federico@nuclecu.unam.mx
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

//#define _USE_32BIT_TIME_T

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <glib.h>
#include "datafiles.h"
#include "errors.h"
#include "general.h"
#include "rc.h"
#include "../app/datadir.h"
#include "../wire/iodebug.h"

//#define stat _stat64i32

int IsExt(const char* s,const char* ext)
{	if(!s)
	{	return 0;
	}
	if(!ext)
	{	return 1;
	}
	const size_t slen = strlen(s);
	const size_t elen = strlen(ext);
	if(elen >= slen)
	{	return 0;
	}
	const char* end = s + slen - elen;
	if(strcmp(end,ext))
	{	return 0;
	}
	return 1;
}

/* loads filepaths to plugins for later loading */
int datafiles_read_directories (char *path,datafile_loader_t loader_func,int flags,const char* ext)
{	if (!path)
	{  return -1;
	}
	printf("Load files %s\n",path);
	/* Open directory */
	DIR* dir = opendir(path);
	if (!dir)
	{	g_message ("error reading datafiles directory \"%s\"", path);
		return -1;
	}
	errno = 0; 
	struct dirent* dir_ent;
	int count = 0;
    while(dir_ent = readdir(dir))
	{	if(errno)// || !dir_ent->d_name)
		{	perror("readdir failed: ");
			break;
		}
//		puts(dir_ent->d_name);
		if(dir_ent->d_type == DT_DIR || !IsExt(dir_ent->d_name,ext))
		{	continue;
		}
		char* filename = g_malloc(strlen(path) + strlen(dir_ent->d_name) + 2);
		sprintf(filename, "%s/%s", path, dir_ent->d_name);
//		puts(filename);
		//filestat_valid = 1;
		count++;
		if(!(*loader_func) (filename))
		{	//filestat_valid = 0;
			g_free(filename);
			count--;
	}	}
    closedir(dir);
	return count;
}

time_t datafile_atime(const char* filename)
{	struct stat filestat;
	int is_fail = stat(filename, &filestat);
	if(is_fail)
	{	return 0;
	}
    return filestat.st_atime;
 }

time_t datafile_mtime (const char* filename)
{	struct stat filestat;
	int is_fail = stat(filename, &filestat);
	if (is_fail)
	{
		return 0;
	}
    return filestat.st_mtime;
}

time_t datafile_ctime (const char* filename)
{	struct stat filestat;
	int is_fail = stat(filename, &filestat);
	if (is_fail)
	{
		return 0;
	}    
	return filestat.st_ctime;
}
