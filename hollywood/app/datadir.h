/* wire/datadir.h 
// Fixes odd linker bug, variable called in libgimp, but in plug-in
// Copyright Nov 10, 2002, Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)
*/

#ifndef DATADIR_H
#define DATADIR_H

#include <unistd.h>
#include <stdbool.h>
#include "../dll_api.h"

#ifdef __cplusplus
extern "C" {
#endif

DLL_API int IsDir(const char* dirname);
DLL_API const char* GetPathDot();
DLL_API const char* GetPathHome();
DLL_API const char* GetPathData();
DLL_API const char* GetPathPo();
DLL_API const char* GetPathExe();
DLL_API const char* GetPathLast();
DLL_API void SetPathLast(const char* last_path,bool isTrim);
DLL_API void SetPathData(const char* arg0);
DLL_API const char* GetPathSeparator();
DLL_API void FlipSlashes(char* s);

void AddSwapFile();
const char* cine_personal_rc_file (const char* rcfile);
DLL_API char* StrDupPath(const char* ext,int is_data);

#define BRUSH_PATH		"/data/brushes"
#define GRADIENT_PATH	"/data/gradients"
#define PATTERN_PATH	"/data/patterns"
#define PALETTE_PATH	"/data/palettes"
#define PLUGIN_PATH		0
#define PO_PATH			"/po"
#define DOT_PATH		".cinepaint"
#define TIPS_FILE		"data/tips.txt"
#define DIR_SEPARATOR	"/"
#define DIR_SEPARATOR_C '/'


//#define PLUGIN_PATH		"/build/Win32/Debug"

#ifdef __cplusplus
}
#endif


#endif

