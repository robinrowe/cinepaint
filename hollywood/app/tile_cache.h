#ifndef __TILE_CACHE_H__
#define __TILE_CACHE_H__


#include "tile.h"

void  tile_cache_init(void);
void  tile_cache_insert   (Tile          *tile);
void  tile_cache_flush    (Tile          *tile);
void  tile_cache_set_size (unsigned long  cache_size);

typedef struct TileCache
{   int initialized;
    GHashTable* tile_hash_table;
    GList* tile_list_head;
    GList* tile_list_tail;
    unsigned long max_tile_size;
    unsigned long cur_cache_size;
    unsigned long max_cache_size;
} TileCache;

#endif /* __TILE_CACHE_H__ */
