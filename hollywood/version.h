/*	hollywood/version.h
	Created 2020/8/5 robin.rowe@cinepaint.org
	Copyright 2020 CinePaint
	License MIT 
 */
                                   
#ifndef version_h
#define version_h

// Note: Remember to update version # in ../version.txt and ../data/tips.txt
#define PROGRAM_VERSION	"1.4.8"
#define MAJOR_VERSION 1
#define MINOR_VERSION 4
#define MICRO_VERSION 8

#include "platform_id.h"

#define PROGRAM_NAME "CinePaint" 
#define NOTES_PROGRAM_NAME "CineNotes"
#define COPYRIGHT "Copyright 2023 CinePaint.org" 
#define LICENSE "License: GPL with some LGPL and MIT"
#define LOGO_WIDTH_MIN 350
#define LOGO_HEIGHT_MIN 150
#define MAX_LABEL_LEN 32
#define PROGRAM_TITLE PROGRAM_NAME " v" PROGRAM_VERSION " " PLATFORM_NAME PLATFORM_BITS
#define NOTES_PROGRAM_TITLE NOTES_PROGRAM_NAME " v" PROGRAM_VERSION " " PLATFORM_NAME PLATFORM_BITS
#define NOTES_PROGRAM_WIDTH 600
#define NOTES_PROGRAM_HEIGHT 400
#endif
