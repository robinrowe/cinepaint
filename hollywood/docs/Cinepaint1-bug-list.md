# CinePaint 1.1 Bug List

## Fixed 2020

1. 8/5-8/5 Dialog: CinePaint 1.0.0". Changed to 1.1 in version.h.
1. 8/5-8/5 Dialog: "Your CinePaint tips file appears to be missing". dirData path set.
1. 8/5-8/6 procedural_db_lookup(extension_script_fu_eval). Deleted.
1. 8/5-8/7 "Searching plug-ins in path:": Fixed
1. 8/7-8/7 On {Preferences}: "file gtkentry.c line 398 assertion 'text != NULL'. Silenced. CMS path is null. Set console_messages = 1. cms_bpc_by_default = FALSE. cms_manage_by_default = FALSE.
1. 8/7-8/7 "Clone image regions" becomes "Clone brush: Ctrl-click to set". NOTE: There are 2 places it says this. Not sure what the other one does. Didn't change that.
1. 8/5-8/7 Dialog: "gdisplay_set_colormanaged cannot colormanage. no disp;ay profile defined Mesage repeated 1 times." To avoid this message switch off automatic colormanagement in File/Preferences/Color Management.
	int cms_bpc_by_default = FALSE;
	int cms_mismatch_action = CMS_MISMATCH_AUTO_CONVERT;
	int cms_manage_by_default = FALSE;
1. 8/7-8/7 "/Help/Bugs & Kudos..." became "/Help/Bug Report..."
1. 8/7-8/7 Added timezones.txt
1. 8/7-8/7 Toolbox refactored and rearranged.

# To Do 2020

1. 8/7- Thread components load.
1. 8/7- DLL_API: Broken.
1. 8/7- {Help}{About} broken.
1. 8/7- Clone brush draws spurious line
1. 8/7- Add "/Help/Registration..."
1. 8/7- Redo Tips messages.
1. 8/5- {Help}{Bug Report} "All non-trivial software contains bugs.\n". Replace with HTTP bug tracker. Redo bugs_dialog_cmd_callback.
	Id# 20201
	Name
	Email
	City
	State
	Country
	Postal Code or ZIP
	Political Party
	Time Zone
	Bug|Feature Request
	Bug Severity: 1-5
	Product Happiness: 1-5
	OS
	Version
1. 8/5- Fix po path. Disabled until gettext and libiconv libs bulit and linked.
1. 8/5- Remove g_assert().
1. 8/7- Bucket fill alpha.
1. 8/7- Rename gradients files as *.grd
1. 8/8- Add status bar to to toolbox

## CinePaint 1.1 Startup Console

### 2020/8/7:

Locale not found in C:/Code/gitlab/Cinepaint/hollywood/po
Locale not found in C:/Code/gitlab/Cinepaint/hollywood/po
Locale not found in
Translation test: About
rc: cine_dir = "C:/Users/rower/AppData/Roaming/.cinepaint"
rc token referenced but not defined: cine_data_dirrc: gfig-path = "${cine_dir}/gfig:${cine_data_dir"
rc token referenced but not defined: cine_data_dirrc: gflare-path = "${cine_dir}/gflares:${cine_data_dir"
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/10x10squareBlurf16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/10x10squaref16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/11circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/11fcirclef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/13circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/13fcirclef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/15circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/15fcirclef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/17circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/17fcirclef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/19circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/19fcirclef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/1circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/20x20squareBlurf16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/20x20squaref16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/3circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/3fcirclef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/5circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/5fcirclef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/5x5squareBlurf16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/5x5squaref16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/7circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/7fcirclef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/9circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/9fcirclef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/callig1f16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/callig2f16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/callig3f16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/callig4f16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/confettif16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/dunesf16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/galaxyf16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/galaxy_bigf16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/galaxy_smallf16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal01.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal02.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal03.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal04.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal05.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal06.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal07.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal08.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal09.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal10.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal11.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal12.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal13.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal14.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal15.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal17.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal18.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal19.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal20.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal21.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal22.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal23.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal24.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal25.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal26.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal27.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal28.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal29.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal30.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal31.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal32.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal33.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal34.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal35.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal36.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal37.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal38.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal39.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal40.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal41.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal42.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal43.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal44.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal45.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal46.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal47.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal48.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal49.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal50.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal51.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal52.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal53.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal54.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal55.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal56.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal57.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal58.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal59.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal60.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal61.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal62.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal63.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal64.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal65.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/pixelf16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/feltpen.gih
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/hsparks.gih
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/SketchBrush-16.gih
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/SketchBrush-32.gih
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/SketchBrush-64.gih
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/vine.gih
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/3dgreen.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/amethyst.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/bark.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/blue.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/bluegrid.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/bluesquares.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/blueweb.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/brick.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/burlap.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/burlwood.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/choc_swirl.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/corkboard.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/cracked.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/Craters.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/crinklepaper.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/electric.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/fibers.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/granite1.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/ground1.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/ice.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/java.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/leather.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/leaves.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/leopard.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/lightning.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/marble1.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/marble2.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/marble3.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/Moonfoot.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/nops.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/paper.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/parque1.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/parque2.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/parque3.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/pastel.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/pine.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/pink_marble.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/pool.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/qube1.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/rain.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/recessed.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/redcube.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/rock.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/sky.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/slate.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/sm_squares.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/starfield.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/stone33.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/terra.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/walnut.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/warning.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/wood1.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/wood2.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/wood3.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/wood4.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/wood5.pat
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Abstract_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Abstract_2
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Abstract_3
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Aneurism
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Blinds
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Blue_Green
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Browns
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Brushed_Aluminium
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Burning_Paper
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Burning_Transparency
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Caribbean_Blues
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/CD
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/CD_Half
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Coffee
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Cold_Steel
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Cold_Steel_2
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Crown_molding
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Danish_flag
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Dark_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Deep_Sea
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Default
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Glow_Angular_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Glow_Radial_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Glow_Radial_2
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Glow_Radial_3
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Glow_Radial_4
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Radial_101
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Radial_102
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Radial_103
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Rays_Radial_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Rays_Radial_2
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Rays_Size_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Sizefac_101
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Four_bars
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/French_flag
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/French_flag_smooth
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Full_saturation_spectrum_CCW
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Full_saturation_spectrum_CW
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/German_flag
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/German_flag_smooth
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Golden
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Greens
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Horizon_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Horizon_2
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Incandescent
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Land_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Land_and_Sea
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Metallic_Something
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Mexican_flag
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Mexican_flag_smooth
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Nauseating_Headache
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Neon_Cyan
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Neon_Green
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Neon_Yellow
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Pastels
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Pastel_Rainbow
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Purples
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Radial_Eyeball_Blue
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Radial_Eyeball_Brown
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Radial_Eyeball_Green
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Radial_Glow_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Radial_Rainbow_Hoop
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Romanian_flag
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Romanian_flag_smooth
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Rounded_edge
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Shadows_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Shadows_2
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Shadows_3
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Skyline
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Skyline_polluted
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Square_Wood_Frame
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Sunrise
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Three_bars_sin
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Tropical_Colors
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Tube_Red
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Wood_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Wood_2
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Yellow_Contrast
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Yellow_Orange
CreatedThread wire #864
Searching plug-ins in path: C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-blur.dll
Loading plug-in: plugin-blur.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-bmp.dll
Loading plug-in: plugin-bmp.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-cineon.dll
Loading plug-in: plugin-cineon.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-compose.dll
Loading plug-in: plugin-compose.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-decompose.dll
Loading plug-in: plugin-decompose.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-edge.dll
Loading plug-in: plugin-edge.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-fits.dll
Loading plug-in: plugin-fits.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-gauss_rle.dll
Loading plug-in: plugin-gauss_rle.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-gbr.dll
Loading plug-in: plugin-gbr.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-gif.dll
Loading plug-in: plugin-gif.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-gifload.dll
Loading plug-in: plugin-gifload.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-hdr.dll
Loading plug-in: plugin-hdr.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-iff.dll
Loading plug-in: plugin-iff.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-jpeg.dll
Loading plug-in: plugin-jpeg.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-mblur.dll
Loading plug-in: plugin-mblur.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-noisify.dll
Loading plug-in: plugin-noisify.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-pic.dll
Loading plug-in: plugin-pic.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-png.dll
Loading plug-in: plugin-png.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-pnm.dll
Loading plug-in: plugin-pnm.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-psd.dll
Loading plug-in: plugin-psd.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-psd_save.dll
Loading plug-in: plugin-psd_save.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-rotate.dll
Loading plug-in: plugin-rotate.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-sgi.dll
Loading plug-in: plugin-sgi.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-sharpen.dll
Loading plug-in: plugin-sharpen.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-snoise.dll
Loading plug-in: plugin-snoise.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-sobel.dll
Loading plug-in: plugin-sobel.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-spread.dll
Loading plug-in: plugin-spread.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-tga.dll
Loading plug-in: plugin-tga.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-unsharp.dll
Loading plug-in: plugin-unsharp.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-winclipboard.dll
Loading plug-in: plugin-winclipboard.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-winsnap.dll
Loading plug-in: plugin-winsnap.dll
plugin count = 31
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
PlugInRun failed!
procedural_db_execute: cine_register_magic_load_handler
procedural_db_execute: PDB_INTERNAL
procedural_db_lookup(cine_xcf_load)
attempt to .cnon-existant load handler "cine_xcf_load"procedural_db_execute: cine_register_save_handler
procedural_db_execute: PDB_INTERNAL
procedural_db_lookup(cine_xcf_save)
attempt to .cnon-existant save handler "cine_xcf_save"CinePaint: Can not open temp directory (nil) to clear temp files
menus_strip_extra_entries -- could not open files
CinePaint: Can not open temp directory (nil) to clear temp files
rename C:/Users/rower/AppData/Roaming/.cinepaint/cinepaintrc C:/Users/rower/AppData/Roaming/.cinepaint/cinepaintrc.old errno #2
Can't rename cinepaintrc to cinepaintrc.old, reason unknown
C:\Code\gitlab\Cinepaint\hollywood\build\Win32\Debug\cinepaint1.exe (process 13308) exited with code 0.
To automatically close the console when debugging stops, enable Tools->Options->Debugging->Automatically close the console when debugging stops.
Press any key to close this window . . .


### 2020/8/7:

Locale not found in C:/Code/gitlab/Cinepaint/hollywood/po
Locale not found in C:/Code/gitlab/Cinepaint/hollywood/po
Locale not found in
Translation test: About
rc: cine_dir = "C:/Users/rower/AppData/Roaming/.cinepaint"
rc token referenced but not defined: cine_data_dirrc: gfig-path = "${cine_dir}/gfig:${cine_data_dir"
rc token referenced but not defined: cine_data_dirrc: gflare-path = "${cine_dir}/gflares:${cine_data_dir"
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/10x10squareBlurf16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/10x10squaref16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/11circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/11fcirclef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/13circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/13fcirclef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/15circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/15fcirclef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/17circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/17fcirclef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/19circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/19fcirclef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/1circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/20x20squareBlurf16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/20x20squaref16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/3circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/3fcirclef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/5circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/5fcirclef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/5x5squareBlurf16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/5x5squaref16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/7circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/7fcirclef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/9circlef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/9fcirclef16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/callig1f16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/callig2f16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/callig3f16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/callig4f16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/confettif16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/dunesf16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/galaxyf16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/galaxy_bigf16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/galaxy_smallf16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal01.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal02.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal03.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal04.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal05.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal06.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal07.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal08.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal09.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal10.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal11.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal12.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal13.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal14.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal15.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal17.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal18.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal19.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal20.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal21.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal22.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal23.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal24.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal25.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal26.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal27.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal28.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal29.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal30.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal31.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal32.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal33.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal34.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal35.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal36.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal37.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal38.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal39.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal40.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal41.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal42.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal43.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal44.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal45.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal46.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal47.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal48.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal49.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal50.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal51.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal52.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal53.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal54.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal55.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal56.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal57.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal58.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal59.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal60.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal61.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal62.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal63.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal64.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/NS1-Charcoal65.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/pixelf16.gbr
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/feltpen.gih
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/hsparks.gih
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/SketchBrush-16.gih
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/SketchBrush-32.gih
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/SketchBrush-64.gih
C:/Code/gitlab/Cinepaint/hollywood/data/brushes/vine.gih
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/3dgreen.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/amethyst.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/bark.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/blue.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/bluegrid.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/bluesquares.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/blueweb.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/brick.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/burlap.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/burlwood.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/choc_swirl.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/corkboard.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/cracked.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/Craters.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/crinklepaper.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/electric.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/fibers.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/granite1.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/ground1.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/ice.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/java.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/leather.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/leaves.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/leopard.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/lightning.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/marble1.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/marble2.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/marble3.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/Moonfoot.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/nops.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/paper.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/parque1.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/parque2.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/parque3.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/pastel.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/pine.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/pink_marble.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/pool.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/qube1.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/rain.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/recessed.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/redcube.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/rock.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/sky.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/slate.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/sm_squares.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/starfield.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/stone33.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/terra.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/walnut.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/warning.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/wood1.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/wood2.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/wood3.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/wood4.pat
C:/Code/gitlab/Cinepaint/hollywood/data/patterns/wood5.pat
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Abstract_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Abstract_2
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Abstract_3
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Aneurism
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Blinds
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Blue_Green
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Browns
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Brushed_Aluminium
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Burning_Paper
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Burning_Transparency
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Caribbean_Blues
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/CD
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/CD_Half
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Coffee
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Cold_Steel
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Cold_Steel_2
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Crown_molding
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Danish_flag
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Dark_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Deep_Sea
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Default
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Glow_Angular_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Glow_Radial_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Glow_Radial_2
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Glow_Radial_3
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Glow_Radial_4
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Radial_101
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Radial_102
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Radial_103
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Rays_Radial_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Rays_Radial_2
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Rays_Size_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Flare_Sizefac_101
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Four_bars
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/French_flag
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/French_flag_smooth
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Full_saturation_spectrum_CCW
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Full_saturation_spectrum_CW
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/German_flag
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/German_flag_smooth
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Golden
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Greens
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Horizon_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Horizon_2
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Incandescent
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Land_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Land_and_Sea
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Metallic_Something
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Mexican_flag
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Mexican_flag_smooth
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Nauseating_Headache
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Neon_Cyan
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Neon_Green
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Neon_Yellow
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Pastels
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Pastel_Rainbow
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Purples
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Radial_Eyeball_Blue
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Radial_Eyeball_Brown
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Radial_Eyeball_Green
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Radial_Glow_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Radial_Rainbow_Hoop
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Romanian_flag
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Romanian_flag_smooth
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Rounded_edge
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Shadows_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Shadows_2
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Shadows_3
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Skyline
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Skyline_polluted
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Square_Wood_Frame
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Sunrise
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Three_bars_sin
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Tropical_Colors
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Tube_Red
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Wood_1
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Wood_2
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Yellow_Contrast
C:/Code/gitlab/Cinepaint/hollywood/data/gradients/Yellow_Orange
CreatedThread wire #872
Searching plug-ins in path: C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-blur.dll
Loading plug-in: plugin-blur.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-bmp.dll
Loading plug-in: plugin-bmp.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-cineon.dll
Loading plug-in: plugin-cineon.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-compose.dll
Loading plug-in: plugin-compose.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-decompose.dll
Loading plug-in: plugin-decompose.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-edge.dll
Loading plug-in: plugin-edge.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-fits.dll
Loading plug-in: plugin-fits.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-gauss_rle.dll
Loading plug-in: plugin-gauss_rle.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-gbr.dll
Loading plug-in: plugin-gbr.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-gif.dll
Loading plug-in: plugin-gif.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-gifload.dll
Loading plug-in: plugin-gifload.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-hdr.dll
Loading plug-in: plugin-hdr.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-iff.dll
Loading plug-in: plugin-iff.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-jpeg.dll
Loading plug-in: plugin-jpeg.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-mblur.dll
Loading plug-in: plugin-mblur.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-noisify.dll
Loading plug-in: plugin-noisify.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-pic.dll
Loading plug-in: plugin-pic.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-png.dll
Loading plug-in: plugin-png.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-pnm.dll
Loading plug-in: plugin-pnm.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-psd.dll
Loading plug-in: plugin-psd.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-psd_save.dll
Loading plug-in: plugin-psd_save.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-rotate.dll
Loading plug-in: plugin-rotate.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-sgi.dll
Loading plug-in: plugin-sgi.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-sharpen.dll
Loading plug-in: plugin-sharpen.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-snoise.dll
Loading plug-in: plugin-snoise.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-sobel.dll
Loading plug-in: plugin-sobel.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-spread.dll
Loading plug-in: plugin-spread.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-tga.dll
Loading plug-in: plugin-tga.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-unsharp.dll
Loading plug-in: plugin-unsharp.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-winclipboard.dll
Loading plug-in: plugin-winclipboard.dll
C:/Code/gitlab/Cinepaint/hollywood/build/Win32/Debug/plugin-winsnap.dll
Loading plug-in: plugin-winsnap.dll
plugin count = 31
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
ERROR: plugin_main=<null>
PlugInRun failed!
procedural_db_execute: cine_register_magic_load_handler
procedural_db_execute: PDB_INTERNAL
procedural_db_lookup(cine_xcf_load)
attempt to .cnon-existant load handler "cine_xcf_load"procedural_db_execute: cine_register_save_handler
procedural_db_execute: PDB_INTERNAL
procedural_db_lookup(cine_xcf_save)
attempt to .cnon-existant save handler "cine_xcf_save"BEFORE: (setup_scale gdisp)
gdisp->hsbdata->value = 0
gdisp->hsbdata->upper = 512
gdisp->hsbdata->page_size = 512
gdisp->hsbdata->page_increment = 1
gdisp->hsbdata->step_increment = 1
gdisp->vsbdata->value = 0
gdisp->vsbdata->upper = 512
gdisp->vsbdata->page_size = 512
gdisp->vsbdata->page_increment = 1
gdisp->vsbdata->step_increment = 1
AFTER: (setup_scale gdisp)
gdisp->hsbdata->value = 0
gdisp->hsbdata->upper = 512
gdisp->hsbdata->page_size = 512
gdisp->hsbdata->page_increment = 256
gdisp->hsbdata->step_increment = 1
gdisp->vsbdata->value = 0
gdisp->vsbdata->upper = 512
gdisp->vsbdata->page_size = 512
gdisp->vsbdata->page_increment = 256
gdisp->vsbdata->step_increment = 1
Cannot colormanage this display.
Either image or display profile are undefined.
. To avoid this message switch off automatic colormanagement in File/Preferences/Color Management

### 2020/8/5:

Locale not found in @localedir@
Locale not found in C:/Code/gitlab/Cinepaint/hollywood/po
Locale not found in
Translation test: About
rc: gimp_dir = "C:/Users/rower/AppData/Roaming/.cinepaint"
parse_gimprc_file: C:/Code/gitlab/Cinepaint/hollywood/gimprc
GetDirDataSuffix: share/cinepaint/1.1
GetDirPrefix: .
parse_gimprc_file: C:/Users/rower/AppData/Roaming/.cinepaint/gimprc
GetDirPrefix: .
GetDirStaticPrefix: .
CreatedThread wire #836
Searching plug-ins in path: (null)
parse_gimprc_file: C:/Users/rower/AppData/Roaming/.cinepaint/pluginrc
GetDirPrefix: .
plugin count = 0
procedural_db_execute: gimp_register_magic_load_handler
procedural_db_execute: PDB_INTERNAL
procedural_db_lookup(gimp_xcf_load)
procedural_db_execute: gimp_register_save_handler
procedural_db_execute: PDB_INTERNAL
procedural_db_lookup(gimp_xcf_save)
save_handler: xcf
parse_gimprc_file: C:/Users/rower/AppData/Roaming/.cinepaint/devicerc
GetDirPrefix: .
procedural_db_lookup(extension_script_fu_eval)
NOT FOUND!

### 2020/8/5:

popen(which C:\Code\gitlab\Cinepaint\hollywood\build\Win32\Debug\cinepaint1.exe,r)
which: no C:\Code\gitlab\Cinepaint\hollywood\build\Win32\Debug\cinepaint1.exe in (/c/Program Files (x86)/Common Files/Oracle/Java/javapath:/c/Python27:/c/Python27/Scripts:/c/Program Files (x86)/Intel/iCLS Client:/c/Program Files/Intel/iCLS Client:/c/WINDOWS/system32:/c/WINDOWS:/c/WINDOWS/System32/Wbem:/c/WINDOWS/System32/WindowsPowerShell/v1.0:/c/Program Files (x86)/Intel/Intel(R) Management Engine Components/DAL:/c/Program Files/Intel/Intel(R) Management Engine Components/DAL:/c/Program Files (x86)/Intel/Intel(R) Management Engine Components/IPT:/c/Program Files/Intel/Intel(R) Management Engine Components/IPT:/c/Program Files/dotnet:/c/Program Files/Microsoft SQL Server/130/Tools/Binn:/cmd:/mingw64/bin:/usr/bin:/c/Program Files/PuTTY:/c/WINDOWS/System32/OpenSSH:/c/Code/github/libunistd/cmaker:/c/Program Files/PowerShell/6:/c/Program Files/CMake/bin:/c/Program Files/doxygen/bin:/c/Program Files (x86)/IncrediBuild:/c/Program Files/Docker/Docker/resources/bin:/c/ProgramData/DockerDesktop/version-bin:/c/Users/rower/AppData/Local/Microsoft/WindowsApps:/c/Users/rower/AppData/Local/Programs/Microsoft VS Code/bin:/c/Program Files/CMake/bin:/c/Users/rower/.dotnet/tools)
no executeable path found
GetDirPrefix: .
rc: gimp_dir = "C:/Users/rower/AppData/Roaming/.cinepaint"
parse_gimprc_file: share/gimprc
GetDirDataSuffix: share/cinepaint/1.0.0
GetDirPrefix: .
parse_gimprc_file: C:/Users/rower/AppData/Roaming/.cinepaint/gimprc
GetDirPrefix: .
GetDirStaticPrefix: .
CreatedThread wire #856
Searching plug-ins in path: (null)
parse_gimprc_file: C:/Users/rower/AppData/Roaming/.cinepaint/pluginrc
GetDirPrefix: .
plugin count = 0
procedural_db_execute: gimp_register_magic_load_handler
procedural_db_execute: PDB_INTERNAL
procedural_db_lookup(gimp_xcf_load)
procedural_db_execute: gimp_register_save_handler
procedural_db_execute: PDB_INTERNAL
procedural_db_lookup(gimp_xcf_save)
save_handler: xcf
parse_gimprc_file: C:/Users/rower/AppData/Roaming/.cinepaint/devicerc
GetDirPrefix: .
procedural_db_lookup(extension_script_fu_eval)
NOT FOUND!
BEFORE: (setup_scale gdisp)
gdisp->hsbdata->value = 0
gdisp->hsbdata->upper = 512
gdisp->hsbdata->page_size = 512
gdisp->hsbdata->page_increment = 1
gdisp->hsbdata->step_increment = 1
gdisp->vsbdata->value = 0
gdisp->vsbdata->upper = 512
gdisp->vsbdata->page_size = 512
gdisp->vsbdata->page_increment = 1
gdisp->vsbdata->step_increment = 1
AFTER: (setup_scale gdisp)
gdisp->hsbdata->value = 0
gdisp->hsbdata->upper = 512
gdisp->hsbdata->page_size = 512
gdisp->hsbdata->page_increment = 256
gdisp->hsbdata->step_increment = 1
gdisp->vsbdata->value = 0
gdisp->vsbdata->upper = 512
gdisp->vsbdata->page_size = 512
gdisp->vsbdata->page_increment = 256
gdisp->vsbdata->step_increment = 1
Cannot colormanage this display.
Either image or display profile are undefined.
. To avoid this message switch off automatic colormanagement in File/Preferences/Color Management
