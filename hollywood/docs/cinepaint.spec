Summary: CinePaint is a tool for manipulating images.
Name: cinepaint
Version: 0.18-0
Release: 1
License: GPL
Group: Applications/Multimedia
Source0: http://prdownloads.sourceforge.net/cinepaint/cinepaint-0.18-0.tar.gz
URL: http://cinepaint.sourceforge.net
BuildRoot: %{_tmppath}/%{name}-root
#Requires: /sbin/ldconfig FIXME: Find dependencies
Prefix:    %{_prefix}

%description
CinePaint is an image editing tool primarily used for painting
 and retouching of motion picture frames.

%package devel
Group: Development/Multimedia
Summary: CinePaint is a tool for manipulating images.
%description devel
Developer files for cinepaint.

%prep
%setup -q

%build
%configure
make %{_smp_mflags}

%install
rm -rf %{buildroot}
make install \
        DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%post

%postun

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog README
%{_bindir}/*
%{_libdir}/*.so.*
%{_datadir}/%{name}
%{_prefix}/share/aclocal/cinepaint.m4
%{_mandir}/man1/*
%{_libdir}/cinepaint/0.18-0/*

%files devel
%defattr(-, root, root)
#%doc HACKING
%{_libdir}/*.a
%{_libdir}/*.la
%{_libdir}/*.so
%{prefix}/include/cinepaint-0.18-0/lib/*.h
#%{_mandir}/man?/*

%changelog

* Fri Apr 11 2003 Robin Rowe
- filmgimp to cinepaint rename
- rename gimp files

* Tue Dec 03 2002 Sam Richards
- Changed include directory path.

* Wed Nov 06 2002 Rene Rask
- release 0.6-2
- Rpms can now be built from source by running "make rpm"
- Updated splash and logo images.

* Mon Nov 04 2002 Rene Rask
- release 0.6.

* Wed Oct 30 2002 Rene Rask
- Initial RPM release.
