/* rsr 6/30/03 magick.h */

#ifndef MAGICK_H
#define MAGICK_H

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>
#include <sys/types.h>

#pragma warning( disable : 4018 )

#define MaxTextExtent  2053
#define False  0
#define True	1

#include "info.h"

int ReadBlobByte(Image *image);
void *AcquireMemory(const size_t size);
void ReacquireMemory(void **memory,const size_t size);
int LocaleCompare(const char *p,const char *q);
unsigned int SetImageAttribute(Image *image,const char *key,const char *value);
void LiberateMemory(void **memory);

typedef struct _LongPixelPacket
{
  unsigned long
    red,
    green,
    blue,
    opacity;
} LongPixelPacket;

typedef unsigned int
  (*MonitorHandler)(const char *,const ExtendedSignedIntegralType,
    const ExtendedUnsignedIntegralType,ExceptionInfo *);

MonitorHandler SetMonitorHandler(MonitorHandler);

/*typedef unsigned int Quantum;*/
typedef Quantum IndexPacket;
#define MagickSignature  0xabacadabUL
Image *AllocateImage(const ImageInfo *image_info);
/*
typedef enum
{
  UndefinedBlobMode,
  ReadBlobMode,
  ReadBinaryBlobMode,
  WriteBlobMode,
  WriteBinaryBlobMode,
  IOBinaryBlobMode
} BlobMode;
*/
//unsigned int OpenBlob(const ImageInfo *image_info,Image *image,const BlobMode mode,ExceptionInfo *exception);

#define ThrowReaderException(code,reason,image)

size_t ReadBlob(Image *image,const size_t length,void *data);
unsigned int AllocateImageColormap(Image *image,const unsigned long colors);
PixelPacket *SetImagePixels(Image *,const long,const long,const unsigned long,const unsigned long);
IndexPacket *GetIndexes(const Image *);
unsigned int ThrowException(ExceptionInfo *,const ExceptionType,const char *,const char *);
unsigned int SyncImagePixels(Image *);

#define QuantumTick(i,span) ((((i) & 0xff) == 0) || (i == ((span)-1)))

unsigned int MagickMonitor(const char *,const ExtendedSignedIntegralType, const ExtendedUnsignedIntegralType,ExceptionInfo *);
extern const char *LoadImageTag;
int EOFBlob(const Image *);
void AllocateNextImage(const ImageInfo *,Image *);
void DestroyImageList(Image *);
Image *SyncNextImageInList(const Image *);
extern const char *LoadImagesTag;
ExtendedSignedIntegralType TellBlob(const Image *image);
ExtendedSignedIntegralType GetBlobSize(const Image *image);
void CloseBlob(Image *);

#define ModuleExport

MagickInfo *SetMagickInfo(const char *);
const char *AcquireString(const char *);
MagickInfo *RegisterMagickInfo(MagickInfo *);
unsigned int UnregisterMagickInfo(const char *);

#define ThrowWriterException(code,reason,image)

unsigned int SetImageColorspace(Image *,const ColorspaceType);
unsigned int IsGrayImage(const Image *,ExceptionInfo *);
unsigned int IsMonochromeImage(const Image *,ExceptionInfo *);
void FormatString(char *,const char *,...);
size_t WriteBlobString(Image *,const char *);
const ImageAttribute *GetImageAttribute(const Image *,const char *);
size_t WriteBlobByte(Image *,const unsigned long);
void SetImageType(Image *,const ImageType);

#define PixelIntensityToQuantum(pixel) ((Quantum) \
  (0.299*(pixel)->red+0.587*(pixel)->green+0.114*(pixel)->blue+0.5))

const PixelPacket *AcquireImagePixels(const Image *,const long,const long,const unsigned long,
    const unsigned long,ExceptionInfo *);

extern const char *SaveImageTag;

size_t WriteBlobMSBShort(Image *,const unsigned long);
size_t WriteBlob(Image *,const size_t,const void *);

extern const char *SaveImagesTag;

unsigned long GetImageListLength(const Image *);

#define MagickExport

#endif
