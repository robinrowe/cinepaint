#!/usr/bin/perl -w
=pod
create_plugins.pl: create img_img plug-ins project files
Copyright 4/5/2006 Robin.Rowe@MovieEditor.com 
revised 4/5/2006
License BSD
=cut

use strict;
package create_plugins;

my @plugins = qw(img_ppm img_exr img_sgi img_dpx);
my $template = q(img_ppm);

sub main()
{	print("creating plug-in files\n");
	CreateVcppProjects(q(vcpp7));
#	CreateVcppProjects(q(vcpp8));
}

sub CreateVcppProjects($)
{	my $subdir = $_[0];
	print $subdir;
	foreach (@plugins)
	{	my $plugin = $_;
		if($plugin eq $template)
		{	print(" skipping $subdir/$plugin (template)\n");
			next;
		}
		if(-e "$subdir/$plugin")
		{	print(" skipping $subdir/$plugin (existing)\n");
			next;
		}
		print(" creating $subdir/$plugin\n");
		mkdir("$subdir/$plugin",755) or die($!);
		CloneProjectFile($subdir,$plugin,'dsp');
		CloneProjectFile($subdir,$plugin,'vcproj');
}	}
		
sub CloneProjectFile($$$)
{	my $subdir=$_[0];
	my $plugin=$_[1];
	my $ext=$_[2];
	my $input="$subdir/$template/$template.$ext";
	my $output="$subdir/$plugin/$plugin.$ext";
#	print("$input - $output\n");
	open(my $INPUT,'<',$input) or die("$! $input");
	open(my $OUTPUT,'>',$output) or die("$! $output");
	{	foreach my $line (<$INPUT>)
		{	chomp($line);
			$line =~ s/$template/$plugin/g;
#			my $good_proj_CAPS = $good_proj;
#			$good_proj_CAPS =~ tr/[a-z]/[A-Z]/;
#			my $plugin_CAPS = $plugin;
#			$plugin_CAPS =~ tr/[a-z]/[A-Z]/;
#			$line =~ tr/$good_proj_CAPS/$plugin_CAPS/; 
			print($OUTPUT <<ZZZ) or die($!);
$line
ZZZ
	}	}
	close($INPUT) or die($!);
	close($OUTPUT) or die($!);			
}
	
main();
