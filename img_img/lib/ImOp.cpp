
	
	
	Substring arg(argv[i]);

		if(arg.StartsWith("plugin="))
		{	plugin_name_input="img_";
			plugin_name_input+=input_ext;
			PRINT(plugin_name_input);
			if(!plugin.Open(plugin_name)
			{	CERR<<"Can't open plugin: "<<plugin_name<<endl;
				return 4;
			}
		}
		if(arg.StartsWith("in="))
		{	input_filename=arg.data+3;
			input_ext=arg.RevSearch('.');
			if(!input_ext)
			{	CERR<<"Can't find file type from extension: "<<input_file<<endl;
				return 2;
			}
			PRINT(input_filename);
			PRINT(input_ext);
			if(!plugin.IsOpen())
			{	plugin_name_input="img_";
				plugin_name_input+=input_ext;
				PRINT(plugin_name_input);
				if(!plugin.Open(plugin_name)
				{	CERR<<"Can't open plugin: "<<plugin_name<<endl;
					return 4;
				}
			}
			MicroTimer micro_timer;
			micro_timer.Start();
			bool ok = img_plugin.Read(input_filename);
			micro_timer.Stop();
			if(!ok)
			{	CERR<<"couldn't read input: "<<input_filename<<endl;
				return 5;
			}
			if(is_verbose)
			{	cout << micro_timer.GetSeconds() << '.' 
				<< micro_timer.GetMicroseconds() << SEP
				<< GetFileSize(input_filename) << SEP;
			}
			continue;
		}
            
		

	VERBOSE << "opening input library: " << cmd_line.input_libname << " for file: "
            << cmd_line.input_filename << "\n"
            << "opening output library: " << cmd_line.output_libname << " for file: "
            << cmd_line.output_filename << " with options: " 
//            << ((cmd_line.output_options.empty()) ? " NONE" : cmd_line.output_options) 
			<< endl;

	VERBOSE <<  << cmd_line.input_libname << endl;

	ImgPlugin img_plugin;
	ImgPluginData plugin_data;
	memset(&plugin_data,0,sizeof(plugin_data));// Important to zero first!
	plugin_data.libname = cmd_line.input_libname.c_str();
	plugin_data.filename = cmd_line.input_filename.c_str();

	VERBOSE << "attempting to read " << cmd_line.input_filename << endl;



	if(cmd_line.is_help)
	{	VERBOSE << "attempting to help " << cmd_line.input_filename << endl;
		if(!img_plugin.GetHelp(plugin_data))
		{	VERROR << "couldn't get help module\n";
			return 2;
		}
		cout << endl;
		PrintHelp(img_plugin.GetImgData(),plugin_data);
	}

	if (cmd_line.output_libname.empty())
	{	cout << endl;
		return 0;
	}

	plugin_data.libname = cmd_line.output_libname.c_str();
	plugin_data.filename = cmd_line.output_filename.c_str();
	plugin_data.write_options = cmd_line.output_options.c_str();

	VERBOSE << "attempting to write " << cmd_line.output_filename << endl;

	micro_timer.Start();
	ok = img_plugin.Write(plugin_data);
	micro_timer.Stop();

	if(!ok)
	{	VERROR << "couldn't write output file " << cmd_line.output_filename << endl;
		return 5;
	}
	cout << micro_timer.GetSeconds() << "." 
		<< micro_timer.GetMicroseconds() << SEP 
		<< GetFileSize(plugin_data.filename) << SEP
		<< endl;
	return 0;
}

#define PRINT2(n,d) n << ((d)? (d):"<null>")

void PrintHelp(const ImgImage& img_data,const ImgPluginData& plugin_data)
{	if(!plugin_data.help_info)
	{	cout<<"no help found!"<<endl;
		return;
	}
	cout<<plugin_data.version_info<<endl
	<< plugin_data.help_info <<endl
	<< PRINT2("filename: ",plugin_data.filename) <<endl
	<< PRINT2("libname: ",plugin_data.libname) <<endl
//	<< "width: " << img_data.width <<endl
//	<< "height: " <<img_data.height <<endl
//	<< "byte depth: " << img_data.byte_depth <<endl
	;
}
