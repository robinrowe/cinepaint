// ImgChannel.c 10/13/04
// Copyright 2004 Robin.Rowe@MovieEditor.com
// License OSI MIT with additional studio disclaimers (see below)

#include "../include/ImgChannel.h"

#undef CHANNEL_TYPE
#define CHANNEL_TYPE(x) extern ChannelType img_type_##x=#x
#undef CHANNEL_COLOR
#define CHANNEL_COLOR(x) extern ChannelColor img_color_##x=#x

#include "../include/ImgChannelNames.h"

ImgChannel* NewChannel(ImgChannel* prev)
{	ImgChannel* channel=new ImgChannel;
	if(!channel)
	{	return 0;
	}
	bzero(channel);
	NodeAppend(CAST(Node*) prev,CAST(Node*) channel);
	return channel;
}

void DeleteChannel(ImgChannel* channel)
{	DeleteBlob(channel);
	NodeRemove(CAST(Node*)channel);
#ifdef _DEBUG	
	bzero(channel);
#endif
	delete channel;
}


bool NewBlob(ImgChannel* channel)
{	const unsigned size=GetByteCount(channel);
	if(!size)
	{	return false;
	}
	channel->blob.data=new BYTE[size];
	if(!channel->blob.data)
	{	return false;
	}
	channel->blob.length=size;
	return true;
}


void DeleteBlob(ImgChannel* channel)
{	if(!channel || !channel->blob.data)
	{	return;
	}
	channel->blob.length=0;
// Crash here unless everything linked using VC++
// {Settings}{C/C++}{Runtime library}{Multithreaded DLL}
// Any other linkage creates multiple heaps across modules
// that will crash if you allocate in one module and free
// in another! The linker switch you should see is /MD or
// /MDd. If you don't have this "doctor" switch your heap
// will get sick! -- Robin Rowe 10/13/04
	delete[] channel->blob.data;
	channel->blob.data=0;
}


unsigned GetSubChannelCount(ImgChannel* channel)
{	if(!channel || !channel->channel_color)
	{	return 1;
	}
	return strlen(channel->channel_color);
}


unsigned GetByteCount(ImgChannel* channel)
{	if(!channel)
	{	return 0;
	}
	const unsigned count = channel->width
							*channel->height
							*GetSubChannelCount(channel)
							*channel->bit_depth/8;
	return count;
}


unsigned GetBytesPerLineCount(ImgChannel* channel)
{	if(!channel)
	{	return 0;
	}
	const unsigned count = channel->width
							*GetSubChannelCount(channel)
							*channel->bit_depth/8;
	return count;
}


unsigned GetPixelsCount(ImgChannel* channel)
{	if(!channel)
	{	return 0;
	}
	const unsigned count = channel->width*channel->height;
	return count;
}


unsigned GetComponentCount(ImgChannel* channel)
{	if(!channel)
	{	return 0;
	}
	const unsigned count = channel->width
							*channel->height
							*GetSubChannelCount(channel);
	return count;
}


unsigned GetBitDepth(ImgChannel* channel)
{	if(!channel)
	{	return 0;
	}
	return channel->bit_depth;
}


unsigned GetByteDepth(ImgChannel* channel)
{	return GetBitDepth(channel)/8;
}


unsigned GetWidth(ImgChannel* channel)
{	if(!channel)
	{	return 0;
	}
	return channel->width;
}


unsigned GetHeight(ImgChannel* channel)
{	if(!channel)
	{	return 0;
	}
	return channel->height;
}


// CopyU8To...

void CopyU8ToU8(const ImgChannel* src,ImgChannel* dst)
{	memcpy(dst->blob.data,src->blob.data,dst->width*dst->height*GetSubChannelCount(dst)*dst->bit_depth/8);
}

void CopyU8ToU16(const ImgChannel* src,ImgChannel* dst)
{	const unsigned components = dst->width*dst->height*GetSubChannelCount(dst);
	const U8* u8 = (U8*) src->blob.data;
	U16* u16 = (U16*) dst->blob.data;
	for(unsigned i=0;i<components;i++)
	{	ScaleChannel(u8++,u16++);
}	}

void CopyU8ToU32(const ImgChannel* src,ImgChannel* dst)
{	const unsigned components = dst->width*dst->height*GetSubChannelCount(dst);
	const U8* u8 = (U8*) src->blob.data;
	U32* u32 = (U32*) dst->blob.data;
	for(unsigned i=0;i<components;i++)
	{	ScaleChannel(u8++,u32++);
}	}

void CopyU8ToF32(const ImgChannel* src,ImgChannel* dst)
{	const unsigned components = dst->width*dst->height*GetSubChannelCount(dst);
	const U8* u8 = (U8*) src->blob.data;
	F32* f32 = (F32*) dst->blob.data;
	for(unsigned i=0;i<components;i++)
	{	ScaleChannel(u8++,f32++);
}	}

// CopyU16To...

void CopyU16ToU8(const ImgChannel* src,ImgChannel* dst)
{	const unsigned components = dst->width*dst->height*GetSubChannelCount(dst);
	const U16* u16 = (U16*) src->blob.data;
	U8* u8 = (U8*) dst->blob.data;
	for(unsigned i=0;i<components;i++)
	{	ScaleChannel(u16++,u8++);
}	}

//void CopyU16ToU16(const ImgChannel* src,ImgChannel* dst);

void CopyU16ToU32(const ImgChannel* src,ImgChannel* dst)
{	const unsigned components = dst->width*dst->height*GetSubChannelCount(dst);
	const U16* u16 = (U16*) src->blob.data;
	U32* u32 = (U32*) dst->blob.data;
	for(unsigned i=0;i<components;i++)
	{	ScaleChannel(u16++,u32++);
}	}

void CopyU16ToF32(const ImgChannel* src,ImgChannel* dst)
{	const unsigned components = dst->width*dst->height*GetSubChannelCount(dst);
	const U16* u16 = (U16*) src->blob.data;
	F32* f32 = (F32*) dst->blob.data;
	for(unsigned i=0;i<components;i++)
	{	ScaleChannel(u16++,f32++);
}	}

// CopyU32To...

void CopyU32ToU8(const ImgChannel* src,ImgChannel* dst)
{	const unsigned components = dst->width*dst->height*GetSubChannelCount(dst);
	const U32* u32 = (U32*) src->blob.data;
	U8* u8 = (U8*) dst->blob.data;
	for(unsigned i=0;i<components;i++)
	{	ScaleChannel(u32++,u8++);
}	}

void CopyU32ToU16(const ImgChannel* src,ImgChannel* dst)
{	const unsigned components = dst->width*dst->height*GetSubChannelCount(dst);
	const U32* u32 = (U32*) src->blob.data;
	U16* u16 = (U16*) dst->blob.data;
	for(unsigned i=0;i<components;i++)
	{	ScaleChannel(u32++,u16++);
}	}

//void CopyU32ToU32(const ImgChannel* src,ImgChannel* dst);

void CopyU32ToF32(const ImgChannel* src,ImgChannel* dst)
{	const unsigned components = dst->width*dst->height*GetSubChannelCount(dst);
	const U32* u32 = (U32*) src->blob.data;
	F32* f32 = (F32*) dst->blob.data;
	for(unsigned i=0;i<components;i++)
	{	ScaleChannel(u32++,f32++);
}	}

// CopyF32To...

void CopyF32ToU8(const ImgChannel* src,ImgChannel* dst)
{	const unsigned components = dst->width*dst->height*GetSubChannelCount(dst);
	const F32* f32 = (F32*) src->blob.data;
	U8* u8 = (U8*) dst->blob.data;
	for(unsigned i=0;i<components;i++)
	{	ScaleChannel(f32++,u8++);
}	}

void CopyF32ToU16(const ImgChannel* src,ImgChannel* dst)
{	const unsigned components = dst->width*dst->height*GetSubChannelCount(dst);
	const F32* f32 = (F32*) src->blob.data;
	U16* u16 = (U16*) dst->blob.data;
	for(unsigned i=0;i<components;i++)
	{	ScaleChannel(f32++,u16++);
}	}

void CopyF32ToU32(const ImgChannel* src,ImgChannel* dst)
{	const unsigned components = dst->width*dst->height*GetSubChannelCount(dst);
	const F32* f32 = (F32*) src->blob.data;
	U32* u32 = (U32*) dst->blob.data;
	for(unsigned i=0;i<components;i++)
	{	ScaleChannel(f32++,u32++);
}	}

//void CopyF32ToF32(const ImgChannel* src,ImgChannel* dst);

void SwapHostToNetByteOrder(ImgChannel* img_channel)
{   U16* data=CAST(U16*) img_channel->blob.data;
    const unsigned image_components=GetComponentCount(img_channel);
    const unsigned short* end_of_data=data+image_components;
    while(data < end_of_data)
    {   *data=htons(*data);
        data++;
}   }

/*****************************************************
The OSI MIT License plus additional studio disclaimers

Copyright (c) 2004 Robin Rowe <Robin.Rowe@movieeditor.com>

Permission is hereby granted, free of charge, to any person 
obtaining a copy of this software and associated documentation 
files (the "Software"), to deal in the Software without 
restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or 
sell copies of the Software, and to permit persons to whom 
the Software is furnished to do so, subject to the following 
conditions:

The above copyright notice and this permission notice shall be 
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
OTHER DEALINGS IN THE SOFTWARE.

***STUDIO DISCLAIMERS

This software was created by Robin Rowe at a motion picture studio. 
As a condition of granting the software back to Robin Rowe for open 
source release, the following clauses are added.

3. THE SOFTWARE IS ASSIGEND "AS-IS" AND ROBIN ROWE MAKES NO 
REPRESENTATIONS, WARRANTIES, GUARANTEES, OR OTHER PROMISES 
WHATSOEVER, WHETHER EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, 
INCLUDING, WITHOUT LIMITATION, ANY WARRANTY REGARDING SAFETY, 
EFFICACY, PERFORMANCE, MERCHANTABILITY, QUALITY, TITLE, 
NON-INFRINGEMENT, FITNESS FOR A PARTICULAR PURPOSE, LACK OF 
VIRUSES, ACCURACY OR COMPLETENESS OF RESPONSES, RESULTS, OR DATA, 
FREEDOM FROM ERRORS, UNITERRUPTED OPERATION, LACK OF NEGLIGENCE, 
LACK OF A WORKMANLIKE EFFORT, QUIET ENJOYMENT, OR QUIET POSSESSION.

4. UNDER NO CIRCUMSTANCES SHALL ROBIN ROWE BE LIABLE TO YOU, OR ANY 
THIRD-PARTY FOR ANY ACTUAL, GENERAL, INDIRECT, SPECIAL, INCIDENTAL, 
CONSEQUENTIAL, PUNITIVE, OR OTHER DAMAGES IN CONNECTION WITH, 
ARISING UNDER, OR RELATED TO THE SOFTWARE (WHETHER UNDER THEORY OF 
CONTRACT, TORT, INDEMNITY, PRODUCT LIABILITY, OR OTHERWISE) INCLUDING, 
WITHOUT LIMITATION, DAMAGES RESULTING FORM DELAY OR FROM LOSS OF 
PROFITS, LOST BUSINESS OPPORTUNITIES OR GOODWILL, LOSS OF CONFIDENTIAL 
OR OTHER INFORMATION, BUSINESS INTERRUPTION, PERSONAL INJURY, LOSS OR 
PRIVACY, FAILURE TO MEET ANY DUTY (INCLUDING, WITHOUT LIMITATION, OF 
GOOD FAITH OR OF REASONABLE CARE), NEGLIGENCE, OR ANY OTHER LOSS 
WHATSOEVER, WHETHER OR NOT ROBIN ROWE HAS BEEN ADVISED OR IS AWARE OF 
THE POSSIBILITY OF SUCH DAMAGES.

*****************************************************/
