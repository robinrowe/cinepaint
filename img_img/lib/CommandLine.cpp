// CommandLine.cpp 10/23/04
// Copyright 2004 Robin.Rowe@MovieEditor.com
// License OSI MIT with additional studio disclaimers

#include <gel/io/IoFile.h>
#include "../include/CommandLine.h"

#define NEXT() argc--;argv++
	
bool CommandLine::parse(int argc, char * argv[])
{	NEXT();
	if(!argc)
	{	Usage();
		return false;
	}
	Substring arg=*argv;
	if(arg == "-v")
	{	cout << "VERBOSE MODE\n";
		is_verbose = true;
		NEXT();
		if(!argc)
		
	i++;
	if(i < argc)
	{   arg = argv[i];
	}   }
	if (i >= argc)
	{   Usage();
	return false;
	}
	if (arg == "-h")
	{   is_help = true;
	i++;
	if (i < argc)
	{   arg = argv[i];
	}   }

	if (!parse_filename_and_get_lib(arg, input_filename, input_libname))
	{   cerr << "ERROR: couldn't find library for infile: " << arg << endl;
	return false;
	}

	if (++i >= argc)
	{   return true;
	}
	arg = argv[i];
	if (!parse_filename_and_get_lib(arg, output_filename, output_libname))
	{   cerr << "ERROR: couldn't find library for outfile: '" << arg << endl;
	return false;
	}
	if (++i >= argc)
	{   return true;
	}
	output_options = argv[i];
	return true;
}

bool CommandLine::parse_filename_and_get_lib(string &param, string& file_name, string& lib_name)
{	const char* lookup[]=
	{ 	"exr","img_openexr.dll" ,
	  	"jp2","img_jasper.dll" ,
	  	"ppm","img_ppm.dll"  
	};
	enum {size=6};
	const char* ext=strrchr(param,'.');
	if(!ext)
	{	return false;
	}
	ext++;
	for(unsigned i=0;i<size;i+=2)
	{	if(!strcmp(lookup[i],ext))
		{	lib_name=lookup[i+1];
			file_name=param;
			return true;
	}	}

    {
        cout << "Error - filename w/o extension?\n\n\n";
        Usage();
        return false;
    }
}


/*****************************************************
The OSI MIT License plus additional studio disclaimers

Copyright (c) 2004 Robin Rowe <Robin.Rowe@movieeditor.com>

Permission is hereby granted, free of charge, to any person 
obtaining a copy of this software and associated documentation 
files (the "Software"), to deal in the Software without 
restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or 
sell copies of the Software, and to permit persons to whom 
the Software is furnished to do so, subject to the following 
conditions:

The above copyright notice and this permission notice shall be 
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
OTHER DEALINGS IN THE SOFTWARE.

***STUDIO DISCLAIMERS

This software was created by Robin Rowe at a motion picture studio. 
As a condition of granting the software back to Robin Rowe for open 
source release, the following clauses are added.

3. THE SOFTWARE IS ASSIGEND "AS-IS" AND ROBIN ROWE MAKES NO 
REPRESENTATIONS, WARRANTIES, GUARANTEES, OR OTHER PROMISES 
WHATSOEVER, WHETHER EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, 
INCLUDING, WITHOUT LIMITATION, ANY WARRANTY REGARDING SAFETY, 
EFFICACY, PERFORMANCE, MERCHANTABILITY, QUALITY, TITLE, 
NON-INFRINGEMENT, FITNESS FOR A PARTICULAR PURPOSE, LACK OF 
VIRUSES, ACCURACY OR COMPLETENESS OF RESPONSES, RESULTS, OR DATA, 
FREEDOM FROM ERRORS, UNITERRUPTED OPERATION, LACK OF NEGLIGENCE, 
LACK OF A WORKMANLIKE EFFORT, QUIET ENJOYMENT, OR QUIET POSSESSION.

4. UNDER NO CIRCUMSTANCES SHALL ROBIN ROWE BE LIABLE TO YOU, OR ANY 
THIRD-PARTY FOR ANY ACTUAL, GENERAL, INDIRECT, SPECIAL, INCIDENTAL, 
CONSEQUENTIAL, PUNITIVE, OR OTHER DAMAGES IN CONNECTION WITH, 
ARISING UNDER, OR RELATED TO THE SOFTWARE (WHETHER UNDER THEORY OF 
CONTRACT, TORT, INDEMNITY, PRODUCT LIABILITY, OR OTHERWISE) INCLUDING, 
WITHOUT LIMITATION, DAMAGES RESULTING FORM DELAY OR FROM LOSS OF 
PROFITS, LOST BUSINESS OPPORTUNITIES OR GOODWILL, LOSS OF CONFIDENTIAL 
OR OTHER INFORMATION, BUSINESS INTERRUPTION, PERSONAL INJURY, LOSS OR 
PRIVACY, FAILURE TO MEET ANY DUTY (INCLUDING, WITHOUT LIMITATION, OF 
GOOD FAITH OR OF REASONABLE CARE), NEGLIGENCE, OR ANY OTHER LOSS 
WHATSOEVER, WHETHER OR NOT ROBIN ROWE HAS BEEN ADVISED OR IS AWARE OF 
THE POSSIBILITY OF SUCH DAMAGES.

*****************************************************/



