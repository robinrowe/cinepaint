// ImgHolder.cpp 6/27/06
// Copyright 2006 Robin.Rowe@MovieEditor.com
// License OSI MIT 

#include "../include/ImgHolder.h"

ImgHolder::~ImgHolder()
{	if(!img_data)
	{	return;
	}
	DL_list_iterator<ImgChannel> image_it(&img_data->channel_list);
	DL_node* node=image_it.NextNode();
	while(node)
	{	DL_remove(&img_data->channel_list,node);
		ImgChannel* img_channel=CAST(ImgChannel*)node;
		DeleteRaster(img_channel);
		delete img_channel;
		node=image_it.NextNode();
}	}

