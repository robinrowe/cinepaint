// main.cpp 2006.6.29
// Copyright 2006 Robin.Rowe@MovieEditor.com
// License OSI MIT
// Allocates ImgProjectMgr and uses ImgOp::Do() to handle command-line args.

#include <gel/io/iofile.h>
#include <gel/io/MicroTimer.h>
#include <gel/data/Substring.h>
#include <gel/data/Cstring.h>
#include "../include/ImgOp.h"
#include "../include/ImgWorld.h"

#define PROGRAM_NAME "img_img"
#define COPYRIGHT "Copyright (c) 2004 Robin.Rowe@MovieEditor.com"
#define LICENSE "License: OSI MIT with additional studio disclaimers"
#define VERSION "Version 0.1"
#define BUILD "Build: " __DATE__ " " __TIME__ 
#define CERR cout << "ERROR: "
#define PRINT(x) if(is_verbose) cout<<#x<<x

const char* Usage()
{	return	"Usage: " PROGRAM_NAME " in=filename [opt=...] [out=filename]\n"
			PROGRAM_NAME " converts between image formats\n"
			"Order of opts is significant:\n"
			"-v is verbose mode\n"
			"-h [plugin] is help mode\n"
			"plugin=<name> explicitly loads a plug-in\n"
			"Examples: " PROGRAM_NAME " -v in=test16.ppm\n"
			"          " PROGRAM_NAME " plugin=ppm -h\n"
			"          " PROGRAM_NAME " -v in=test16.ppm bits=8 out=out8.ppm\n"
			;
}

const char* Info()
{	return	VERSION " (" BUILD ")\n"
			COPYRIGHT "\n"
			LICENSE "\n"
			;
}

int main(int argc, char * argv[])
{	ImgWorld world;
	bzero(&world);
	ImgOp img_op(&world);
	if(!img_op)
	{	CERR<<"Can't create img_op (out of memory?)\n";
		return 1;
	}
	if(1>=argc)
	{	cout<<Info();
		return 2;
	}
	for(int i=1;i<argc;i++)
	{	gel::Cstring_t<char> arg(argv[i]);
		if(arg=="-v")
		{	cout<< "*** " PROGRAM_NAME " ***\n"
			BUILD "\n"
			COPYRIGHT "\n"
			LICENSE "\n";
			img_op.IsVerbose(true);
			continue;
		}
		if(arg=="-h")
		{	if(img_op.IsOpenPlugin())
			{	cout<<img_op.GetPluginHelp();
			}
			else
			{	cout<<Info()<<Usage();
			}
			continue;
		}
		if(!img_op.Do(arg))
		{	return 3;
	}	}
	if(img_op.IsVerbose())
	{	cout<<"done!\n";
	}
	return 0;
}
