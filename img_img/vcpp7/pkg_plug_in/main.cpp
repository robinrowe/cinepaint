// main.cpp
// Creates VC++ dsp files for plug-ins, run any time new plug-ins added
// Created Dec 25, 2002, by Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)

#include <gel/io/IoFileBuffer.h>
#include <stdlib.h>
#include "FileParser.h"
#include "DirList.h"
#include "helpers.h"

#define PLUGIN_DIR "plug-in/"
#define PROJ_DIR "vcpp7/"
#define KNOWN_GOOD_PLUG_IN "img_ppm/img_ppm"
#define EXT ".vcproj"

int main(int argc,const char* argv[])
{	cout<<"*** pkg_plug_in creating any missing VC++ packages ***"<<endl
		<<"    run in 'plug-ins' directory"<<endl;
	pwd(); // should be img_img directory
	FileParser parser;
	if(!parser.Open(PROJ_DIR KNOWN_GOOD_PLUG_IN EXT))
	{	cout<<"Can't find plug-ins!"<<endl;
		return 1;
	}
	DirList dir_list_plug_ins;
	const char* plug_in_name;
	char temp_filename[_MAX_FNAME];
	temp_filename[0]=char(0);
	if(!dir_list_plug_ins.Open(PLUGIN_DIR "*"))
	{	cout<<"couldn't find any plug-ins!"<<endl;
		return 1;	
	}
	char dirname[_MAX_FNAME];
	dirname[0]=char(0);
	char legal_proj_name[_MAX_FNAME];
	legal_proj_name[0]=char(0);
	for(;;)
	{	if(!dir_list_plug_ins.Next())
		{	break;
		}
		plug_in_name=dir_list_plug_ins.GetFileName();
		cout<<plug_in_name<<": ";
		if( !dir_list_plug_ins.IsSubDir()
		 || !strcmp(plug_in_name,"..")
		 || !strcmp(plug_in_name,KNOWN_GOOD_PLUG_IN) )
		{	cout<<"ignoring"<<endl;
			continue;
		}
		MakeFilename(temp_filename,plug_in_name,plug_in_name,EXT);
		if(IsFile(temp_filename))
		{	cout<<"skipping"<<endl;
			continue;
		}
		MakeFilename(dirname,PROJ_DIR,plug_in_name,0);
		if(!CreateDir(dirname))
		{	cout<<"can't create plug-in directory!"<<endl;
			return 1;
		}
		strcpy(legal_proj_name,plug_in_name);
		ReplaceChar(legal_proj_name,'-','_');
		plug_in_name=legal_proj_name;
		MakeFilename(temp_filename,PROJ_DIR,plug_in_name,EXT);
		if(!parser.Write(temp_filename,KNOWN_GOOD_PLUG_IN,plug_in_name))
		{	cout<<"can't write project file!"<<endl;
			return 1;
		}
		cout<<"created"<<endl;
	}
	cout<<"*** done ***"<<endl;
	return 0;
}

