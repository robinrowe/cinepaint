// helpers.cpp
// Utility helper functions
// Created Dec 25, 2002, by Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)

#include "helpers.h"

const char* FindNextMatch(const char* string,const char* substring,const char* caps_substring)
{	if(!string || !substring || !caps_substring)
	{	return 0;
	}
	const char* sub=strstr(string,substring);
	const char* csub=strstr(string,caps_substring);
	if(!sub)
	{	return csub;
	}
	if(!csub)
	{	return sub;
	}
	return (sub<csub)? sub:csub;
}

void CopyToUpper(char* dest,const char* src)
{	if(!dest || !src)
	{	return;
	}
	while(*src)
	{	*dest=toupper(*src);
		dest++;
		src++;
	}
	*dest=char(0);
}

void MakeFilename(char* filename,const char* dir,const char* name,const char* ext)
{	filename[0]=char(0);
	if(dir)
	{	strcpy(filename,dir);
		strcat(filename,"/");
	}
	if(!name)
	{	return;
	}
	strcat(filename,name);
	if(ext)
	{	strcat(filename,ext);
	}
	return;
}

unsigned ReplaceChar(char* string,char before,char after)
{	if(!string)
	{	return 0;
	}
	unsigned i=0;
	char* ptr=strchr(string,before);
	while(ptr)
	{	i++;
		*ptr=after;
		ptr=strchr(ptr+1,before);
	}
	return i;
}