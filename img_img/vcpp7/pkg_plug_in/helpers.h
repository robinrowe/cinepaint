// helpers.h
// Utility helper functions
// Created Dec 25, 2002, by Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)

#ifndef HELPERS_H
#define HELPERS_H

#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <direct.h>
#include <errno.h>
//#include <iostream.h>
#include <gel/io/IoFile.h>

const char* FindNextMatch(const char* string,const char* substring,const char* caps_substring);
void CopyToUpper(char* dest,const char* src);
void MakeFilename(char* filename,const char* dir,const char* name,const char* ext);
unsigned ReplaceChar(char* string,char before,char after);

inline
unsigned long GetFileSize(const char* filename)
{	struct _stati64 s;
	if(0!=_stati64(filename,&s))
	{	return 0;
	}
	return (unsigned long)s.st_size;
}

inline
bool IsFile(const char* filename)
{	struct stat s;
	return stat(filename,&s)==0;
}

inline
bool IsExtFile(const char* dir,const char* filename,const char* ext)
{	char temp[_MAX_FNAME];
	MakeFilename(temp,dir,filename,ext);
	return IsFile(temp);
}

inline
bool CreateDir(const char* dirname)
{	const int error=mkdir(dirname);
	if(!error)
	{	return true;
	}
	if(EEXIST==errno)
	{	return true;
	}
	return false;
}

inline
void pwd()
{	char dir[_MAX_PATH];
	if( _getcwd(dir,_MAX_PATH)==NULL)
	{	perror( "_getcwd error" );
		return;
	}
	cout<<dir<<endl;
}

#endif
