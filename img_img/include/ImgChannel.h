// ImgChannel.h 10/13/04
// Copyright 2004 Robin.Rowe@MovieEditor.com
// License OSI MIT with additional studio disclaimers (see below)

#ifndef IMG_CHANNEL_H
#define IMG_CHANNEL_H

#include <gel/data/dl_list.h>
#include <gel/data/ByteData.h>
#include "ImgChannelTypes.h"

typedef struct ImgChannel ImgChannel;

struct ImgChannel
{	Node node; // channel's node in channel list
	unsigned layer; // layer #, the first layer is 0
	ChannelType channel_type; // e.g., img_type_u8
	ChannelColor channel_color; // e.g., img_color_rgb
	unsigned x; // position in layer, e.g., 0
	unsigned y; // position in layer, e.g., 0
	unsigned width; // width in pixels, e.g., 720
	unsigned height; // height in pixels, e.g., 480
	unsigned bit_depth; // depth in bits, e.g., 8
	ByteData blob; // raster or other image data
	StringData metadata;// text data about channel
};

// *** memory allocation ***

ImgChannel* NewChannel(ImgChannel* tail);
bool NewBlob(ImgChannel* channel);
void DeleteBlob(ImgChannel* channel);
void DeleteChannel(ImgChannel* channel);
void DeleteAllChannels(ImgChannel* channel);

// *** channel getters ***

inline
ImgChannel* GetChannel(ImgChannel* head,unsigned n)
{	return CAST(ImgChannel*) GetNode(CAST(Node*)head,n);
}

inline
ImgChannel* GetLastChannel(ImgChannel* head)
{	return CAST(ImgChannel*) GetLastNode(CAST(Node*)head);
}

// *** info functions ***

unsigned GetSubChannelCount(ImgChannel* channel);
unsigned GetByteCount(ImgChannel* channel);
unsigned GetBytesPerLineCount(ImgChannel* channel);
unsigned GetPixelsCount(ImgChannel* channel);
unsigned GetComponentCount(ImgChannel* channel);
unsigned GetBitDepth(ImgChannel* channel);
unsigned GetByteDepth(ImgChannel* channel);
unsigned GetWidth(ImgChannel* channel);
unsigned GetHeight(ImgChannel* channel);

// *** bit depth ops ***

void SwapHostToNetByteOrder(ImgChannel* channel);

void CopyU8ToU8(const ImgChannel* src,ImgChannel* dst);
void CopyU8ToU16(const ImgChannel* src,ImgChannel* dst);
void CopyU8ToU32(const ImgChannel* src,ImgChannel* dst);
void CopyU8ToF32(const ImgChannel* src,ImgChannel* dst);

void CopyU16ToU8(const ImgChannel* src,ImgChannel* dst);
//void CopyU16ToU16(const ImgChannel* src,ImgChannel* dst);
void CopyU16ToU32(const ImgChannel* src,ImgChannel* dst);
void CopyU16ToF32(const ImgChannel* src,ImgChannel* dst);

void CopyU32ToU8(const ImgChannel* src,ImgChannel* dst);
void CopyU32ToU16(const ImgChannel* src,ImgChannel* dst);
//void CopyU32ToU32(const ImgChannel* src,ImgChannel* dst);
void CopyU32ToF32(const ImgChannel* src,ImgChannel* dst);

void CopyF32ToU8(const ImgChannel* src,ImgChannel* dst);
void CopyF32ToU16(const ImgChannel* src,ImgChannel* dst);
void CopyF32ToU32(const ImgChannel* src,ImgChannel* dst);
//void CopyF32ToF32(const ImgChannel* src,ImgChannel* dst);

#define CopyU16ToU16 CopyU8ToU8    
#define CopyU32ToU32 CopyU8ToU8
#define CopyF32ToF32 CopyU8ToU8

#endif

/*****************************************************
The OSI MIT License plus additional studio disclaimers

Copyright (c) 2004 Robin Rowe <Robin.Rowe@movieeditor.com>

Permission is hereby granted, free of charge, to any person 
obtaining a copy of this software and associated documentation 
files (the "Software"), to deal in the Software without 
restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or 
sell copies of the Software, and to permit persons to whom 
the Software is furnished to do so, subject to the following 
conditions:

The above copyright notice and this permission notice shall be 
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
OTHER DEALINGS IN THE SOFTWARE.

***STUDIO DISCLAIMERS

This software was created by Robin Rowe at a motion picture studio. 
As a condition of granting the software back to Robin Rowe for open 
source release, the following clauses are added.

3. THE SOFTWARE IS ASSIGEND "AS-IS" AND ROBIN ROWE MAKES NO 
REPRESENTATIONS, WARRANTIES, GUARANTEES, OR OTHER PROMISES 
WHATSOEVER, WHETHER EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, 
INCLUDING, WITHOUT LIMITATION, ANY WARRANTY REGARDING SAFETY, 
EFFICACY, PERFORMANCE, MERCHANTABILITY, QUALITY, TITLE, 
NON-INFRINGEMENT, FITNESS FOR A PARTICULAR PURPOSE, LACK OF 
VIRUSES, ACCURACY OR COMPLETENESS OF RESPONSES, RESULTS, OR DATA, 
FREEDOM FROM ERRORS, UNITERRUPTED OPERATION, LACK OF NEGLIGENCE, 
LACK OF A WORKMANLIKE EFFORT, QUIET ENJOYMENT, OR QUIET POSSESSION.

4. UNDER NO CIRCUMSTANCES SHALL ROBIN ROWE BE LIABLE TO YOU, OR ANY 
THIRD-PARTY FOR ANY ACTUAL, GENERAL, INDIRECT, SPECIAL, INCIDENTAL, 
CONSEQUENTIAL, PUNITIVE, OR OTHER DAMAGES IN CONNECTION WITH, 
ARISING UNDER, OR RELATED TO THE SOFTWARE (WHETHER UNDER THEORY OF 
CONTRACT, TORT, INDEMNITY, PRODUCT LIABILITY, OR OTHERWISE) INCLUDING, 
WITHOUT LIMITATION, DAMAGES RESULTING FORM DELAY OR FROM LOSS OF 
PROFITS, LOST BUSINESS OPPORTUNITIES OR GOODWILL, LOSS OF CONFIDENTIAL 
OR OTHER INFORMATION, BUSINESS INTERRUPTION, PERSONAL INJURY, LOSS OR 
PRIVACY, FAILURE TO MEET ANY DUTY (INCLUDING, WITHOUT LIMITATION, OF 
GOOD FAITH OR OF REASONABLE CARE), NEGLIGENCE, OR ANY OTHER LOSS 
WHATSOEVER, WHETHER OR NOT ROBIN ROWE HAS BEEN ADVISED OR IS AWARE OF 
THE POSSIBILITY OF SUCH DAMAGES.

*****************************************************/



