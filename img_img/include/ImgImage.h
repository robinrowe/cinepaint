// ImgImage.h  2006.12.2
// Copyright 2006 Robin.Rowe@MovieEditor.com
// License OSI MIT 

#ifndef IMG_IMAGE_H
#define IMG_IMAGE_H

#include "ImgChannel.h"

class ImgImage
{	ImgChannel* channel_list; // a linked-list of channels
public:	
	ImgImage(ImgChannel* channel_list=0)
	:	channel_list(channel_list)
	{}
	void ExplicitDestructor()
	{	DeleteAllChannels(channel_list);
		channel_list=0;
	}
	void SetChannelList(ImgChannel* channel)
	{	ExplicitDestructor();
		channel_list=channel;
	}
	ImgChannel* GetChannelList()
	{	return channel_list;
	}
	operator ImgChannel*()
	{	return GetChannelList();
	}
	ImgChannel* NewChannel(ImgChannel* prev)
	{	if(!prev)
		{	channel_list=::NewChannel(0);
			return channel_list;
		}
		return ::NewChannel(GetLastChannel());
	}
	//bool NewBlob(ImgChannel* channel);
	//void DeleteBlob(ImgChannel* channel);
	void DeleteChannel(ImgChannel* channel)
	{	DeleteChannel(channel);
		if(channel_list==channel)
		{	channel_list=0;
	}	}
	ImgChannel* GetChannel(unsigned n)
	{	return ::GetChannel(channel_list,n);
	}
	ImgChannel* GetLastChannel()
	{	return ::GetLastChannel(channel_list);
	}
};

#endif