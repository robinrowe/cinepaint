// ImgOp.h 2006.6.29
// Copyright 2004 Robin.Rowe@MovieEditor.com
// License OSI MIT

#ifndef IMG_OP_H
#define IMG_OP_H

#include <gel/data/SubstringField.h>
#include "ImgPlugin.h"
#include "ImgImage.h"

class ImgOp
{	bool is_verbose;
	ImgPlugin plugin;
public:
	ImgOp(ImgChannel* channel)
	:	plugin(channel)
	{	is_verbose=false;
	}
	bool operator!() const
	{	return plugin.IsBad();
	}
	bool IsOpenPlugin() const
	{	return plugin.IsOpen();
	}
	bool GetPluginHelp()
	{	return plugin.GetHelp("help");
	}
	bool IsVerbose() const
	{	return is_verbose;
	}
	void IsVerbose(bool is_verbose)
	{	this->is_verbose=is_verbose;
	}
	const char* GetPluginName(const char* filename)
	{	return "img_ppm.dll";
	}	
	bool Do(const char* command)
	{//	"in=filename [opt=...] [out=filename]"	
		gel::SubstringField field(command);
		if(!field)
		{	return false;
		}
		if(field.IsName("in"))
		{	const char* plugin_name=GetPluginName(field.value.data);
			if(!plugin_name)
			{	return false;
			}
			if(plugin.GetLibName()!=plugin_name)
			{	if(!plugin.Open(plugin_name))
				{	return false;
			}	}
			return plugin.Read(field.GetData());
		}
		if(field.IsName("out"))
		{	return plugin.Write(field.GetData());
		}
		return plugin.Setting(command);
	}
};

#endif


