// ImgWorld.h  2007.01.27
// Copyright 2006 Robin.Rowe@MovieEditor.com
// License OSI MIT 

#ifndef IMG_WORLD_H
#define IMG_WORLD_H

#include "ImgChannel.h"
#include "ImgPluginInfo.h"

struct ImgWorld
{	ImgChannel* channel;
	ImgPluginInfo* info;
	unsigned channel_from;
	unsigned channel_from_count;
	unsigned channel_to;
	unsigned channel_to_count;
};

#endif