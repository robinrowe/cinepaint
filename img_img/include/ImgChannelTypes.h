// ChannelTypes.h 10/23/04
// Copyright 2004 Robin.Rowe@MovieEditor.com
// License OSI MIT with additional studio disclaimers

#ifndef CHANNEL_TYPES_H
#define CHANNEL_TYPES_H

#include <limits.h>
#include <math.h>
#include <gel/data/dl_list.h>

#ifdef WIN32
#include <windows.h>
#include <windef.h>
#else
#define BYTE unsigned char
#endif

typedef unsigned char U8;
typedef unsigned short U16;
typedef unsigned long U32;
typedef unsigned long long U64;
typedef unsigned short F16;
typedef float F32;
typedef double F64;

typedef const char* ChannelType;
typedef const char* ChannelColor;
#undef CHANNEL_TYPE
#undef CHANNEL_COLOR
#define CHANNEL_TYPE(x) extern ChannelType img_type_##x
#define CHANNEL_COLOR(x) extern ChannelColor img_color_##x
#include "ImgChannelNames.h"

#undef CHANNEL_TYPE
#undef CHANNEL_COLOR

#ifdef WIN32
#define RINT int
#else
#define RINT rint
#endif

const U16 U16_U8_SCALER = USHRT_MAX/UCHAR_MAX; // 65535/255 = 257
const U32 U32_U8_SCALER = ULONG_MAX/UCHAR_MAX; // 4294967295UL/255 = 16843009
const U32 U32_U16_SCALER = ULONG_MAX/USHRT_MAX; // 4294967295UL/65535 = 65537

const float FLOAT_U8_MAX = (float) UCHAR_MAX;
const float FLOAT_U16_MAX = (float) USHRT_MAX;
const float FLOAT_U32_MAX = (float) ULONG_MAX;

inline
float ClampFloatChannel(float float_channel)
{	if(float_channel<0.0)
	{	return 0.0;
	}
	if(float_channel>1.0)
	{	return 1.0;
	}
	return float_channel;
}


inline
void ScaleChannel(const U8* u8,U16* u16)
{   *u16=U16_U8_SCALER**u8;
}

inline
void ScaleChannel(const U8* u8,U32* u32)
{   *u32=U32_U8_SCALER**u8;
}

inline
void ScaleChannel(const U8* u8,F32* f32)
{   *f32=(CAST(float) *u8)/FLOAT_U8_MAX;
}


inline
void ScaleChannel(const U16* u16,U8* u8)
{   *u8=CAST(U8) (*u16/U16_U8_SCALER);
}

inline
void ScaleChannel(const U16* u16,U32* u32)
{   *u32=U32_U16_SCALER**u16;
}

inline
void ScaleChannel(const U16* u16,F32* f32)
{   *f32=(CAST(float) *u16) / FLOAT_U16_MAX;
}


inline
void ScaleChannel(const U32* u32,U8* u8)
{   *u8=CAST(U8) (*u32/U32_U8_SCALER);
}

inline
void ScaleChannel(const U32* u32,U16* u16)
{   *u16=CAST(U16) (*u32/U32_U16_SCALER);
}

inline
void ScaleChannel(const U32* u32,F32* f32)
{   *f32=(CAST(float) *u32) / FLOAT_U32_MAX;
}


inline
void ScaleChannel(const F32* f32,U8* u8)
{   *u8=CAST(U8) RINT(ClampFloatChannel(*f32)*FLOAT_U8_MAX);
}

inline
void ScaleChannel(const F32* f32,U16* u16)
{   *u16=CAST(U16) RINT(ClampFloatChannel(*f32)*FLOAT_U16_MAX);
}

inline
void ScaleChannel(const F32* f32,U32* u32)
{   *u32=CAST(U32) RINT(ClampFloatChannel(*f32)*FLOAT_U32_MAX);
}

#endif

/*****************************************************
The OSI MIT License plus additional studio disclaimers

Copyright (c) 2004 Robin Rowe <Robin.Rowe@movieeditor.com>

Permission is hereby granted, free of charge, to any person 
obtaining a copy of this software and associated documentation 
files (the "Software"), to deal in the Software without 
restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or 
sell copies of the Software, and to permit persons to whom 
the Software is furnished to do so, subject to the following 
conditions:

The above copyright notice and this permission notice shall be 
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
OTHER DEALINGS IN THE SOFTWARE.

***STUDIO DISCLAIMERS

This software was created by Robin Rowe at a motion picture studio. 
As a condition of granting the software back to Robin Rowe for open 
source release, the following clauses are added.

3. THE SOFTWARE IS ASSIGEND "AS-IS" AND ROBIN ROWE MAKES NO 
REPRESENTATIONS, WARRANTIES, GUARANTEES, OR OTHER PROMISES 
WHATSOEVER, WHETHER EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, 
INCLUDING, WITHOUT LIMITATION, ANY WARRANTY REGARDING SAFETY, 
EFFICACY, PERFORMANCE, MERCHANTABILITY, QUALITY, TITLE, 
NON-INFRINGEMENT, FITNESS FOR A PARTICULAR PURPOSE, LACK OF 
VIRUSES, ACCURACY OR COMPLETENESS OF RESPONSES, RESULTS, OR DATA, 
FREEDOM FROM ERRORS, UNITERRUPTED OPERATION, LACK OF NEGLIGENCE, 
LACK OF A WORKMANLIKE EFFORT, QUIET ENJOYMENT, OR QUIET POSSESSION.

4. UNDER NO CIRCUMSTANCES SHALL ROBIN ROWE BE LIABLE TO YOU, OR ANY 
THIRD-PARTY FOR ANY ACTUAL, GENERAL, INDIRECT, SPECIAL, INCIDENTAL, 
CONSEQUENTIAL, PUNITIVE, OR OTHER DAMAGES IN CONNECTION WITH, 
ARISING UNDER, OR RELATED TO THE SOFTWARE (WHETHER UNDER THEORY OF 
CONTRACT, TORT, INDEMNITY, PRODUCT LIABILITY, OR OTHERWISE) INCLUDING, 
WITHOUT LIMITATION, DAMAGES RESULTING FORM DELAY OR FROM LOSS OF 
PROFITS, LOST BUSINESS OPPORTUNITIES OR GOODWILL, LOSS OF CONFIDENTIAL 
OR OTHER INFORMATION, BUSINESS INTERRUPTION, PERSONAL INJURY, LOSS OR 
PRIVACY, FAILURE TO MEET ANY DUTY (INCLUDING, WITHOUT LIMITATION, OF 
GOOD FAITH OR OF REASONABLE CARE), NEGLIGENCE, OR ANY OTHER LOSS 
WHATSOEVER, WHETHER OR NOT ROBIN ROWE HAS BEEN ADVISED OR IS AWARE OF 
THE POSSIBILITY OF SUCH DAMAGES.

*****************************************************/


