// ImgChannelNames.h 6/18/2006
// Copyright 2006 Robin.Rowe@MovieEditor.com
// License OSI MIT (http://opensource.org/licenses/mit-license.php)

// No #ifndef here is deliberate!
// Do not add any #includes here either!
// Not a stand-alone header file
// Included twice on purpose (for declaration and definition)

// Declare all our channel types as strings, e.g., img_type_u8="u8":

CHANNEL_TYPE(b16); //Binary 16-bit (HDR)

CHANNEL_TYPE(f16); //Float 16-bit (half)
CHANNEL_TYPE(f32); //Float 32-bit
CHANNEL_TYPE(f64); //Float 64-bit (double)

CHANNEL_TYPE(u8); //Unsigned 8-bit
CHANNEL_TYPE(u16);//Unsigned 16-bit
CHANNEL_TYPE(u32);//Unsigned 32-bit
CHANNEL_TYPE(u64);//Unsigned 64-bit

CHANNEL_TYPE(vec);//Vector data (not raster)

// Declare all our channel colors as strings, e.g., img_color_R="R":

CHANNEL_COLOR(r); //Red
CHANNEL_COLOR(g); //Green
CHANNEL_COLOR(b); //Blue
CHANNEL_COLOR(z); //Depth
CHANNEL_COLOR(rgb); //RGB interleave
CHANNEL_COLOR(rgba);//RGBA interleave
CHANNEL_COLOR(c);//Cyan
CHANNEL_COLOR(m);//Magenta
CHANNEL_COLOR(y);//Yellow
CHANNEL_COLOR(k);//Black (or Gray or Grey)
CHANNEL_COLOR(ka);//Black with Alpha
CHANNEL_COLOR(cmyk);//CMYK Interleave 
//future: XYZ 
//future: HSV 



