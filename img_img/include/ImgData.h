// ImgData.h 6/18/2006
// Copyright 2006 Robin.Rowe@MovieEditor.com
// License OSI MIT (http://opensource.org/licenses/mit-license.php)

#ifndef IMG_DATA_H
#define IMG_DATA_H

#include <gel/data/dl_list.h>
#include "ImgPluginData.h"

struct ImgData
{	DL_list channel_list; // a linked-list of channels
	bool is_verbose;
	const char* error_msg;
/* internal housekeeping */
	char* buffer_ptr;
	unsigned buffer_size;
	ImgPluginData plugin_data;
};

#endif

