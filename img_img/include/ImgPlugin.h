// ImgPlugin.h 2004.10.23
// Copyright 2004 Robin.Rowe@MovieEditor.com
// License OSI MIT with additional studio disclaimers

#ifndef IMG_PLUGIN_H
#define IMG_PLUGIN_H

#include <gel/io/GetFileSize.h>
#include <gel/io/DllLoader.h>
#include "ImgImage.h"
#include "ImgWorld.h"
#include "../plug-in/ImgPluginInterface.h"

class ImgPlugin
{	ImgWorld* world;
	DllLoader dll;
	const char* error_msg;
	const char* NO_PLUGIN;
	const char* BAD_OPEN;
	const char* NO_DLL_FUNCTION;
	const char* RETURNED_FALSE;
	bool RunPluginFunc(const char* func_name,const char* action)
	{	if(!dll.IsOpen())
		{	error_msg=NO_PLUGIN;
			return false;
		}
		PluginFunc func = (PluginFunc) dll.GetFunction(func_name);
		if(!func)
		{	error_msg=NO_DLL_FUNCTION;
			return false;
		}
		const bool ok = func(channel,action);
		if(!ok)
		{	error_msg=RETURNED_FALSE;
		}
		return ok;
	}
public:
	ImgPlugin(ImgImage& image)
	:	channel(image.GetChannelList())
	{	error_msg=0;
		bzero(&info);
		NO_PLUGIN="No plugin loaded";
		BAD_OPEN="Couldn't open plug-in";
		NO_DLL_FUNCTION="Couldn't open dll function";
		RETURNED_FALSE="Dll function returned false";
	}
	bool IsBad() const
	{	return 0!=error_msg;
	}
	bool IsOpen() const
	{	return dll.IsOpen();
	}
	bool Open(const char* plugin_name)
	{	if(!dll.Open(plugin_name))
		{	error_msg=BAD_OPEN;
			return false;
		}
		return true;
	}
	bool Read(const char* action)
	{	return RunPluginFunc("ImgReadFile",action);
	}
	bool Write(const char* action)
	{	if(!dll.IsOpen())
		{	error_msg=NO_PLUGIN;
			return false;
		}
		return RunPluginFunc("ImgWriteFile",action);
	}
	const char* GetErrorMsg() const
	{	return error_msg? error_msg:"(no error)";
	}
	bool GetHelp(const char* action) 
	{	return RunPluginFunc("ImgGetInfo",action);
	}	
//	ImgImage& GetImgData()
//	{	return img_data;
//	}
	bool Setting(const char* action)
	{	return RunPluginFunc("ImgSetting",action);
	}
	const string& GetLibName() const
	{	return dll.GetLibName();
	}
};

#endif

/*****************************************************
The OSI MIT License plus additional studio disclaimers

Copyright (c) 2004 Robin Rowe <Robin.Rowe@movieeditor.com>

Permission is hereby granted, free of charge, to any person 
obtaining a copy of this software and associated documentation 
files (the "Software"), to deal in the Software without 
restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or 
sell copies of the Software, and to permit persons to whom 
the Software is furnished to do so, subject to the following 
conditions:

The above copyright notice and this permission notice shall be 
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
OTHER DEALINGS IN THE SOFTWARE.

***STUDIO DISCLAIMERS

This software was created by Robin Rowe at a motion picture studio. 
As a condition of granting the software back to Robin Rowe for open 
source release, the following clauses are added.

3. THE SOFTWARE IS ASSIGEND "AS-IS" AND ROBIN ROWE MAKES NO 
REPRESENTATIONS, WARRANTIES, GUARANTEES, OR OTHER PROMISES 
WHATSOEVER, WHETHER EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, 
INCLUDING, WITHOUT LIMITATION, ANY WARRANTY REGARDING SAFETY, 
EFFICACY, PERFORMANCE, MERCHANTABILITY, QUALITY, TITLE, 
NON-INFRINGEMENT, FITNESS FOR A PARTICULAR PURPOSE, LACK OF 
VIRUSES, ACCURACY OR COMPLETENESS OF RESPONSES, RESULTS, OR DATA, 
FREEDOM FROM ERRORS, UNITERRUPTED OPERATION, LACK OF NEGLIGENCE, 
LACK OF A WORKMANLIKE EFFORT, QUIET ENJOYMENT, OR QUIET POSSESSION.

4. UNDER NO CIRCUMSTANCES SHALL ROBIN ROWE BE LIABLE TO YOU, OR ANY 
THIRD-PARTY FOR ANY ACTUAL, GENERAL, INDIRECT, SPECIAL, INCIDENTAL, 
CONSEQUENTIAL, PUNITIVE, OR OTHER DAMAGES IN CONNECTION WITH, 
ARISING UNDER, OR RELATED TO THE SOFTWARE (WHETHER UNDER THEORY OF 
CONTRACT, TORT, INDEMNITY, PRODUCT LIABILITY, OR OTHERWISE) INCLUDING, 
WITHOUT LIMITATION, DAMAGES RESULTING FORM DELAY OR FROM LOSS OF 
PROFITS, LOST BUSINESS OPPORTUNITIES OR GOODWILL, LOSS OF CONFIDENTIAL 
OR OTHER INFORMATION, BUSINESS INTERRUPTION, PERSONAL INJURY, LOSS OR 
PRIVACY, FAILURE TO MEET ANY DUTY (INCLUDING, WITHOUT LIMITATION, OF 
GOOD FAITH OR OF REASONABLE CARE), NEGLIGENCE, OR ANY OTHER LOSS 
WHATSOEVER, WHETHER OR NOT ROBIN ROWE HAS BEEN ADVISED OR IS AWARE OF 
THE POSSIBILITY OF SUCH DAMAGES.

*****************************************************/


