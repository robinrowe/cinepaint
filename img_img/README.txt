img_img
by Robin Rowe 10/23/04

The img_img project is a library and related programs for reading and writing image file formats. It's designed to make it easy to convert between in-memory RGB framebuffers (of many varieties) and image files (of many varieties). 

* Features

- Easy for programmers to read and write images without worrying about the details of any particular file format 
- Easy for programmers to code supporting plug-ins that read and write an image file format.
- Version 0.1 converts between 8-bit and 16-bit PPM files

* Copyright 2004 Robin.Rowe@MovieEditor.com

* Licenses

Check each file for which of these licenses applies.

- OSI MIT license, a BSD-style license (http://opensource.org/licenses/mit-license.php)
- OSI MIT license with additional studio disclaimer clauses

* Dependencies

This code has only been compiled with VC++ 6. It is designed to port easily to other platforms, but that hasn't been done yet. Despite being Windows-based, you may be surprised to find that in many cases this code calls to unix-based APIs and header files.

This code depends upon a new utility library by Robin Rowe called Gel. This lightweight library emulates some of the more popular bits of iostream and the STL. It also makes some unix-based APIs available on Windows. I replaced iostream and STL because they're too heavy and have annoying bugs on VCPP6. With Gel, calls that look like iostream actually use the much more optimized stdio (printf) library. I haven't tried mixing iostream and Gel. Programs that want to use iostream-like io caninclude Gel's iofile.h instead of iostream.h.

* API

Movie.Sequence.Image.Layer.Channel.Pixel

An image is a list of channels. Each channel has a layer#.

enum {width=640,height=480,depth=8};
Channel* channel=0;
channel=ImgCreateChannel(width,height,depth,format,channel);// 0=new image
memset(channel,0,3*width*height*depth/8); // set image all black, for example

Channel* channel_b=ImgRead(filename);
ok=ImgWrite(channel_b);

channel2=channel->GetNext();





data* ImgOpen(params*)
bool ImgClose(name*,#)
data* ImgGet(name*,#)
bool ImgCopy(data*,data*)
bool ImgClear(data*,pixel*)

--

Movie* ImgCreateMovie();
Sequence* ImgCreateSequence(Movie*);
Image* ImgCreateImage(Sequence*);
Layer* ImgCreateLayer(Image*);
Channel* ImgCreateChannel(Layer*);// if 0, create new movie

Movie* ImgGetMovie();
Sequence* ImgGetSequence(Movie*);
Image* ImgGetImage(Sequence*);
Layer* ImgGetLayer(Image*);
Channel* ImgGetChannel(Layer*);


enum {width=640,height=480,depth=8};
Channel* channel=ImgCreateChannel(width,height,depth,"RGB");
memset(channel,0,3*width*height); // set image all black, for example
unsigned id=channel.GetId();

Channel* channel3=ImgRead(filename);
ok=ImgWrite(channel);

... do whatever else you want to do with a raw rgb buffer ...
Channel* channel2=ImgCreateChannel(channel);
bool ok;
ok=ImgCommand(image_id,"blur","image");
bool ok=imgCommand(image_id,"blur","channel|1");
float progress=imgGetProgress(image_id);// 100.0% is default, 0.0 is error

bool ok=imgWriteImage(image_num,"test1.jpg");// save image as JPEG, for example
imgClose(image_id);// Memory released, image_id and rgb no longer valid
image_id=imgOpenImage("test1.jpg");// image_id here is different from before
imgInfo info; /* struct imgInfo { unsigned width; unsigned height; ...}; */
imgGetInfo(image_id,&info); // get some image stats
rgb=imgGetImage(image_id);



* Directories and Files

img_img/docs - Documentation (not much yet)
img_img/img_img - Command-line img_img file converter (like ImageMagick convert)
	main.cpp
img_img/img_server - Future server wrapper
img_img/include - include files
	ImgChannel.h: ImgChannel struct with data and metadata
	ImgChannelNames.h: RGB, RGBA, CMYK, etc.
	ImgChannelTypes.h: U8, U16, U32, F16, F32
	ImgImage.h: a list of layers
	ImgLayer.h: a list of channels
	ImgManager.h: a class wrapper to the image struct
	ImgMovie.h: a list of sequences
	ImgOp.h: parses a command and passes it to a plug-in
	ImgProject.h: contains a movie
	ImgProjectMgr.h: contains a project
	ImgPlugin.h: a class wrapper to the C plug-in API
	ImgPluginData.h: unused
	ImgSequence.h: a list of images
img_img/lib - cpp files
	CommandLine.cpp
	ImOp.cpp
	ImgChannel.cpp
	ImgHolder.cpp
	ImgProjectMgr.cpp
img_img/plug-in
	ImgInfoInterface.h
	ImgPluginInterface.h
	ImgReaderInterface.h
	ImgWriterInterface.h
	PLUGIN.HOWTO.txt
	main.cpp
img_img/plug-in/img_ppm - ppm reader/writer plug-in
	ImgInfo.h
	ImgReader.cpp
	ImgReader.h
	ImgWriter.cpp
	ImgWriter.h
img_img/test - test data files
img_img/vcpp6 - project files and where executables build
../../gel - Gel library
../../gel/data - class Buffer, etc.
../../gel/io - class IoFile, etc.
../../gel/lib - cpp files
../../gel/vcpp6 - project files
../../gel/win32 - unix-based APIs remapped into Win32 calls

Release 0.1 10/23/04

- Support for PPM files only, at 8 or 16 bit
