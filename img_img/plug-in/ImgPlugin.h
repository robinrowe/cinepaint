// ImgPlugin.h 10/23/04
// Copyright 2004 Robin.Rowe@MovieEditor.com
// License OSI MIT

#ifndef IMAGE_PLUGIN_H
#define IMAGE_PLUGIN_H

#include <gel/io/dll_api.h>

extern "C" {

struct ImgData;
struct ImgPluginData;

typedef bool (*PluginFunc)(ImgData* img_data,ImgPluginData* plugin_data);
typedef const char* (*HelpFunc)(const char* options);
typedef const char* (*VerFunc)();
    	
DLL_API bool ImgReadFile(ImgData* img_data,ImgPluginData* plugin_data);
DLL_API bool ImgWriteFile(ImgData* img_data,ImgPluginData* plugin_data);
DLL_API bool ImgGetInfo(ImgData* img_data,ImgPluginData* plugin_data);

}

#endif

