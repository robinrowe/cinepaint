// ImgPlugin.h 10/23/04
// Copyright 2004 Robin.Rowe@MovieEditor.com
// License OSI MIT

#ifndef IMAGE_PLUGIN_H
#define IMAGE_PLUGIN_H

#include <gel/io/dll_api.h>

extern "C" {

struct ImgWorld;

typedef bool (*PluginFunc)(ImgWorld* world,const char* action);
}
    	
DLL_C_API bool ImgReadFile(ImgWorld* world,const char* action);
DLL_C_API bool ImgWriteFile(ImgWorld* world,const char* action);
DLL_C_API bool ImgSet(ImgWorld* world,const char* action);
DLL_C_API bool ImgDo(ImgWorld* world,const char* action);

#endif

