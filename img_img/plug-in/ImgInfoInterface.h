// ImgInfoInterface.h 10/9/04
// Copyright 2004 Robin.Rowe@MovieEditor.com
// License OSI MIT

#ifndef IMG_INFO_INTERFACE_H
#define IMG_INFO_INTERFACE_H

#include "../include/ImgImage.h"
#include "../include/ImgPluginInfo.h"

class ImgInfoInterface
{public:
	virtual bool GetInfo(ImgPluginInfo* plugin_info)=0;
};

#endif