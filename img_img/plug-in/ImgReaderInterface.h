// ImgReaderInterface.h 10/9/04
// Copyright 2004 Robin.Rowe@MovieEditor.com
// License OSI MIT with additional studio disclaimers

#ifndef IMG_READER_INTERFACE_H
#define IMG_READER_INTERFACE_H

#include <gel/io/IoFileBuffer.h>
#include "../include/ImgWorld.h"

class ImgReaderInterface
{	
public:
	gel::IoFileBuffer<unsigned char> file_in_memory;
	virtual bool Read(ImgWorld* world,const char* filename)
	{	if(!filename) 
		{	world->info->error_msg="Invalid filename";
			return false;
		}
		if(!file_in_memory.OpenAndRead(filename))
		{	return false;
		}
		return Unpack(world);
	}
	virtual bool Unpack(ImgWorld* world)
	{	if(!UnpackHeader(world))
		{	return false;
		}
		return UnpackBody(world);
	}
	virtual bool UnpackHeader(ImgWorld* world)=0;
	virtual bool UnpackBody(ImgWorld* world)=0;
};

#endif

