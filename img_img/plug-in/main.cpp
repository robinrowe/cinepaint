// main.cpp 10/9/04
// Copyright 2004 Robin.Rowe@MovieEditor.com
// License OSI MIT 

#include <ImgInfo.h>
#include <ImgReader.h>
#include <ImgWriter.h>
#include <ImgSetting.h>
#include <ImgFilter.h>
#include "../include/ImgWorld.h"
#include "ImgPluginInterface.h"

DLL_C_API bool ImgReadFile(ImgWorld* world,const char* filename)
{	if(!world)
	{	return false;
	}
	if(!filename ||!*filename)
	{	world->info->error_msg="Invalid plug-in data";
		return false;
	}
	ImgReader reader;
	const bool result = reader.Read(world,filename);
	return result;
}


DLL_C_API bool ImgWriteFile(ImgWorld* world,const char* filename)
{	if(!world)
	{	return false;
	}
	if(!filename || !*filename)
	{	world->info->error_msg="Invalid plug-in data";
		return false;
	}
	ImgWriter writer;
	const bool result = writer.Write(world,filename);
	return result;
}

DLL_C_API bool ImgSet(ImgWorld* world,const char* action)
{	if(!world)
	{	return false;
	}
	if(!action || !*action)
	{	world->info->error_msg="Invalid plug-in data";
		return false;
	}
	ImgSetting setting;
	const bool result = setting.Set(world,action);
	return result;
}

DLL_C_API bool ImgDo(ImgWorld* world,const char* action)
{	if(!world)
	{	return false;
	}
	if(!action || !*action)
	{	world->info->error_msg="Invalid plug-in data";
		return false;
	}
	ImgFilter filter;
	const bool result = filter.Do(world,action);
	return result;
}

#ifdef WIN32

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

BOOL APIENTRY 
DllMain(HANDLE hModule,DWORD ul_reason_for_call,LPVOID lpReserved)
{    return TRUE;
}

#endif

