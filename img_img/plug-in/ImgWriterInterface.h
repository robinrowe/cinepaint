// ImgWriterInterface.h 10/9/04
// Copyright 2004 Robin.Rowe@MovieEditor.com
// License OSI MIT

#ifndef IMG_WRITER_INTERFACE_H
#define IMG_WRITER_INTERFACE_H

#include <gel/io/IoFile.h>
#include "../include/ImgWorld.h"

class ImgWriterInterface
{protected:
	gel::IoFile file;
	bool WriteRawData(ImgChannel* channel)
	{	if(!channel)
		{	return false;
		}
		void* data=channel->blob.data;
		const unsigned image_bytes=GetByteCount(channel);
		file.Write(data,image_bytes);
		return file.IsGood();
	}
	virtual bool WriteHeader(ImgWorld* world)=0;
	virtual bool WriteBody(ImgWorld* world)=0;
public:
	virtual bool Write(ImgWorld* world,const char* filename)
	{	if(!filename || !*filename || !world)
		{	return false;
		}
		if(!file.Open(filename,"wb"))
		{	return false;
		}
		if(!WriteHeader(world))
		{	return false;
		}
		return WriteBody(world);
	}
};

#endif

