// ImgFilter.h 2007.01.27
// Copyright 2007 Robin.Rowe@MovieEditor.com
// License OSI MIT

#ifndef IMG_FILTER_H
#define IMG_FILTER_H

#include "../ImgFilterInterface.h"

class ImgFilter
:	public ImgFilterInterface
{public:
	virtual bool Do(ImgWorld* world,const char* action)
	{	return false;
	}
};

#endif