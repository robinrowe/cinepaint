// ImgReader.h 9/21/04
// Copyright 2004 Robin.Rowe@MovieEditor.com
// License OSI MIT

#ifndef IMG_READER_H
#define IMG_READER_H

#include "../ImgReaderInterface.h"

class ImgReader
:	public ImgReaderInterface
{
public: 
//	ImageBuffer image_buffer;
//	gel::FileInMemory file_in_memory;
	virtual bool UnpackHeader(ImgWorld* world);
	virtual bool UnpackBody(ImgWorld* world);
};

#endif
