// ImgWriter.h 10/10/04
// Copyright 2004 Robin.Rowe@MovieEditor.com
// License OSI MIT

#ifndef IMG_WRITER_H
#define IMG_WRITER_H

#include "../ImgWriterInterface.h"

class ImgWriter
:	public ImgWriterInterface
{	unsigned bit_depth_out;
public:
	ImgWriter()
	{	bit_depth_out=0;
	}
	bool SetOutputOptions(const char* options);
	bool WriteHeader(ImgWorld* world);
	bool WriteBody(ImgWorld* world);
};

#endif
