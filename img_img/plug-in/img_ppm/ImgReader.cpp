// ImageReader.cpp 10/23/04
// Copyright 2004 Robin.Rowe@MovieEditor.com
// License OSI MIT with additional studio disclaimers (see below)

#include <gel/io/IoFile.h>
#include <gel/data/Substring.h>
#include <gel/data/Number.h>
#include <ctype.h>
#include <netinet/in.h>  // for ntohs()
#include "ImgReader.h"

#define VERBOSE if(image->IsVerbose()) cout

/* 

*** Robin's optimization to read buffer at a go in 16-bit then htons:

/usr/home/rrowe/local/test% img_img sx.ppm sx2.ppm
0.96946 12075787 2.437188 12075787

*** Read performance took 3x longer reading one htons short at a time:

/usr/home/rrowe/local/test% img_img sx.ppm sx2.ppm
2.902723 12075787 2.427430 12075787
*/

bool ImgReader::UnpackHeader(ImgChannel* channel)
{	ImgImage image(channel);
	ImgChannel* channel=image->NewChannel(0);
	if(!channel)
	{	return false;
	}
	// P6
	// # comment
	// 640 480
	// 255
	// blob...
	Substring tokenizer((char*)file_in_memory.buffer.GetUnsafeData(),file_in_memory.buffer.GetSize());
	enum TOKEN {p_type,width,height,max_color_val,t_count};
	unsigned token[t_count];
	unsigned i=p_type;
	Substring line(tokenizer.EatLine());
	line.Squeeze();
	if(!line.IsEqual("P6"))
	{	image->SetErrorMsg("Not a 'P6' PPM file");
		return false;
	}
	token[i]=6;
	i++;
	while(i<t_count)
	{	line=tokenizer.EatLine();
		if(!line)
		{	if(!tokenizer)
			{	break;
			}
			continue;
		}
		if('#'==line.Peek(0)) 
		{	// skip comments
			continue;
		}
		line.Squeeze();
		while(!line.IsBlank())
		{	Substring word=line.EatWord();
			gel::Unsigned_t number(word);
			if(number.IsBad())
			{	image->SetErrorMsg("Bad header (1)");
				return false;
			}
			token[i]=number;
			i++;
	}	}
	if(i<t_count)
	{	image->SetErrorMsg("Bad header (2)");
		return false;
	}
	tokenizer.EatWhitespace();
	unsigned char* ptr=(unsigned char*) tokenizer.data;
	channel->blob.data=ptr;
	channel->blob.length=tokenizer.length;
	const long offset=ptr-file_in_memory.buffer.GetUnsafeData();
	// bug: needs sanity check on tokenizer.length.

	VERBOSE << "offset to PPM raster: "<<offset<<endl;

	channel->width=token[width];
	channel->height=token[height];
	channel->channel_color=img_color_rgb;
	if(token[max_color_val] < 256)
	{	channel->bit_depth=8;
		channel->channel_type=img_type_u8;
	}
	else
	{	channel->bit_depth=16;
		channel->channel_type=img_type_u16;
	}

	VERBOSE << "PPMReader::read() got:\n"
	<< " MAGICK NUMBER: P6\n"
	<< " Width: " << GetWidth(channel) << "\n"
	<< " Height: " << GetHeight(channel) << "\n"
	<< " Max color value: " << token[max_color_val] << "\n"
	<< " bit depth: " << GetBitDepth(channel) << "\n";
	return true;
}

bool ImgReader::UnpackBody(ImgImage* image)
{//	Don't allocate new buffer because can reuse existing!
	ImgChannel* channel=image->GetChannel(0);
	if(!channel)
	{	return false;
	}
	file_in_memory.buffer.UnsafeDisableDelete();
	if(16==GetBitDepth(channel))
	{	SwapHostToNetByteOrder(channel);
	}
	return true;
}

/*****************************************************
The OSI MIT License plus additional studio disclaimers

Copyright (c) 2004 Robin Rowe <Robin.Rowe@movieeditor.com>

Permission is hereby granted, free of charge, to any person 
obtaining a copy of this software and associated documentation 
files (the "Software"), to deal in the Software without 
restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or 
sell copies of the Software, and to permit persons to whom 
the Software is furnished to do so, subject to the following 
conditions:

The above copyright notice and this permission notice shall be 
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
OTHER DEALINGS IN THE SOFTWARE.

***STUDIO DISCLAIMERS

This software was created by Robin Rowe at a motion picture studio. 
As a condition of granting the software back to Robin Rowe for open 
source release, the following clauses are added.

3. THE SOFTWARE IS ASSIGEND "AS-IS" AND ROBIN ROWE MAKES NO 
REPRESENTATIONS, WARRANTIES, GUARANTEES, OR OTHER PROMISES 
WHATSOEVER, WHETHER EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, 
INCLUDING, WITHOUT LIMITATION, ANY WARRANTY REGARDING SAFETY, 
EFFICACY, PERFORMANCE, MERCHANTABILITY, QUALITY, TITLE, 
NON-INFRINGEMENT, FITNESS FOR A PARTICULAR PURPOSE, LACK OF 
VIRUSES, ACCURACY OR COMPLETENESS OF RESPONSES, RESULTS, OR DATA, 
FREEDOM FROM ERRORS, UNITERRUPTED OPERATION, LACK OF NEGLIGENCE, 
LACK OF A WORKMANLIKE EFFORT, QUIET ENJOYMENT, OR QUIET POSSESSION.

4. UNDER NO CIRCUMSTANCES SHALL ROBIN ROWE BE LIABLE TO YOU, OR ANY 
THIRD-PARTY FOR ANY ACTUAL, GENERAL, INDIRECT, SPECIAL, INCIDENTAL, 
CONSEQUENTIAL, PUNITIVE, OR OTHER DAMAGES IN CONNECTION WITH, 
ARISING UNDER, OR RELATED TO THE SOFTWARE (WHETHER UNDER THEORY OF 
CONTRACT, TORT, INDEMNITY, PRODUCT LIABILITY, OR OTHERWISE) INCLUDING, 
WITHOUT LIMITATION, DAMAGES RESULTING FORM DELAY OR FROM LOSS OF 
PROFITS, LOST BUSINESS OPPORTUNITIES OR GOODWILL, LOSS OF CONFIDENTIAL 
OR OTHER INFORMATION, BUSINESS INTERRUPTION, PERSONAL INJURY, LOSS OR 
PRIVACY, FAILURE TO MEET ANY DUTY (INCLUDING, WITHOUT LIMITATION, OF 
GOOD FAITH OR OF REASONABLE CARE), NEGLIGENCE, OR ANY OTHER LOSS 
WHATSOEVER, WHETHER OR NOT ROBIN ROWE HAS BEEN ADVISED OR IS AWARE OF 
THE POSSIBILITY OF SUCH DAMAGES.

*****************************************************/






