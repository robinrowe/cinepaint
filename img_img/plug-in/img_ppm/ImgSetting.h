// ImgSetting.h 10/9/04
// Copyright 2004 Robin.Rowe@MovieEditor.com
// License OSI MIT

#ifndef IMG_SETTING_H
#define IMG_SETTING_H

#include "../ImgSettingInterface.h"

class ImgSetting
:	public ImgSettingInterface
{public:
	virtual bool Set(ImgWorld* world,const char* action)
	{	if(!world)
		{	return false;
		}
		world->info->plugin_help=
			"Copyright: 2004 Robin.Rowe@MovieEditor.com\n"
			"License: OSI MIT\n"
			"Type: ppm\n"
			"Settings: bits=8|16\n"
			;
		world->info->plugin_version=
			"img_ppm 0.1 "__DATE__ " " __TIME__;
	if(!action)
	{	return true;
	}
	if(strstr(options,"bits=8"))
	{	bit_depth_out=8;
		return true;
	}
	if(strstr(options,"bits=16"))
	{	bit_depth_out=16;
		return true;
	}
	bit_depth_out=0;
	return false;


		return true;
	}
};

#endif