// ImgInfo.h 10/9/04
// Copyright 2004 Robin.Rowe@MovieEditor.com
// License OSI MIT

#ifndef IMG_INFO_H
#define IMG_INFO_H

#include "../ImgInfoInterface.h"

class ImgInfo
:	public ImgInfoInterface
{public:
	virtual bool GetInfo(ImgPluginInfo* plugin_info)
	{	if(!plugin_info)
		{	return false;
		}
		plugin_info->plugin_help=
			"Copyright: 2004 Robin.Rowe@MovieEditor.com\n"
			"License: OSI MIT\n"
			"Type: ppm\n"
			"Settings: bits=8|16\n"
			;
		plugin_info->plugin_version=
			"img_ppm 0.1 "__DATE__ " " __TIME__;
		return true;
	}
};

#endif