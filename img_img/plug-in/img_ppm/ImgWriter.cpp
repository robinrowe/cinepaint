// ImageWriter.cpp 10/10/04
// Copyright 2004 Robin.Rowe@MovieEditor.com
// License OSI MIT

#include "ImgWriter.h"

bool ImgWriter::WriteHeader(ImgWorld* channel)
{	if(!channel)
	{	return false;
	}	
	if(!bit_depth_out)
	{	bit_depth_out=GetBitDepth(channel);
	}
	const char* max_color_val=(8==bit_depth_out)? "255":"65535";
	file<<"P6"<<endl
	    <<GetWidth(channel)<<endl
	    <<GetHeight(channel)<<endl
	    <<max_color_val<<endl;
	return file.IsGood();
}

bool ImgWriter::WriteBody(ImgWorld* world)
{	if(!channel)
	{	return false;
	}
	if(GetBitDepth(channel)==bit_depth_out)
	{	return WriteRawData(channel);
	}
	if(8==GetBitDepth(channel) && 16==bit_depth_out)
	{	ImgChannel channel16=*channel;
		bzero(&channel16.blob);
		channel16.bit_depth=16;
		if(!NewBlob(&channel16))
		{	return false;
		}
		CopyU8ToU16(channel,&channel16);
		SwapHostToNetByteOrder(&channel16);
		const bool ok=WriteRawData(&channel16);
		DeleteBlob(&channel16);
		return ok;
	}
	if(16==GetBitDepth(channel) && 8==bit_depth_out)
	{	ImgChannel channel8=*channel;
		bzero(&channel8.blob);
		channel8.bit_depth=8;
		if(!NewBlob(&channel8))
		{	return false;
		}
		CopyU16ToU8(channel,&channel8);
		const bool ok=WriteRawData(&channel8);
		DeleteBlob(&channel8);
		return ok;
	}
	return false;
}

