// FileParser.h
// Parses and writes VC++ Dsp project files
// Created Dec 25, 2002, by Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)

#ifndef FILE_PARSER_H
#define FILE_PARSER_H

class FileParser
{	char* buffer;
	unsigned filesize;
	void Reset()
	{	delete[] buffer;
		buffer=0;
		filesize=0;
	}

public:
	FileParser()
	:	buffer(0)
	{	Reset();
	}
	~FileParser()
	{	Reset();
	}
	bool operator!() const
	{	return !buffer;
	}
	bool Open(const char* known_good_file);
	bool Write(const char* filename,const char* old_name,const char* new_name);
};


#endif
