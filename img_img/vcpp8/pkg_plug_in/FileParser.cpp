// FileParser.cpp
// Parses and writes VC++ Dsp project files
// Created Dec 25, 2002, by Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)

//#include <io.h>
//#include <fstream.h>
//#include <stdio.h>

#include <gel/io/IoFile.h>
#include "FileParser.h"
#include "helpers.h"

bool FileParser::Open(const char* filename)
{	Reset();
	if(!filename)
	{	return false;
	}
	gel::IoFile is;
	if(!is.Open(filename))
	{	return false;
	}
	filesize=GetFileSize(filename);
	if(!filesize)
	{	return false;
	}
	buffer=new char[filesize+1];
	if(!buffer)
	{	return false;
	}
	if(filesize!=is.Read(buffer,filesize))
	{	return false;
	}
	buffer[filesize]=char(0);
	return true;
}

bool FileParser::Write(const char* filename,const char* old_name,const char* new_name)
{	if(!old_name || !new_name)
	{	return false;
	}
	gel::IoFile os;
	if(!os.Open(filename,"wb"))
	{	return false;
	}
	const size_t name_len=strlen(old_name);
	char caps_old_name[_MAX_FNAME];
	CopyToUpper(caps_old_name,old_name);
	char caps_new_name[_MAX_FNAME];
	CopyToUpper(caps_new_name,new_name);
	const char* ptr=buffer;
	const char* end=0;
	for(;;)
	{	end=FindNextMatch(ptr,old_name,caps_old_name);
		if(!end)
		{	const unsigned bytes=buffer+filesize-ptr;
			return bytes==os.Write((void*)ptr,bytes);
		}
		const unsigned bytes=end-ptr;
		if(!bytes==os.Write((void*)ptr,bytes))
		{	return false;
		}
		if(isupper(*end))
		{	os<<caps_new_name;
		}
		else
		{	os<<new_name;
		}
		if(0==os.IsGood())
		{	return false;
		}
		ptr=end+name_len;
	}
}

