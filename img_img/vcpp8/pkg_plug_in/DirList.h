// DirList.h
// Iterates through directory names
// Created Dec 25, 2002, by Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)

#ifndef DIR_LIST_H
#define DIR_LIST_H

#include <io.h>

class DirList
{	struct _finddata_t find_data;
	long handle;
public:
	DirList()
	{	handle=-1L;
	}
	~DirList()
	{	Close();
	}
	bool IsHandle() const
	{	return -1L!=handle;
	}
	void Close()
	{	if(IsHandle())
		{	_findclose(handle);
			handle=-1L;
	}	}	
	bool Open(const char* match)
	{	Close();
		handle=_findfirst(match,&find_data);
		return IsHandle();
	}
	bool IsSubDir() const
	{	return IsHandle() && (find_data.attrib & _A_SUBDIR);
	}
	const char* GetFileName() const
	{	return find_data.name;
	}
	unsigned GetFileSize() const
	{	return find_data.size;
	}
	bool Next()
	{	const int error=_findnext(handle,&find_data);
		if(error)
		{	Close();
		}
		return 0==error;
	}
};

#endif
