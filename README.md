CinePaint

HDR paint app used by the film industry and photographers for retouching of image sequences.

* Hollywood: Version 1 of CinePaint, uses GTK
* Glasgow: Version 2 of CinePaint, uses FLTK
* Ostend: CineBrush, uses FLTK
* img_img: CLI image manipulation

Hollywood CinePaint1 GTK1 only runs on Windows now. I've been working on the Linux version, expect it this month. MacOS port after that. Wild guess, end of April. 

Glasgow CinePaint2 FLTK has some functionality, but is incomplete. Nobody has touched it in a long time. I tried to build it now, but is broken due to outdated build settings. Too much missing and broken for now. 

Ostend Cinebrush FLTK is a minimal paint program. I hadn't built it in a while. Tried tonight. Fixed hundreds of compile errors, but still has link errors. Should be able to get it working pretty quickly on all platforms including MacOS. For MacOS rotobrush development soon, seems best. Maybe easier approach anyway. Work on a much smaller app and then integrate feature into Hollywood.

