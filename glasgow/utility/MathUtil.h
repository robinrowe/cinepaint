/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Util Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: MathUtil.h,v 1.1 2006/01/05 00:49:29 robinrowe Exp $
 */

#ifndef _MATH_UTIL_H_
#define _MATH_UTIL_H_

/**
* Various Collected static Math methods and constants
*
*/
class MathUtil
{
	public:
		/**
		 * Clamps to_clamp between the values min_val and max_val.
		 * to_clamp and the clamping values need not be the same type so long as
		 * the <= oprtator and >= operator are supported between types.
		 *
		 * @param to_clamp the value to be clamped
		 * @param min_val the minimum value of the clamp range
		 * @param max_val the maximum value in the clamp range
		 * @return the clamped value
		 */
		template<class ArgType1, class ArgType2>
		static ArgType1 clamp(ArgType1 to_clamp, ArgType2 min_val, ArgType2 max_val) ;


		static void calc_scale_to_fit_cell(int& width, int& height, int cell_width, int cell_height);

        // static constants

        static const double CP_PI;

protected:
	/**
	 * Disallow instances
	 */
	MathUtil() {} ;

private:

}; /* class MathUtil */


// implementation

template<class ArgType1, class ArgType2>
ArgType1 MathUtil::clamp(ArgType1 to_clamp, ArgType2 min_val, ArgType2 max_val)
{
	if(to_clamp <= min_val)
	{
		return(min_val);
	}
	else if(to_clamp >= max_val)
	{
		return(max_val);
	}
	else
	{
		return(to_clamp);
	}
}


#endif /* _MATH_UTIL_H_ */
