/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Util Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CPRect.cpp,v 1.1 2006/01/05 00:49:29 robinrowe Exp $
 */

#include "CPRect.h"

#include <algorithm>

CPRect::CPRect()
		: m_x(0), m_y(0), m_width(0), m_height(0)
{}

CPRect::CPRect(int x, int y, int w, int h)
		: m_x(x), m_y(y), m_width(w), m_height(h)
{}

CPRect::~CPRect()
{}

int CPRect::GetX() const
{
	return(m_x);
}

void CPRect::SetX(int x)
{
	m_x = x;
}

int CPRect::GetY() const
{
	return(m_y);
}

void CPRect::SetY(int y)
{
	m_y = y;
}


int CPRect::GetWidth() const
{
	return(m_width);
}


void CPRect::SetWidth(int width)
{
	m_width = width;
}

int CPRect::GetHeight() const
{
	return(m_height);
}

void CPRect::SetHeight(int height)
{
	m_height = height;
}

void CPRect::SetRect(const CPRect& rect)
{
	SetRect(rect.GetX(), rect.GetY(), rect.GetWidth(), rect.GetHeight());
}

void CPRect::SetRect(int x, int y, int w, int h)
{
	m_x = x;
	m_y = y;
	m_width = w;
	m_height = h;
}

void CPRect::Add(int x, int y, int w, int h)
{
	CPRect _r(x, y, w, h);
	this->Add(_r);
}

void CPRect::Add(const CPRect& rect)
{
	int _dest_x = this->GetX() <= 0 ? rect.GetX() : std::min(this->GetX(), rect.GetX());
	int _dest_y = this->GetY() <= 0 ? rect.GetY() : std::min(this->GetY(), rect.GetY());
	int _dest_w = this->GetWidth() <= 0 ? rect.GetWidth() : std::max(this->GetX() + this->GetWidth(), rect.GetX() + rect.GetWidth()) - _dest_x;
	int _dest_h = this->GetHeight() <= 0 ? rect.GetHeight() : std::max(this->GetY() + this->GetHeight(), rect.GetY() + rect.GetHeight()) - _dest_y;

	this->SetRect(_dest_x, _dest_y, _dest_w, _dest_h);
}

bool CPRect::Contains(int x, int y, int w, int h) const
{
	return((this->GetX() <= x) && ((this->GetX() + this->GetWidth()) >= (x + w)) &&
		(this->GetY() <= y) && ((this->GetY() + this->GetHeight()) >= (y + h)));
}

bool CPRect::Contains(const CPRect& rect) const
{
	return(Contains(rect.GetX(), rect.GetY(), rect.GetWidth(), rect.GetHeight()));
}

bool CPRect::IsEmpty() const
{
	return((m_height <= 0) || (m_width <= 0));
}


CPRect& CPRect::Intersect(const CPRect& src2, CPRect& dst) const
{
	int _dest_x = std::max(this->GetX(), src2.GetX());
	int _dest_y = std::max(this->GetY(), src2.GetY());
	int _dest_w = std::min(this->GetX() + this->GetWidth(), src2.GetX() + src2.GetWidth()) - _dest_x;
	int _dest_h = std::min(this->GetY() + this->GetHeight(), src2.GetY() + src2.GetHeight()) - _dest_y;

	if((_dest_w > 0) && (_dest_h > 0))
	{
		dst.SetRect(_dest_x, _dest_y, _dest_w, _dest_h);
	}
	else
	{
		dst.SetRect(0, 0, 0, 0);
	}

	return(dst);
}

bool CPRect::Intersects(const CPRect& rect) const
{
	int _dest_x = std::max(this->GetX(), rect.GetX());
	int _dest_y = std::max(this->GetY(), rect.GetY());
	int _dest_w = std::min(this->GetX() + this->GetWidth(), rect.GetX() + rect.GetWidth()) - _dest_x;
	int _dest_h = std::min(this->GetY() + this->GetHeight(), rect.GetY() + rect.GetHeight()) - _dest_y;

	if((_dest_w < 0) || (_dest_h < 0))
	{
		return(false);
	}
	else
	{
		return(true);
	}
}

