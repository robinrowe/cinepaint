/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CPSocket - Client/Server execution module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * Based on Linux Socket Programming In C++ By Rob Tougher from LinuxGazzete
 *
 * $Id: CPSocket.h,v 1.2 2006/12/18 08:21:25 robinrowe Exp $
 */

#ifndef _CPSOCKET_H_
#define _CPSOCKET_H_

#include <string>

#ifdef _WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
///	int MSG_NOSIGNAL = 0;
	typedef SOCKET Socket_t;
	typedef long suseconds_t;
#else 
	#include <sys/time.h>
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <arpa/inet.h>

	#ifdef __APPLE__
		// brings in sockaddr_in
		#include <tcpd.h>
		// not defined in Darwin
		typedef long suseconds_t;
		// not in Darwin either
		const int MSG_NOSIGNAL = SO_NOSIGPIPE;
	#endif /* __APPLE__ */ 

	typedef int Socket_t;
	const Socket_t INVALID_SOCKET = -1;
#endif

const int MAXHOSTNAME = 200;
const int MAXCONNECTIONS = 5;
const int MAXRECV = 500;

class CPSocket
{
public:
	CPSocket();
	virtual ~CPSocket();

	//==================================
	// Enum for wait return.
	enum WaitEndENUM {SOCK_DATA, SOCK_CLOSE, SOCK_ERROR};
	//==================================
	//Methods required to create a Server
	bool create();
	bool bind ( const int port );
	bool listen();
	bool accept ( CPSocket& );

	//===================================
	// Methods required to create a Client
	bool connect ( const char*  host, const int port );

	bool close();
	//===================================
	// Blocking call to wait for data.
	WaitEndENUM BlockingWait();
	void CancelBlockingWait();
	//===============================
	// Methods for data transimission 
	// Overloaded for various types.
	bool send ( const std::string ) const;
	bool send ( const int ) const;
	bool send ( const float ) const;
	bool send ( const char*  ) const;
	bool send ( const unsigned char* , const int ) const;

	int recv ( std::string& ) const;
	int recv ( int& ) const;
	int recv ( float& ) const;
	int recv ( char* & ) const;
	int recv ( unsigned char* , const int ) const;


	//==================================================
	// Stream 
	const CPSocket& operator << ( const std::string& ) const;
	const CPSocket& operator << ( const int& ) const;
	const CPSocket& operator << ( const float& ) const;
	const CPSocket& operator << ( const char* & ) const;

	const CPSocket& operator >> ( std::string& ) const;
	const CPSocket& operator >> ( int& ) const;
	const CPSocket& operator >> ( float& ) const;
	const CPSocket& operator >> ( char* & ) const;

	void set_non_blocking ( const bool );

	bool is_valid() const { return m_sock != INVALID_SOCKET; }

private:

	Socket_t m_sock;
	sockaddr_in m_addr;
	
	bool m_block_wait;
	time_t m_block_wait_secs;
	suseconds_t m_block_wait_usecs;

	/**
	 * Check to see if we have a close - EOF character
	**/
	bool check_close();

};

// SocketException class


class CPSocketException
{
 public:
  CPSocketException ( std::string s ) : m_s ( s ) {};
  ~CPSocketException (){};

  std::string description() { return m_s; }

 private:

  std::string m_s;

};


#endif // _CPSOCKET_H_

