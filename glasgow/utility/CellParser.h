/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CellParser - Spreadsheet cell entry parser
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CellParser.h,v 1.1 2006/01/05 00:49:29 robinrowe Exp $
 */

#ifndef _CELL_PARSER_H_
#define _CELL_PARSER_H_

#include <list>
#include <string>

/**
 * CellParser parses string data as input into a CPDAG to determine what the string data represents.
 * The string may represent a numner (float/int), a raw string, or a more complex string representing
 * the name and parameters of a plugin to be called
 *
 * @todo describe syntax used
 */
class CellParser
{
	public:
		//-------------------------
		// Constructor / Destructor

		/**
		 * Construct a new CellParser
		 *
		 */
		CellParser();

		/**
		 * Destructor
		 */
		virtual ~CellParser();


		//--------
		// Parsing

		/**
		 * Parse the specified string
		 *
		 * @param the string to parse
		 * @return true if the parsing completed successfully, false otherwise
		 */
		bool Parse(const std::string& s);

		/**
		 * Parse the specified string starting at offset for length characters
		 *
		 * @param the string to parse
		 * @param offset the offset within s to start parsing
		 * @param length the number of characters from offset to parse
		 * @return true if the parsing completed successfully, false otherwise
		 */
		bool Parse(const std::string& s, std::string::size_type offset, std::string::size_type length);

		/**
		 * Parse the specified string
		 *
		 * @param the string to parse
		 * @return true if the parsing completed successfully, false otherwise
		 */
		bool Parse(const char* s);

		/**
		 * Parse the specified string starting at offset for length characters
		 *
		 * @param the string to parse
		 * @param offset the offset within s to start parsing
		 * @param length the number of characters from offset to parse
		 * @return true if the parsing completed successfully, false otherwise
		 */
		bool Parse(const char* s, size_t offset, size_t length);

		/**
		 * Returns true if this CellParser contains parsed data.
		 * This CellParser will contain parsed data if Parse has been called and returned
		 * successfully.
		 *
		 * @return true if this CellParser contains parsed data, false otherwise
		 */
		bool IsParsed() const;

		/**
		 * Resets the state of this CellParser to an unparsed state, ready for a call to Parse
		 * The CellParser maintains some internal data after a call to Parse, a second string should
		 * therefore not be parsed until this CellParser has been Reset
		 *
		 */
		void Reset();




		//----------------------
		// Parsed Data Accessors

		/** Defined cell type data - the types that this parser can recognise */
		enum CellType
		{
		    NO_TYPE_ENUM,
		    INT_TYPE_ENUM,
		    FLOAT_TYPE_ENUM,
		    STRING_TYPE_ENUM,
		    PLUGIN_TYPE_ENUM,
		    UNKNOWN_TYPE_ENUM
		};

		/**
		 * Returns the data type of the parsed data
		 * 
		 * @return the data type of the parsed data
		 * @todo descrive syntax
		 */
		CellType GetDataType() const;


		/**
		 * Sets i to the int parameter parsed from the input data
		 * This method is only useful if the parsed data type was INT_TYPE_ENUM, in which case
		 * the reference parameter i, is set to the parsed int value, and this method returns true.
		 * Otherwise false is returned and i is untouched.
		 *
		 * @param i set to the parsed int value
		 * @return true if the parsed data type was INT_TYPE_ENUM
		 */
		bool GetInt(int& i) const;

		/**
		 * Sets f to the float parameter parsed from the input data
		 * This method is only useful if the parsed data type was FLOAT_TYPE_ENUM, in which case
		 * the reference parameter f, is set to the parsed float value, and this method returns true.
		 * Otherwise false is returned and f is untouched.
		 *
		 * @param f set to the parsed float value
		 * @return true if the parsed data type was FLOAT_TYPE_ENUM
		 */
		bool GetFloat(float& f) const;

		/**
		 * Sets s to the string parameter parsed from the input data
		 * This method is only useful if the parsed data type was STRING_TYPE_ENUM, in which case
		 * the reference parameter s is set to the parsed string value, and this method returns true.
		 * Otherwise false is returned and s is untouched.
		 *
		 * @param s set to the parsed string value
		 * @return true if the parsed data type was STRING_TYPE_ENUM
		 */
		bool GetString(std::string& s) const;




		/**
		 * Structure defining parsed parameter data for a plugin
		 *
		 */
		struct PluginParamData
		{
			std::string m_param_name;
			std::string m_evaluation_name;
			std::string m_param_cell;
			std::string m_param_text;
		};


		/**
		 * Structure representing parsed plugin information
		 *
		 */
		class PluginData
		{
			private:
				typedef std::list<PluginParamData> PluginParamList;
				friend class CellParser;

				std::string m_plugin_name;
				PluginParamList m_param_list;

			public:
				const std::string& GetPluginName() const;
				size_t GetParamCount() const;
				typedef PluginParamList::const_iterator const_iterator;

				const_iterator param_begin() const;
				const_iterator param_end() const;

			protected:

		};

		/**
		 * Returns parsed plugin data.
		 * This method is useful only if the parsed data type is PLUGIN_TYPE_ENUM, in which case a valid
		 * PluginData is returned detailing the plugin.
		 * The PluginData remains owned by this CellParser and should not be released by the user.
		 * If the parsed data type is not PLUGIN_TYPE_ENUM, this method returns 0
		 *
		 * @return a PluginData representing the parsed plugin data
		 */
		const PluginData* GetPluginData() const;


		/**
		 * Convenience method to strip whitespace from the specified string
		 *
		 * @param s the string from which to strip white space
		 * @param front set true to remove white space from the start of the string
		 * @param end set true to strip whitespace from the end of the string
		 * @return the string parameter with white space removed
		 */
		static std::string& StripWhiteSpace(std::string& s, bool front, bool end);

		/**
		 * Extract the contents contained within parenthesis of s
		 * The exxtracted string is set within extracted, and paren_depth is set to the depth of
		 * of the parenthesis. If there is only one set of parenthesis, cell(A1), paren_depth will be
		 * set to 1.
		 * The offset and length parameter control the offset and length from the offset of the specfiedi
		 * string to process
		 * If the specified string contains mismatched brackets, this method will return false and
		 * extracted will be untouched.
		 *
		 * @param s the string to extract from
		 * @param extracted set the the extracted value
		 * @param paren_depth set to the depth of the parenthesis
		 * @param offset offset from the start of s to process from
		 * @param length the length of s to process from offset
		 * @return true if data was extracted from s, false otherwise
		 */
		static bool ExtractFromParenthesis(const std::string s, std::string& extracted, int& paren_depth, std::string::size_type offset, std::string::size_type length);

		/**
		 * Extract the contents contained within parenthesis of s
		 * This method searches for the specified prefixx within s, and extracts the contents contained within
		 * paramethesis after prefix. For example given s as cell(A2), and orefix as cell, the extracted
		 * results would be set to A2.
		 * The param_depth parameter indicates the depth of paraenthesis expected. For example, cell(A2) would
		 * give a parenthesis depth of 1, if this is not equal to param_depth, the extracted string is
		 * deemed to contain unexpxected data, therefore false is returned. 
		 * 
		 * @param s the string to extract from
		 * @param prefix the string string prefix indicating the required component to begin extraction
		 * @param extracted set the the extracted value
		 * @param paren_depth indicates the depth of the parenthesis expected
		 * @param offset offset from the start of s to process from
		 * @param length the length of s to process from offset
		 * @return true if data was extracted from s, false otherwise
		 */
		static bool ExtractFromParenthesis(const std::string s, const std::string& prefix, std::string& extracted, int paren_depth, std::string::size_type offset, std::string::size_type length);


		/**
		 * Returns true if the specified string specifies a cell range parameter
		 * A cell range parameter begins with the word 'cell' with the range in
		 * parenthesis, ie. cell(A1). This method determines if the work cell
		 * is present followed by an opening parenthesis
		 *
		 * @param s the string to check
		 * @return true if the string contains a cell range parameter
		 * @todo improve how this works ...
		 */
		static bool IsCellParam(const std::string& s);




		//-----------
		// constants

		/** cell specifier name */
		static const std::string CELL_PARAM_NAME;

		/** divider between cell value and evaluation name */
		static const std::string CELL_EVALUATION_NAME;

	protected:

	private:

		/**
		 * Determines the data type of the specified string
		 *
		 * @param s the string for which to determine the data type
		 * @param offset offset from s to start porocessing
		 * @return the parsed data type of s
		 */
		CellType get_data_type(const std::string& s, int& offset) const;

		/**
		 * Parse the specified plugn string storing plugin details within the specified PluginData object
		 *
		 * @param s the plugin string to parse
		 * @param plugin_data used to store the parsed plugin data
		 * @return true if the parseing completed sucessfully, false otherwise
		 */
		bool parse_plugin(const std::string& s, PluginData& plugin_data);

		/**
		 * Breaks the specified params string into individual parameter definitions.
		 * The definitions are separated by the comma ',' character. Each parameter definition is
		 * added to the param_list as a separate string
		 *
		 * @param param_list list to which separated param definitions are added
		 * @param params string containing the parameter definitions
		 * @return the param_list with individual param elements added
		 */
		std::list<std::string>& separate_plugin_param_strings(std::list<std::string>& param_list, const std::string& params);

		/**
		 * Parse the specified params string into the componente parameters to a plugin
		 * The componente parameter data is set within the specified plugin_data object
		 *
		 * @param params the params string to parse
		 * @param plugin_data used to hold the plugin parameter data
		 * @return true if the parameter string was parsed successfully, false otherwise
		 */
		bool parse_plugin_params(const std::string& params, PluginData& plugin_data);





		//--------
		// Members

		/** hold parsed plugin data */
		PluginData* m_plugin_data;

		/** indicates if this CellParser holds valid parsed data */
		bool m_parsed;

		/** offset to the start of the raw data, offsetting whitespace and preceeding data type information */
		int m_data_offset;

		/** parsed data type of the input string */
		CellType m_cell_type;

		/** the complete original parse string - unparsed */
		std::string m_parse_string;



}; /* class SheetViewWindow */

#endif /* _CELL_PARSER_H_ */
