/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Util Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CPRect.h,v 1.1 2006/01/05 00:49:29 robinrowe Exp $
 */

#ifndef _CINEPAINT_RECTANGLE_H_
#define _CINEPAINT_RECTANGLE_H_

class CPRect
{
	public:
		CPRect();
		CPRect(int x, int y, int w, int h);
		virtual ~CPRect();

		int GetX() const;
		void SetX(int x);

		int GetY() const;
		void SetY(int y);

		int GetWidth() const;
		void SetWidth(int width);

		int GetHeight() const;
		void SetHeight(int height);

		void SetRect(const CPRect& rect);
		void SetRect(int x, int y, int w, int h);
		void Add(int x, int y, int w, int h);
		void Add(const CPRect& rect);
		bool Contains(int x, int y, int w, int h) const;
		bool Contains(const CPRect& rect) const;

		bool IsEmpty() const;

		CPRect& Intersect(const CPRect& src2, CPRect& dst) const;
		bool Intersects(const CPRect& rect) const;

	protected:

	private:

		int m_x;
		int m_y;
		int m_width;
		int m_height;

}
; /* class CPRect */

#endif /* _CINEPAINT_RECTANGLE_H_ */
