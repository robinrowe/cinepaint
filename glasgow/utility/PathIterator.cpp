/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Resource path iterator
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: PathIterator.cpp,v 1.4 2006/12/18 08:21:25 robinrowe Exp $
 */
//#error UNUSED

#include "PathIterator.h"

#include <string>

PathIterator::PathIterator(const char* p)
{
	parse_path(p);
	path =0;
}

PathIterator::~PathIterator()
{
}

void PathIterator::parse_path(const char* path)
{
	// Divide path into segments and add too list.
	const char* curr;
	if(NULL == path || '\0' == path[0] || strlen(path) == 0) {
		return;
	}
	bool done = false;
	//std::string *str = new std::string;
	std::string str = "";
	curr = path;
	while(!done)
	{

		if ( (*curr == ':'  && str.length() > 1) || *curr == 0)
		{
			m_pathList.push_back(str);
			str = "";
		}
		else
            str += *curr;
		if (*curr == 0 )
			done = true;
		else
            curr++;
	}
}

const char* PathIterator::begin()
{
	if (m_pathList.size() > 0 )
	{
		m_pathIter = m_pathList.begin();
		path = m_pathIter->c_str();
		return m_pathIter->c_str();
	}
	return 0;
}

const char* PathIterator::end()
{
	path = 0;
	return 0; //m_pathList.end()->c_str();
}

PathIterator &PathIterator::operator++()
{
	m_pathIter++;
	if (m_pathIter == m_pathList.end() )
		path = 0;
	else
        path = m_pathIter->c_str();
	return *this;
}
