/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ServerSocket - Client/Server execution module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * Based on Linux Socket Programming In C++ By Rob Tougher from LinuxGazzete
 *
 * $Id
 */

#ifndef _SERVER_SOCKET_H_
#define _SERVER_SOCKET_H_

#include "CPSocket.h"

class ServerSocket : public CPSocket
{

public:
	ServerSocket( int port);
	ServerSocket();

	void Open( int port );

	virtual ~ServerSocket();
 
	const ServerSocket& operator << ( const std::string& ) const;
	const ServerSocket& operator << ( const int& ) const;
	const ServerSocket& operator << ( const float& ) const;
	const ServerSocket& operator << ( const char* & ) const;

	const ServerSocket& operator >> ( std::string& ) const;
	const ServerSocket& operator >> ( int& ) const;
	const ServerSocket& operator >> ( float& ) const;
	const ServerSocket& operator >> ( char* & ) const;

	void accept ( ServerSocket& );
protected:

private:
	int m_port;

};


#endif // _SERVER_SOCKET_H_

