/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Resource File module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id
 *
 */

/** @file
 * Class Preferences header file
 */

#ifndef _H_PREFERENCES_
#define _H_PREFERENCES_

#include <map>
#include <set>
#include <string>

// #include "MiniParser.h"
class MiniParser;
#ifdef CORE_EXPORTS
#define DLL_API __declspec( dllexport )
#else
#define DLL_API __declspec( dllimport )
#endif

using namespace std;

/**
 * A class to represent "preferences" files.
 * Many applications store user preferences and application settings in files.
 * This class is used to store, alter and access preferences and contains the
 * functionality to read and write preference files to disk. The file format
 * used is described in preferences_format.txt
 */

class Preferences {

public:

//// FUNCTIONS ////

	/**
	 * Default constructor.
	 * Creates a Preferences object unassociated with any file.
	 */
	Preferences();

	/**
	 * Constructor.
	 * Creates a Preferences object associated with a particular file, but does
	 * not load preferences from that file.
	 * @param filename	[i] path and name of the preferences file
	 */
	Preferences(const string filename);

	/**
	 * Destructor.
	 */
	virtual ~Preferences();

	/**
	 * Get the path and filename of a Preferences object.
	 * An accessor function for retrieving the currently set filename (including
	 * path) of a Preferences object.
	 * @return	the current filename
	 */
	virtual string Filename(void) const;

	/**
	 * Set the path and filename of a Preferences object.
	 * A mutator function for setting the current filename (including path) of a
	 * Preferences object.
	 * @param filename	[i] path and name of the preferences file
	 */
	virtual void Filename(const string filename);

	/**
	 * Tests whether a preference with the given key exists.
	 * @param key	[i] the preference key to test
	 * @return	true if the preference is present, else false
	 */
	virtual bool PreferenceExists(const string key) const;

	/**
	 * Get the string value associated with a given key.
	 * An accessor function for retrieving the value associated with the
	 * supplied key as a string. The retrieved value is \e undefined
	 * if the \c key does not exist, or the value is not a string.
	 * @param key	[i] the key string
	 * @param value	[o] the retrieved preference value
	 * @return	true if the key was found and the value could be retrieved in
	 * 			the requested format, else false
	 */
	virtual bool GetString(const string key, string &value) const;

	/**
	 * Get the boolean value associated with a given key.
	 * An accessor function for retrieving the boolean value associated with
	 * the supplied key. The retrieved value is \e undefined if the \c key does
	 * not exist, or the value is not a recognised boolean.
	 * @param key	[i] the key string
	 * @param value	[o] the retrieved preference value
	 * @return	true if the key was found and the value could be retrieved in
	 * 			the requested format, else false
	 */
	virtual bool GetBool(const string key, bool &value) const;

	/**
	 * Get the integer value associated with a given key.
	 * An accessor function for retrieving the integer value associated with
	 * the supplied key. The retrieved value is \e undefined if the \c key does
	 * not exist, or the value is not an integer.
	 * @param key	[i] the key string
	 * @param value	[o] the retrieved preference value
	 * @return	true if the key was found and the value could be retrieved in
	 * 			the requested format, else false
	 */
	virtual bool GetInt(const string key, int &value) const;

	/**
	 * Get the double value associated with a given key.
	 * An accessor function for retrieving the double value associated with
	 * the supplied key. The retrieved value is \e undefined if the \c key does
	 * not exist, or the value is not a double.
	 * @param key	[i] the key string
	 * @param value	[o] the retrieved preference value
	 * @return	true if the key was found and the value could be retrieved in
	 * 			the requested format, else false
	 */
	virtual bool GetDouble(const string key, double &value) const;

	/**
	 * Expands any variables present within a string using the current
	 * preferences.
	 * @param value		[i] the string to expand
	 * @param expanded	[o] the string in which to place the expanded result
	 * @return	true if all the variables were expanded, else false
	 */
	virtual bool SubstituteVariables(const string &value, string &expanded) const;


	/**
	 * Set the string value associated with a given key.
	 * A mutator function for setting the string value associated with the
	 * supplied key.
	 * @param key	[i] the key string
	 * @param value	[i] the value string
	 */
	virtual void Set(const string key, const string value);

	/**
	 * Set the string value associated with a given key.
	 * A mutator function for setting the string value associated with the
	 * supplied key.
	 * @param key	[i] the key string
	 * @param value	[i] the value string
	 */
	virtual void Set(const string key, const char* value);

	/**
	 * Set the boolean value associated with a given key.
	 * A mutator function for setting the boolean value associated with the
	 * supplied key.
	 * @param key	[i] the key string
	 * @param value	[i] the boolean value
	 */
	virtual void Set(const string key, const bool value);

	/**
	 * Set the integer value associated with a given key.
	 * A mutator function for setting the integer value associated with the
	 * supplied key.
	 * @param key	[i] the key string
	 * @param value	[i] the integer value
	 */
	virtual void Set(const string key, const int value);

	/**
	 * Set the double value associated with a given key.
	 * A mutator function for setting the double value associated with the
	 * supplied key.
	 * @param key	[i] the key string
	 * @param value	[i] the double value
	 */
	virtual void Set(const string key, const double value);

	/**
	 * Remove a preference with the given key from the store.
	 * @param key	[i] the name of the preference to remove
	 */
	virtual void Erase(const string key);

	/**
	 * Get the list of keys for a Preferences object.
	 * Return a vector of strings representing the currently stored keys.
	 * @param store	[o] set for putting key names in
	 */
	virtual void Keys(std::set<string> &store) const;

	/**
	 * Load preferences from the currently associated file.
	 * Parses the file currently associated with a Preferences object and sets
	 * key/value pairs from it. If no file is currently associated, does nothing
	 * and returns false.
	 * @return	true if parse was successful, false otherwise
	 */
	virtual bool Load(void);
	
	/**
	 * Load preferences from the given file.
	 * Parses the file specified by the given filename and sets key/value pairs
	 * in a Preferences object from it. Also sets the path and filename of the
	 * Preferences object. If the file does not exist, does nothing.
	 * @param filename	[i] path and name of the preferences file
	 * @return	true if parse was successful, false otherwise
	 */
	virtual bool Load(const string filename);

	/**
	 * Save preferences to the currently associated file.
	 * Saves all the key/value preferences to the file currently associated with
	 * a Preferences object. If the file is a pre-existing preferences file,
  	 * save will attempt to write the preferences in place without disturbing
	 * surrounding text, appending any new preferences to the end. If no file is
	 * currently associated, does nothing and returns false.
	 * @return	true if save was successful, false otherwise
	 */
	virtual bool Save(void);

	/**
	 * Save preferences to the given file.
	 * Saves all the key/value preferences of a Preferences object to the file
	 * specified by the given filename. If the file is a pre-existing
	 * preferences file, save will attempt to write the preferences in place
	 * without disturbing surrounding text, appending any new preferences to the
	 * end. Also sets the path and filename of the Preferences object. If no
	 * file is currently associated, does nothing and returns false.
	 * @param filename	[i] path and name of the preferences file
	 * @return	true if save was successful, false otherwise
	 */
	virtual bool Save(const string filename);

	/**
	 * Save preferences to the currently associated file, overwriting the file
	 * in the process.
	 * Saves all the key/value preferences to the file currently associated with
	 * a Preferences object. If the file already exists it is erased and
	 * overwritten. If no file is currently associated, does nothing and returns
	 * false.
	 * @return	true if overwrite was successful, false otherwise
	 */
	virtual bool Overwrite(void);

	/**
	 * Save preferences to the given file, overwriting the file in the process.
	 * Saves all the key/value preferences of a Preferences object to the file
	 * specified by the given filename. If the file already exists it is erased
	 * and overwritten. Also sets the path and filename of the Preferences
	 * object. If no file is currently associated, does nothing and returns
	 * false.
	 * @param filename	[i] path and name of the preferences file
	 * @return	true if overwrite was successful, false otherwise
	 */
	virtual bool Overwrite(const string filename);
	
	/**
	 * Copy the preferences file on disk.
	 * This does not take account of any changes to preferences since they were
	 * last loaded or saved. It is a backup mechanism for the actual preferences
	 * file, allowing it to be restored manually if required. If the filename
  	 * supplied refers to a file that already exists, that file will be
     * overwritten. If no filename is supplied then one will be generated.
	 * @param filename	[i] the name of the backup file
	 * @return	the name of the file written to, or an empty string if the
	 *			backup failed
	 */
	virtual string Backup(string filename = "");

	/**
	 * Indicates whether any preferences have changed since the last load or
	 * save to disk.
	 * @return	true if any preferences have changed
	 */
	virtual bool HasChanged(void) const;

	/**
	 * Dump all the preferences to stdout, one per line.
	 */
	void Dump(void);


protected:

//// MEMBERS ////

	//@{
	/**
	 * Special symbols used in RC files.
	 */
	static const string mc_RCComment;
	static const string mc_RCOpenPref;
	static const string mc_RCClosePref;
	static const string mc_RCString;
	static const string mc_RCSpecials;
	static const string mc_RCVariable;
	static const string mc_RCOpenVar;
	static const string mc_RCCloseVar;
	static const string mc_RCEscape;
	//@}

	/**
	 * The array of strings that can be used to represent boolean "true". The
	 * array is terminated by an empty string to allow easy traversal.
	 */
	static const string mc_TrueValues[];

	/**
	 * The array of strings that can be used to represent boolean "false". The
	 * array is terminated by an empty string to allow easy traversal.
	 */
	static const string mc_FalseValues[];

	/**
	 * The store of key/value pairs
	 */
	map<const string, string> m_prefs;

	/**
	 * Indicates whether any preferences have changed since the last save
	 */
	bool m_hasChanged;
	
	/**
	 * The path and name of the file associated with the set of preferences
	 */
	string m_filename;


//// FUNCTIONS ////

	/**
	 * Get the preference value associated with a given key.
	 * An accessor function for retrieving the value associated with the
	 * supplied key in a Preferences object. The returned value is \e undefined
	 * if the \c key does not exist. #get is intended to be used as the basis
	 * for more specific \c Get routines such as #GetString and #GetInt. The
	 * value it returns is unadulterated from the value given in the preferences
	 * file. A string for example will be returned \e with quotes still around
	 * it, whereas #GetString removes the quotes. Essentially everything after
	 * the key and before the closing brace of a tuple, excluding whitespace
	 * immmediately following the key, is considered to be the value.
	 * @param key	[i] the key
	 * @param value	[o] the retrieved preference value
	 * @return	true if the key was found and the value could be retrieved in
	 * 			the requested format, else false
	 */
	virtual bool get(const string &key, string &value) const;

	/**
	 * Set the value associated with a given key.
	 * A mutator function for setting the value associated with the supplied key
	 * in a Preferences object. #set is intended to be used as the basis for more
	 * specific #Set routines taking an integer or boolean etc as a value.
	 * @param key	[i] the key
	 * @param value	[i] the associated value
	 */
	virtual void set(const string &key, const string &value);

	/**
	 * Tests whether a given string is present in an array of strings. The array
	 * must be terminated by an empty string. All string comparisons are
	 * \e case-insensitive
	 * @param s		[i] the string to test for membership
	 * @param array	[i] the array to search
	 * @return	true if the string is in the array, else false
	 */
	static bool is_string_in(const string &s, const string *array);

	/**
	 * Constructs the filename of a backup file based on the current filename.
	 * The returned C-string is created with \c malloc and the caller is
	 * responsible for deleting it with \c free.
	 * @return the filename of the backup file
	 */
	virtual string get_backup_filename(void) const;


private:

	/**
	 * Helper function for parsing tuples after the opening bracket is parsed
	 * @param parser	[i] the parser object being used to parse the prefs file
	 * @return	true if a properly formatted tuple was parsed, else false
	 */
	bool parse_tuple(MiniParser &parser);


}; // class Preferences


/**
 * A shorthand pointer to Preferences type
 */
typedef Preferences *pPreferences;


#endif // _H_PREFERENCES_


