/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CellParser - Spreadsheet cell entry parser
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CellParser.cpp,v 1.1 2006/01/05 00:49:29 robinrowe Exp $
 */

#include "CellParser.h"
#include <algorithm>
#include <cctype>
#include <list>

const std::string CellParser::CELL_PARAM_NAME = "cell";
const std::string CellParser::CELL_EVALUATION_NAME = ":";


//-------------------------
// Constructor / Destructor

/**
 * Construct a new CellParser
 *
 */
CellParser::CellParser()
{
	m_plugin_data = 0;
	Reset();
}

/**
 * Destructor
 */
CellParser::~CellParser()
{
	if(m_plugin_data)
	{
		delete m_plugin_data;
		m_plugin_data = 0;
	}
}


//--------
// Parsing

/**
 * Parse the specified string
 *
 * @param the string to parse
 * @return true if the parsing completed successfully, false otherwise
 */
bool CellParser::Parse(const std::string& s)
{
	return(Parse(s, 0, s.length()));
}

/**
 * Parse the specified string starting at offset for length characters
 *
 * @param the string to parse
 * @param offset the offset within s to start parsing
 * @param length the number of characters from offset to parse
 * @return true if the parsing completed successfully, false otherwise
 */
bool CellParser::Parse(const std::string& s, std::string::size_type offset, std::string::size_type length)
{
	if(IsParsed())
	{
		Reset();
	}

	int type_offset = 0;
	m_parse_string = s.substr(offset, length);
	StripWhiteSpace(m_parse_string, true, true);

	m_cell_type = get_data_type(m_parse_string, type_offset);
	m_data_offset = type_offset;

	switch(m_cell_type)
	{
			case INT_TYPE_ENUM:
			case FLOAT_TYPE_ENUM:
			case STRING_TYPE_ENUM:
			{
				m_parsed = true;
				break;
			}
			case PLUGIN_TYPE_ENUM:
			{
				m_plugin_data = new PluginData();
				m_parsed = parse_plugin(m_parse_string.substr(m_data_offset, m_parse_string.length() - m_data_offset), *m_plugin_data);
				break;
			}
			case NO_TYPE_ENUM:
			{
				// no data - we did parse nothing correctly...
				m_parsed = true;
				break;
			}
			case UNKNOWN_TYPE_ENUM:
			default:
			{
				m_parsed = false;
			}
	}

	return(m_parsed);
}


/**
 * Parse the specified string
 *
 * @param the string to parse
 * @return true if the parsing completed successfully, false otherwise
 */
bool CellParser::Parse(const char* s)
{
	std::string str(s);
	return(Parse(str));
}

/**
 * Parse the specified string starting at offset for length characters
 *
 * @param the string to parse
 * @param offset the offset within s to start parsing
 * @param length the number of characters from offset to parse
 * @return true if the parsing completed successfully, false otherwise
 */
bool CellParser::Parse(const char* s, size_t offset, size_t length)
{
	std::string str(s);
	return(Parse(str, offset, length));
}

/**
 * Returns true if this CellParser contains parsed data.
 * This CellParser will contain parsed data if Parse has been called and returned
 * successfully.
 *
 * @return true if this CellParser contains parsed data, false otherwise
 */
bool CellParser::IsParsed() const
{
	return(m_parsed);
}

/**
 * Resets the state of this CellParser to an unparsed state, ready for a call to Parse
 * The CellParser maintains some internal data after a call to Parse, a second string should
 * therefore not be parsed until this CellParser has been Reset
 *
 */
void CellParser::Reset()
{
	m_parsed = false;
	m_cell_type = NO_TYPE_ENUM;
	m_parse_string.clear();
	m_data_offset = 0;

	if(m_plugin_data)
	{
		delete m_plugin_data;
		m_plugin_data = 0;
	}
}



/**
 * Returns the data type of the parsed data
 * 
 * @return the data type of the parsed data
 * @todo descrive syntax
 */
CellParser::CellType CellParser::GetDataType() const
{
	return(m_cell_type);
}


/**
 * Sets i to the int parameter parsed from the input data
 * This method is only useful if the parsed data type was INT_TYPE_ENUM, in which case
 * the reference parameter i, is set to the parsed int value, and this method returns true.
 * Otherwise false is returned and i is untouched.
 *
 * @param i set to the parsed int value
 * @return true if the parsed data type was INT_TYPE_ENUM
 */
bool CellParser::GetInt(int& i) const
{
	bool ret = false;

	if(IsParsed() && m_cell_type == INT_TYPE_ENUM)
	{
		std::string s = m_parse_string.substr(m_data_offset, m_parse_string.length() - m_data_offset);
		i = atoi(s.c_str());

		ret = true;
	}

	return(ret);
}

/**
 * Sets f to the float parameter parsed from the input data
 * This method is only useful if the parsed data type was FLOAT_TYPE_ENUM, in which case
 * the reference parameter f, is set to the parsed float value, and this method returns true.
 * Otherwise false is returned and f is untouched.
 *
 * @param f set to the parsed float value
 * @return true if the parsed data type was FLOAT_TYPE_ENUM
 */
bool CellParser::GetFloat(float& f) const
{
	bool ret = false;

	if(IsParsed() && m_cell_type == FLOAT_TYPE_ENUM)
	{
		std::string s = m_parse_string.substr(m_data_offset, m_parse_string.length() - m_data_offset);
		f = (float)atof(s.c_str());

		ret = true;
	}

	return(ret);
}

/**
 * Sets s to the string parameter parsed from the input data
 * This method is only useful if the parsed data type was STRING_TYPE_ENUM, in which case
 * the reference parameter s is set to the parsed string value, and this method returns true.
 * Otherwise false is returned and s is untouched.
 *
 * @param s set to the parsed string value
 * @return true if the parsed data type was STRING_TYPE_ENUM
 */
bool CellParser::GetString(std::string& s) const
{
	bool ret = false;

	if(IsParsed() && m_cell_type == STRING_TYPE_ENUM)
	{
		s = m_parse_string.substr(m_data_offset, m_parse_string.length() - m_data_offset);
		ret = true;
	}

	return(ret);
}


/**
 * Returns parsed plugin data.
 * This method is useful only if the parsed data type is PLUGIN_TYPE_ENUM, in which case a valid
 * PluginData is returned detailing the plugin.
 * The PluginData remains owned by this CellParser and should not be released by the user.
 * If the parsed data type is not PLUGIN_TYPE_ENUM, this method returns 0
 *
 * @return a PluginData representing the parsed plugin data
 */
const CellParser::PluginData* CellParser::GetPluginData() const
{
	CellParser::PluginData* ret = 0;

	if(IsParsed() && m_cell_type == PLUGIN_TYPE_ENUM)
	{
		ret = m_plugin_data;
	}

	return(ret);
}


/**
 * Convenience method to strip whitespace from the specified string
 *
 * @param s the string from which to strip white space
 * @param front set true to remove white space from the start of the string
 * @param end set true to strip whitespace from the end of the string
 * @return the string parameter with white space removed
 */
std::string& CellParser::StripWhiteSpace(std::string& s, bool front, bool end)
{
	if(front)
	{
		// reomve white space from the front
		while((!s.empty()) && (isspace(s[0])))
		{
			s.erase(0, 1);
		}
	}

	if(end)
	{
		while((!s.empty()) && isspace(s[s.length() -1]))
		{
			s.erase(s.length() -1, 1);
		}
	}

	return(s);
}

/**
 * Extract the contents contained within parenthesis of s
 * The exxtracted string is set within extracted, and paren_depth is set to the depth of
 * of the parenthesis. If there is only one set of parenthesis, cell(A1), paren_depth will be
 * set to 1.
 * The offset and length parameter control the offset and length from the offset of the specfiedi
 * string to process
 * If the specified string contains mismatched brackets, this method will return false and
 * extracted will be untouched.
 *
 * @param s the string to extract from
 * @param extracted set the the extracted value
 * @param paren_depth set to the depth of the parenthesis
 * @param offset offset from the start of s to process from
 * @param length the length of s to process from offset
 * @return true if data was extracted from s, false otherwise
 */
bool CellParser::ExtractFromParenthesis(const std::string s, std::string& extracted, int& paren_depth, std::string::size_type offset, std::string::size_type length)
{
	bool ret = false;

	//pos = offset;
	std::string::size_type pos = s.find('(', offset);
	if(pos != std::string::npos)
	{
		int	depth = 1;
		int count = 1;
		pos++;

		while((count > 0) && (pos < (offset + length)) && (pos < s.length()))
		{
			if(s[pos] == '(')
			{
				count++;
				depth++;
			}
			else if(s[pos] == ')')
			{
				count--;
			}

			pos++;
		}

		if(count == 0)
		{
			// offset the open and close positions to cut the '(' and ')' chars
			extracted = s.substr(offset +1, (pos - offset) -2);
			paren_depth = depth;
			ret = true;
		}
	}

	return(ret);
}

/**
 * Extract the contents contained within parenthesis of s
 * This method searches for the specified prefixx within s, and extracts the contents contained within
 * paramethesis after prefix. For example given s as cell(A2), and orefix as cell, the extracted
 * results would be set to A2.
 * The param_depth parameter indicates the depth of paraenthesis expected. For example, cell(A2) would
 * give a parenthesis depth of 1, if this is not equal to param_depth, the extracted string is
 * deemed to contain unexpxected data, therefore false is returned. 
 * 
 * @param s the string to extract from
 * @param prefix the string string prefix indicating the required component to begin extraction
 * @param extracted set the the extracted value
 * @param paren_depth indicates the depth of the parenthesis expected
 * @param offset offset from the start of s to process from
 * @param length the length of s to process from offset
 * @return true if data was extracted from s, false otherwise
 */
bool CellParser::ExtractFromParenthesis(const std::string s, const std::string& prefix, std::string& extracted, int paren_depth, std::string::size_type offset, std::string::size_type length)
{
	bool ret = false;

	std::string s_upper(s);
	std::string prefix_upper(prefix);

	std::transform(s_upper.begin(), s_upper.end(), s_upper.begin(), (int(*)(int))std::toupper);
	std::transform(prefix_upper.begin(), prefix_upper.end(), prefix_upper.begin(), (int(*)(int))std::toupper);

	std::string::size_type pos = s_upper.find(prefix_upper, offset);
	std::string::size_type ext_pos = 0;
	if(pos != std::string::npos)
	{
		// the next character should be a '('
		ext_pos += prefix.length();
		bool done = false;
		bool found = false;
		while(((pos + ext_pos) < s.length()) && !done)
		{
			if(s[pos + ext_pos] == '(')
			{
				done = true;
				found = true;
			}
			else if(isspace(s[pos]))
			{
				ext_pos++;
			}
			else
			{
				done = true;
				found = false;
			}
		}

		if(found)
		{
			std::string tmp_extract;
			int depth = 0;

			if(ExtractFromParenthesis(s, tmp_extract, depth, pos + ext_pos, length - ext_pos))
			{
				if(depth == paren_depth)
				{
					extracted = tmp_extract;
					ret = true;
				}
			}

		}
	}

	return(ret);
}


/**
 * Returns true if the specified string specifies a cell range parameter
 * A cell range parameter begins with the word 'cell' with the range in
 * parenthesis, ie. cell(A1). This method determines if the work cell
 * is present followed by an opening parenthesis
 *
 * @param s the string to check
 * @return true if the string contains a cell range parameter
 * @todo improve how this works ...
 */
bool CellParser::IsCellParam(const std::string& s)
{
	bool ret = false;

	std::string param(s);
	std::string cell_param(CELL_PARAM_NAME);
	std::transform(param.begin(), param.end(), param.begin(), (int(*)(int))std::toupper);
	std::transform(cell_param.begin(), cell_param.end(), cell_param.begin(), (int(*)(int))std::toupper);

	if(param.find(cell_param) == 0)
	{
		// the next char should be '(', ignoring whitespace chars
		bool done = false;
		std::string::size_type pos = 4; // step over the 'cell' chars
		while(pos < s.length() && !done)
		{
			if(s[pos] == '(')
			{
				done = true;
				ret = true;
			}
			else if(isspace(s[pos]))
			{
				pos++;
			}
			else
			{
				done = true;
				ret = false;
			}
		}
	}

	return(ret);
}




/**
 * Determines the data type of the specified string
 *
 * @param s the string for which to determine the data type
 * @param offset offset from s to start porocessing
 * @return the parsed data type of s
 */
CellParser::CellType CellParser::get_data_type(const std::string& s, int& offset) const
{
	CellType ret = UNKNOWN_TYPE_ENUM;

	while(isspace(s[offset]))
	{
		offset++;
	}

	if(s.empty())
	{
		ret = NO_TYPE_ENUM;
	}
	else if(s[offset] == '=')
	{
		// if the first char is a '=' then its a plugin
		ret = PLUGIN_TYPE_ENUM;
		offset++;
	}
	else if(s[offset] == '`')
	{
		ret = STRING_TYPE_ENUM;
		offset++;
	}
	else
	{
		// process further
		// if all chars are a numerical, or contains a single '.' we have a number

		int point_count = 0;
		bool is_number = true;

		bool done = false;
		std::string::size_type pos = offset;
		while((pos < s.length()) && !done)
		{
			if(s[pos] == '.')
			{
				point_count++;
				if(point_count > 1)
				{
					is_number = false;
					done = true;
				}
			}
			else if((s[pos] == '-') && pos != 0)
			{
				// a minus sign at any position other than the front is not a number
				is_number = false;
				done = true;
			}
			else if(!isdigit(s[pos]))
			{
				is_number = false;
				done = true;
			}

			pos++;
		}

		if(is_number)
		{
			if(point_count == 0)
			{
				ret = INT_TYPE_ENUM;
			}
			else
			{
				ret = FLOAT_TYPE_ENUM;
			}
		}
		else
		{
			// otherwise assume it is raw text
			ret = STRING_TYPE_ENUM;
		}
	}

	return(ret);
}

/**
 * Parse the specified plugn string storing plugin details within the specified PluginData object
 *
 * @param s the plugin string to parse
 * @param plugin_data used to store the parsed plugin data
 * @return true if the parseing completed sucessfully, false otherwise
 */
bool CellParser::parse_plugin(const std::string& s, PluginData& plugin_data)
{
	bool parsed = false;

	// plugin name is the text up to the opening bracket
	// we assume that all plugins take a parameter for now so we
	// will always have a bracket if the plugin string is formed correctly
	std::string::size_type open_param_pos = s.find('(');

	if(open_param_pos != std::string::npos)
	{
		std::string plugin_name = s.substr(0, open_param_pos);
		StripWhiteSpace(plugin_name, true, true);

		plugin_data.m_plugin_name = plugin_name;

		int depth = 0;
		std::string params;
		if(ExtractFromParenthesis(s, params, depth, open_param_pos, s.length() - open_param_pos))
		{
			parsed = parse_plugin_params(params, plugin_data);
		}
	}

	return(parsed);
}

/**
 * Breaks the specified params string into individual parameter definitions.
 * The definitions are separated by the comma ',' character. Each parameter definition is
 * added to the param_list as a separate string
 *
 * @param param_list list to which separated param definitions are added
 * @param params string containing the parameter definitions
 * @return the param_list with individual param elements added
 */
std::list<std::string>& CellParser::separate_plugin_param_strings(std::list<std::string>& param_list, const std::string& params)
{
	std::string::size_type start_offset = 0;
	std::string::size_type end_offset = 0;

	// divide the params string into individual params, separating on the ','
	while(end_offset < params.length())
	{
		end_offset = params.find(',', start_offset);
		if(end_offset == std::string::npos)
		{
			end_offset = params.length();
		}

		std::string param = params.substr(start_offset, end_offset - start_offset);
		StripWhiteSpace(param, true, true);

		if(!param.empty())
		{
			param_list.push_back(param);
		}

		start_offset = end_offset;

		// step over the ',' char
		start_offset++;
	}

	return(param_list);
}

/**
 * Parse the specified params string into the componente parameters to a plugin
 * The componente parameter data is set within the specified plugin_data object
 *
 * @param params the params string to parse
 * @param plugin_data used to hold the plugin parameter data
 * @return true if the parameter string was parsed successfully, false otherwise
 */
bool CellParser::parse_plugin_params(const std::string& params, PluginData& plugin_data)
{
	bool ret = false;

	std::list<std::string> param_list;
	separate_plugin_param_strings(param_list, params);

	// parse the individual param elements
	if(!param_list.empty())
	{
		for(std::list<std::string>::const_iterator citer = param_list.begin(); citer != param_list.end(); ++citer)
		{
			// first, get the param name
			std::string param = *citer;
			std::string::size_type pos = param.find('=');

			if(pos != std::string::npos)
			{
				std::string param_name = param.substr(0, pos);
				StripWhiteSpace(param_name, true, true);

				// setp over the '=' char
				pos++;

				// now determine whether the parameter value comes from a cell or a is 'hard coded'
				std::string param_value = param.substr(pos, param.length() - pos);
				StripWhiteSpace(param_value, true, true);

				if(IsCellParam(param_value))
				{
					if(!param_name.empty() && !param_value.empty())
					{
						// extract the cell name and possible evaluation name
						PluginParamData param_data;

						std::string::size_type offset = 0;
						std::string param_cell;
						if(ExtractFromParenthesis(param_value, CELL_PARAM_NAME, param_cell, 1, offset, param_value.length() - offset))
						{
							param_data.m_param_cell = param_cell;
							param_data.m_param_name = param_name;
							param_data.m_param_text = param_value;

							offset += (param_data.m_param_cell.length()) + CELL_PARAM_NAME.length() +2;

							std::string evaluation_name;
							if(ExtractFromParenthesis(param_value, CELL_EVALUATION_NAME, evaluation_name, 1, offset, param_value.length() - offset))
							{
								param_data.m_evaluation_name = evaluation_name;
							}

							plugin_data.m_param_list.push_back(param_data);
							ret = true;

							printf("CellParser::parse_plugin_params: param name=%s param cell=%s eval name=%s  full text=%s\n", param_name.c_str(), param_data.m_param_cell.c_str(), evaluation_name.c_str(), param_value.c_str());
						}
					}
				}
				else
				{
					printf("TODO : support 'hard coded' plugin params!\n");
				}
			}
		}
	}

	return(ret);
}





//---------------
// Nested Classes

const std::string& CellParser::PluginData::GetPluginName() const
{
	return(m_plugin_name);
}

size_t CellParser::PluginData::GetParamCount() const
{
	return(m_param_list.size());
}

CellParser::PluginData::const_iterator CellParser::PluginData::param_begin() const
{
	return(m_param_list.begin());
}

CellParser::PluginData::const_iterator CellParser::PluginData::param_end() const
{
	return(m_param_list.end());
}
