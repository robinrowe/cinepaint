/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Util Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: Vector2d.cpp,v 1.1 2006/01/05 00:49:29 robinrowe Exp $
 */

#include "Vector2d.h"
#include <cmath>

/**
 * Default Constructor, initializes this Vector2d's x and y to zero
 *
 */
Vector2d::Vector2d() : m_x(0), m_y(0)
{
	// nothing else to do
}

/**
 * Constructs a new Vector2d with the specified values
 *
 * @param x the x value
 * @param y the y value
 */
Vector2d::Vector2d(double x, double y) : m_x(x), m_y(y)
{
	// nothing else to do
}

/**
 * Copy Constructor, copies the x and y values of v to this Vector2d
 *
 * @param v the Vector2d to copy
 */
Vector2d::Vector2d(const Vector2d& v)
{
	this->m_x = v.GetX() ;
	this->m_y = v.GetY() ;
}

/**
 * Destructor
 */
Vector2d::~Vector2d()
{
	// nothing to do
}



//---------------------
// accessors / mutatots

/**
 * Sets this Vector2d to the specified values
 *
 * @param x the x value
 * @param y the y value
 */
void Vector2d::Set(double x, double y)
{
	m_x = x ;
	m_y = y ;
}

/**
 * Returns the y value of this Vector2d
 *
 * @return the x value of this Vector2d
 */
double Vector2d::GetX() const
{
	return(m_x) ;
}

/**
 * Returns the y value of this Vector2d
 *
 * @return the y value of this Vector2d
 */
double Vector2d::GetY() const
{
	return(m_y) ;
}

/**
 * Returns the dot product of this Vector2d and the specified Vector2d
 *
 * @param v Vector2d to calculate the dot product with this Vector2d
 * @return the dot product of this vactor2d and the specified Vector2d
 */
double Vector2d::GetDotProduct(const Vector2d& v) const
{
	return(Vector2d::GetDotProduct(*this, v));
}

/**
 * Returns the magnitude of this Vector2d
 *
 * @return the magnitude of this Vector2d
 */
double Vector2d::GetMagnitude() const
{
	return(Vector2d::GetMagnitude(*this));
}


//---------------
// static methods

/**
 * Returns the dot product of v1 and v2
 *
 * @param v1 first Vector2d of the dot product calculation
 * @param v2 second Vector2d of the dot product calculation
 * @return the dot product of v1 and v2
 */
double Vector2d::GetDotProduct(const Vector2d& v1, const Vector2d& v2)
{
  return((v1.m_x * v2.m_x) + (v1.m_y * v2.m_y));
}

/**
 * Returns the magnitude of v
 *
 * @return the magnitude v
 */
double Vector2d::GetMagnitude(const Vector2d& v)
{
	return(std::sqrt((v.m_x * v.m_x) + (v.m_y * v.m_y)));
}
