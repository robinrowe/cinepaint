/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Some global enumerations.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Tom Baker
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 *
 * $Id: PlatformUtil.cpp,v 1.1 2006/01/05 00:49:29 robinrowe Exp $
 */

#include "PlatformUtil.h"

#ifdef WIN32

#include <windows.h>
//#define	snprintf _snprintf

// TB Make sure that this works correctly
//#define S_ISREG(m) (bool)((m) & _S_IFREG)


const char* PlatformUtil::CP_PATH_SEPARATOR = "\\" ;
const int PlatformUtil::CP_PATH_MAX = _MAX_PATH ;


void PlatformUtil::CpSleep(long millis)
{
	Sleep(millis) ;
}

#else

#include <time.h>
#include <limits.h>

const char* PlatformUtil::PlatformUtil::CP_PATH_SEPARATOR = "/" ;
const int PlatformUtil::CP_PATH_MAX = PATH_MAX ;

void PlatformUtil::CpSleep(long millis)
{
	struct timespec ts ;

	ts.tv_sec = millis / 1000L ;
	ts.tv_nsec= (millis % 1000L) * 1000000L ;

	nanosleep(&ts, 0) ;
}

#endif

