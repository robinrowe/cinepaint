/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Resource file module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id:
 *
 */

/** @file
 * Class Preferences source file
 */


#include <cstdio>
#include <cctype>
#include <map>
#include <string>

#include "MiniParser.h"
#include "Preferences.h"

using namespace std;


// utility for checking boolean expressions and returning if they are false
#define BVERIFY(exp)	if (!(exp)) return false;


//// STATIC MEMBERS ////

const string Preferences::mc_RCComment = "#";
const string Preferences::mc_RCOpenPref = "(";
const string Preferences::mc_RCClosePref = ")";
const string Preferences::mc_RCSpecials = " \t\n\r#()";
const string Preferences::mc_RCString = "\"";
const string Preferences::mc_RCVariable = "$";
const string Preferences::mc_RCOpenVar = "{";
const string Preferences::mc_RCCloseVar = "}";
const string Preferences::mc_RCEscape = "\\";

const string Preferences::mc_TrueValues[] = {"true", "on", "yes", "1", ""};
const string Preferences::mc_FalseValues[] = {"false", "off", "no", "0", ""};


//// METHODS ////


Preferences::Preferences()
			 : m_hasChanged(false), m_filename("") {
} // default constructor


Preferences::Preferences(const string filename)
			 : m_hasChanged(false), m_filename(filename) {
} // constructor


Preferences::~Preferences() {
} // destructor


// MACRO only used in Backup
#define BACKUPBUFFERSIZE 2048

string
Preferences::Backup(string filename) {

	char buffer[BACKUPBUFFERSIZE];
	FILE *pOriginal, *pBackup;
	size_t count;

	if (filename.empty()) {
		filename = get_backup_filename();
	}

	if ((pOriginal = fopen(m_filename.c_str(), "r")) == NULL) {
		return NULL;
	}
	if ((pBackup = fopen(filename.c_str(), "w")) == NULL) {
		return NULL;
	}

	while (!feof(pOriginal)) {
		count = fread(buffer, sizeof(char), BACKUPBUFFERSIZE, pOriginal);
		fwrite(buffer, sizeof(char), count, pBackup);
	}

	fclose(pOriginal);
	fclose(pBackup);

	return filename;

} // Backup


void
Preferences::Dump(void) {

	string value;
	std::set<string> keys;
	std::set<string>::iterator iter;

	Keys(keys);
	for (iter = keys.begin(); iter != keys.end(); iter++) {
		get(*iter, value);
		printf("%s = %s\n", (*iter).c_str(), value.c_str());
	}

} // Dump


void
Preferences::Erase(const string key) {

	m_prefs.erase(key);
	m_hasChanged = true;

} // Erase


bool
Preferences::SubstituteVariables(const string &value, string &expanded) const {

	unsigned int i, startIndex, endIndex;
	char previousChar;
	string key, var;
	bool inVariable, variableNotFound;

	startIndex = endIndex = i = 0;
	previousChar = '\0';
	inVariable = false;
	variableNotFound = true;
	expanded = value;

	i = 0;
	while (variableNotFound && (i < value.length())) {
		if (inVariable) {
      		if (value[i] == mc_RCCloseVar[0]) {
				endIndex = i;
            	variableNotFound = false;
			}
			else {
				key += value[i];
			}
		}
		// else if char is mc_RCOpenVar and the previous char was mc_RCVariable
		// then this is the start of a variable
		else if ((previousChar == mc_RCVariable[0]) &&
								(value[i] == mc_RCOpenVar[0])) {
			startIndex = i - 1;
			inVariable = true;
		}

		previousChar = value[i];
		i++;
	}

	if (variableNotFound) {
		return true;
	}

	if (m_prefs.find(key) == m_prefs.end()) {
		return false;
	}

	expanded.erase(startIndex, endIndex - startIndex + 1);
	var = m_prefs.find(key)->second;
	// if surrounded by mc_RCString then remove these chars
	if (var[0] == mc_RCString[0] && var[var.length()-1] == mc_RCString[0]) {
		expanded.insert(startIndex, var, 1, var.length()-2);
	}
	else {
		expanded.insert(startIndex, var);
	}

	return SubstituteVariables(expanded, expanded);

} // SubstituteVariables


string
Preferences::Filename(void) const {

	return m_filename;

} // Filename


void
Preferences::Filename(const string filename) {

	m_filename = filename;

} // Filename


bool
Preferences::get(const string &key, string &value) const {

	map<const string, string>::const_iterator it;

	it = m_prefs.find(key);
	if (it != m_prefs.end()) {
		SubstituteVariables(it->second, value);
		return true;
	}

	return false;

} // get


string
Preferences::get_backup_filename(void) const {

	return m_filename + ".old";

} // get_backup_filename


bool
Preferences::GetBool(const string key, bool &value) const {

	string s;

	if (!get(key, s)) {
		return false;
	}
	else if (is_string_in(s, mc_TrueValues)) {
		value = true;
		return true;
	}
	else if (is_string_in(s, mc_FalseValues)) {
		value = false;
		return true;
	}

	return false;

} // GetBool


bool
Preferences::GetDouble(const string key, double &value) const {

	string s;
	unsigned int i;
	bool isNumber;

	if (!get(key, s)) {
		return false;
	}

	i = 0;
	isNumber = true;
	while (isNumber && i < s.length()) {
		if (!(isdigit(s[i]) || (s[i] == '.'))) {
   			isNumber = false;
		}
		i++;
	}

	if (isNumber) {
		value = atof(s.c_str());
		return true;
	}

	return false;

} // GetDouble


bool
Preferences::GetInt(const string key, int &value) const {

	string s;
	unsigned int i;
	bool isNumber;

	if (!get(key, s)) {
		return false;
	}

	i = 0;
	isNumber = true;
	while (isNumber && i < s.length()) {
		if (!isdigit(s[i])) {
   			isNumber = false;
		}
		i++;
	}

	if (isNumber) {
		value = atoi(s.c_str());
		return true;
	}

	return false;

} // GetInt


bool
Preferences::GetString(const string key, string &value) const {

	if (!get(key, value)) {
		return false;
	}
	
	if (value.empty()) {
		return true;
	}

	if ((value[0] == mc_RCString[0]) &&
				(value[value.length()-1] == mc_RCString[0])) {
		// remove string delimiters (quotes usually)
		value = value.substr(1, value.length()-2);
	}

	return true;

} // GetString


bool
Preferences::HasChanged(void) const {

	return m_hasChanged;

} // HasChanged


bool
Preferences::is_string_in(const string &s, const string *array) {

	bool notFound;
	unsigned int i, j;
	size_t length;

	notFound = true;
	i = 0;
	while (notFound && !array[i].empty()) {
		length = s.length();
    	if (array[i].length() == length) {
			j = 0;
			while ((tolower(s[j]) != tolower(array[i][j])) && (j < length)) {
				j++;
        	}
        	if (j == length) {
				notFound = false;
			}
		}
		i++;
	}

	return !notFound;

} // is_string_in


void
Preferences::Keys(std::set<string> &store) const {

	map<const string, string>::const_iterator it;

	for (it = m_prefs.begin(); it != m_prefs.end(); it++) {
		store.insert(it->first);
	}

} // Keys


bool
Preferences::Load(void) {

	MiniParser parser(mc_RCSpecials);
	string symbol;

	if (!parser.Filename(m_filename)) {
		return false;
	}
	
	while (parser.Next(symbol)) {
		// if symbol is mc_RCComment then this is the start of a comment so
		// parse to end the of line and discard
		if (symbol == mc_RCComment) {
			BVERIFY(parser.ParseUpto("\n", symbol));	// won't work on MacOS 9 and under
			BVERIFY(parser.Next(symbol));		// eats up the end of line
		}
		// else if symbol is mc_RCOpenPref then this is the start of a tuple, so parse the
		// tuple and store it
		else if (symbol == mc_RCOpenPref) {
			BVERIFY(parse_tuple(parser));
		}
		// else if symbol is whitespace then ignore it
		else if (parser.IsWhitespace(symbol)) {
			// do nothing
		}
		// else we have illegal syntax so indicate an error
		else {
			return false;
		}
	}

	m_hasChanged = false;

	return true;

} // Load


bool
Preferences::Load(const string filename) {

	if (filename.empty()) {
		return false;
	}

	m_filename = filename;

	return Load();

} // Load


bool
Preferences::Overwrite(void) {

	string line;
	string oldfilename;
	FILE *pFile;
	map<const string, string>::iterator it;

	// open the file for writing
	if ((pFile = fopen(m_filename.c_str(), "w")) == NULL) {
		return false;
	}

	for (it = m_prefs.begin(); it != m_prefs.end(); it++) {
		line = mc_RCOpenPref + "%s %s" + mc_RCClosePref + "\n\n";
		fprintf(pFile, line.c_str(), it->first.c_str(), it->second.c_str());
	}

	fclose(pFile);

	m_hasChanged = false;
	
	return true;

} // Overwrite


bool
Preferences::Overwrite(const string filename) {

	if (filename.empty()) {
		return false;
	}

	m_filename = filename;

	return Overwrite();

} // Overwrite


bool
Preferences::parse_tuple(MiniParser &parser) {

	string key, value, symbol, special;
	bool inString;

	special = mc_RCClosePref + mc_RCEscape + mc_RCString;

	// at this point the opening brace of the tuple has been parsed
	// and we're now just inside the tuple
	// discard all following whitespace and try to parse key
	do {
		BVERIFY(parser.Next(symbol));
	} while (parser.IsWhitespace(symbol));
	if (parser.LastSymbolSpecial()) {
		return false;
	}
	key = symbol;

	// discard all following whitespace
	do {
		BVERIFY(parser.Next(symbol));
	} while (parser.IsWhitespace(symbol));

	// if symbol is mc_RCClosePref then assume boolean value = true
	if (symbol == mc_RCClosePref) {
		value = mc_TrueValues[0];
	}
	// if the symbol is some other special character the tuple is badly formed
	else if (parser.IsSpecial(symbol)) {
		return false;
	}
	// else value is the last read symbol plus everything upto mc_RCClosePref
	else {

		value = "";
		inString = (symbol == mc_RCString);
		while (symbol != mc_RCClosePref || inString) {

			value += symbol;
			BVERIFY(parser.ParseUpto(special, symbol));
			value += symbol;
			BVERIFY(parser.Next(symbol));

			if (symbol == mc_RCString) {
				inString = !inString;
			}
			// if escape char inside a string add the next char
			// to value but don't worry about what it is
//			else if (symbol == mc_RCEscape && inString) {
//				value += symbol;
//				BVERIFY(parser.Next(symbol));	// escaped char
//			}

		}

	}

	// insert the tuple in the preferences store
	m_prefs[key] = value;

	return true;

} // parse_tuple


bool
Preferences::PreferenceExists(const string key) const {

	return m_prefs.find(key) != m_prefs.end();

} // PreferenceExists


bool
Preferences::Save(void) {

	string symbol, line, symbol2;
	char tmpfilename[L_tmpnam];
	FILE *pFile;
	MiniParser parser(mc_RCSpecials);
	map<const string, string> oldPrefs;
	map<const string, string>::iterator it;

	// make temporary copy by renaming file
	if (rename(m_filename.c_str(), tmpnam(tmpfilename)) != 0) {
		return false;
	}

	// open the temporary file for parsing
	BVERIFY(parser.Filename(tmpfilename));

	// open the file for writing
	if ((pFile = fopen(m_filename.c_str(), "w")) == NULL) {
		return false;
	}

	while (parser.Next(symbol)) {
		// if symbol is mc_RCComment then this is the start of a comment so
		// parse to end the of line and copy over
		if (symbol == mc_RCComment) {
			BVERIFY(parser.ParseUpto("\n", symbol));
			fprintf(pFile, "%s%s\n", mc_RCComment.c_str(), symbol.c_str());
			BVERIFY(parser.Next(symbol));		// eats up the end of line
		}
		// else if symbol is mc_RCOpenPref then this is the start of a tuple, so parse the
		// tuple and copy it
		else if (symbol == mc_RCOpenPref) {
			BVERIFY(parser.Next(symbol)); 	// read the tuple key
    		// comment preference out if it is no longer in the preferences store
    		if ((it = m_prefs.find(symbol)) == m_prefs.end()) {
				BVERIFY(parser.ParseUpto(mc_RCClosePref, symbol2));
    			line = mc_RCComment + " " + mc_RCOpenPref
       								+ "%s%s" + mc_RCClosePref;
    			fprintf(pFile, line.c_str(),
       					symbol.c_str(), symbol2.c_str());
    		}
    		// else write out the preference with its new value
    		else {
    			line = mc_RCOpenPref + "%s %s" + mc_RCClosePref;
    			fprintf(pFile, line.c_str(), symbol.c_str(), m_prefs[symbol].c_str());
    			BVERIFY(parser.ParseUpto(mc_RCClosePref, symbol2)); // parse and discard value
    			BVERIFY(parser.Next(symbol2));	// parse and discard mc_RCClosePref

    			// add this pref to a list of old prefs so it is not written out
    			// when we come to write out brand new prefs later on
    			oldPrefs[symbol] = m_prefs[symbol];
    		}
		}
		// else if symbol is whitespace then copy it over
		else if (parser.IsWhitespace(symbol)) {
			fprintf(pFile, "%s", symbol.c_str());
		}
		// else we have illegal syntax so indicate an error
		else {
			return false;
		}
	}

	// write out brand new prefs at the end of the prefs file
	for (it = m_prefs.begin(); it != m_prefs.end(); it++) {
		if (oldPrefs.find(it->first) == oldPrefs.end()) {
			line = "\n" + mc_RCOpenPref + "%s %s" + mc_RCClosePref + "\n";
			fprintf(pFile, line.c_str(), it->first.c_str(), it->second.c_str());
		}
	}

	remove(tmpfilename);

	fclose(pFile);
	
	m_hasChanged = false;
	return true;

} // Save


bool
Preferences::Save(const string filename) {

	if (filename.empty()) {
		return false;
	}

	m_filename = filename;

	return Save();

} // Save


void
Preferences::set(const string &key, const string &value) {

	m_prefs[key] = value;
	m_hasChanged = true;

} // set


void
Preferences::Set(const string key, const string value) {

	string val;

	val = mc_RCString + value + mc_RCString;
	m_prefs[key] = value;
	m_hasChanged = true;

} // Set


void
Preferences::Set(const string key, const char* value) {

	string val;

	val = mc_RCString + value + mc_RCString;
	m_prefs[key] = value;
	m_hasChanged = true;

} // Set


void
Preferences::Set(const string key, const bool value) {

	m_prefs[key] = (value) ? mc_TrueValues[0] : mc_FalseValues[0];
	m_hasChanged = true;

} // Set


void
Preferences::Set(const string key, const int value) {

	char sbuffer[256];
	
	sprintf(sbuffer, "%d", value);
	m_prefs[key] = sbuffer;
	m_hasChanged = true;

} // Set


void
Preferences::Set(const string key, const double value) {

	char sbuffer[256];

	sprintf(sbuffer, "%f", value);
	m_prefs[key] = sbuffer;
	m_hasChanged = true;

} // Set


