/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Resource file parsing module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id
 *
 */

/** @file
 * Class MiniParser source file
 */


#include <string>

#include "MiniParser.h"


using namespace std;


//// STATIC MEMBERS ////

const string MiniParser::WHITESPACE = " \t\n\r";

const int MiniParser::mc_DefaultBufferSize	= 2048;

// the default special symbols are the whitesapce characters
const string MiniParser::mc_DefaultSpecials = " \t\n\r";


//// METHODS ////


MiniParser::MiniParser()
			: m_pFile(NULL), m_lineNum(0), m_charNum(0), m_position(0) {

	m_specials = mc_DefaultSpecials;
	m_bufferSize = mc_DefaultBufferSize;
	m_buffer = new char[m_bufferSize];
	m_buffer[0] = '\0';

} // default constructor


MiniParser::MiniParser(const string &specials)
			: m_specials(specials), m_pFile(NULL), m_lineNum(0), m_charNum(0), m_position(0) {

	m_bufferSize = mc_DefaultBufferSize;
	m_buffer = new char[mc_DefaultBufferSize];
	m_buffer[0] = '\0';

} // constructor


MiniParser::MiniParser(const string &filename, const string &specials)
			: m_specials(specials), m_pFile(NULL), m_lineNum(0), m_charNum(0), m_position(-1) {

	m_bufferSize = mc_DefaultBufferSize;
	Filename(filename);
	m_buffer = new char[mc_DefaultBufferSize];
	m_buffer[0] = '\0';

} // constructor


MiniParser::MiniParser(const MiniParser &mp) {

	m_lineNum = mp.m_lineNum;
	m_charNum = mp.m_charNum;
	m_position = mp.m_position;
	m_specials = mp.m_specials;
	m_currentChar = mp.m_currentChar;
	m_nextSymbol = mp.m_nextSymbol;
	m_isSpecial = mp.m_isSpecial;
	m_bufferSize = mp.m_bufferSize;
	Filename(mp.m_filename);
	m_buffer = new char[m_bufferSize];
	memcpy(m_buffer, mp.m_buffer, sizeof(char) * m_bufferSize);
	m_pFile=0;

} // copy constructor


MiniParser::~MiniParser() {

	if (m_pFile != 0) {
		fclose(m_pFile);
	}
	delete [] m_buffer;

} // destructor


int
MiniParser::BufferSize(void) const {

	return m_bufferSize;

} // BufferSize


void
MiniParser::BufferSize(int size) {

	char* temp;

	// need to copy the old buffer to the new to avoid losing data
	// data will be lost anyway if the buffer size is reduced
	temp = new char[size];
	memcpy(temp, m_buffer, (size < m_bufferSize) ? size : m_bufferSize);
	delete [] m_buffer;

	m_bufferSize = size;
	m_buffer = temp;

} // BufferSize


int
MiniParser::CharacterNumber(void) const {

	return m_charNum;

} // CharacterNumber


const string &
MiniParser::Filename(void) const {

	return m_filename;

} // Filename


bool
MiniParser::Filename(const string &filename) {

	if (m_pFile != NULL) {
		fclose(m_pFile);
	}
	if ((m_pFile = fopen(filename.c_str(), "r")) == NULL) {
		return false;
	}

	m_filename = filename;

	return true;

} // Filename


bool
MiniParser::IsMember(const char symbol, const string &symbols) {

	return symbols.find(symbol) != string::npos;

} // IsMember


bool
MiniParser::IsSpecial(const char symbol) const {

	return IsMember(symbol, m_specials);

} // IsSpecial


bool
MiniParser::IsSpecial(const string &symbol) const {

	if (symbol.length() > 1) {
		return false;
	}
	
	return IsMember(symbol[0], m_specials);

} // IsSpecial


bool
MiniParser::IsWhitespace(const char symbol) {

	return IsMember(symbol, WHITESPACE);

} // IsWhitespace


bool
MiniParser::IsWhitespace(const string &symbol) {

	if (symbol.length() > 1 || symbol.empty()) {
		return false;
	}

	return IsMember(symbol[0], WHITESPACE);

} // IsWhitespace


bool
MiniParser::LastSymbolSpecial(void) const {

	return m_isSpecial;

} // LastSymbolSpecial


int
MiniParser::LineNumber(void) const {

	return m_lineNum;

} // LineNumber


bool
MiniParser::Next(string &symbol) {

	bool stillParsing;	// loop control, true if still parsing symbol

	// if the next symbol has already been parsed return it
	if (!m_nextSymbol.empty()) {
		symbol = m_nextSymbol;
		// will always be special symbol except when ParseUpto is called
		// in which case the 'upto' char will be the next symbol and it
		// does not have to be special
		m_isSpecial = IsSpecial(m_nextSymbol);
		m_nextSymbol.erase();
		return true;
	}

	// character reading loop
	stillParsing = true;
	symbol.erase();
	while (stillParsing) {

		if (!update_buffer()) {
			return false;
		}
		update_counters();

		// get next character from buffer and increment position in buffer
		m_currentChar = m_buffer[m_position++];

		// if reached the end of the file
		if (m_currentChar == '\0') {
			stillParsing = false;
		}
		// else if the character is special
		else if (IsSpecial(m_currentChar)) {
			if (symbol.length() == 0) {
				symbol = m_currentChar;
				m_isSpecial = true;
			}
			else {
				m_nextSymbol = m_currentChar;
				m_isSpecial = false;
			}
			stillParsing = false;
		}
		// else part of a regular symbol
		else {
	   		symbol += m_currentChar;
		}

	}

	return symbol.length() > 0;

} // Next


bool
MiniParser::ParseUpto(const char endSymbol, string &symbol) {

	bool stillParsing;	// loop control, true if still parsing symbol

	m_isSpecial = false;

	// if next symbol is end symbol then we're finished
	if (m_nextSymbol[0] == endSymbol) {
		symbol.erase();
		return true;
	}
	// else if the next symbol has already been parsed add it to symbol
	else if (!m_nextSymbol.empty()) {
		symbol = m_nextSymbol;
		m_nextSymbol.erase();
	}

	// character reading loop
	stillParsing = true;
	while (stillParsing) {

		if (!update_buffer()) {
			return false;
		}
		update_counters();

		// get next character from buffer and increment position in buffer
		m_currentChar = m_buffer[m_position++];

		// if reached the end of the file
		if (m_currentChar == '\0') {
			stillParsing = false;
		}
		// else if character is the end symbol
		else if (m_currentChar == endSymbol) {
			m_nextSymbol = m_currentChar;
			stillParsing = false;
		}
		// else part of a regular symbol
		else {
	   		symbol += m_currentChar;
		}

	}

	return symbol.length() > 0;

} // ParseUpto


bool
MiniParser::ParseUpto(const string &endSymbols, string &symbol) {

	bool stillParsing;	// loop control, true if still parsing symbol

	m_isSpecial = false;

	// if next symbol is end symbol then we're finished
	if (IsMember(m_nextSymbol[0], endSymbols)) {
		symbol.erase();
		return true;
	}
	// if the next symbol has already been parsed add it to symbol
	if (!m_nextSymbol.empty()) {
		symbol = m_nextSymbol;
		m_nextSymbol.erase();
	}

	// character reading loop
	stillParsing = true;
	while (stillParsing) {

		if (!update_buffer()) {
			return false;
		}
		update_counters();

		// get next character from buffer and increment position in buffer
		m_currentChar = m_buffer[m_position++];

		// if reached the end of the file
		if (m_currentChar == '\0') {
			stillParsing = false;
		}
		// if character is one of the end symbols
		else if (IsMember(m_currentChar, endSymbols)) {
			m_nextSymbol = m_currentChar;
			stillParsing = false;
		}
		// else part of a regular symbol
		else {
	   		symbol += m_currentChar;
		}

	}

	return symbol.length() > 0;

} // ParseUpto


bool
MiniParser::update_buffer(void) {

	size_t count;	// number of bytes read into buffer

	// fill the parsing buffer from the current file position if required
	if ((m_position == (m_bufferSize - 1)) || (m_buffer[m_position] == '\0')) {
		count = fread(m_buffer,
					  sizeof(char),
   					  m_bufferSize - 1,
					  m_pFile);
		// reset current position in buffer and terminate buffer with a NULL
		m_buffer[count] = '\0';
		m_position = 0;
	}
	
	return ferror(m_pFile) == 0;

} // update_buffer


void
MiniParser::update_counters(void) {

	// if an end of line was previously read, increment line number
	if ((m_currentChar == '\n') || (m_currentChar == '\r')) {
		m_lineNum++;
		m_charNum = 0;
	}
	// else increment char number on this line
	else {
		m_charNum++;
	}

} // update_counters

