/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CPSocket - Client/Server execution module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * Based on Linux Socket Programming In C++ By Rob Tougher from LinuxGazzete
 * $Id: CPSocket.cpp,v 1.2 2006/12/18 08:21:25 robinrowe Exp $
 */

#include "CPSocket.h"

#ifdef WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#endif


#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <iostream>

#if !defined(MSG_NOSIGNAL)
# define MSG_NOSIGNAL 0
#endif

CPSocket::CPSocket() :
  m_sock ( -1 )
{

#ifdef WIN32
	WORD wVersionRequested;
	WSADATA wsaData;
	int err;
	wVersionRequested = MAKEWORD( 2, 2 );
 
	err = WSAStartup( wVersionRequested, &wsaData );
#endif

  memset ( &m_addr, 0, sizeof ( m_addr ) );


  // default block wait timeouts
  // @todo [claw] should provide modifiers for these
  m_block_wait = false;
  m_block_wait_secs = 1;
  m_block_wait_usecs = 5000;
}

CPSocket::~CPSocket()
{
	if(is_valid())
	{
#ifdef WIN32
		::closesocket(m_sock);
#else
		::close(m_sock);
#endif
	}
}

bool CPSocket::create()
{
	m_sock = ::socket ( PF_INET, SOCK_STREAM, 0);
//	int err = WSAGetLastError();
  if ( !is_valid() )
    return false;


  // TIME_WAIT - argh
  int on = 1;
  if ( setsockopt ( m_sock, SOL_SOCKET, SO_REUSEADDR, ( const char* ) &on, sizeof ( on ) ) == -1 )
    return false;


  return true;

}



bool CPSocket::bind ( const int port )
{

  if ( ! is_valid() )
    {
      return false;
    }



  m_addr.sin_family = AF_INET;
  m_addr.sin_addr.s_addr = INADDR_ANY;
  m_addr.sin_port = htons ( port );

  int bind_return = ::bind ( m_sock,
			     ( struct sockaddr * ) &m_addr,
			     sizeof ( m_addr ) );


  if ( bind_return == -1 )
    {
      return false;
    }

  return true;
}


bool CPSocket::listen()
{
  if ( ! is_valid() )
    {
      return false;
    }

	int listen_return = ::listen ( m_sock, MAXCONNECTIONS );


  if ( listen_return == -1 )
    {
      return false;
    }

  return true;
}


bool CPSocket::accept ( CPSocket& new_socket )
{
  int addr_length = sizeof ( m_addr );
  new_socket.m_sock = ::accept ( m_sock, ( sockaddr * ) &m_addr, ( socklen_t * ) &addr_length );

  if ( new_socket.m_sock <= 0 )
    return false;
  else
    return true;
}


bool CPSocket::send ( const std::string s ) const
{
	int status = ::send ( m_sock, s.c_str(), int(s.size())+1, MSG_NOSIGNAL );
	return (status != -1);
}

bool CPSocket::send ( const int i ) const
{
	int status = ::send(m_sock, (char* )&i, sizeof(int), MSG_NOSIGNAL );
	return (status != -1);
}
bool CPSocket::send ( const float f) const
{
	int status = ::send(m_sock, (char* )&f, sizeof(float), MSG_NOSIGNAL );
	return (status != -1);
}
bool CPSocket::send ( const char* s ) const
{
	int status = ::send(m_sock, s, int(strlen(s)), MSG_NOSIGNAL );
	return (status != -1);
}


bool CPSocket::send ( const unsigned char* buf, const int len) const
{
	int status = ::send(m_sock, (char* )buf, len, MSG_NOSIGNAL );
	return (status != -1);
}


int CPSocket::recv ( std::string& s ) const
{
	char buf [ MAXRECV + 1 ];
	s = "";
	memset ( buf, 0, MAXRECV + 1 );

	int status = ::recv ( m_sock, buf, MAXRECV, MSG_PEEK);
	// Find NULL terminator.
	//for(int i=0; buf[i] != '\0' && i < MAXRECV ; i++);
	int i = 0;
	while(buf[i] != '\0' && i < MAXRECV)
	{
		i++;
	}
	if (buf[i] == '\0')
        status = ::recv(m_sock, buf, i+1, 0);

	if ( status == -1 )
	{
		std::cout << "status == -1   errno == " << errno << "  in CPSocket::recv(std::string)\n";
		return 0;
    }
	else if ( status == 0 )
		return 0;
	else
    {
		s = buf;
		return status;
    }
}

int CPSocket::recv ( int &i ) const
{
	char buf [sizeof(int)+1];
	memset ( buf, 0, sizeof(int)+1 );
	int status = ::recv ( m_sock, buf, sizeof(int), 0 );
	if ( status == -1 )
	{
		std::cout << "ERROR: status == -1   errno == " << errno << "  in CPSocket::recv(int)\n";
		return 0;
    }
	else if ( status == 0 )
		return 0;
	else
    {
		if (status == sizeof(int))
		{
			i = *(int *)buf;
			return status;
		}
		else
		{
			std::cout << "ERROR: status != sizeof(int) in CPSocket::recv(int)\n";
			return 0;
		}
    }
}
int CPSocket::recv ( float &f ) const
{
	char buf [sizeof(float)+1];
	memset ( buf, 0, sizeof(float)+1 );
	int status = ::recv ( m_sock, buf, sizeof(float), 0 );
	if ( status == -1 )
	{
		std::cout << "ERROR: status == -1   errno == " << errno << "  in CPSocket::recv(float)\n";
		return 0;
    }
	else if ( status == 0 )
		return 0;
	else
    {
		if (status == sizeof(float))
		{
            f = *(float *)buf;
			return status;
		}
		else
		{
			std::cout << "ERROR: status != sizeof(float) in CPSocket::recv(float)\n";
			return 0;
		}

    }
}
int CPSocket::recv ( char* &s ) const
{
	char buf [ MAXRECV + 1 ];
	s = "";
	memset ( buf, 0, MAXRECV + 1 );

	int status = ::recv ( m_sock, buf, MAXRECV, 0 );

	if ( status == -1 )
	{
		std::cout << "status == -1   errno == " << errno << "  in CPSocket::recv(char* )\n";
		return 0;
    }
	else if ( status == 0 )
		return 0;
	else
    {
		s = new char[status];
		for (int i=0;i<status; i++)
			s[i] = buf[i];
		return status;
    }
}

int CPSocket::recv ( unsigned char* buf, const int len) const
{
	int n_bytes=0;
    int status;
	bool error = false;
	bool bytes_to_read = true;

	char* bp = (char* )buf;
	int bytes_remaining = len;

	while (bytes_to_read && !error)
	{
		status = ::recv ( m_sock, bp, bytes_remaining, 0 );
        if ( status == -1 )
			error = true;
		else
			n_bytes += status;
		bytes_to_read = n_bytes < len;
		if( bytes_to_read)
		{
			bp += status;
			bytes_remaining -= status;
		}
	}

	if ( error )
	{
		std::cout << "status == -1   errno == " << errno << "  in CPSocket::recv(unsigned char* , int)\n";
		return 0;
    }
	return n_bytes;
}

bool CPSocket::connect ( const char* host, const int port )
{
	int status;
	char port_c[10];
	sprintf(port_c, "%d", port); 
    if ( ! is_valid() ) return false;

	m_addr.sin_family = PF_INET;
	m_addr.sin_port = htons ( port );

	addrinfo *res;
	addrinfo hint;
	memset(&hint, 0,sizeof(hint));
	hint.ai_family = PF_INET;
	hint.ai_socktype = SOCK_STREAM;
	status = getaddrinfo(host, port_c, &hint, &res);

//	int err = WSAGetLastError();
	
	status = ::connect ( m_sock, res->ai_addr, int(res->ai_addrlen));
//		err = WSAGetLastError();

  if ( status == 0 )
    return true;
  else
    return false;
}


bool CPSocket::check_close()
{
	char buf [ 1 ];
	int status = ::recv ( m_sock, buf, MAXRECV, MSG_PEEK);

	return status == 0;
}

CPSocket::WaitEndENUM CPSocket::BlockingWait()
{
	CPSocket::WaitEndENUM _ret = CPSocket::SOCK_ERROR;

	char buf[20];
	//int _bytes;
//	int status = ::recv( m_sock, buf, 20, MSG_PEEK);
	fd_set rfs;
	FD_ZERO(&rfs);
	FD_SET(m_sock, &rfs);

#ifdef WIN32
	// included in winsock2 select call only for compatibility
	int _fd = 0;
	//int _err;
#else
	// highest numbered descriptor in file descriptor sets to select call
	int _fd = m_sock + 1;
#endif

	struct timeval tv ;

	m_block_wait = true;
	while(m_block_wait)
	{
		tv.tv_sec = long(m_block_wait_secs) ;
		tv.tv_usec = m_block_wait_usecs ;

		int status = select(_fd, &rfs, 0, 0, &tv);

		if(status == 0)
		{
			// timeout - keep looping until m_block_wait is unset#
			printf("INFO: BlockingWait() - waiting\n");

//#ifdef WIN32
			// win32 seems to reset the descriptor when the select call returns 0,
			// so we need the (re)set read descriptor set to watch our socket
			FD_ZERO(&rfs);
			FD_SET(m_sock, &rfs);
//#endif
		}
		else if (status == 1)
		{
			m_block_wait = false;
			int _bytes = ::recv( m_sock, buf, 20, MSG_PEEK);

			if ( _bytes == -1 )
				_ret = CPSocket::SOCK_ERROR;
			else if (_bytes == 0 )
				_ret = CPSocket::SOCK_CLOSE;
			else
				_ret = CPSocket::SOCK_DATA;
		}
		else
		{
			// somethign weird happened,
			m_block_wait = false;
			_ret = CPSocket::SOCK_ERROR;
		}
	}

	return(_ret);
}

void CPSocket::CancelBlockingWait()
{
	// this halts the BlockingWait timeout loop, resulting in the BlockingWait
	// call returning after its timeout next expires.
	m_block_wait = false;
}

bool CPSocket::close()
{
#ifdef WIN32
	int status = ::closesocket(m_sock);
#else
	int status = ::close(m_sock);
#endif

	return (status == 0);
}

void CPSocket::set_non_blocking ( const bool b )
{

  //int opts;
/*
  opts = fcntl ( m_sock,
		 F_GETFL );

  if ( opts < 0 )
    {
      return;
    }

  if ( b )
    opts = ( opts | O_NONBLOCK );
  else
    opts = ( opts & ~O_NONBLOCK );

  fcntl ( m_sock,
	  F_SETFL,opts );
*/
}

//=====================================
// The stream operators.
const CPSocket& CPSocket::operator << ( const std::string& s ) const
{
  if ( ! CPSocket::send ( s ) )
      throw CPSocketException ( "Could not write to socket." );
  return *this;
}

const CPSocket& CPSocket::operator << ( const int& i ) const
{
  if ( ! CPSocket::send ( i ) )
      throw CPSocketException ( "Could not write to socket <int>." );
  return *this;
}

const CPSocket& CPSocket::operator << ( const float& f ) const
{
  if ( ! CPSocket::send ( f ) )
      throw CPSocketException ( "Could not write to socket <float>." );
  return *this;
}

const CPSocket& CPSocket::operator << ( const char* & c ) const
{
  if ( ! CPSocket::send ( c ) )
      throw CPSocketException ( "Could not write to socket <char* >." );
  return *this;
}


const CPSocket& CPSocket::operator >> ( std::string& s ) const
{
  if ( ! CPSocket::recv ( s ) )
      throw CPSocketException ( "Could not read from socket." );
  return *this;
}

const CPSocket& CPSocket::operator >> (int& i ) const
{
  if ( ! CPSocket::recv ( i ) )
      throw CPSocketException ( "Could not read from socket <int>." );
  return *this;
}

const CPSocket& CPSocket::operator >> ( float& f ) const
{
  if ( ! CPSocket::recv ( f ) )
      throw CPSocketException ( "Could not read from socket.<float>." );
  return *this;
}

const CPSocket& CPSocket::operator >> ( char* & s ) const
{
  if ( ! CPSocket::recv ( s ) )
      throw CPSocketException ( "Could not read from socket <char* >." );
  return *this;
}

