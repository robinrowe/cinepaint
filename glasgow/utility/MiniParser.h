/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Resource file parsing module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK *****
 * 
 * $Id
 *
 */
// created 01/10/04 Stuart Ford

/** @file
 * Class MiniParser header file
 */

#ifndef _H_MINIPARSER_
#define _H_MINIPARSER_


#include <cstdio>
#include <string>


using namespace std;

/**
 * A class for performing very simple text parsing.
 * A MiniParser object is used to parse text into simple building blocks. Each
 * block is deemed to be: an unclassified symbol, or a special symbol. Special
 * symbols are characters previously indicated by the user to be significant,
 * and include purely delimiting characters such as whitespace as well as
 * semantically significant symbols like punctuation. All other strings are
 * regarded as basic symbols.
 */
class MiniParser {

public:

//// MEMBERS ////

	static const string WHITESPACE;


//// FUNCTIONS ////

	/**
	 * Tests a symbol for membership of a set of symbols.
	 * @param symbol	the symbol to test for membership
	 * @param symbols	the set of symbols
	 * @return	true if the symbol is a member of the set of symbols
	 */
	static bool IsMember(const char symbol, const string &symbols);

	/**
	 * Default constructor.
	 * Creates a MiniParser with only whitespace characters as special symbols.
	 * Since no filename is supplied, parsing cannot begin until the #filename
	 * mutator function is called.
	 */
	MiniParser();

	/**
	 * Copy constructor.
	 * Creates a MiniParser with the same properties and state as another.
	 */
	MiniParser(const MiniParser &mp);

	/**
	 * Constructor.
	 * Creates a MiniParser object and using the supplied special symbols.
	 * Since no filename is supplied, parsing cannot begin until the #filename
	 * mutator function is called.
	 * @param specials		[i] an aggregate of special symbols
	 */
	MiniParser(const string &specials);

	/**
	 * Constructor.
	 * Creates a MiniParser object associated with a particular file and using
	 * the supplied special symbols.
	 * @param filename		[i] path and name of the preferences file
	 * @param specials		[i] an aggregate of special symbols
	 */
	MiniParser(const string &filename, const string &specials);

	/**
	 * Destructor.
	 */
	virtual ~MiniParser();

	/**
	 * Parse and return the next symbol.
	 * @param symbol	[o] the next symbol
	 * @return	true if the next symbol was parsed, false on end of file or
	 *			file-reading problem
	 */
	virtual bool Next(string &symbol);

	/**
	 * Parses all text as a single symbol until a given end symbol or the end of
	 * the file is reached, whichever comes first.
	 * When parsing begins again the first symbol to be returned will be the end
	 * symbol.
	 * @param endSymbol	[i] the symbol at which to stop collating text
	 * @param symbol	[o] the parsed and collated text
	 * @return	true if the next symbol was parsed, false on end of file or
	 *			file-reading problem
	 */
	virtual bool ParseUpto(const char endSymbol, string &symbol);

	/**
	 * Parses all text as a single symbol until one of a given set of end
	 * symbols or the end of the file is reached, whichever comes first.
	 * When parsing begins again the first symbol to be returned will be the end
	 * symbol that caused parsing to stop.
	 * @param endSymbols	[i] set of symbols at which to stop collating text
	 * @param symbol		[o] the parsed and collated text
	 * @return	true if the next symbol was parsed, false on end of file or
	 *			file-reading problem
	 */
	virtual bool ParseUpto(const string &endSymbols, string &symbol);

	/**
	 * Get the path and filename of a Preferences object.
	 * An accessor function for retrieving the currently set filename (including
	 * path) of a Preferences object.
	 * @return	the current filename as a string
	 */
	virtual const string &Filename(void) const;

	/**
	 * Set the path and filename of a Preferences object.
	 * A mutator function for setting the current filename (including path) of a
	 * Preferences object.
	 * @param filename	[i] path and name of the preferences file
	 * @return	true if the file could be opened for reading, else false
	 */
	virtual bool Filename(const string &filename);

	/**
	 * Indicates whether the last symbol parsed was a special symbol. The
	 * parser keeps track of this as it parses symbols so this is a quick way
	 * of determining if a symbol is special.
	 * @return	true if the last symbol read was special, else false
	 */
	bool LastSymbolSpecial(void) const;

	/**
	 * Returns the count of the number of characters parsed in the current line.
	 * @return	the number of characters parsed so far
	 */
	int CharacterNumber(void) const;

	/**
	 * Returns the count of the number of lines parsed so far.
	 * @return	the number of lines parsed
	 */
	int LineNumber(void) const;

	/**
	 * Accessor for the size of the read buffer.
	 * Text is read into a buffer before parsing, and the current size of this
	 * buffer is returned by this function.
	 * @return	the size of the read buffer in bytes
	 */
	virtual int BufferSize(void) const;

	/**
	 * Mutator for setting the size of the read buffer.
	 * Text is read into a buffer before parsing, and the current size of this
	 * buffer is set by this function. The buffer size is set by default so
	 * there is no need to call this function unless there is reason to suspect
	 * a different buffer size will increase efficiency. Warning: reducing the
	 * buffer size after parsing has started will cause loss of data.
	 * @param size	[i] the size to make the read buffer in bytes
	 */
	virtual void BufferSize(const int size);

	/**
	 * Indicates whether a given symbol is special.
	 * @param symbol	the symbol to test
	 * @return	true if the symbol is special, else false
	 */
	bool IsSpecial(const char symbol) const;

	/**
	 * Indicates whether a given symbol is special.
	 * @param symbol	the symbol to test
	 * @return	true if the symbol is special, else false
	 */
	bool IsSpecial(const string &symbol) const;

	/**
	 * Indicates whether a given symbol is a whitespace character.
	 * @param symbol	the symbol to test
	 * @return	true if the symbol is whitespace, else false
	 */
	static bool IsWhitespace(const char symbol);

	/**
	 * Indicates whether a given symbol is a single whitespace character.
	 * @param symbol	the symbol to test
	 * @return	true if the symbol is whitespace, else false
	 */
	static bool IsWhitespace(const string &symbol);


protected:

//// MEMBERS ////

	string m_filename;			/**< path and name of file being parsed */

	string m_specials;			/**< aggregate of special symbol strings */

	FILE *m_pFile;				/**< Pointer into stream for filling buffer */

	char* m_buffer;				/**< Read buffer for quick parsing */

	char m_currentChar;			/**< the last character read from the buffer */

 	string m_nextSymbol;		/**< holds the next symbol in the buffer if it
  									 has been parsed */

	int m_lineNum;				/**< Current line number */

	int m_charNum;				/**< Current character number */

	int m_position;				/**< Current index in read buffer */

	int m_bufferSize;			/**< Size of the read buffer */

	bool m_isSpecial;			/**< true if last parsed symbol was reserved */


//// FUNCTIONS ////

	/**
	 * Reads text from file into the buffer if the end of the buffer has been
	 * reached during parsing, otherwise does nothing. A NULL character is
	 * inserted after the last read character in the buffer
	 * @return	true if the buffer was updated,
	 *			false if there was a file-reading problem
	 */
	bool update_buffer(void);

	/**
	 * Increments the line and character counters depending on the value of the
	 * last character read from the buffer.
	 */
	void update_counters(void);


private:

	/** default read buffer size */
	static const int mc_DefaultBufferSize;
	
	/** an array of special symbols used by default if none are supplied */
	static const string mc_DefaultSpecials;


}; // class MiniParser


/**
 * Pointer to MiniParser type.
 */
typedef MiniParser *pMiniParser;




#endif // _H_MINIPARSER_


