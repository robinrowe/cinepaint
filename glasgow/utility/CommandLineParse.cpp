/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CommandLineParse - parse the command line.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CommandLineParse.cpp,v 1.2 2006/12/18 08:21:25 robinrowe Exp $
 */

#include <cstring>
#include <cstdlib>
#include <cstdio>
#include "CommandLineParse.h"

CommandLineParse::CommandLineParse(int argc, char* *argv)
{
	m_argCount = argc;
	m_pArgs = argv;
/*	m_noInterface = false;
	m_noData = false;
	m_noSplash = false;
	m_noSplashImage = false;
	m_useDebugHandler = false;
	m_consoleMessages = false;
	m_beVerbose = false;
*/
	m_showVersion = false;
	m_showHelp = false;

	m_serverPort = 0;
	m_serverLog = 0;

	parse_command_line();
}

int CommandLineParse::parse_command_line()
{
	int i;
	for (i = 1; i < m_argCount; i++)
	{
		/* added by IMAGEWORKS (02/21/02) */
		if( strcmp( m_pArgs[i], "--server" ) == 0 ) {
			m_serverPort = atoi( m_pArgs[++i] );
			m_serverLog = m_pArgs[++i];
		}
		else if ((strcmp (m_pArgs[i], "--help") == 0) ||
			(strcmp (m_pArgs[i], "-h") == 0))
		{
			m_showHelp = true;
			m_pArgs[i] = NULL;
		}
		else if (strcmp (m_pArgs[i], "--version") == 0 ||
			strcmp (m_pArgs[i], "-v") == 0)
		{
			m_showVersion = true;
			m_pArgs[i] = NULL;
		}
#ifdef BUILD_SHM
		else if (strcmp (m_pArgs[i], "--no-shm") == 0)
		{
			use_shm = false;
			m_pArgs[i] = NULL;
		}
#endif
		else if( strcmp( m_pArgs[i], "--sharedmem" ) == 0 )
		{
#ifdef BUILD_SHM
			if( i+6 >= argc )
				m_showHelp = true;
			else
			{
				useSharedMem = true;
				parentPID = atoi( m_pArgs[++i] );
				shmid = atoi( m_pArgs[++i] );
				offset = atoi( m_pArgs[++i] );
				chans = atoi( m_pArgs[++i] );
				xSize = atoi( m_pArgs[++i] );
				ySize = atoi( m_pArgs[++i] );
				/*
				d_printf( "Parent PID: %d, shmid: %d, off: %d, chans: %d, (%d,%d)\n", 
				parentPID, shmid, offset, chans, xSize, ySize );
				*/
			}
#else
			printf("Built without shared memory -- can't enable!");
#endif
		}
		/*
		*    ANYTHING ELSE starting with a '-' is an error.
		*/
		else if (m_pArgs[i][0] == '-')
		{
			m_showHelp = true;
		}
	}
	return 1;
}
