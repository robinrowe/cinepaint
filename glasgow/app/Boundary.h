/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Modified for CinePaint by Colin law 2004/09/22
 *
 * $Id: Boundary.h,v 1.2 2008/01/28 18:22:28 robinrowe Exp $
 */
#ifndef _CINEPAINT_BOUNDARY_H_
#define _CINEPAINT_BOUNDARY_H_

#include <list>
#include "dll_api.h"

/**
 * Boundary manages a list of Boundary Segments that describe a Boundary within an image
 *
 */
class CINEPAINT_IMAGE_API Boundary
{
	public:
		/**
		 * Constructs an initially empty boundary containing zero segments
		 *
		 */
		Boundary();

		/**
		 * Destructor, destroys all added boundary segments
		 */
		~Boundary();


		/**
		 * Clears the current Boundary List
		 *
		 */
		void Clear();

		/**
		 * Returns the number of BoundarySegments within this Boundary
		 *
		 * @return the number of BoundarySegments within this Boundary
		 */
		size_t GetBoundaryCount() const;

		//---------------
		// Nested classes

		/**
		 * BoundarySegment provides a segment within a Boundary describing an area of an image
		 *
		 */
		class BoundarySegment
		{
			public:
				/**
				 * Construcrts a new Boundary Segment from point (x1, y1) to (x2, y2)
				 *
				 * @param x1 x coordinate of segment start point
				 * @param y1 y coordinate of segment start point
				 * @param x2 x coordinate of segment end point
				 * @param y2 y coordinate of segment end point
				 * @param open
				 * @param visited
				 */
				BoundarySegment(int x1, int y1, int x2, int y2, bool open = false, bool visited = false);

				/**
				 * Destructor
				 */
				~BoundarySegment() {};
	
				enum BoundaryTypeEnum { WITHIN_BOUNDS_ENUM, IGNORE_BOUNDS_ENUM };
	
			protected:
	
			private:
				/** x coordinate of segment start point */
				int m_x1;

				/** y coordinate of segment start point */
				int m_y1;
		

				/** x coordinate of segment end point */
				int m_x2;

				/** y coordinate of segment end point */
				int m_y2;
		        
				bool m_open;
				bool m_visited;
		}; /* class BoundarySegment */

	protected:

	private:

		typedef std::list<BoundarySegment*> BoundaryList_t;

		/** list of the boundary segments */
		BoundaryList_t m_boundary_list;

}; /* class Boundary */

#endif /* _CINEPAINT_BOUNDARY_H_ */
