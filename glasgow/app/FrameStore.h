/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      FrameStore - FrameManager Image pre-loader
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: FrameStore.h,v 1.2 2006/12/21 11:18:04 robinrowe Exp $
 */

#ifndef _FRAME_STORE_H_
#define _FRAME_STORE_H_

#include "dll_api.h"
#include "FrameManagerListener.h"
#include "FrameManager.h"
#include "plug-ins/pdb/ListenerList.h"
#include "plug-ins/pdb/World.h"
#include <pthread.h>

#include <list>
#include <string>


class CinePaintImage;
class FrameManagerListener;
class FrameStoreListener;

/**
 * FrameStore provides an image pre-loading mechanism.
 * the FrameStore provides a means to look ahead of the currently selected frame
 * within a FrameManager to pre-load required data. During pre-loading, this FrameStore
 * loades images to its capacity, then waits until there is space to load the next item.
 * When an item is removed, the next item within the FrameManager can be loaded.
 * The images loaded by this FrameStore are intended fo use within a flip-book type
 * application, as such, a renderor is constructed and added to the RenderingManager
 * of each loaded image.
 * The FrameStore can be used in two modes, on demans or pre-load. see SetMode.
 *
 */
class CINEPAINT_CORE_API FrameStore : public FrameManagerListener
{	private:
		/**
		 * data struct holding the items loaded by this FrameStore
		 *
		 */
		struct FrameItem
		{
			const FrameManager::FrameRecord* m_frame;
			CinePaintImage* m_image;
		};

		typedef std::list<FrameItem*> FrameStore_t;

	public:
		//-------------------------
		// Constructor / Destructor

		/**
		 * Constructs a new FrameManager pre-loading images for the  specified FrameManager
		 *
		 * @param frame_manager the FrameManager to pre-load images for
		 */
		FrameStore(FrameManager& frame_manager);

		/**
		 * Dectructor
		 */
		virtual ~FrameStore();



		//--------------------
		// Pre-loading Control

		/**
		 * Starts this FrameStore pre-loading images.
		 *
		 */
		void StartPreLoad();

		/**
		 * Stops this FrameStore pre-loading images
		 *
		 */
		void StopPreLoad();

		/**
		 * Returns whether this FrameStore is currently pre-loading images.
		 *
		 * @return true if this FrameStore is currently pre-loading images, false otherwise
		 */
		bool IsPreLoading();




		//---------------------
		// accessors / mutators


		/**
		 * Returns the CinePaintImage specified within FrameManager::FrameRecord.
		 * The operation of this method is dependant upon the mode of operation of this FrameStore,
		 * If the returned image is not 0, the caller must assume the responsibility for the
		 * CinePaintImage reference, releasing the reference witin the CinePaintDocList when conplete. 
		 *
		 * @param frame the FrameManager::FrameRecord indicating the image source
		 * @see SetLoadMode
		 */
		CinePaintImage* GetFrame(const FrameManager::FrameRecord& frame);

		enum StoreLoadMode { ON_DEMAND_ENUM, PRE_LOAD_ENUM };

		typedef FrameStore_t::size_type size_type;

		/**
		 * Sets the mode this FrameStore will be used in
		 * The FrameStore can be used in two modes, on demans or pre-load. When in on-demand
		 * mode, a call to GetFrame will return the image corresponding to the specified frame,
		 * if the image has not been pre-loaded, it is loaded. When in pre-load mode, a thread
		 * runs continually pre-loading images, to the set capacity of this FrameStore. A call
		 * to GetFrame will return the pre-loaded image is pre-loaded, or 0 if not.
		 *
		 * @param mode the mode of operation of this FrameStore
		 */
		void SetLoadMode(StoreLoadMode mode);

		/**
		 * Returns the current mode of operation for this FrameStore
		 *
		 * @return the current mode of operation for this FrameStore
		 */
		StoreLoadMode GetLoadBehaviour() const;






		/**
		 * Returns the current number of item contained within this FrameStore
		 *
		 * @return the number of items currently contained within thie FrameStore
		 */
		FrameStore::size_type GetStoreSize() const;

		/**
		 * Returns the capacity of this FrameStore.
		 * The capacity of this FrameStore indicates how many images this FrameStore can pre-load.
		 *
		 * @return the capacity of this FrameStore
		 */
		FrameStore::size_type GetStoreCapacity() const;

		/**
		 * Sets the capacity of this FrameStore to the specified value.
		 *
		 * @param capacity the new capacity for this FrameStore
		 */
		void SetCapacity(FrameStore::size_type capacity);

		/**
		 * Returns true if this FrameStore is buffered,
		 * This FrameStore is buffered when the size of this FrameStore reaches its capacity,
		 * or there are no more items to be loaded.
		 *
		 * @return true if this FrameStore is buffered
		 */
		bool IsBuffered() const;

		/**
		 * Clears this FrameStore of loaded images.
		 * All references to loaded images are released,
		 *
		 */
		void ClearBuffer();




		/**
		 * Returns the current position within the FrameManaget this FrameStore is reading from
		 *
		 * @return the current reading position within the FrameManager
		 */
		FrameManager::size_type GetStoreReadPos() const;

		/**
		 * Sets the current reading position within the FrameManager.
		 * The next item added to this FrameStore during pre-loading will be read from
		 * the specified position within the FrameManager
		 *
		 * @param pos the next position to read during pre-loading
		 */
		void SetStoreReadPos(FrameManager::size_type pos);


		/**
		 * Sets the size of the rendereor created for each loaded image.
		 *
		 * @param width the width of the renderor
		 * @param height the height of the renderor
		 */
		void SetRenderSize(int width, int height);

		/**
		 * Returns the size of the rendereor being created for each loaded image.
		 *
		 * @param width set to the width of the renderor
		 * @param height set to the height of the renderor
		 */
		void GetRenderSize(int& width, int& height) const;


		//-------------------
		// Listener handling

		/**
		 * Adds the specified FrameStoreListener to receive FrameStore events from this FrameStore
		 * If the specified FrameStoreListener has already been added it is not added again.
		 * The resources of the specified FrameStoreListener are not managed by this class
		 *
		 * @param listener the FrameStoreListener to be added
		 */
		void AddStoreListener(FrameStoreListener* listener);

		/**
		 * Removes and returns the specified FrameStoreListener.
		 * The removed FrameStoreListener will no longer receive Doc List events from this FrameStore
		 * If the specified listener is not present, 0 is returned
		 *
		 * @param listener the FrameStoreListener to be removed
		 */
		FrameStoreListener* RemoveStoreListener(FrameStoreListener* listener);




		//-------------------------------------
		// FrameManagerListener implementations

		virtual void SelectionChanged(const FrameManager::FrameRecord* frame, FrameManager::size_type position);
		virtual void FrameAdded(const FrameManager::FrameRecord& frame, FrameManager::size_type position);
		virtual void FrameRemoved(const FrameManager::FrameRecord& frame, FrameManager::size_type position);
		virtual void FrameUpdated(const FrameManager::FrameRecord& frame, FrameManager::size_type position);
		virtual void FrameIndexChanged(const FrameManager::FrameRecord& frame, FrameManager::size_type old_pos, FrameManager::size_type new_pos);



		/** renderor name for the image render added to a CinePaintImage by this FrameStore */
		static const char* FLIP_FRAME_RENDER_NAME;

	protected:

	private:

		/**
		 * Gets the image as specified within the FrameManager::FrameRecord
		 * If the frame record indicates an image Id, the CinePaintDocList is first
		 * queried for the names image. If the CinePaintDocList indicates the document
		 * is already loaded, the image is referenced and returned. If the CinePaintDocList
		 * does not manage the document of the specified Id, or no Id is given by the frame
		 * record, the source of the image is used, and the CinePaintDocList is used to load
		 * and reference the image.
		 *
		 * @return the loaded image
		 */
		CinePaintImage* get_image(const FrameManager::FrameRecord& frame);

		/**
		 * Removes and returns the FrameItem from this FrameStore created for the spcified frame record
		 * If no item has been loaded for the specified FrameRecord, 0 is returned,
		 * The caller must assume responsibility for the returned FrameItem, and any referenced image
		 * it may contain
		 *
		 * @param frame the FrameRecord for which the FrameItem was constructed
		 * @return the FrameItem corresponding to the specified FrameRecord
		 */
		FrameItem* remove_from_store(const FrameManager::FrameRecord& frame);

		/**
		 * Constructs a renderor for the spcified CinePaintImage if it does not already have one.
		 * The renderor is constructed for display on screen at the size as given by GetRenderSize and
		 * added to the inages RenderingManager using the renderor name FrameStore::FLIP_FRAME_RENDER_NAME
		 *
		 * @todo If the image already contains the renderor, we need to check the size etc.
		 * @param image the CinePaintImage for which to create a renderor
		 */
		void create_render(CinePaintImage& image);

		/**
		 * Returns trus if the specified read position can be moved onto a next valid frame within the FrameManager
		 * If the FrameManager is set non-looping and the current read position has reached the end, or there
		 * are no more active frames, this method will return false
		 *
		 * @param pos the read position to test
		 * @return true if there is a next position for the value to be moved to
		 */
		bool has_look_ahead(size_t pos) const;

		/**
		 * increments the look ahead read position
		 *
		 */
		void increment_look_ahead();




		/** the store of loaded frames / images */
		FrameStore_t m_store;



		//--------
		// members

		/** the FrameManager this FrameStore is loading images for */
		FrameManager& m_frame_manager;

		FrameManager::size_type m_look_ahead_pos;
		size_type m_capacity;

		/** current FrameStore mode of operation */
		StoreLoadMode m_load_mode;

		/** width of render required for loaded images */
		int m_render_width;

		/** height of render required for loaded images */
		int m_render_height;



		//----------
		// Threading

		/** the pre-load thread */
		pthread_t m_pre_load_thread;

		/** controls the m_pre_load_thread */
		bool m_pre_load_control;
		//bool m_pre_load_run;

		/** protects access to the FrameStore */
		pthread_mutex_t m_store_mutex;

		/** used to signal changes in the satte of the FrameStore */
		pthread_cond_t m_store_space_cond;




		/** controls the paused state of the m_pre_load_thread */
		bool m_pre_load_pause;

		/** protecs access to the pause flag */
		pthread_mutex_t m_store_pause_mutex;

		/** used to signal changes to the paused state within tihs FrameStore */
		pthread_cond_t m_store_pause_cond;



		/**
		 * Pre-load thread functionl run from the m_pre_load_thread
		 * Loops until m_pre_load_control is set false, loading/getting images and adding them to
		 * this FrameStore,
		 *
		 * @param arg instance of this FrameStire
		 */
		static void* preload_run(void* arg);



		/** delay between pre-load theread run loops */
		static const unsigned long STORE_LOOP_MILLISECS;


		//-------------------
		// Listener Handling

		typedef ListenerList<FrameStoreListener> ListenerList_t;
		ListenerList_t m_listener_list;
};

#endif /* _FRAME_STORE_H_ */
