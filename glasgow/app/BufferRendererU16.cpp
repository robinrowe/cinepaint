/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      BufferRendererU16 - Rendering for Unsigned 16bit type.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: BufferRendererU16.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "BufferRendererU16.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/CinePaintTag.h"
#include <stdio.h>

/**
 * Renders the specified row of canvas into buf.
 * The specified row is converted to a displayable RGB format and rendered into buf, suitable
 * for displaying on screen.
 * The step parameter may be used to set the step value between successive pixel from canvas,
 * this allows a canvas to be reduced in size by skipping, and only rendering every step pixels
 * 
 * @todo error/bounds checking what do we do if we try to draw past the end of the canvas?
 * @param buf the data buffer to render canvas data into
 * @param canvas the AbstractBuf to render
 * @param row the canvas row to render
 * @param offset an offset from the left of the row to render
 * @param width the number of pixels to render from the specified row
 * @param step a step value between successive pixels.
 */
void BufferRendererU16::RenderMaskRowRGB(unsigned char* buf, AbstractBuf& canvas, int row, int offset, int width, int step)
{
	if(width == -1)
	{
		width = canvas.GetWidth();
	}

	unsigned short* _s = reinterpret_cast<unsigned short*>(canvas.GetPortionData(0, 0));
	_s += (row * canvas.GetTag().GetNumChannels() * canvas.GetWidth());

	unsigned char* _b = buf;
	_b += (row * 3 * canvas.GetWidth());

	unsigned char pix;

	switch(canvas.GetTag().GetFormat())
	{
		case CinePaintTag::FORMAT_GRAY_ENUM:
		{
			// Invert the mask for display.
			for(int i = offset; i < width; i++)
			{
				// U16 to U8
				pix = 255 - ((*_s) >> 8);
				*_b++ = pix;
				*_b++ = pix;
				*_b++ = pix;
				_s += step;
			}
			break;
		}
		default:
			break;
	}
}

/**
 * Renders the specified AbstractBuf as RGB data into buf.
 * This method inverts an image mask for display
 *
 * @param buf the data buffer to render the AbstractBuf into
 * @param canvas the AbstractBuf to render
 * @param width the number of pixels to render from the specified row
 * @param step a step value between successive pixels.
 */
void BufferRendererU16::RenderMaskRGB(unsigned char* buf, AbstractBuf& canvas, int width, int step)
{
	for(int i = 0; i < canvas.GetHeight(); i++)
	{
		RenderMaskRowRGB(buf, canvas, i, 0, width, step);
	}
}
