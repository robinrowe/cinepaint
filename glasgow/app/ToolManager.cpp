/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Tool Operation plugin loading module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ToolManager.cpp,v 1.2 2006/06/08 17:58:59 robinrowe Exp $
 */

#include "ToolManager.h"
#include "plug-ins/pdb/CinePaintTool.h"
#include "app/CinePaintApp.h"
#include "plug-ins/pdb/CinePaintTool.h"
#include "AppSettings.h"
#include <dirent.h>
#include <sys/types.h>
#include <dlfcn.h>
#include <algorithm>
#include <map>

/**
 * Constructs a new Tool Manager initially managing no Tools
 */
ToolManager::ToolManager()
{
	m_selected_tool = 0;
	m_prev_tool = 0;
}

/**
 * Destructor
 */
ToolManager::~ToolManager()
{
	ToolContainer_t::iterator iter = m_tools.begin() ;

	while(iter != m_tools.end())
	{
		ToolContainer_t::iterator next = iter;
		++next;

		CinePaintTool* _t = iter->second;
		m_tools.erase(iter);

		delete _t;
		_t = 0;

		iter = next;
	}
}

//-----------------
// Tool management

/**
 * Register the specified CinePaintTool with this ToolManager with the spcified tool name.
 * If the specified CinePaintTool is a 'core' application tool, one of the standard tool name strings
 * defined within CinePaintTool should be used.
 * This ToolManager assumes responsibilty for the resources of the registered Tool.
 * If a tool is already registered with the specified tool name, the specified tool cannot be registered
 * against the specified name until the existing tool is removed, in this case this method returns false.
 *
 * @param tool_type the type of the tool being registered
 * @param tool the CinePaintTool to register
 * @return true if the tool was registered successfully, false otherwise
 */
bool ToolManager::RegisterTool(const char* tool_type, CinePaintTool* tool)
{
	printf("RegisterTool: %s\n",tool_type);
	bool _ret = false;

	ToolContainer_t::iterator _iter = m_tools.find(tool_type);

	if(_iter == m_tools.end())
	{
		m_tools.insert(std::make_pair(tool_type, tool));
		_ret = true;
	}

	return(_ret);
}

/**
 * Registers the specified CinePaintTool with this ToolManager
 * The CinePaintTool is registered with the tool name returned by the CinePaintTool itself.
 *
 * @param tool the CinePaintTool to register
 * @return true if the tool was registered successfully, false otherwise
 */
bool ToolManager::RegisterTool(CinePaintTool* tool)
{
	return(RegisterTool(tool->GetToolType(), tool));
}

static const char* TOOL_PATH_SUFFIX = "tools";

/**
* Initializes, and loads available CinePaintTool plugins from an application specified path
*
*/
void ToolManager::Initialize()
{
	// The directory from which to load the main tools.
//	char _tool_dir[PATH_MAX + 1];
//	char* _tmp_dir;


	const char* _full_path = GetPathToolsPlugin();


	// open the module
	void* _tool_lib = dlopen(_full_path, RTLD_LAZY);

	// Pointer to the create tool functions
	CinePaintTool::PFNCreateCinePaintTool* _ToolCtor = 0;
	CinePaintTool::PFNGetToolClassList _ToolListFunc = 0;

	const char* _dlErrMsg = 0;

	if (_tool_lib)
	{
		// keep the opened handle so we can clean up properly at shutdown
		m_open_plugins.push_back(_tool_lib);

		_ToolListFunc = (CinePaintTool::PFNGetToolClassList)dlsym(_tool_lib, "GetToolClassList");

		if (_ToolListFunc)
		{
			_ToolCtor = _ToolListFunc();
			while (*_ToolCtor)
			{
				// Construct and keep a pointer to the 'Core Tool'
				// These plugins are 'core' to the application, so we do not create and destroy on-the-fly,
				// we always want these to be available. If the construction method return non-zero, we
				// register the tool with this ToolManager for future use. The ToolManager assumes the reponsiblity
				// of the resource from this point onwards.

				CinePaintTool* _tool = (*_ToolCtor)();

				if (_tool)
				{
					//snprintf(_info_txt, sizeof(_info_txt), "Registering Tool: %s", _tool->GetToolType());
					//init_progress_set_text(_info_txt);

					RegisterTool(_tool);

					//init_progress_update();
				}

				++_ToolCtor;
			}
		}
		else
		{
			_dlErrMsg = dlerror();
			printf("Failed to load Tool Plugin, %s, No entry point not Found\nError is: %s\n", _full_path, _dlErrMsg);
		}
	}
	else
	{
		_dlErrMsg = dlerror();
		printf("Failed to open Tool Plugin: %s\nError is: %s\n", _full_path, _dlErrMsg);
	}
	/*
	if(_plugin_full_path)
	{
	strncpy(_tool_dir, _plugin_full_path, strlen(_plugin_full_path) +1);
	}
	else
	{
	const char *tool_path_prefix = "";
	printf("Warning: No Tool Plugin path defined, trying \"%s/%s/\" instead\n",
	tool_path_prefix, TOOL_PATH_SUFFIX);
	strncpy(_tool_dir, tool_path_prefix, strlen(tool_path_prefix) +1);
	if(strncmp(_tool_dir + strlen(_tool_dir) -1, "/", 1) != 0)
	{
	strcat(_tool_dir, "/");
	}
	strcat(_tool_dir, TOOL_PATH_SUFFIX);
	}

	char _full_path[PATH_MAX + 1];
	char _info_txt[PATH_MAX + 1];

	// Pointer to the create tool functions
	CinePaintTool::PFNCreateCinePaintTool* _ToolCtor = 0;
	CinePaintTool::PFNGetToolClassList _ToolListFunc = 0;

	const char* _dlErrMsg = 0;

	struct dirent* _item;
	DIR* _dir;
	if((_dir = opendir(_tool_dir)) != 0)
	{
	//int _tool_count = 0;
	//init_progress_reset(0, _tool_count, 1);

	while((_item = readdir(_dir)) != 0)
	{
	// Tool Plugins have a '.so' extension and are not dotfiles
	// there is no requirement for a 'lib' prefix, which is probably only confusing anyways
	// but if is not refused either
	if((strlen(_item->d_name) > 3) && (strncmp(_item->d_name, ".", 1) != 0) &&
	(strncmp(_item->d_name + strlen(_item->d_name) - 3, ".so", 3) == 0))
	{
	snprintf(_full_path, sizeof(_full_path), "%s/%s", _tool_dir, _item->d_name);
	snprintf(_info_txt, sizeof(_info_txt), "Loading Tool: %s", _item->d_name);
	//init_progress_set_text(_info_txt);

	// open the module
	void* _tool_lib = dlopen(_full_path, RTLD_LAZY);

	if(_tool_lib)
	{
	// keep the opened handle so we can clean up properly at shutdown
	m_open_plugins.push_back(_tool_lib);

	_ToolListFunc = (CinePaintTool::PFNGetToolClassList)dlsym(_tool_lib, "GetToolClassList");

	if(_ToolListFunc)
	{
	_ToolCtor = _ToolListFunc();
	while(*_ToolCtor)
	{
	// Construct and keep a pointer to the 'Core Tool'
	// These plugins are 'core' to the application, so we do not create and destroy on-the-fly,
	// we always want these to be available. If the construction method return non-zero, we
	// register the tool with this ToolManager for future use. The ToolManager assumes the reponsiblity
	// of the resource from this point onwards.

	CinePaintTool* _tool = (*_ToolCtor)();

	if(_tool)
	{
	//snprintf(_info_txt, sizeof(_info_txt), "Registering Tool: %s", _tool->GetToolType());
	//init_progress_set_text(_info_txt);

	RegisterTool(_tool);

	//init_progress_update();
	}

	++_ToolCtor;
	}
	}
	else
	{
	_dlErrMsg = dlerror();
	printf("Failed to load Tool Plugin, %s, No entry point not Found\nError is: %s\n", _full_path, _dlErrMsg);
	}
	}
	else
	{
	_dlErrMsg = dlerror();
	printf("Failed to open Tool Plugin: %s\nError is: %s\n", _full_path, _dlErrMsg);
	}
	}
	}

	closedir(_dir);
	}
	else
	{
	printf("Could not open Tool Dir: %s\n", _tool_dir);
	}
	*/
}

void ToolManager::ShutDown()
{
	std::list<void*>::iterator iter = m_open_plugins.begin();

	while (iter != m_open_plugins.end())
	{
		dlclose(*iter);

		m_open_plugins.erase(iter);
	}
}





//------------------------
// Tool Selection Handling

/**
 * Sets the currently selected tool to the tool registered with the spacified name
 * If the specified name does not match a tool currently registered, the current tool selection is set to 0.
 * If the specified CinePaintTool is a 'core' application tool, one of the tool type strings
 * defined within CinePaintTool should be used.
 *
 * @param tool_type the registered type of the tool to set currently selected
 */
void ToolManager::SetSelectedTool(const char* tool_type)
{
	ToolContainer_t::const_iterator _citer = m_tools.find(std::string(tool_type));
	if(_citer != m_tools.end())
	{
		m_prev_tool = m_selected_tool;
		m_selected_tool = _citer->second;

		post_tool_changed_update(tool_type);
	}
	else
	{
		m_prev_tool = m_selected_tool;
		m_selected_tool = 0;

		post_tool_changed_update(0);
	}
}

/**
 * Sets the currently selected tool to the specified tool.
 * If this specified tool is not managed by this ToolManager, the currently selected tool is set to 0.
 *
 * @param tool the tool to set currently selected
 */
void ToolManager::SetSelectedTool(CinePaintTool* tool)
{
	// we manage the tool, right?
	bool _found = false;
	const char* _tool_type = 0;
	ToolContainer_t::const_iterator _citer = m_tools.begin();
	while(_citer != m_tools.end() && !_found)
	{
		if(_citer->second == tool)
		{
			_found = false;
			_tool_type = _citer->first.c_str();
		}

		++_citer;
	}

	if(_found)
	{
		m_prev_tool = m_selected_tool;
		m_selected_tool = tool;
	}
	else
	{
		m_prev_tool = m_selected_tool;
		m_selected_tool = 0;
	}

	post_tool_changed_update(_tool_type);
}


/**
 * Returns the currently selected CinePaintTool
 * If not tool is currently selected, this method returns 0
 *
 * @return the currently selected CinePaintTool
 */
CinePaintTool* ToolManager::GetSelectedTool() const
{
	return(m_selected_tool);
}





//----------------
// Change Listener

/**
 * Adds the specified ToolChangeListener to receive notification upon tool changes
 * if the specified ToolChangeListener has already been added it is not added again.
 * The resources of the specified ToolChangeListener are not managed by this class
 *
 * @param tcl the ToolChangeListener to be added
 */
void ToolManager::AddToolChangeListener(ToolManager::ToolChangeListener* tcl)
{
	m_listener_list.AddListener(tcl);
}

/**
 * Removes the specified ToolChangeListener
 * The removed ToolChangeListener will no longer receive tool change notifications from
 * this class
 *
 * @param tcl the ToolChangeListener to be removed
 */
void ToolManager::RemoveToolChangeListener(ToolManager::ToolChangeListener* tcl)
{
	m_listener_list.RemoveListener(tcl);
}




//----------------
// Iterator access


/**
 * Returns a constant iterator at the beginning of this ToolManager
 * The returned iteraotr iterates over ToolManager::ToolPair_t's containing
 * the tool type and the tool itself
 *
 * @return a constant iterator at the beginning of this ToolManager
 */
ToolManager::const_iterator ToolManager::begin() const
{
	return(m_tools.begin());
}

/**
 * Returns a constant iterator past the end of this ToolManager
 *
 * @return a consdtant iterator past the end of this ToolManager
 */
ToolManager::const_iterator ToolManager::end() const
{
	return(m_tools.end());
}



/**
 * Posts a ToolChanged message to all registered ToolChangeListeners
 *
 * @param tool_type the currently selected tool type
 */
void ToolManager::post_tool_changed_update(const char* tool_type)
{
	for(ListenerList_t::const_iterator citer = m_listener_list.begin(); citer != m_listener_list.end(); ++citer)
	{
		(*citer)->ToolChanged(tool_type);
	}
}

