/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      BrushList - manages loaded CinePaint brushes
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: BrushList.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CINEPAINT_BRUSHLIST_H_
#define _CINEPAINT_BRUSHLIST_H_

#include "dll_api.h"

#include "CinePaintBrush.h"

#include <functional>
#include <vector>

#include <string>

class CINEPAINT_PAINTING_API BrushList
{
	public:
		/**
		 * Construct a new initially empty BrushList
		 *
		 */
		BrushList();

		/**
		 * Destructor
		 */
		~BrushList();


		//------------
		// List Access

		/**
		 * Adds the specified brush to this BrushList
		 *
		 * @param brush the CinePaintBrush to add

		 */
		void AddBrush(CinePaintBrush* brush);

		/**
		 * Removes the specified brush from this BrushList
		 * The caller must take responsibility for the resources of the removed brush.
		 *
		 * @param brush the CinePaintBrush to remove
		 * @return the removed brush
		 */
		CinePaintBrush* RemoveBrush(CinePaintBrush* brush);

		/**
		 * Sorts this BrushList by the name of each brush
		 *
		 */
		void Sort();

		/**
		 * Clears this BrushList
		 *
		 */
		void ClearBrushList();

		/**
		 * Returns the number of brushed this BrushList holds
		 *
		 * @return the number of brushes this BrushList contains
		 */
		size_t Size() const;



		//-------------
		// Brush Access

		/**
		 * Returns the CinePaintBrush with the specified name
		 * If the named brush does not exist, 0 is returned
		 *
		 * @param name the name of the brush to return
		 */
		CinePaintBrush* GetBrush(const char* name) const;

		/**
		 * Returns a CinePaintBrush by its index within this BrushList
		 * The index of a brush is not gauranteed to remain the same
		 *
		 * @param index the index within this BrushList
		 */
		CinePaintBrush* GetBrushByIndex(unsigned int index) const;

		/**
		 * Returns the index within this BrushList of the specified brush
		 * The index of a brush is not gauranteed to remain the same
		 *
		 * @param brush the brush to get the index of
		 * @return the current index of the spexified brush
		 */
		unsigned int GetBrushIndex(CinePaintBrush* brush) const;

		/**
		 * Return sthe currently active brush
		 *
		 * @return the currently active brush
		 */
		CinePaintBrush* GetActiveBrush() const;

		/**
		 * Sets the specified brush as the currently selected brush
		 *
		 * @param brush the brush to set currently selected
		 */
		void SelectBrush(CinePaintBrush* brush);


		//----------------------
		// active brush settings
		// @note [claw] these should prbably be moved out of the BrushList and into an active tool object

		/**
		 * Noise data structure
		 */
		struct BrushNoiseInfo
		{
			double freq;
			double step_start;
			double step_width;
			double freq_variation;
		}; 

		void SetNoiseMode(bool noiseOn);
		bool GetNoiseMode() const;

		void SetNoiseInfo(const BrushNoiseInfo& info);
		void GetNoiseInfo(BrushNoiseInfo& info) const;

		void SetOpacity(double opac);
		double GetOpacity() const;

		void SetPaintMode(int pm);
		int GetPaintMode() const;



		//--------------
		// Brush Loading

		/**
		 * Loads the specified brush file and adds it to this brushList
		 *
		 * @param filename the brush file to load
		 */
		void LoadBrush(const char* filename);

	protected:

	private:
		/**
		 * Creates and returns a default genereated brush
		 *
		 * @return default generated brush
		 */
		CinePaintBrush* create_default_brush();

		/**
		 * Alters the name of the specified brush to so that the brush name is unique within this brushList
		 * If the brush name matches and existing brush within this BrushList, it is appended with a # and digit
		 * to distinguish it
		 *
		 * @param brush the brush with which to uniquefy the name
		 */
		void uniquefy_brush_name(CinePaintBrush* brush);

		/**
		 * Determines whether the specified brush's name is unique within this BrushList
		 *
		 * @param brush the brush to check the name of
		 * @return true if the specified brush's name is unique within this BrushList
		 */
		bool brush_name_unique(const char* brush) const;



		// the brush list container
		typedef std::vector<CinePaintBrush*> BrushContainer_t;
		BrushContainer_t m_brush_container;

		/** the currently active brush */
		CinePaintBrush* m_active_brush;

		/** current opacity settings */
		double m_opacity;

		/** current paint mode */
		int m_paint_mode;

		/** noise on or off */
		bool m_noise_mode ;

		/** noise data */
		BrushNoiseInfo m_brush_noise_info;

		/**
		 * Nested class used to sort the BrushList
		 */
		class BrushListComparator : public std::binary_function<const CinePaintBrush*, const CinePaintBrush*, bool>
		{
			public:
				result_type operator() (first_argument_type p, second_argument_type q) const { return(strcmp(p->GetName(), q->GetName()) < 0) ;} ;
		};


		static const int DEFAULT_NAME_UNIQUEFY_LENGTH;

}; /* class BrushList */

#endif /* _CINEPAINT_BRUSHLIST_H_ */
