/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintBrush - Paint brush class
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintBrush.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "CinePaintBrush.h"
//#include <config.h>
#include "plug-ins/pdb/CinePaintTag.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include <fstream>
#include <string>

const unsigned int CinePaintBrush::DEFAULT_BRUSH_SPACING = 25;


/**
 * Constructs a new CinePaintBrush using the specified brushmask
 * Protected Constructor. CinePaintBrush objects are constructed through the static LoadCinePaintBrush method
 *
 * @param filename the filename the brush data was originally read from
 * @param brushname the name of this Brush
 * @param brushmask the brush mask data
 * @param spacing
 * @param x_axis
 * @param y_axis
 */
CinePaintBrush::CinePaintBrush(const char* filename, const char* brushname, AbstractBuf* brushmask, float spacing, const Vector2d& x_axis, const Vector2d& y_axis)
: m_x_axis(x_axis), m_y_axis(y_axis)
{
	m_filename = new char[strlen(filename) +1];
	strcpy(m_filename,filename);

    m_name = new char[strlen(brushname) +1];
	strcpy(m_name, brushname);

	m_mask = brushmask;
	m_spacing = spacing;
}


/**
 * Destructor
 */
CinePaintBrush::~CinePaintBrush()
{
	if(m_filename)
	{
		delete[] m_filename;
		m_filename = 0;
	}

	if(m_name)
	{
		delete[] m_name;
		m_name = 0;
	}

	if(m_mask)
	{
		delete m_mask;
		m_mask = 0;
	}
}


/**
 * Returns the name of this brush
 * The returned char* remains owned by this class.
 *
 * @return the name of this brush
 */
const char* CinePaintBrush::GetName() const
{
	return(m_name);
}


/**
 * Sets the name of this brush
 * The name parameter is copied by this class
 *
 * @param name the new brush name
 */
void CinePaintBrush::SetName(const char* name)
{
	if(strcmp(name, m_name) != 0)
	{
		delete[] m_name;
		m_name = new char[strlen(name) +1];
		strcpy(m_name, name);

		// @note [Claw] how are we going to handle signals with fltk??
		//gtk_signal_emit(GTK_OBJECT(brush), gimp_brush_signals[RENAME]);
	}
}


/**
 * Returns the AbstractBuf of the brush mask data
 *
 * @return the brush mask AbstractBuf
 */
AbstractBuf* CinePaintBrush::GetMask() const
{
	return(m_mask);
}

void CinePaintBrush::SetSpacing(float spacing)
{
	m_spacing = spacing;
}

float CinePaintBrush::GetSpacing() const
{
	return(m_spacing);
}

void CinePaintBrush::SetXAxis(const Vector2d& x_axis)
{
	m_x_axis.Set(x_axis.GetX(), x_axis.GetY());
}

const Vector2d& CinePaintBrush::GetXAxis() const
{
	return(m_x_axis);
}

void CinePaintBrush::SetYAxis(const Vector2d& y_axis)
{
	m_y_axis.Set(y_axis.GetX(), y_axis.GetY());
}

const Vector2d& CinePaintBrush::GetYAxis() const
{
	return(m_y_axis);
}





// protected

/**
 * Sets the brush mask of this brush to the specified AbstractBuf
 *
 * @param mask the new brush mask
 */
void CinePaintBrush::SetMask(AbstractBuf* mask)
{
	if(m_mask)
	{
		// @note [claw] need to check references here
		delete m_mask;
		m_mask = 0;
	}

	m_mask = mask;
}

