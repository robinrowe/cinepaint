/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ColorManager - Manages currently selected Color
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ColorManager.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _COLOR_MANAGER_H_
#define _COLOR_MANAGER_H_

#include "dll_api.h"
#include "plug-ins/pdb/Color.h"
#include "app/ColorChangeListener.h"
#include <list>

/**
 * Manages the current foregound/background color selection
 * The current foreground/background color copied as the previous value if the new value
 * differs from the current value. This allows access to the previous used Color.
 *
 */
class CINEPAINT_COLOR_API ColorManager
{
	public:
		/**
		 * Constructs a new Colormanager initializing the foreground to black, and the background white
		 *
		 */
		ColorManager()
		:	m_foreground(ColorManager::GetDefaultForeground()), 
			m_prev_foreground(ColorManager::GetDefaultForeground()), 
			m_background(ColorManager::GetDefaultBackground()),
			m_prev_background(ColorManager::GetDefaultBackground())
		{}

		/**
		 * Constructs a new Colormanager initialized to the specified foreground and background colors
		 *
		 * @param fg the foreground color
		 * @param gb the background color
		 */
		ColorManager(const Color& fg, const Color& bg)
		:	m_foreground(fg), 
			m_prev_foreground(fg), 
			m_background(bg), 
			m_prev_background(bg)
		{}

		// default destructor is OK



		//---------------------
		// Accessors / Mutators


		/**
		 * Sets the foreground Color to the specified Color
		 *
		 * @param c the new foreground Color
		 */
		void SetForeGround(const Color& c)
		{
			if(c != m_foreground)
			{
				m_prev_foreground = m_foreground;
				m_foreground.Set(c);

				post_color_changed_update(ColorChangeListener::FOREGROUND_ENUM, m_foreground);
			}
		}

		/**
		 * Sets the background color to the specified Color
		 *
		 * @param c the new background color
		 */
		void SetBackGround(const Color& c)
		{
			if(c != m_background)
			{
				m_prev_background = m_background;
				m_background.Set(c);

				post_color_changed_update(ColorChangeListener::BACKGROUND_ENUM, m_background);
			}
		}

		/**
		 * Returns the current foreground Color
		 *
		 * @return the current foreground color
		 */
		const Color& GetForeGround()
		{
			return(m_foreground);
		}

		/**
		 * Returns the current background Color
		 *
		 * @return the current background color
		 */
		const Color& GetBackGround()
		{
			return(m_background);
		}

		/**
		 * Returns the previous foreground Color
		 *
		 * @return the previous foreground color
		 */
		const Color& GetPrevForeGround()
		{
			return(m_prev_foreground);
		}

		/**
		 * Returns the previous background color
		 *
		 * @return the previous foreground color
		 */
		const Color& GetPrevBackGround()
		{
			return(m_prev_background);
		}


		/**
		 * Swaps the foreground and background Colors
		 *
		 */
		void Swap()
		{
			Color _temp_color = m_foreground;
			m_foreground = m_background;
			m_background = _temp_color;

			post_color_changed_update(ColorChangeListener::FOREGROUND_ENUM, m_foreground);
			post_color_changed_update(ColorChangeListener::BACKGROUND_ENUM, m_background);
		}



		//----------------
		// Change Listener

		/**
		 * Adds the specified ColorChangeListener to receive notification upon color changes
		 * if the specified ColorChangeListener has already been added it is not added again.
		 * The resources of the specified ColorChangeListener are not managed by this class
		 *
		 * @param ccl the ColorChangeListener to be added
		 */
		void AddColorChangeListener(ColorChangeListener* ccl);

		/**
		 * Removes the specified ColorChangeListener
		 * The removed ColorChangeListener will no longer receive Color change notifications from
		 * this class
		 *
		 * @param ccl the ColorChangeListener to be removed
		 */
		ColorChangeListener* RemoveColorChaneListener(ColorChangeListener* tcl);

		/** default foreground color */
		static Color GetDefaultForeground()
		{	return Color(0.0, 0.0, 0.0, 1.0);
		}

		/** default background color */
		static Color GetDefaultBackground()
		{	return Color(1.0, 1.0, 1.0, 1.0);
		}

	protected:

	private:
		/** current foreground Color */
		Color m_foreground;

		/** previous foreground Color */
		Color m_prev_foreground;

		/** current background Color */
		Color m_background;

		/** previous background Color */
		Color m_prev_background;


		/**
		 * post a ColorChanged message to all registered ColorChangeListeners
		 *
		 * @param color_type the color to have changed
		 * @param c the new Color
		 */
		void post_color_changed_update(ColorChangeListener::ColorType color_type, const Color& c);

		typedef std::list<ColorChangeListener*> ListenerList_t;

		/** list of registered ColorChangeListerner */
		ListenerList_t m_color_change_listeners;

}; /* class ColorBox */


#endif /* _COLOR_MANAGER_H_ */
