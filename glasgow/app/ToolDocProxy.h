/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ToolProxy - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ToolDocProxy.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CINEPAINT_TOOL_DOC_PROXY_H_
#define _CINEPAINT_TOOL_DOC_PROXY_H_

#include "dll_api.h"

#include "CinePaintDocListListener.h"
#include "ToolManager.h"

class CinePaintTool;
class CinePaintDoc;

/**
 * ToolProxy acts as a proxy to the currently selcted tool
 * ToolProxy implements ToolChangeListener and CinePaintMouseListener to respond to changes
 * in the currently selected tool and passes mouse events to that tool without the need
 * to register and unregister tools individually, as they are selected, to receive mouse
 * events
 *
 */
class CINEPAINT_CORE_API ToolDocProxy : public ToolManager::ToolChangeListener, public CinePaintDocListListener
{
	public:
		ToolDocProxy();

		/**
		 * Destructor
		 */
		~ToolDocProxy();

		/**
		 * Invoked when the currently selected tool is changed
		 *
		 * @param tool_type the type of the newly selected tool
		 */
		virtual void ToolChanged(const char* tool_type);



		/**
		 * Invoked when the current document is changed.
		 * @param doc the new active document.
		**/
		virtual void CurrentDocChanged(const char* doc_id, CinePaintDoc* doc);

		// Not interesed in these messages.
		virtual void DocOpened(const char* doc_id, CinePaintDoc& doc)  {};
		virtual void DocClosed(const char* doc_id, CinePaintDoc& doc)  {};


	protected:

	private:

		/** the currently selected tool */
		CinePaintTool* m_selected_tool;
		CinePaintDoc* m_active_doc;

}; /* class ToolProxy */

#endif /* _CINEPAINT_TOOL_DRIVER_H_ */
