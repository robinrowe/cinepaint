/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintKeyListener - Abstract key listener interface
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintKeyListener.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CINEPAINT_KEY_LISTENER_H_
#define _CINEPAINT_KEY_LISTENER_H_

#include "dll_api.h"

/**
 * Purely Abstract class defining a CinePaintKeyListener interface.
 * Classes interested in receiving key events should implement this interface
 *
 * @todo need to clarify mouse buttons
 */
class CINEPAINT_CORE_INTERFACES_API CinePaintKeyListener
{
	public:
		/**
		 * Empty Destructor
		 */
		virtual ~CinePaintKeyListener() {};

		/**
		 * Invoked when a key is pressed
		 *
		 * @param key the key id pressed
		 * @param text the text that would be inserted from the key press
		 * @param modifier any keyboard modifier key being pressed
		 */
		virtual void KeyPressed(int key, const char* text, unsigned int modifier) = 0;

	protected:
		/**
		 * Protected constructor - disallow instances
		 */
		CinePaintKeyListener() {} ;

}; /* class CinePaintKeyListener */

#endif /* _CINEPAINT_KEY_LISTENER_H_ */
