/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      FlipFrameController - Frame Manager flipframe controller
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: FlipFrameController.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _FLIPFRAME_CONTROLLER_H_
#define _FLIPFRAME_CONTROLLER_H_

#include "dll_api.h"
#include "FrameManagerListener.h"
#include "FrameStoreListener.h"
#include "CinePaintDocListListener.h"

#include <list>
#include <set>

#include <pthread.h>

class CinePaintImage;
class FrameManager;
class FrameStore;
class SimpleFlipFrameDriver;


/**
 * FlipFrameController provides the top level controller for automatic driving, or 'flipping' of a FrameManager
 * The FlipFrameController may be used to automatically flip between frames within a FrameManager, updating
 * the currently selected Frame. This FlipFrameController uses an internal FlipFrameDriver to periodically
 * update the FrameManager.
 *
 */
class CINEPAINT_CORE_API FlipFrameController
{
	public:
		/**
		 * Constructs a new FlipFrameController to control the specified FrameManager
		 *
		 * @param frameManager the FrameManager to control
		 * @param store the store used to pre-load images
		 */
		FlipFrameController(FrameManager& frame_manager, FrameStore& store);

		/**
		 * Destructor
		 */
		~FlipFrameController();

		/** directions in which this FlipFrameController may run */
		enum Direction { FORWARD_ENUM, BACKWARD_ENUM };

		/**
		 * Steps the current selection within the FrameManager by one place in the specified Directipn
		 *
		 * @param dir the Direction in which to step the FrameManager
		 */
		void Step(Direction dir);

		/**
		 * Initiates the automatic 'flipping' of the FrameManager in the specified direction
		 *
		 * @param dir the Direction in which to Play the FrameManager
		 */
		void Play(Direction dir);

		/**
		 * Stops the matic 'flipping' of the FrameManager
		 *
		 */
		void Stop();

		/**
		 * Ensures the FrameStore used to pre-load images is buffered before Initiates the automatic 'flipping' of the FrameManager.
		 * The associated FrameStore may be used to pre-load a certain number of images for processing, this method ensures
		 * that the FrameStore is buffered before initiating the automatic 'flipping'
		 *
		 * @param dir the Direction in which to Play the FrameManager
		 */
		void BufferAndPlay(Direction dir);

		/**
		 * Returns the Direction in which this FlipFrameController is currently flipping
		 *
		 * @return the Direction this FlipFrameController is currently flipping
		 */
		Direction GetDirection() const;




		//
		// @todo is there someplace better these methods belong?!
		//

		/**
		 * Registers a modified CinePaintDoc with this FlipFrameController.
		 * If the specified image is already registered, it is not registered a seccond time, and false is returned,
		 * otherwise true is returned.
		 * This method exists to avoid Unreferencing the last reference to a CinePaintImage during flipping.
		 * As each CinePaintImage is loaded (by the FrameStore via the CinePaintDocList) a reference to the image
		 * is added. Generally, when the last reference is released, we should close the CinePaintImage.
		 * However, if an image is modified, asking the user if the image should be saved during the flipping may not
		 * be desirable, thie method allows a reference to be maintained so that the image can be saved at a later point
		 *
		 * @todo this needs some review!
		 * @param the Image to register
		 * @return true if the image was indeed registered, false otherwise
		 */
		bool RegisterModifiedDocument(CinePaintImage& image);

		/**
		 * Unregisters the specified image from this FlipFrameController
		 * If the specified image has not been previously registered, false is returned, otherwise
		 * the image is unregistered, decrementing its reference count by one within the CinePaintDocList
		 * and true is returned.
		 *
		 * @todo this needs some review!
		 * @param the Image to unregister
		 * @return trus if the image was unregistered
		 */
		bool UnRegisterModifiedDocument(CinePaintImage& image);

	protected:
	private:
		void start_flip_frame();

		/** frame manager rhis handler is updating */
		FrameManager& m_frame_manager;

		/** frame store from which to get image data */
		FrameStore& m_frame_store;

		SimpleFlipFrameDriver* m_flip_driver;

		/** current flipping direction */
		Direction m_direction;


		/** controls the looping of the buffer run thread */
		bool m_buffer_run_flag;

		/** thread to run the buffer fill loop */
		pthread_t m_buffer_run_thread;

		/** thread run function for the buffer run loop */
		static void* buffer_run(void* arg);





		//
		// @todo is there someplace better these methods belong?!
		//

		typedef std::set<CinePaintImage*> ReferenceStore_t;

		/** container for the holding references to the modified CinePaintImages */
		ReferenceStore_t m_image_references;


		class StoreHandler : public FrameStoreListener
		{
			public:
				StoreHandler(ReferenceStore_t& refs, FrameManager& fm);
				virtual ~StoreHandler();

				void FrameAdded(const FrameManager::FrameRecord& frame, CinePaintImage& image);
				void FrameRemoved(const FrameManager::FrameRecord& frame, CinePaintImage& image);

			protected:
			private:
				ReferenceStore_t& m_image_references;
				FrameManager& m_frame_manager;
		};

		class DocListHandler : public CinePaintDocListListener
		{
			public:
				DocListHandler(FlipFrameController& controller, ReferenceStore_t& refs, FrameManager& fm);
				virtual ~DocListHandler();

				virtual void DocOpened(const char* doc_id, CinePaintDoc& doc);
				virtual void DocClosed(const char* doc_id, CinePaintDoc& doc);
				virtual void CurrentDocChanged(const char* doc_id, CinePaintDoc* doc);

			protected:
			private:
				FlipFrameController& m_controller;
				ReferenceStore_t& m_image_references;
				FrameManager& m_frame_manager;
		};

		class StoreMaintainer : public FrameManagerListener
		{
			public:
				StoreMaintainer(FrameStore& store, FrameManager& frame_manager, FlipFrameController& controller);
				virtual ~StoreMaintainer();

				virtual void SelectionChanged(const FrameManager::FrameRecord* frame, FrameManager::size_type position);
				virtual void FrameAdded(const FrameManager::FrameRecord& frame, FrameManager::size_type position);
				virtual void FrameRemoved(const FrameManager::FrameRecord& frame, FrameManager::size_type position);
				virtual void FrameUpdated(const FrameManager::FrameRecord& frame, FrameManager::size_type position);
				virtual void FrameIndexChanged(const FrameManager::FrameRecord& frame, FrameManager::size_type old_pos, FrameManager::size_type new_pos);

			protected:
			private:
				FrameStore& m_store;
				FrameManager& m_frame_manager;
				FlipFrameController& m_controller;
		};

		StoreHandler* m_store_handler;
		DocListHandler* m_doc_handler;
		StoreMaintainer* m_store_maintainer;
};

#endif /* _FLIPFRAME_CONTROLLER_H_ */
