/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintPlugin Interface
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintEvent.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CINEPAINTEVENT_H_
#define _CINEPAINTEVENT_H_

#include "dll_api.h"

/**
 * CinePaintEvent provides an abstract interface for an arbitary event to process within an event loop
 * CinePaintEvent should be implemented to provide the specific process functionality required during the
 * process loop
 * A CinePaintEvent should be added to a CinePaintEventList. The managed state of the event indicates to
 * the processing event list that if the event is unmanaged, the resource should be deleted after processing
 * he event. If IsManaged() returns true, the event is managed by an external object and so should not be
 * deleted after processing
 */
class CINEPAINT_CORE_API CinePaintEvent
{
	public:
		/**
		 * Destructor
		 */
		virtual ~CinePaintEvent() {} ;

		/**
		 * Process this CinepaintEvent
		 * Subclasses should implement this for specific event processing
		 */
		virtual void Process() = 0 ;

		/**
		 * Returns whether this CinepaintEvent is managed by an external process.
		 * An event which is not managed indicates that is should be released once
		 * processed.
		 *
		 * @return true if this CinePaintEvent is managed by an extranal process
		 */
		bool IsManaged() const
		{	return(m_managed);
		}

	protected:
		/**
		 * Constrcts a new CinePaintEvent - protected to avoid attempted instances of abstract class
		 *
		 * @param managed set true if this CinePaintEvent is managed by an external process, false otherwise
		 */
		CinePaintEvent(bool managed)
		: m_managed(managed)
		{}

	private:
		/** managed state, if false the event should be released after processing */
		bool m_managed;

}; /* class CinepaintEvent */

#endif /* CinePaintEvent */
