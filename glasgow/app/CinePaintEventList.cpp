/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintEventList - UI Event list.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintEventList.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "CinePaintEventList.h"
#include "CinePaintEvent.h"

/**
 * Constructs an initially empty event list
 */
CinePaintEventList::CinePaintEventList()
{
	pthread_mutex_init(&m_event_list_lock, 0);
}

/**
 * Destructor, will destroy all unmanaged events without calling their Process method
 */
CinePaintEventList::~CinePaintEventList()
{
	while(!m_event_list.empty())
	{
		CinePaintEvent* e = m_event_list.front();
		m_event_list.pop_front();

		if(!e->IsManaged())
		{
			delete e;
			e = 0;
		}
		// else ... somebody else manages the resource
	}
}

/**
 * Process the first element within this Event list.
 * Events added to the list during a call to Process are not immediatly processed. These events
 * will be processed within subsequent calls to Process upon this Event List.
 * A call to Process returns true if an event was actually process, false otherwise, for example
 * if process was called upon an empty event list.
 *
 * @return true if any events were processed, false otherwise
 */
bool CinePaintEventList::Process()
{
	bool _ret = false;

	if(!m_event_list.empty())
	{
		// remove front item from the list
		pthread_mutex_lock(&m_event_list_lock);
		CinePaintEvent* e = m_event_list.front();
		m_event_list.pop_front();
		pthread_mutex_unlock(&m_event_list_lock);

		// ... process it
		e->Process();

		// ... destroy it
		if(!e->IsManaged())
		{
			delete e;
			e = 0;
		}

		_ret = true;
	}

	return(_ret);
}

/**
 * Returns the number of CinePaintEvents within the event list
 *
 * @return the size of the event list
 */
size_t CinePaintEventList::Size() const
{
	return(m_event_list.size());
}

/**
 * Post an event into the event list to be processed
 *
 * @param e the event to add
 * @return the current size of the event list
 */
size_t CinePaintEventList::PostEvent(CinePaintEvent* e)
{
	pthread_mutex_lock(&m_event_list_lock);
	m_event_list.push_back(e);
	pthread_mutex_unlock(&m_event_list_lock);

	return(Size());
}
