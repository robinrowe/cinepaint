/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ToolManager Tool plugin loading module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ToolManager.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CINEPAINT_TOOL_MANAGER_H_
#define _CINEPAINT_TOOL_MANAGER_H_

#include "dll_api.h"
#include "plug-ins/pdb/ListenerList.h"
#include "plug-ins/pdb/CinePaintTool.h"
#include <list>
#include <map>
#include <string>

/**
 * Manages the Core CinePaint Tools list.
 * The ToolManager is reposnsible for managing registered CinePaintTools, the current and previously selected tool
 * and providing access to a selected tools.
 *
 */
class CINEPAINT_CORE_API ToolManager
{
	private:
		typedef std::map<std::string, CinePaintTool*> ToolContainer_t;

	public:
		/**
		 * Constructs a new Tool Manager initially managing no Tools
		 */
		ToolManager();

		/**
		 * Destructor
		 */
		~ToolManager();

		/**
		 * Initializes, and loads available CinePaintTool plugins from an application specified path
		 *
		 */
		void Initialize();
		
		void ShutDown();



		//-----------------
		// Tool management

		/**
		 * Register the specified CinePaintTool with this ToolManager with the spcified tool name.
		 * If the specified CinePaintTool is a 'core' application tool, one of the standard tool name strings
		 * defined within CinePaintTool should be used.
		 * This ToolManager assumes responsibilty for the resources of the registered Tool.
		 * If a tool is already registered with the specified tool name, the specified tool cannot be registered
		 * against the specified name until the existing tool is removed, in this case this method returns false.
		 *
		 * @param tool_type the name of the tool being registered
		 * @param tool the CinePaintTool to register
		 * @return true if the tool was registered successfully, false otherwise
		 */
		bool RegisterTool(const char* tool_type, CinePaintTool* tool);

		/**
		 * Registers the specified CinePaintTool with this ToolManager
		 * The CinePaintTool is registered with the tool name returned by the CinePaintTool itself.
		 *
		 * @param tool the CinePaintTool to register
		 * @return true if the tool was registered successfully, false otherwise
		 */
		bool RegisterTool(CinePaintTool* tool);





		//------------------------
		// Tool Selection Handling

		/**
		 * Sets the currently selected tool to the tool registered with the spacified name
		 * If the specified name does not match a tool currently registered, the current tool selection is set to 0.
		 * If the specified CinePaintTool is a 'core' application tool, one of the tool type strings
		 * defined within CinePaintTool should be used.
		 *
		 * @param tool_type the registered name of the tool to set currently selected
		 */
		void SetSelectedTool(const char* tool_type);

		/**
		 * Sets the currently selected tool to the specified tool.
		 * If this specified tool is not managed by this ToolManager, the currently selected tool is set to 0.
		 *
		 * @param tool the tool to set currently selected
		 */
		void SetSelectedTool(CinePaintTool* tool);


		/**
		 * Returns the currently selected CinePaintTool
		 * If not tool is currently selected, this method returns 0
		 *
		 * @return the currently selected CinePaintTool
		 */
		CinePaintTool* GetSelectedTool() const;




		//----------------
		// Change Listener

		// @note [claw] should probably separate/un-nest this

		/**
		 * ToolChangeListner interface.
		 * Classes interested in receiving Tool change notifications should implement this
		 */
		class CINEPAINT_CORE_API ToolChangeListener
		{
			public:
				virtual ~ToolChangeListener() {} ;

				/**
				 * Invoked when the currently selected tool is changed
				 *
				 * @param tool_type the type of the newly selected tool
				 */
				virtual void ToolChanged(const char* tool_type) = 0;
		};

		/**
		 * Adds the specified ToolChangeListener to receive notification upon tool changes
		 * if the specified ToolChangeListener has already been added it is not added again.
		 * The resources of the specified ToolChangeListener are not managed by this class
		 *
		 * @param tcl the ToolChangeListener to be added
		 */
		void AddToolChangeListener(ToolChangeListener* tcl);

		/**
		 * Removes the specified ToolChangeListener
		 * The removed ToolChangeListener will no longer receive tool change notifications from
		 * this class
		 *
		 * @param tcl the ToolChangeListener to be removed
		 */
		void RemoveToolChangeListener(ToolChangeListener* tcl);





		//----------------
		// Iterator access

		// @TODO this needs improved upon, we end up forcing users to know how things are contained ...
		//       we just want an iterator over the values in the map, not the key-value pairs

		typedef std::pair<std::string, CinePaintTool*> ToolPair_t;
		typedef ToolContainer_t::const_iterator const_iterator;

		/**
		 * Returns a constant iterator at the beginning of this ToolManager
		 * The returned iteraotr iterates over ToolManager::ToolPair_t's containing
		 * the tool type and the tool itself
		 *
		 * @return a constant iterator at the beginning of this ToolManager
		 */
		const_iterator begin() const;

		/**
		 * Returns a constant iterator past the end of this ToolManager
		 *
		 * @return a consdtant iterator past the end of this ToolManager
		 */
		const_iterator end() const;

	protected:

	private:

		/** container for all CinePaint tools */
		ToolContainer_t m_tools;

		/** the currently selected tool */
		CinePaintTool* m_selected_tool;

		/** previously active tool */
		CinePaintTool* m_prev_tool;



		/**
		 * Posts a ToolChanged message to all registered ToolChangeListeners
		 *
		 * @param tool_type the currently selected tool name
		 */
		void post_tool_changed_update(const char* tool_type);

		typedef ListenerList<ToolChangeListener> ListenerList_t;

		/** listeners interested in being notified upon tool changes - update may happen in an arbitary thread */
		ListenerList_t m_listener_list;
		
		std::list<void*> m_open_plugins;

}; /* class ToolManager */

#endif /* _CINEPAINT_TOOL_MANAGER_H_ */
