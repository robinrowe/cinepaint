/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintBrush - Paint brush class
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintBrushLoader.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CINEPAINT_BRUSH_LOADER_H_
#define _CINEPAINT_BRUSH_LOADER_H_

#include "dll_api.h"
#include <utility/Vector2d.h>
#include "plug-ins/pdb/CinePaintTag.h"
#include <fstream>
#include <string>

// forward declaration
class AbstractBuf;
class CinePaintBrush;

/**
 * A representation of a Brush
 */
class CINEPAINT_PAINTING_API CinePaintBrushLoader
{
	public:
		/**
		 * Destructor
		 */
		~CinePaintBrushLoader() {};

		/**
		 * Constructs and returns a new CinePaintBrush from data loaded from the specified brush data file.
		 * If the brush data file is invalid in some way, 0 is returned.
		 *
		 * @param filename the brush data file to load
		 * @return a new brush for the loaded data file
		 */
		static CinePaintBrush* LoadBrush(const char* filename);

		// Defined Image Types
		// these are used to convert between brush header type (version 3) and CinePaintTag
		static const unsigned int RGB_IMAGE;
		static const unsigned int RGBA_IMAGE;
		static const unsigned int GRAY_IMAGE;
		static const unsigned int GRAYA_IMAGE;
		static const unsigned int INDEXED_IMAGE;
		static const unsigned int INDEXEDA_IMAGE;

		// These are the 16bit types
		static const unsigned int U16_RGB_IMAGE;
		static const unsigned int U16_RGBA_IMAGE;
		static const unsigned int U16_GRAY_IMAGE;
		static const unsigned int U16_GRAYA_IMAGE;
		static const unsigned int U16_INDEXED_IMAGE;
		static const unsigned int U16_INDEXEDA_IMAGE;

		// These are the float types
		static const unsigned int FLOAT_RGB_IMAGE;
		static const unsigned int FLOAT_RGBA_IMAGE;
		static const unsigned int FLOAT_GRAY_IMAGE;
		static const unsigned int FLOAT_GRAYA_IMAGE;

		// These are the 16bit float types
		static const unsigned int FLOAT16_RGB_IMAGE;
		static const unsigned int FLOAT16_RGBA_IMAGE;
		static const unsigned int FLOAT16_GRAY_IMAGE;
		static const unsigned int FLOAT16_GRAYA_IMAGE;

		// These are the bfp types
		static const unsigned int BFP_RGB_IMAGE;
		static const unsigned int BFP_RGBA_IMAGE;
		static const unsigned int BFP_GRAY_IMAGE;
		static const unsigned int BFP_GRAYA_IMAGE;


	protected:
		/**
		 * avoid instances
		 */
		CinePaintBrushLoader() {};

	private:

		struct BrushHeader
		{
			unsigned int m_header_size;
			unsigned int m_version;
			unsigned int m_width;
			unsigned int m_height;
			unsigned int m_type;
			unsigned int m_magic_number;
			unsigned int m_spacing;
		};

		static bool read_brush_header(const char* filename, std::ifstream& fin, BrushHeader& header);
		static bool get_brush_name(const char* filename, std::ifstream& fin, BrushHeader& header, std::string& brushname);
		static bool verify_magic_number(unsigned int val);
		static unsigned int gimp_brush_bytes_to_type(unsigned int gimp_brush_bytes);
		static AbstractBuf* read_brush_data(const char* filename, std::ifstream& fin, BrushHeader& header, CinePaintTag& tag);



		// @note [claw] these need removed/replaced/forgotten about
		//       this is temporary while i'm removing the original tag cinepaint header
		//       possibly to a legacy TagUtils class?
		static CinePaintTag& tag_from_drawable_type(int drawable_type, CinePaintTag& tag);


}; /* class CinePaintBrushLoader */

#endif /* _CINEPAINT_BRUSH_LOADER_H_ */
