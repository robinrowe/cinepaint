// AppSettings.h
// rsr 7/6/06 
// license OSI MIT

#ifndef APP_SETTINGS_H
#define APP_SETTINGS_H

inline
const char* GetPathApp()
{	return "unknown";
}

inline
const char* GetPathPlugins()
{	return "plug-ins";
}

inline
const char* GetPluginsExt()
#ifdef WIN32
{	return ".dll";
#else
{	return ".so";
#endif
}

inline
const char* GetPathImageOpsPlugin()
#ifdef WIN32 //ML
{	return "plug-ins/image_ops.dll";
#else
{	return "plug-ins/image_ops.so";
#endif 
}

inline
const char* GetPathToolsPlugin()
#ifdef WIN32
{	return "plug-ins/tools.dll";
#else
{	return "plug-ins/tools.so";
#endif 
}

inline
const char* GetPathBrushes()
{	return "data/brushes";
}

inline
const char* GetPathUserHome()
{	return "home";
}

#endif

