/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintPluginExecutioner - Plugin execution control
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintPluginExecutioner.cpp,v 1.3 2006/12/21 11:18:04 robinrowe Exp $
 */

#include "CinePaintApp.h"
#include "CinePaintPluginExecutioner.h"
#include "plug-ins/pdb/CPWidgetBuilder.h"
#include "plug-ins/pdb/PDB.h"
#include "plug-ins/pdb/CinePaintImage.h"
#include "plug-ins/pdb/Layer.h"

CinePaintPluginExecutioner::CinePaintPluginExecutioner()
{
	
}


CinePaintPluginExecutioner::~CinePaintPluginExecutioner()
{
}

bool CinePaintPluginExecutioner::Execute(const char* name, CinePaintImage &data,World* world)
{
	bool _ret = false;

	CinePaintPlugin *_p = 0;
	PDB* pdb=PDB::GetPDB(world);
	if(!pdb)
	{	return false;
	}
	CinePaintPluginInfo *_pinfo = pdb->LookupPlugin(name);
	CPParamList _in;
	CPParamList _return;
	if(_pinfo)
	{
		_p = _pinfo->GetCtor()();
	}

	if(_p)
	{
		// @TODO this only handles plugins that modify the input Drawable.
		//       this needs extended to handle various return args etc.


		CPPluginArg arg;
		Layer* _active_layer = data.GetLayers().GetActiveLayer();
		arg.pdb_drawable = _active_layer;
		CPPluginUIDef ui;
		CinePaintApp::GetInstance().GetUIWidgetBuilder().DoModalDialog(_p->GetUIDefs(ui), _in);

		// temporarily passes ownership of the drawable to the param list
		// the param list will release this when it is released, so we must reclaim
		// ownership before the param list os released.
		_in.AddParam("drawable", &arg, PDB_DRAWABLE);
		_p->run(_in, _return);

		// take back ownership of the drawable
		if(_in.RemoveParam("drawable", arg, PDB_DRAWABLE) == 1)
		{
			_active_layer->Update(0, 0, _active_layer->GetWidth(), _active_layer->GetHeight());
			_ret = true;
		}
		else
		{
			// something weird happened, plugin destroyed Drawable?
			printf("Cannot update Layer, %s Plugin removed Layer!", name);
		}

		// finally cleanup by releasing the plugin via the Procedure DB
		pdb->ReleaseInstance(_p);
	}

	return(_ret);
}
