/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintDocList - Document list manager
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****.
 *
 * $Id: CinePaintDocList.h,v 1.3 2006/12/21 11:18:04 robinrowe Exp $
 */

#ifndef _CINEPAINTDOCLIST_H_
#define _CINEPAINTDOCLIST_H_

#include "dll_api.h"
#include "plug-ins/pdb/ListenerList.h"
#include "plug-ins/pdb/CinePaintDoc.h"
#include "plug-ins/pdb/World.h"
#include <list>
#include <map>

// Forward Declarations
class CinePaintDocListListener;

/**
 * CinePaintDocList acts as the main management structure for holding all of the open images,
 * their drawables, data etc can all be accessed through this list.
 */
class CINEPAINT_CORE_API CinePaintDocList
{	World* world;
public:

	/**
	 * Construtor and Destructor
	**/
	CinePaintDocList(World* world);
	~CinePaintDocList();

	/**
	 * Open a document using the registered magic handlers
	 * The filename extension and the "magic" number are used to determine the file type.
	 * The reference count of the returned document is initially zero, If a valid document
	 * was opened and returned the caller should call RefDocument() to add a reference to 
	 * the document.
	 *
	 * @param file_path the document filename to open
	 * @return the opened CinePaintDoc, or 0 if no document was opened
	 */
	CinePaintDoc* OpenDocument(const char* file_path);

	/**
	 * Open a document as the specified type.
	 * the type parameter is used to determine the appropriate file handler.
	 *
	 * @param file_path the document filename to open
	 * @param type the type of the file to open
	 * @return the opened CinePaintDoc, or 0 if no document was opened
	 */
	CinePaintDoc* OpenDocument(const char* file_path, const char* type);

	/**
	 * Adds the specified CinePaintDoc to this CinePaintDocList
	 * The reference count added document is initially set to zero, the caller should 
	 * make a call RefDocument() to add a reference to the document.
	 *
	 * @param doc the CinepaintDoc to add
	 */
	void AddDocument(CinePaintDoc& doc);

	/**
	 * Save the current document using the already defined name
	 * if no name - i.e it was a new document then prompt for one.
	**/
	void SaveCurrentDocument();

	/**
	 * Save the current document with  the given name.
	 * @param file_name the file name to use to save the document.
	**/
	void SaveCurrentDocumentAs(const char* file_name);

	/**
	 * Saves the specified Docuement to the specified file path
	 *
	 * @param doc the CinePaintDoc to be saved
	 * @param file_path the file path to save to
	 */
	void SaveDocument(CinePaintDoc& doc, const char* file_name);

	/**
	 * Saves the specified document using the path as defined in the document,
	 **/
	void SaveDocument(CinePaintDoc& doc);
	
	/**
	 * reverts the specified document to the version on disk
	 *
	 * @param doc the CinePaintDoc to reload/revert
	 */
	void RevertDocument(CinePaintDoc& doc);

	////////////////
	// Access members for getting/setting current doc.

	/**
	 * Get the current active document
	**/
	CinePaintDoc* GetCurrentDoc();

	/**
	 * Set the current active document
	 *
	 *@param id the id of the document to select as the current one.
	 */
	void SetCurrentDoc(const char* id);

	/**
	 * Sets the currently active document
	 * If the specified document is not open and managed by CinePaintDocList, no change is made
	 * 
	 * @param doc the currently active document
	 */
	void SetCurrentDoc(CinePaintDoc& doc);




	//----------------------------
	// Document reference counting

	/**
	 * Increases the reference count of the specified Document.
	 *
	 * @param doc the Document to increment the reference count of
	 * @return true if this DocList manages the document and the ref count was incremented,
	 *         false otherwise
	 */
	bool RefDocument(CinePaintDoc& doc);

	/**
	 * Decrements the reference count of the specified Document.
	 * If the reference count reaches zero, ie the last remaining reference to the document
	 * has been released, then the document is closed.
	 *
	 * @param doc the document to decrement the reference count for
	 * @return true if the document is managed by this doc list and the ref count decremented,
	 *         false otherwise
	 */
	bool UnRefDocument(CinePaintDoc& doc);

	/**
	 * Returns the number of references to the specified Document
	 *
	 * @param doc the CinePaintDoc to get the reference count for
	 * @return the reference count of the specified Document
	 */
	int GetRefCount(CinePaintDoc& doc);



	
	//---------------------
	// Document Id handling

	/**
	 * Returns the id of the specified document.
	 * Each managed document is assigned a unique Id, within this CinePaintDocList.
	 * This unique Id allows two documents constructed from the same file to be
	 * differentiated.
	 * If the specified document is not managed by this CinePaintDocList, 0 is returned.
	 * The char* returned remains owned by this CinePaintDocList
	 *
	 * @param doc the Document to obtain the Id for
	 * @return the Id of the spcified Document, or 0
	 */
	const char* GetDocId(const CinePaintDoc& doc) const;
	
	/**
	 * Returns the document by its Id.
	 * If the spcified id does not match any document mamnaged by this CinePaintDocList,
	 * 0 is returned.
	 *
	 * @param id the document id
	 * @return the document, or 0
	 */
	CinePaintDoc* GetDocById(const char* id);

	/**
	 * Populates the the ids parameter with the Ids of he managed documents of a particular type.
	 * This method iterates through the managed documents, and adds the Id of the document to the
	 * ids list if the documents type matches the specified type parameter.
	 *
	 * @param type the document type to filter on
	 * @param ids list to populate with the ids of documents matching the specified type
	 * @return the ids parameter populated.
	 */
	std::list<const char*>& GetDocIdsByType(CinePaintDoc::DocType type, std::list<const char*>& ids); 


	//---------------------------
	// Doc List Listener handling

	/**
	 * Adds the specified CinePaintDocListListener to receive Doc List events from this CinePaintDocList
	 * if the specified CinePaintDocListListener has already been added it is not added again.
	 * The resources of the specified CinePaintDocListListener are not managed by this class
	 *
	 * @param listener the CinePaintDocListListener to be added
	 */
	void AddDocListener(CinePaintDocListListener* listener);

	/**
	 * Removes and returns the specified CinePaintDocListListener.
	 * The removed CinePaintDocListListener will no longer receive Doc List events from this CinePaintDocList
	 * If the specified listener is not present, 0 is returned
	 *
	 * @param listener the CinePaintDocListListener to be removed
	 */
	CinePaintDocListListener* RemoveDocListener(CinePaintDocListListener* listener);

protected:

	/**
	 * Close the currently selected document.
	 * @param save determines whether to save any changes or not, true equal save,
	 * false don't save.
	**/
	void close_current_document(bool save);

	/**
	 * Close an open document,
	 * @param save save changes if true - don't save if false
	 * @param name name of the document to close
	**/
	void close_document(bool save, const char* name);

	/**
	 * Closes the specifeied CinePaintDoc.
	 * 
	 * @param doc the CinePaintDoc to be closed
	 */
	void close_document(CinePaintDoc* doc);

	/**
	 * Generates a new CinePaintDoc ID for the specified document.
	 * The document ID is unique for all documents contained within this CinePaintDocList.
	 * The ID is based upon the filename of the document, appending a numerical suffix
	 * to differentiate between different copies of the same file being open at once.
	 *
	 * @param doc the CinePaintDoc for which to generate the id
	 * @return a new id which is unique within this CinePaintDocList for the managed document.
	 */
	std::string generate_doc_id(CinePaintDoc& doc);

private:

	/**
	 * Managed document record struct
	 */
	struct DocRec
	{
		DocRec(CinePaintDoc* doc, int ref_count) : m_doc(doc), m_ref_count(ref_count) {};

		CinePaintDoc* m_doc;
		int m_ref_count;
	};

	// The document list typedef - document Id, managed document record
	typedef  std::map<std::string, DocRec*> DocList_t;

	/* The actual list */
	DocList_t m_docList;

	/* The current selected document. */
	CinePaintDoc *m_pCurrentDoc;


	// Doc Listener
	typedef ListenerList<CinePaintDocListListener> ListenerList_t;
	ListenerList_t m_listener_list;

}; /* class CinePaintDocList */

#endif /* _CINEPAINTDOCLIST_H_ */
