/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      FlipFrameController - Frame Manager flipframe controller
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: FlipFrameController.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "FlipFrameController.h"
#include "FlipFrameDriver.h"
#include "plug-ins/pdb/CinePaintImage.h"
#include "CinePaintApp.h"
#include "FrameStore.h"
#include "FrameManager.h"
#include <utility/PlatformUtil.h> // req. for CpSleep

/**
 * Constructs a new FlipFrameController to control the specified FrameManager
 *
 * @param frameManager the FrameManager to control
 * @param store the store used to pre-load images
 */
FlipFrameController::FlipFrameController(FrameManager& frame_manager, FrameStore& store)
		: m_frame_manager(frame_manager), m_frame_store(store)
{
	m_flip_driver = 0;
	m_buffer_run_flag = false;
//	m_buffer_run_thread = 0;

	m_doc_handler = new DocListHandler(*this, m_image_references, m_frame_manager);
	CinePaintApp::GetInstance().GetDocList().AddDocListener(m_doc_handler);

	m_store_handler = new StoreHandler(m_image_references, m_frame_manager);
	m_frame_store.AddStoreListener(m_store_handler);

	m_direction = FORWARD_ENUM;

	m_store_maintainer = new StoreMaintainer(m_frame_store, m_frame_manager, *this);
	m_frame_manager.AddFrameListener(m_store_maintainer);
}

/**
 * Destructor
 */
FlipFrameController::~FlipFrameController()
{
	if(m_flip_driver)
	{
		m_flip_driver->Stop();
		delete m_flip_driver;
		m_flip_driver = 0;
	}

	if(m_store_maintainer)
	{
		m_frame_manager.RemoveFrameListener(m_store_maintainer);
		delete m_store_maintainer;
		m_store_maintainer = 0;
	}

	if(m_doc_handler)
	{
		CinePaintApp::GetInstance().GetDocList().RemoveDocListener(m_doc_handler);

		delete m_doc_handler;
		m_doc_handler = 0;
	}
}

/**
 * Steps the current selection within the FrameManager by one place in the specified Directipn
 *
 * @param dir the Direction in which to step the FrameManager
 */
void FlipFrameController::Step(Direction dir)
{
	switch(dir)
	{
		case FORWARD_ENUM:
		{
			m_frame_manager.Next();
			break;
		}
		case BACKWARD_ENUM:
		{
			m_frame_manager.Previous();
			break;
		}
		default:
			break;
	}
}

/**
 * Initiates the automatic 'flipping' of the FrameManager in the specified direction
 *
 * @param dir the Direction in which to Play the FrameManager
 */
void FlipFrameController::Play(Direction dir)
{
	m_direction = dir;

	switch(dir)
	{
		case FORWARD_ENUM:
		{
			// @TODO setup pre loading direction etc.

			BufferAndPlay(m_direction);
			break;
		}
		case BACKWARD_ENUM:
		{
			BufferAndPlay(m_direction);
			break;
		}
		default:
			break;
	}
}

/**
 * Stops the matic 'flipping' of the FrameManager
 *
 */
void FlipFrameController::Stop()
{
	// stop the buffer run thread, if running
	m_buffer_run_flag = false;

	if(m_flip_driver)
	{
		m_flip_driver->Stop();

		m_frame_store.StopPreLoad();
		m_frame_store.SetLoadMode(FrameStore::ON_DEMAND_ENUM);
	}
}

/**
 * Ensures the FrameStore used to pre-load images is buffered before Initiates the automatic 'flipping' of the FrameManager.
 * The associated FrameStore may be used to pre-load a certain number of images for processing, this method ensures
 * that the FrameStore is buffered before initiating the automatic 'flipping'
 *
 * @param dir the Direction in which to Play the FrameManager
 */
void FlipFrameController::BufferAndPlay(Direction dir)
{
	m_direction = dir;

	if(m_flip_driver && m_flip_driver->IsRunning())
	{
		m_flip_driver->Stop();
	}

	if(!m_buffer_run_flag)
	{
		m_buffer_run_flag = true;

		if(pthread_create(&m_buffer_run_thread, NULL, FlipFrameController::buffer_run, this) == 0)
		{
			pthread_detach(m_buffer_run_thread);
		}
		else
		{
			// thread creation failed!
			printf("Creation of Buffer Run Thread failed\n");
			m_buffer_run_flag = false;
		}
	}
	else
	{
		printf("Buffer Run Thread already running\n");
	}
}

/**
 * Returns the Direction in which this FlipFrameController is currently flipping
 *
 * @return the Direction this FlipFrameController is currently flipping
 */
FlipFrameController::Direction FlipFrameController::GetDirection() const
{
	return(m_direction) ;
}




//
// @todo is there someplace better these methods belong?!
//

/**
 * Registers a modified CinePaintDoc with this FlipFrameController.
 * If the specified image is already registered, it is not registered a seccond time, and false is returned,
 * otherwise true is returned.
 * This method exists to avoid Unreferencing the last reference to a CinePaintImage during flipping.
 * As each CinePaintImage is loaded (by the FrameStore via the CinePaintDocList) a reference to the image
 * is added. Generally, when the last reference is released, we should close the CinePaintImage.
 * However, if an image is modified, asking the user if the image should be saved during the flipping may not
 * be desirable, thie method allows a reference to be maintained so that the image can be saved at a later point
 *
 * @todo this needs some review!
 * @param the Image to register
 * @return true if the image was indeed registered, false otherwise
 */
bool FlipFrameController::RegisterModifiedDocument(CinePaintImage& image)
{
	bool added = false;

	if(m_image_references.find(&image) == m_image_references.end())
	{
		m_image_references.insert(&image);
		added = true;
	}

	return(added);
}

/**
 * Unregisters the specified image from this FlipFrameController
 * If the specified image has not been previously registered, false is returned, otherwise
 * the image is unregistered, decrementing its reference count by one within the CinePaintDocList
 * and true is returned.
 *
 * @todo this needs some review!
 * @param the Image to unregister
 * @return trus if the image was unregistered
 */
bool FlipFrameController::UnRegisterModifiedDocument(CinePaintImage& image)
{
	bool removed = false;

	ReferenceStore_t::iterator iter = m_image_references.find(&image);
	if(iter != m_image_references.end())
	{
		m_image_references.erase(iter);
		removed = true;
	}

	return(removed);
}




void FlipFrameController::start_flip_frame()
{
	if(!m_flip_driver)
	{
		m_flip_driver = new SimpleFlipFrameDriver(m_frame_manager, *this);
	}

	// start with the current frame
	if(m_frame_manager.size() > 0)
	{
		if(m_frame_manager.GetCurrentIndex() == FrameManager::INVALID_INDEX)
		{
			m_frame_manager.SetCurrentFrame(0);
		}

		m_flip_driver->Start();
	}
	else
	{
		printf("Nothing to flip\n");
	}
}

void* FlipFrameController::buffer_run(void* arg)
{
	printf("Starting buffer run ... \n");

	FlipFrameController* instance = reinterpret_cast<FlipFrameController*>(arg);

	instance->m_frame_store.StopPreLoad();
	instance->m_frame_store.ClearBuffer();
	instance->m_frame_store.SetLoadMode(FrameStore::PRE_LOAD_ENUM);
	instance->m_frame_store.SetStoreReadPos(instance->m_frame_manager.GetNextActiveIndex(instance->m_frame_manager.GetCurrentIndex()));
	instance->m_frame_store.StartPreLoad();

	while(instance->m_buffer_run_flag && instance->m_frame_store.IsPreLoading() && !instance->m_frame_store.IsBuffered())
	{
		// wait for the buffer to get some data
		//usleep(100000);
		PlatformUtil::CpSleep(100) ;
	}

	// check the buffer run flag again to ensure we haven't been cancelled,
	// this avoids us restarting the flip frame is the buffer run starts right before
	// we hit stop
	if(instance->m_buffer_run_flag)
	{
		instance->start_flip_frame();
	}

	printf("Buffer run complete ... \n");
	instance->m_buffer_run_flag = false;
//	instance->m_buffer_run_thread = 0;

	return(0);
}





//---------------
// Nested Classes

FlipFrameController::StoreHandler::StoreHandler(ReferenceStore_t& refs, FrameManager& fm)
		: m_image_references(refs), m_frame_manager(fm)
{}

FlipFrameController::StoreHandler::~StoreHandler()
{}

void FlipFrameController::StoreHandler::FrameAdded(const FrameManager::FrameRecord& frame, CinePaintImage& image)
{
	FrameManager::size_type pos = m_frame_manager.GetIndex(frame);
	if(pos != FrameManager::INVALID_INDEX)
	{
		m_frame_manager.SetFramePreLoaded(pos, true);
	}
}

void FlipFrameController::StoreHandler::FrameRemoved(const FrameManager::FrameRecord& frame, CinePaintImage& image)
{
	FrameManager::size_type pos = m_frame_manager.GetIndex(frame);
	if(pos != FrameManager::INVALID_INDEX)
	{
		m_frame_manager.SetFramePreLoaded(pos, false);
	}
}







FlipFrameController::DocListHandler::DocListHandler(FlipFrameController& controller, ReferenceStore_t& refs, FrameManager& fm)
		: m_controller(controller), m_image_references(refs), m_frame_manager(fm)
{}

FlipFrameController::DocListHandler::~DocListHandler()
{}

void FlipFrameController::DocListHandler::DocOpened(const char* doc_id, CinePaintDoc& doc)
{
	if(doc.GetDocType() == CinePaintDoc::CINEPAINT_IMAGE_ENUM)
	{
		CinePaintImage* img = dynamic_cast<CinePaintImage*>(&doc);
		if(img)
		{
			std::string filename(img->GetFileName());

			for(FrameManager::const_iterator citer = m_frame_manager.begin(); citer != m_frame_manager.end(); ++citer)
			{
				const FrameManager::FrameRecord* rec = *citer;

				if(filename == rec->GetSource())
				{
					if(rec->GetId().empty())
					{
						FrameManager::size_type pos = m_frame_manager.GetIndex(*rec);
						if(pos != FrameManager::INVALID_INDEX)
						{
							m_frame_manager.SetFrameLoaded(pos, true);
							m_frame_manager.SetFrameId(pos, doc_id);
						}
					}
				}
			}
		}
	}
}

void FlipFrameController::DocListHandler::DocClosed(const char* doc_id, CinePaintDoc& doc)
{
	if(doc.GetDocType() == CinePaintDoc::CINEPAINT_IMAGE_ENUM)
	{
		const FrameManager::FrameRecord* frame = 0;
		for(size_t pos = 0; pos < m_frame_manager.size(); pos++)
		{
			if((frame = m_frame_manager.GetFrame(pos)) != 0)
			{
				if(strcmp(frame->GetId().c_str(), doc_id) == 0)
				{
					m_frame_manager.SetFrameLoaded(pos, false);
					m_frame_manager.SetFrameId(pos, 0);
				}
			}
		}
	}
}

void FlipFrameController::DocListHandler::CurrentDocChanged(const char* doc_id, CinePaintDoc* doc)
{}





FlipFrameController::StoreMaintainer::StoreMaintainer(FrameStore& store, FrameManager& frame_manager, FlipFrameController& controller)
		: m_store(store), m_frame_manager(frame_manager), m_controller(controller)
{}

FlipFrameController::StoreMaintainer::~StoreMaintainer()
{

}

void FlipFrameController::StoreMaintainer::SelectionChanged(const FrameManager::FrameRecord* frame, FrameManager::size_type position)
{
	/*
	if((m_store.GetLoadBehaviour() == FrameStore::PRE_LOAD_ENUM) && (m_store.IsPreLoading()))
{
		printf("Position=%lu,  store read pos=%lu\n", position, m_store.GetStoreReadPos());

		switch(m_controller.GetDirection())
		{
			case FlipFrameController::FORWARD_ENUM:
			{
				if(m_frame_manager.GetLoop() && m_store.GetStoreSize() > m_store.GetStoreReadPos())
				{
					// looped
					FrameManager::size_type pos = (m_store.GetStoreSize() -1) - m_store.GetStoreReadPos();
					FrameManager::size_type s_pos = m_frame_manager.size() - pos;
					printf("Has looped pos=%lu s_pos=%lu fm_size=%lu storesize=%lu\n", pos, s_pos, m_frame_manager.size(), m_store.GetStoreSize());
			
					if((position +1) < s_pos)
					{
						m_controller.BufferAndPlay(FlipFrameController::FORWARD_ENUM);
					}
				}
				else
				{
					printf("Not looped\n");
					if(position > m_store.GetStoreReadPos())
					{
						m_controller.BufferAndPlay(FlipFrameController::FORWARD_ENUM);
					}
				}
				break;
			}
			case FlipFrameController::BACKWARD_ENUM:
			{
				if(position > m_store.GetStoreReadPos())
				{
					m_controller.BufferAndPlay(FlipFrameController::BACKWARD_ENUM);
				}
				break;
			}
			default:
			{
				break;
			}
		}
}

	*/
}

void FlipFrameController::StoreMaintainer::FrameAdded(const FrameManager::FrameRecord& frame, FrameManager::size_type position)
{}

void FlipFrameController::StoreMaintainer::FrameRemoved(const FrameManager::FrameRecord& frame, FrameManager::size_type position)
{}

void FlipFrameController::StoreMaintainer::FrameUpdated(const FrameManager::FrameRecord& frame, FrameManager::size_type position)
{}

void FlipFrameController::StoreMaintainer::FrameIndexChanged(const FrameManager::FrameRecord& frame, FrameManager::size_type old_pos, FrameManager::size_type new_pos)
{}

