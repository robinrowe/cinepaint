/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ColorManager - Manages currently selected Color
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ColorManager.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "ColorManager.h"
#include "plug-ins/pdb/Color.h"
#include <algorithm>

/**
	* Adds the specified ColorChangeListener to receive notification upon color changes
	* if the specified ColorChangeListener has already been added it is not added again.
	* The resources of the specified ColorChangeListener are not managed by this class
	*
	* @param ccl the ColorChangeListener to be added
	*/
void ColorManager::AddColorChangeListener(ColorChangeListener* ccl)
{
	if(std::find(m_color_change_listeners.begin(), m_color_change_listeners.end(), ccl) == m_color_change_listeners.end())
	{
		m_color_change_listeners.push_back(ccl);
	}
}


/**
	* Removes the specified ColorChangeListener
	* The removed ColorChangeListener will no longer receive Color change notifications from
	* this class
	*
	* @param ccl the ColorChangeListener to be removed
	*/
ColorChangeListener* ColorManager::RemoveColorChaneListener(ColorChangeListener* ccl)
{
	ColorChangeListener* ret = 0;

	ListenerList_t::iterator iter = std::find(m_color_change_listeners.begin(), m_color_change_listeners.end(), ccl);

	if(iter != m_color_change_listeners.end())
	{
		ret = ccl;
		m_color_change_listeners.erase(iter);
	}

	return(ret);
}

/**
 * post a ColorChanged message to all registered ColorChangeListeners
 *
 * @param color_type the color to have changed
 * @param c the new Color
 */
void ColorManager::post_color_changed_update(ColorChangeListener::ColorType color_type, const Color& c)
{
	for(ListenerList_t::const_iterator citer = m_color_change_listeners.begin(); citer != m_color_change_listeners.end(); ++citer)
	{
		(*citer)->ColorChanged(color_type, c);
	}
}
