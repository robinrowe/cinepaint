/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintBrush - Paint brush class
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintBrushLoader.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "CinePaintBrushLoader.h"
#include "CinePaintBrush.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include "plug-ins/pdb/CinePaintTag.h"
#include "utility/Vector2d.h"
#include <fstream>

#define GIMP_BRUSH_MAGIC_NUMBER (('G' << 24) + ('I' << 16) + ('M' << 8) + ('P' << 0))

// for Darwin, htonl is defined in endian.h
#ifndef __APPLE__
#define htonl(A)  ((((unsigned int)(A) & 0xff000000) >> 24) | \
                   (((unsigned int)(A) & 0x00ff0000) >> 8)  | \
                   (((unsigned int)(A) & 0x0000ff00) << 8)  | \
                   (((unsigned int)(A) & 0x000000ff) << 24))
#endif /* !__APPLE__*/

// Defined Image Types
// these values are taken from the original cinepaint tag.h defines
const unsigned int CinePaintBrushLoader::RGB_IMAGE = 0;
const unsigned int CinePaintBrushLoader::RGBA_IMAGE = 1;
const unsigned int CinePaintBrushLoader::GRAY_IMAGE = 2;
const unsigned int CinePaintBrushLoader::GRAYA_IMAGE = 3;
const unsigned int CinePaintBrushLoader::INDEXED_IMAGE = 4;
const unsigned int CinePaintBrushLoader::INDEXEDA_IMAGE = 5;

// These are the 16bit types
const unsigned int CinePaintBrushLoader::U16_RGB_IMAGE = 6;
const unsigned int CinePaintBrushLoader::U16_RGBA_IMAGE = 7;
const unsigned int CinePaintBrushLoader::U16_GRAY_IMAGE = 8;
const unsigned int CinePaintBrushLoader::U16_GRAYA_IMAGE = 9;
const unsigned int CinePaintBrushLoader::U16_INDEXED_IMAGE = 10;
const unsigned int CinePaintBrushLoader::U16_INDEXEDA_IMAGE = 11;

// These are the float types
const unsigned int CinePaintBrushLoader::FLOAT_RGB_IMAGE = 12;
const unsigned int CinePaintBrushLoader::FLOAT_RGBA_IMAGE = 13;
const unsigned int CinePaintBrushLoader::FLOAT_GRAY_IMAGE = 14;
const unsigned int CinePaintBrushLoader::FLOAT_GRAYA_IMAGE = 15;

// These are the 16bit float types
const unsigned int CinePaintBrushLoader::FLOAT16_RGB_IMAGE = 16;
const unsigned int CinePaintBrushLoader::FLOAT16_RGBA_IMAGE = 17;
const unsigned int CinePaintBrushLoader::FLOAT16_GRAY_IMAGE = 18;
const unsigned int CinePaintBrushLoader::FLOAT16_GRAYA_IMAGE = 19;

// These are the bfp types
const unsigned int CinePaintBrushLoader::BFP_RGB_IMAGE = 20;
const unsigned int CinePaintBrushLoader::BFP_RGBA_IMAGE = 21;
const unsigned int CinePaintBrushLoader::BFP_GRAY_IMAGE = 22;
const unsigned int CinePaintBrushLoader::BFP_GRAYA_IMAGE = 23;


/**
 * Constructs and returns a new CinePaintBrush from data loaded from the specified brush data file.
 * If the brush data file is invalid in some way, 0 is returned.
 *
 * @param filename the brush data file to load
 * @return a new brush for the loaded data file
 */
CinePaintBrush* CinePaintBrushLoader::LoadBrush(const char* filename)
{
	CinePaintBrush* _brush = 0;

	std::ifstream _in_stream;

	BrushHeader _header;

	// Open the requested file
	_in_stream.open(filename, std::ifstream::in|std::ifstream::binary);

	if(_in_stream.good())
	{
		if(CinePaintBrushLoader::read_brush_header(filename, _in_stream, _header))
		{
			std::string _brushname;
			if(CinePaintBrushLoader::get_brush_name(filename, _in_stream, _header, _brushname))
			{
				// convert the drawable type to a CinePaintTag
				CinePaintTag _tag;
				CinePaintBrushLoader::tag_from_drawable_type(_header.m_type, _tag);

				AbstractBuf* _brushmask = CinePaintBrushLoader::read_brush_data(filename, _in_stream, _header, _tag);

				if(_brushmask)
				{
					// Okay, successful reading of the brush file, now create the brush object
					Vector2d _x_axis(_header.m_width / 2.0, 0.0) ;
					Vector2d _y_axis(0.0, _header.m_height / 2.0) ;
					_brush = new CinePaintBrush(filename, _brushname.c_str(), _brushmask, static_cast<float>(_header.m_spacing), _x_axis, _y_axis);
				}
				else
				{
					std::cout << "Failed to read brush data: " << filename << std::endl;
				}
			}
			else
			{
				std::cout << "Failed to read brush name: " << filename << std::endl;
			}
		}
		else
		{
			std::cout << "Failed to read brush header: " << filename << std::endl;
		}
	}
	else
	{
		std::cout << "Failed to open brush file: " << filename << std::endl;
	}

	// finished with the brush file, close it
	_in_stream.close();

	return(_brush);
}

bool CinePaintBrushLoader::read_brush_header(const char* filename, std::ifstream& fin, CinePaintBrushLoader::BrushHeader& header)
{
	bool _ret = false;

	fin.read((char*)&header.m_header_size, sizeof(header.m_header_size));
	fin.read((char*)&header.m_version, sizeof(header.m_version));
	fin.read((char*)&header.m_width, sizeof(header.m_width));
	fin.read((char*)&header.m_height, sizeof(header.m_height));

	header.m_header_size = htonl(header.m_header_size);
	header.m_version = htonl(header.m_version);
	header.m_width = htonl(header.m_width);
	header.m_height = htonl(header.m_height);

	switch(header.m_version)
	{
			case 1:
			{
				// Gimp brush version 1
				// version 1 did not include these fields, so fake the header size
				header.m_header_size += sizeof(header.m_magic_number);
				header.m_header_size += sizeof(header.m_spacing);

				header.m_spacing = CinePaintBrush::DEFAULT_BRUSH_SPACING;

				fin.read((char*)&header.m_type, sizeof(header.m_type));
				header.m_type = htonl(header.m_type);

				header.m_type = gimp_brush_bytes_to_type(header.m_type);

				_ret = true;
				break;
			}
			case 2:
			{
				fin.read((char*)&header.m_type, sizeof(header.m_type));
				fin.read((char*)&header.m_magic_number, sizeof(header.m_magic_number));
				fin.read((char*)&header.m_spacing, sizeof(header.m_spacing));

				header.m_type = htonl(header.m_type);
				header.m_magic_number = htonl(header.m_magic_number);
				header.m_spacing = htonl(header.m_spacing);

				// Gimp brush version 2
				if(verify_magic_number(header.m_magic_number))
				{
					header.m_type = gimp_brush_bytes_to_type(header.m_type);
					_ret = true;
				}
				else
				{
					std::cout << "Brush Magic Number did not match" << std::endl;
				}

				break;
			}
			case 3:
			{
				fin.read((char*)&header.m_type, sizeof(header.m_type));
				fin.read((char*)&header.m_magic_number, sizeof(header.m_magic_number));
				fin.read((char*)&header.m_spacing, sizeof(header.m_spacing));

				header.m_type = htonl(header.m_type);
				header.m_magic_number = htonl(header.m_magic_number);
				header.m_spacing = htonl(header.m_spacing);

				if(verify_magic_number(header.m_magic_number))
				{
					_ret = true;
				}
				else
				{
					std::cout << "Brush Magic Number did not match" << std::endl;
				}

				break;
			}
			default:
			{
				std::cout << "Fatal parse error in brush file: " << filename << ", Unknown version: " << header.m_version << std::endl;
			}
	}

	return(_ret);
}

bool CinePaintBrushLoader::get_brush_name(const char* filename, std::ifstream& fin, CinePaintBrushLoader::BrushHeader& header, std::string& brushname)
{
	bool _ret = true;
	int _name_size = header.m_header_size - sizeof(CinePaintBrushLoader::BrushHeader);

	if(_name_size > 0)
	{
		char* _name = new char[_name_size +1];

		fin.read(_name, _name_size);

		brushname = _name;
		delete[] _name;

		if(fin.gcount() < _name_size)
		{
			std::cout << "Fatal parse error in brush file: " << filename << ", Failed to read brushname" << std::endl;
			_ret = false;
		}
		else
		{
			_ret = true;
		}
	}
	else
	{
		brushname = "Untitled";
		_ret = true;
	}

	return(_ret);
}


AbstractBuf* CinePaintBrushLoader::read_brush_data(const char* filename, std::ifstream& fin, CinePaintBrushLoader::BrushHeader& header, CinePaintTag& tag)
{
	std::auto_ptr<AbstractBuf> _brushmask = AbstractBufFactory::CreateBuf(tag, header.m_width, header.m_height, AbstractBufFactory::STORAGE_FLAT);

	_brushmask->RefPortionReadWrite(0, 0);

	// returns direct access to the canvas data
	unsigned char* _data = _brushmask->GetPortionData(0, 0);
	int _bytes = header.m_width * header.m_height * tag.GetBytes();
	fin.read((char*)_data, _bytes);

	if(fin.gcount() == _bytes)
	{

#ifndef WORDS_BIGENDIAN

		// This switches the byte order of the Float16 brush types
		// if we are not building on a big endian machine
		if(tag.GetPrecision() == CinePaintTag::PRECISION_FLOAT16_ENUM)
		{
			for(unsigned int i = 0; i < (header.m_width * header.m_height * tag.GetBytes() -1); i += 2)
			{
				unsigned char tmp = _data[i];
				_data[i] = _data[i+1];
				_data[i+1] = tmp;
			}
		}
#endif

	}
	else
	{
		std::cout << "Fatal parse error in brush file: " << filename << ", Brush file appears to be truncated." << std::endl;
	}


	_brushmask->UnrefPortion(0, 0);

	return(_brushmask.release());
}



// @note [claw] these need removed/replaced/forgotten about
//       this is temporary while i'm removing the original tag cinepaint header
//       possibly to a legacy TagUtils class?
CinePaintTag& CinePaintBrushLoader::tag_from_drawable_type(int drawable_type, CinePaintTag& tag)
{
	CinePaintTag::Precision p;
	CinePaintTag::Format f;
	bool a = false;

	switch(drawable_type)
	{
			/* The 8bit data cases */
		case RGBA_IMAGE:
			a = true;
		case RGB_IMAGE:
			p = CinePaintTag::PRECISION_U8_ENUM;
			f = CinePaintTag::FORMAT_RGB_ENUM;
			break;
		case GRAYA_IMAGE:
			a = true;
		case GRAY_IMAGE:
			p = CinePaintTag::PRECISION_U8_ENUM;
			f = CinePaintTag::FORMAT_GRAY_ENUM;
			break;
		case INDEXEDA_IMAGE:
			a = true;
		case INDEXED_IMAGE:
			p = CinePaintTag::PRECISION_U8_ENUM;
			f = CinePaintTag::FORMAT_INDEXED_ENUM;
			break;

			/* The 16bit data cases */
		case U16_RGBA_IMAGE:
			a = true;
		case U16_RGB_IMAGE:
			p = CinePaintTag::PRECISION_U16_ENUM;
			f = CinePaintTag::FORMAT_RGB_ENUM;
			break;
		case U16_GRAYA_IMAGE:
			a = true;
		case U16_GRAY_IMAGE:
			p = CinePaintTag::PRECISION_U16_ENUM;
			f = CinePaintTag::FORMAT_GRAY_ENUM;
			break;
		case U16_INDEXEDA_IMAGE:
			a = true;
		case U16_INDEXED_IMAGE:
			p = CinePaintTag::PRECISION_U16_ENUM;
			f = CinePaintTag::FORMAT_INDEXED_ENUM;
			break;

			/* The float data cases */
		case FLOAT_RGBA_IMAGE:
			a = true;
		case FLOAT_RGB_IMAGE:
			p = CinePaintTag::PRECISION_FLOAT_ENUM;
			f = CinePaintTag::FORMAT_RGB_ENUM;
			break;
		case FLOAT_GRAYA_IMAGE:
			a = true;
		case FLOAT_GRAY_IMAGE:
			p = CinePaintTag::PRECISION_FLOAT_ENUM;
			f = CinePaintTag::FORMAT_GRAY_ENUM;
			break;

			/* The float 16 data cases */
		case FLOAT16_RGBA_IMAGE:
			a = true;
		case FLOAT16_RGB_IMAGE:
			p = CinePaintTag::PRECISION_FLOAT16_ENUM;
			f = CinePaintTag::FORMAT_RGB_ENUM;
			break;
		case FLOAT16_GRAYA_IMAGE:
			a = true;
		case FLOAT16_GRAY_IMAGE:
			p = CinePaintTag::PRECISION_FLOAT16_ENUM;
			f = CinePaintTag::FORMAT_GRAY_ENUM;
			break;

			/* The bfp data cases */
		case BFP_RGBA_IMAGE:
			a = true;
		case BFP_RGB_IMAGE:
			p = CinePaintTag::PRECISION_BFP_ENUM;
			f = CinePaintTag::FORMAT_RGB_ENUM;
			break;
		case BFP_GRAYA_IMAGE:
			a = true;
		case BFP_GRAY_IMAGE:
			p = CinePaintTag::PRECISION_BFP_ENUM;
			f = CinePaintTag::FORMAT_GRAY_ENUM;
			break;

			/* any undefined cases */
		default:
			p = CinePaintTag::PRECISION_NONE_ENUM;
			f = CinePaintTag::FORMAT_NONE_ENUM;
			a = false;
			break;
	}

	tag.SetPrecision(p);
	tag.SetFormat(f);
	tag.SetAlpha(a);

	return(tag);
}










bool CinePaintBrushLoader::verify_magic_number(unsigned int val)
{
	return(val == GIMP_BRUSH_MAGIC_NUMBER);
}


unsigned int CinePaintBrushLoader::gimp_brush_bytes_to_type(unsigned int gimp_brush_bytes)
{
	unsigned int _ret = 0;

	// If the brush header version is version 1 or 2, the bytes field
	// (gimp_brush_bytes) contains justthe number of bytes and must be
	// converted to one of the CinePaint drawable type values.
	if(gimp_brush_bytes == 1)
	{
		_ret = GRAY_IMAGE;
	}
	else if(gimp_brush_bytes == 3)
	{
		_ret = RGB_IMAGE;
	}

	return(_ret);
}
