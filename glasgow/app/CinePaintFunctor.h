/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintFunctor Interface
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintFunctor.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CINEPAINT_FUNCTOR_H_
#define _CINEPAINT_FUNCTOR_H_

#include "dll_api.h"

/**
 * Abstract Base class for Functor Objects.
 * AbstractFuctor exists so that containers of functors may be created of various
 * CinePaintFunctor objects, without the need to know the type that CinePaintFunctor
 * was instanced for, the operator() will be called on the cirrect type.
 */
class AbstractFunctor
{
	public:
		/**
		 * Destructor
		 */
		virtual ~AbstractFunctor() {};

		/**
		 * Calls this Functor.
		 */
		virtual void operator() () = 0;

	protected:
		/**
		 * protected constructor - disallow instances
		 */
		AbstractFunctor() {};

	private:
};


/**
 * Templated Member Function Functor.
 * CinePaintFunctor is templated on an object on which a member function is to be called.
 * The CinePaintFunctor takes an object and a pointer to a member function within that
 * object. Calling operator() on this functor calls the member function of the specified
 * object.
 *
 * This simplistic implementation assumes the member function takes no atguments and returns void.
 */
template <class T>
class CinePaintFunctor0 : public AbstractFunctor
{
	public:
		/**
		 * Constructs a new CinePaintFunctor which call the specified member function of the specified object
		 * The member function takes and returns void.
		 *
		 * @param obj the object on which to call the member function
		 * @param member_fun pointer to member function
		 */
		CinePaintFunctor0(T* obj, void (T::*member_fun)());

		/**
		 * Calls this Functor object
		 * Invokes the member function of the object specified in the constructor
		 */
		virtual void operator() ();

	protected:

	private:
		/** object to call member function on */
		T* m_obj;

		/** member function */
		void (T::*m_function_pointer)();
};

/**
 * Templated Member Function Functor with one data element.
 * CinePaintFunctor is templated on an object on which a member function is to be called.
 * An additional data element is also specified which is passed to the member function of
 * the specified object.
 * The CinePaintFunctor takes an object, a piece of data and a pointer to a member function
 * within that object. Calling operator() on this functor calls the member function of the
 * specified object passing the specified additional data.
 *
 * This simplistic implementation assumes the member function takes one atguments and returns void.
 */
template <class T, class D>
class CinePaintFunctor1 : public AbstractFunctor
{
	public:
		/**
		 * Constructs a new CinePaintFunctor which call the specified member function of the specified object
		 * The member function takes one argument and returns void.
		 *
		 * @param obj the object on which to call the member function
		 * @param data the additional data passed to the member function
		 * @param member_fun pointer to member function
		 */
		CinePaintFunctor1(T* obj, D data, void (T::*member_fun)(D));

		/**
		 * Calls this Functor object
		 * Invokes the member function of the object specified in the constructor
		 * passing the addition data item.
		 */
		virtual void operator() ();

	protected:

	private:
		/** object to call member function on */
		T* m_obj;

		/** data to pass to the member function */
		D m_data;

		/** member function */
		void (T::*m_function_pointer)(D);
};


//---------------
// implementation

// CinePaintFunctor0

template <class T>
CinePaintFunctor0<T>::CinePaintFunctor0(T* obj, void (T::*member_fun)())
{
	m_obj = obj;
	m_function_pointer = member_fun;
}

template <class T>
void CinePaintFunctor0<T>::operator() ()
{
	(*m_obj.*m_function_pointer)();
}




// CinePaintFunctor1

template <class T, class D>
CinePaintFunctor1<T,D>::CinePaintFunctor1(T* obj, D data, void (T::*member_fun)(D))
{
	m_obj = obj;
	m_data = data;
	m_function_pointer = member_fun;
}

template <class T, class D>
void CinePaintFunctor1<T,D>::operator() ()
{
	(*m_obj.*m_function_pointer)(m_data);
}

#endif /* _CINEPAINT_FUNCTOR_H_ */
