/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Image enums.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ImageEnums.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _IMAGE_ENUMS_H_
#define _IMAGE_ENUMS_H_

enum CinePaintImageType
{
  RGB           = 0,
  GRAY          = 1,
  INDEXED       = 2,
  U16_RGB       = 3,
  U16_GRAY      = 4,
  U16_INDEXED   = 5,
  FLOAT_RGB     = 6,
  FLOAT_GRAY    = 7,
  FLOAT16_RGB   = 8,
  FLOAT16_GRAY  = 9, 
  BFP_RGB       = 10,
  BFP_GRAY      = 11 
};

enum CinePaintChannelType
{
  RED_CHANNEL,
  GREEN_CHANNEL,
  BLUE_CHANNEL,
  GRAY_CHANNEL,
  INDEXED_CHANNEL,
  AUXILLARY_CHANNEL
};

#endif /* _IMAGE_ENUMS_H_ */


