/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintBrush - Paint brush class
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintBrush.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CINEPAINT_BRUSH_H_
#define _CINEPAINT_BRUSH_H_

#include "dll_api.h"
#include "CinePaintBrushLoader.h"
#include <utility/Vector2d.h>

#include <fstream>

// forward declaration
class AbstractBuf;

/**
 * A representation of a Brush
 */
class CINEPAINT_PAINTING_API CinePaintBrush
{
	public:
		/**
		 * Destructor
		 */
		virtual ~CinePaintBrush() ;

		/**
		 * Return sthe name of this brush
		 * The returned char* remains owned by this class.
		 *
		 * @return the name of this brush
		 */
		const char* GetName() const ;

		/**
		 * Sets the name of this brush
		 * The name parameter is copied by this class
		 *
		 * @param name the new brush name
		 */
		void SetName(const char* name) ;

		/**
		 * Returns the AbstractBuf of the brush mask data
		 *
		 * @return the brush mask AbstractBuf
		 */
		AbstractBuf* GetMask() const ;

		void SetSpacing(float spacing);
		float GetSpacing() const;

		void SetXAxis(const Vector2d& x_axis);
		const Vector2d& GetXAxis() const;
		void SetYAxis(const Vector2d& y_axis);
		const Vector2d& GetYAxis() const;


	protected:
		friend class CinePaintBrushLoader;

		/**
		 * Constructs a new CinePaintBrush using the specified brushmask
		 * Protected Constructor. CinePaintBrush objects are constructed through the static LoadCinePaintBrush method
		 *
		 * @param filename the filename the brush data was originally read from
		 * @param brushname the name of this Brush
		 * @param brushmask the brush mask data
		 * @param spacing
		 * @param x_axis
		 * @param y_axis
		 */
		CinePaintBrush(const char* filename, const char* brushname, AbstractBuf* brushmask, float spacing, const Vector2d& x_axis, const Vector2d& y_axis) ;

		/**
		 * Sets the brush mask of this brush to the specified AbstractBuf
		 *
		 * @param mask the new brush mask
		 */
		void SetMask(AbstractBuf* mask);


		static const unsigned int DEFAULT_BRUSH_SPACING;

	private:

		/** brush filename */
		char* m_filename;

		/** name of this brush */
		char* m_name;

		/** brush's spacing */
		float m_spacing;

		/** for calculating brush spacing */
		Vector2d m_x_axis;

		/** for calculating brush spacing */
		Vector2d m_y_axis;

		/** the actual mask */
		AbstractBuf* m_mask;

}; /* class CinePaintBrush */

#endif /* _CINEPAINT_BRUSH_H_ */
