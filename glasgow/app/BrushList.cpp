/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      BrushList - manages loaded CinePaint brushes
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: BrushList.cpp,v 1.2 2006/12/18 08:21:16 robinrowe Exp $
 */

#include "BrushList.h"
#include "CinePaintBrush.h"
#include "CinePaintBrushLoader.h"
#include <algorithm>
#include <cmath>

const int BrushList::DEFAULT_NAME_UNIQUEFY_LENGTH = 10;

/**
 * Construct a new initially empty BrushList
 *
 */
BrushList::BrushList()
{
	m_active_brush = 0;
	m_noise_mode = false;
	m_opacity = 1.0;
	m_paint_mode = 0;
}

/**
 * Destructor
 */
BrushList::~BrushList()
{
	ClearBrushList();
}



//------------
// List Access

/**
 * Adds the specified brush to this BrushList
 *
 * @param brush the CinePaintBrush to add
 */
void BrushList::AddBrush(CinePaintBrush* brush)
{
	uniquefy_brush_name(brush);
	m_brush_container.push_back(brush);
	
	// @note [claw] signals again!
}

/**
 * Removes the specified brush from this BrushList
 * The caller must take responsibility for the resources of the removed brush.
 *
 * @param brush the CinePaintBrush to remove
 * @return the removed brush
 */
CinePaintBrush* BrushList::RemoveBrush(CinePaintBrush* brush)
{
	CinePaintBrush* removed = 0;

	// @note [claw] signals again!
	//gtk_signal_disconnect_by_data(GTK_OBJECT(brush), brush_list);

	BrushContainer_t::iterator iter = std::find(m_brush_container.begin(), m_brush_container.end(), brush);
	if(iter != m_brush_container.end())
	{
		removed = *iter;
		m_brush_container.erase(iter);
	}

	return(removed);
}

/**
 * Sorts this BrushList by the name of each brush
 *
 */
void BrushList::Sort()
{
	BrushListComparator comparator ;
	std::sort(m_brush_container.begin(), m_brush_container.end(), comparator) ;
}

/**
 * Clears this BrushList
 *
 */
void BrushList::ClearBrushList()
{
	while(!m_brush_container.empty())
	{
		CinePaintBrush* brush = m_brush_container.back();
		m_brush_container.pop_back();

		delete brush;
		brush = 0;
	}

	m_active_brush = 0;
}

/**
 * Returns the number of brushed this BrushList holds
 *
 * @return the number of brushes this BrushList contains
 */
size_t BrushList::Size() const
{
	return(m_brush_container.size());
}





//-------------
// Brush Access

/**
 * Returns the CinePaintBrush with the specified name
 * If the named brush does not exist, 0 is returned
 *
 * @param name the name of the brush to return
 */
CinePaintBrush* BrushList::GetBrush(const char* name) const
{
	CinePaintBrush* brush = 0;

	for(BrushContainer_t::const_iterator citer = m_brush_container.begin(); citer != m_brush_container.end(); ++citer)
	{
		if(strcmp((*citer)->GetName(), name))
		{
			brush = *citer;

			// found the brush so break from the for loop
			break ;
		}
	}

	return(brush);
}

/**
 * Returns a CinePaintBrush by its index within this BrushList
 * The index of a brush is not gauranteed to remain the same
 *
 * @param index the index within this BrushList
 */
CinePaintBrush* BrushList::GetBrushByIndex(unsigned int index) const
{
	CinePaintBrush* brush = 0;

	if(index < m_brush_container.size())
	{
		brush = m_brush_container[index];
	}

	return(brush);
}

/**
 * Returns the index within this BrushList of the specified brush
 * The index of a brush is not gauranteed to remain the same
 *
 * @param brush the brush to get the index of
 * @return the current index of the spexified brush
 */
unsigned int BrushList::GetBrushIndex(CinePaintBrush* brush) const
{
	unsigned int index = 0;

	BrushContainer_t::const_iterator citer = m_brush_container.begin();
	while((citer != m_brush_container.end()) && (*citer != brush))
	{
		index++;
	}

	return(index);
}

/**
 * Return sthe currently active brush
 *
 * @return the currently active brush
 */
CinePaintBrush* BrushList::GetActiveBrush() const
{
	CinePaintBrush* _ret = 0;

	if(m_active_brush)
	{
		_ret = m_active_brush;
	}

	return(_ret);
}

/**
 * Sets the specified brush as the currently selected brush
 *
 * @param brush the brush to set currently selected
 */
void BrushList::SelectBrush(CinePaintBrush* brush)
{
	// Set the active brush
	m_active_brush = brush;

	// @note [claw] probably need to create some kind of abstract change listener here (java-esque) since
	//       we dont have any signal mechanisms available
	//       we need to avoid tieing the data(ie this class) to a specific dialog
}


//----------------------
// active brush settings
// @note [claw] these should prbably be moved out of the BrushList and into an active tool object

void BrushList::SetNoiseMode(bool noiseOn)
{
	m_noise_mode = noiseOn;
}

bool BrushList::GetNoiseMode() const
{
	return(m_noise_mode);
}

void BrushList::SetNoiseInfo(const BrushNoiseInfo& info)
{
	m_brush_noise_info.freq = info.freq;
	m_brush_noise_info.freq_variation = info.freq_variation;
	m_brush_noise_info.step_start = info.step_start;
	m_brush_noise_info.step_width = info.step_width;
}

void BrushList::GetNoiseInfo(BrushNoiseInfo& info) const
{
	info.freq = m_brush_noise_info.freq;
	info.freq_variation = m_brush_noise_info.freq_variation;
	info.step_start = m_brush_noise_info.step_start;
	info.step_width = m_brush_noise_info.step_width;
} 

void BrushList::SetOpacity (double opac)
{
	m_opacity = opac;
}

double BrushList::GetOpacity() const
{
	return(m_opacity);
}

void BrushList::SetPaintMode(int pm)
{
	m_paint_mode = pm;
}

int BrushList::GetPaintMode() const
{
	return(m_paint_mode);
}



//--------------
// Brush Loading

/**
 * Loads the specified brush file and adds it to this brushList
 *
 * @param filename the brush file to load
 */
void BrushList::LoadBrush(const char* filename)
{
	CinePaintBrush* brush = 0 ;

	if(strcmp(&filename[strlen(filename) - 4], ".gbr") == 0)
	{
		brush = CinePaintBrushLoader::LoadBrush(filename);
	}
	//else if(strcmp(&filename[strlen(filename) - 4], ".vbr") == 0)
	//{
		//brush = CinePaintBrushGenerated::LoadCinePaintBrushGenerated(filename);
	//}

	if(brush)
	{
		AddBrush(brush);
	}
	else
	{
		printf("Warning: failed to load brush \"%s\"\n", filename);
	}
}



//--------
// private

/**
 * Creates and returns a default genereated brush
 *
 * @return default generated brush
 */
CinePaintBrush* BrushList::create_default_brush()
{
	//CinePaintBrushGenerated* brush = new CinePaintBrushGenerated("Untitled", 5.0, 5.0, 0.5, 0.0, 1.0);
	//return(brush);
	return(0);
}

/**
 * Alters the name of the specified brush to so that the brush name is unique within this brushList
 * If the brush name matches and existing brush within this BrushList, it is appended with a # and digit
 * to distinguish it
 *
 * @param brush the brush with which to uniquefy the name
 */
void BrushList::uniquefy_brush_name(CinePaintBrush* brush)
{
	// check if we have a name clash
	if(!brush_name_unique(brush->GetName()))
	{
		// names conflict
		const char* oldname = brush->GetName();
		char* newname = new char[strlen(oldname) + DEFAULT_NAME_UNIQUEFY_LENGTH];
		strcpy(newname, oldname);

		int number = 1;
		char* ext = 0;
		if((ext = strrchr(newname, '#')))
		{
			number = atoi(ext+1);
			// cast to log10 param to double appease compiler
			if(&ext[static_cast<int>(log10(static_cast<double>(number))) + 1] != &newname[strlen(newname) - 1])
			{
				number = 1;
				ext = &newname[strlen(newname)];
			}
		}
		else
		{
			number = 1;
			ext = &newname[strlen(newname)];
		}
		sprintf(ext, "#%d", number+1);
		
		// make sure the new name is unique
		while(!brush_name_unique(newname))
		{
			number++;
			sprintf(ext, "#%d", number+1);
		}

		brush->SetName(newname);
		delete[] newname;
		newname = 0;
	}
}

/**
 * Determines whether the specified brush's name is unique within this BrushList
 *
 * @param brush the brush to check the name of
 * @return true if the specified brush's name is unique within this BrushList
 */
bool BrushList::brush_name_unique(const char* name) const
{
	bool unique = true;

	BrushContainer_t::const_iterator citer = m_brush_container.begin();

	while((citer != m_brush_container.end()) && unique)
	{
		if(strcmp(name, (*citer)->GetName()) == 0)
		{
			unique = false;
		}
		++citer;
	}

	return(unique);
}
