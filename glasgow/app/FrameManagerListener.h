/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      FrameManagerListener - frame manager listener interface
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: FrameManagerListener.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _FRAME_MANAGER_LISTENER_H_
#define _FRAME_MANAGER_LISTENER_H_

#include "dll_api.h"
#include "FrameManager.h" // req. for Frame

/**
 * Purely abstract class for receiving events from the FrameManager
 *
 */
class CINEPAINT_CORE_API FrameManagerListener
{
	public:
		virtual ~FrameManagerListener() {}

		/**
		 * Invokedx when the currently selected FrameManager::FrameRecord within the FrameManager is changed
		 *
		 * @param frame the currently selected FrameManager::FrameRecord
		 * @param position the index within the FrameManager of the current selection
		 */
		virtual void SelectionChanged(const FrameManager::FrameRecord* frame, FrameManager::size_type position) = 0;

		/**
		 * Invoked when a new FrameManager::FrameRecord is added to the FrameManager
		 *
		 * @param frame the newly added FrameManager::FrameRecord
		 * @param position the index within the FrameManager of the new addition
		 */
		virtual void FrameAdded(const FrameManager::FrameRecord& frame, FrameManager::size_type position) = 0;

		/**
		 * Invoked when a FrameManager::FrameRecord is removed from the FrameManager
		 *
		 * @param frame the FrameManager::FrameRecord being removed from the FrameManager
		 * @param position the index within the FrameManager of the FrameManager::FrameRecord neing removed
		 */
		virtual void FrameRemoved(const FrameManager::FrameRecord& frame, FrameManager::size_type position) = 0;

		/**
		 * Invoked when the properties of a FrameManager::FrameRecord have been updated
		 *
		 * @param frame the FrameManager::FrameRecord whose properties have been updated
		 * @param position tje index within the FrameManage of the altered FrameManager::FrameRecord
		 * @todo this method of notification needs a rethink, perhaps each frame should signal that
		 *       a change has occured, and indicate what the change is
		 */
		virtual void FrameUpdated(const FrameManager::FrameRecord& frame, FrameManager::size_type position) = 0;

		/**
		 * Invoked when the position of a FrameManager::FrameRecord within a FrameManager is changed
		 *
		 * @param frame the FrameManager::FrameRecord being moved
		 * @param old_pos the previous position the FrameManager::FrameRecord withi the FrameManager
		 * @param new_pos the new position of the FrameManager::FrameRecord within the FrameManager
		 */
		virtual void FrameIndexChanged(const FrameManager::FrameRecord& frame, FrameManager::size_type old_pos, FrameManager::size_type new_pos) = 0;

	protected:
		FrameManagerListener() {};

	private:
};

#endif /* _FRAME_MANAGER_LISTENER_H_ */
