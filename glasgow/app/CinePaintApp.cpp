/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintApp The main application class.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK ******
 *
 * $Id: CinePaintApp.cpp,v 1.9 2008/01/28 23:36:18 robinrowe Exp $
 */

#include <sys/stat.h>
#include <pthread.h>
#include <string.h>

#include "CinePaintApp.h"
#include "Version.h"
#include "CinePaintEventList.h"
#include "FrameManager.h"
#include "ToolDocProxy.h"
#include "BrushList.h"
#include "ColorManager.h"
#include "app/AppSettings.h"
#include "gui/CPDAGDoc.h"
#include "plug-ins/pdb/CinePaintFillOp.h"
#include "plug-ins/pdb/CinePaintProgressListener.h"
#include "plug-ins/pdb/enums.h"
#include "plug-ins/pdb/CinePaintImage.h"
#include "plug-ins/pdb/Layer.h"
#include "plug-ins/pdb/PixelArea.h"
#include "plug-ins/pdb/Color.h"
#include <utility/CommandLineParse.h>
#include <utility/ServerSocket.h>
#include <utility/PlatformUtil.h>
#include <dlfcn.h>
#include <dirent.h>

// initialize static members
CinePaintApp* CinePaintApp::m_app_instance = 0;
World CinePaintApp::world;
//============================
// Application install / Setup

/**
 * Initalises and Starts the CinePaintApp Core, initializing and starting core threads as required.
 * A call to Run returns (almost) immediatly, having started any required worker threads
 *
 */
bool CinePaintApp::InitInstance(CommandLineParse& clp, CinePaintProgressListener& lst)
{	ListenerList<CinePaintProgressListener>* init_prog_list = GetInitList();
	if(init_prog_list)
	{	init_prog_list->AddListener(&lst);
	}
	bool _ret = true;

	m_core_initialize_state = CinePaintApp::INIT_PROGRESS_ENUM;

	//Create a thread which will continue the initalistion.
	pthread_t _init_thread;
	int _res = pthread_create(&_init_thread, NULL, static_InitMainThreadFunc, this);
	if( _res != 0)
	{
		// thread creation failed!
		m_core_initialize_state = CinePaintApp::INIT_FAILED_ENUM;
		printf("Create Thread failed\n");
		_ret = false;
	}
	else
	{
		// Should we start server mode?
		if(clp.m_serverPort)
		{
			m_Server.InitServer(clp.m_serverPort);
		}
	}

	return(_ret);
}


/**
 * Returns true if the CinePaintApp core is installed
 * This method simply checks for the existance of the app settings dir in
 * the users home area
 *
 * @return true if the CinePaintApp core is installed for the current user
 *         false otherwise
 */
bool CinePaintApp::IsInstalled()
{
	bool _installed = false;

	struct stat stat_buf;
	const char* _app_dir = GetPathApp();
	char* _filename = new char[strlen(_app_dir)];

	// Copy it so we can remove the trailing "/" if required.
	strncpy(_filename, _app_dir,  strlen(_app_dir));
	if(_filename[strlen(_app_dir)-1] == *PlatformUtil::CP_PATH_SEPARATOR)
	{
			_filename[strlen(_app_dir)-1] = '\0';
	}

	printf("AppDir is %s\n", _filename);
	if(stat(_filename, &stat_buf) != 0)
	{
		printf(APP_NAME " is not properly installed for the current user\n");
		_installed = false;
	}
	else
	{
		// assume aready installed... the installation dir is there at least
		_installed = true;
	}

	return(_installed);
}

/**
 * Installs the CinePaintApp core for the current user.
 * If CinePaint is already installed for the current user, it is simply reinstalled
 *
 * @param install_log set to the text from the user install log
 * @return true if the CinePaintApp core installed successfully, false otherwise
 */
/*
bool CinePaintApp::UserInstall(std::string& install_log)
{
	bool _installed = false;

	const char* _filename = GetPathApp();
	//struct stat stat_buf;

	if('\000' == _filename[0])
	{
		printf("No home directory -- skipping CinePaint user installation.\n");
		_installed = false;
	}
	else
	{
//		_installed = user_install(install_log);
	}

	return(_installed);
}
*/
/*
Procedure_DB &CinePaintApp::GetPluginDB()
{
	return m_procDB;
}
*/


CinePaintDoc* CinePaintApp::CreateCPDAG()
{	CPDAGDoc* _dag_doc = new CPDAGDoc(&world);
	GetDocList().AddDocument(*_dag_doc);
	return(_dag_doc);
}

CinePaintDoc* CinePaintApp::CreateFrameManager()
{	FrameManager* _fm = new FrameManager();
	GetDocList().AddDocument(*_fm);
	return(_fm);
}
//--------------
// Image Methods

CinePaintDoc* CinePaintApp::CreateCinePaintImage(const CinePaintTag& tag, int width, int height, FillType fill)
{
	CinePaintImage* _image = new CinePaintImage(tag, width, height);

	// set the profile
	//if(cms_workspace_profile_name != NULL)
	//{
	//	gimage_set_cms_profile(gimage, cms_get_profile_from_file(cms_workspace_profile_name));
	//} 

	// Make the background (or first) layer
	Layer* _layer = new Layer(_image->GetTag(), _image->GetWidth(), _image->GetHeight(), "Background", 1.0f);
	_image->GetLayers().AddLayer(_layer, 0, true);

	CinePaintFillOp* _fill_op = GetImageOpManager().GetFillOp(_image->GetTag());
	
	PixelArea *_pa = _layer->GetBuffer().GetPixelArea(0, 0, _layer->GetWidth(), _layer->GetHeight());
	switch(fill)
	{
		case FILL_BACKGROUND_ENUM:
		{
			_fill_op->FillArea(*_pa, GetColorManager().GetBackGround());
			break;
		}	
		case FILL_FOREGROUND_ENUM:
		{
			_fill_op->FillArea(*_pa, GetColorManager().GetForeGround());
			break;
		}
		case FILL_TRANS_ENUM:
		case FILL_WHITE_ENUM:
		default:
		{
			_fill_op->FillArea(*_pa, Color::GetWHITE());
			break;
		}

	}

	delete _pa;
	// Add the new Document
	GetDocList().AddDocument(*_image);

	return(_image);
}

//===================================
// general Application helper methods


void* CinePaintApp::static_InitMainThreadFunc(void* arg)
{
	CinePaintApp* _instance = reinterpret_cast<CinePaintApp*>(arg);
	ListenerList<CinePaintProgressListener>* init_prog_list = GetInitList();
	if(!init_prog_list)
	{	return 0;
	}
	for(ListenerList<CinePaintProgressListener>::const_iterator citer = init_prog_list->begin(); citer != init_prog_list->end(); ++citer)
	{
		(*citer)->ProgressStart("DUMMY", "DUMMY", 0, 100);
	}

	// We now know user install is either complete
	// or not required to can load "cinepaintrc" file.
//    _instance->m_cpRc.Init();

	// initialize the image operations database
	_instance->m_image_op_manager.Initialize();

	// Initalise the main Tools
	_instance->m_tool_manager.Initialize();
	// Connect tools too doc
	ToolDocProxy *tcl = new ToolDocProxy();
	_instance->m_tool_manager.AddToolChangeListener(tcl);
	_instance->m_DocList.AddDocListener(tcl);

	// Load the brushes.
	_instance->load_brush_list();

	// Load Gradients.

	// Load Plugins.
	world.pdb=new PDB();
//	PDB* pdb=PDB::GetPDB();
	const bool ok=_instance->InitializePDB();
	world.pdb->Dump();
	// Init done so show main tool bar.
	//	GetUIThreadEventList().PostEvent(new ShowMainToolbarEvent(true));
	_instance->m_core_initialize_state = CinePaintApp::INIT_COMPLETE_ENUM;
	// When progress is complete, remove listener.  It can't be done with an iterator
	// as remove is called on the listener list, invalidating it.
	ListenerList<CinePaintProgressListener>* pll = ListenerList<CinePaintProgressListener>::GetInitProgressListenerList();
	while(!pll->empty())
	{
		CinePaintProgressListener* p = pll->pop_front();
		p->ProgressComplete("DUMMY");
	}

	//pthread_exit(0); // implicitly called when this routine exits
	return(0);
}

void CinePaintApp::RegisterProgressListener(CinePaintProgressListener *lst)
{	ListenerList<CinePaintProgressListener>* prog_list = GetProgressList();
	if(prog_list)
	{	prog_list->AddListener(lst);
	}
//	m_ProgressListenerList.AddListener(lst);
}


void CinePaintApp::RegisterDialogListener(CinePaintDialogListener *lst)
{	ListenerList<CinePaintDialogListener>* dlg_list = GetDialogList();
	if(dlg_list)
	{	dlg_list->AddListener(lst);
	}
//	m_DialogListenerList.AddListener(lst);
}

void CinePaintApp::RemoveInitProgressListener(CinePaintProgressListener *lst)
{
	ListenerList<CinePaintProgressListener>::GetInitProgressListenerList()->RemoveListener(lst);
}

//=================
// Initialisation Progress Listener helper methods.
/** 
 * Register a Listener for init process updates.
**/
//void CinePaintApp::RegisterInitProgress(CinePaintProgressListener *lst)
//{
//	m_InitProgressListenerList.AddListener(lst);
//}


void CinePaintApp::init_progress_set_text(char* msg)
{	ListenerList<CinePaintProgressListener>* prog_list = GetProgressList();
	if(!prog_list)
	{	return;
	}
	for(ListenerList<CinePaintProgressListener>::const_iterator citer = prog_list->begin(); citer != prog_list->end(); ++citer)
	{
		(*citer)->MessageChanged("splash_screen", msg);
	}
}

void CinePaintApp::init_progress_reset(int min, int max, int step)
{	ListenerList<CinePaintProgressListener>* init_prog_list = GetInitList();
	if(!init_prog_list)
	{	return;
	}
	for(ListenerList<CinePaintProgressListener>::const_iterator citer = init_prog_list->begin(); citer != init_prog_list->end(); ++citer)
	{
		(*citer)->LimitsChanged("splash_screen", min, max, step);
	}
}

void CinePaintApp::init_progress_update()
{	ListenerList<CinePaintProgressListener>* init_prog_list = GetInitList();
	if(!init_prog_list)
	{	return;
	}
	for(ListenerList<CinePaintProgressListener>::const_iterator citer = init_prog_list->begin(); citer != init_prog_list->end(); ++citer)
	{
		(*citer)->ProgressUpdate("splash_screen" );
	}
}

int CinePaintApp::load_brush_list()
{// Get Brush Directory.
	const char* _brush_dir=GetPathBrushes();
	if (!_brush_dir)
	{	printf("ERROR: No brush path defined\n");
		return 0;
	}
	char _brush_full_path[400];
	char _info_txt[400];
	int _ret = 1;
	int _brush_count = 0;
//	sprintf(_search_fname, "%s/*.gbr", _brush_dir);
	init_progress_reset(0,_brush_count, 1);
	DIR* dp=opendir(_brush_dir);
	if(!dp)
	{	printf("ERROR: opendir() failed: %s\n",_brush_dir);
		return 0;
	}
	dirent* node;
	for(;;)
	{	node=readdir(dp);
		if(!node)
		{	break;
		}
//		if(!(node->d_ino))
//		{	continue;
//		}
		const char* brush=node->d_name;
		if(!strstr(brush,".gbr"))
		{	printf("Ignoring, not a .gbr brush: %s\n",brush);
			continue;
		}
		_brush_count++;
		sprintf(_brush_full_path, "%s/%s",_brush_dir, brush);
		sprintf(_info_txt, "Loading Brush: brushes/%s", brush);
		puts(_info_txt);
		init_progress_set_text(_info_txt);
		m_brushList.LoadBrush(_brush_full_path);
		init_progress_update();
	}
	closedir(dp);
	puts("");
	printf("Brush Search '%s' found %i brushes\n", _brush_dir,_brush_count);
	return 0;
}

bool CinePaintApp::InitializePDB()
{	char _search_fname[200];
	char _plugin_full_path[400];
	char _info_txt[400];
	int _ret = 1;
	int _plugin_count=0;
	// Loop over 'plugin-ins path' and load all *.so files.
	
	// Pointer to the create plugin function.
	CinePaintPlugin::PFNCreateCinePaintPlugin *_PluginCtor = 0;
	CinePaintPlugin::PFNGetPluginClassList _PluginListFunc = 0;
	sprintf(_search_fname, "%s", GetPathPlugins());
	init_progress_reset(0,_plugin_count, 1);
	DIR* dp=opendir(_search_fname);
	if(!dp)
	{	printf("ERROR: opendir() failed: %s\n",_search_fname);
		return 0;
	}
	dirent* node;
	for(;;)
	{	node=readdir(dp);
		if(!node)
		{	break;
		}
//		if(!(node->d_ino))
//		{	continue;
//		}
		const char* plugin=node->d_name;
		if(!strstr(plugin,GetPluginsExt()))
		{	printf("Ignoring, not a plugin: %s\n",plugin);
			continue;
		}
		sprintf(_plugin_full_path, "%s/%s", GetPathPlugins(),plugin);
		sprintf(_info_txt, "Loading Plugin: %s",_plugin_full_path);
		//		GetUIThreadEventList().PostEvent( new InitDialogSetTextEvent(true,_info_txt));
		init_progress_set_text(_info_txt);
		void* _plugin_lib = dlopen(_plugin_full_path, RTLD_LAZY);
		if(_plugin_lib == 0) 
		{	printf("WARN: couldn't LoadLibrary %s\n",_plugin_full_path);
			continue;
		}
		_PluginListFunc = (CinePaintPlugin::PFNGetPluginClassList)dlsym(_plugin_lib,"GetPluginClassList"); 
		// Create an instance and run Query Method.
		if(!_PluginListFunc)
		{	printf("WARN: couldn't _PluginListFunc %s\n",_plugin_full_path);
			continue;
		}
		_PluginCtor = _PluginListFunc();
		while (*_PluginCtor)
		{	CinePaintPlugin *plugin = (*_PluginCtor)();
			plugin->query(&world);
			CinePaintPlugin::PFNDeleteCinePaintPlugin del = plugin->GetDeleteFunc();
			if (del)         // Use the plugins clean up routine if available
				del(plugin); // this is required since here we do not know what size
			else             // plugin actually is.
				delete plugin;   // NOT good (incomplete type), but better than nothing.
			++_PluginCtor;
		}
		//		GetUIThreadEventList().PostEvent( new InitDialogUpdateProgressEvent(true));
		init_progress_update();
	}
	return true;
}
