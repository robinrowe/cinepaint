/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Modified for CinePaint by Colin law 2004/09/22
 *
 * $Id: Boundary.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "Boundary.h"

/**
 * Constructs an initially empty boundary containing zero segments
 *
 */
Boundary::Boundary()
{}

/**
 * Destructor, destroys all added boundary segments
 */
Boundary::~Boundary()
{}


/**
 * Clears the current Boundary List
 *
 */
void Boundary::Clear()
{
	while(!m_boundary_list.empty())
	{
		BoundarySegment* _seg = m_boundary_list.front();
		m_boundary_list.pop_front();

		delete _seg;
		_seg = 0;
	}
}

/**
 * Returns the number of BoundarySegments within this Boundary
 *
 * @return the number of BoundarySegments within this Boundary
 */
size_t Boundary::GetBoundaryCount() const
{
	return(m_boundary_list.size());
}


//---------------
// Nested classes


/**
 * Construcrts a new Boundary Segment from point (x1, y1) to (x2, y2)
 *
 * @param x1 x coordinate of segment start point
 * @param y1 y coordinate of segment start point
 * @param x2 x coordinate of segment end point
 * @param y2 y coordinate of segment end point
 * @param open
 * @param visited
 */
Boundary::BoundarySegment::BoundarySegment(int x1, int y1, int x2, int y2, bool open, bool visited)
: m_x1(x1), m_y1(y1), m_x2(x2), m_y2(y2), m_open(open), m_visited(visited)
{}

