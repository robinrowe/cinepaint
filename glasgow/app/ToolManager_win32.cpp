/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ToolManage Tool Operation plugin loading module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ToolManager_win32.cpp,v 1.6 2007/01/01 00:57:06 robinrowe Exp $
 */

#ifndef _WIN32
#error This file used only in a Windows build
#endif

#include "ToolManager.h"
#include "CinePaintApp.h"
#include <plug-ins/CinePaintTool.h>
#include <app/AppSettings.h>
#include <errno.h>
#include <direct.h>
#include <string.h>
#include <windows.h>

/**
 * Initializes, and loads available CinePaintTool plugins from an application specified path
 *
 */
void ToolManager::Initialize()
{
	const char* _plugin_full_path=GetPathToolsPlugin(); 
	// Handle too the dll
	HINSTANCE hdll;

	// Pointer to the create tool functions
	CinePaintTool::PFNCreateCinePaintTool* _ToolCtor = 0;
	CinePaintTool::PFNGetToolClassList _ToolListFunc = 0;
	hdll = LoadLibrary(_plugin_full_path); 
	if(!hdll) 
	{	//BUG
		printf("ERROR: couldn't LoadLibrary %s\n", _plugin_full_path);
		return;
	}
	_ToolListFunc = (CinePaintTool::PFNGetToolClassList)GetProcAddress(hdll, "GetToolClassList"); 
	if(!_ToolListFunc)
	{	//BUG
		printf("ERROR: %s entry point not Found\n","GetToolClassList");
		return;
	}
	_ToolCtor = _ToolListFunc();
	while (*_ToolCtor)
	{
		// Construct and keep a pointer to the 'Core Tool'
		// These plugins are 'core' to the application, so we do not create and destroy on-the-fly,
		// we always want these to be available. If the construction method return non-zero, we
		// register the tool with this ToolManager for future use. The ToolManager assumes the reponsiblity
		// of the resource from this point onwards.
		CinePaintTool* _tool = (*_ToolCtor)();
		if(_tool)
		{	RegisterTool(_tool);
		}
		++_ToolCtor;
	}
}
