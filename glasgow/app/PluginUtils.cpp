/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintPlugin Interface
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: PluginUtils.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "PluginUtils.h"

PluginUtils::PluginUtils()
{}

PluginUtils::~PluginUtils()
{}

std::string& PluginUtils::StripWindowName(std::string& plugin_path)
{
	// format the menu path to remove the legacy window name i.e. '<image>'
	// assumes that all the paths a re prefixed in some way by <...>
	std::string::size_type _first_pos = plugin_path.find("<", 0);
	std::string::size_type _last_pos = plugin_path.find(">", _first_pos);

	if(_first_pos != std::string::npos && _last_pos != std::string::npos)
	{
		// step over the '>' char
		_last_pos++;

		// if the next char is a '/' skip that too - probably always do this, but you never know
		if(plugin_path[_last_pos] == '/')
		{
			_last_pos++;
		}

		plugin_path.erase(_first_pos, _last_pos - _first_pos);
	}

	return(plugin_path);
}

std::string& PluginUtils::StripMenuPath(std::string& plugin_path)
{
	std::string::size_type _last_pos = plugin_path.rfind("/");
	plugin_path.erase(0, _last_pos +1);

	return(plugin_path);
}

