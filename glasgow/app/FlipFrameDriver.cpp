/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      FlipFrameDriver - FrameManager timer
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: FlipFrameDriver.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "FlipFrameDriver.h"
#include "FlipFrameController.h"
#include "FrameManager.h"
#include <utility/PlatformUtil.h> // req. for CpSleep

/**
 * Constructs a new FlipFrameDriver to drive a FrameManager
 *
 * @param frame_manager the FrameManager being driven
 */
FlipFrameDriver::FlipFrameDriver(FrameManager& frame_manager)
		: m_frame_manager(frame_manager)
{}

/**
 * Destructor
 */
FlipFrameDriver::~FlipFrameDriver()
{}

/**
 * Returns the FrameManager this FlipFrameDriver is driving
 *
 * @return the FrameManager this FlipFrameDriver is driving
 */
FrameManager& FlipFrameDriver::GetFrameManager()
{
	return(m_frame_manager);
}






//----------------------------------------------------------------


/**
 * Constructs a new SimpleFlipFrameDriver to drive a FrameManager
 *
 * @param frame_manager the FrameManager being driven
 * @param controller the FlipFrameController controlling this driver
 */
SimpleFlipFrameDriver::SimpleFlipFrameDriver(FrameManager& frame_manager, FlipFrameController& controller)
		: FlipFrameDriver(frame_manager), m_controller(controller)
{
//	m_run_thread = 0;
	m_control_flag = false;
	m_delay = 500000;
	m_driver_pause = true;


	if(pthread_mutex_init(&m_driver_pause_mutex, 0) != 0)
	{
		printf("Failed to initialize store mutex\n");
	}

	if(pthread_cond_init(&m_driver_pause_cond, 0) != 0)
	{
		printf("Failed to initialize store condition variable\n");
	}

	m_control_flag = true;
	if(pthread_create(&m_run_thread, NULL, SimpleFlipFrameDriver::run, this) != 0)
	{
		// thread creation failed!
		printf("SimpleFlipFrameDriver thread creation failed\n");
		m_control_flag = false;
	}
}

/**
 * Destructor
 */
SimpleFlipFrameDriver::~SimpleFlipFrameDriver()
{
	// stop the thread
	if(m_control_flag)
	{
		m_control_flag = false;

		pthread_mutex_lock(&m_driver_pause_mutex) ;
		m_driver_pause = false;
		pthread_cond_signal(&m_driver_pause_cond) ;
		pthread_mutex_unlock(&m_driver_pause_mutex) ;

		printf("Waiting for SimpleFlipFrameDriver thread to complete\n");
		if(pthread_join(m_run_thread, 0) != 0)
		{
			printf("Error joining on SimpleFlipFrameDriver thread\n");
		}
	}
}



//--------------------------
// Flip Frame Driver Control

/**
 * Starts this FlipFrameDriver driving the FrameManager
 *
 */
void SimpleFlipFrameDriver::Start()
{
	pthread_mutex_lock(&m_driver_pause_mutex) ;
	m_driver_pause = false;
	pthread_cond_signal(&m_driver_pause_cond) ;
	pthread_mutex_unlock(&m_driver_pause_mutex) ;
}

/**
 * Stops this FlipFrameDriver from driving the FrameManager
 *
 */
void SimpleFlipFrameDriver::Stop()
{
	pthread_mutex_lock(&m_driver_pause_mutex) ;
	m_driver_pause = true;
	pthread_cond_signal(&m_driver_pause_cond) ;
	pthread_mutex_unlock(&m_driver_pause_mutex) ;
}

/**
 * Returns the running state of this FlipFrameDriver
 *
 * @return true if this FlipFrameDriver is currently running, false otherwise
 */
bool SimpleFlipFrameDriver::IsRunning() const
{
	return(m_control_flag && !m_driver_pause);
}



//--------------------------
// Delay / Direction Control

/**
 * Gets the current delay between updates to the FrameManager
 *
 * @return the current delay in Milliseconds
 */
unsigned long SimpleFlipFrameDriver::GetDelay() const
{
	return(m_delay);
}

/**
 * Sets the current delay between updates to the FrameManager
 *
 * @param millis the delay between FrameManager updates in Milliseconds
 */
void SimpleFlipFrameDriver::SetDelay(unsigned long usec)
{
	m_delay = usec;
}






/**
 * Thread run function
 *
 * @param arg instance of the running FlipFrameDriver
 */
void* SimpleFlipFrameDriver::run(void* arg)
{
	printf("Entering SimpleFlipFrameDriver::run\n");
	SimpleFlipFrameDriver* instance = reinterpret_cast<SimpleFlipFrameDriver*>(arg);

	// initial update pause to avoid immediatly changing the current selection on start
	// generally we start by clicking one frame within the UI, as this becomes the first
	// frame, we dont want to change it straight away, so we pause first.
	// We possibly want to do this the other way around, by setting the selected item,
	// then pauseing as normal
	PlatformUtil::CpSleep(instance->m_delay);

	while(instance->m_control_flag)
	{
		bool done = false;
		switch(instance->m_controller.GetDirection())
		{
			case FlipFrameController::FORWARD_ENUM:
				{
					done = (instance->GetFrameManager().Next() == FrameManager::INVALID_INDEX);
					break;
				}
			case FlipFrameController::BACKWARD_ENUM:
				{
					done = (instance->GetFrameManager().Previous() == FrameManager::INVALID_INDEX);
					break;
				}
			default:
				{
					break;
				}
		}

		if(done)
		{
			instance->m_controller.Stop();
			printf("Automatically stopping driver thread, end of programe reached\n");
		}
		else if(instance->m_driver_pause)
		{
			while(instance->m_control_flag && instance->m_driver_pause)
			{
				pthread_mutex_lock(&instance->m_driver_pause_mutex);
				pthread_cond_wait(&instance->m_driver_pause_cond, &instance->m_driver_pause_mutex);
				pthread_mutex_unlock(&instance->m_driver_pause_mutex);
			}
		}
		else
		{
			PlatformUtil::CpSleep(instance->m_delay);
		}
	}

	printf("Exiting SimpleFlipFrameDriver::run\n");

	return(0);
}
