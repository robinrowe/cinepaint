/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintDocList - Manages doc list.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintDocList.cpp,v 1.3 2006/12/21 11:18:04 robinrowe Exp $
 */

#include "CinePaintDocList.h"
#include "CinePaintApp.h"
#include "CinePaintDocListListener.h"
#include "plug-ins/pdb/ListenerList.h"
#include "plug-ins/pdb/CPWidgetBuilder.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/CinePaintImage.h"
#include <utility/PlatformUtil.h>

#include <algorithm>
#include <string>
#include <sstream>


CinePaintDocList::CinePaintDocList(World* world)
{	this->world=world;
	m_pCurrentDoc =0 ;
}

CinePaintDocList::~CinePaintDocList()
{
}

CinePaintDoc* CinePaintDocList::OpenDocument(const char* file_path)
{	CinePaintImage* _doc = 0;
	printf("Opening File : %s\n", file_path);
	PDB* pdb=PDB::GetPDB(world);
	if(!pdb)
	{	return 0;
	}
	CinePaintPlugin::PFNCreateCinePaintPlugin pfn = pdb->GetLoadHandler(file_path);
	if(!pfn)
	{	printf("Error No Load Plugin Found\n");
		return 0;
	}	
	CinePaintPlugin *t = pfn();
	CPPluginArg arg;
	CPParamList _in;
	CPParamList _return;

	// allocated memory is passaed to the param list, and is released by the
	// plugins param list on its release
	arg.pdb_cstring = new char[strlen(file_path) +1];
	memset(arg.pdb_cstring, '\0', strlen(file_path) +1);
	strncpy(arg.pdb_cstring, file_path, strlen(file_path));
	_in.AddParam("filename", &arg, PDB_CSTRING);

	t->run(_in, _return);

	if(_return.RemoveParam("image", arg, PDB_CPIMAGE) == 1)
	{
		// we take ownership of the image, removing it from the param list,
		// and pass it onto the doc list to manage
		_doc = arg.pdb_cpimage;

		// the initial reference count is set to 0, the added document must be
		// referenced after this call
		DocRec* _rec = new DocRec(_doc, 0);
		std::string id = generate_doc_id(*_doc);
		m_docList.insert(std::pair<std::string, DocRec*>(id, _rec));

		// let all out registered Doc Listeners know
		for(ListenerList_t::const_iterator citer = m_listener_list.begin(); citer != m_listener_list.end(); ++citer)
		{
			(*citer)->DocOpened(id.c_str(), *_doc);
		}

		//SetCurrentDoc(*_doc);
	}
	else
	{
		printf("ERROR: %s Plugin failed to load file\n", t->GetName());
	}
	_in.RemoveParam("filename", arg, PDB_CSTRING);
	delete arg.pdb_cstring;
	// Try destruction in DLL.
	pdb->ReleaseInstance(t);
	return(_doc);
}

CinePaintDoc* CinePaintDocList::OpenDocument(const char* file_path, const char* type)
{
	return(0);
}

void CinePaintDocList::AddDocument(CinePaintDoc& doc)
{
	// the initial reference count is set to 0, the added document must be
	// referenced after this call
	DocRec* _rec = new DocRec(&doc, 0);
	std::string id = generate_doc_id(doc);
	m_docList.insert(std::pair<std::string, DocRec*>(id, _rec));

	// let all out registered Doc Listeners know
	for(ListenerList_t::const_iterator citer = m_listener_list.begin(); citer != m_listener_list.end(); ++citer)
	{
		(*citer)->DocOpened(id.c_str(), doc);
	}
}

void CinePaintDocList::SaveCurrentDocument()
{
	// @TODO
}

void CinePaintDocList::SaveCurrentDocumentAs(const char* file_name)
{
	// @TODO
}

void CinePaintDocList::SaveDocument(CinePaintDoc& doc, const char* file_name)
{
	printf("Saving Document as : %s\n", file_name);
	PDB* pdb=PDB::GetPDB(world);
	if(!pdb)
	{	return;
	}
	CinePaintPlugin::PFNCreateCinePaintPlugin pfn = pdb->GetSaveHandler(file_name);
	CPParamList _in;
	CPParamList _return;
	if (pfn)
	{
		CinePaintPlugin *_save_handler = pfn();
		doc.SetSaveArgs(_in);
		CPPluginArg arg; 
		arg.pdb_cstring = new char[1024];
		strncpy(arg.pdb_cstring, file_name, 1024);
		_in.AddParam("filename", &arg, PDB_CSTRING);
		CPPluginUIDef ui;
		DialogResponse _res = CinePaintApp::GetInstance().GetUIWidgetBuilder().DoModalDialog(_save_handler->GetUIDefs(ui), _in);
		if ( _res == OK_ENUM )
            _save_handler->run(_in, _return);
		else
			printf("User cancelled Save\n");

		doc.UnSetSaveArgs(_in);
		_in.RemoveParam("filename", arg, PDB_CSTRING);
		delete arg.pdb_cstring;
	}
}

void CinePaintDocList::SaveDocument(CinePaintDoc& doc)
{
	// Simply call the fuller version.
	SaveDocument(doc, doc.GetFileName());
}

/**
 * reverts the specified document to the version on disk
 *
 * @param doc the CinePaintDoc to reload/revert
 */
void CinePaintDocList::RevertDocument(CinePaintDoc& doc)
{	if(doc.GetDocType() != CinePaintDoc::CINEPAINT_IMAGE_ENUM)
	{	printf("Error No Load Plugin Found\n");
		return;
	}
	const char* _filename = doc.GetFileName();
	printf("Reverting File : %s\n", _filename);
	PDB* pdb=PDB::GetPDB(world);
	if(!pdb)
	{	return;
	}
	CinePaintPlugin::PFNCreateCinePaintPlugin pfn = pdb->GetLoadHandler(_filename);
	CPParamList _in;
	CPParamList _return;
	if(!pfn)
	{	printf("Error Loading img\n");
		return;
	}
	CinePaintPlugin* _plugin = pfn();
	CPPluginArg arg; 
	
	arg.pdb_cstring = new char[256];
	strncpy(arg.pdb_cstring, _filename, 256);
	_in.AddParam("filename", &arg, PDB_CSTRING);
	_plugin->run(_in, _return);

	AbstractBuf* _buf;
	if(_return.GetParam("background", &_buf))
	{
		// @note [claw] need to review how this works
		//       should we create a new image, and 'set' it, or reset the existing image?
		//       if we reset, should we handle the wok, or the image itself

		// remove any existing layers
		CinePaintImage* _img = reinterpret_cast<CinePaintImage*>(&doc);				

		_img->Reset(_buf->GetTag(), _buf->GetWidth(), _buf->GetHeight());
		_img->AddLayer(*_buf, "Background");
		_img->Update(0, 0, _img->GetWidth(), _img->GetHeight());
	}
}

CinePaintDoc* CinePaintDocList::GetCurrentDoc()
{
	return (m_pCurrentDoc);
}

void CinePaintDocList::SetCurrentDoc(const char* id)
{
	DocList_t::const_iterator _citer = m_docList.find(std::string(id));
	if(_citer != m_docList.end())
	{
		printf("CinePaintDocList::SetCurrentDoc changed\n");
		m_pCurrentDoc = _citer->second->m_doc;

		// let all out registered Doc Listeners know
		for(ListenerList_t::const_iterator _lciter = m_listener_list.begin(); _lciter != m_listener_list.end(); ++_lciter)
		{
			(*_lciter)->CurrentDocChanged(id, m_pCurrentDoc);
		}
	}
}

/**
 * Sets the currently active document
 * If the specified document is not open and managed by CinePaintDocList, no change is made
 * 
 * @param doc the currently active document
 */
void CinePaintDocList::SetCurrentDoc(CinePaintDoc& doc)
{
	DocList_t::const_iterator _citer = m_docList.begin();
	bool _found = false;

	while(!_found && _citer != m_docList.end())
	{
		if(_citer->second->m_doc == &doc)
		{
			_found = true;
			SetCurrentDoc(_citer->first.c_str());
		}
		else
		{
			++_citer;
		}
	}
}


bool CinePaintDocList::RefDocument(CinePaintDoc& doc)
{
	bool _found = false;

	DocList_t::const_iterator _citer = m_docList.begin();
	while(!_found && _citer != m_docList.end())
	{
		if(_citer->second->m_doc == &doc)
		{
			_citer->second->m_ref_count++;
			_found = true;
			printf("Referenced Doc %d\n", _citer->second->m_ref_count);
		}

		++_citer;
	}

	return(_found);
}

bool CinePaintDocList::UnRefDocument(CinePaintDoc& doc)
{
	bool _found = false;

	DocList_t::const_iterator _citer = m_docList.begin();
	while(!_found && _citer != m_docList.end())
	{
		if(_citer->second->m_doc == &doc)
		{
			_citer->second->m_ref_count--;
			_found = true;

			printf("UnReferenced Doc %d\n", _citer->second->m_ref_count);

			if(_citer->second->m_ref_count <= 0)
			{
				printf("Released last reference to doc, closing\n");
				close_document(_citer->second->m_doc);
			}
		}
		
		++_citer;
	}

	return(_found);
}

int CinePaintDocList::GetRefCount(CinePaintDoc& doc)
{
	// @note[claw] im not convinced making this available is a good idea, but it is required
	//     in order to displpay a save dialog prior to closing the last reference to a doc.
	//     An alternative would be to catch the close event within a DocListListener and
	//     display the dialog then, at which point the view is already likely to have been
	//     closed (to trigger the unref) making a 'cancel' option harder to implement.

	int _ret = 0;
	bool _found = false;

	DocList_t::const_iterator _citer = m_docList.begin();
	while(!_found && _citer != m_docList.end())
	{
		if(_citer->second->m_doc == &doc)
		{
			_ret = _citer->second->m_ref_count;
			_found = true;
		}
		_citer++;
	}

	return(_ret);
}



const char* CinePaintDocList::GetDocId(const CinePaintDoc& doc) const
{
	const char* _ret = 0;

	bool _found = false;

	DocList_t::const_iterator _citer = m_docList.begin();
	while(!_found && _citer != m_docList.end())
	{
		if(_citer->second->m_doc == &doc)
		{
			_found = true;
			_ret = _citer->first.c_str();
		}
		
		++_citer;
	}

	return(_ret);
}

CinePaintDoc* CinePaintDocList::GetDocById(const char* id)
{
	CinePaintDoc* doc = 0;

	DocList_t::const_iterator _citer = m_docList.find(std::string(id));

	if(_citer != m_docList.end())
	{
		doc = _citer->second->m_doc;
	}

	return(doc);
}

std::list<const char*>& CinePaintDocList::GetDocIdsByType(CinePaintDoc::DocType type, std::list<const char*>& ids)
{
	for(DocList_t::const_iterator _citer = m_docList.begin(); _citer != m_docList.end(); ++_citer)
	{
		if(_citer->second->m_doc->GetDocType() == type)
		{
			ids.push_back(_citer->first.c_str());
		}
	}

	return(ids);
}


//---------------------------
// Doc List Listener handling

/**
 * Adds the specified CinePaintDocListListener to receive Doc List events from this CinePaintDocList
 * if the specified CinePaintDocListListener has already been added it is not added again.
 * The resources of the specified CinePaintDocListListener are not managed by this class
 *
 * @param listener the CinePaintDocListListener to be added
 */
void CinePaintDocList::AddDocListener(CinePaintDocListListener* listener)
{
	m_listener_list.AddListener(listener);
}

/**
 * Removes and returns the specified CinePaintDocListListener.
 * The removed CinePaintDocListListener will no longer receive Doc List events from this CinePaintDocList
 * If the specified listener is not present, 0 is returned
 *
 * @param listener the CinePaintDocListListener to be removed
 */
CinePaintDocListListener* CinePaintDocList::RemoveDocListener(CinePaintDocListListener* listener)
{
	return(m_listener_list.RemoveListener(listener));
}




void CinePaintDocList::close_current_document(bool save)
{
	// @TODO
}

void CinePaintDocList::close_document(CinePaintDoc* doc)
{
	bool _found = false;
	if(!m_docList.size())
	{	return;
	}
	DocList_t::iterator _iter = m_docList.begin();
	while(!_found && _iter != m_docList.end())
	{
		if(_iter->second->m_doc == doc)
		{
			_found = true;

			// let all out registered Doc Listeners know
			for(ListenerList_t::const_iterator lciter = m_listener_list.begin(); lciter != m_listener_list.end(); ++lciter)
			{
				(*lciter)->DocClosed(_iter->first.c_str(), *doc);
			}

			m_docList.erase(_iter);

			// destroy the document
			delete doc;
			doc = 0;
		}
		
		++_iter;
	}
}

/**
 * Generates a new CinePaintDoc ID for the specified document.
 * The document ID is unique for all documents contained within this CinePaintDocList.
 * The ID is based upon the filename of the document, appending a numerical suffix
 * to differentiate between different copies of the same file being open at once.
 *
 * @param doc the CinePaintDoc for which to generate the id/
 * @return a new id which is unique within this CinePaintDocList for the managed document.
 */
std::string CinePaintDocList::generate_doc_id(CinePaintDoc& doc)
{
	// @TODO
	std::string s;
	std::string prefix;

	if(doc.HasFileName())
	{
		prefix = std::string(doc.GetFileName());
		std::string::size_type pos = prefix.rfind(PlatformUtil::CP_PATH_SEPARATOR, prefix.length() -1);
		prefix = prefix.substr(pos +1, (prefix.size() -1) - pos);
	}
	else
	{
		switch(doc.GetDocType())
		{
			case CinePaintDoc::CINEPAINT_IMAGE_ENUM:
			{
				prefix = "Untitled IMG";
				break;
			}
			case CinePaintDoc::CINEPAINT_DAG_ENUM:
			{
				prefix = "Untitled DAG";
				break;
			}
			case CinePaintDoc::CINEPAINT_FRAMEMANAGER_ENUM:
			{
				prefix = "Untitled FRM";
				break;
			}
			default:
			{
				prefix = "Untitled DOC";
				break;
			}
		}
	}

	unsigned long uniquifier = 0;
	bool done = false;
	while(!done)
	{
		// @TODO this could loop forever!

		std::ostringstream ss;
		uniquifier++;
		ss << prefix << "-" << uniquifier;

		if(m_docList.find(ss.str()) == m_docList.end())
		{
			s = ss.str();
			done = true;
		}
	}

	return(s);
}
