/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintEventList - UI event list 
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintEventList.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CINEPAINTEVENTLIST_H_
#define _CINEPAINTEVENTLIST_H_

#include "dll_api.h"

#include <list>
#include <pthread.h>

// forward declaration
class CinePaintEvent;

/**
 * CinePaintEventList is an event list that may be processed from within an arbitary thread.
 * CinePaintEvents are added to the list as required, from any thread, and are processed, in order,
 * with a call to Process. The process method processes the first element within the current event
 * list.
 *
 * The orignal intention of this Event List is to process events within the FLTK event loop, as with
 * most other toolkits, FLTK is not thread safe, that is, you cannot simply call any FLTK function from
 & an arbitary thread. If this list is Processed within the FLTK event loop, we have a convenient mechanism
 * for posting arbitary events into the loop. Note: the word event here is not an FLTK specific event, but
 * instead a generic, user defined process event. This approach is by no means real time, and relies upon
 * repeated calls to Process, in order to /process/ this event list.
 */
class CINEPAINT_CORE_API CinePaintEventList
{
	public:
		/**
		 * Constructs an initially empty event list
		 */
		CinePaintEventList();

		/**
		 * Destructor, will destroy all unmanaged events without calling their Process method
		 */
		virtual ~CinePaintEventList();

		/**
		 * Process the first element within this Event list.
		 * Events added to the list during a call to Process are not immediatly processed. These events
		 * will be processed within subsequent calls to Process upon this Event List.
		 * A call to Process returns true if an event was actually process, false otherwise, for example
		 * if process was called upon an empty event list.
		 *
		 * @return true if any events were processed, false otherwise
		 */
		virtual bool Process();

		/**
		 * Returns the number of CinePaintEvents within the event list
		 *
		 * @return the size of the event list
		 */
		size_t Size() const;

		/**
		 * Post an event into the event list to be processed
		 *
		 * @param e the event to add
		 * @return the current size of the event list
		 */
		virtual size_t PostEvent(CinePaintEvent* e);

	protected:

	private:
		typedef std::list<CinePaintEvent*> EventList_t;

		/** the event list */
		EventList_t m_event_list;

		/** used to lock access to the event list */
		pthread_mutex_t m_event_list_lock;

}; /* CinePaintEventList */


#endif /* _CINEPAINTEVENTLIST_H_ */
