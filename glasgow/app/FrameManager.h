/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      FrameManager - frame manager
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: FrameManager.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _FRAME_MANAGER_H_
#define _FRAME_MANAGER_H_

#include "dll_api.h"
#include "plug-ins/pdb/CinePaintDoc.h"
#include "plug-ins/pdb/ListenerList.h"
#include <vector>
#include <string>
#include <pthread.h>

class FrameManagerListener;

/**
 * FrameManager the management of an ordered list of frames to be displayed/processed
 * The FrameManager provides a list of FrameRecord which detail the frame to be processed,
 * it does not performaany actual processing, nor any image loading/preparation.
 *
 */
class CINEPAINT_CORE_API FrameManager : public CinePaintDoc
{
	public:
		// Nested Classes

		/**
		 * Representation of a Frame within this FrameManager
		 * The FrameRecord indicates the data source of the frame by either a source/filepath
		 * or by an Id of an already loaded image, as given by a CinePaintDocList.
		 *
		 */
		class CINEPAINT_CORE_API FrameRecord
		{
			public:
				/**
				 * Constructs a new FrameRecord
				 *
				 * @param id the id of the data source of this FrameRecord
				 * @param source the filename source of this FrameRecord
				 * @param duration the duration onscreen of this frame during presentation
				 */
				FrameRecord(const std::string& id, const std::string& source, long duration);

				/**
				 * Destructor
				 */
				~FrameRecord();

				/**
				 * Sets the Id of the data source of this FrameRecord
				 * The Id is the Id as given by a CinePaintDocList which is currently managing
				 * the loaded image.
				 *
				 * @param id the new Id of the data source of this FrameRecord
				 */
				void SetId(const std::string& id);

				/**
				 * Returns the Id of the image data within CinePaint for this frame
				 *
				 * @return the Id of the image data
				 */
				const std::string& GetId() const;

				/**
				 * Sets the filename of the data source within this FrameRecord
				 *
				 * @param source the new filename
				 */
				void SetSource(const std::string& source);

				/**
				 * Returns the Filename of the data source for this FrameRecord
				 *
				 * @return the filename of the data source
				 */
				const std::string& GetSource() const;

				/**
				 * Sets the duration in usecs this frame should be displayed during presentation
				 *
				 * @param duration the duration in usecs this frame should be displayed during presentation
				 */
				void SetDuration(long duration);

				/**
				 * Returns the duration in usecs that this frame should be displayed during presentation
				 *
				 * @return the duration in usecs that this frame should be displayed during presentation
				 */
				long GetDuration() const;

				/**
				 * Sets whether the data of this frame is currently loaded within CinePaint
				 *
				 * @param loaded set true to indicate the data is loaded within CinePaint
				 */
				void SetLoaded(bool loaded);

				/**
				 * Returns whether the data of this Frame is currently loaded within CinePaint
				 *
				 * @return true if the data of this frame is currently loaded within CinePaint
				 */
				bool GetLoaded() const;

				/**
				 * Sets whether thw data of this frame has been pre-loaded and is ready for processing
				 * This property is used with a FrameStore to pre-load and prepare the data for
				 * processing.
				 *
				 * @param loaded set true to indicate that the data for this frame has been pre-loaded
				 */
				void SetPreLoaded(bool loaded);

				/**
				 * Returns whether the data of this frame has been pre-loaded
				 *
				 * @return true if the data of this frame has been pre-loaded
				 */
				bool GetPreLoaded() const;

				/**
				 * Sets whether this frame is currently active within the containing FrameManager
				 * An inactive frame will be stepped over while stepping through the frames
				 * within the FrameManager
				 *
				 * @param active set true to set this frame active within the containing FrameManager
				 */
				void SetActive(bool active);

				/**
				 * Returns whether this frame is currently active within the containing FrameManager
				 *
				 * @return true if this frame is currently active within the containing FrameManager
				 */
				bool GetActive() const;


			protected:
			private:

				//--------
				// Members

				/** unique id of this frame */
				std::string m_id;

				/** filename source of this frame */
				std::string m_source;

				/** duration this frame is onscreen during presentation */
				long m_duration;

				/** indicates if the data of this frame is currently loaded within CinePaint */
				bool m_loaded;

				/** indicates if the data of this FrameRecord has been pre-loaded ready for processing */
				bool m_pre_loaded;

				/** indicates if this frame is active during flip/playback */
				bool m_active;
		};

	private:

		typedef std::vector<FrameRecord*> FrameList_t;

	public:
		//-------------------------
		// Constructor / Destructor

		typedef FrameList_t::size_type size_type;

		/**
		 * Constructs a new, initially empty FrameManager
		 *
		 */
		FrameManager();

		/**
		 * Destructor
		 */
		virtual ~FrameManager();





		//-------------------------
		// Frame Addition / Removal

		/**
		 * Adds a FrameRecord to this FrameManager at the specified index
		 * The index parameter controls the position within the frame list of the added frame.
		 * The index if zero offset. If the specified index is greater than the current size 
		 * of this FrameManager, the frame is added to the end.
		 * The data source of the frame is specified by either an Id of the loaed image data
		 * (maintained by the CinePaintDocList) or by the filename of the image to be loaded,
		 * or both.
		 * The duration parameter is currently unused. It is intended to indicate how long the
		 * frame should be displayed onscreen during presentation.
		 *
		 * @param index the index within this FrameManager for the added frame.
		 * @param id the unique id image data within the CinePaintDocList
		 * @param source the frame source/filename
		 * @param duration duration in usecs the frame is onscreen during frame presentation
		 */
		void Add(size_type index, const std::string& id, const std::string& source, long duration);

		/**
		 * Adds a new FrameRecord to the start of this FrameManager
		 *
		 * @param id the unique id image data within the CinePaintDocList
		 * @param source the frame source/filename
		 * @param duration duration in usecs the frame is onscreen during frame presentation
		 */
		void AddFirst(const std::string& id, const std::string& source, long duration);

		/**
		 * Adds a new FrameRecord to the end of this FrameManager
		 *
		 * @param id the unique id image data within the CinePaintDocList
		 * @param source the frame source/filename
		 * @param duration duration in usecs the frame is onscreen during frame presentation
		 */
		void AddLast(const std::string& id, const std::string& source, long duration);

		/**
		 * Removes the specified frame from this FrameManager.
		 * If the frame is not managed within this FrameManager, no action is taken and false is returned
		 *
		 * @param frame the frame to remove
		 * @return true if the frame was removed, false otherwise
		 */
		bool Remove(const FrameRecord& frame);

		/**
		 * Removes the FrameRecord atthe specified index from this FrameManager.
		 * If the index falls ouwith the bounds of this FrameManager, false is returned and no further
		 * action is taken.
		 *
		 * @param index the index of the frame to remove
		 * @return true if the frame was removed, false otherwise
		 */
		bool Remove(size_type index);

		/**
		 * Removes all FrameRecords from this FrameManager
		 *
		 */
		void Clear();

		/**
		 * Moves the position of the specified frame withihn this FrameManager to the spcified index
		 * If the spcified index falls outwith the bounds of this FrameManager, the position of the 
		 * frame is left unchanged.
		 *
		 * @param frame the frame to be moved
		 * @param index the new position of the frame within this FrameManager
		 */
		void SetIndex(const FrameRecord& frame, size_type index);

		/**
		 * Moves the frame at the spcified position to the new position
		 * If either of the spcified indexs fall outwith the bounds of this FrameManager, the position of the 
		 * frame is left unchanged.
		 *
		 * @param curr_index the position of the frame to move
		 * @param new_index the new position of the frame within this FrameManager
		 */
		void SetIndex(size_type curr_index, size_type new_index);


		/**
		 * Returns the FrameRecord at the spcified index
		 * If the specified index falls outwith the bounds of this FrameManager, 0 is returned.
		 *
		 * @param index the index of the frame to returns
		 * @return the FrameRecord at the specified index
		 */
		const FrameRecord* GetFrame(size_type index) const;


		/**
		 * Sets the currently selected frame to the spcified index.
		 * If the spcified index falls outwith the bounds of this FrameManager, the
		 * currently selected frame is left unchanged
		 *
		 * @param index the index frame to set as the currently selected frame
		 */
		bool SetCurrentFrame(size_type index);

		/**
		 * Returns the index of the currently selected frame
		 * If this FrameManager contains no frames, INVALID_INDEX is returned
		 *
		 * @return the index of the currently selected frame within thie FrameManager
		 */
		size_type GetCurrentIndex() const;

		/**
		 * Returns the index within this FrameManager of the spcified FrameRecord
		 * If the spcified FrameRecord is not contained within this FrameManager,
		 * INVALID_INDEX is returned
		 *
		 * @param frame the FrameRecord to obtain the index of
		 * @return the index of the spcified frame
		 */
		size_type GetIndex(const FrameRecord& frame) const;



		/**
		 * Sets whether this FrameManager loops as it reaches the bounds of the frame list
		 * The currently selected FrameRecord may be moved with calls to Next and Previous.
		 * If GetLoop() returns true, as the currently selection reaches the bounds of the
		 * Frame list, it loops back to the start of the list.
		 *
		 * @param loop set true to set looping within this FrameManager
		 */
		void SetLoop(bool loop);

		/**
		 * Returns whether the currently selection loops as it reaches the bounds of this FrameManager
		 *
		 * @return true if this Framemanager loops as it reaches the bounds of the frame list
		 */
		bool GetLoop() const;


		/**
		 * Returns the number of FrameRecords contained within this FrameManager
		 *
		 * @return the number of FrameRecords contained within this FrameManager
		 */
		size_type size() const;

		//-------------
		// step control

		/**
		 * Moves the currently selected FrameRecord to the next active FrameRecord.
		 * If the current selection reaches the end of the frame list, and GetLoop returns true,
		 * the selection loops to the beginning of the list, otherwise INVALID_INDEX is returned.
		 *
		 * @return the currently selected FrameRecord index
		 */
		size_type Next();

		/**
		 * Moves the currently selected FrameRecord to the previous active FrameRecord.
		 * If the current selection reaches the start of the frame list, and GetLoop returns true,
		 * the selection loops to the end of the list, otherwise INVALID_INDEX is returned.
		 *
		 * @return the currently selected FrameRecord index
		 */
		size_type Previous();

		/**
		 * Returns the index of the next active FrameRecord from the spcified index
		 *
		 * @param index the starting index from which to get the index of the next active frame
		 */
		size_type GetNextActiveIndex(size_type index);

		/**
		 * Returns the index of the previous active FrameRecord from the spcified index
		 *
		 * @param index the starting index from which to get the index of the previous active frame
		 */
		size_type GetPreviousActiveIndex(size_type index);




		//-----------------
		// Itterator access

		typedef FrameList_t::const_iterator const_iterator;

		/**
		 * Returns a const_iterator at the begining of this FrameManager
		 *
		 * @return a const_iterator at the begining of this FrameManager
		 */
		const_iterator begin() const;

		/**
		 * Returns a const_iterator past the end of this FrameManager
		 *
		 * @return a const_iterator past the end of this FrameManager
		 */
		const_iterator end() const;

		/**
		 * Returns a const_iterator positioned at the currently selected FrameRecord
		 *
		 * @return a const_iterator at the currently selected frame
		 */
		const_iterator current() const;




		//---------------------
		// FrameRecord Mutators

		void SetFrameLoaded(size_type pos, bool loaded);
		void SetFramePreLoaded(size_type pos, bool loaded);
		void SetFrameActive(size_type pos, bool active);
		void SetFrameId(size_type pos, const char* id);
		void SetFrameSource(size_type pos, const char* source);




		//-------------------
		// Listener handling

		/**
		 * Adds the specified FrameManagerListener to receive Frame Manager change events from this FrameManager
		 * If the specified FrameManagerListener has already been added it is not added again.
		 * The resources of the specified FrameManagerListener are not managed by this class
		 *
		 * @param listener the FrameManagerListener to be added
		 */
		void AddFrameListener(FrameManagerListener* listener);

		/**
		 * Removes and returns the specified FrameManagerListener.
		 * The removed FrameManagerListener will no longer receive Doc List events from this FrameManager
		 * If the specified listener is not present, 0 is returned
		 *
		 * @param listener the FrameManagerListener to be removed
		 */
		FrameManagerListener* RemoveFrameListener(FrameManagerListener* listener);



		//----------------------------
		// CinePaintDoc Loading/Saving

		/**
		 * Sets implementation specific save arguments that are passed to save handling plugin. 
		 * This method must be overridden by subclasses to set implementation specific parameters that
		 * are passed to the save handler plugin.
		 *
		 * @param params paramaeter list to used to pass parameter to the save plugin
		 */
		virtual void SetSaveArgs(CPParamList& params) {};

		/**
		 * UnSets implementation specific save arguments, reclaiming ownership of any implementation specific resources
		 * This method must be overridden by subclasses to unset, or take back ownership of implementation specific
		 * parameters that were set in a call to SetSaveArgs.
		 *
		 * @param params paramaeter list that was used to pass parameter to the save plugin
		 */
		virtual void UnSetSaveArgs(CPParamList& params) {};



		static const size_type INVALID_INDEX;


	protected:
		/**
		 * Dis-allow copy constructor
		 */
		FrameManager(const FrameManager& c) : CinePaintDoc(0, CinePaintDoc::CINEPAINT_FRAMEMANAGER_ENUM) {}

	private:

		void post_selection_changed(size_type index);
		void post_frame_changed(size_type index);

		/** user ordered frame list held by this Framemanager */
		FrameList_t m_frames;

		/** protects access to the frame list */
		mutable pthread_mutex_t m_frames_mutex;

		/** current position within the frame list */
		size_type m_cursor;

		/** indicates if the currently selected frame cursor should loop once it reaches the top/bottom */
		bool m_loop;

		//-------------------
		// Listener Handling
		typedef ListenerList<FrameManagerListener> ListenerList_t;
		ListenerList_t m_listener_list;
};

#endif /* _FRAME_MANAGER_H_ */
