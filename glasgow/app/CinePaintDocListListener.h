/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintDocListListener - Abstract Doc list listener.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****.
 *
 * $Id: CinePaintDocListListener.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CINEPAINT_DOC_LIST_LISTENER_H_
#define _CINEPAINT_DOC_LIST_LISTENER_H_

#include "dll_api.h"

// Forward Declaration
class CinePaintDoc;

/**
 * Abstract Listener for CinePaintDoc events
 * Classes interested in receiving events from a CinePaintDocList should implement this interface
 *
 */
class CINEPAINT_CORE_API CinePaintDocListListener
{
	public:
		/**
		 * Destructor
		 */
		virtual ~CinePaintDocListListener() {} ;

		/**
		 * Invoked when a CinePaintDoc is opened.
		 *
		 * @param doc_id the unique id of the opened document
		 * @param doc the newly opened CinePaintDoc
		 */
		virtual void DocOpened(const char* doc_id, CinePaintDoc& doc) = 0;

		/**
		 * Invoked when a CinePaintDoc is closed.
		 * Clients should not maintain a reference to the doc parameter as the lifetime of
		 * the CinePaintDoc may not be known.
		 *
		 * @param doc_id the unique id of the closed document
		 * @param doc the closed CinePaintDoc
		 */
		virtual void DocClosed(const char* doc_id, CinePaintDoc& doc) = 0;

		/**
		 * Invoked when the current document changes.
		 *
		 * @param doc_id the unique id of the current document
		 * @param doc the newly selected current document.
		 */
		virtual void CurrentDocChanged(const char* doc_id, CinePaintDoc* doc) = 0;

		/**
		 * Invoked when the ID of a managed document is updated.
		 * The ID of a CinePaintDoc is unique for all documents managed by a CinePaintDocList,
		 * allowing different documents of the same file to be referenced without reference
		 * to the CinePaintDoc itself. The CinePaintDoc Id maintained by a CinePaintDocList.
		 *
		 * @param doc the CinePaintDoc whose id has been changed
		 * @param old_id the previous ID of the CinePaintDoc
		 * @param new_id the new ID of the CinePaintDoc
		 */
		//virtual void DocIdChanged(CinePaintDoc& doc, const char* old_id, const char* new_id);

	protected:
		/**
		 * Protected Constructor - disallow instances of an Abstract class
		 */
		CinePaintDocListListener() {} ;

}; /* class CinePaintDocListListener */

#endif /* _CINEPAINT_DOC_LIST_LISTENER_H_ */
