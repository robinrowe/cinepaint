/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ToolProxy - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ToolDocProxy.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "ToolDocProxy.h"
#include "plug-ins/pdb/CinePaintTool.h"
#include "CinePaintApp.h"

ToolDocProxy::ToolDocProxy()
{
	m_selected_tool = CinePaintApp::GetInstance().GetToolManager().GetSelectedTool();
	m_active_doc = CinePaintApp::GetInstance().GetDocList().GetCurrentDoc();
}

/**
 * Destructor
 */
ToolDocProxy::~ToolDocProxy()
{
	
}

/**
 * Invoked when the currently selected tool is changed
 *
 * @param tool_type the type of the newly selected tool
 */
void ToolDocProxy::ToolChanged(const char* tool_type)
{
	// if tool_type is 0, we do not have a valid tool
	if(tool_type)
	{
		m_selected_tool = CinePaintApp::GetInstance().GetToolManager().GetSelectedTool();
		if (m_selected_tool)
			m_selected_tool->SetActiveDocument(m_active_doc);
	}
}

/**
 * Invoked when the current document is changed.
 * @param doc the new active document.
**/
void ToolDocProxy::CurrentDocChanged(const char* doc_id, CinePaintDoc* doc)
{
	m_active_doc = doc;
	if (m_selected_tool)
		m_selected_tool->SetActiveDocument(m_active_doc);
}
