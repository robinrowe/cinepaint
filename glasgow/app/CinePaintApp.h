/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintApp - The main application class.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintApp.h,v 1.5 2006/12/21 11:18:04 robinrowe Exp $
 */

#ifndef _CINEPAINTAPP_H_
#define _CINEPAINTAPP_H_

#include "dll_api.h"
#include <app/CinePaintDocList.h>
#include <app/CinePaintPluginExecutioner.h>
#include <utility/CommandLineParse.h>
#include "plug-ins/pdb/enums.h"
#include "plug-ins/pdb/ImageOpManager.h"
#include "plug-ins/pdb/ListenerList.h"
#include <app/ToolManager.h>
#include "plug-ins/pdb/CinePaintServer.h"
#include "plug-ins/pdb/PDB.h"
#include <app/ColorManager.h>
#include <app/ColorChangeListener.h>
#include <app/BrushList.h>
#include <map>
#include <string>

#include "plug-ins/pdb/CinePaintTag.h"
class CPAppUI;
class CinePaintProgressListener;
class CinePaintDialogListener;
class CPWidgetBuilder;
class FrameManager;

/**
 * Main application class used to manage applicaton level data
 * parse the command line, initalise GUI, etc...
**/
class CINEPAINT_CORE_API CinePaintApp
{	/** the singleton CinePaintApp instance */
	static CinePaintApp* m_app_instance;
	static World world;
	CinePaintPluginExecutioner m_pluginExec;
	BrushList m_brushList;
	ToolManager m_tool_manager;
	ImageOpManager m_image_op_manager;
	/** Applciation Color Selection manager */
	ColorManager m_color_manager;
	/**
	 * Document handling Member and access method.
	 * Maintains a list of Open documents and Current doc 
	 * Each doc is generally an image - but may be other items,
	 * such as a Spreadsheet - or EDL.
	 */
	CinePaintDocList m_DocList;
	CinePaintServer m_Server;
	CPWidgetBuilder *m_widgetBuilder;
	CPAppUI *m_pUI;

	CinePaintApp()
	:	m_Server(&world),
		m_DocList(&world)
	{	m_core_initialize_state = CinePaintApp::INIT_UNINIT_ENUM;
	}

	bool InitializePDB();
	int load_brush_list();
	/**
	 * Thread function for Initaliseation process - ensures all brushes are
	 * loaded, plugins are registered etc. This is now required since GUI needs to be
	 * initalised while other init work is carried out.
	 */
	static void* static_InitMainThreadFunc(void *arg);
	void init_progress_set_text(char* );
	void init_progress_reset(int min, int max, int step);
	void init_progress_update();	
public:
	enum InitState { INIT_UNINIT_ENUM, INIT_FAILED_ENUM, INIT_COMPLETE_ENUM, INIT_PROGRESS_ENUM };
	InitState m_core_initialize_state;
	~CinePaintApp()
	{}
	//============================
	// Application install / Setup
	/**
	 * Initalises and Starts the CinePaintApp Core, initializing and starting core threads as required.
	 * A call to Run returns (almost) immediatly, having started any required worker threads
	 *
	 */
	bool InitInstance(CommandLineParse& clp, CinePaintProgressListener& lst);
	/**
	 * Returns the current initialization state of the CinePaintApp core
	 *
	 * @return current initialization state of the CinePaintApp core
	 */
	InitState GetInitState() const
	{	return(m_core_initialize_state);
	}

	/**
	 * Returns true if the CinePaintApp core is installed
	 * This method simply checks for the existance of the app settings dir in
	 * the users home area
	 *
	 * @return true if the CinePaintApp core is installed for the current user
	 *         false otherwise
	 */
	bool IsInstalled();

	/**
	 * Installs the CinePaintApp core for the current user.
	 * If CinePaint is already installed for the current user, it is simply reinstalled
	 *
	 * @param install_log set to the text from the user install log
	 * @return true if the CinePaintApp core installed successfully, false otherwise
	 */
//	bool UserInstall(std::string& install_log);

	BrushList& GetBrushList()
	{	return(m_brushList);
	}
	ToolManager& GetToolManager()
	{	return(m_tool_manager);
	}
	ImageOpManager& GetImageOpManager()
	{	return(m_image_op_manager);
	}
	/**
	 * Returns the Application Color Selection Manager managing the currently selected color
	 *
	 * @return Application Color selection manager
	 */
	ColorManager& GetColorManager()
	{	return(m_color_manager);
	}
	CinePaintDocList& GetDocList()
	{	return m_DocList;
	}
	/**
	 * Returns the plugin executioner which is used to execute
	 * a plugin.
	 **/
	CinePaintPluginExecutioner& GetPluginExecutioner() { return m_pluginExec;};
	static ListenerList<CinePaintProgressListener>* GetInitList() 
	{	ListenerList<CinePaintProgressListener>* list = ListenerList<CinePaintProgressListener>::GetInitProgressListenerList();
		return list;
	}
	static ListenerList<CinePaintProgressListener>* GetProgressList() 
	{	ListenerList<CinePaintProgressListener>* list = ListenerList<CinePaintProgressListener>::GetProgressListenerList();
		return list;
	}
	static ListenerList<CinePaintDialogListener>* GetDialogList() 
	{	ListenerList<CinePaintDialogListener>* list = ListenerList<CinePaintDialogListener>::GetDialogListenerList();
		return list;
	}
	CPWidgetBuilder &GetUIWidgetBuilder() { return *m_widgetBuilder; };
	CinePaintDoc* CreateCinePaintImage(const CinePaintTag& tag, int width, int height, FillType fill);
	CinePaintDoc* CreateCPDAG();
	CinePaintDoc* CreateFrameManager();
	void RegisterProgressListener(CinePaintProgressListener *lst);
	void RegisterDialogListener(CinePaintDialogListener *lst);
	void RegisterWidgetBuilder(CPWidgetBuilder *bldr) {m_widgetBuilder = bldr;};
	void RemoveInitProgressListener(CinePaintProgressListener *lst);
	static CinePaintApp& GetInstance()
	{	if(!m_app_instance)
		{	m_app_instance = new CinePaintApp();
		}
		return(*m_app_instance) ;
	}
	static World* GetWorld()
	{	return &world;
	}
};

#endif /* _CINEPAINTAPP_H_ */

