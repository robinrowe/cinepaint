/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      BufferRenderer - Abstract base class for rendering.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: BufferRenderer.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _BUFFER_RENDERER_H_
#define _BUFFER_RENDERER_H_

#include "dll_api.h"

// forward declaration
class AbstractBuf;

/**
 * BufferRenderer is an Abstract Base class for all Classes capable of rendering image data into an RGB(A) buffer.
 * BufferRenderer implemenations are intended to render image data contained in various supported data formats into
 * an RGB(A) buffer suitable for onscreen display.
 *
 */
class BufferRenderer
{
	public:
		/**
		 * Destructor
		 */
		virtual ~BufferRenderer() {};


		/**
		 * Renders the specified row of AbstractBuf as RGB data into buf.
		 * The specified row is converted to a displayable RGB format and rendered into buf, suitable
		 * for displaying on screen.
		 * The step parameter may be used to set the step value between successive pixel from canvas,
		 * this allows a canvas to be reduced in size by skipping, and only rendering every step pixels
		 * This method inverts an image mask for display
		 * 
		 * @todo error/bounds checking what do we do if we try to draw past the end of the canvas?
		 * @param buf the data buffer to render the AbstractBuf data into
		 * @param canvas the AbstractBuf to render
		 * @param row the canvas row to render
		 * @param offset an offset from the left of the row to render
		 * @param width the number of pixels to render from the specified row
		 * @param step a step value between successive pixels.
		 */
		virtual void RenderMaskRowRGB(unsigned char* buf, AbstractBuf& canvas, int row, int offset = 0, int width = -1, int step = 1) = 0;

		/**
		 * Renders the specified AbstractBuf as RGB data into buf.
		 * This method inverts an image mask for display
		 *
		 * @param buf the data buffer to render the AbstractBuf into
		 * @param canvas the AbstractBuf to render
		 * @param width the number of pixels to render from the specified row
		 * @param step a step value between successive pixels.
		 */
		virtual void RenderMaskRGB(unsigned char* buf, AbstractBuf& canvas, int width = -1, int step = 1) = 0;

	protected:
		/**
		 * Dis-allow direct instances of this abstract level class
		 */
		BufferRenderer() {};

	private:

}; /* class BufferRenderer */

#endif /* _BUFFER_RENDERER_H_ */
