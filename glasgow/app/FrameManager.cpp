/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      FrameManager - frame manager
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: FrameManager.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "FrameManager.h"
#include "FrameManagerListener.h"

const FrameManager::size_type FrameManager::INVALID_INDEX = FrameManager::size_type(-1);



/**
 * Constructs a new, initially empty FrameManager
 *
 */
FrameManager::FrameManager()
		: CinePaintDoc(0, CinePaintDoc::CINEPAINT_FRAMEMANAGER_ENUM)
{
	m_cursor = FrameManager::INVALID_INDEX;
	m_loop = false;

	if(pthread_mutex_init(&m_frames_mutex, 0) != 0)
	{
		printf("Failed to initialize frames mutex\n");
	}
}

/**
 * Destructor
 */
FrameManager::~FrameManager()
{
	Clear();
	pthread_mutex_destroy(&m_frames_mutex) ;
}





//-------------------------
// Frame Addition / Removal

/**
 * Adds a FrameRecord to this FrameManager at the specified index
 * The index parameter controls the position within the frame list of the added frame.
 * The index if zero offset. If the specified index is greater than the current size 
 * of this FrameManager, the frame is added to the end.
 * The data source of the frame is specified by either an Id of the loaed image data
 * (maintained by the CinePaintDocList) or by the filename of the image to be loaded,
 * or both.
 * The duration parameter is currently unused. It is intended to indicate how long the
 * frame should be displayed onscreen during presentation.
 *
 * @param index the index within this FrameManager for the added frame.
 * @param id the unique id image data within the CinePaintDocList
 * @param source the frame source/filename
 * @param duration duration in usecs the frame is onscreen during frame presentation
 */
void FrameManager::Add(size_type index, const std::string& id, const std::string& source, long duration)
{
	if(index >= m_frames.size())
	{
		AddLast(id, source, duration);
	}
	else
	{
		pthread_mutex_lock(&m_frames_mutex);
		FrameList_t::iterator iter = m_frames.begin();
		iter += index;
		FrameRecord* frame = new FrameRecord(id, source, duration);
		m_frames.insert(iter, frame);
		pthread_mutex_unlock(&m_frames_mutex);
	}
}

/**
 * Adds a new FrameRecord to the start of this FrameManager
 *
 * @param id the unique id image data within the CinePaintDocList
 * @param source the frame source/filename
 * @param duration duration in usecs the frame is onscreen during frame presentation
 */
void FrameManager::AddFirst(const std::string& id, const std::string& source, long duration)
{
	printf("FrameManager: Adding Frame First %s %s %ld\n", id.c_str(), source.c_str(), duration);

	FrameRecord* frame = new FrameRecord(id, source, duration);

	pthread_mutex_lock(&m_frames_mutex);
	m_frames.insert(m_frames.begin(), frame);
	pthread_mutex_unlock(&m_frames_mutex);

	// inform registererd listeners
	for(ListenerList_t::const_iterator citer = m_listener_list.begin(); citer != m_listener_list.end(); ++citer)
	{
		(*citer)->FrameAdded(*frame, 0);
	}
}

/**
 * Adds a new FrameRecord to the end of this FrameManager
 *
 * @param id the unique id image data within the CinePaintDocList
 * @param source the frame source/filename
 * @param duration duration in usecs the frame is onscreen during frame presentation
 */
void FrameManager::AddLast(const std::string& id, const std::string& source, long duration)
{
	FrameRecord* frame = new FrameRecord(id, source, duration);

	pthread_mutex_lock(&m_frames_mutex);
	m_frames.push_back(frame);
	pthread_mutex_unlock(&m_frames_mutex);

	// inform registererd listeners
	for(ListenerList_t::const_iterator citer = m_listener_list.begin(); citer != m_listener_list.end(); ++citer)
	{
		(*citer)->FrameAdded(*frame, m_frames.size() -1);
	}
}

/**
 * Removes the specified frame from this FrameManager.
 * If the frame is not managed within this FrameManager, no action is taken and false is returned
 *
 * @param frame the frame to remove
 * @return true if the frame was removed, false otherwise
 */
bool FrameManager::Remove(const FrameRecord& frame)
{
	bool ret = false;

	pthread_mutex_lock(&m_frames_mutex);
	size_type pos = GetIndex(frame);
	if(pos != FrameManager::INVALID_INDEX)
	{
		Remove(pos);
		ret = true;
	}
	pthread_mutex_unlock(&m_frames_mutex);

	return(ret);
}

/**
 * Removes the FrameRecord atthe specified index from this FrameManager.
 * If the index falls ouwith the bounds of this FrameManager, false is returned and no further
 * action is taken.
 *
 * @param index the index of the frame to remove
 * @return true if the frame was removed, false otherwise
 */
bool FrameManager::Remove(size_type index)
{
	bool ret = false;

	pthread_mutex_lock(&m_frames_mutex);
	if(index < m_frames.size())
	{
		if(index == m_cursor)
		{
			m_cursor = FrameManager::INVALID_INDEX;
		}

		FrameManager::FrameRecord* rec = m_frames.at(index);

		FrameList_t::iterator iter = m_frames.begin();
		iter += index;

		m_frames.erase(iter);
		ret = true;

		for(ListenerList_t::const_iterator citer = m_listener_list.begin(); citer != m_listener_list.end(); ++citer)
		{
			(*citer)->FrameRemoved(*rec, index);
		}

		delete rec;
		rec = 0;
	}
	pthread_mutex_unlock(&m_frames_mutex);

	return(ret);
}

/**
 * Removes all FrameRecords from this FrameManager
 *
 */
void FrameManager::Clear()
{
	pthread_mutex_lock(&m_frames_mutex);
	while(!m_frames.empty())
	{
		FrameRecord* rec = m_frames.back();
		m_frames.pop_back();

		for(ListenerList_t::const_iterator citer = m_listener_list.begin(); citer != m_listener_list.end(); ++citer)
		{
			// size() returns the position of the previously removed item
			// the back element is size() -1, since we've just removed it, we
			// adjust the value, by not subtracting 1
			(*citer)->FrameRemoved(*rec, m_frames.size());
		}

		delete rec;
		rec = 0;
	}

	m_cursor = FrameManager::INVALID_INDEX;

	pthread_mutex_unlock(&m_frames_mutex);
}

/**
 * Moves the position of the specified frame withihn this FrameManager to the spcified index
 * If the spcified index falls outwith the bounds of this FrameManager, the position of the 
 * frame is left unchanged.
 *
 * @param frame the frame to be moved
 * @param index the new position of the frame within this FrameManager
 */
void FrameManager::SetIndex(const FrameManager::FrameRecord& frame, FrameList_t::size_type index)
{
	size_type pos = GetIndex(frame);
	if(pos != FrameManager::INVALID_INDEX)
	{
		SetIndex(pos, index);
	}
}

/**
 * Moves the frame at the spcified position to the new position
 * If either of the spcified indexs fall outwith the bounds of this FrameManager, the position of the 
 * frame is left unchanged.
 *
 * @param curr_index the position of the frame to move
 * @param new_index the new position of the frame within this FrameManager
 */
void FrameManager::SetIndex(size_type curr_index, FrameList_t::size_type new_index)
{
	if((curr_index < m_frames.size()) && (new_index < m_frames.size()))
	{
		pthread_mutex_lock(&m_frames_mutex);
		FrameRecord* temp_rec = m_frames.at(curr_index);
		m_frames.at(curr_index) = m_frames.at(new_index);
		m_frames.at(new_index) = temp_rec;

		pthread_mutex_unlock(&m_frames_mutex);

		// inform registererd listeners
		for(ListenerList_t::const_iterator citer = m_listener_list.begin(); citer != m_listener_list.end(); ++citer)
		{
			(*citer)->FrameIndexChanged(*temp_rec, curr_index, new_index);
		}
	}
}


/**
 * Returns the FrameRecord at the spcified index
 * If the specified index falls outwith the bounds of this FrameManager, 0 is returned.
 *
 * @param index the index of the frame to returns
 * @return the FrameRecord at the specified index
 */
const FrameManager::FrameRecord* FrameManager::GetFrame(FrameList_t::size_type index) const
{
	FrameManager::FrameRecord* f = 0;

	pthread_mutex_lock(&m_frames_mutex);
	if(index < m_frames.size())
	{
		f = m_frames.at(index);
	}
	pthread_mutex_unlock(&m_frames_mutex);

	return(f);
}


/**
 * Sets the currently selected frame to the spcified index.
 * If the spcified index falls outwith the bounds of this FrameManager, the
 * currently selected frame is left unchanged
 *
 * @param index the index frame to set as the currently selected frame
 */
bool FrameManager::SetCurrentFrame(size_type index)
{
	bool ret = false;

	if(index < m_frames.size())
	{
		m_cursor = index;

		post_selection_changed(m_cursor);
		ret = true;
	}

	return(ret);
}

/**
 * Returns the index of the currently selected frame
 * If this FrameManager contains no frames, INVALID_INDEX is returned
 *
 * @return the index of the currently selected frame within thie FrameManager
 */
FrameManager::size_type FrameManager::GetCurrentIndex() const
{
	size_type ret = FrameManager::INVALID_INDEX;

	if(!m_frames.empty())
	{
		ret = m_cursor;
	}

	return(ret);
}

/**
 * Returns the index within this FrameManager of the spcified FrameRecord
 * If the spcified FrameRecord is not contained within this FrameManager,
 * INVALID_INDEX is returned
 *
 * @param frame the FrameRecord to obtain the index of
 * @return the index of the spcified frame
 */
FrameManager::size_type FrameManager::GetIndex(const FrameManager::FrameRecord& frame) const
{
	size_type ret = FrameManager::INVALID_INDEX;

	size_type pos = 0;
	bool done = false;

	FrameList_t::const_iterator citer = m_frames.begin();
	while(!done && (citer != m_frames.end()))
	{
		if(*citer == &frame)
		{
			ret = pos;
			done = true;
		}

		++citer;
		pos++;
	}

	return(ret);
}



/**
 * Sets whether this FrameManager loops as it reaches the bounds of the frame list
 * The currently selected FrameRecord may be moved with calls to Next and Previous.
 * If GetLoop() returns true, as the currently selection reaches the bounds of the
 * Frame list, it loops back to the start of the list.
 *
 * @param loop set true to set looping within this FrameManager
 */
void FrameManager::SetLoop(bool loop)
{
	m_loop = loop;
}

/**
 * Returns whether the currently selection loops as it reaches the bounds of this FrameManager
 *
 * @return true if this Framemanager loops as it reaches the bounds of the frame list
 */
bool FrameManager::GetLoop() const
{
	return(m_loop);
}


/**
 * Returns the number of FrameRecords contained within this FrameManager
 *
 * @return the number of FrameRecords contained within this FrameManager
 */
FrameManager::size_type FrameManager::size() const
{
	return(m_frames.size());
}

//-------------
// step control

/**
 * Moves the currently selected FrameRecord to the next active FrameRecord.
 * If the current selection reaches the end of the frame list, and GetLoop returns true,
 * the selection loops to the beginning of the list, otherwise INVALID_INDEX is returned.
 *
 * @return the currently selected FrameRecord index
 */
FrameManager::size_type FrameManager::Next()
{
	m_cursor = GetNextActiveIndex(m_cursor);
	post_selection_changed(m_cursor);
	return(m_cursor);
}

/**
 * Moves the currently selected FrameRecord to the previous active FrameRecord.
 * If the current selection reaches the start of the frame list, and GetLoop returns true,
 * the selection loops to the end of the list, otherwise INVALID_INDEX is returned.
 *
 * @return the currently selected FrameRecord index
 */
FrameManager::size_type FrameManager::Previous()
{
	m_cursor = GetPreviousActiveIndex(m_cursor);
	post_selection_changed(m_cursor);
	return(m_cursor);
}

/**
 * Returns the index of the next active FrameRecord from the spcified index
 *
 * @param index the starting index from which to get the index of the next active frame
 */
FrameManager::size_type FrameManager::GetNextActiveIndex(size_type index)
{
	size_type ret = FrameManager::INVALID_INDEX;

	if(index < m_frames.size())
	{
		bool done = false;
		size_type loop_count = m_frames.size() -1;
		size_type pos = index;

		while((!done) && (loop_count > 0))
		{
			loop_count--;

			bool got_val = false;

			if((pos +1) < m_frames.size())
			{
				pos++;
				got_val = true;
			}
			else
			{
				if(m_loop && (m_frames.size() > 1))
				{
					pos = 0;
					got_val = true;
				}
				//else got_val = false;
			}

			if(got_val)
			{
				FrameRecord* rec = m_frames.at(pos);
				if(rec->GetActive())
				{
					ret = pos;
					done = true;
				}
			}
		}
	}

	return(ret);
}

/**
 * Returns the index of the previous active FrameRecord from the spcified index
 *
 * @param index the starting index from which to get the index of the previous active frame
 */
FrameManager::size_type FrameManager::GetPreviousActiveIndex(size_type index)
{
	size_type ret = FrameManager::INVALID_INDEX;

	if(index < m_frames.size())
	{
		bool done = false;
		size_type loop_count = m_frames.size() -1;
		size_type pos = index;

		while((!done) && (loop_count > 0))
		{
			loop_count--;

			bool got_val = false;

			if(pos != 0)
			{
				pos--;
				got_val = true;
			}
			else
			{
				if(m_loop && (m_frames.size() > 1))
				{
					pos = m_frames.size() -1;
					got_val = true;
				}
				//else got_val = false;
			}

			if(got_val)
			{
				FrameRecord* rec = m_frames.at(pos);
				if(rec->GetActive())
				{
					ret = pos;
					done = true;
				}
			}
		}
	}

	return(ret);
}




//-----------------
// Iterator access

/**
 * Returns a const_iterator at the begining of this FrameManager
 *
 * @return a const_iterator at the begining of this FrameManager
 */
FrameManager::const_iterator FrameManager::begin() const
{
	return(m_frames.begin());
}


/**
 * Returns a const_iterator past the end of this FrameManager
 *
 * @return a const_iterator past the end of this FrameManager
 */
FrameManager::const_iterator FrameManager::end() const
{
	return(m_frames.end());
}


/**
 * Returns a const_iterator positioned at the currently selected FrameRecord
 *
 * @return a const_iterator at the currently selected frame
 */
FrameManager::const_iterator FrameManager::current() const
{
	FrameManager::const_iterator citer = m_frames.begin();
	citer += m_cursor;
	return(citer);
}




//---------------------
// FrameRecord Mutators

void FrameManager::SetFrameLoaded(size_type pos, bool loaded)
{
	if(pos < m_frames.size())
	{
		FrameRecord* f = m_frames.at(pos);
		if(f)
		{
			f->SetLoaded(loaded);
			post_frame_changed(pos);
		}
	}
}

void FrameManager::SetFramePreLoaded(size_type pos, bool loaded)
{
	if(pos < m_frames.size())
	{
		FrameRecord* f = m_frames.at(pos);
		if(f)
		{
			f->SetPreLoaded(loaded);
			post_frame_changed(pos);
		}
	}
}

void FrameManager::SetFrameActive(size_type pos, bool active)
{
	if(pos < m_frames.size())
	{
		FrameRecord* f = m_frames.at(pos);
		if(f)
		{
			f->SetActive(active);
			post_frame_changed(pos);
		}
	}
}

void FrameManager::SetFrameId(size_type pos, const char* id)
{
	if(pos < m_frames.size())
	{
		FrameRecord* f = m_frames.at(pos);
		if(f)
		{
			std::string s;
			if(id)
			{
				s = std::string(id);
			}
			else
			{
				s.clear();
			}

			f->SetId(s);
			post_frame_changed(pos);
		}
	}
}

void FrameManager::SetFrameSource(size_type pos, const char* source)
{
	if(pos < m_frames.size())
	{
		FrameRecord* f = m_frames.at(pos);
		if(f)
		{
			std::string s;
			if(source)
			{
				s = std::string(source);
			}
			else
			{
				s.clear();
			}

			f->SetSource(s);
			post_frame_changed(pos);
		}
	}
}




//-------------------
// Listener handling

/**
 * Adds the specified FrameManagerListener to receive Frame Manager change events from this FrameManager
 * If the specified FrameManagerListener has already been added it is not added again.
 * The resources of the specified FrameManagerListener are not managed by this class
 *
 * @param listener the FrameManagerListener to be added
 */
void FrameManager::AddFrameListener(FrameManagerListener* listener)
{
	m_listener_list.AddListener(listener);
}

/**
 * Removes and returns the specified FrameManagerListener.
 * The removed FrameManagerListener will no longer receive Doc List events from this FrameManager
 * If the specified listener is not present, 0 is returned
 *
 * @param listener the FrameManagerListener to be removed
 */
FrameManagerListener* FrameManager::RemoveFrameListener(FrameManagerListener* listener)
{
	return(m_listener_list.RemoveListener(listener));
}




void FrameManager::post_selection_changed(size_type index)
{
	FrameRecord* f = 0;

	if(index < m_frames.size())
	{
		f = m_frames.at(index);
	}

	if((index < m_frames.size()) || (index == FrameManager::INVALID_INDEX))
	{
		for(ListenerList_t::const_iterator citer = m_listener_list.begin(); citer != m_listener_list.end(); ++citer)
		{
			(*citer)->SelectionChanged(f, index);
		}
	}
}

void FrameManager::post_frame_changed(size_type index)
{
	if(index < m_frames.size())
	{
		FrameRecord* f = m_frames.at(index);

		if(f)
		{
			for(ListenerList_t::const_iterator citer = m_listener_list.begin(); citer != m_listener_list.end(); ++citer)
			{
				(*citer)->FrameUpdated(*f, index);
			}
		}
	}
}

//---------------
// Nested Classes


/**
 * Constructs a new FrameRecord
 *
 * @param id the id of the data source of this FrameRecord
 * @param source the filename source of this FrameRecord
 * @param duration the duration onscreen of this frame during presentation
 */
FrameManager::FrameRecord::FrameRecord(const std::string& id, const std::string& source, long duration)
		: m_id(id), m_source(source), m_duration(duration), m_loaded(false), m_active(true)
{}

/**
 * Destructor
 */
FrameManager::FrameRecord::~FrameRecord()
{}

/**
 * Sets the Id of the data source of this FrameRecord
 * The Id is the Id as given by a CinePaintDocList which is currently managing
 * the loaded image.
 *
 * @param id the new Id of the data source of this FrameRecord
 */
void FrameManager::FrameRecord::SetId(const std::string& id)
{
	m_id = id;
}

/**
 * Returns the Id of the image data within CinePaint for this frame
 *
 * @return the Id of the image data
 */
const std::string& FrameManager::FrameRecord::GetId() const
{
	return(m_id);
}

/**
 * Sets the filename of the data source within this FrameRecord
 *
 * @param source the new filename
 */
void FrameManager::FrameRecord::SetSource(const std::string& source)
{
	m_source = source;
}

/**
 * Returns the Filename of the data source for this FrameRecord
 *
 * @return the filename of the data source
 */
const std::string& FrameManager::FrameRecord::GetSource() const
{
	return(m_source);
}

/**
 * Sets the duration in usecs this frame should be displayed during presentation
 *
 * @param duration the duration in usecs this frame should be displayed during presentation
 */
void FrameManager::FrameRecord::SetDuration(long duration)
{
	m_duration = duration;
}

/**
 * Returns the duration in usecs that this frame should be displayed during presentation
 *
 * @return the duration in usecs that this frame should be displayed during presentation
 */
long FrameManager::FrameRecord::GetDuration() const
{
	return(m_duration);
}

/**
 * Sets whether the data of this frame is currently loaded within CinePaint
 *
 * @param loaded set true to indicate the data is loaded within CinePaint
 */
void FrameManager::FrameRecord::SetLoaded(bool loaded)
{
	m_loaded = loaded;
}

/**
 * Returns whether the data of this Frame is currently loaded within CinePaint
 *
 * @return true if the data of this frame is currently loaded within CinePaint
 */
bool FrameManager::FrameRecord::GetLoaded() const
{
	return(m_loaded);
}

/**
 * Sets whether thw data of this frame has been pre-loaded and is ready for processing
 * This property is used with a FrameStore to pre-load and prepare the data for
 * processing.
 *
 * @param loaded set true to indicate that the data for this frame has been pre-loaded
 */
void FrameManager::FrameRecord::SetPreLoaded(bool loaded)
{
	m_pre_loaded = loaded;
}

/**
 * Returns whether the data of this frame has been pre-loaded
 *
 * @return true if the data of this frame has been pre-loaded
 */
bool FrameManager::FrameRecord::GetPreLoaded() const
{
	return(m_pre_loaded);
}

/**
 * Sets whether this frame is currently active within the containing FrameManager
 * An inactive frame will be stepped over while stepping through the frames
 * within the FrameManager
 *
 * @param active set true to set this frame active within the containing FrameManager
 */
void FrameManager::FrameRecord::SetActive(bool active)
{
	m_active = active;
}

/**
 * Returns whether this frame is currently active within the containing FrameManager
 *
 * @return true if this frame is currently active within the containing FrameManager
 */
bool FrameManager::FrameRecord::GetActive() const
{
	return(m_active);
}
