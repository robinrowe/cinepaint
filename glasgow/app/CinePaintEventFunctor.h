/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintEventFunctor - Abstract Interface for event functions.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintEventFunctor.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CINEPAINT_EVENT_FUNCTOR_H_
#define _CINEPAINT_EVENT_FUNCTOR_H_

#include "dll_api.h"
#include <app/CinePaintEvent.h>

/**
 * CinePaintEvent provides an abstract interface for an arbitary event to process within an event loop
 * CinePaintEvent should be implemented to provide the specific process functionality required during the
 * process loop
 * A CinePaintEvent should be added to a CinePaintEventList. The managed state of the event indicates to
 * the processing event list that if the event is unmanaged, the resource should be deleted after processing
 * he event. If IsManaged() returns true, the event is managed by an external object and so should not be
 * deleted after processing
 */
template <class T>
class CinePaintEventFunctor0 : public CinePaintEvent
{
	public:
		CinePaintEventFunctor0(T* obj, void (T::*member_fun)(), bool managed);
		virtual ~CinePaintEventFunctor0() {};
		virtual void Process();

	protected:

	private:
		/** object to call member function on */
		T* m_obj;

		/** member function */
		void (T::*m_function_pointer)();

}
; /* class CinePaintEventFunctor0 */


template <class T, class P1>
class CinePaintEventFunctor1 : public CinePaintEvent
{
	public:
		CinePaintEventFunctor1(T* obj, void (T::*member_fun)(P1), P1 param1, bool managed);
		virtual ~CinePaintEventFunctor1() {};
		virtual void Process();

	protected:

	private:
		/** object to call member function on */
		T* m_obj;

		/** member function */
		void (T::*m_function_pointer)(P1);

		P1 m_param1;

}
; /* class CinePaintEventFunctor1 */


template <class T, class P1, class P2>
class CinePaintEventFunctor2 : public CinePaintEvent
{
	public:
		CinePaintEventFunctor2(T* obj, void (T::*member_fun)(P1,P2), P1 param1, P2 param2, bool managed);
		virtual ~CinePaintEventFunctor2() {};
		virtual void Process();

	protected:

	private:
		/** object to call member function on */
		T* m_obj;

		/** member function */
		void (T::*m_function_pointer)(P1,P2);

		P1 m_param1;
		P2 m_param2;

}
; /* class CinePaintEventFunctor2 */



template <class T, class P1, class P2, class P3>
class CinePaintEventFunctor3 : public CinePaintEvent
{
	public:
		CinePaintEventFunctor3(T* obj, void (T::*member_fun)(P1,P2,P3), P1 param1, P2 param2, P3 param3, bool managed);
		virtual ~CinePaintEventFunctor3() {};
		virtual void Process();

	protected:

	private:
		/** object to call member function on */
		T* m_obj;

		/** member function */
		void (T::*m_function_pointer)(P1,P2,P3);

		P1 m_param1;
		P2 m_param2;
		P3 m_param3;

}
; /* class CinePaintEventFunctor3 */





//---------------
// implementation

// CinePaintEventFunctor0

template<class T>
CinePaintEventFunctor0<T>::CinePaintEventFunctor0(T* obj, void (T::*member_fun)(), bool managed)
		: CinePaintEvent(managed)
{
	m_obj = obj;
	m_function_pointer = member_fun;
}

template<class T>
void CinePaintEventFunctor0<T>::Process()
{
	(*m_obj.*m_function_pointer)();
}




// CinePaintEventFunctor1

template<class T, class P1>
CinePaintEventFunctor1<T,P1>::CinePaintEventFunctor1(T* obj, void (T::*member_fun)(P1), P1 param1, bool managed)
		: CinePaintEvent(managed)
{
	m_obj = obj;
	m_function_pointer = member_fun;
	m_param1 = param1;
}

template<class T, class P1>
void CinePaintEventFunctor1<T,P1>::Process()
{
	(*m_obj.*m_function_pointer)(m_param1);
}


// CinePaintEventFunctor2

template<class T, class P1, class P2>
CinePaintEventFunctor2<T,P1,P2>::CinePaintEventFunctor2(T* obj, void (T::*member_fun)(P1,P2), P1 param1, P2 param2, bool managed)
		: CinePaintEvent(managed)
{
	m_obj = obj;
	m_function_pointer = member_fun;
	m_param1 = param1;
	m_param2 = param2;
}

template<class T, class P1, class P2>
void CinePaintEventFunctor2<T,P1,P2>::Process()
{
	(*m_obj.*m_function_pointer)(m_param1, m_param2);
}


// CinePaintEventFunctor3

template<class T, class P1, class P2, class P3>
CinePaintEventFunctor3<T,P1,P2,P3>::CinePaintEventFunctor3(T* obj, void (T::*member_fun)(P1,P2,P3), P1 param1, P2 param2, P3 param3, bool managed)
		: CinePaintEvent(managed)
{
	m_obj = obj;
	m_function_pointer = member_fun;
	m_param1 = param1;
	m_param2 = param2;
	m_param3 = param3;
}

template<class T, class P1, class P2, class P3>
void CinePaintEventFunctor3<T,P1,P2,P3>::Process()
{
	(*m_obj.*m_function_pointer)(m_param1, m_param2, m_param3);
}



#endif /* _CINEPAINT_EVENT_FUNCTOR_H_ */

