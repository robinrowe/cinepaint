/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      FrameStore - FrameManager Image pre-loader
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: FrameStore.cpp,v 1.2 2006/12/21 11:18:04 robinrowe Exp $
 */

#include "FrameStore.h"
#include "plug-ins/pdb/CinePaintImage.h"
#include "plug-ins/pdb/AbstractRenderer.h"
#include "CinePaintApp.h"
#include "CinePaintDocList.h"
#include "FrameManager.h"
#include "FrameStoreListener.h"
#include "plug-ins/pdb/CinePaintRenderOp.h"
#include "plug-ins/pdb/RenderingManager.h"
#include "utility/MathUtil.h"
#include "utility/PlatformUtil.h" // req. for CpSleep

const unsigned long FrameStore::STORE_LOOP_MILLISECS = 100;


const char* FrameStore::FLIP_FRAME_RENDER_NAME = "flip_frame_cell_renderer";


//-------------------------
// Constructor / Destructor

/**
 * Constructs a new FrameManager pre-loading images for the  specified FrameManager
 *
 * @param frame_manager the FrameManager to pre-load images for
 */
FrameStore::FrameStore(FrameManager& frame_manager)
		: m_frame_manager(frame_manager)
{	m_capacity = 4;
	m_pre_load_control = false;
//	m_pre_load_thread = 0;

	m_load_mode = ON_DEMAND_ENUM;


	if(pthread_mutex_init(&m_store_mutex, 0) != 0)
	{
		printf("Failed to initialize store mutex\n");
	}

	if(pthread_cond_init(&m_store_space_cond, 0) != 0)
	{
		printf("Failed to initialize store condition variable\n");
	}

	if(pthread_mutex_init(&m_store_pause_mutex, 0) != 0)
	{
		printf("Failed to initialize store pause mutex\n");
	}

	if(pthread_cond_init(&m_store_pause_cond, 0) != 0)
	{
		printf("Failed to initialize store pause condition variable\n");
	}

	m_look_ahead_pos = frame_manager.GetCurrentIndex();
	m_render_width = 400;
	m_render_height = 400;


	m_pre_load_pause = true;
	m_pre_load_control = true;
	printf("Starting preload thread\n");

	if(pthread_create(&m_pre_load_thread, 0, FrameStore::preload_run, this) != 0)
	{
		printf("FrameStore thread creation Failed!\n");
		m_pre_load_control = false ;
	}
}


/**
 * Dectructor
 */
FrameStore::~FrameStore()
{
	if(m_pre_load_control)
	{
		m_pre_load_control = false;

		if(m_pre_load_pause)
		{
			pthread_mutex_lock(&m_store_pause_mutex) ;
			pthread_cond_signal(&m_store_pause_cond) ;
			pthread_mutex_unlock(&m_store_pause_mutex) ;
		}
		else
		{
			pthread_mutex_lock(&m_store_mutex) ;
			pthread_cond_signal(&m_store_space_cond) ;
			pthread_mutex_unlock(&m_store_mutex) ;
		}

		// wait for thread to complete
		printf("Waiting for FrameStore thread to complete...");
		if(pthread_join(m_pre_load_thread, 0) != 0)
		{
			printf("Error joining on FrameStore thread\n");
		}
		else
		{
			printf("OK\n");
		}
	}

	pthread_mutex_destroy(&m_store_mutex) ;
	pthread_cond_destroy(&m_store_space_cond) ;
	pthread_mutex_destroy(&m_store_pause_mutex) ;
	pthread_cond_destroy(&m_store_pause_cond) ;
}



//--------------------
// Pre-loading Control

/**
 * Starts this FrameStore pre-loading images.
 *
 */
void FrameStore::StartPreLoad()
{
	pthread_mutex_lock(&m_store_pause_mutex) ;
	m_pre_load_pause = false;
	pthread_cond_signal(&m_store_pause_cond) ;
	pthread_mutex_unlock(&m_store_pause_mutex);

	// give the thread a kick if it is waiting for space to become available
	pthread_mutex_lock(&m_store_mutex) ;
	pthread_cond_signal(&m_store_space_cond) ;
	pthread_mutex_unlock(&m_store_mutex) ;
}

/**
 * Stops this FrameStore pre-loading images
 *
 */
void FrameStore::StopPreLoad()
{
	pthread_mutex_lock(&m_store_pause_mutex) ;
	m_pre_load_pause = true;
	pthread_cond_signal(&m_store_pause_cond) ;
	pthread_mutex_unlock(&m_store_pause_mutex) ;

	// give the thread a kick if it is waiting for space to become available
	pthread_mutex_lock(&m_store_mutex) ;
	pthread_cond_signal(&m_store_space_cond) ;
	pthread_mutex_unlock(&m_store_mutex) ;
}

/**
 * Returns whether this FrameStore is currently pre-loading images.
 *
 * @return true if this FrameStore is currently pre-loading images, false otherwise
 */
bool FrameStore::IsPreLoading()
{
	return(m_pre_load_control && !m_pre_load_pause);
}




//---------------------
// aceessors / mutators


/**
 * Returns the CinePaintImage specified within FrameManager::FrameRecord.
 * The operation of this method is dependant upon the mode of operation of this FrameStore,
 * If the returned image is not 0, the caller must assume the responsibility for the
 * CinePaintImage reference, releasing the reference witin the CinePaintDocList when conplete. 
 *
 * @param frame the FrameManager::FrameRecord indicating the image source
 * @see SetLoadMode
 */
CinePaintImage* FrameStore::GetFrame(const FrameManager::FrameRecord& frame)
{
	CinePaintImage* image = 0;

	pthread_mutex_lock(&m_store_mutex) ;

	switch(m_load_mode)
	{
		case ON_DEMAND_ENUM:
		{
			FrameItem* item = remove_from_store(frame);
			if(item)
			{
				image = item->m_image;

				delete item;
				item = 0;
			}
			else
			{
				image = get_image(frame);
				if(image)
				{
					create_render(*image);
				}
			}

			break;
		}
		case PRE_LOAD_ENUM:
		{
			FrameItem* item = remove_from_store(frame);

			if(item)
			{
				image = item->m_image;

				delete item;
				item = 0;
			}
			break;
		}
		default:
		{
			break;
		}
	}

	// signal that we may have space for further items to be loaded
	if(GetStoreSize() < GetStoreCapacity())
	{
		pthread_cond_signal(&m_store_space_cond) ;
	}

	pthread_mutex_unlock(&m_store_mutex) ;

	return(image);
}

/**
 * Sets the mode this FrameStore will be used in
 * The FrameStore can be used in two modes, on demans or pre-load. When in on-demand
 * mode, a call to GetFrame will return the image corresponding to the specified frame,
 * if the image has not been pre-loaded, it is loaded. When in pre-load mode, a thread
 * runs continually pre-loading images, to the set capacity of this FrameStore. A call
 * to GetFrame will return the pre-loaded image is pre-loaded, or 0 if not.
 *
 * @param mode the mode of operation of this FrameStore
 */
void FrameStore::SetLoadMode(StoreLoadMode mode)
{
	m_load_mode = mode;

	if((m_load_mode == ON_DEMAND_ENUM))
	{
		StopPreLoad();
	}
}

/**
 * Returns the current mode of operation for this FrameStore
 *
 * @return the current mode of operation for this FrameStore
 */
FrameStore::StoreLoadMode FrameStore::GetLoadBehaviour() const
{
	return(m_load_mode);
}






/**
 * Returns the current number of item contained within this FrameStore
 *
 * @return the number of items currently contained within thie FrameStore
 */
FrameStore::size_type FrameStore::GetStoreSize() const
{
	return(m_store.size());
}

/**
 * Returns the capacity of this FrameStore.
 * The capacity of this FrameStore indicates how many images this FrameStore can pre-load.
 *
 * @return the capacity of this FrameStore
 */
FrameStore::size_type FrameStore::GetStoreCapacity() const
{
	return(m_capacity);
}

/**
 * Sets the capacity of this FrameStore to the specified value.
 *
 * @param capacity the new capacity for this FrameStore
 */
void FrameStore::SetCapacity(FrameStore::size_type capacity)
{
	m_capacity = capacity;
}

/**
 * Returns true if this FrameStore is buffered,
 * This FrameStore is buffered when the size of this FrameStore reaches its capacity,
 * or there are no more items to be loaded.
 *
 * @return true if this FrameStore is buffered
 */
bool FrameStore::IsBuffered() const
{
	return((GetStoreCapacity() == GetStoreSize()) ||
	       ((GetStoreSize() != 0) && !has_look_ahead(m_look_ahead_pos)));
}

/**
 * Clears this FrameStore of loaded images.
 * All references to loaded images are released,
 *
 */
void FrameStore::ClearBuffer()
{
	if(!IsPreLoading())
	{
		FrameStore_t::iterator iter = m_store.begin();
		while(iter != m_store.end())
		{
			FrameStore_t::iterator next = iter;
			++next;

			FrameStore::FrameItem* item = *iter;
			m_store.erase(iter);

			if(item->m_image)
			{
				CinePaintApp::GetInstance().GetDocList().UnRefDocument(*(item->m_image));
				for(ListenerList_t::const_iterator citer = m_listener_list.begin(); citer != m_listener_list.end(); ++citer)
				{
					(*citer)->FrameRemoved(*(item->m_frame), *(item->m_image));
				}
			}

			iter = next;
		}
	}

	// signal that we may have space for further items to be loaded
	if(GetStoreSize() < GetStoreCapacity())
	{
		pthread_cond_signal(&m_store_space_cond) ;
	}
}




/**
 * Returns the current position within the FrameManaget this FrameStore is reading from
 *
 * @return the current reading position within the FrameManager
 */
FrameManager::size_type FrameStore::GetStoreReadPos() const
{
	return(m_look_ahead_pos);
}

/**
 * Sets the current reading position within the FrameManager.
 * The next item added to this FrameStore during pre-loading will be read from
 * the specified position within the FrameManager
 *
 * @param pos the next position to read during pre-loading
 */
void FrameStore::SetStoreReadPos(FrameManager::size_type pos)
{
	m_look_ahead_pos = pos;
}


/**
 * Sets the size of the rendereor created for each loaded image.
 *
 * @param width the width of the renderor
 * @param height the height of the renderor
 */
void FrameStore::SetRenderSize(int width, int height)
{
	m_render_width = width;
	m_render_height = height;
}

/**
 * Returns the size of the rendereor being created for each loaded image.
 *
 * @param width set to the width of the renderor
 * @param height set to the height of the renderor
 */
void FrameStore::GetRenderSize(int& width, int& height) const
{
	width = m_render_width;
	height = m_render_height;
}


//-------------------
// Listener handling

/**
 * Adds the specified FrameStoreListener to receive FrameStore events from this FrameStore
 * If the specified FrameStoreListener has already been added it is not added again.
 * The resources of the specified FrameStoreListener are not managed by this class
 *
 * @param listener the FrameStoreListener to be added
 */
void FrameStore::AddStoreListener(FrameStoreListener* listener)
{
	m_listener_list.AddListener(listener);
}

/**
 * Removes and returns the specified FrameStoreListener.
 * The removed FrameStoreListener will no longer receive Doc List events from this FrameStore
 * If the specified listener is not present, 0 is returned
 *
 * @param listener the FrameStoreListener to be removed
 */
FrameStoreListener* FrameStore::RemoveStoreListener(FrameStoreListener* listener)
{
	return(m_listener_list.RemoveListener(listener));
}




//-------------------------------------
// FrameManagerListener implementations

void FrameStore::SelectionChanged(const FrameManager::FrameRecord* frame, FrameManager::size_type position)
{
	// ignored
}

void FrameStore::FrameAdded(const FrameManager::FrameRecord& frame, FrameManager::size_type position)
{
	// @todo invalidate FrameManager iterator
}

void FrameStore::FrameRemoved(const FrameManager::FrameRecord& frame, FrameManager::size_type position)
{
	// @todo invalidate FrameManager iterator
}

void FrameStore::FrameUpdated(const FrameManager::FrameRecord& frame, FrameManager::size_type position)
{
	// @todo check loaded image sources
}

void FrameStore::FrameIndexChanged(const FrameManager::FrameRecord& frame, FrameManager::size_type old_pos, FrameManager::size_type new_pos)
{
	// @todo invalidate FrameManager iterator
}










/**
 * Gets the image as specified within the FrameManager::FrameRecord
 * If the frame record indicates an image Id, the CinePaintDocList is first
 * queried for the names image. If the CinePaintDocList indicates the document
 * is already loaded, the image is referenced and returned. If the CinePaintDocList
 * does not manage the document of the specified Id, or no Id is given by the frame
 * record, the source of the image is used, and the CinePaintDocList is used to load
 * and reference the image.
 *
 * @return the loaded image
 */
CinePaintImage* FrameStore::get_image(const FrameManager::FrameRecord& frame)
{
	CinePaintImage* img = 0;

	CinePaintDocList& doc_list = CinePaintApp::GetInstance().GetDocList();

	if(!frame.GetId().empty())
	{
		CinePaintDoc* doc = doc_list.GetDocById(frame.GetId().c_str());
		if(doc && doc->GetDocType() == CinePaintDoc::CINEPAINT_IMAGE_ENUM)
		{
			img = dynamic_cast<CinePaintImage*>(doc);
		}
	}

	if(!img && !frame.GetSource().empty())
	{
		CinePaintDoc* doc = doc_list.OpenDocument(frame.GetSource().c_str());

		if(doc && doc->GetDocType() == CinePaintDoc::CINEPAINT_IMAGE_ENUM)
		{
			img = dynamic_cast<CinePaintImage*>(doc);
		}
	}

	if(img)
	{
		CinePaintApp::GetInstance().GetDocList().RefDocument(*img);
	}

	return(img);
}

/**
 * Removes and returns the FrameItem from this FrameStore created for the spcified frame record
 * If no item has been loaded for the specified FrameRecord, 0 is returned,
 * The caller must assume responsibility for the returned FrameItem, and any referenced image
 * it may contain
 *
 * @param frame the FrameRecord for which the FrameItem was constructed
 * @return the FrameItem corresponding to the specified FrameRecord
 */
FrameStore::FrameItem* FrameStore::remove_from_store(const FrameManager::FrameRecord& frame)
{
	FrameItem* item = 0;

	FrameStore_t::iterator iter = m_store.begin();
	bool done = false;

	while(!done && iter != m_store.end())
	{
		if((*iter)->m_frame == &frame)
		{
			item = *iter;
			m_store.erase(iter);
			done = true;

			for(ListenerList_t::const_iterator citer = m_listener_list.begin(); citer != m_listener_list.end(); ++citer)
			{
				(*citer)->FrameRemoved(*(item->m_frame), *(item->m_image));
			}
		}
		else
		{
			++iter;
		}

	}

	return(item);
}

/**
 * Constructs a renderor for the spcified CinePaintImage if it does not already have one.
 * The renderor is constructed for display on screen at the size as given by GetRenderSize and
 * added to the inages RenderingManager using the renderor name FrameStore::FLIP_FRAME_RENDER_NAME
 *
 * @todo If the image already contains the renderor, we need to check the size etc.
 * @param image the CinePaintImage for which to create a renderor
 */
void FrameStore::create_render(CinePaintImage& image)
{
	AbstractRenderer* renderer = image.GetRenderingManager().GetRenderer(FLIP_FRAME_RENDER_NAME);

	if(!renderer)
	{
		CinePaintRenderOp* op = CinePaintApp::GetInstance().GetImageOpManager().GetRenderOp(image.GetTag());
		if(op)
		{
			// calc the scaled instance
			int scale_w = image.GetWidth();
			int scale_h = image.GetHeight();
			MathUtil::calc_scale_to_fit_cell(scale_w, scale_h, m_render_width, m_render_height);

			renderer = op->CreateScaledScreenRendererInstance(image, scale_w, scale_h);

			if(renderer)
			{
				image.GetRenderingManager().RegisterRenderer(FLIP_FRAME_RENDER_NAME, renderer);
				renderer->Render();
			}
			else
			{
				printf("Error: Cannot instantiate a Renderer to render Image Data\n");
			}
		}
		else
		{
			// no render installed
			printf("Error: No RenderOp Installed, or Format/Precision not supported\n");
		}
	}
}

/**
 * Returns trus if the specified read position can be moved onto a next valid frame within the FrameManager
 * If the FrameManager is set non-looping and the current read position has reached the end, or there
 * are no more active frames, this method will return false
 *
 * @param pos the read position to test
 * @return true if there is a next position for the value to be moved to
 */
bool FrameStore::has_look_ahead(size_t pos) const
{
	return(m_frame_manager.GetNextActiveIndex(pos) != FrameManager::INVALID_INDEX);
}

/**
 * increments the look ahead read position
 *
 */
void FrameStore::increment_look_ahead()
{
	m_look_ahead_pos = m_frame_manager.GetNextActiveIndex(m_look_ahead_pos);
}






/**
 * Pre-load thread functionl run from the m_pre_load_thread
 * Loops until m_pre_load_control is set false, loading/getting images and adding them to
 * this FrameStore,
 *
 * @param arg instance of this FrameStire
 */
void* FrameStore::preload_run(void* arg)
{
	FrameStore* instance = reinterpret_cast<FrameStore*>(arg);

	while(instance->m_pre_load_control)
	{
		pthread_mutex_lock(&instance->m_store_mutex) ;

		if((instance->GetStoreSize() < instance->GetStoreCapacity()) &&
		        instance->has_look_ahead(instance->m_look_ahead_pos))
		{
			const FrameManager::FrameRecord* frame = instance->m_frame_manager.GetFrame(instance->m_look_ahead_pos);

			if(frame)
			{
				FrameItem* fi = new FrameItem();

				fi->m_frame = frame;
				fi->m_image = instance->get_image(*frame);

				if(fi->m_image)
				{
					instance->create_render(*(fi->m_image));

					for(ListenerList_t::const_iterator citer = instance->m_listener_list.begin(); citer != instance->m_listener_list.end(); ++citer)
					{
						(*citer)->FrameAdded(*frame, *(fi->m_image));
					}
				}

				instance->m_store.push_back(fi);

				instance->increment_look_ahead();
			}
		}
		else if(instance->GetStoreSize() >= instance->GetStoreCapacity())
		{
			while(!instance->m_pre_load_pause && instance->m_pre_load_control && instance->GetStoreSize() >= instance->GetStoreCapacity())
			{
				pthread_cond_wait(&instance->m_store_space_cond, &instance->m_store_mutex);
			}
		}
		else if(!instance->has_look_ahead(instance->m_look_ahead_pos))
		{
			while(!instance->m_pre_load_pause && instance->m_pre_load_control && instance->m_look_ahead_pos == instance->m_frame_manager.size())
			{
				pthread_cond_wait(&instance->m_store_space_cond, &instance->m_store_mutex) ;
			}
		}

		pthread_mutex_unlock(&instance->m_store_mutex) ;

		if(instance->m_pre_load_pause)
		{
			while(instance->m_pre_load_control && instance->m_pre_load_pause)
			{
				pthread_mutex_lock(&instance->m_store_pause_mutex);
				pthread_cond_wait(&instance->m_store_pause_cond, &instance->m_store_pause_mutex);
				pthread_mutex_unlock(&instance->m_store_pause_mutex);
			}
		}
		else
		{
			//usleep(FrameStore::STORE_LOOP_USECS);
			PlatformUtil::CpSleep(FrameStore::STORE_LOOP_MILLISECS);
		}
	}

	printf("exiting FrameStore thread\n");

	//pthread_exit(0) ;
	return(0) ;
}

