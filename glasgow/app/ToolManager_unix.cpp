/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Tool Plugin Loading
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ToolManager_unix.cpp,v 1.3 2008/01/28 18:22:28 robinrowe Exp $
 */
#ifndef WIN32

#include <ToolManager.h>

#include <CinePaintApp.h>
#include <plug-ins/CinePaintTool.h>

//#include <utility/AppRC.h>
#include <AppSettings.h>

#include <dirent.h>
#include <sys/types.h>

#include <dlfcn.h>

static const char* TOOL_PATH_SUFFIX = "tools";

/**
 * Initializes, and loads available CinePaintTool plugins from an application specified path
 *
 */
void ToolManager::Initialize()
{
	// The directory from which to load the main tools.
	char _tool_dir[PATH_MAX + 1];
	char* _tmp_dir;


	const char* _full_path=GetPathToolsPlugin(); 
	

	// open the module
	void* _tool_lib = dlopen(_full_path, RTLD_LAZY);

	// Pointer to the create tool functions
	CinePaintTool::PFNCreateCinePaintTool* _ToolCtor = 0;
	CinePaintTool::PFNGetToolClassList _ToolListFunc = 0;

	const char* _dlErrMsg = 0;

	if(_tool_lib)
	{
		// keep the opened handle so we can clean up properly at shutdown
		m_open_plugins.push_back(_tool_lib);

		_ToolListFunc = (CinePaintTool::PFNGetToolClassList)dlsym(_tool_lib, "GetToolClassList");

		if(_ToolListFunc)
		{
			_ToolCtor = _ToolListFunc();
			while(*_ToolCtor)
			{
				// Construct and keep a pointer to the 'Core Tool'
				// These plugins are 'core' to the application, so we do not create and destroy on-the-fly,
				// we always want these to be available. If the construction method return non-zero, we
				// register the tool with this ToolManager for future use. The ToolManager assumes the reponsiblity
				// of the resource from this point onwards.

				CinePaintTool* _tool = (*_ToolCtor)();

				if(_tool)
				{
					//snprintf(_info_txt, sizeof(_info_txt), "Registering Tool: %s", _tool->GetToolType());
					//init_progress_set_text(_info_txt);

					RegisterTool(_tool);

					//init_progress_update();
				}

				++_ToolCtor;
			}
		}
		else
		{
			_dlErrMsg = dlerror();
			printf("Failed to load Tool Plugin, %s, No entry point not Found\nError is: %s\n", _full_path, _dlErrMsg);
		}
	}
	else
	{
		_dlErrMsg = dlerror();
		printf("Failed to open Tool Plugin: %s\nError is: %s\n", _full_path, _dlErrMsg);
	}
/*
	if(_plugin_full_path)
	{
		strncpy(_tool_dir, _plugin_full_path, strlen(_plugin_full_path) +1);
	}
	else
	{
	        const char *tool_path_prefix = "";
		printf("Warning: No Tool Plugin path defined, trying \"%s/%s/\" instead\n",
		  tool_path_prefix, TOOL_PATH_SUFFIX);
		strncpy(_tool_dir, tool_path_prefix, strlen(tool_path_prefix) +1);
		if(strncmp(_tool_dir + strlen(_tool_dir) -1, "/", 1) != 0)
		{
			strcat(_tool_dir, "/");
		}
		strcat(_tool_dir, TOOL_PATH_SUFFIX);
	}

	char _full_path[PATH_MAX + 1];
	char _info_txt[PATH_MAX + 1];

	// Pointer to the create tool functions
	CinePaintTool::PFNCreateCinePaintTool* _ToolCtor = 0;
	CinePaintTool::PFNGetToolClassList _ToolListFunc = 0;

	const char* _dlErrMsg = 0;

	struct dirent* _item;
	DIR* _dir;
	if((_dir = opendir(_tool_dir)) != 0)
	{
		//int _tool_count = 0;
		//init_progress_reset(0, _tool_count, 1);

		while((_item = readdir(_dir)) != 0)
		{
			// Tool Plugins have a '.so' extension and are not dotfiles
			// there is no requirement for a 'lib' prefix, which is probably only confusing anyways
			// but if is not refused either
			if((strlen(_item->d_name) > 3) && (strncmp(_item->d_name, ".", 1) != 0) &&
			        (strncmp(_item->d_name + strlen(_item->d_name) - 3, ".so", 3) == 0))
			{
				snprintf(_full_path, sizeof(_full_path), "%s/%s", _tool_dir, _item->d_name);
				snprintf(_info_txt, sizeof(_info_txt), "Loading Tool: %s", _item->d_name);
				//init_progress_set_text(_info_txt);

				// open the module
				void* _tool_lib = dlopen(_full_path, RTLD_LAZY);

				if(_tool_lib)
				{
					// keep the opened handle so we can clean up properly at shutdown
					m_open_plugins.push_back(_tool_lib);

					_ToolListFunc = (CinePaintTool::PFNGetToolClassList)dlsym(_tool_lib, "GetToolClassList");

					if(_ToolListFunc)
					{
						_ToolCtor = _ToolListFunc();
						while(*_ToolCtor)
						{
							// Construct and keep a pointer to the 'Core Tool'
							// These plugins are 'core' to the application, so we do not create and destroy on-the-fly,
							// we always want these to be available. If the construction method return non-zero, we
							// register the tool with this ToolManager for future use. The ToolManager assumes the reponsiblity
							// of the resource from this point onwards.

							CinePaintTool* _tool = (*_ToolCtor)();

							if(_tool)
							{
								//snprintf(_info_txt, sizeof(_info_txt), "Registering Tool: %s", _tool->GetToolType());
								//init_progress_set_text(_info_txt);

								RegisterTool(_tool);

								//init_progress_update();
							}

							++_ToolCtor;
						}
					}
					else
					{
						_dlErrMsg = dlerror();
						printf("Failed to load Tool Plugin, %s, No entry point not Found\nError is: %s\n", _full_path, _dlErrMsg);
					}
				}
				else
				{
					_dlErrMsg = dlerror();
					printf("Failed to open Tool Plugin: %s\nError is: %s\n", _full_path, _dlErrMsg);
				}
			}
		}

		closedir(_dir);
	}
	else
	{
		printf("Could not open Tool Dir: %s\n", _tool_dir);
	}
*/
}

void ToolManager::ShutDown()
{
	std::list<void*>::iterator iter = m_open_plugins.begin();

	while(iter != m_open_plugins.end())
	{
		dlclose(*iter);

		m_open_plugins.erase(iter);
	}
}

#endif
