/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      FlipFrameDriver - FrameManager timer
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: FlipFrameDriver.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _FLIPFRAME_DRIVER_H_
#define _FLIPFRAME_DRIVER_H_

#include "dll_api.h"
#include <pthread.h>

class FrameManager;
class FlipFrameController;

/**
 * FlipFrameDriver provides a simple interface for driving the periodic updating of a FrameManager.
 *
 */
class CINEPAINT_CORE_API FlipFrameDriver
{
	public:

		/**
		 * Destructor
		 */
		virtual ~FlipFrameDriver();


		//--------------------------
		// Flip Frame Driver Control

		/**
		 * Starts this FlipFrameDriver driving the FrameManager
		 *
		 */
		virtual void Start() = 0;

		/**
		 * Stops this FlipFrameDriver from driving the FrameManager
		 *
		 */
		virtual void Stop() = 0;

		/**
		 * Returns the running state of this FlipFrameDriver
		 *
		 * @return true if this FlipFrameDriver is currently running, false otherwise
		 */
		virtual bool IsRunning() const = 0;

	protected:
		/**
		 * Constructs a new FlipFrameDriver to drive a FrameManager
		 *
		 * @param frame_manager the FrameManager being driven
		 */
		FlipFrameDriver(FrameManager& frame_manager);

		/**
		 * Returns the FrameManager this FlipFrameDriver is driving
		 *
		 * @return the FrameManager this FlipFrameDriver is driving
		 */
		FrameManager& GetFrameManager();

	private:

		//--------
		// Members

		/** frame manager we drive */
		FrameManager& m_frame_manager;


}; /* class FlipFrameDriver */


/**
 * SimpleFlipFrameDriver provides a simple means of automatically progressing the current selection within a FrameManager
 * the SimpleFlipFrameDriver uses a simple timed delay to periodically update the specified FrameManager,
 * once started, a thread within this SimpleFlipFrameDriver will repeatedly update the FrameManager, then
 * go to sleep for a set period. The FrameManager is updated using either the Framemanager methods,
 * Next, or Previous depending upon the current direction within the FlipFrameController.
 * If a call to Next, or Previous upon the FrameManager, returns INVALID_INDEX, this FlipFrameDriver will call
 * Stop upon the FlipFrameController, halting this FlipFrameDriver.
 */
class CINEPAINT_CORE_API SimpleFlipFrameDriver : public FlipFrameDriver
{
	public:
		/**
		 * Constructs a new SimpleFlipFrameDriver to drive a FrameManager
		 *
		 * @param frame_manager the FrameManager being driven
		 * @param controller the FlipFrameController controlling this driver
		 */
		SimpleFlipFrameDriver(FrameManager& frame_manager, FlipFrameController& controller);

		/**
		 * Destructor
		 */
		virtual ~SimpleFlipFrameDriver();


		//--------------------------
		// Flip Frame Driver Control

		/**
		 * Starts this FlipFrameDriver driving the FrameManager
		 *
		 */
		virtual void Start();

		/**
		 * Stops this FlipFrameDriver from driving the FrameManager
		 *
		 */
		virtual void Stop();

		/**
		 * Returns the running state of this FlipFrameDriver
		 *
		 * @return true if this FlipFrameDriver is currently running, false otherwise
		 */
		virtual bool IsRunning() const;



		//--------------------------
		// Delay / Direction Control

		/**
		 * Gets the current delay between updates to the FrameManager
		 *
		 * @return the current delay in Milliseconds
		 */
		unsigned long GetDelay() const;

		/**
		 * Sets the current delay between updates to the FrameManager
		 *
		 * @param millis the delay between FrameManager updates in Milliseconds
		 */
		void SetDelay(unsigned long usec);

	protected:

	private:

		/**
		 * Thread run function
		 *
		 * @param arg instance of the running FlipFrameDriver
		 */
		static void* run(void* arg);

		//--------
		// Members

		/** flip frame controller */
		FlipFrameController& m_controller;

		/** driver run thread */
		pthread_t m_run_thread;

		/** indicates if this driver is running */
		bool m_control_flag;

		/** delay in Milliseconds between updates to the FrameManager */
		unsigned long m_delay;

		/** controls pauseing of the driver thread */
		bool m_driver_pause;

		/** protects acces ot the driver pause flag */
		pthread_mutex_t m_driver_pause_mutex;

		/** signal to the driver thread a change in pause state */
		pthread_cond_t m_driver_pause_cond;

}; /* class SimpleFlipoFrameDriver */

#endif /* _FLIPFRAME_DRIVER_H_ */
