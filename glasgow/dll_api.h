// dll_api.h
// Copyright 2006 Robin.Rowe@MovieEditor.com
// License: BSD

/* rsr:
   This is a standard hack to accommodate Windows because it needs functions
   explicitly exported to work with dll linkage. 
   
   The Glasgow prototype had a dozen different versions of this file instead 
   of using my dll_api.h from Film Gimp. Why? Don't know. I've replaced all 
   that clutter with this one file. 
   
   To do. Make everywhere DLL_API instead of the various Glasgow *_API names.
   
*/

/* rsr: 
   Turning off WARNING 4251 stops VC++ 7 from complaining that it can't
   possibly export stl stuff cleanly. There's a KB article that explains you
   can't export the stl because it has hidden internal classes. This is just 
   another reason not to use the STL! Yuck. 11/26/05
*/

#include <unistd.h>

#ifndef DLL_API_H

// rsr: This would have been simpler...
#if 0
#ifdef WIN32)
#	ifdef _USRDLL
#		define DLL_API __declspec( dllexport )
#	else
#		define DLL_API __declspec( dllimport )
#	endif
#pragma warning( disable: 4275 )
#else
#	define DLL_API
#endif
#endif

#ifdef DLL_EXPORTS
//#pragma message( "DLL: DLL_EXPORTS" )
#define COLOR_EXPORTS
#define CORE_EXPORTS
#define CORE_INTERFACES_EXPORTS
#define GUI_EXPORTS
#define IMAGE_EXPORTS
#define IMAGE_TOOLS_EXPORTS
#define PAINTING_EXPORTS
#define TOOLS_EXPORTS
#define UTIL_EXPORTS
#endif

#ifdef PLUGIN_EXPORTS
//#pragma message( "DLL: PLUGIN_EXPORTS" )
#define BLUR_EXPORTS
#define JPEG_EXPORTS
#define PNG_EXPORTS
#define TEST_CLIENT_EXPORTS
#define TIFF_EXPORTS
#endif

#ifdef _USRDLL
#	ifdef UTIL_EXPORTS
//#pragma message( "dllexport: CINEPAINT_UTIL_API" )
#		define CINEPAINT_UTIL_API __declspec( dllexport )
#	else
#		define CINEPAINT_UTIL_API __declspec( dllimport )
#	endif
#else
#	define CINEPAINT_UTIL_API
#endif

// plug-ins.dll as lib:
#ifdef _USRDLL_NOT
//#ifdef _USRDLL
#	ifdef CORE_EXPORTS
//#pragma message( "dllexport: CINEPAINT_CORE_API" )
#		define CINEPAINT_CORE_API __declspec( dllexport )
#	else
#		define CINEPAINT_CORE_API __declspec( dllimport )
#	endif
#	ifdef CORE_INTERFACES_EXPORTS
//#pragma message( "dllexport: CORE_INTERFACES_EXPORTS" )
#		define CINEPAINT_CORE_INTERFACES_API __declspec( dllexport )
#	else
#		define CINEPAINT_CORE_INTERFACES_API __declspec( dllimport )
#	endif
#pragma warning( disable: 4251 )
#else
#	define CINEPAINT_CORE_API
#	define CINEPAINT_CORE_INTERFACES_API
#endif

#ifdef _USRDLL
#	ifdef GUI_EXPORTS
//#pragma message( "dllexport: CINEPAINT_GUI_API" )
#		define CINEPAINT_GUI_API __declspec( dllexport )
#	else
#		define CINEPAINT_GUI_API __declspec( dllimport )
#	endif
#pragma warning( disable: 4275 )
#else
#	define CINEPAINT_GUI_API
#endif

#ifdef _USRDLL
#	ifdef IMAGE_TOOLS_EXPORTS
//#pragma message( "dllexport: IMAGE_CINEPAINT_OPS_API" )
#		define IMAGE_CINEPAINT_OPS_API __declspec( dllexport )
#	else
#		define IMAGE_CINEPAINT_OPS_API __declspec( dllimport )
#	endif
#else
#	define IMAGE_CINEPAINT_OPS_API
#endif

#ifdef _USRDLL
#	ifdef TOOLS_EXPORTS
//#pragma message( "dllexport: CINEPAINT_TOOLS_API" )
#		define CINEPAINT_TOOLS_API __declspec( dllexport )
#	else
#		define CINEPAINT_TOOLS_API __declspec( dllimport )
#	endif
#else
#	define CINEPAINT_TOOLS_API
#endif

#ifdef _USRDLL
#	ifdef COLOR_EXPORTS
//#pragma message( "dllexport: CINEPAINT_COLOR_API" )
#		define CINEPAINT_COLOR_API __declspec( dllexport )
#	else
#		define CINEPAINT_COLOR_API __declspec( dllimport )
#	endif
#pragma warning( disable: 4251 )
#else
#	define CINEPAINT_COLOR_API
#endif

#ifdef _USRDLL
#	ifdef PAINTING_EXPORTS
//#pragma message( "dllexport: CINEPAINT_PAINTING_API" )
#		define CINEPAINT_PAINTING_API __declspec( dllexport )
#	else
#		define CINEPAINT_PAINTING_API __declspec( dllimport )
#	endif
#pragma warning( disable: 4251 )
#else
#	define CINEPAINT_PAINTING_API
#endif

// plug-ins.dll as lib:
#ifdef _USRDLL_NOT
//#ifdef _USRDLL
#	ifdef IMAGE_EXPORTS
//#pragma message( "dllexport: CINEPAINT_IMAGE_API" )
#		define CINEPAINT_IMAGE_API __declspec( dllexport )
#	else
#		define CINEPAINT_IMAGE_API __declspec( dllimport )
#	endif
#else
#	define CINEPAINT_IMAGE_API
#endif

// Plug-ins

#ifdef _USRDLL
#	ifdef BLUR_EXPORTS
//#pragma message( "dllexport: CINEPAINT_BLUR_API" )
#		define CINEPAINT_BLUR_API __declspec( dllexport )
#	else
#		define CINEPAINT_BLUR_API __declspec( dllimport )
#	endif
#else
#	define CINEPAINT_BLUR_API
#endif

#ifdef _USRDLL
#	ifdef JPEG_EXPORTS
//#pragma message( "dllexport: CINEPAINT_JPEG_API" )
#		define CINEPAINT_JPEG_API __declspec( dllexport )
#	else
#		define CINEPAINT_JPEG_API __declspec( dllimport )
#	endif
#else
#	define CINEPAINT_JPEG_API
#endif

#ifdef _USRDLL
#	ifdef PNG_EXPORTS
//#pragma message( "dllexport: CINEPAINT_PNG_API" )
#		define CINEPAINT_PNG_API __declspec( dllexport )
#	else
#		define CINEPAINT_PNG_API __declspec( dllimport )
#	endif
#else
#	define CINEPAINT_PNG_API
#endif

#ifdef _USRDLL
#	ifdef TIFF_EXPORTS
//#pragma message( "dllexport: CINEPAINT_TIFF_API" )
#		define CINEPAINT_TIFF_API __declspec( dllexport )
#	else
#		define CINEPAINT_TIFF_API __declspec( dllimport )
#	endif
#else
#	define CINEPAINT_TIFF_API
#endif

#ifdef _USRDLL
#	ifdef TEST_CLIENT_EXPORTS
//#pragma message( "dllexport: CINEPAINT_TEST_CLIENT_API" )
#		define CINEPAINT_TEST_CLIENT_API __declspec( dllexport )
#	else
#		define CINEPAINT_TEST_CLIENT_API __declspec( dllimport )
#	endif
#else
#	define CINEPAINT_TEST_CLIENT_API
#endif


#endif 

