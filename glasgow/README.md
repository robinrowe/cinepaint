# README: CinePaint Glasgow
Robin.Rowe@cinepaint.org
2018.1.26, 2006.12.15
www.cinepaint.org

Note: Nothing has been cleaned up yet for civilians. CinePaint is full of old misleading notes. Ignore them all.

## What is CinePaint?

CinePaint is an OSS HDR alternative to Adobe Photoshop or Corel Paint. CinePaint Glasgow is a rewrite of the core for CinePaint 2.0. CinePaint FilmGimp 1.0 is GTK. CinePaint Glasgow 2.0 is FLTK. The 1.0 build system is broken, so don't bother with that.

## To build Glasgow

git clone...
cd cinepaint/glasgow
mkdir build
cd build
cmake ..

Any cmake should do. Outputs a sln file for VS2017. Should open and manipulate JPEG and PNG files for starters. 

## CinePaint History

CinePaint is a painting and retouching tool primarily used for motion 
picture frame-by-frame retouching and dust-busting. It was used on THE 
LAST SAMURAI, HARRY POTTER and many other films. CinePaint is different 
from other painting tools because it supports deep color depth image 
formats up to 32 bits per channel deep and has a RAM-based flipbook
movie player to aid retouching between frames.

CinePaint was at one time based on Film Gimp, a project to add deep
paint capabilities that was forked by the GIMP project. Glasgow, the
codename for this version of CinePaint, is a new codebase not based
on Film Gimp. Both CinePaint Film Gimp and CinePaint Glasgow will be
around for a while until Glasgow stabilizes. CinePaint Glasgow is 
designed to work natively on Windows, Linux, and Mac OS X. 

-00-