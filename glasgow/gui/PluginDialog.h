/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      PluginDialog - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: PluginDialog.h,v 1.2 2006/12/18 08:21:17 robinrowe Exp $
 */

#ifndef _PLUGIN_DIALOG_H_
#define _PLUGIN_DIALOG_H_

#include "dll_api.h"
#include "plug-ins/pdb/CinePaintDialogListener.h"
#include "app/CinePaintEvent.h"
#include "plug-ins/pdb/CPPluginUIDialog.h"
#include <list>

#undef CreateDialog

// Declaration
class Fl_Window;

/**
 * This class manages a Progress Dialog box.
 * It is also thread safe - it checks whether it is running in the same thread as the 
 * main UI event loop - if so all Updates and windows creation is done directly,
 * otherwise it posts a message into the event queue.
 * 
**/
class CINEPAINT_GUI_API PluginDialog
{
	
public:
	PluginDialog(char* ID);
	~PluginDialog();

	/**
	 * Create the progress dialog.
	 * With given text and progress bar size.
	**/
	void CreatePluginDialog(CPPluginUIDialog *dlg);

	/**
	 * Run the dialog in Modal 
	**/
	DialogResponse  DoModal();

	/**
	 * Hide the dilaog.
	 **/
	void Hide();

	/**
	 * Get this dialogs ID.
	 * @return The ID string for this dialog
	**/
	const char* GetID() {return m_ID;};
private:

	CPPluginUIDialog *m_dlg;
	DialogResponse m_response; 

	// Unique Identifier.
	char m_ID[50];
	// Whether we are in teh main UI thread or not
	bool m_main_thread;

private:

	void create_plugin_dialog(CPPluginUIDialog *dlg);
	DialogResponse do_modal();
	void hide();

	Fl_Window *build_dialog();

public:
	//===========================
	// DialogListener for this Dialog.
	class DialogListener : public CinePaintDialogListener
	{
	public:
		DialogListener();
		~DialogListener();

		virtual void CreateDialog(char* ID, CPPluginUIDialog *dlg );

		virtual DialogResponse DoModal( char* ID );

	private:
		std::list<PluginDialog *> m_dlgList;
		PluginDialog *find_dlg(char* ID);

	};

	//==========================
	// Events to post into event loop.
	class NewPluginDialogEvent : public CinePaintEvent
	{
	public:
		NewPluginDialogEvent( PluginDialog *dlg, CPPluginUIDialog *piDlg );
		~NewPluginDialogEvent() {};
		void Process();

	private:
		PluginDialog *m_dlg;
		CPPluginUIDialog *m_piDlg;
	};

	class PluginDialogDoModalEvent : public CinePaintEvent
	{
	public:
		PluginDialogDoModalEvent( PluginDialog *dlg );
		~PluginDialogDoModalEvent() {};
		void Process();

	private:
		PluginDialog *m_dlg;
	};
};

#endif // _PLUGIN_DIALOG_H_
