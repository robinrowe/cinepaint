/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ImageInfoWindow - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ImageInfoWindow.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CINEPAINT_IMAGE_INFO_WINDOW_H_
#define _CINEPAINT_IMAGE_INFO_WINDOW_H_

#include "dll_api.h"
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>
#include <plug-ins/pdb/CinePaintMouseListener.h>

// Forward Declarations
class CINEPAINT_GUI_API Fl_Window;
class CinePaintImage;
class CinePaintDrawArea;

/**
 * Small information window to display the current mouse position and image details under that position.
 * The ImageInfoWindow monitors events from a set CinePaintDrawArea to track the current mouse position
 * and gather sinformation from the specified image for the current position.
 *
 */
class CINEPAINT_GUI_API ImageInfoWindow : public Fl_Window, public CinePaintMouseListener
{
	public:
		/**
		 * Constructs a new ImageInfoWindow with the specified size and title
		 *
		 * @param w the window width
		 * @param h the window height
		 * @param title the window title
		 */
		ImageInfoWindow(int w, int h, const char* title);

		/**
		 * Destructor
		 */
		virtual ~ImageInfoWindow();

		/**
		 * Sets the CinePaintImage And CinePaintDrawArea this ImageInfoWindow provides information from.
		 * If both are set to 0, this ImageInfoWindow no longer tracks and display image info.
		 * 
		 * @param image the CinePaintImage we this ImageInfoWindow display information from
		 * @param draw_area the CinePaintDrawArea this ImageInfoWindow monitors for mouse movement
		 */
		void SetImage(CinePaintImage* image, CinePaintDrawArea* draw_area);


		//-------------------------------
		// CinePaintMouseListener  methods

		virtual void MouseEntered();
		virtual void MouseExited();
		virtual void MousePressed(int button, int count, int x, int y, unsigned int modifier);
		virtual void MouseReleased(int button, int x, int y);
		virtual void MouseDragged(int button, int x, int y);
		virtual void MouseMoved(int x, int y);

	protected:

	private:

		/** the the draw area we monitor for mouse movement */
		CinePaintDrawArea* m_drawarea;

		// @note [claw] do we need this or should everything be available from CinepaintDrawArea?
		/** the CinePaintImage we display information from */
		CinePaintImage* m_image;


		//----------------------------
		// Information display widgets

		/** image dimension */
		Fl_Box* m_dimension_label;

		/** current scale zoom settings */
		Fl_Box* m_scale_label;

		/** current mouse position */
		Fl_Box* m_position_label;

		/** pixel valus under the current mouse position */
		Fl_Box* m_value_label;


		//-------------------------------------
		// text buffers for current information

		/** image dimension */
		char* m_dimension_buf;

		/** current scale zoom settings */
		char* m_scale_buf;

		/** current mouse position */
		char* m_position_buf;

		/** pixel valus under the current mouse position */
		char* m_value_buf;


		/**
		 * Update the image dimension display from the set CinepaintImage data
		 *
		 */
		void update_dimensions();

		/**
		 * Updates the current position display with the specified values
		 *
		 * @param x the x position
		 * @param y the y position
		 */
		void update_position(int x, int y);

		/**
		 * Updates the current image value at the specified location
		 *
		 * @param x the x position
		 * @param y the y position
		 */
		void update_image_value(int x, int y);

}; /* class ImageInfoWindow */

#endif /* _CINEPAINT_IMAGE_INFO_WINDOW_H_ */
