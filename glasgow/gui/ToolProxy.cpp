/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ToolProxy - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ToolProxy.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "gui/ToolProxy.h"
#include "plug-ins/pdb/CinePaintTool.h"
#include <app/CinePaintApp.h>

ToolProxy::ToolProxy()
{
	m_selected_tool = CinePaintApp::GetInstance().GetToolManager().GetSelectedTool();
}

/**
 * Destructor
 */
ToolProxy::~ToolProxy()
{
	
}

/**
 * Invoked when the currently selected tool is changed
 *
 * @param tool_type the type of the newly selected tool
 */
void ToolProxy::ToolChanged(const char* tool_type)
{
	// if tool_type is 0, we do not have a valid tool
	if(tool_type)
	{
		m_selected_tool = CinePaintApp::GetInstance().GetToolManager().GetSelectedTool();
	}
}


/**
 * Invoked when the mouse pointer moves into an active area
 *
 */
void ToolProxy::MouseEntered()
{
	if(m_selected_tool)
	{
		m_selected_tool->MouseEntered();
	}
}

/**
 * Invoked when the mouse pointer leaves an active area
 *
 */
void ToolProxy::MouseExited()
{
	if(m_selected_tool)
	{
		m_selected_tool->MouseExited();
	}
}

/**
 * Invoked when a mouse button is pressed.
 *
 * @param button the button that was pressed
 * @param count the number of button clicks
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void ToolProxy::MousePressed(int button, int count, int x, int y, unsigned int modifier)
{
	if(m_selected_tool)
	{
		m_selected_tool->MousePressed(button, count, x, y, modifier);
	}
}

/**
 * Invoked when a mouse button is released
 *
 * @param button the mouse button released
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void ToolProxy::MouseReleased(int button, int x, int y)
{
	if(m_selected_tool)
	{
		m_selected_tool->MouseReleased(button, x, y);
	}
}

/**
 * Invoked when the mouse pointer is dragged with a mouse button held pressed
 *
 * @param button the held mouse button
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void ToolProxy::MouseDragged(int button, int x, int y)
{
	if(m_selected_tool)
	{
		m_selected_tool->MouseDragged(button, x, y);
	}
}

/**
 * Invoked when the mouse pointer is moved within an active area
 *
 * @param x the current mouse position
 * @param y the current mouse position
 */
void ToolProxy::MouseMoved(int x, int y)
{
	if(m_selected_tool)
	{
		m_selected_tool->MouseMoved(x, y);
	}
}

