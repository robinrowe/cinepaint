/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintImageWindow - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: SimpleDisplayWindow.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _SIMPLE_DISPLAY_WINDOW_H_
#define _SIMPLE_DISPLAY_WINDOW_H_

#include <FL/Fl_Double_Window.H>

#include <plug-ins/pdb/ListenerList.h>
#include <utility/CPRect.h>

class AbstractBuf;
class CPRect;
class CinePaintWidgetListener;

class SimpleDisplayWindow : public Fl_Double_Window
{
	public:
		SimpleDisplayWindow(int w, int h, const char* title);
		virtual ~SimpleDisplayWindow();

		void SetBuffer(const AbstractBuf* buf);


		//----------------------------
		// Overriden FL_Widget methods

		/**
		 * Resizes and position this Widget to the specified values.
		 * This implementation calls FL_Widget::resize but also informs all registered
		 * CinePaintWidgetListerns of the change.
		 *
		 * @param x the x location
		 * @param y the y location
		 * @param w the widget width
		 * @param h the widget height
		 */
		virtual void resize(int x, int y, int w, int h);

		/**
		 * Sets the size of thie widget to the specified size.
		 * This methods is a convenience method for resize, hiding FL_Widget::size inorder to
		 * provide notifications of widget events to registered CinePaintWidgetListeners
		 *
		 * @param w the widget width
		 * @param h the widget height
		 */
		void size(short w, short h);

		/**
		 * Adds the specified CinePaintWidgetListener to receive widget events from this CinePaintDrawArea
		 *
		 * @param cwl the CinePaintWidgetListener to be added
		 */
		void AddWidgetListener(CinePaintWidgetListener* cwl);

		/**
		 * Removes and returns the specified CinePaintWidgetListener.
		 *
		 * @param cwl the CinePaintWidgetListener to be removed
		 */
		CinePaintWidgetListener* RemoveWidgetListener(CinePaintWidgetListener* cwl);

	protected:

		void draw();

	private:
		/** the buffer to display */
		const AbstractBuf* m_buf;

		/** region of interest - the area that gets redrawn */
		CPRect m_roi;

		typedef ListenerList<CinePaintWidgetListener> WidgetListenerList_t;
		WidgetListenerList_t m_widget_listeners;
};

#endif /* _SIMPLE_DISPLAY_WINDOW_H_ */
