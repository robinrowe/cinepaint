/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      DocViewManager - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: DocViewManager.cpp,v 1.2 2006/12/21 11:18:06 robinrowe Exp $
 */

#include "DocViewManager.h"
#include "CPDAGDoc.h"
#include "CinePaintAppUI.h"
#include "CinePaintImageWindow.h"
#include "UIUtils.h"
#include "FrameManagerWindow.h"
#include "SheetViewWindow.h"
#include <plug-ins/pdb/CinePaintImage.h>
#include <plug-ins/pdb/AbstractRenderer.h>
#include <plug-ins/pdb/RenderingManager.h>
#include <app/CinePaintApp.h>
#include <plug-ins/pdb/CinePaintDoc.h>
#include <app/FrameManager.h>
#include <plug-ins/pdb/CinePaintRenderOp.h>
#include <FL/fl_ask.H>

/**
 * Constructs an intially empty DocViewManager
 */
DocViewManager::DocViewManager()
{}

/**
 * Destructor
 */
DocViewManager::~DocViewManager()
{
	// remove and destroy all our view, we dont own the CinePaintDoc, so it is not destroyed
	DocViewContainer_t::iterator _iter = m_doc_views.begin();

	while(_iter != m_doc_views.end())
	{
		DocViewContainer_t::iterator _next = _iter;
		++_next;

		Fl_Window* _win = (*_iter).second;

		m_doc_views.erase(_iter);

		if(_win->shown())
		{
			_win->hide();
		}

		delete _win;
		_win = 0;


		_iter = _next;
	}
}



/**
 * Opens and registers a new view for the specified CinePaintDoc.
 * Multiple views may be opened for a particular view.
 * Currently this implementation only handles CinePaintImage and the associated
 * CinePaintImageWindows view.
 * The reference count for the CinePaintDoc is incremented by one within the CinePaintDocList
 *
 * @param doc the CinePaintDoc to open the view for
 */
void DocViewManager::OpenView(CinePaintDoc& doc)
{
	switch(doc.GetDocType())
	{
		case CinePaintDoc::CINEPAINT_IMAGE_ENUM:
		{
			// dynamic cast just to be sure ...
			CinePaintImage* _img = dynamic_cast<CinePaintImage*>(&doc);

			if(_img)
			{
				open_image_window(_img);
			}
			break;
		}
		case CinePaintDoc::CINEPAINT_DAG_ENUM:
		{
			CPDAGDoc* _dag = dynamic_cast<CPDAGDoc*>(&doc);

			if(_dag)
			{
				open_sheet_view_window(_dag);
			}
			break;
		}
		case CinePaintDoc::CINEPAINT_FRAMEMANAGER_ENUM:
		{
			FrameManager* _fm = dynamic_cast<FrameManager*>(&doc);

			if(_fm)
			{
				open_frame_manager_view(_fm);
			}
			break;
		}
		default:
		{
			// unknown doc type
			break;
		}
	}
}

/**
 * Closes and unregisters the specified view of the specified CinePaintDoc.
 * Currently this implementation only handles CinePaintImage and the associated
 * CinePaintImageWindows view.
 * If the view is the last remaining reference of the Document within the CinePaintDoc
 * the document will be closed. If the Document has been modified, the user is prompted
 * if they wish to save the document
 *
 * @param doc the CinePaintDoc to open the view for
 * @param view the view of the CinePaintDoc
 */
void DocViewManager::CloseView(CinePaintDoc& doc, Fl_Window& view)
{
	if(doc.GetDocType() == CinePaintDoc::CINEPAINT_IMAGE_ENUM)
	{
		// dynamic cast just to be sure ...
		CinePaintImageWindow* _win = dynamic_cast<CinePaintImageWindow*>(&view);

		if(_win)
		{
			close_view(_win);
		}
	}
}




// CinePaintDocListListener methods

/**
 * Invoked when a CinePaintDoc is opened.
 * If the CinePaintDoc is a supported type a view for the document is created and displayed.
 * Currently this implementation only supports a CinePaintImage
 *
 * @param doc_id the unique id of the opened document
 * @param doc the newly opened CinePaintDoc
 */
void DocViewManager::DocOpened(const char* doc_id, CinePaintDoc& doc)
{
	// dont automatically open new view for the document.
}

/**
 * Invoked when a CinePaintDoc is closed.
 * Closes any open views over the specified CinePaintDoc
 *
 * @param doc_id the unique id of the closed document
 * @param doc the closed CinePaintDoc
 */
void DocViewManager::DocClosed(const char* doc_id, CinePaintDoc& doc)
{
	// close all the views registered against the specified CinePaintDoc

	DocViewContainer_t::iterator _low = m_doc_views.lower_bound(&doc);
	DocViewContainer_t::iterator _high = m_doc_views.upper_bound(&doc);

	while(_low != _high)
	{
		DocViewContainer_t::iterator _next = _low;
		++_next;
		
		switch(doc.GetDocType())
		{
			case CinePaintDoc::CINEPAINT_IMAGE_ENUM:
			case CinePaintDoc::CINEPAINT_DAG_ENUM:
			{
				Fl_Window* _win = _low->second;
				if(_win->shown())
				{
					_win->hide();
				}

				Fl::delete_widget(_win);
				_win = 0;
				m_doc_views.erase(_low);
				break;
			}
			default:
			{
				break;
			}
		}

		_low = _next;
	}	
}






//------------------------
// CinePaintImage Handling

/**
 * Creates a display window for the specified image and displays it.
 * The new window is registered as a view over the specified CinePaintImage
 *
 * @param image the CinePaintImage to display
 */
void DocViewManager::open_image_window(CinePaintImage* image)
{
	// get the default renderer to render the image
	AbstractRenderer* _renderer = image->GetRenderingManager().getDefaultRenderer();

	// no default renderer already in use, attempt to create a new instance
	if(!_renderer)
	{
		CinePaintRenderOp* _op = CinePaintApp::GetInstance().GetImageOpManager().GetRenderOp(image->GetTag());
		if(_op)
		{
			_renderer = _op->CreateDefaultRendererInstance(*image);

			if(_renderer)
			{
				// install a default renderer
				image->GetRenderingManager().SetDefaultRenderer(_renderer);
			}
			else
			{
				fl_alert("Cannot create image window.\nNo image renderer is available");
				printf("Error: Cannot instantiate a Renderer to render Image Data\n");
			}
		}
		else
		{
			// no render installed
			fl_alert("Cannot create image window.\nFormat/Precision is not supported, or no renderers are installed");
			printf("Error: No RenderOp Installed, or Format/Precision not supported\n");
		}
	}

	if(_renderer)
	{
		image->GetRenderingManager().SetDefaultRenderer(_renderer);

		CinePaintImageWindow* _win = new CinePaintImageWindow(400, 400, image->GetFileName());
		_win->callback(static_image_window_close_cb, this);

		// @note not sure whether this referencing belongs here. or in the image with an associated unref
		// in the image destructor, or both ...
		CinePaintApp::GetInstance().GetDocList().RefDocument(*image);

		_win->SetImage(image, _renderer);
		_win->show();

		// add the window to our managed views
		CinePaintApp::GetInstance().GetDocList().RefDocument(*image);
		m_doc_views.insert(DocViewPair_t(image, _win));
	}
}

/**
 * Unregisters and Closes the specified CinePaintImageWindow.
 * The window is unregisters as a view over its CinePaintImage. If the view was the
 * last remaining open view, a request is made to close the CinePaintImage. If the
 * Document was indeed closed, this DocViewmanager will receice notification and delete
 * the view, otherwise the view is left open. If the view is not the last open view, it
 * is simply uinregistered, closed, and deleted
 *
 * @param window the window being closed
 */
void DocViewManager::close_view(CinePaintImageWindow* window)
{
	if(window)
	{
		CinePaintImage* _key = window->GetImage();

		// if this is that reference ot the image, we may need to save the image
		bool _close_doc = true;
		if(CinePaintApp::GetInstance().GetDocList().GetRefCount(*_key) == 1)
		{
			_close_doc = CinePaintAppUI::GetInstance().ReleaseAndSaveDocument(*_key);
		}

		if(_close_doc)
		{
			std::pair<DocViewContainer_t::iterator, DocViewContainer_t::iterator> _range = m_doc_views.equal_range(_key);
			bool _done = false;

			while(!_done && (_range.first != _range.second))
			{
				DocViewContainer_t::iterator _next = _range.first;
				++_next;

				if(_range.first->second == window)
				{
					m_doc_views.erase(_range.first);
					window->hide();

					Fl::delete_widget(window);
					window = 0;

					CinePaintApp::GetInstance().GetDocList().UnRefDocument(*_key);

					// there can only be one window instance registered for a document//
					// so we can stop the while loop
					_done = true;
				}

				_range.first = _next;
			}
		}
	}
}


/**
 * static callback on closing a window
 * This callback expects the window and the widget parameter and the DocViewManager as the additional data.
 * calls close_image_window on the DocViewmanager instance
 *
 * @param window the window being closed
 * @param data the DocViewmanager
 */
void DocViewManager::static_image_window_close_cb(Fl_Widget* window, void* data)
{
	reinterpret_cast<DocViewManager*>(data)->close_view(reinterpret_cast<CinePaintImageWindow*>(window));
}






//-------------------
// CPDAGDoc Handling

/**
 * Creates and displays a sheet view window to display the DAG
 * The new window is registerd as a view over the specified CPDAGDoc
 *
 * @param sheet the CPDAGDoc to display
 */
void DocViewManager::open_sheet_view_window(CPDAGDoc* sheet)
{
	if(sheet)
	{
		SheetViewWindow* _win = new SheetViewWindow(600, 400,0);

		_win->callback(static_sheet_view_window_close_cb, this);
		_win->SetDAGDoc(*sheet);
		_win->show();

		// add the window to our managed views
		CinePaintApp::GetInstance().GetDocList().RefDocument(*sheet);
		m_doc_views.insert(DocViewPair_t(sheet, _win));
	}
}

/**
 * Unregisters and closes the specified SheetViewWindow
 * The windows is unregistered as a view of the CPDAGDoc. If the view held the last
 * remaining reference to the CPDAGDoc, and the DAG has been modified, the user is queried
 * to save the document.
 *
 * @param sheet the view to close
 */
void DocViewManager::close_view(SheetViewWindow* sheet)
{
	if(sheet)
	{
		CPDAGDoc* _key = sheet->GetDAGDoc();

		// if this is that reference ot the image, we may need to save the image
		bool _close_doc = true;
		if(CinePaintApp::GetInstance().GetDocList().GetRefCount(*_key) == 1)
		{
			_close_doc = CinePaintAppUI::GetInstance().ReleaseAndSaveDocument(*_key);
		}

		if(_close_doc)
		{
			std::pair<DocViewContainer_t::iterator, DocViewContainer_t::iterator> _range = m_doc_views.equal_range(_key);
			bool _done = false;

			while(!_done && (_range.first != _range.second))
			{
				DocViewContainer_t::iterator _next = _range.first;
				++_next;

				if(_range.first->second == sheet)
				{
					m_doc_views.erase(_range.first);
					sheet->hide();

					Fl::delete_widget(sheet);
					sheet = 0;

					CinePaintApp::GetInstance().GetDocList().UnRefDocument(*_key);

					// there can only be one window instance registered for a document
					// so we can stop the while loop
					_done = true;
				}

				_range.first = _next;
			}
		}
	}
}

/**
 * static callback on closing a SheetViewWindow
 * This callback expects the window and the widget parameter and the DocViewManager as the additional data.
 * Calls close_view on the DocViewmanager instance
 *
 * @param window the window being closed
 * @param data the DocViewmanager
 */
void DocViewManager::static_sheet_view_window_close_cb(Fl_Widget* window, void* data)
{
	reinterpret_cast<DocViewManager*>(data)->close_view(reinterpret_cast<SheetViewWindow*>(window));
}


//----------------------
// FrameManager handling

/**
 * Creates and displays a new Frame Manager view window to display a FrameManager
 * The new window is registerd as a view over the specified FrameManager
 *
 * @param fm the FrameManager to display
 */
void DocViewManager::open_frame_manager_view(FrameManager* fm)
{
	if(fm)
	{
		FrameManagerWindow* _win = new FrameManagerWindow(400, 400, "Frame Manager", *fm);

		_win->callback(static_frame_manager_window_close_cb, this);
		_win->show();

		// add the window to our managed views
		CinePaintApp::GetInstance().GetDocList().RefDocument(*fm);
		m_doc_views.insert(DocViewPair_t(fm, _win));
	}
}

/**
 * Unregisters and closes the specified FrameManagerWindow
 * The windows is unregistered as a view of the FrameManager. If the view held the last
 * remaining reference to the FrameManager, and the FrameManager has been modified,
 * the user is queried to save the document.
 *
 * @param fmw the view to close
 */
void DocViewManager::close_view(FrameManagerWindow* fmw)
{
	if(fmw)
	{
		FrameManager* _key = &fmw->GetFrameManager();

		bool _close_doc = true;
		if(CinePaintApp::GetInstance().GetDocList().GetRefCount(*_key) == 1)
		{
			_close_doc = CinePaintAppUI::GetInstance().ReleaseAndSaveDocument(*_key);
		}

		if(_close_doc)
		{
			std::pair<DocViewContainer_t::iterator, DocViewContainer_t::iterator> _range = m_doc_views.equal_range(_key);
			bool _done = false;

			while(!_done && (_range.first != _range.second))
			{
				DocViewContainer_t::iterator _next = _range.first;
				++_next;

				if(_range.first->second == fmw)
				{
					m_doc_views.erase(_range.first);
					fmw->hide();

					Fl::delete_widget(fmw);
					fmw = 0;

					CinePaintApp::GetInstance().GetDocList().UnRefDocument(*_key);

					// there can only be one window instance registered for a document
					// so we can stop the while loop
					_done = true;
				}

				_range.first = _next;
			}
		}
	}
}

/**
 * static callback on closing a FrameManagerWindow
 * This callback expects the window and the widget parameter and the DocViewManager as the additional data.
 * Calls close_view on the DocViewmanager instance
 *
 * @param window the window being closed
 * @param data the DocViewmanager
 */
void DocViewManager::static_frame_manager_window_close_cb(Fl_Widget* window, void* data)
{
	reinterpret_cast<DocViewManager*>(data)->close_view(reinterpret_cast<FrameManagerWindow*>(window));
}

