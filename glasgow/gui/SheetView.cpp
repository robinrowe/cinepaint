/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      SheetView - Spreadsheet for images ui widget
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: SheetView.cpp,v 1.2 2006/07/26 20:37:44 lamfada Exp $
 */

#include "SheetView.h"
#include <gui/UIUtils.h>
#include "CPSpreadsheet.h"
#include "SheetChangeListener.h"
#include "SheetCellRenderer.h"
#include "SheetKeyListener.h"
#include "SheetMouseListener.h"
#include "CPDAGNode.h"
#include <plug-ins/pdb/PluginParam.h>
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Int_Input.H>
#include <FL/Fl_Value_Slider.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Table.H>
#include <FL/fl_ask.H>


/**
 * Constructs a new SheetView to display and edit a spreadsheet for imaged
 * The SheetView interfaces with the spreadsheet data via the specified 
 * CPSreadsheet, this is used to update cells, and get cell data for display
 *
 * @param x position of the Spreadsheet widget
 * @param y position of the Spreadsheet widget
 * @param w width of the widget
 * @param h height of the widget
 * @param title title of the widget
 * @param cps CPSpreadsheet interface into the data backing the spreadsheet
 */
SheetView::SheetView(int x, int y, int w, int h, const char* title, CPSpreadsheet& cps)
		: Fl_Table(x, y, w, h, title), m_spreadsheet(cps)
{
	m_curr_row = -1;
	m_curr_col = -1;
	m_curr_select_cols = 0;
	m_curr_select_rows = 0;

	m_edit_cell_row = 0;
	m_edit_cell_col = 0;

	this->callback(&static_table_cb, this);
	this->when(this->when() | FL_WHEN_NOT_CHANGED);

	m_text_input = new Fl_Input(w/2,h/2,0,0);
	m_text_input->hide();
	m_text_input->callback(static_text_input_cb, this);
	m_text_input->when(FL_WHEN_ENTER_KEY_ALWAYS);

	m_cell_renderer = 0;

	this->end();
}

/**
 * Destructor
 */
SheetView::~SheetView()
{
	if(m_cell_renderer)
	{
		delete m_cell_renderer;
		m_cell_renderer = 0;
	}
}


/**
 * Sets the number of rows within this SheetView
 * If the cell input entry is currently visible, it will be set non-visaible and its
 * callback done
 *
 * @param val the new number of Rows within the SheetView
 */
void SheetView::rows(int val)
{
	if(m_text_input->visible())
	{
		m_text_input->do_callback();
	}
	Fl_Table::rows(val);
}

/**
 * Sets the number of columns within this SheetView
 * If the cell input entry is currently visible, it will be set non-visaible and its
 * callback done
 *
 * @param val the new number of Columns within the SheetView
 */
void SheetView::cols(int val)
{
	if(m_text_input->visible())
	{
		m_text_input->do_callback();
	}
	Fl_Table::cols(val);
}

//--------------------
// Input entry Control

/**
 * Sets the visibility of the cell input entry.
 * The cell input entry allows direct text entry into the curently selected cell
 * If vis is set true, the focus parametyer may be used to request that the
 * entry widget is given input focus.
 *
 * @param vis the visibility of the cell input entry
 * @param forcus indicates whether the input entry should gain focus of the cursor
 */
void SheetView::SetInputEntryVisibile(bool vis, bool focus)
{
	if(vis)
	{
		int cell_x, cell_y, cell_w, cell_h;
		m_edit_cell_row = get_current_row();
		m_edit_cell_col = get_current_col();
		find_cell(CONTEXT_CELL, m_edit_cell_row, m_edit_cell_col, cell_x, cell_y, cell_w, cell_h);
		m_text_input->resize(cell_x, cell_y, cell_w, cell_h);

		m_text_input->show();

		if(focus)
		{
			m_text_input->take_focus();
		}

		m_text_input->position(m_text_input->mark());
	}
	else
	{
		m_text_input->hide();
	}
}

/**
 * Returns whether the cell input entry is visible
 *
 * @return true if the cell input entry is visible, false otherwise
 */
bool SheetView::IsInputEntryVisible() const
{
	return(0!=m_text_input->visible());
}

/**
 * Sets the test displayed upon the cell input entry.
 * This method sets the text upon the cell iunput entry, a call should be made
 * to SetInputEntryVisible to actuially set the input visible at the currently
 * selected cell
 *
 * @param text the text to display.
 */
void SheetView::SetInputEntryText(const std::string& text)
{
	m_text_input->value(text.length() == 0 ? 0 : text.c_str());
	m_text_input->redraw();
}

/**
 * Processes the current cell text entry input.
 * This method invokes the cell entry callback with the currently set text
 *
 */
void SheetView::ProcessInputEntryText()
{
	m_text_input->do_callback();
}




//-----------------
// Change Listeners

/**
 * Adds a SheetChangeListener to this SheetView to review notifications of sheet changes
 * if the specified SheetChangeListener has already been added, it is not added again.
 * The resources of the specified SheetChangeListener are not managed by this class
 *
 * @param scl the SheetCjangeListener to add
 */
void SheetView::AddSheetChangeListener(SheetChangeListener* scl)
{
	m_sheet_listeners.AddListener(scl);
}

/**
 * Removes the specified SheetChangeListener
 * The removed SheetChangeListener will no longer receive SheetView change notifications from
 * this class
 *
 * @param scl the SheetChangeListener to be removed
 */
SheetChangeListener* SheetView::RemoveSheetChangeListener(SheetChangeListener* scl)
{
	return(m_sheet_listeners.RemoveListener(scl));
}



/**
 * Adds the specified CinePaintMouseListener to receive mouse events from this SheetView
 * if the specified CinePaintMouseListener has already been added it is not added again.
 * The resources of the specified CinePaintMouseListener are not managed by this class
 *
 * @param cml the CinePaintMouseListener to be added
 */
void SheetView::AddMouseListener(CinePaintMouseListener* cml)
{
	m_mouse_listeners.AddListener(cml);
}

/**
 * Removes the specified CinePaintMouseListener.
 * The removed CinePaintMouseListener will no longer receive mouse events from this SheetView
 *
 * @param cml the CinePaintMouseListener to be removed
 */
CinePaintMouseListener* SheetView::RemoveMouseListener(CinePaintMouseListener* cml)
{
	return(m_mouse_listeners.RemoveListener(cml));
}



/**
 * Adds a CinePaintKeyListener to this SheetView to review notifications of key pressed
 * if the specified CinePaintKeyListener has already been added, it is not added again.
 * The resources of the specified CinePaintKeyListener are not managed by this class
 *
 * @param kl the CinePaintKeyListener to add
 */
void SheetView::AddKeyListener(CinePaintKeyListener* kl)
{
	m_key_listeners.AddListener(kl);
}

/**
 * Removes the specified CinePaintKeyListener
 * The removed CinePaintKeyListener will no longer receive key press notifications from
 * this SheetView
 *
 * @param kl the CinePaintKeyListener to be removed
 */
CinePaintKeyListener* SheetView::RemoveKeyListener(CinePaintKeyListener* kl)
{
	return(m_key_listeners.RemoveListener(kl));
}


/**
 * Handles the drawaing of each cell when the table needcs redrawn
 *
 */
void SheetView::draw_cell(TableContext context, int row, int col, int x, int y, int w, int h)
{
	switch(context)
	{
			case CONTEXT_STARTPAGE:
			{
				// Get kb nav + mouse 'selection region' for use below
				//get_selection(top, left, bottom, right);
				break;
			}
			case CONTEXT_COL_HEADER:
			{
				draw_col_header(col, x, y, w, h);
				break;
			}
			case CONTEXT_ROW_HEADER:
			{
				draw_row_header(row, x, y, w, h);
				break;
			}
			case CONTEXT_CELL:
			{
				draw_context_cell(row, col, x, y, w, h);
				break;
			}
			case CONTEXT_RC_RESIZE:
			{
				if(m_text_input->visible())
				{
					find_cell(CONTEXT_TABLE, m_edit_cell_row, m_edit_cell_col, x, y, w, h);
					if(x != m_text_input->x() || y != m_text_input->y() || w != m_text_input->w() || h != m_text_input->h())
					{
						m_text_input->resize(x, y, w, h);
					}
				}
				break;
			}
			default:
			{
				break;
			}
	}
}

/**
 * Overidden from Widget to handle SheetView specific requirements.
 * Once this SheetView has finished with the event, the event is passes to handle(int e) within
 * the Fl_Table parent class
 * Currently the SheetView is required to handle the event first to extract the cursor
 * movement keypresses from fltk in order let interested listeners know of the cursor movement,
 * currently Fl_Table wallows these events.
 *
 * @param e the fltk event
 */
int SheetView::handle(int e)
{
	int _ret = Fl_Table::handle(e);

	switch(e)
	{
			case FL_KEYBOARD:
			{
				switch(Fl::event_key())
				{
						case FL_Home:
						case FL_End:
						case FL_Page_Up:
						case FL_Page_Down:
						case FL_Left:
						case FL_Right:
						case FL_Up:
						case FL_Down:
						{
							process_cursor_move();
							break;
						}
						default:
						{
							break;
						}
				}
			}
	}

	return(_ret);
}


//----------------------
// Table Drawing methods

/**
 * Draws the column header for the specified column
 *
 * @param col the column of the header to draw
 * @param x x coordinate of the header
 * @param y y coordinate of the header
 * @param w width of the header
 * @param h height of the header
 */
void SheetView::draw_col_header(int col, int x, int y, int w, int h)
{
	fl_font(FL_HELVETICA | FL_BOLD, 14);
	fl_push_clip(x, y, w, h);

	int top, left, bottom, right;
	get_selection(top, left, bottom, right);

	// @TODO handle coloring the header differently
	//       This only works when we force a redraw
	//if((col >= left) && (col <= right))
	//{
	//fl_draw_box(FL_THIN_UP_BOX, x, y, w, h, fl_darker(col_header_color()));
	//}
	//else
	//{
	fl_draw_box(FL_THIN_UP_BOX, x, y, w, h, col_header_color());
	//}

	fl_color(FL_BLACK);

	std::string s = CPSpreadsheet::ColName(col);
	fl_draw(s.c_str(), x, y, w, h, FL_ALIGN_CENTER);

	fl_pop_clip();
}

/**
 * Draws the row header for the specified column
 *
 * @param row the row of the header to draw
 * @param x x coordinate of the header
 * @param y y coordinate of the header
 * @param w width of the header
 * @param h height of the header
 */
void SheetView::draw_row_header(int row, int x, int y, int w, int h)
{
	fl_font(FL_HELVETICA | FL_BOLD, 14);
	fl_push_clip(x, y, w, h);

	int top, left, bottom, right;
	get_selection(top, left, bottom, right);

	// @TODO handle coloring the header differently
	//       This only works when we force a redraw
	//if((row >= top) && (row <= bottom))
	//{
	//fl_draw_box(FL_THIN_UP_BOX, x, y, w, h, fl_darker(row_header_color()));
	//}
	//else
	//{
	fl_draw_box(FL_THIN_UP_BOX, x, y, w, h, row_header_color());
	//}

	fl_color(FL_BLACK);

	std::string s = CPSpreadsheet::RowName(row);
	fl_draw(s.c_str(), x, y, w, h, FL_ALIGN_CENTER);

	fl_pop_clip();
}

/**
 * Draws a single specified cell within the table
 *
 * @param row the row of the cell to draw
 * @param col the column of the cell to draw
 * @param x x coordinate of the cell
 * @param y y coordinate of the cell
 * @param w width of the cell
 * @param h height of the cell
 */
void SheetView::draw_context_cell(int row, int col, int x, int y, int w, int h)
{
	if((row == m_edit_cell_row) && (col == m_edit_cell_col) && (m_text_input->visible()))
	{
		return;
	}

	if(!m_cell_renderer)
	{
		m_cell_renderer = new SheetCellRenderer(*this, m_spreadsheet);
	}
	m_cell_renderer->RenderCell(row, col, x, y, w, h);

	// get the cell from the graph, if it exists
	const CPDAGNode* node = m_spreadsheet.GetCellNode(row, col);

	if(node)
	{
		m_cell_renderer->RenderCellBackground(row, col, x, y, w, h, *node);
		m_cell_renderer->RenderCellData(row, col, x, y, w, h, *node);
	}

	m_cell_renderer->RenderCellSelection(row, col, x, y, w, h);
}




/**
 * process an update to the current cursor position
 * This method compares the current position to the previous position to determine if
 * the current cursor position has change, or whether the current range has changed,
 * and posts the appropriate SheetChangeListener event
 *
 */
void SheetView::process_cursor_move()
{
	if((get_current_row() != m_curr_row) || (get_current_col() != m_curr_col))
	{
		m_curr_col = get_current_col();
		m_curr_row = get_current_row();

		post_cell_changed_event(m_curr_row, m_curr_col);
	}

	int _start_col, _start_row, _end_col, _end_row;
	get_selection(_start_row, _start_col, _end_row, _end_col);

	if((m_curr_select_cols != (_start_col - _end_col)) || (m_curr_select_rows != (_start_row - _end_row)))
	{
		if((_start_col != _end_col) || (_start_row != _end_row))
		{
			post_range_changed_event(_start_row, _start_col, _end_row, _end_col);
		}
		else
		{
			if((m_curr_select_cols != 0) || (m_curr_select_rows != 0))
			{
				post_range_changed_event(_start_row, _start_col, _end_row, _end_col);
			}
		}

		m_curr_select_cols = _start_col - _end_col;
		m_curr_select_rows = _start_row - _end_row;
	}
}




//------------------
// Listener handling

void SheetView::post_cell_changed_event(int row, int col)
{
	for(SheetListenerList_t::const_iterator citer = m_sheet_listeners.begin(); citer != m_sheet_listeners.end(); ++citer)
	{
		(*citer)->CursorCellChanged(row, col);
	}
}

void SheetView::post_range_changed_event(int start_row, int start_col, int end_row, int end_col)
{
	for(SheetListenerList_t::const_iterator citer = m_sheet_listeners.begin(); citer != m_sheet_listeners.end(); ++citer)
	{
		(*citer)->RangeChanged(start_row, start_col, end_row, end_col);
	}
}


/**
 * Handler for the table callback.
 * This implementation Determines the current context of the table event. If the
 * context is CONTEXT_CELL, and event may be fired to a registered listener depending
 * on whever the table callback weas genereated from a keypress, or a mouse press.
 * If the context is either, CONTEXT_ROW_HEADER, CONTEXT_COL_HEADER, or CONTEXST_TABLE,
 * the cell text input is hidden and the callback performed.
 *
 */
void SheetView::table_cb()
{
	int _rows = this->callback_row();
	int _cols = this->callback_col();

	switch(this->callback_context())
	{
			case CONTEXT_CELL:
			{
				handle_cell_event(callback_row(), callback_col());
				break;
			}
			case CONTEXT_ROW_HEADER: // fall-through
			case CONTEXT_COL_HEADER:
			{
				if (m_text_input->visible())
				{
					m_text_input->do_callback();
				}
				m_text_input->hide();
				break;
			}
			case CONTEXT_TABLE:
			{
				if(_rows < 0 && _cols < 0)
				{
					if (m_text_input->visible())
					{
						m_text_input->do_callback();
					}
					m_text_input->hide();
				}
				break;
			}
			default:
			{
				break;
			}
	}
}

/**
 * handle the callback from the cell text input.
 * This method fires a ValueChanged event to any registered SheetViewlisteners 
 * indicating that the data in the current cell has been changed
 *
 */
void SheetView::text_input_cb()
{
	m_text_input->hide();

	// let those interested know of the change
	for(SheetListenerList_t::const_iterator citer = m_sheet_listeners.begin(); citer != m_sheet_listeners.end(); ++citer)
	{
		const char* c = m_text_input->value();
		std::string s = c != 0 ? std::string(c) : std::string("");
		(*citer)->ValueChanged(m_edit_cell_row, m_edit_cell_col, s);
	}
}

/**
 * Helper method to process a CONTEXT_CELL event.
 *
 * @param row the row of the cell event
 * @param col the column of the cell event
 */
void SheetView::handle_cell_event(int row, int col)
{
	// @TODO extract this fltk-to-event listener conversion
	//       its not the first time this has been used, so we should try to generalize it



	int event_state = Fl::event_state();
	int event_x_pos = Fl::event_x();
	int event_y_pos = Fl::event_y();

	switch(Fl::event())
	{
			case FL_PUSH:
			{
				process_cursor_move();

				for(MouseListenerList_t::const_iterator citer = m_mouse_listeners.begin(); citer != m_mouse_listeners.end(); ++citer)
				{
					(*citer)->MousePressed(Fl::event_button(), Fl::event_clicks(), event_x_pos, event_y_pos, UIUtils::fltk_event_state_to_cp_mouse_modifier(event_state));
				}
				break;
			}
			case FL_KEYBOARD:
			{
				for(KeyListenerList_t::const_iterator citer = m_key_listeners.begin(); citer != m_key_listeners.end(); ++citer)
				{
					(*citer)->KeyPressed(Fl::event_key(), Fl::event_text(), UIUtils::fltk_event_state_to_cp_mouse_modifier(event_state));
				}
				break;
			}
	}
}

int SheetView::get_current_row() {
	return m_curr_row;
}
int SheetView::get_current_col() {
	return m_curr_col;
}
