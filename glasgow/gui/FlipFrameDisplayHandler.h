/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      FlipFrameDisplayHandler - FlipFrame display update handling
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: FlipFrameDisplayHandler.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _FLIP_FRAME_DISPLAY_HANDLER_H_
#define _FLIP_FRAME_DISPLAY_HANDLER_H_

#include "dll_api.h"
#include <app/FrameManagerListener.h>

class FrameStore;
class SimpleDisplayWindow;
class CinePaintImage;
class FlipFrameController;

/**
 * FlipFrameDisplayHandler updates a display upon chnages to the currently selected frame.
 * FlipFrameDisplayHandler is a simple FrameManagerListener imlementation which attempts to
 * retrieve the Image corresponding to the selected frame from the FrameStore and set it to
 * display using a SimpleDisplayWindow
 *
 * @TODO fix syncing between FrameManager/FrameStore
 */
class CINEPAINT_GUI_API FlipFrameDisplayHandler : public FrameManagerListener
{
	public:
		/**
		 * Constructs a new FlipFrameDisplayHandler to handle updating the currently displayed frame
		 *
		 * @param store the FrameStore from which to retrieve image data
		 * @param display the display to update
		 * @param controller the FlipFrame Controller controlling this frame flipping
		 */
		FlipFrameDisplayHandler(FrameStore& store, SimpleDisplayWindow& display, FlipFrameController& controller);

		/**
		 * Destructor
		 */
		virtual ~FlipFrameDisplayHandler();


		/**
		 * Sets whether this FlipFrameDisplayHandler should pause the FlipFrameController when no data is available.
		 * Thie method rpvides a crude attemp at maintaining a sync between the FrameStore and the 
		 * FrameManager. If set true, upon a SelectionChanged notification, the FrameStore does not have any
		 * data for the specified frame, this FlipFrameDisplayHandler will call BufferAndPlay upon
		 * the FlipFrameController in order to pause, and refill the FrameStore.
		 *
		 * @TODO this is a very quick and dirty hack, and should be replaced with something a little safer!
		 */
		void SetDropMissingFrames(bool yn);


		//--------------------------------------
		// FrameManagerListener implementataions

		virtual void SelectionChanged(const FrameManager::FrameRecord* frame, size_t position);
		virtual void FrameAdded(const FrameManager::FrameRecord& frame, size_t position);
		virtual void FrameRemoved(const FrameManager::FrameRecord& frame, size_t position);
		virtual void FrameUpdated(const FrameManager::FrameRecord& frame, size_t position);
		virtual void FrameIndexChanged(const FrameManager::FrameRecord& frame, size_t old_pos, size_t new_pos);

	protected:

	private:

		bool maybe_drop_doc(CinePaintImage& image);
		void process_selection_change(const FrameManager::FrameRecord* frame);

		//--------
		// Members

		/** frame store from which to get image data */
		FrameStore& m_frame_store;

		/** display to set the current image upon */
		SimpleDisplayWindow& m_display;

		/** controller for the FlipFrame process */
		FlipFrameController& m_controller;

		struct ImageRecord
		{
			CinePaintImage* m_image;
			const FrameManager::FrameRecord* m_frame;
		};

		/** Currently active image data */
		ImageRecord m_curr_image;



		/** indicates if the FlipFrame should be paused if no data is available */
		bool m_pause_on_refill_buffer;
};

#endif /* _FLIP_FRAME_DISPLAY_HANDLER_H_ */
