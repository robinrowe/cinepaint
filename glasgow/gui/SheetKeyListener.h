/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      SheetView - Spreadsheet for images ui widget
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: SheetKeyListener.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _SHEET_KEY_LISTENER_H_
#define _SHEET_KEY_LISTENER_H_

#include "dll_api.h"
#include <app/CinePaintKeyListener.h>

class CPSpreadsheet;
class SheetView;


/**
 * Key Listener interface to handle key events generated from a SheetView.
 * This Key Listener is responsible for listening to key presses from within the
 * SheetView and responding to certain conditions.
 *
 */
class CINEPAINT_GUI_API SheetKeyListener : public CinePaintKeyListener
{
	public:
		SheetKeyListener(SheetView& view, CPSpreadsheet& spreadsheet);
		virtual ~SheetKeyListener();

		/**
		 * If the text is being typed, the cell entry input is displayed to accept the input
		 * within the SheetView.
		 * If ecscape is pressed, the current entry is cancelled and and entered text is ignored.
		 * If enter is pressed, the current entry is accepted and the input entry callback is done.
		 * If delete is pressed the current cell is deleted
		 *
		 * @param key the key id pressed
		 * @param text the text that would be inserted from the key press
		 * @param modifier any keyboard modifier key being pressed
		 */
		virtual void KeyPressed(int key, const char* text, unsigned int modifier);

	protected:

	private:

		/** the sheet view this key listener is listening to for events */
		SheetView& m_view;
		
		/** CPSpreadsheet interface to the DAG */
		CPSpreadsheet& m_spreadsheet;
};

#endif /* _SHEET_KEY_LISTENER_H_ */
