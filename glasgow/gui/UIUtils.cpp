/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      UIUtils - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: UIUtils.cpp,v 1.2 2008/01/28 18:22:28 robinrowe Exp $
 */

#include <gui/UIUtils.h>

#include <plug-ins/pdb/AbstractBuf.h>
#include <plug-ins/pdb/ModifierEnums.h>

#include <FL/Fl_Box.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_Value_Slider.H>
#include <FL/Fl_Window.H>

// initialize statis members

/** general row height for a label entry type combination row */
const int UIUtils::DEFAUTL_ROW_HEIGHT = 30;

/** general test labelled button height */
const int UIUtils::BTN_LABEL_HEIGHT = 30;

/** Default x padding between widgets */
const int UIUtils::DEFAULT_X_PAD = 5;

/** Default y padding between widgets */
const int UIUtils::DEFAULT_Y_PAD = 5;

/** Default layout height */
const int UIUtils::DUMMY_DEFAULT_HEIGHT = 1000;


// initialize values
UIUtils::LayoutHelper::LayoutHelper()
: m_total_width(0), m_total_height(0), m_posx(0), m_posy(0), m_suggested_width(0), m_suggested_height(0), m_actual_width(0), m_actual_height(0)
{}

UIUtils::LayoutHelper::LayoutHelper(int x, int y, int sw, int sh)
: m_total_width(0), m_total_height(0), m_posx(x), m_posy(y), m_suggested_width(sw), m_suggested_height(sh), m_actual_width(0), m_actual_height(0)
{}


/**
 * Crates an FL_Image from an AbstractBuf.
 * The AbstractBuf is expected to contain unsigned char RGB image data, if this is not the case,
 * this method returns 0
 *
 * @param buf the AbstractBuf to create an Fl_Image from
 * @return the constructed Fl_Image, or 0 if the AbstractBuf is an incompatible format
 */
Fl_Image* UIUtils::CreateFlImage(const AbstractBuf& buf)
{
	Fl_RGB_Image* _image = 0;

	if(buf.GetTag().GetFormat() == CinePaintTag::FORMAT_RGB_ENUM)
	{
		_image = new Fl_RGB_Image(buf.GetPortionData(0, 0), buf.GetWidth(), buf.GetHeight(), buf.GetTag().GetBytes());
	}

	return(_image);
}

/**
 * Convenience method to create an aligned textual label
 *
 * @param text label text
 * @return newly constructed Fl_Box with specified label left aligned
 */
Fl_Box* UIUtils::CreateLabel(int x, int y, int w, int h, const char* label, Fl_Align align)
{
	Fl_Box* _box = new Fl_Box(x, y, w, h, label);
	_box->align(align | FL_ALIGN_INSIDE);

	return(_box);
}

/**
 * Convenience method to Creates a consistant looking value slider
 *
 * @param x x position of the widget
 * @param y y position of the widget
 * @param w width of the widget
 * @param h height of the widget
 * @param label label of the widget
 * @param min min value of the slide range
 * @param max max value of the slide range
 * @param val initial value of the slide
 * @param step step increment of the slide
 * @return a newly allocated Fl_Value_Slider
 */
Fl_Value_Slider* UIUtils::CreateSlider(int x, int y, int w, int h, double min, double max, double val, double step)
{
	Fl_Value_Slider* _slider = new Fl_Value_Slider(x, y, w, h);
	_slider->type(FL_HOR_NICE_SLIDER);
	_slider->bounds(min, max);
	_slider->step(step);
	_slider->value(val);

	return(_slider);
}


UIUtils::IdleLoopWindowDestructor::IdleLoopWindowDestructor(Fl_Window* window)
: CinePaintEvent(false), m_window(window)
{}
				
void  UIUtils::IdleLoopWindowDestructor::Process()
{
	delete m_window;
	m_window = 0;
}

unsigned int UIUtils::fltk_event_state_to_cp_mouse_modifier( int _state )
{
	unsigned int _ret = 0;
	if ( _state & FL_SHIFT  )

		_ret |= CP_MOD_SHIFT_MASK_ENUM;

	if ( _state & FL_CAPS_LOCK )

		_ret |= CP_MOD_CAPS_LOCK_MASK_ENUM;

	if ( _state & FL_CTRL )

		_ret |= CP_MOD_CONTROL_MASK_ENUM;

	if ( _state & FL_ALT )

		_ret |= CP_MOD_ALT_MASK_ENUM;

	if ( _state & FL_NUM_LOCK )

		_ret |= CP_MOD_NUM_LOCK_MASK_ENUM;

	if ( _state & FL_META )

		_ret |= CP_MOD_META_MASK_ENUM;

	if ( _state & FL_SCROLL_LOCK )

		_ret |= CP_MOD_SCROLL_LOCK_MASK_ENUM;

	if ( _state & FL_BUTTON1 )

		_ret |= CP_MOD_BUTTON1_MASK_ENUM;

	if ( _state & FL_BUTTON2 )
		_ret |= CP_MOD_BUTTON2_MASK_ENUM;
	if ( _state & FL_BUTTON3 )
		_ret |= CP_MOD_BUTTON3_MASK_ENUM;
	return (_ret);
}
