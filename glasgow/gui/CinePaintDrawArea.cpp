/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                     CinePaintDrawArea - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintDrawArea.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "CinePaintDrawArea.h"
#include <plug-ins/pdb/CinePaintMouseListener.h>
#include <plug-ins/pdb/ModifierEnums.h>
#include "CinePaintWidgetListener.h"
#include "UIUtils.h"
#include <plug-ins/pdb/AbstractBufFactory.h>
#include <plug-ins/pdb/AbstractRenderer.h>
#include <plug-ins/pdb/CinePaintImage.h>
#include <plug-ins/pdb/CinePaintTag.h>
#include <plug-ins/pdb/Drawable.h>

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <algorithm>
#include <memory>

/**
 * Constructs a new CinePaintDrawArea with the specified size and position
 *
 * @param x widget x position
 * @param y widget y position
 * @param w widget width
 * @param h widget height
 */
CinePaintDrawArea::CinePaintDrawArea(int x, int y, int w, int h)
:	Fl_Widget(x, y, w, h, 0), 
	m_projection(w, h)
/* rsr:
	, m_image_change_listener(*this)
*/
{	m_image_change_listener.SplitInit(this);
	m_image = 0;
	m_glass_layer = 0;
}

/**
 * Constructs a new CinePaintDrawArea with the specified size and position to dispay the specified CinePaintImage
 *
 * @param x widget x position
 * @param y widget y position
 * @param w widget width
 * @param h widget height
 * @param image the CinePaintIamge to display
 */
CinePaintDrawArea::CinePaintDrawArea(int x, int y, int w, int h, CinePaintImage* image, AbstractRenderer* renderer)
:	Fl_Widget(x, y, w, h, 0), 
	m_projection(w, h)
/* rsr:
, m_image_change_listener(*this)
*/
{	m_image_change_listener.SplitInit(this);
	SetImage(image, renderer);
	m_glass_layer = 0;
}

CinePaintDrawArea::~CinePaintDrawArea()
{
	if(m_glass_layer)
	{
		delete m_glass_layer;
		m_glass_layer = 0;
	}
}


//-----------------------
// CinePaintImage methods

/**
 * Sets the CinePaintImage this CinePaintDrawArea display
 *
 * @param image the CinePaintImage to display
 * @param renderer the AbstractRenderer used to render the image this draw area displays
 */
void CinePaintDrawArea::SetImage(CinePaintImage* image, AbstractRenderer* renderer)
{
	if(m_image)
	{
		m_image->RemoveImageChangeListener(&m_image_change_listener);
	}

	m_image = image;

	m_projection.SetRenderer(renderer);

	m_image->AddImageChangeListener(&m_image_change_listener);


	// calculate the projection
	m_projection.InvalidateImageRegion(0, 0, image->GetWidth(), image->GetHeight());
}

/**
 * Returns the CinePaintImage diaplayed by this CinePaintDrawArea
 *
 * @return the CinepaintImage being displayed
 */
CinePaintImage* CinePaintDrawArea::GetImage() const
{
	return(m_image);
}


/**
 * Returns access to the Projection used to provide image data for this draw area
 *
 * @return access to the Projection used to provide image data
 */
Projection& CinePaintDrawArea::getProjection()
{
	return(m_projection);
}


/**
 * Returns the Glass Layer for this Draw Area.
 * The glass layer is a trasnsparent layer ontop of the Projection data.
 * It may be used for temporary drawing ontop of the projection, for example
 * to draw a selection area as the user drags the mouse.
 * The data upon the Glass layer is not safe, another process may override
 * this data, or clear the layer completely
 *
 * @return the glass layer of this DrawArea
 */
Drawable* CinePaintDrawArea::GetGlassLayer()
{
	if(!m_glass_layer)
	{
		CinePaintTag _tag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false);
		m_glass_layer = new Drawable(_tag, this->w(), this->h(), "Glass Layer");
	}

	return(m_glass_layer);
}


//----------------------------
// Overriden FL_Widget methods

/**
 * Resizes and position this Widget to the specified values.
 * This implementation calls FL_Widget::resize but also informs all registered
 * CinePaintWidgetListerns of the change.
 *
 * @param x the x location
 * @param y the y location
 * @param w the widget width
 * @param h the widget height
 */
void CinePaintDrawArea::resize(int x, int y, int w, int h)
{
	Fl_Widget::resize(x, y, w, h);

	// resize the Projection
	m_projection.SetSize(w, h);

	// @TODO resize the Glass Layer
	//       we either need to copy the data, or provide a proper resize method on a Drawable

	for(WidgetListenerList_t::const_iterator _citer = m_widget_listeners.begin(); _citer != m_widget_listeners.end(); ++_citer)
	{
		(*_citer)->WidgetResized(w, h);
	}
}

/**
 * Sets the size of thie widget to the specified size.
 * This methods is a convenience method for resize, hiding FL_Widget::size inorder to
 * provide notifications of widget events to registered CinePaintWidgetListeners
 *
 * @param w the widget width
 * @param h the widget height
 */
void CinePaintDrawArea::size(short w, short h)
{
	resize(this->x(), this->y(), w, h);
}



//--------------
// Canvas Events

/**
 * Adds the specified CinePaintMouseListener to receive mouse events from this CinePaintDrawArea
 * if the specified CinePaintMouseListener has already been added it is not added again.
 * The resources of the specified CinePaintMouseListener are not managed by this class
 * It is safe to assume that generated events occur within the UI event loop.
 *
 * @param cml the CinePaintMouseListener to be added
 */
void CinePaintDrawArea::AddMouseListener(CinePaintMouseListener* cml)
{
	m_mouse_listeners.AddListener(cml);
}

/**
 * Removes the specified CinePaintMouseListener.
 * The removed CinePaintMouseListener will no longer receive mouse events from this CinePaintDrawArea
 * It is safe to assume that generated events occur within the UI event loop.
 *
 * @param cml the CinePaintMouseListener to be removed
 */
CinePaintMouseListener* CinePaintDrawArea::RemoveMouseListener(CinePaintMouseListener* cml)
{
	return(m_mouse_listeners.RemoveListener(cml));
}

/**
 * Adds the specified CinePaintWidgetListener to receive widget events from this CinePaintDrawArea
 * It is NOT  safe to assume that generated events occur within the UI event loop.
 *
 * @param cwl the CinePaintWidgetListener to be added
 */
void CinePaintDrawArea::AddWidgetListener(CinePaintWidgetListener* cwl)
{
	m_widget_listeners.AddListener(cwl);
}

/**
 * Removes and returns the specified CinePaintWidgetListener.
 *
 * @param cwl the CinePaintWidgetListener to be removed
 */
CinePaintWidgetListener* CinePaintDrawArea::RemoveWidgetListener(CinePaintWidgetListener* cwl)
{
	return(m_widget_listeners.RemoveListener(cwl));
}



void CinePaintDrawArea::repaint(int x, int y, int w, int h)
{
	this->redraw();
}




/**
 * handles an FLTK event
 * This method translates the event into an appropriate CanvasMouseListener event and informs
 * all registered CanvasMouseListeners
 *
 * @param e the FLTK event type
 * @return non-zero if the event was handled by this class, zero otherwise
 */
int CinePaintDrawArea::handle(int e)
{
	int _ret = 0;
	int _event_x_pos = Fl::event_x();
	int _event_y_pos = Fl::event_y();
	int _shift_event_state = Fl::event_state(); 

	event_to_image_space(_event_x_pos, _event_y_pos);

	switch(e)
	{
		case FL_PUSH:
		{
			_ret = 1;
			for(MouseListenerList_t::const_iterator citer = m_mouse_listeners.begin(); citer != m_mouse_listeners.end(); ++citer)
			{
				(*citer)->MousePressed(Fl::event_button(), Fl::event_clicks(), _event_x_pos, _event_y_pos, UIUtils::fltk_event_state_to_cp_mouse_modifier(_shift_event_state));
			}
			break;
		}
		case FL_RELEASE:
		{
			_ret = 1;
			for(MouseListenerList_t::const_iterator citer = m_mouse_listeners.begin(); citer != m_mouse_listeners.end(); ++citer)
			{
				(*citer)->MouseReleased(Fl::event_button(), _event_x_pos, _event_y_pos);
			}
			break;
		}
		case FL_DRAG:
		{
			_ret = 1;
			for(MouseListenerList_t::const_iterator citer = m_mouse_listeners.begin(); citer != m_mouse_listeners.end(); ++citer)
			{
				(*citer)->MouseDragged(Fl::event_button(), _event_x_pos, _event_y_pos);
			}
			break;
		}
		case FL_MOVE:
		{
			_ret = 1;
			for(MouseListenerList_t::const_iterator citer = m_mouse_listeners.begin(); citer != m_mouse_listeners.end(); ++citer)
			{
				(*citer)->MouseMoved(_event_x_pos, _event_y_pos);
			}
			break;
		}
		case FL_ENTER:
		{
			_ret = 1;
			for(MouseListenerList_t::const_iterator citer = m_mouse_listeners.begin(); citer != m_mouse_listeners.end(); ++citer)
			{
				(*citer)->MouseEntered();
			}
			break;
		}
		case FL_LEAVE:
		{
			_ret = 0;
			for(MouseListenerList_t::const_iterator citer = m_mouse_listeners.begin(); citer != m_mouse_listeners.end(); ++citer)
			{
				(*citer)->MouseExited();
			}
			break;
		}
		default:
		{
			break;
		}
	}

	return(_ret);
}

/**
 * Draws this widget
 */
void CinePaintDrawArea::draw()
{
	if((m_projection.GetWidth() != this->w()) || (m_projection.GetHeight() != this->h()))
	{
		m_projection.SetSize(this->w(), this->h());
	}

	unsigned char* _data = m_projection.GetProjection()->GetPortionData(0, 0);
	if(_data)
	{
		fl_draw_image(_data, x(), y(), m_projection.GetWidth(), m_projection.GetHeight(), 3);
	}
	else
	{
		printf("GOT NO PROJECTION DATA!!\n");
	}

	// is there a glass layer?
	if(m_glass_layer)
	{
		_data = m_glass_layer->GetBuffer().GetPortionData(0, 0);
		if(_data)
		{
			fl_draw_image(_data, x(), y(), w(), h(), 3);
		}
	}
}

/**
 * Translates the specified coordinates into image space coordinates
 *
 */
void CinePaintDrawArea::event_to_image_space(int& x, int& y)
{
	// account for the window position
	x -= this->x();
	y -= this->y();

	// account for the image offset
	x += m_projection.GetOffSetX();
	y += m_projection.GetOffSetY();
	
	// @TODO do something usefull about zoom
}

void CinePaintDrawArea::image_to_event_space(int& x, int& y)
{
	
}

