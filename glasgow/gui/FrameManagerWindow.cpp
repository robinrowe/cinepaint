/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      FrameManagerWindow - FrameManager UI
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: FrameManagerWindow.cpp,v 1.2 2006/12/17 07:28:54 robinrowe Exp $
 */

#include "FrameManagerWindow.h"
#include "CinePaintAppUI.h"
#include "CinePaintFrame.h"
#include "FlipFrameDisplayHandler.h"
#include "MenuFunctorHelper.h"
#include "UIUtils.h"
#include "SimpleDisplayWindow.h"

#include <app/AppSettings.h>
#include <app/CinePaintApp.h>
#include <app/CinePaintDocList.h>
#include <app/CinePaintEventList.h>
#include <app/CinePaintEventFunctor.h>
#include <app/FlipFrameController.h>
#include <app/FlipFrameDriver.h>
#include <app/FrameStore.h>

#include <FL/Fl_Button.H>
#include <FL/fl_draw.H>
#include <FL/Fl_File_Chooser.H>
#include <FL/Fl_Menu_Bar.H>

const int FrameManagerWindow::FrameManagerTable::TABLE_COLS = 6;


/**
 * Constructs a new FrameManagerWindow to disply the specified FrameManager
 * The FrameManagerWindow provides a sinple UI to the FrameManager allowing addition,
 * removal and re-ordering along with the controls for a simple flipbook presentation
 *
 * @param w window width
 * @param h window height
 * @param title window title
 * @param fm FrameManaget to display
 */
FrameManagerWindow::FrameManagerWindow(int w, int h, const char* title, FrameManager& fm)
		: Fl_Window(w,h,title), m_frame_manager(fm)
{
	Fl_Menu_Bar* _menu = new Fl_Menu_Bar(0, 0, w, 30);
	m_menu_functor_helper = new MenuFunctorHelper();
	build_menu(*_menu, *m_menu_functor_helper);

	// frame spacing values / hacks
	int frame_border = 5;
	int frame_offset_y = (frame_border * 2) + 20;
	int frame_height_extra = frame_offset_y + (frame_border * 2);
	int frame_internal_x_offset = 2 * frame_border;
	int frame_height = UIUtils::BTN_LABEL_HEIGHT + frame_height_extra;

	// skip the menubar
	int posy = 30;
	int posx = 0;


	//
	// construct the table view
	//
	int table_height = h - 30 - ((frame_height * 2) + (3 * UIUtils::DEFAULT_Y_PAD));
	m_frame_view = new FrameManagerTable(posx, posy, w, table_height, 0, fm);

	// rows
	m_frame_view->row_header(0);
	m_frame_view->row_header_width(0);
	m_frame_view->row_resize(1);
	m_frame_view->rows(0);
	m_frame_view->row_height_all(25);
	m_frame_view->row_resize_min(10);

	// COLS
	m_frame_view->col_header(1);
	m_frame_view->col_header_height(25);
	m_frame_view->col_resize(1);
	m_frame_view->cols(FrameManagerWindow::FrameManagerTable::TABLE_COLS);
	m_frame_view->col_width_all(w / FrameManagerWindow::FrameManagerTable::TABLE_COLS);
	m_frame_view->col_resize_min(10);

	m_frame_view->selection_color(FL_YELLOW);

	m_frame_view->when(FL_WHEN_CHANGED);
	m_frame_view->callback(&static_frame_view_cb, this);
	m_frame_view->end();

	this->resizable(m_frame_view);

	posy += table_height + UIUtils::DEFAULT_Y_PAD ;



	//
	// Construct the Frame Controls
	//
	CinePaintFrame* frame_controls_frame = new CinePaintFrame(posx, posy, w, frame_height, "Frame Control", FL_ENGRAVED_FRAME);
	posy += frame_offset_y;

	int num_btns = 4;
	int btn_width = ((w - (2 * frame_internal_x_offset)) - ((num_btns +1) * UIUtils::DEFAULT_X_PAD)) / num_btns;

	posx = 0;
	posx += UIUtils::DEFAULT_X_PAD + frame_internal_x_offset;

	Fl_Button* shift_up_btn = new Fl_Button(posx, posy, btn_width, UIUtils::BTN_LABEL_HEIGHT, "U");
	shift_up_btn->tooltip("Shift Frame Upwards");
	shift_up_btn->callback(&static_shift_up_cb, this);
	posx += btn_width + UIUtils::DEFAULT_X_PAD;

	Fl_Button* shift_down_btn = new Fl_Button(posx, posy, btn_width, UIUtils::BTN_LABEL_HEIGHT, "D");
	shift_down_btn->tooltip("Shift Frame Downwards");
	shift_down_btn->callback(&static_shift_down_cb, this);
	posx += btn_width + UIUtils::DEFAULT_X_PAD;

	Fl_Button* add_frame_btn = new Fl_Button(posx, posy, btn_width, UIUtils::BTN_LABEL_HEIGHT, "A");
	add_frame_btn->tooltip("Add Frames");
	add_frame_btn->callback(&static_add_frames_cb, this);
	posx += btn_width + UIUtils::DEFAULT_X_PAD;

	Fl_Button* remove_frame_btn = new Fl_Button(posx, posy, btn_width, UIUtils::BTN_LABEL_HEIGHT, "R");
	remove_frame_btn->tooltip("Remove Selected Frames");
	remove_frame_btn->callback(&static_remove_frame_cb, this);
	posx += btn_width + UIUtils::DEFAULT_X_PAD;

	frame_controls_frame->end();

	posy += UIUtils::BTN_LABEL_HEIGHT + (frame_height_extra - frame_offset_y) + UIUtils::DEFAULT_Y_PAD;



	//
	// Construct the Playback Controls
	//

	CinePaintFrame* playback_controls_frame = new CinePaintFrame(0, posy, w, frame_height, "Playback Control", FL_ENGRAVED_FRAME);
	posy += frame_offset_y;

	// create the step/flip control buttons
	num_btns = 5;
	btn_width = ((w - (2 * frame_internal_x_offset)) - ((num_btns +1) * UIUtils::DEFAULT_X_PAD)) / num_btns;

	posx = 0;
	posx += UIUtils::DEFAULT_X_PAD + frame_internal_x_offset;

	Fl_Button* rstep_btn = new Fl_Button(posx, posy, btn_width, UIUtils::BTN_LABEL_HEIGHT, "SB");
	rstep_btn->tooltip("Step Backwards");
	rstep_btn->callback(&static_step_back_cb, this);
	posx += btn_width + UIUtils::DEFAULT_X_PAD;

	Fl_Button* rplay_btn = new Fl_Button(posx, posy, btn_width, UIUtils::BTN_LABEL_HEIGHT, "B");
	rplay_btn->tooltip("Play Backwards");
	rplay_btn->callback(&static_play_backward_cb, this);
	posx += btn_width + UIUtils::DEFAULT_X_PAD;

	Fl_Button* stop_btn = new Fl_Button(posx, posy, btn_width, UIUtils::BTN_LABEL_HEIGHT, "S");
	stop_btn->tooltip("Stop Playback");
	stop_btn->callback(&static_stop_cb, this);
	posx += btn_width + UIUtils::DEFAULT_X_PAD;

	Fl_Button* fplay_btn = new Fl_Button(posx, posy, btn_width, UIUtils::BTN_LABEL_HEIGHT, "P");
	fplay_btn->tooltip("Play");
	fplay_btn->callback(&static_play_cb, this);
	posx += btn_width + UIUtils::DEFAULT_X_PAD;

	Fl_Button* fstep_btn = new Fl_Button(posx, posy, btn_width, UIUtils::BTN_LABEL_HEIGHT, "SF");
	fstep_btn->tooltip("Step Forwards");
	fstep_btn->callback(&static_step_forward_cb, this);

	playback_controls_frame->end();



	this->end();






	m_frame_manager_listener = new FrameListener(*this);
	m_frame_manager.AddFrameListener(m_frame_manager_listener);

	m_frame_store = new FrameStore(m_frame_manager);
	m_frame_controller = new FlipFrameController(m_frame_manager, *m_frame_store);

	m_display_window = new SimpleDisplayWindow(400, 400, "FlipFrame Display");
	m_display_window->show();

	m_display_handler = new FlipFrameDisplayHandler(*m_frame_store, *m_display_window, *m_frame_controller);
	m_frame_manager.AddFrameListener(m_display_handler);

	// update the FrameStore on changes to the display window size
	// this lets the FrameStore create image rendors at the required size
	m_display_resize_handler = new DisplayResizeHandler(*m_frame_store);
	m_display_window->AddWidgetListener(m_display_resize_handler);
}

/**
 * Destructor
 */
FrameManagerWindow::~FrameManagerWindow()
{
	if(m_display_handler)
	{
		m_frame_manager.RemoveFrameListener(m_display_handler);
		delete m_display_handler;
		m_display_handler = 0;
	}

	if(m_frame_manager_listener)
	{
		m_frame_manager.RemoveFrameListener(m_frame_manager_listener);
		delete m_frame_manager_listener;
		m_frame_manager_listener = 0;
	}

	if(m_display_resize_handler)
	{
		if(m_display_window)
		{
			m_display_window->RemoveWidgetListener(m_display_resize_handler);
		}

		delete m_display_resize_handler;
		m_display_resize_handler = 0;
	}

	if(m_display_window)
	{
		delete m_display_window;
		m_display_window = 0;
	}

	if(m_frame_store)
	{
		delete m_frame_store;
		m_frame_store = 0;
	}

	if(m_menu_functor_helper)
	{
		delete m_menu_functor_helper;
		m_menu_functor_helper = 0;
	}

}

/**
 * Sets the selected row upon this FrameManger display window
 * All other rows are deselected
 *
 * @param index the index of the row to set selected.
 */
void FrameManagerWindow::SetSelected(size_t index)
{
	m_frame_view->select_all_rows(0);

	if((index != FrameManager::INVALID_INDEX) && (!m_frame_view->row_selected(int(index))))
	{
		m_frame_view->select_row(int(index));
	}
}

/**
 * Returns the FrameManager being displayed by this FrameManagerWindow
 *
 * @return the FrameManager being displayed
 */
FrameManager& FrameManagerWindow::GetFrameManager() const
{
	return(m_frame_manager);
}





void FrameManagerWindow::build_menu(Fl_Menu_Bar& menu, MenuFunctorHelper& menu_helper)
{
	menu_helper.AddMenuItem0(menu, 0, "&File/&New...", this, &FrameManagerWindow::DUMMY_CB);
	menu_helper.AddMenuItem0(menu, 0, "&Frame Manager/&Add Frame", this, &FrameManagerWindow::add_file_source_cb);
	menu_helper.AddMenuItem0(menu, 0, "&Frame Manager/&Add Frames", this, &FrameManagerWindow::add_frames_cb);
	menu_helper.AddMenuItem0(menu, 0, "&Frame Manager/&_Remove Selected", this, &FrameManagerWindow::remove_frames_cb);
	menu_helper.AddMenuItem0(menu, 0, "&Frame Manager/&Clear", &m_frame_manager, &FrameManager::Clear);
	menu_helper.AddMenuItem0(menu, 0, "&View/&Close", this, &FrameManagerWindow::close_view_cb);
}




//-----------------
// Widget Callbacks

void FrameManagerWindow::DUMMY_CB()
{
	printf("TODO: DUMMY_CB\n");
}


void FrameManagerWindow::frame_view_cb()
{
	int row = m_frame_view->callback_row();
	int col = m_frame_view->callback_col();

	switch(m_frame_view->callback_context())
	{
			case FrameManagerTable::CONTEXT_CELL:
			{
				handle_cell_selection(row, col);
				break;
			}
			case FrameManagerTable::CONTEXT_ROW_HEADER: // fall-through
			case FrameManagerTable::CONTEXT_COL_HEADER:
			case FrameManagerTable::CONTEXT_TABLE:
			default:
			{
				break;
			}
	}
}

void FrameManagerWindow::step_back_cb()
{
	printf("Step Back\n");
	if(m_frame_controller)
	{
		m_frame_controller->Step(FlipFrameController::BACKWARD_ENUM);
	}
}

void FrameManagerWindow::play_backward_cb()
{
	printf("Play Backwards\n");
	if(m_frame_controller)
	{
		m_frame_controller->Play(FlipFrameController::BACKWARD_ENUM);
	}
}

void FrameManagerWindow::stop_cb()
{
	printf("Stop\n");
	if(m_frame_controller)
	{
		m_frame_controller->Stop();
	}
}

void FrameManagerWindow::play_cb()
{
	printf("Play\n");
	if(m_frame_controller)
	{
		m_frame_controller->Play(FlipFrameController::FORWARD_ENUM);
	}
}

void FrameManagerWindow::step_forward_cb()
{
	printf("Step Forwards\n");
	if(m_frame_controller)
	{
		m_frame_controller->Step(FlipFrameController::FORWARD_ENUM);
	}
}

void FrameManagerWindow::add_file_source_cb()
{
	if(m_prev_file_select_dir.empty())
	{
		const char* home_dir = GetPathUserHome();
		if(home_dir)
		{
			m_prev_file_select_dir = std::string(home_dir);
		}
	}

	Fl_File_Chooser chooser(m_prev_file_select_dir.c_str(), 0, 0, "File Selection");

	// @note there seems little point in setting the file chooser to accept multi selection,
	//       as theres no obvious way to get back the multiple selections
	//chooser.type(Fl_File_Chooser::MULTI);

	chooser.show();

	while(chooser.shown())
	{
		Fl::wait();
	}

	if(chooser.count() > 0)
	{
		const char* filename = chooser.value();
		printf("Selected file = %s\n", filename);
		if(filename)
		{
			// remeber the last used directory
			m_prev_file_select_dir = std::string(chooser.directory());

			std::string s(filename);

			m_frame_manager.AddLast(std::string(""), s, 100);
		}
	}
}

void FrameManagerWindow::shift_up_cb()
{
	if(!m_frame_view->row_selected(0))
	{
		// start at 1, cant move up the top row
		for(int i = 1; i < m_frame_view->rows(); i++)
		{
			if(m_frame_view->row_selected(i))
			{
				m_frame_view->select_row(i, 0);

				m_frame_manager.SetIndex(i, i -1);
				m_frame_view->select_row(i -1, 1);
			}
		}
	}
}

void FrameManagerWindow::shift_down_cb()
{
	if(!m_frame_view->row_selected(m_frame_view->rows() -1))
	{
		// start at rows() -2, 0-indexed gives rows()-1, and -2 because we cant move the bottom row
		for(int i = m_frame_view->rows() -2; i > -1; i--)
		{
			if(m_frame_view->row_selected(i))
			{
				m_frame_view->select_row(i, 0);

				m_frame_manager.SetIndex(i, i +1);
				m_frame_view->select_row(i +1, 1);
			}
		}
	}
}


// @TODO add multiple frames
void FrameManagerWindow::add_frames_cb()
{
	printf("TODO\n");
}

void FrameManagerWindow::remove_frames_cb()
{
	for(int i = m_frame_view->rows() -1; i > -1; i--)
	{
		if(m_frame_view->row_selected(i))
		{
			m_frame_view->select_row(i, 0);
			printf("Removing Row %d\n", i);
			m_frame_manager.Remove(i);
		}
	}
}

void FrameManagerWindow::close_view_cb()
{
	this->hide();
}

void FrameManagerWindow::handle_cell_selection(int row, int col)
{
	if(!m_frame_view->row_selected(row))
	{
		printf("Selection changed\n");
		m_frame_manager.SetCurrentFrame(row);
	}

	if(col == FrameManagerTable::ACTIVE_ENUM)
	{
		const FrameManager::FrameRecord* frame = m_frame_manager.GetFrame(row);
		if(frame)
		{
			m_frame_manager.SetFrameActive(row, !frame->GetActive());
			//frame->SetActive(!frame->GetActive());
		}
	}
}






//---------------
// Nested Classes

FrameManagerWindow::FrameManagerTable::FrameManagerTable(int x, int y, int w, int h, const char* title, FrameManager& fm)
		: Fl_Table_Row(x, y, w, h, title), m_frame_manager(fm)
{
	// setup the column headers
	m_col_headers = new char*[FrameManagerWindow::FrameManagerTable::TABLE_COLS];
	m_col_headers[ID_ENUM] = "Id";
	m_col_headers[FILENAME_ENUM] = "Filename";
	m_col_headers[LOADED_ENUM] = "Loaded";
	m_col_headers[PRE_LOADED_ENUM] = "Stored";
	m_col_headers[MODIFIED_ENUM] = "Modified";
	m_col_headers[ACTIVE_ENUM] = "Active";
}

FrameManagerWindow::FrameManagerTable::~FrameManagerTable()
{}

void FrameManagerWindow::FrameManagerTable::draw_cell(TableContext context, int row, int col, int x, int y, int w, int h)
{
	switch(context)
	{
			case CONTEXT_COL_HEADER:
			{
				fl_font(FL_HELVETICA | FL_BOLD, 14);
				fl_push_clip(x, y, w, h);

				int top, left, bottom, right;
				get_selection(top, left, bottom, right);
				fl_draw_box(FL_THIN_UP_BOX, x, y, w, h, col_header_color());
				fl_color(FL_BLACK);
				fl_draw(m_col_headers[col], x, y, w, h, FL_ALIGN_CENTER);

				fl_pop_clip();
				break;
			}
			case CONTEXT_ROW_HEADER:
			{
				fl_font(FL_HELVETICA | FL_BOLD, 14);
				fl_push_clip(x, y, w, h);

				int top, left, bottom, right;
				get_selection(top, left, bottom, right);
				fl_draw_box(FL_THIN_UP_BOX, x, y, w, h, col_header_color());
				fl_color(FL_BLACK);

				fl_pop_clip();
				break;
			}
			case CONTEXT_CELL:
			{
				Fl_Color prev_col = fl_color();
				fl_push_clip(x, y, w, h);

				// draw the white background
				fl_color(row_selected(row) ? selection_color() : FL_WHITE);
				fl_rectf(x, y, w, h);

				// draw grey border
				fl_color(FL_GRAY);
				fl_line_style(FL_SOLID, 1);

				fl_rect(x, y, w, h);

				fl_color(FL_BLACK);
				fl_font(FL_HELVETICA, 14);

				const FrameManager::FrameRecord* frame = m_frame_manager.GetFrame(row);
				if(frame)
				{
					switch(col)
					{
							case FILENAME_ENUM:
							{
								fl_color(FL_BLACK);
								fl_draw(frame->GetSource().c_str(), x, y, w, h, FL_ALIGN_CENTER);
								break;
							}
							case ID_ENUM:
							{
								fl_color(FL_BLACK);
								fl_draw(frame->GetId().c_str(), x, y, w, h, FL_ALIGN_CENTER);
								break;
							}
							case MODIFIED_ENUM:
							{
								fl_color(FL_BLACK);

								std::string id = frame->GetId();
								if(!id.empty())
								{
									CinePaintDoc* doc = CinePaintApp::GetInstance().GetDocList().GetDocById(id.c_str());
									if(doc)
									{
										if(doc->IsDirty())
										{
											fl_draw("Yes", x, y, w, h, FL_ALIGN_CENTER);
										}
										else
										{
											fl_draw("No", x, y, w, h, FL_ALIGN_CENTER);
										}
									}
								}
								else
								{
									// Not Loaded
									fl_draw("N/L", x, y, w, h, FL_ALIGN_CENTER);
								}
								break;
							}
							case LOADED_ENUM:
							{
								fl_color(FL_BLACK);
								fl_draw(frame->GetLoaded() ? "Yes" : "No", x, y, w, h, FL_ALIGN_CENTER);
								break;
							}
							case PRE_LOADED_ENUM:
							{
								fl_color(FL_BLACK);
								fl_draw(frame->GetPreLoaded() ? "Yes" : "No", x, y, w, h, FL_ALIGN_CENTER);
								break;
							}
							case ACTIVE_ENUM:
							{
								if(frame->GetActive())
								{
									fl_draw_box(FL_THIN_UP_BOX, x +1, y +1, w -2, h -2, (row_selected(row) ? selection_color() : FL_WHITE));
									fl_color(FL_BLACK);
									fl_draw("Yes", x, y, w, h, FL_ALIGN_CENTER);
								}
								else
								{
									fl_draw_box(FL_THIN_DOWN_BOX, x +1, y +1, w -2, h -2, (row_selected(row) ? selection_color() : FL_WHITE));
									fl_color(FL_BLACK);
									fl_draw("No", x, y, w, h, FL_ALIGN_CENTER);
								}
								break;
							}
							default:
							break;
					}
				}

				fl_pop_clip();

				fl_color(prev_col);
				fl_line_style(0);
				break;
			}
			case CONTEXT_RC_RESIZE:
			case CONTEXT_STARTPAGE:
			default:
			{
				break;
			}
	}



}





FrameManagerWindow::FrameListener::FrameListener(FrameManagerWindow& win)
		: m_win(win)
{

}

void FrameManagerWindow::FrameListener::SelectionChanged(const FrameManager::FrameRecord* frame, size_t position)
{
	// update could come from an arbitary thread, we need to update the ui in the ui event loop
	CinePaintEventFunctor1<FrameManagerWindow, size_t>* functor = new CinePaintEventFunctor1<FrameManagerWindow, size_t>(&m_win, &FrameManagerWindow::SetSelected, position, false);
	CinePaintAppUI::GetInstance().GetUIThreadEventList().PostEvent(functor);
}

void FrameManagerWindow::FrameListener::FrameAdded(const FrameManager::FrameRecord& frame, size_t position)
{
	printf("Frame Added ---- %d\n", position);
	if(m_win.m_frame_view->rows() < int(m_win.m_frame_manager.size()))
	{
		printf("Frame Added adding table row ---- %d\n", position);
		m_win.m_frame_view->rows(int(m_win.m_frame_manager.size()));
	}
}

void FrameManagerWindow::FrameListener::FrameRemoved(const FrameManager::FrameRecord& frame, size_t position)
{
	printf("Frame Removed ---- %d\n", position);
	if((m_win.m_frame_manager.size() == 0) || (m_win.m_frame_view->rows() > int(m_win.m_frame_manager.size()) -1))
	{
		printf("Frame removing table row ---- %d\n", position);
		m_win.m_frame_view->rows(int(m_win.m_frame_manager.size()));
	}
}

void FrameManagerWindow::FrameListener::FrameUpdated(const FrameManager::FrameRecord& frame, size_t position)
{
	if(m_win.m_frame_view)
	{
		CinePaintEventFunctor0<FrameManagerWindow::FrameManagerTable>* functor = new CinePaintEventFunctor0<FrameManagerWindow::FrameManagerTable>(m_win.m_frame_view, &FrameManagerWindow::FrameManagerTable::redraw, false);
		CinePaintAppUI::GetInstance().GetUIThreadEventList().PostEvent(functor);
	}
}

void FrameManagerWindow::FrameListener::FrameIndexChanged(const FrameManager::FrameRecord& frame, size_t old_pos, size_t new_pos)
{
	if(m_win.m_frame_view)
	{
		CinePaintEventFunctor0<FrameManagerWindow::FrameManagerTable>* functor = new CinePaintEventFunctor0<FrameManagerWindow::FrameManagerTable>(m_win.m_frame_view, &FrameManagerWindow::FrameManagerTable::redraw, false);
		CinePaintAppUI::GetInstance().GetUIThreadEventList().PostEvent(functor);
	}
}



FrameManagerWindow::DisplayResizeHandler::DisplayResizeHandler(FrameStore& store)
		: m_frame_store(store)
{}

FrameManagerWindow::DisplayResizeHandler::~DisplayResizeHandler()
{}

void FrameManagerWindow::DisplayResizeHandler::WidgetResized(int w, int h)
{
	m_frame_store.SetRenderSize(w, h);
}
