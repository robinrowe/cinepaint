/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CPSpreadsheet - Spreadsheet for images
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Stuart Ford
 *   Colin Law
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * $Id: CPSpreadsheet.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CPSPREADSHEET_H_
#define _CPSPREADSHEET_H_

#include "dll_api.h"
#include <set>
#include <string>

class AbstractBuf;
class CPDAG;
class CPDAGData;
class CPDAGNode;

/**
 * CPSpreadsheet represents a \e spreadsheet interface into a CPDAG.
 * All spreadsheet type operations should be handled by this class, leaving the CPDAG
 * to manage generic DAG related method only. Most of the method within this class should act
 * as higher level interface mathods to the methods available within the CPDAG.
 *
 */
class CINEPAINT_GUI_API CPSpreadsheet
{
	public:
		//-------------------------
		// Constructor / Destructor

		/**
		 * Default constructor
		 */
		CPSpreadsheet();

		/**
		 * Destructor
		 */
		virtual ~CPSpreadsheet();




		//---------------------
		// Accessors / Mutators

		/**
		 * Sets the CPDAG this CPSpreadsheet acts as an interface to
		 *
		 * @param dag the CPDAG to interface to
		 */
		void SetDAG(CPDAG& dag);

		/**
		 * Returns the CPDAG this CPSpreadsheet interfaces to
		 *
		 * @return the CPDAG this CPSpreadsheet interface to.
		 */
		CPDAG* GetDAG();
		const CPDAG* GetDAG() const;



		//-----------------
		// Cell/Data Access

		/**
		 * Returns the data held in the specified cell of the spreadsheet.
		 * If the specified cell does not contain any data, 0 is returned
		 *
		 * @param row the row witihn the spreadsheet
		 * @param col the column within the spreadsheet
		 * @return the data held within the specified cell, or 0
		 */
		const CPDAGData* GetCellData(int row, int col);

		/**
		 * Returns the data held in the specified cell of the spreadsheet.
		 * If the specified cell does not contain any data, 0 is returned
		 * Thie method takes the row and column numbers as a single string
		 * representing the cell name
		 *
		 * @param cell the spreadsheet cell "rowcol"
		 * @return the data held within the specified cell, or 0
		 */
		const CPDAGData* GetCellData(const std::string& cell);

		/**
		 * Returns the graph node backing the spreadsheet at the specified spreadsheet cell location.
		 * If the specified cell does not contain any data, 0 is returned
		 *
		 * @param row the row witihn the spreadsheet
		 * @param col the column within the spreadsheet
		 * @return the graph node backing the spreadsheet at the specified cell, or 0
		 */
		const CPDAGNode* GetCellNode(int row, int col) const;




		//--------------------
		// Cell Data Modifiers

		/**
		 * Set the contents of a cell to an integer.
		 *
		 * @param row the row the cell is in
		 * @param col the column the cell is in
		 * @param data the integer to store in the cell
		 */
		void SetCell(int row, int col, int data);

		/**
		 * Set the contents of a cell to a floating point number.
		 *
		 * @param row the row the cell is in
		 * @param col the column the cell is in
		 * @param data the number to store in the cell
		 */
		void SetCell(int row, int col, float data);

		/**
		 * Set the contents of a cell to a string value
		 * The string value is copied by the graph structure backing the spreadsheet
		 *
		 * @param row the row the cell is in
		 * @param col the column the cell is in
		 * @param data the string to store in the cell
		 */
		void SetCell(int row, int col, const char* data);

		/**
		 * Set the contents of a cell to an AbstractBuf.
		 *
		 * @param row the row the cell is in
		 * @param col the column the cell is in
		 * @param data the image to store in the cell
		 */
		void SetCell(int row, int col, AbstractBuf* data);

		/**
		 * Set the contents of a cell to a plugin name.
		 * The plugin name is copied by the graph structure backing the spreadsheet
		 *
		 * @param row the row the cell is in
		 * @param col the column the cell is in
		 * @param plugin the name of the plugin to store in the cell
		 */
		void SetPluginCell(int row, int col, const char* plugin);





		//-------------
		// Cell Linking

		/**
		 * Connects two cells in the graph backing this spreadsheet.
		 * The connection of nodes joins the parameter input to a plugin, to the plugin that requires the input
		 * parameters.
		 * In order to correctly connect cells, the cells must exist. the create_missing flag may be used
		 * to automatically create parameter cell and populate them with default data. The default data used
		 * is a string representing the parameter name. Plugin cells are not automatically created.
		 *
		 * @param row the parameter cell row
		 * @param col the parameter cell col
		 * @param ref_row the plugin cell row
		 * @param abs_row indicates if the row is absolute
		 * @param ref_col the plugin cell col
		 * @param abs_col indicates if the column is absolute
		 * @param param the parameter name
		 * @param eval_name the evaluation name of the parameter value within the parameter cell
		 * @param create_missing set true to create automatically missing cells
		 */
		void Reference(int row, int col, int ref_row, bool abs_row, int ref_col, bool abs_col, const std::string& param, const std::string& eval_name, bool create_missing);

		/**
		 * Connects two cells in the graph backing this spreadsheet.
		 * The connection of nodes joins the parameter input to a plugin, to the plugin that requires the input
		 * parameters.
		 * In order to correctly connect cells, the cells must exist. the create_missing flag may be used
		 * to automatically create parameter cell and populate them with default data. The default data used
		 * is a string representing the parameter name. Plugin cells are not automatically created.
		 *
		 * @param cell the parameter cell
		 * @param ref_cell the plugin cell
		 * @param abs_row indicates if the row is absolute
		 * @param abs_col indicates if the column is absolute
		 * @param param the parameter name
		 * @param eval_name the evaluation name of the parameter value within the parameter cell
		 * @param create_missing set true to create automatically missing cells
		 */
		void Reference(const std::string& cell, const std::string& ref_cell, bool abs_row, bool abs_col, const std::string& param, const std::string& eval_name, bool create_missing);


		/**
		 * Disconnects two cells in the graph backing this spreadsheet.
		 *
		 * @see Reference(int, int, int, bool, int, bool, const std::string&, const std::string&, bool);
		 * @param row the parameter cell row
		 * @param col the parameter cell col
		 * @param ref_row the plugin cell row
		 * @param abs_row indicates if the row is absolute
		 * @param ref_col the plugin cell col
		 * @param abs_col indicates if the column is absolute
		 * @param param the parameter name
		 * @param eval_name the evaluation name of the parameter value within the parameter cell
		 */
		void UnReference(int row, int col, int ref_row, bool abs_row, int ref_col, bool abs_col, const std::string& param, const std::string& eval_name);

		/**
		 * Disconnects two cells in the graph backing this spreadsheet.
		 *
		 * @see Reference(const std::string&, const std::string&, bool, bool, const std::string&, const std::string&, bool);
		 * @param cell the parameter cell
		 * @param ref_cell the plugin cell
		 * @param abs_row indicates if the row is absolute
		 * @param abs_col indicates if the column is absolute
		 * @param param the parameter name
		 * @param eval_name the evaluation name of the parameter value within the parameter cell
		 */
		void UnReference(const std::string& cell, const std::string& ref_cell, bool abs_row, bool abs_col, const std::string& param, const std::string& eval_name);


		//---------------
		// Data Selection

		/**
		 * Select a single cell.
		 *
		 * @param row the row the cell is in
		 * @param col the column the cell is in
		 */
		void Select(int row, int col);

		/**
		 * Select a block of cells. A block is defined by 2 cells in opposite
		 * corners of the block. Either pair of opposing corners is allowed, and the
		 * corners may be in any order. Note that the ordering of corners is
		 * significant in some operations, filling for example.
		 *
		 * @param row1 the row of the the first selected cell
		 * @param col1 the column of the first selected cell
		 * @param row2 the row of the last selected cell
		 * @param col2 the column of the last selected cell
		 */
		void Select(int row1, int col1, int row2, int col2);

		/**
		 * Select an entire row of cells.
		 *
		 * @param row the row to select
		 */
		void SelectRow(int row);

		/**
		 * Select a number of contiguous rows of cells.
		 *
		 * @param row1 the first row in the selection
		 * @param row2 the last row in the selection
		 */
		void SelectRows(int row1, int row2);

		/**
		 * Select an entire column of cells.
		 *
		 * @param col the column to select
		 */
		void SelectCol(int col);

		/**
		 * Select a number of contiguous columns of cells.
		 *
		 * @param col1 the first column in the selection
		 * @param col2 the last column in the selection
		 */
		void SelectCols(int col1, int col2);






		//--------------------
		// Spreadsheet actions

		// @TODO implement these methods

		/**
		 * Erases the contents of the current selection.
		 *
		 * @return the number of cells erased
		 */
		//int Erase();

		/**
		 * Erases the contents of a single cell.
		 *
		 * @param row the row number of the cell to erase
		 * @param col the column number of the cell to erase
		 * @return the number of cells erased
		 */
		int Erase(int row, int col);

		/**
		 * Cut the contents of the current selection and put them in the clipboard selection.
		 *
		 * @return the number of cells cut
		 */
		//int Cut();

		/**
		 * Cut the contents of the specified cell and add it to the clipboard selection.
		 *
		 * @return the number of cells cut
		 */
		//int Cut(int row, int col);

		/**
		 * Copy the contents of the current selection to the clipboard selection.
		 *
		 * @return the number of cells copied
		 */
		//int Copy();

		/**
		 * Copy the contents of the specified cell and add it to the clipboard selection.
		 *
		 * @return the number of cells copied
		 */
		//int Copy(int row, int col);

		/**
		 * Paste the contents of the clipboard selection to the current selection.
		 *
		 * @return the number of cells pasted
		 */
		//void Paste();

		/**
		 * Paste the contents of the clipboard selection into the specified cell.
		 *
		 * @return the number of cells pasted
		 */
		//void Paste(int row, int col);






		//----------------------------
		// Display Convenience Methods

		/**
		 * Rebuilds the complete plugin string from the data contained within the specified cell.
		 * This method checks the data type contained within the specified cell and if it holds
		 * a plugin, it rebuilds a complete displayable string based upon the data held within
		 * the cell. This includes the plugin name, the parameters into the plugin - and the 
		 * connected cells that parameters come from, and and evaluation names for parameters.
		 *
		 * @param row the row witihn the spreadsheet
		 * @param col the column within the spreadsheet
		 * @return the displayable plugin string, or an empty string if the cell does not contain a plugin
		 */
		std::string RebuildPluginString(int row, int col) const;

		/**
		 * Returns a string representation of the cell name from its spreadsheet position
		 *
		 * @param row the row of the cell
		 * @param col the column of the cell
		 * @return string representaion of the cell name
		 */
		static std::string CellName(int row, int col);

		/**
		 * Returns a string representation of the cell range from its spreadsheet position
		 *
		 * @param start_row the start row of the range
		 * @param start_col the start column of the range
		 * @param end_row the end row of the range
		 * @param end_col the end column of the range
		 * @return string representaion of the cell range
		 */
		static std::string CellRange(int start_row, int start_col, int end_row, int end_col);

		/**
		 * Writes the column name for the specified column position into the given buffer
		 * The column name represents the displayable name of the column, this is usually
		 * a letter representation 0 = A, 1 = B.
		 * The size parameter is used to control how many characters are actually written
		 * and is the minimum of the buffer length, or size
		 *
		 * @param col the column number, 0 offset
		 * @return the column name
		 */
		static std::string ColName(int col);

		/**
		 * Writes the row name for the specified column position into the given buffer
		 * The row name represents the displayable name of the column, this is usually
		 * a number representation. The row name is offset by 1.
		 *
		 * @param col the row number, 0 offset
		 * @return the row name
		 */
		static std::string RowName(int row);

		/**
		 * Returns the column of a cell from the cell name.
		 *
		 * @param name the cell name
		 * @return the column of the cell
		 */
		static int ColumnFromName(const std::string& name);

		/**
		 * Returns the row of a cell from the cell name.
		 *
		 * @param name the cell name
		 * @return the row of the cell
		 */
		static int RowFromName(const std::string& name);

	protected:

		/**
		 * Type defining a selection of cells
		 */
		struct Selection
		{
			int row1, row2;
			int col1, col2;

			void set(int r1, int c1, int r2, int c2)
			{
				row1 = r1;
				col1 = c1;
				row2 = r2;
				col2 =c2;
			}

			void clear()
			{
				row1 = row2 = col1 = col2 = -1;
			}

		}; // struct Selection



	private:

		/**
		 * Update the spreadsheet by re-evaluating all dirty nodes.
		 */
		void update();






		/** The underlying graph structure of the spreadsheet */
		CPDAG *m_graph;

		/** The currently selected group of cells */
		// @TODO this is currently handled in the Fl_Table, although this might be a good place
		//       to handle it instead, we also want to allow for multiple selections
		Selection m_selection;

		/** The group of cells selected in a copying/cutting operation */
		// @TODO copy, cut & paste
		Selection m_clipboard;



		//----------
		// Constants

		/** Separator between column and row numbers in link names */
		static const char* mc_LinkSeparator;

		/** Separator between row and column numbers in cell names */
		static const char* mc_CellSeparator;

}; // class CPSpreadsheet


#endif // _CPSPREADSHEET_H_
