/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintWindow - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintWindow.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include <gui/CinePaintWindow.h>

#include <FL/Fl.H>

/**
 * Constructs a new Window of the specified size.
 *
 * @param x
 * @param y
 * @param title
 */
CinePaintWindow::CinePaintWindow(int x, int y, const char* title)
: Fl_Window(x, y, title)
{
	this->callback(window_close_callback, this);
	m_delete_on_close = false;
}

/**
 * Destructor
 */
CinePaintWindow::~CinePaintWindow()
{}

void CinePaintWindow::SetDeleteOnClose(bool delete_on_close)
{
	m_delete_on_close = delete_on_close;
}

bool CinePaintWindow::GetDeleteOnClose() const
{
	return(m_delete_on_close);
}

/**
 * Shows this window and waits until the window is hidden again.
 * The return value is used to indicates the close state of this window. For a
 * 'dialog', the return may be used to indicate the choosen option, for example,
 * OK or Cancel. For a plain window, this may simply be set to NONE_ENUM.
 *
 * @return DialogResponse indicating the
 */
DialogResponse CinePaintWindow::ShowAndWait()
{
	this->set_modal();
	this->hotspot(this);
	this->show();
    
	while(this->shown())
	{
		Fl::wait();
    }

	return(m_window_response);
}

void CinePaintWindow::SetResponse(DialogResponse response)
{
	m_window_response = response;
}

DialogResponse CinePaintWindow::GetResponse() const
{
	return(m_window_response);
}

void CinePaintWindow::window_close_callback(Fl_Widget* window, void* data)
{
	CinePaintWindow* _window = reinterpret_cast<CinePaintWindow*>(window);
	CinePaintWindow* _cine_paint_window = reinterpret_cast<CinePaintWindow*>(data);

	_window->hide();

	if(_cine_paint_window->m_delete_on_close)
	{
		Fl::add_idle(window_close_idle_callback, _window);
	}
}

void CinePaintWindow::window_close_idle_callback(void* data)
{
	CinePaintWindow* _window = reinterpret_cast<CinePaintWindow*>(data);

	delete _window;
	_window = 0;
}
