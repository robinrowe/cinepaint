/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      InstallDialog - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: InstallDialog.h,v 1.2 2006/12/18 08:21:17 robinrowe Exp $
 */

#ifndef _INSTALL_DIALOG_H_
#define _INSTALL_DIALOG_H_

#include "dll_api.h"
#include "plug-ins/pdb/enums.h"

class Fl_Button;
class Fl_Text_Buffer;
class Fl_Text_Display;
class Fl_Widget;
class Fl_Window;

/**
 * Dialog box to run user install
 **/
class CINEPAINT_GUI_API InstallDialog
{

public:
	InstallDialog(const char* dotdir)
	:	dotdir(dotdir)
	{}
	~InstallDialog()
	{}
	
	/**
	 * Create the DialogBox.
	 * @return user selection, install or not.
	**/
	DialogResponse InstallDialogDo();

	/**
	 * Show the Installation Log message
	 **/
	DialogResponse InstallLogDo(const char* );
private:



	/**
	 * Sets the style for text in the style buffer: Used to generate styled output for
	 * Install Info display. 
	 * @param stylebuf Buffer which will get the style info.
	 * @param style The Style to use for this bit of text.
	 * @param start The first character to format with {@link}style
	 * @param end The last character to format with {@link}style.
	**/
	void set_style(Fl_Text_Buffer *stylebuf , char style, int start, int end);

	/** 
	 * Function to insert desired text into display.
	 * @param buf Buffer to hold the actual text.
	 * @param styleBuf Buffer to hold the style information.
	**/
	void set_text(Fl_Text_Buffer *buf, Fl_Text_Buffer *styleBuf);

	/****************************
	* Callback functions for Install dialog buttons.
	* Each callback requires a static and non-static pair so that the callback
	* can be assigned to the button and still have access to the class members.
	*/
	/**
	 * Callback for the Install button on the Install dialog.
	**/
	static void install_install_button_static_callback(Fl_Widget *w, void *d) {((InstallDialog *)d)->install_install_button_callback(w);};
	void install_install_button_callback(Fl_Widget *w);

	/**
	 * Callback for the Ignore button on the Install dialog.
	**/
	static void install_ignore_button_static_callback(Fl_Widget *w, void *d) {((InstallDialog *)d)->install_ignore_button_callback(w);};
	void install_ignore_button_callback(Fl_Widget *w);

	/**
	 * Callback for the Quit button on the Install dialog.
	**/
	static void install_quit_button_static_callback(Fl_Widget *w, void *d) {((InstallDialog *)d)->install_quit_button_callback(w);};
	void install_quit_button_callback(Fl_Widget *w);

	/**
	 * Perform the actual install.
	 *
	 **/
	void install_run();

	// Callbacks for Install done dialog buttons.
	static void install_done_continue_static_callback(Fl_Widget *w, void *d) {((InstallDialog *)d)->install_done_continue_callback(w);};
	void install_done_continue_callback(Fl_Widget *w);
	static void install_done_quit_static_callback(Fl_Widget *w, void *d) {((InstallDialog *)d)->install_done_quit_callback(w);};
	void install_done_quit_callback(Fl_Widget *w);

	/** 
	 * Private member fields for class variables.
	**/
	Fl_Window *install_help_dialog;
	Fl_Window *install_dialog;

	Fl_Button *install_install_button;
	Fl_Button *install_ignore_button;
	Fl_Button *install_quit_button;

	Fl_Text_Display *install_msg;
		
	Fl_Window *install_done_dialog;			/// FL_Window variable for user install done message
	Fl_Button *install_done_cont_button;    /// Continue Button for user install done.
	Fl_Button *install_done_quit_button;    /// Quit Button for user install done.
	
	DialogResponse m_response;
	const char* dotdir;
};

#endif // _INSTALL_DIALOG_H_
