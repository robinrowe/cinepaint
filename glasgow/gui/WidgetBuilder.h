/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      WidgetBuilder - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: WidgetBuilder.h,v 1.2 2006/12/18 08:21:20 robinrowe Exp $
 */


#ifndef _WIDGET_BUILDER_H_
#define _WIDGET_BUILDER_H_

#include "dll_api.h"
#include "plug-ins/pdb/CPWidgetBuilder.h"

class CPPluginUIWidget;
struct CPPluginUIDef;
class CPPluginUIDialog;
class CPPluginUINumber;
class CPPluginUIBool;
class CPPluginUIList;
class CPPluginUIText;

#include <FL/Fl_Window.H>
#include <FL/Fl_Slider.H>
#include <FL/Fl_Round_Button.H>

#include <list>
/**
 * FLTK Implementation to Construct Plugin Dialogs and UI's
 * from list given by Plugin.
**/
class CINEPAINT_GUI_API WidgetBuilder  : public CPWidgetBuilder
{

	
private:
	// List to hold created Widgets.
	typedef std::list<Fl_Widget *> widget_list_t;

public:
	WidgetBuilder() {};

	virtual ~WidgetBuilder() {};

	DialogResponse DoModalDialog(CPPluginUIDef &ui_def, CPParamList &params);

	Fl_Window *BuildDialogFLTK(CPPluginUIDef &ui_def, CPParamList &params);

	Fl_Group *BuildGroupFLTK(CPPluginUIDef &ui_def, CPParamList &params);
	Fl_Group *BuildGroupFLTK(CPPluginUIDef &ui_def, CPParamList &params, widget_list_t &_lst);

private:

	Fl_Widget *CreateWidget(CPPluginUIDialog *, CPParamList &params);
	Fl_Widget *CreateWidget(CPPluginUINumber *wdgt, CPParamList &params);
	Fl_Widget *CreateWidget(CPPluginUIBool *wdgt, CPParamList &params);
	Fl_Widget *CreateWidget(CPPluginUIList *wdgt, CPParamList &params);
	Fl_Widget *CreateWidget(CPPluginUIText *wdgt, CPParamList &params);

	void layout( Fl_Group *_grp, widget_list_t &lst );

	// Callbacks for OK and CANCEL buttons in a dialog.
	static void cancel_callback(Fl_Widget* w, void* data);
	static void ok_callback(Fl_Widget* w, void* data);

	// Callbacls for widgets in main group
	static void valuator_callback(Fl_Widget* w, void* data);
	static void button_callback(Fl_Widget* w, void* data);
	static void list_callback(Fl_Widget* w, void* data);
	static void radio_callback(Fl_Widget* w, void* data);
	// @TODO need to think about where this really goes
	static void choice_callback(Fl_Widget* w, void* data);

	// Data type passed to callback.
	struct callback_struct_t
	{
		char* param_name;
		CPParamList *params;
	};
};


#endif //#ifndef _CP_WIDGET_BUILDER_H_
