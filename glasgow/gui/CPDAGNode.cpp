/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CPDAGNode - Directed acyclic graph node
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Stuart Ford
 *   Colin Law
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * $Id: CPDAGNode.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "CPDAGNode.h"
#include "plug-ins/pdb/CinePaintImage.h"
#include "app/CinePaintApp.h"
#include "app/CinePaintDocList.h"
#include "utility/CPSocket.h"
#include "plug-ins/pdb/PluginParam.h"
#include "CPDAGData.h"
#include <iostream>
#include <map>
#include <string>

const std::string CPDAGNode::DEFAULT_EVALUATION_NAME = "__DEFAULT_EVAULATION";

/**
 * Protected Default constructor.
 * Generally, Nodes should only be created by a controlling CPDAG.
 *
 */
CPDAGNode::CPDAGNode()
{
	m_data = new CPDAGData();
	m_default_evaluation.clear();
	m_dirty = false;
}

/**
 * Destructor.
 */
CPDAGNode::~CPDAGNode()
{
	if(m_data)
	{
		delete m_data;
		m_data = 0;
	}
}


//---------------------
// Accessors / Mutators

/**
 * Returns the data held by this Node
 *
 * @return the data held by this Node
 */
const CPDAGData& CPDAGNode::GetData() const
{
	return(*m_data);
}

/**
 * Returns the name of the default evaluation held by this Node
 *
 * @return the name of the default evaluation held by this Node
 */
const std::string& CPDAGNode::GetDefaultEvaluationName() const
{
	return(m_default_evaluation);
}

/**
 * Returns the dfault evaluation held by this Node
 * If there is no default evaluation, 0 is returned.
 * Althouh there may be no default evaluation, this does not mean that there
 * are no evalautions. The default evaluation name may simply not map to an
 * evaluated value
 *
 * @return the default evaluation held by this Node
 */
const PluginParam* CPDAGNode::GetEvaluation() const
{
	const PluginParam* param = 0;

	if(!m_default_evaluation.empty())
	{
		EvalulationContainer_t::const_iterator citer = m_evaulations.find(m_default_evaluation);
		if(citer != m_evaulations.end())
		{
			param = citer->second;
		}
	}

	return(param);
}

/**
 * Returns the named evaluation held by this node
 * If the named evaluation does not exist, 0 is returned
 *
 * @param eval_name the name of the evaluation value to return
 * @return the named evaluation held by this Node, or 0 is the named evaluation
 *         does not exist
 */
const PluginParam* CPDAGNode::GetEvaluation(const std::string& eval_name) const
{
	const PluginParam* param = 0;

	EvalulationContainer_t::const_iterator citer = m_evaulations.find(eval_name);

	if(citer != m_evaulations.end())
	{
		param = citer->second;
	}

	return(param);
}


/**
 * Returns whether this CPDAGNode is dirty.
 * A dirty CPDAGData is one which has had data, or an evaluation changed, but has not been
 * evaluated. This value is controlled by the parent CPDAG during evaluation
 *
 * @return true if this CPDAGNode is dirty, false otherwise
 */
bool CPDAGNode::IsDirty() const
{
	return(m_dirty);
}

/**
 * Serialize and write a node to a given socket.
 * @param socket	[i] the socket to write the node to
 */
void CPDAGNode::Serialize(CPSocket& socket) const
{
	// vector<string>::iterator it;
	//
	// socket << m_indegree;
	// socket << m_returnParam;
	// socket << m_returnType;
	// socket << m_dirty;
	//
	// socket << m_neighbours.size();
	// for (it = m_neighbours.begin(); it != m_neighbours.end(); ++it)
	// {
	// socket << it->first;
	// socket << it->second;
	// }
	//
	// if (m_data != NULL)
	// {
	// m_data->Serialize(socket);
	// }
	//
	// if (!m_dirty)
	// {
	// socket << m_evaluation.type;
	// switch (m_evaluation.type)
	// {
	//
	// case PDB_INT :
	// socket << m_evaluation.value.pdb_int;
	// break;
	//
	// case PDB_FLOAT :
	// socket << m_evaluation.value.pdb_float;
	// break;
	//
	// case PDB_CSTRING :
	// socket << m_evaluation.value.pdb_cstring;
	// break;
	//
	// case PDB_ABSTRACTBUF :
	// m_evaluation.value.pdb_abstractbuf->Serialize(socket);
	// break;
	//
	// }
	// }
}

/**
 * Read and deserialize a node from a given socket.
 * @param socket	[i] the socket to read the node from
 */
void CPDAGNode::Deserialize(CPSocket& socket)
{
	// int i, numNeighbours;
	// string node, param;
	//
	// socket >> m_indegree;
	// socket >> m_returnParam;
	// socket >> m_returnType;
	// socket >> m_dirty;
	//
	// socket >> numNeighbours;
	// for (i = 0; i < numNeighbours; ++i)
	// {
	// socket >> node;
	// socket >> param;
	// m_neighbours[node] = param;
	// }
	//
	// m_data = new CPDAGData(socket);
	//
	// if (!m_dirty)
	// {
	// socket >> m_evaluation.type;
	// switch (m_evaluation.type)
	// {
	//
	// case PDB_INT :
	// socket >> m_evaluation.value.pdb_int;
	// break;
	//
	// case PDB_FLOAT :
	// socket >> m_evaluation.value.pdb_float;
	// break;
	//
	// case PDB_CSTRING :
	// socket >> m_evaluation.value.pdb_cstring;
	// break;
	//
	// case PDB_ABSTRACTBUF :
	// m_evaluation.value.pdb_abstractbuf->Deserialize(socket);
	// break;
	//
	// }
	// }

}


/**
 * Returns the data held by this Node
 *
 * @return the data held by this node
 */
CPDAGData& CPDAGNode::get_data()
{
	return(*m_data);
}


// attachment handling

/**
 * Attach a node to this one.
 * The attach direction is from this Node to the specified node one.
 * A node cannot be attached to itself.
 *
 * Two names are associated with the attachment, the parameter name and the evaluation name.
 * the parameter name represents the name of the parameter the attached node is providing
 * to an evaluation of a plugin. Dring the evaluation of a node, there may be multiple values
 * returned. These values represent the evaluations of the node. The eval name represents the
 * od the evluatioed value that the attached node is providing.
 *
 * @param node the node to attach
 * @param param the parameter name,
 * @param eval the evaluation name of the parameter linking from the specified node
 * @see #CPDAG::Attach
 */
void CPDAGNode::attach(const std::string& node, const std::string& param, const std::string& eval)
{
	m_neighbours.insert(std::pair<std::string, std::pair<std::string, std::string> >(node, std::pair<std::string, std::string>(param, eval)));
	increment_indegree();
}

/**
 * Detach the named node from this node
 * If the specified node is not attached to this node, no action is taken and the
 * detach is considered successful.
 *
 * @param node the node to detach
 */
void CPDAGNode::detach(const std::string& node, const std::string& param, const std::string& eval)
{
	std::pair<CPDAGNode::neightbour_iterator, CPDAGNode::neightbour_iterator> nr = neighbours_range(node);
	while(nr.first != nr.second)
	{
		CPDAGNode::neightbour_iterator next = nr.first;
		next++;

		// check the param and eval names match the node
		// range pair . start of range -> param-eval pair in neighbouring nodes . param/eval
		if(nr.first->second.first == param && nr.first->second.second == eval)
		{
			m_neighbours.erase(nr.first);
			decrement_indegree();
		}

		nr.first = next;
	}
}

/**
 * Completely detach the named node from this node
 * If the specified node is not attached to this node, no action is taken and the
 * detach is considered successful.
 *
 * @param node the node to detach
 */
void CPDAGNode::detach(const std::string& node)
{
	std::pair<CPDAGNode::neightbour_iterator, CPDAGNode::neightbour_iterator> nr = neighbours_range(node);
	while(nr.first != nr.second)
	{
		CPDAGNode::neightbour_iterator next = nr.first;
		next++;

		m_neighbours.erase(nr.first);
		decrement_indegree();

		nr.first = next;
	}
}

/**
 * Detach all nodes from this node.
 *
 */
void CPDAGNode::detach_all()
{
	m_neighbours.clear();
	set_indegree(0);
}


// neighbour handling

/**
 * Returns an iterator pointing to the specified attched node
 * If the specified node is not attached to this node, the iterator will be
 * equal to neighbours_end()
 * a node may be attached multiple times, see neighbours_range
 *
 * @param node_name the name of the attched node
 * @return an iterator pointing the attched node
 */
CPDAGNode::neightbour_iterator CPDAGNode::find_neighbour(const std::string& node_name)
{
	return(m_neighbours.find(node_name));
}

/**
 * Returns an iterator pointing to the beginning of the neighbouring nodes
 *
 * @return an iterator pointing to the beginning of the neighbouring nodes
 */
CPDAGNode::neightbour_iterator CPDAGNode::neighbours_begin()
{
	return(m_neighbours.begin());
}

/**
 * Returns an iterator pointing past the end of the neighbouring nodes
 *
 * @return an iterator pointing past the end of the neighbouring nodes
 */
CPDAGNode::neightbour_iterator CPDAGNode::neighbours_end()
{
	return(m_neighbours.end());
}

/**
 * Returns a pair of iterators representing the start and end of nodes attahced by the specified name
 * A node may be attached several times to this node, for example if a particular node provides
 * multiple parameters to this node, therefore the attached node name may not be unique.
 * This method returns a pair of iterators representing the start and end range of the
 * named node within the neighbouring nodes.
 *
 * @param node_name the attached node name
 * @return pair of iterators representing the start and end range
 */
std::pair<CPDAGNode::neightbour_iterator, CPDAGNode::neightbour_iterator> CPDAGNode::neighbours_range(const std::string& node_name)
{
	return(m_neighbours.equal_range(node_name));
}


// evaluation handling

/**
 * Sets the name of default evaluation value to the specified name
 *
 * @param name the name of the default evaluation
 */
void CPDAGNode::set_default_evaluation(const std::string& name)
{
	m_default_evaluation = name;
}

/**
 * Adds the specified value to the list of evaluation values held by this node.
 * 
 * @param eval_name the name ampping to the evaluation value
 * @param param the evaluation value to set
 */
bool CPDAGNode::set_evalulation(const std::string& eval_name, PluginParam* param)
{
	if(m_evaulations.find(eval_name) != m_evaulations.end())
	{
		printf("CPDAGNode::set_evalulation:: param %s already set\n", eval_name.c_str());
		return(false);
	}

	switch(param->GetType())
	{
			case PDB_INT:
			case PDB_FLOAT:
			case PDB_CSTRING:
			case PDB_DRAWABLE:
			case PDB_VOIDPOINTER:
			{
				break;
			}
			case PDB_CPIMAGE:
			{
				param->SetManaged(false);
				CinePaintApp::GetInstance().GetDocList().RefDocument(*(param->GetArg().pdb_cpimage));
				break;
			}
			default:
			{
				break;
			}
	}

	m_evaulations.insert(std::pair<std::string, PluginParam*>(eval_name, param));
	return(true);
}

/**
 * Returns the named evaluation held by this node
 * If the named evaluation does not exist, 0 is returned
 *
 * @param eval_name the name of the evaluation value to return
 * @return the named evaluation held by this Node, or 0 is the named evaluation
 *         does not exist
 */
PluginParam* CPDAGNode::get_evaluation(const std::string& eval_name)
{
	PluginParam* param = 0;

	EvalulationContainer_t::const_iterator citer = m_evaulations.find(eval_name);

	if(citer != m_evaulations.end())
	{
		param = citer->second;
	}

	return(param);
}

/**
 * Clears the evaluations list
 *
 */
void CPDAGNode::clear_evaluations()
{
	EvalulationContainer_t::iterator iter = m_evaulations.begin();
	while(iter != m_evaulations.end())
	{
		EvalulationContainer_t::iterator next = iter;
		++next;

		PluginParam* param = iter->second;
		iter->second = 0;

		switch(param->GetType())
		{
				case PDB_INT:
				case PDB_FLOAT:
				case PDB_CSTRING:
				case PDB_DRAWABLE:
				case PDB_VOIDPOINTER:
				{
					break;
				}
				case PDB_CPIMAGE:
				{
					param->SetManaged(false);
					CinePaintApp::GetInstance().GetDocList().UnRefDocument(*(param->GetArg().pdb_cpimage));
					break;
				}
				default:
				{
					break;
				}
		}

		delete param;
		param = 0;

		m_evaulations.erase(iter);

		iter = next;
	}
}

/**
 * Returns an interator at the beginning of the evaluation values held by this Node
 *
 * @return an iterator at the beginning of the evaluation calues held by this node
 */
CPDAGNode::eval_iterator CPDAGNode::evaulations_begin()
{
	return(m_evaulations.begin());
}

/**
 * Returns an interator past the end of the evaluation values held by this Node
 *
 * @return an iterator past the end of the evaluation calues held by this node
 */
CPDAGNode::eval_iterator CPDAGNode::evaulations_end()
{
	return(m_evaulations.end());
}




// indegree handling


/**
 *  Sets the indegree of this Node
 * The indegree is the count of the number of paths into this node
 * i.e. the number of nodes that connect to this one.
 *
 * @param i the nre indegree of this Node
 */
void CPDAGNode::set_indegree(int i)
{
	m_indegree = i;
}

/**
 * Increments the indegree of this node by one
 *
 */
void CPDAGNode::increment_indegree()
{
	m_indegree++;
}

/**
 * decremenets the indegree of this node by one
 *
 */
void CPDAGNode::decrement_indegree()
{
	m_indegree--;
}

/**
 * Returns the indegree of this node
 *
 * @return the indegree of this node
 */
int CPDAGNode::get_indegree() const
{
	return(m_indegree);
}

/**
 * Sets the dirty state of this Node
 * A dirty CPDAGData is one which has had data, or an evaluation changed, but has not been
 * evaluated. This value is controlled by the parent CPDAG during evaluation
 *
 * @param dirty the new dirty state.
 */
void CPDAGNode::set_dirty(bool dirty)
{
	m_dirty = dirty;
}
