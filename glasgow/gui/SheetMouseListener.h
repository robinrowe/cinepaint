/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      SheetView - Spreadsheet for images ui widget
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: SheetMouseListener.h,v 1.2 2006/12/21 11:18:07 robinrowe Exp $
 */

#ifndef _SHEET_MOUSE_LISTENER_H_
#define _SHEET_MOUSE_LISTENER_H_

#include "dll_api.h"
#include "plug-ins/pdb/CinePaintMouseListener.h"
#include "plug-ins/pdb/World.h"

class CPSpreadsheet;
class SheetView;

/**
 * Mouse Listener implementation to handle mouse events from a SheetView.
 *
 */
class CINEPAINT_GUI_API SheetMouseListener : public CinePaintMouseListener
{	public:
		SheetMouseListener(SheetView& view, CPSpreadsheet& spreadsheet);
		virtual ~SheetMouseListener();

		virtual void MouseEntered();
		virtual void MouseExited();
		virtual void MousePressed(int button, int count, int x, int y, unsigned int modifier);
		virtual void MouseReleased(int button, int x, int y);
		virtual void MouseDragged(int button, int x, int y);
		virtual void MouseMoved(int x, int y);

	protected:

	private:

		/** the SheetView this mouse listener is listening to for events */
		SheetView& m_view;
		
		/** CPSpreadsheet interface to the DAG */
		CPSpreadsheet& m_spreadsheet;
};
		
		
#endif /* _SHEET_MOUSE_LISTENER_H_ */
