/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      FlipFrameDisplayHandler - FlipFrame display update handling
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: FlipFrameDisplayHandler.cpp,v 1.2 2006/12/21 11:18:06 robinrowe Exp $
 */

#include "FlipFrameDisplayHandler.h"
#include "CinePaintAppUI.h"
#include "SimpleDisplayWindow.h"
#include <plug-ins/pdb/AbstractRenderer.h>
#include <plug-ins/pdb/CinePaintImage.h>
#include <plug-ins/pdb/RenderingManager.h>
#include <app/CinePaintApp.h>
#include <app/CinePaintDocList.h>
#include <app/CinePaintEventList.h>
#include <app/CinePaintEventFunctor.h>
#include <app/FrameStore.h>
#include <app/FlipFrameController.h>
#include <plug-ins/pdb/CinePaintRenderOp.h>


FlipFrameDisplayHandler::FlipFrameDisplayHandler(FrameStore& store, SimpleDisplayWindow& display, FlipFrameController& controller)
		: m_frame_store(store), m_display(display), m_controller(controller)
{
	m_curr_image.m_image = 0;
	m_curr_image.m_frame = 0;

	m_pause_on_refill_buffer = true;
}

FlipFrameDisplayHandler::~FlipFrameDisplayHandler()
{}

void FlipFrameDisplayHandler::SetDropMissingFrames(bool yn)
{
	m_pause_on_refill_buffer = yn;
}



//--------------------------------------
// FrameManagerListener implementations

void FlipFrameDisplayHandler::SelectionChanged(const FrameManager::FrameRecord* frame, size_t position)
{
	// handle the selection change within the UI thread

	CinePaintEventFunctor1<FlipFrameDisplayHandler, const FrameManager::FrameRecord*>* functor = new CinePaintEventFunctor1<FlipFrameDisplayHandler, const FrameManager::FrameRecord*>(this, &FlipFrameDisplayHandler::process_selection_change, frame, false);
	CinePaintAppUI::GetInstance().GetUIThreadEventList().PostEvent(functor);
}

void FlipFrameDisplayHandler::FrameAdded(const FrameManager::FrameRecord& frame, size_t position)
{
	// ignored
}

void FlipFrameDisplayHandler::FrameRemoved(const FrameManager::FrameRecord& frame, size_t position)
{
	if(&frame == m_curr_image.m_frame)
	{
		process_selection_change(0);
	}
}

void FlipFrameDisplayHandler::FrameUpdated(const FrameManager::FrameRecord& frame, size_t position)
{
	// ignored
}

void FlipFrameDisplayHandler::FrameIndexChanged(const FrameManager::FrameRecord& frame, size_t old_pos, size_t new_pos)
{
	// ignored
}




bool FlipFrameDisplayHandler::maybe_drop_doc(CinePaintImage& image)
{
	bool ret = false;

	// only drop references to unmodified images to avoid unreffing the last remaining
	// reference to a modified image, we do want to close it without prompting the user,
	// and we dont want a prompt during a possible playback

	if(!image.IsDirty())
	{
		// @TODO this needs some thought...
		// if we previously registered the image, and it has since been saved,
		// unregster the reference
		m_controller.UnRegisterModifiedDocument(image);

		CinePaintApp::GetInstance().GetDocList().UnRefDocument(image);

		ret = true;
	}
	else
	{
		if(!m_controller.RegisterModifiedDocument(image))
		{
			// duplicate reference, so drop this one
			CinePaintApp::GetInstance().GetDocList().UnRefDocument(image);
		}
		else
		{
			printf("Not Dropping reference to modified document\n");
		}
	}

	return(ret);
}

void FlipFrameDisplayHandler::process_selection_change(const FrameManager::FrameRecord* frame)
{
	if(frame)
	{
		CinePaintImage* prev_image = m_curr_image.m_image;

		// get the selected frame
		m_curr_image.m_frame = frame;
		m_curr_image.m_image = m_frame_store.GetFrame(*frame);

		if(m_curr_image.m_image)
		{
			AbstractRenderer* renderer = m_curr_image.m_image->GetRenderingManager().GetRenderer(FrameStore::FLIP_FRAME_RENDER_NAME);

			if(renderer)
			{
				{
					m_display.SetBuffer(&renderer->GetRender());
				}
			}
		}
		else
		{
			m_display.SetBuffer(0);

			if(m_pause_on_refill_buffer)
			{
				printf("Need to buffer - calling BufferAndPlay \n");
				m_controller.BufferAndPlay(m_controller.GetDirection());
			}
			else
			{
				printf("Dropped Frame %s %s\n", frame->GetSource().c_str(), frame->GetId().c_str());
			}
		}

		if(prev_image)
		{
			maybe_drop_doc(*prev_image);
			prev_image = 0;
		}
	}
	else
	{
		m_display.SetBuffer(0);

		if(m_curr_image.m_image)
		{
			maybe_drop_doc(*m_curr_image.m_image);
		}

		m_curr_image.m_image = 0;
		m_curr_image.m_frame = 0;
	}
}

