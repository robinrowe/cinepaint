/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      SheetView - Spreadsheet for images ui widget
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: SheetView.h,v 1.2 2006/07/26 20:37:44 lamfada Exp $
 */

#ifndef _SHEET_VIEW_H_
#define _SHEET_VIEW_H_

#include "dll_api.h"
#include "CPDAGData.h"
#include "plug-ins/pdb/ListenerList.h"
#include <FL/Fl.H>
#include <FL/Fl_Table.H>
#include <FL/Fl_Input.H>

class CinePaintKeyListener;
class CinePaintMouseListener;
class CPDAGNode;
class CPSpreadsheet;
class SheetChangeListener;
class CinePaintImage;
class PluginParam;
class SheetCellRenderer;


/**
 * SheetView represetns the main 'spreadsheet' widget for the spreadsheet of images within CinePaint
 *
 */
class CINEPAINT_GUI_API SheetView : public Fl_Table
{
	public:
		/**
		 * Constructs a new SheetView to display and edit a spreadsheet for imaged
		 * The SheetView interfaces with the spreadsheet data via the specified 
		 * CPSreadsheet, this is used to update cells, and get cell data for display
		 *
		 * @param x position of the Spreadsheet widget
		 * @param y position of the Spreadsheet widget
		 * @param w width of the widget
		 * @param h height of the widget
		 * @param title title of the widget
		 * @param cps CPSpreadsheet interface into the data backing the spreadsheet
		 */
		SheetView(int x, int y, int w, int h, const char* title, CPSpreadsheet& cps);

		/**
		 * Destructor
		 */
		~SheetView();


		// required so we dont hide the Fl_Table inplemenations of int rows() and int cols()
		using Fl_Table::rows;
		using Fl_Table::cols;

		/**
		 * Sets the number of rows within this SheetView
		 * If the cell input entry is currently visible, it will be set non-visaible and its
		 * callback done
		 *
		 * @param val the new number of Rows within the SheetView
		 */
		void rows(int val);

		/**
		 * Sets the number of columns within this SheetView
		 * If the cell input entry is currently visible, it will be set non-visaible and its
		 * callback done
		 *
		 * @param val the new number of Columns within the SheetView
		 */
		void cols(int val);

		//--------------------
		// Input entry Control

		/**
		 * Sets the visibility of the cell input entry.
		 * The cell input entry allows direct text entry into the curently selected cell
		 * If vis is set true, the focus parametyer may be used to request that the
		 * entry widget is given input focus.
		 *
		 * @param vis the visibility of the cell input entry
		 * @param forcus indicates whether the input entry should gain focus of the cursor
		 */
		void SetInputEntryVisibile(bool vis, bool focus = true);

		/**
		 * Returns whether the cell input entry is visible
		 *
		 * @return true if the cell input entry is visible, false otherwise
		 */
		bool IsInputEntryVisible() const;

		/**
		 * Sets the test displayed upon the cell input entry.
		 * This method sets the text upon the cell iunput entry, a call should be made
		 * to SetInputEntryVisible to actuially set the input visible at the currently
		 * selected cell
		 *
		 * @param text the text to display.
		 */
		void SetInputEntryText(const std::string& text);

		/**
		 * Processes the current cell text entry input.
		 * This method invokes the cell entry callback with the currently set text
		 *
		 */
		void ProcessInputEntryText();




		//-----------------
		// Change Listeners

		/**
		 * Adds a SheetChangeListener to this SheetView to review notifications of sheet changes
		 * if the specified SheetChangeListener has already been added, it is not added again.
		 * The resources of the specified SheetChangeListener are not managed by this class
		 *
		 * @param scl the SheetCjangeListener to add
		 */
		void AddSheetChangeListener(SheetChangeListener* scl);

		/**
		 * Removes the specified SheetChangeListener
		 * The removed SheetChangeListener will no longer receive SheetView change notifications from
		 * this class
		 *
		 * @param scl the SheetChangeListener to be removed
		 */
		SheetChangeListener* RemoveSheetChangeListener(SheetChangeListener* scl);



		/**
		 * Adds the specified CinePaintMouseListener to receive mouse events from this SheetView
		 * if the specified CinePaintMouseListener has already been added it is not added again.
		 * The resources of the specified CinePaintMouseListener are not managed by this class
		 *
		 * @param cml the CinePaintMouseListener to be added
		 */
		void AddMouseListener(CinePaintMouseListener* cml);

		/**
		 * Removes the specified CinePaintMouseListener.
		 * The removed CinePaintMouseListener will no longer receive mouse events from this SheetView
		 *
		 * @param cml the CinePaintMouseListener to be removed
		 */
		CinePaintMouseListener* RemoveMouseListener(CinePaintMouseListener* cml);



		/**
		 * Adds a CinePaintKeyListener to this SheetView to review notifications of key pressed
		 * if the specified CinePaintKeyListener has already been added, it is not added again.
		 * The resources of the specified CinePaintKeyListener are not managed by this class
		 *
		 * @param kl the CinePaintKeyListener to add
		 */
		void AddKeyListener(CinePaintKeyListener* kl);

		/**
		 * Removes the specified CinePaintKeyListener
		 * The removed CinePaintKeyListener will no longer receive key press notifications from
		 * this SheetView
		 *
		 * @param kl the CinePaintKeyListener to be removed
		 */
		CinePaintKeyListener* RemoveKeyListener(CinePaintKeyListener* kl);

		/**
		 * Get the current row
		 */
		int get_current_row();
		/**
		 * Get the current column
		 */
		int get_current_col();

	protected:

		/**
		 * Handles the drawaing of each cell when the table needcs redrawn
		 *
		 */
		void draw_cell(TableContext context, int row, int col, int x, int y, int w, int h);

		/**
		 * Overidden from Widget to handle SheetView specific requirements.
		 * Once this SheetView has finished with the event, the event is passes to handle(int e) within
		 * the Fl_Table parent class
		 * Currently the SheetView is required to handle the event first to extract the cursor
		 * movement keypresses from fltk in order let interested listeners know of the cursor movement,
		 * currently Fl_Table wallows these events.
		 *
		 * @param e the fltk event
		 */
		int handle(int e);

	private:


		//----------------------
		// Table Drawing methods

		/**
		 * Draws the column header for the specified column
		 *
		 * @param col the column of the header to draw
		 * @param x x coordinate of the header
		 * @param y y coordinate of the header
		 * @param w width of the header
		 * @param h height of the header
		 */
		void draw_col_header(int col, int x, int y, int w, int h);

		/**
		 * Draws the row header for the specified column
		 *
		 * @param row the row of the header to draw
		 * @param x x coordinate of the header
		 * @param y y coordinate of the header
		 * @param w width of the header
		 * @param h height of the header
		 */
		void draw_row_header(int row, int x, int y, int w, int h);

		/**
		 * Draws a single specified cell within the table
		 *
		 * @param row the row of the cell to draw
		 * @param col the column of the cell to draw
		 * @param x x coordinate of the cell
		 * @param y y coordinate of the cell
		 * @param w width of the cell
		 * @param h height of the cell
		 */
		void draw_context_cell(int row, int col, int x, int y, int w, int h);




		/**
		 * process an update to the current cursor position
		 * This method compares the current position to the previous position to determine if
		 * the current cursor position has change, or whether the current range has changed,
		 * and posts the appropriate SheetChangeListener event
		 *
		 */
		void process_cursor_move();

		//--------
		// Widgets

		Fl_Input* m_text_input;


		//------------------
		// Listener handling

		void post_cell_changed_event(int row, int col);
		void post_range_changed_event(int start_row, int start_col, int end_row, int end_col);

		typedef ListenerList<SheetChangeListener> SheetListenerList_t;
		typedef ListenerList<CinePaintMouseListener> MouseListenerList_t;
		typedef ListenerList<CinePaintKeyListener> KeyListenerList_t;
		SheetListenerList_t m_sheet_listeners;
		MouseListenerList_t m_mouse_listeners;
		KeyListenerList_t m_key_listeners;


		//-----------------
		// Widget Callbacks
		static void static_table_cb(Fl_Widget* w, void* d)
		{
			reinterpret_cast<SheetView*>(d)->table_cb();
		}
		static void static_text_input_cb(Fl_Widget* w, void* d)
		{
			static_cast<SheetView*>(d)->text_input_cb();
		}

		/**
		 * Handler for the table callback.
		 * This implementation Determines the current context of the table event. If the
		 * context is CONTEXT_CELL, and event may be fired to a registered listener depending
		 * on whever the table callback weas genereated from a keypress, or a mouse press.
		 * If the context is either, CONTEXT_ROW_HEADER, CONTEXT_COL_HEADER, or CONTEXST_TABLE,
		 * the cell text input is hidden and the callback performed.
		 *
		 */
		void table_cb();

		/**
		 * handle the callback from the cell text input.
		 * This method fires a ValueChanged event to any registered SheetViewlisteners 
		 * indicating that the data in the current cell has been changed
		 *
		 */
		void text_input_cb();

		/**
		 * Helper method to process a CONTEXT_CELL event.
		 *
		 * @param row the row of the cell event
		 * @param col the column of the cell event
		 */
		void handle_cell_event(int row, int col);



		//--------
		// Members


		/** CPSpreadsheet interface to the DAG */
		CPSpreadsheet& m_spreadsheet;

		/** renderer for rendering the various data items into cells */
		SheetCellRenderer* m_cell_renderer;



		/* current cell row position for cell input entry */
		int m_edit_cell_row;

		/* current cell column position for cell input entry */
		int m_edit_cell_col;





		/** previously selected column - used to process cursor movements agains current position */
		int m_curr_col;

		/** previously selected row - used to process cursor movements agains current position */
		int m_curr_row;

		/** Previsou number of selected columns */
		int m_curr_select_cols;

		/** Previsou number of selected row */
		int m_curr_select_rows;

};

#endif /* _SHEET_VIEW_H_ */
