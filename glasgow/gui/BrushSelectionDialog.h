/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      BrushSelectionDialog - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: BrushSelectionDialog.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _BRUSH_SELECTION_PANEL_H_
#define _BRUSH_SELECTION_PANEL_H_

#include "dll_api.h"
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include "UIUtils.h" // reg for LayoutHelper nested class

// forward declarations
class BrushList;
class CinePaintBrush;

class CINEPAINT_GUI_API Fl_Window;
class Fl_Box;
class Fl_Button;
class Fl_Check_Button;
class Fl_Choice;
class Fl_Scroll;
class Fl_Value_Slider;

class CINEPAINT_GUI_API BrushSelectionDialog : public Fl_Window
{
	public:
		/**
		 * Constructs a new BrushSelectionPanel
		 *
		 * @param title dialog title
		 * @param init_name
		 * @param init_opacity
		 * @param init_spacing
		 * @param init_noise_freq
		 * @param init_noise_step_start
		 * @param init_noise_step_width
		 * @param init_mode
		 */
		BrushSelectionDialog(const char* title,
			const char* init_name,
			double init_opacity,
			double init_spacing,
			double init_noise_freq,
			double init_noise_step_start,
			double init_noise_step_width,
			int init_mode) ;

		/**
		 * Destructor
		 */
		virtual ~BrushSelectionDialog();


		/**
		 * Refresh the selectable list of brushes from the specified BrushList
		 *
		 * @param brush_list the BrushList to refresh the selectable brush list display from
		 */
		void RefreshBrushList(const BrushList& brush_list);

	protected:
	private:

		//=============
		// Data Members

		CinePaintBrush* m_active_brush;

		//=================
		// Widget callbacks

		static void static_noise_button_toggled_cb(Fl_Widget* w, void* d) { static_cast<BrushSelectionDialog*>(d)->noise_button_toggled_cb(reinterpret_cast<Fl_Check_Button*>(w)); };
		void noise_button_toggled_cb(Fl_Check_Button* btn);

		static void static_paint_mode_menu_cb(Fl_Widget* w, void* d) { static_cast<BrushSelectionDialog*>(d)->paint_mode_menu_cb(reinterpret_cast<Fl_Choice*>(w)); };
		void paint_mode_menu_cb(Fl_Choice* choice);

		static void static_edit_brush_button_cb(Fl_Widget* w, void* d) { static_cast<BrushSelectionDialog*>(d)->edit_brush_button_cb(reinterpret_cast<Fl_Button*>(w)); };
		void edit_brush_button_cb(Fl_Button* btn) ;

		static void static_new_brush_button_cb(Fl_Widget* w, void* d) { static_cast<BrushSelectionDialog*>(d)->new_brush_button_cb(reinterpret_cast<Fl_Button*>(w)); };
		void new_brush_button_cb(Fl_Button* btn) ;

		static void static_close_button_cb(Fl_Widget* w, void* d) { static_cast<BrushSelectionDialog*>(d)->close_button_cb(reinterpret_cast<Fl_Button*>(w)); };
		void close_button_cb(Fl_Button* btn) ;

		static void static_refresh_brush_list_button_cb(Fl_Widget* w, void* d) { static_cast<BrushSelectionDialog*>(d)->refresh_brush_list_button_cb(reinterpret_cast<Fl_Button*>(w)); };
		void refresh_brush_list_button_cb(Fl_Button* btn) ;

		//===============
		// Dialog Widgets

		Fl_Scroll* m_brush_scroll;
		Fl_Box* m_brush_name_label;
		Fl_Box* m_brush_size_label;
		Fl_Choice* m_mode_option_menu;

		Fl_Value_Slider* m_opacity_slider;
		Fl_Value_Slider* m_spacing_slider;
		Fl_Value_Slider* m_noise_freq_slider;
		Fl_Value_Slider* m_noise_step_start_slider;
		Fl_Value_Slider* m_noise_step_width_slider;

		Fl_Group* m_noise_box;



		//============================
		// widget construction helpers

		/**
		 * Createa the brush selection / preview scrolling panel
		 *
		 */
		void create_brush_select_panel(UIUtils::LayoutHelper& lh) ;

		/**
		 * Creates the main widgets of the dialog panel
		 */
		void create_options_panel(double init_opacity,
			double init_spacing,
			double init_noise_freq,
			double init_noise_step_start,
			double init_noise_step_width,
			UIUtils::LayoutHelper& lh) ;

		/**
		 * Creates the action area widgets
		 * Currently this consists of close and refresh panel buttons
		 *
		 * @return the constructed panel
		 */
		void create_action_area_panel(UIUtils::LayoutHelper& lh);
		
		/**
		 * Creates the paint mode menu options
		 * @param menu the menu to add menu option into
		 */
		Fl_Choice& create_paint_mode_menu(Fl_Choice& menu) ;

		Fl_Image* create_brush_image(CinePaintBrush& brush) ;

		/**
		 * Adds a small scaled instance indicator (a small cross) to the top left of the specified image data buffer.
		 * The image buffer is assumed to be of BrushSelectionDialog::DEFAULT_BRUSH_DEPTH
		 * If the width of the buffer is not large enough for the scale indicator, no indicator is added
		 *
		 * @param buf the image data buffer to add the indicator to
		 * @param width the width of the image buffer
		 */
		void add_scale_indicator(unsigned char* buf, unsigned int width) const;


		/** brush cell width/height */
		int m_brush_cell_width ;
		int m_brush_cell_height ;




		//================
		// Static Members

		/* default dialog title string */
		static const char* DEFAULT_DIALOG_TITLE;

		static const unsigned int DEFAULT_CELL_WIDTH;
		static const unsigned int DEFAULT_CELL_HEIGHT;
		static const unsigned int DEFAULT_BRUSH_COLUMNS;
		static const unsigned int DEFAULT_BRUSH_ROWS;

		static const unsigned int DEFAULT_ROW_HEIGHT;
		static const unsigned int DEFAULT_LABEL_WIDTH;

		static const unsigned int DEFAULT_MARGIN_WIDTH;
		static const unsigned int DEFAULT_MARGIN_HEIGHT;

		static const unsigned int DEFAULT_BRUSH_DEPTH;
		

} ; /* class BrushSelectionDialog */

#endif /* _BRUSH_SELECTOR_PANEL_H_ */
