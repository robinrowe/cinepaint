/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      InitDialog - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: InitDialog.cpp,v 1.2 2008/01/28 18:22:28 robinrowe Exp $
 */

#include "InitDialog.h"
#include "CinePaintAppUI.h"
#include <app/CinePaintEventFunctor.h>
#include <string>

#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Progress.H>
#include <FL/Fl_PNM_Image.H>

const int InitDialog::MAX_PROGRESS_MSG_SIZE = 500;

// DIRTY HACK FIXME:::: Find a better way of doing this.
#ifdef WIN32
#define PKG_DATA_DIR "."
#else
#define PKG_DATA_DIR "."
#endif
/**
 * Constructs a new InitDialog with the specified splash screen image
 *
 * @param width the widthh of this InitDialog
 * @param height the height of this InitDialog
 * @param title the dialog title
 * @param splash_image the splash screen image
 */
InitDialog::InitDialog(int width, int height, const char* title, const char* splash_image)
: Fl_Window(width, height, title)
{
	std::string _s(PKG_DATA_DIR);
	if(_s[_s.length() -1] != '/')
	{
		_s.append("/");
	}
	_s.append(splash_image);

	Fl_PNM_Image* img= new Fl_PNM_Image(_s.c_str());
	Fl_Box* img_box = new Fl_Box(0,1,400,240);

	img_box->image(img);
	m_progress_info = new Fl_Box(0,240,400,18, "Initalising         ");
	m_progress_info->box(FL_NO_BOX);
	m_progress_bar = new Fl_Progress(0, 255, 400, 20);

	this->end();

	m_text = new char[MAX_PROGRESS_MSG_SIZE];
	m_progress_step = 1;
}

InitDialog::~InitDialog()
{
	if(m_text)
	{
		delete[] m_text;
		m_text = 0;
	}
}


void InitDialog::ProgressStart(char* ID, char* msg, int stop, int step)
{
	if(CinePaintAppUI::IsUIThread())
	{
		progress_start(msg, stop, step);
	}
	else
	{
		CinePaintAppUI::GetInstance().GetUIThreadEventList().PostEvent(new CinePaintEventFunctor3<InitDialog, std::string, int, int>(this, &InitDialog::progress_start, std::string(msg), stop, step, false));
	}
}

void InitDialog::MessageChanged(char* ID, char* msg )
{
	if(CinePaintAppUI::IsUIThread())
	{
		message_changed(msg);
	}
	else
	{
		CinePaintAppUI::GetInstance().GetUIThreadEventList().PostEvent(new CinePaintEventFunctor1<InitDialog, std::string>(this, &InitDialog::message_changed, std::string(msg), false));
	}
}

void InitDialog::LimitsChanged(char* ID, int start, int stop, int step)
{
	if(CinePaintAppUI::IsUIThread())
	{
		limits_changed(start, stop, step);
	}
	else
	{
		CinePaintAppUI::GetInstance().GetUIThreadEventList().PostEvent(new CinePaintEventFunctor3<InitDialog, int, int, int>(this, &InitDialog::limits_changed, start, stop, step, false));
	}
}

void InitDialog::ProgressUpdate(char* ID)
{
	if(CinePaintAppUI::IsUIThread())
	{
		progress_update();
	}
	else
	{
		CinePaintAppUI::GetInstance().GetUIThreadEventList().PostEvent(new CinePaintEventFunctor0<InitDialog>(this, &InitDialog::progress_update, false));
	}

}

void InitDialog::ProgressPosition(char* ID, int pos)
{
	if(CinePaintAppUI::IsUIThread())
	{
		progress_position(pos);
	}
	else
	{
		CinePaintAppUI::GetInstance().GetUIThreadEventList().PostEvent(new CinePaintEventFunctor1<InitDialog, int>(this, &InitDialog::progress_position, pos, false));
	}
}

void InitDialog::ProgressReset(char* ID)
{
	if(CinePaintAppUI::IsUIThread())
	{
		progress_reset();
	}
	else
	{
		CinePaintAppUI::GetInstance().GetUIThreadEventList().PostEvent(new CinePaintEventFunctor0<InitDialog>(this, &InitDialog::progress_reset, false));
	}
}

void InitDialog::ProgressComplete(char* ID)
{
	if(CinePaintAppUI::IsUIThread())
	{
		progress_complete();
	}
	else
	{
		CinePaintAppUI::GetInstance().GetUIThreadEventList().PostEvent(new CinePaintEventFunctor0<InitDialog>(this, &InitDialog::progress_complete, false));
	}
}





/**
 * Initializes the progress dialog
 * This method should be called only from he UI thread
 *
 * @param msg progress description message
 * @param stop progress stop/end value
 * @param step step increments of progress updates
 */
void InitDialog::progress_start(std::string msg, int stop, int step)
{
	strncpy(m_text, msg.c_str(), MAX_PROGRESS_MSG_SIZE);
	m_progress_info->label(m_text);

	m_progress_bar->value(0.0f);
	m_progress_bar->minimum(0.0f);
	m_progress_bar->maximum(static_cast<float>(stop));
	m_progress_step = step;
}

/**
 * Updates the displayed progress message
 * This method should be called only from he UI thread
 *
 * @param msg progress information message
 */
void InitDialog::message_changed(std::string msg)
{
	strncpy(m_text, msg.c_str(), MAX_PROGRESS_MSG_SIZE);
	m_progress_info->label(m_text);
}

/**
 * Updates the limits of the displayed progress bar
 * This method also reset the current position to the start value
 * This method should be called only from he UI thread
 *
 * @param start the new starting value
 * @param stop the new stop vaule
 * @param step size of incremental step during a progress update
 */
void InitDialog::limits_changed(int start, int stop, int step)
{
	m_progress_bar->minimum(static_cast<float>(start));
	m_progress_bar->maximum(static_cast<float>(stop));
	m_progress_bar->value(static_cast<float>(start));
	m_progress_step = step;
}

/**
 * Increments the current progress position by the set step value
 * This method should be called only from he UI thread
 * This method should be called only from he UI thread
 *
 */
void InitDialog::progress_update()
{
	float _val = m_progress_bar->value();
	_val += m_progress_step;

	m_progress_bar->value(_val);
}

/**
 * Sets the current progress position to the specified value
 * This method should be called only from he UI thread
 * This method should be called only from he UI thread
 *
 * @param pos the new pogress position
 */
void InitDialog::progress_position(int pos)
{
	m_progress_bar->value(static_cast<float>(pos));
}

/**
 * Resets the progress display to the start value
 * This method should be called only from he UI thread
 *
 */
void InitDialog::progress_reset()
{
	m_progress_bar->value(m_progress_bar->minimum());
}

/**
 * Sets the progress to complete
 * This method should be called only from he UI thread
 *
 */
void InitDialog::progress_complete()
{
	m_progress_bar->value(m_progress_bar->maximum());
}

