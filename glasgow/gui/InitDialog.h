/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      InitDialog - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: InitDialog.h,v 1.2 2006/12/18 08:21:17 robinrowe Exp $
 */

#ifndef _INIT_DIALOG_H_
#define _INIT_DIALOG_H_

#include "dll_api.h"
#include <FL/Fl_Window.H>
#include <plug-ins/pdb/CinePaintProgressListener.h>
#include <app/CinePaintEventFunctor.h>

#include <string>

// forward declaration
class Fl_Progress;
class Fl_Box;

/**
 * CinePaint initilization progress dialog/splash screen.
 * This class display a progress dialog to keep a user informed of the application
 * initialization
 *
 */
class CINEPAINT_GUI_API InitDialog : public Fl_Window, public CinePaintProgressListener
{
	public:
		/**
		 * Constructs a new InitDialog with the specified splash screen image
		 *
		 * @param width the widthh of this InitDialog
		 * @param height the height of this InitDialog
		 * @param title the dialog title
		 * @param splash_image the splash screen image
		 */
		InitDialog(int width, int height, const char* title, const char* splash_image);

		virtual ~InitDialog();


		//-----------------------------------------
		// CinePaintProgressListener implementation

		virtual void ProgressStart(char* ID, char* msg, int stop, int step);
		virtual void MessageChanged(char* ID, char* msg );
		virtual void LimitsChanged(char* ID, int start, int stop, int step);
		virtual void ProgressUpdate(char* ID);
		virtual void ProgressPosition(char* ID, int pos);
		virtual void ProgressReset(char* ID);
		virtual void ProgressComplete(char* ID);

		/** maximum size of a progress message */
		static const int MAX_PROGRESS_MSG_SIZE;

	protected:

	private:

		/**
		 * Initializes the progress dialog
		 * This method should be called only from he UI thread
		 *
		 * @param msg progress description message
		 * @param stop progress stop/end value
		 * @param step step increments of progress updates
		 */
		void progress_start(std::string msg, int stop, int step);

		/**
		 * Updates the displayed progress message
		 * This method should be called only from he UI thread
		 *
		 * @param msg progress information message
		 */
		void message_changed(std::string msg);

		/**
		 * Updates the limits of the displayed progress bar
		 * This method also reset the current position to the start value
		 * This method should be called only from he UI thread
		 *
		 * @param start the new starting value
		 * @param stop the new stop vaule
		 * @param step size of incremental step during a progress update
		 */
		void limits_changed(int start, int stop, int step);

		/**
		 * Increments the current progress position by the set step value
		 * This method should be called only from he UI thread
		 * This method should be called only from he UI thread
		 *
		 */
		void progress_update();

		/**
		 * Sets the current progress position to the specified value
		 * This method should be called only from he UI thread
		 * This method should be called only from he UI thread
		 *
		 * @param pos the new pogress position
		 */
		void progress_position(int pos);

		/**
		 * Resets the progress display to the start value
		 * This method should be called only from he UI thread
		 *
		 */
		void progress_reset();

		/**
		 * Sets the progress to complete
		 * This method should be called only from he UI thread
		 *
		 */
		void progress_complete();

		char* m_splash_img_file;

		Fl_Progress* m_progress_bar;
		Fl_Box* m_progress_info;

		char* m_text;
		int m_progress_step;
};

#endif // _INIT_DIALOG_H_
