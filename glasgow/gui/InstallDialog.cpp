/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      InstallDialog - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: InstallDialog.cpp,v 1.3 2006/12/18 08:21:17 robinrowe Exp $
 */

#include "gui/InstallDialog.h"
//#include <config.h>
#include "CinePaintAppUI.h"
#include "app/CinePaintApp.h"
#include "app/AppSettings.h"
#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Text_Buffer.H>
#include <FL/Fl_Text_Display.H>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>

// Define the font styles we will use.
static Fl_Text_Display::Style_Table_Entry styletable[] = {     // Style table
	{ FL_BLACK,   FL_HELVETICA,        10, 0 },  // A - Plain
	{ FL_BLACK,   FL_HELVETICA_BOLD,   12, 0 },  // B - Strong
	{ FL_BLUE,    FL_HELVETICA_ITALIC, 10, 0 }   // C - Italic
};

DialogResponse InstallDialog::InstallDialogDo()
{
	printf("InstallDialog::InstallDialogDo()\n");
	
	install_dialog = new Fl_Window(620,500, "Installation" );

	install_install_button = new Fl_Button(10, 460,70,30,"Install");
	install_ignore_button = new Fl_Button(110,460,70,30,"Ignore");
	install_quit_button = new Fl_Button(210,460,70,30,"Quit");
	// Setup the callbacks for buttons.
	install_install_button->callback( install_install_button_static_callback, this );
	install_ignore_button->callback( install_ignore_button_static_callback, this );
	install_quit_button->callback( install_quit_button_static_callback, this );



	install_msg = new Fl_Text_Display(10,10,590, 440);
	// Add text buffer to Dipslay.
	Fl_Text_Buffer *buf, *style;
	// Set some styles for text.
	
	buf = new Fl_Text_Buffer();
	style = new Fl_Text_Buffer();

	set_text(buf, style);

	install_dialog->end();
	install_dialog->show();
	while (install_dialog->visible())
		Fl::wait();

	 return m_response;
}

void InstallDialog::set_style(Fl_Text_Buffer *stylebuf , char style, int start, int end)
{
	char* tmp = new char[end-start+1];
	memset(tmp, style, end-start);
	tmp[end-start]='\0';
	stylebuf->append(tmp);
//	delete tmp;
}

void InstallDialog::set_text(Fl_Text_Buffer *buf, Fl_Text_Buffer *stylebuf)
{
		int end=0;
	buf->append(APP_NAME "\n\n");
	set_style(stylebuf, 'B', end, buf->length()); 
	end = buf->length();
	//buf->append(COPYRIGHT "\n");
	set_style(stylebuf, 'C', end, buf->length()); 
	end = buf->length();
	buf->append("\n");
	buf->append("This program is free software; you can redistribute it and/or modify\n");
	buf->append("it under the terms of the Mozilla Public License Version 1.1 (the \"License\")\n");
	buf->append("you may not use this software except in compliance with\n");
    buf->append("the License. You may obtain a copy of the License at\n");
	buf->append("http://www.mozilla.org/MPL/.\n");
	buf->append("\n");
	buf->append("Alternatively, this software may be used under the terms of\n");
	buf->append("either the GNU General Public License Version 2 or later (the \"GPL\"),\n");
	buf->append("in which case the provisions of the GPL are applicable instead of those\n");
	buf->append("above.\n");
	buf->append("\n");
	buf->append("Software distributed under the License is distributed on an \"AS IS\" basis,\n");
	buf->append("WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License\n");
	buf->append("for the specific language governing rights and limitations under the\n");
	buf->append("License.\n");
	buf->append("\n\n");
	set_style(stylebuf, 'A', end, buf->length()); 
	end = buf->length();
	buf->append("Personal " APP_NAME " Installation\n\n");
	set_style(stylebuf, 'B', end, buf->length()); 
	end = buf->length();
	buf->append("For a proper " APP_NAME " installation, a subdirectory called\n");
	buf->append(dotdir);
	buf->append(" needs to be created.  This\n");
	buf->append("subdirectory will contain a number of important files:\n\n");

	set_style(stylebuf, 'A', end, buf->length()); 
	end = buf->length();
	buf->append("cinepaintrc\n");
	set_style(stylebuf, 'C', end, buf->length()); 
	end = buf->length();
	buf->append("\t\tThe cinepaintrc is used to store personal preferences\n");
	buf->append("\t\tsuch as default " APP_NAME " behaviors & plug-in hotkeys.\n");
	buf->append("\t\tPaths to search for brushes, palettes, gradients\n");
	buf->append("\t\tpatterns, and plug-ins are also configured here.\n");

	set_style(stylebuf, 'A', end, buf->length()); 
	end = buf->length();
	buf->append("pluginrc\n");
	set_style(stylebuf, 'C', end, buf->length()); 
	end = buf->length();
	buf->append("\t\tPlug-ins and extensions are extern programs run by\n");
	buf->append("\t\t" APP_NAME " which provide additional functionality.\n");
	buf->append("\t\tThese programs are searched for at run-time and\n");
	buf->append("\t\tinformation about their functionality and mod-times\n");
	buf->append("\t\tis cached in this file.  This file is intended to\n");
	buf->append("\t\tbe " APP_NAME " readable only, and should not be edited.\n");

	set_style(stylebuf, 'A', end, buf->length()); 
	end = buf->length();
	buf->append("brushes\n");
	set_style(stylebuf, 'C', end, buf->length()); 
	end = buf->length();
	buf->append("\t\tThis is a subdirectory which can be used to store\n");
	buf->append("\t\tuser defined brushes.  The default cinepaintrc file\n");
	buf->append("\t\tchecks this subdirectory in addition to the system-\n");
	buf->append("\t\twide " APP_NAME " brushes installation when searching for\n");
	buf->append("\t\tbrushes.\n");

	set_style(stylebuf, 'A', end, buf->length()); 
	end = buf->length();
	buf->append("gradients\n");
	set_style(stylebuf, 'C', end, buf->length()); 
	end = buf->length();
	buf->append("\t\tThis is a subdirectory which can be used to store\n");
	buf->append("\t\tuser defined gradients.  The default cinepaintrc file\n");
	buf->append("\t\tchecks this subdirectory in addition to the system-\n");
	buf->append("\t\twide " APP_NAME " gradients installation when searching for\n");
	buf->append("\t\tgradients.\n");

	set_style(stylebuf, 'A', end, buf->length()); 
	end = buf->length();
	buf->append("gfig\n");
	set_style(stylebuf, 'C', end, buf->length()); 
	end = buf->length();
	buf->append("\t\tThis is a subdirectory which can be used to store\n");
	buf->append("\t\tuser defined figures to be used by the gfig plug-in.\n");
	buf->append("\t\tThe default cinepaintrc file checks this subdirectory in\n");
	buf->append("\t\taddition to the systemwide CinePaint gfig installation\n");
	buf->append("\t\twhen searching for gfig figures.\n");

	set_style(stylebuf, 'A', end, buf->length()); 
	end = buf->length();
	buf->append("gflares\n");
	set_style(stylebuf, 'C', end, buf->length()); 
	end = buf->length();
	buf->append("\t\tThis is a subdirectory which can be used to store\n");
	buf->append("\t\tuser defined gflares to be used by the gflare plug-in.\n");
	buf->append("\t\tThe default cinepaintrc file checks this subdirectory in\n");
	buf->append("\t\taddition to the systemwide CinePaint gflares installation\n");
	buf->append("\t\twhen searching for gflares.\n");

	set_style(stylebuf, 'A', end, buf->length()); 
	end = buf->length();
	buf->append("palettes\n");
	set_style(stylebuf, 'C', end, buf->length()); 
	end = buf->length();
	buf->append("\t\tThis is a subdirectory which can be used to store\n");
	buf->append("\t\tuser defined palettes.  The default cinepaintrc file\n");
	buf->append("\t\tchecks only this subdirectory (not the system-wide\n");
	buf->append("\t\tinstallation) when searching for palettes.  During\n");
	buf->append("\t\tinstallation, the system palettes will be copied\n");
	buf->append("\t\there.  This is done to allow modifications made to\n");
	buf->append("\t\tpalettes during " APP_NAME " execution to persist across\n");
	buf->append("\t\tsessions.\n");

	set_style(stylebuf, 'A', end, buf->length()); 
	end = buf->length();
	buf->append("patterns\n");
	set_style(stylebuf, 'C', end, buf->length()); 
	end = buf->length();
	buf->append("\t\tThis is a subdirectory which can be used to store\n");
	buf->append("\t\tuser defined patterns.  The default cinepaintrc file\n");
	buf->append("\t\tchecks this subdirectory in addition to the system-\n");
	buf->append("\t\twide " APP_NAME " patterns installation when searching for\n");
	buf->append("\t\tpatterns.\n");

	set_style(stylebuf, 'A', end, buf->length()); 
	end = buf->length();
	buf->append("plug-ins\n");
	set_style(stylebuf, 'C', end, buf->length()); 
	end = buf->length();
	buf->append("\t\tThis is a subdirectory which can be used to store\n");
	buf->append("\t\tuser created, temporary, or otherwise non-system-\n");
	buf->append("\t\tsupported plug-ins.  The default cinepaintrc file\n");
	buf->append("\t\tchecks this subdirectory in addition to the system-\n");
	buf->append("\t\twide " APP_NAME " plug-in directories when searching for\n");
	buf->append("\t\tplug-ins.\n");

	set_style(stylebuf, 'A', end, buf->length()); 
	end = buf->length();
	buf->append("scripts\n");
	set_style(stylebuf, 'C', end, buf->length()); 
	end = buf->length();
	buf->append("\t\tThis subdirectory is used by the CinePaint to store \n");
	buf->append("\t\tuser created and installed scripts. The default cinepaintrc\n");
	buf->append("\t\tfile checks this subdirectory in addition to the system\n");
	buf->append("\t\t-wide " APP_NAME " scripts subdirectory when searching for scripts\n");

	set_style(stylebuf, 'A', end, buf->length()); 
	end = buf->length();
	buf->append("tmp\n");
	set_style(stylebuf, 'C', end, buf->length()); 
	end = buf->length();
	buf->append("\t\tThis subdirectory is used by " APP_NAME " to temporarily\n");
	buf->append("\t\tstore undo buffers to reduce memory usage.  If " APP_NAME " is\n");
	buf->append("\t\tunceremoniously killed, files may persist in this directory\n");
	buf->append("\t\tof the form: cinepaint<#>.<#>.  These files are useless across\n");
	buf->append("\t\t" APP_NAME " sessions and can be destroyed with impunity.\n");
	set_style(stylebuf, 'A', end, buf->length()); 
	end = buf->length();

	install_msg->buffer(buf);
	install_msg->highlight_data(stylebuf, styletable, 3, 'A', NULL, 0);
}

void InstallDialog::install_install_button_callback(Fl_Widget *w)
{
	m_response = YES_ENUM;
	install_dialog->hide();
}

void InstallDialog::install_ignore_button_callback(Fl_Widget *w)
{
	// Initalise the main window.
//	CinePaintAppUI::GetInstance().InitMain();
	// Hide and deactivate the install done window
	m_response = NO_ENUM;
	install_dialog->hide();
}

void InstallDialog::install_quit_button_callback(Fl_Widget *w)
{
	// Deactivate all windows so that application exits,
	// as Fl:run has no active windows.
	m_response = QUIT_ENUM;
	install_dialog->hide();

}

DialogResponse InstallDialog::InstallLogDo(const char* msg )
{
	install_done_dialog = new Fl_Window(500, 200, "Installation Log");
	install_done_cont_button = new Fl_Button(10, 160, 100, 20, "Continue");
	install_done_quit_button = new Fl_Button(150, 160, 100, 20, "Quit");

	install_done_cont_button->callback(install_done_continue_static_callback, this);
	install_done_quit_button->callback(install_done_quit_static_callback, this);

	Fl_Text_Display *log = new Fl_Text_Display(10,10,470, 140);

	// Add text buffer to Dipslay.
	Fl_Text_Buffer *buf, *style;

	buf = new Fl_Text_Buffer();
	style = new Fl_Text_Buffer();

	int end=0;
	buf->append("User Installation Log\n\n");
	set_style(style, 'B', end, buf->length()); 
	end = buf->length();
	{
		if(msg && *msg)
		{
			buf->append(msg);
			set_style(style, 'A', end, buf->length()); 
			end = buf->length();
			msg="\nInstallation failed!\n";
		}
		else
		{	msg="\nInstallation successful!\n";
		}
		buf->append(msg);
		set_style(style, 'A', end, buf->length()); 
	}
	log->buffer(buf);
	log->highlight_data(style, styletable, 3, 'A', NULL, 0);

	install_done_dialog->end();
	install_done_dialog->show();
	while (install_done_dialog->visible())
		Fl::wait();
	return m_response;
}

void InstallDialog::install_done_continue_callback (Fl_Widget *w)
{
	// Hide and deactivate the install done window
	install_done_dialog->hide();
	//Hide and deactivate the install info window.
	install_dialog->hide();
	m_response = YES_ENUM;
}

void InstallDialog::install_done_quit_callback (Fl_Widget *w)
{
	// Deactivate all windows so that application exits,
	// as Fl:run has no active windows.
	m_response = QUIT_ENUM;
	install_done_dialog->hide();
	install_dialog->hide();
}
