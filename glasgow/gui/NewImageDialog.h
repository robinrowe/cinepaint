/* ***** BEGIN LICENSE BLOCK *****

 * Version: MPL 1.1/GPL 2.0

 *

 * The contents of this file are subject to the Mozilla Public License Version

 * 1.1 (the "License"); you may not use this file except in compliance with

 * the License. You may obtain a copy of the License at

 * http://www.mozilla.org/MPL/

 *

 * Software distributed under the License is distributed on an "AS IS" basis,

 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License

 * for the specific language governing rights and limitations under the

 * License.

 *

 * The Original Code is CinePaint, an image sequence manipulation program

 *                      NewImageDialog - GUI Module

 *

 * The Initial Developer of the Original Code is

 * the University of Glasgow.

 * Portions created by the Initial Developer are Copyright (C) 2004

 * the Initial Developer. All Rights Reserved.

 *

 * Contributor(s):

 *   Donald MacVicar

 *   Colin Law

 *   Stuart Ford

 *

 * Alternatively, the contents of this file may be used under the terms of

 * either the GNU General Public License Version 2 or later (the "GPL"),

 * in which case the provisions of the GPL are applicable instead of those

 * above. If you wish to allow use of your version of this file only under

 * the terms of the GPL, and not to allow others to use your version of this

 * file under the terms of the MPL, indicate your decision by deleting the

 * provisions above and replace them with the notice and other provisions

 * required by the GPL. If you do not delete the provisions above, a recipient

 * may use your version of this file under the terms of any one of the MPL,

 * or the GPL.

 *

 * ***** END LICENSE BLOCK *****

 *

 * $Id: NewImageDialog.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $

 */


#ifndef _NEW_IMAGE_DIALOG_H_

#define _NEW_IMAGE_DIALOG_H_

#include "dll_api.h"
#include "CinePaintWindow.h"
#include <plug-ins/pdb/CinePaintTag.h>
#include <plug-ins/pdb/enums.h>

class CinePaintSpinButton;

class Fl_Check_Button;


class CINEPAINT_GUI_API NewImageDialog : public CinePaintWindow

{

	public:

		NewImageDialog(int w, int h, const char* title);

		virtual ~NewImageDialog() {};


		struct NewImageInfo

		{

			int width;

			int height;


			CinePaintTag::Format format;

			CinePaintTag::Precision precision;


			FillType fill;

		};


		NewImageInfo& GetNewImageInfo(NewImageInfo& info) const;


	protected:


	private:


		void initialize_defaults();


		//--------

		// Widgets


		CinePaintSpinButton* m_width;

		CinePaintSpinButton* m_height;


		// image types

		Fl_Check_Button* m_rgb;

		Fl_Check_Button* m_greyscale;


		// precision

		Fl_Check_Button* m_prec_8bit_unsigned;

		Fl_Check_Button* m_prec_16bit_unsigned;

		Fl_Check_Button* m_prec_16bit_rnh;

		Fl_Check_Button* m_prec_32bit_float;

		Fl_Check_Button* m_prec_16bit_fixed_point;


		// fill type

		Fl_Check_Button* m_background;

		Fl_Check_Button* m_white;

		Fl_Check_Button* m_trans;

		Fl_Check_Button* m_foreground;


		CinePaintTag::Format m_format;

		CinePaintTag::Precision m_precision;

		FillType m_fill;


		static void static_create_button_cb(Fl_Widget* w, void* data) { reinterpret_cast<NewImageDialog*>(data)->create_button_cb(); }

		void create_button_cb();


		static void static_cancel_button_cb(Fl_Widget* w, void* data) { reinterpret_cast<NewImageDialog*>(data)->cancel_button_cb(); }

		void cancel_button_cb();


		static void static_format_cb(Fl_Widget* w, void* dialog);

		static void static_precision_cb(Fl_Widget* w, void* dialog);

		static void static_fill_cb(Fl_Widget* w, void* dialog);


}; /* NewImageDialog */


#endif /* _NEW_IMAGE_DIALOG_H_ */

