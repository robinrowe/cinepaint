/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      WidgetBuilder - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: WidgetBuilder.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "WidgetBuilder.h"
#include <plug-ins/pdb/CPPluginUIDialog.h>
#include <plug-ins/pdb/CPPluginUINumber.h>
#include <plug-ins/pdb/CPPluginUIBool.h>
#include <plug-ins/pdb/CPPluginUIList.h>
#include <plug-ins/pdb/CPPluginUIText.h>
#include "CinePaintFrame.h"
#include "UIUtils.h"
#include <FL/Fl.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Value_Slider.H>
#include <FL/Fl_Value_Input.H>
#include <FL/Fl_Round_Button.H>


DialogResponse WidgetBuilder::DoModalDialog(CPPluginUIDef &ui_def, CPParamList &params)
{
	Fl_Window *_wnd = BuildDialogFLTK(ui_def, params);

	if (_wnd)
	{
		_wnd->show();
		while( _wnd->shown() )
			Fl::wait();
		int res;
		if ( params.GetParam("response", &res) )
			if ( res )
			{
				printf("User clicked OK\n");
				return OK_ENUM;
			}
			else
			{
				printf("User clicked CANCEL\n");
				return CANCEL_ENUM;
			}
	}
	if (ui_def.num == 0)
		return OK_ENUM;
	else
		return CANCEL_ENUM;
}


/*
 * TODO: DWM
 * Check delection of callback Info strcut
 */

Fl_Window *WidgetBuilder::BuildDialogFLTK(CPPluginUIDef &ui_def, CPParamList &params)
{
	Fl_Window *_wnd = 0;
	widget_list_t _lst;
	CPPluginUIDialog *_dlg;
	if ( ui_def.num > 0 )
	{
		// Create the containing Window.
		if ( ui_def.widgets[0]->GetType() == CPPluginUIWidget::DIALOG )
		{
			_dlg = (CPPluginUIDialog *)ui_def.widgets[0];
            _wnd = (Fl_Window *)CreateWidget(_dlg,params);
		}
		if ( _wnd )
		{
			// Add required components.
			Fl_Group *_grp = BuildGroupFLTK(ui_def, params, _lst);

			// Now add Ok and Cancel Buttons.
			int w = _grp->w();
			int h = _grp->h();
			int ok_x = (w/2) - 10 - 40;
			int cancel_x = (w/2)+10;
			_wnd->size(w+10, h+50);
			Fl_Button *_ok = new Fl_Button( ok_x, h+10 ,40, 30, "OK" );
			_ok->callback(ok_callback, &params);
			Fl_Button *_cancel = new Fl_Button( cancel_x,h+10,40, 30, "CANCEL" );
			_cancel->callback(cancel_callback, &params);
			_wnd->end();
			_wnd->init_sizes();
		}

	}
	return _wnd;
}


Fl_Group *WidgetBuilder::BuildGroupFLTK(CPPluginUIDef &ui_def, CPParamList &params)
{
	widget_list_t _lst;
	return BuildGroupFLTK(ui_def, params, _lst);
}

Fl_Group *WidgetBuilder::BuildGroupFLTK(CPPluginUIDef &ui_def, CPParamList &params, widget_list_t &_lst)
{
	CPPluginUIWidget **p = 0;
	Fl_Group *_grp = 0;
	Fl_Widget *_wdgt = 0;
	
	int cnt  = 0 ; 

	if ( ui_def.num > 0 )
	{
		p = ui_def.widgets;
		_grp = new Fl_Group(10, 10, 100, 100);
		while (cnt < ui_def.num)
		{

			switch((*p)->GetType())
			{
			case CPPluginUIWidget::BOOL:
				_wdgt = CreateWidget((CPPluginUIBool *)*p, params);
				_lst.push_back(_wdgt);
				break;
			case CPPluginUIWidget::DIALOG:
				// Should never get here.
				printf("ERROR: WidgetBuilder() Trying to create Dialog\n");
				break;
			case CPPluginUIWidget::LIST:
				_wdgt = CreateWidget((CPPluginUIList *)*p, params);
				_lst.push_back(_wdgt);
				break;
			case CPPluginUIWidget::NUMBER:
				_wdgt = CreateWidget((CPPluginUINumber *)*p, params);
				_lst.push_back(_wdgt);
				break;
			case CPPluginUIWidget::PROGRESS:
				// Should never get here.
				printf("ERROR: WidgetBuilder() Trying to create Progress bar\n");
				break;
			case CPPluginUIWidget::TEXT:
				_wdgt = CreateWidget((CPPluginUIText *)*p, params);
				_lst.push_back(_wdgt);
				break;
			}
			cnt++;
			p++;
		}
		_grp->end();
		if ( _lst.size() > 0 )
		{
			layout(_grp, _lst);
		}
	}
	return _grp;
}

void WidgetBuilder::layout( Fl_Group *_grp, widget_list_t &lst )
{

	widget_list_t::iterator iter = lst.begin();
	int curr_x = 5, curr_y = 15;
	int label_offset = 0;
	int h;
	int w = 10;
	int spacer = 15;

	_grp->resizable(0);
	while (iter != lst.end())
	{
		if ( (*iter)->align() == FL_ALIGN_LEFT )
			label_offset = 60;
		else
			label_offset = 0;
		(*iter)->position(curr_x+label_offset, curr_y);
        h = (*iter)->h();
		if ((*iter)->w() > w)
			w  = (*iter)->w();
		curr_y = curr_y + h + spacer;
		++iter;
	}
	_grp->size(curr_x+w+spacer, curr_y);
	_grp->init_sizes();
}

Fl_Widget *WidgetBuilder::CreateWidget(CPPluginUIDialog *wdgt, CPParamList &params)
{

	Fl_Window *wnd = new Fl_Window(wdgt->GetWidth(), wdgt->GetHeight(), wdgt->GetTitle());
	return wnd;
}

Fl_Widget *WidgetBuilder::CreateWidget(CPPluginUINumber *wdgt, CPParamList &params)
{

	// Set the initial parameter value as default if not set in param list
	CPPluginArg arg;
	arg.pdb_float = (float)wdgt->GetValDouble();
	params.AddParam(wdgt->GetName(), &arg, PDB_FLOAT);


	Fl_Valuator *res = 0;
	// Setup callback data.
	callback_struct_t *_cb_info = new callback_struct_t;
	_cb_info->param_name = wdgt->GetName();
	_cb_info->params = &params;

	// Build widget and attach callback.
	switch(wdgt->GetHint())
	{
	case CPPluginUINumber::BOX:
		res = new Fl_Value_Input(0, 0, 200, 30, wdgt->GetLabel());
		break;
	case CPPluginUINumber::SLIDER:
		res = new Fl_Value_Slider(0, 0, 200, 30, wdgt->GetLabel());
		res->type(FL_HOR_NICE_SLIDER);
		break;
	case CPPluginUINumber::SPIN:
		res = new Fl_Value_Slider(0, 0, 200, 30, wdgt->GetLabel());
		break;
	}
	// Set the value to initial value as specified in param list
	float val = 0.0;
	params.GetParam(wdgt->GetName(), &val);
	res->bounds(wdgt->GetMin(), wdgt->GetMax());
	res->value(double(val));
	res->callback(valuator_callback, _cb_info);

	return res;
}

Fl_Widget *WidgetBuilder::CreateWidget(CPPluginUIBool *wdgt, CPParamList &params)
{
	// Ensure param is set to a default value.
	CPPluginArg arg;
	arg.pdb_int = wdgt->GetVal();
	params.AddParam(wdgt->GetName(), &arg, PDB_INT);

	// Setup callback data.
	callback_struct_t *_cb_info = new callback_struct_t;
	_cb_info->param_name = wdgt->GetName();
	_cb_info->params = &params;
	// Build widget and attach callback.
	Fl_Round_Button *_btn = new Fl_Round_Button(0, 0, 200, 30, wdgt->GetLabel());
	_btn->callback(button_callback, _cb_info);

	//Set button to correct state.
	int val=0;
	params.GetParam(wdgt->GetName(), &val);
	_btn->value(val);
	return _btn;
}

Fl_Widget *WidgetBuilder::CreateWidget(CPPluginUIText *wdgt, CPParamList &params)
{
	return 0;
}

Fl_Widget *WidgetBuilder::CreateWidget(CPPluginUIList *wdgt, CPParamList &params)
{
	// Two choices here - either a dropdown list, or
	// a number of radio buttons. 
	// TODO: Radio/List change over hardcoded at 4, allow user/programmer choice.
	// Setup callback data.
	callback_struct_t *_cb_info = new callback_struct_t;
	_cb_info->param_name = wdgt->GetName();
	_cb_info->params = &params;

	// Initial Setting.
	CPPluginArg arg;
	arg.pdb_int = wdgt->GetSelection();
	params.AddParam(wdgt->GetName(), &arg, PDB_INT);


	int x_pos = 10, y_pos = 15;
	if ( wdgt->GetEntriesCount() > 4 ) // More than 4 do selection list.
	{
		Fl_Choice* _choice = new Fl_Choice(0, 0, 100, 30, wdgt->GetLabel());
		for(int i = 0; i < wdgt->GetEntriesCount(); i++)
			_choice->add(wdgt->GetEntries()[i], "", choice_callback, _cb_info);
		// Set chosen entry
		_choice->value(arg.pdb_int);

		return _choice;
		_choice->callback(list_callback, _cb_info);
	}
	else // 4 or less do radio buttons.
	{
		CinePaintFrame *_frame = new CinePaintFrame(0,0,200,(UIUtils::DEFAUTL_ROW_HEIGHT * (wdgt->GetEntriesCount()+1)) + (UIUtils::DEFAULT_Y_PAD * 1),wdgt->GetLabel(), FL_ENGRAVED_FRAME);
		Fl_Round_Button *_btn;
		// Add the buttons to the frame.
		for(int i = 0; i < wdgt->GetEntriesCount(); i++)
		{
			_btn = new Fl_Round_Button(x_pos, y_pos , 50, 30, wdgt->GetEntries()[i]);
			_btn->type(FL_RADIO_BUTTON);
			_btn->callback(radio_callback, _cb_info);
			// Set selected.
			if ( i == wdgt->GetSelection() )
				_btn->value(1);
			y_pos += UIUtils::DEFAULT_Y_PAD+30;
//			_btn->labelsize();
		}
		_frame->end();
		return _frame;
	}
}


//===============================
// Callbacks for the Ok and Cancel Buttons.

void WidgetBuilder::cancel_callback(Fl_Widget* w, void* data)
{
	CPParamList *params = (CPParamList *)data;
	CPPluginArg arg;
	arg.pdb_int = 0;
	params->AddParam("response", &arg, PDB_INT );
	((Fl_Window *)w->parent())->hide();
}

void WidgetBuilder::ok_callback(Fl_Widget* w, void* data)
{
	CPParamList *params = (CPParamList *)data;
	CPPluginArg arg;
	arg.pdb_int = 1;
	params->AddParam("response", &arg, PDB_INT );
	((Fl_Window *)w->parent())->hide();
}

void WidgetBuilder::valuator_callback(Fl_Widget* w, void* data)
{
	callback_struct_t *_cb_info = (callback_struct_t *)data;
	CPPluginArg arg;
	arg.pdb_float = ((Fl_Valuator *)w)->value();
	_cb_info->params->SetParam(_cb_info->param_name, &arg, PDB_FLOAT);
}

void WidgetBuilder::button_callback(Fl_Widget* w, void* data)
{
	callback_struct_t *_cb_info = (callback_struct_t *)data;
	CPPluginArg arg;
	arg.pdb_int = (int)((Fl_Button *)w)->value();
	_cb_info->params->SetParam(_cb_info->param_name, &arg, PDB_INT);
}

// @TODO need to think about where this really goes
void WidgetBuilder::choice_callback(Fl_Widget* w, void* data)
{
}

void WidgetBuilder::radio_callback(Fl_Widget* w, void* data)
{
	callback_struct_t *_cb_info = (callback_struct_t *)data;
	Fl_Round_Button *_btn = (Fl_Round_Button *)w;
	Fl_Group *_grp = _btn->parent();
	CPPluginArg arg;
	arg.pdb_int = _grp->find(w);
	_cb_info->params->SetParam(_cb_info->param_name, &arg, PDB_INT);


}
void WidgetBuilder::list_callback(Fl_Widget* w, void* data)
{
	callback_struct_t *_cb_info = (callback_struct_t *)data;
	CPPluginArg arg;
	arg.pdb_int = ((Fl_Choice *)w)->value();
	_cb_info->params->SetParam(_cb_info->param_name, &arg, PDB_INT);
}

