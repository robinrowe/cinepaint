/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ColorBox - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ColorBox.cpp,v 1.1 2006/08/02 17:43:45 lamfada Exp $
 */

#include <gui/ColorBox.h>
#include <plug-ins/pdb/Color.h>
#include <app/ColorChangeListener.h>
#include <app/ColorManager.h>
#include <gui/CinePaintAppUI.h>
#include <gui/UIUtils.h>
#include <FL/fl_draw.H>
#include <FL/Fl.H>
#include <algorithm>

/** default size ratio of the color chip */
static const double DEFAULT_CHIP_SIZE = 0.55;

/** default x pad from edge to color chip */
static const double DEFAULT_X_PAD = 0.1;

/** default y pad from edge to color chip */
static const double DEFAULT_Y_PAD = 0.1;

/** default size ratio of the additional action areas */
static const double DEFAULT_XTRA_SIZE = 0.25;

/** default line width for the swap colors arc */
static const double DEFAULT_ARC_WIDTH = 0.12;

/** default swap color arrow head size */
static const double DEFAULT_ARROW_SIZE = 0.30;

/** default pad between the fltk box and its filled rectangle (frame border width)
    - this must be defined in FLTK somewhere? */
static const int DEFAULT_BOX_PAD = 2;

/**
 * Constructs a new ColorBox using the specified ColorManager for initialization values
 * The ColorBox constructor registers the newly constructed ColorBox as a ColorChangeListener
 * of the specified ColorManager to recieve notifications of color change events.
 *
 * @param x widget x position
 * @param y widget y position
 * @param w widget width
 * @param h widget height
 * @param color_manager
 */
ColorBox::ColorBox(int x, int y, int w, int h, ColorManager& color_manager)
: Fl_Widget(x, y, w, h), m_color_manager(color_manager)
{
	m_color_manager.AddColorChangeListener(this);
}

/**
 * Destructor
 * The ColorBox is removed as a ColrChangeListener from the ColorManager
 */
ColorBox:: ~ColorBox()
{
	m_color_manager.RemoveColorChaneListener(this);
}

void ColorBox::draw()
{
	if(damage() & FL_DAMAGE_ALL)
	{
		draw_box();
	}

	// draw the color chips
	int _rect_width = static_cast<int>(this->w() * DEFAULT_CHIP_SIZE);
	int _rect_height = static_cast<int>(this->h() * DEFAULT_CHIP_SIZE);
	int _pad_x = static_cast<int>(this->w() * DEFAULT_X_PAD);
	int _pad_y = static_cast<int>(this->h() * DEFAULT_Y_PAD);

	fl_draw_box(FL_UP_FRAME, this->x() + (this->w() - _rect_width - _pad_x), this->y() + (this->h() - _rect_height - _pad_y), _rect_width, _rect_height, FL_BACKGROUND_COLOR);
	fl_rectf(this->x() + (this->w() - _rect_width - _pad_x) + DEFAULT_BOX_PAD, this->y() + (this->h() - _rect_height - _pad_y) + DEFAULT_BOX_PAD, _rect_width - (2 * DEFAULT_BOX_PAD), _rect_height - (2 * DEFAULT_BOX_PAD), m_color_manager.GetBackGround().GetRed8(), m_color_manager.GetBackGround().GetGreen8(), m_color_manager.GetBackGround().GetBlue8());
	fl_draw_box(FL_DOWN_FRAME, this->x() + _pad_x, this->y() + _pad_y, _rect_width, _rect_height, FL_BACKGROUND_COLOR);
	fl_rectf(this->x() + _pad_x + DEFAULT_BOX_PAD, this->y() + _pad_y + DEFAULT_BOX_PAD, _rect_width - (2 * DEFAULT_BOX_PAD), _rect_height - (2 * DEFAULT_BOX_PAD), m_color_manager.GetForeGround().GetRed8(), m_color_manager.GetForeGround().GetGreen8(), m_color_manager.GetForeGround().GetBlue8());

	// draw the swap arc
	int _xtra_width = static_cast<int>(this->w() * DEFAULT_XTRA_SIZE);
	int _xtra_height = static_cast<int>(this->h() * DEFAULT_XTRA_SIZE);
	fl_color(FL_FOREGROUND_COLOR);
#undef min
	fl_line_style(FL_SOLID, static_cast<int>(DEFAULT_ARC_WIDTH * std::min(_xtra_width, _xtra_height)));

	int _arrow_size = static_cast<int>(DEFAULT_ARROW_SIZE * std::min(_xtra_width, _xtra_height));
	int _arc_x_pos = this->x() + this->w() - (2 * _xtra_width) - _pad_x;
	int _arc_y_pos = this->y() + _arrow_size + _pad_y;
	int _arc_width = (_xtra_width * 2) - _arrow_size;
	int _arc_height = (_xtra_height * 2) - _arrow_size - _pad_y;
	fl_arc(_arc_x_pos, _arc_y_pos, _arc_width, _arc_height, 0, 90);

	// top left swap indicator arrow head
	fl_polygon(_arc_x_pos + (_arc_width /2), _arc_y_pos, 
		_arc_x_pos + (_arc_width /2) + _arrow_size, _arc_y_pos + _arrow_size,
		_arc_x_pos + (_arc_width /2) + _arrow_size, _arc_y_pos - _arrow_size);

	// bottom right swap indicator arrow head
	fl_polygon(_arc_x_pos + _arc_width, _arc_y_pos + (_arc_height / 2), 
		_arc_x_pos + _arc_width + _arrow_size, _arc_y_pos + (_arc_height / 2) - _arrow_size,
		_arc_x_pos + _arc_width - _arrow_size, _arc_y_pos + (_arc_height / 2) - _arrow_size);

	// draw the default color chip
	int _dft_rect_width = static_cast<int>(_xtra_width * DEFAULT_CHIP_SIZE);
	int _dft_rect_height = static_cast<int>(_xtra_height * DEFAULT_CHIP_SIZE);
	fl_rectf(this->x() + _xtra_width - _dft_rect_width + _pad_x, this->y() + this->h() - _dft_rect_height - _pad_y, _dft_rect_width, _dft_rect_height, ColorManager::GetDefaultBackground().GetRed8(), ColorManager::GetDefaultBackground().GetGreen8(), ColorManager::GetDefaultBackground().GetBlue8());
	fl_rectf(this->x() + _pad_x, this->y() + this->h() - _xtra_height - _pad_y, _dft_rect_width, _dft_rect_height, ColorManager::GetDefaultForeground().GetRed8(), ColorManager::GetDefaultForeground().GetGreen8(), ColorManager::GetDefaultForeground().GetBlue8());
}

int ColorBox::handle(int event)
{
	int ret = 0;

	switch(event)
	{
		case FL_RELEASE:
		{
			// is the click inside this widget,
			if(Fl::event_inside(this))
			{
				// if so process where we were clicked and take action
				process_click();
			}

			ret = 0;
			break;
		}
		default:
		{
			ret = Fl_Widget::handle(event);
		}
	}

	return(ret);
}


/**
 * Invoked when the a Color change is made by the ColorManager
 *
 * @param color_type the color which has changed
 * @param color the new color
 */
void ColorBox::ColorChanged(ColorChangeListener::ColorType color_type, const Color& color)
{
	// we are not really interested in the chage, only that a change has occured, we simply redraw
	// we cannot be sure of which thread has caused the change, so we need to lock fltk
	Fl::lock();
	this->redraw();
	Fl::unlock();
}


/**
 * Processes a click upon this widgets and updates the ColorManager as required
 *
 */
void ColorBox::process_click()
{
	// we need to determine if the click happened on the swap color area, the default chip area, or color chip area
	int _xtra_width = static_cast<int>(this->w() * DEFAULT_XTRA_SIZE);
	int _xtra_height = static_cast<int>(this->h() * DEFAULT_XTRA_SIZE);
	int _pad_x = static_cast<int>(this->w() * DEFAULT_X_PAD);
	int _pad_y = static_cast<int>(this->h() * DEFAULT_Y_PAD);

	if(Fl::event_inside(this->x() + _pad_x, this->y() + this->h() - _xtra_height - _pad_y, _xtra_width, _xtra_height))
	{
		// inside the default color area
		// this is enough to cause a redraw from the ColorManager change events
		m_color_manager.SetForeGround(ColorManager::GetDefaultBackground());
		m_color_manager.SetBackGround(ColorManager::GetDefaultBackground());
	}
	else if(Fl::event_inside(this->x() + this->w() - _xtra_width - _pad_x, this->y() + _pad_y, _xtra_width, _xtra_height))
	{
		// inside the swap color area
		// this is enough to cause a redraw from the ColorManager change event
		m_color_manager.Swap();
	}
	else
	{
		// assume a click anywhere else means to select a color

		int _rect_width = static_cast<int>(this->w() * DEFAULT_CHIP_SIZE);
		int _rect_height = static_cast<int>(this->h() * DEFAULT_CHIP_SIZE);
		if(Fl::event_inside(this->x(), this->y(), _rect_width, _rect_height))
		{
			CinePaintAppUI::GetInstance().SetColorSelectionDialogVisible(true, ColorChangeListener::FOREGROUND_ENUM);
		}
		else
		{
			CinePaintAppUI::GetInstance().SetColorSelectionDialogVisible(true, ColorChangeListener::BACKGROUND_ENUM);
		}
	}
}
