/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintSpinButton - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintSpinButton.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CINEPAINT_SPIN_BUTTON_H_
#define _CINEPAINT_SPIN_BUTTON_H_

#include "dll_api.h"
#include <FL/fl_draw.H>
#include <FL/Fl_Valuator.H>

/**
 * Simple Spin Button implementation
 *
 */
class CINEPAINT_GUI_API CinePaintSpinButton : public Fl_Valuator
{
	public:
		/**
		 * Constructs a new CinePaintSpinButton with a step of 1.0 and maximum range
		 *
		 * @param x widget x position
		 * @param x widget y position
		 * @param x widget width
		 * @param x widget height
		 * @param label widget test label
		 */
		CinePaintSpinButton(int x, int y, int w, int h, const char* label = 0);

		/**
		 * Desctructor
		 */
		virtual ~CinePaintSpinButton();

	protected:

		/**
		 * Draws this CinePaintSpinButton Widget
		 *
		 */
		void draw();

		/**
		 * handles an FLTK event
		 *
		 * @param e the FLTK event type
		 */
		int handle(int e);

	private:

		/** defined spin directions */
		enum SpinDirectionEnum { SPIN_NONE_ENUM, SPIN_UP_ENUM, SPIN_DOWN_ENUM } ;

		/** current spin direction */
		SpinDirectionEnum m_spin_dir;

		/** previous spin button - used for calculating timeout delays, repeat count etc */
		SpinDirectionEnum m_prev_spin_dir;

		/** counts the number of increments in the same direction - for increment speed control */
		int m_repeat_count;


		/**
		 * Handles a button push to determine if the event occurs within the up and down spin areas.
		 * If the event occured within the up or down spin action area, add a timeout to continually
		 * update the value until the button is released, or the mouse moves off the button
		 *
		 * @return 1 if the event was consumed, 0 otherwise
		 */
		int process_push();

		/**
		 * Stops and automatic update from a mouse click and hold action
		 *
		 * @return 1 if the last mouse event lies within this widget, 0 otherwise
		 */
		int process_release();

		/**
		 * Determines if the last mouse event lies within either of the increment arrows.
		 * If the last mouse position lies outwith this widget, the continual increment
		 * on a mouse click and hold is suspended.
		 *
		 * @return 0 if the event falls outwith this widget, 1 if within
		 */
		int process_drag_position();

		/**
		 * Process a keydown event to determine if we need to increment the value
		 * If either the up or down arrow key was pressed, increment the counter and return
		 * 1 to indicate that the event processed, otherwise return 0
		 *
		 * @return 1 if either the up or doen arrow key was pressed, otherwise 0
		 */
		int process_key_press();

		/**
		 * Process a keyup event.
		 * If either the up or down arrow key was pressed, we cause a redraw on this widget
		 * upon the keyup, and return 1 to indicate this widget reposnded to the event, otherwise return 0
		 *
		 * @return 1 if either the up or doen arrow key was released, otherwise 0
		 */
		int process_key_release();


		/**
		 * Increments the current value of this CinePaintSpinButton by the specified delta value
		 * delta * step() is added to the current value
		 *
		 * @param delta delta * step value for addition to value
		 */
		void increment_val(int delta);

		/**
		 * Sets the current spin direction and increments the repeat count
		 * If the new spin direction differs from the previous value, the repeat count is set to 0
		 *
		 * @param dir the new Dpin Direction
		 */
		void set_spin_dir(CinePaintSpinButton::SpinDirectionEnum dir);

		/**
		 * Calculates the delta increment value based upon the current repeat count
		 * This acts as a spin accelerator value
		 *
		 * @return delta increment value based upon the current repeat count
		 */
		int calc_increment_delta();

		/**
		 * Calculatas the timeout callback delay based upon the current repeat count
		 * This method cts as a spin accelerator
		 *
		 * @return FLTK timeout callback delay in seconds
		 */
		double calc_timeout();

		/**
		 * Static timeout callback, used to repeatedly increment the value as the mouse is held down
		 *
		 */
		static void static_timeout_callback(void* data) { reinterpret_cast<CinePaintSpinButton*>(data)->timeout_callback(); }

		/**
		 * Timeout callback, used to repeatedly increment the value as the mouse is held down
		 *
		 */
		void timeout_callback();

}; /* class CinePaintSpinButton */

#endif /* CINEPAINT_SPIN_BUTTON */
