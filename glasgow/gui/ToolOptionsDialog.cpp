/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ToolOptionsDialog - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ToolOptionsDialog.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include <gui/ToolOptionsDialog.h>

#include <app/CinePaintEventList.h>
#include <plug-ins/pdb/CinePaintTool.h>
#include "CinePaintAppUI.h"
#include <app/CinePaintApp.h>
#include "UIUtils.h"
#include <app/CinePaintEventFunctor.h>

#include <FL/Fl_Box.H>

#include <stdio.h>


// initialize static members
static const char* DEFAULT_DIALOG_TITLE = "Tool Options" ;

/**
 * Constructs a new ShowToolOptionsPanel with the specified title
 * The dialog initially contains no tool panels to display
 *
 * @param title the dialog title
 */
ToolOptionsDialog::ToolOptionsDialog(const char* title)
: Fl_Window(100, 100, 300, 200, title)
{
	if(!title)
	{
		this->label(DEFAULT_DIALOG_TITLE);
	}

	end();

	m_no_options_panel = 0;
	m_tool_unavail_panel = 0;
}

/**
 * Destructor
 * All managed tool panels are destroyed with the ShowToolOptionsPanel, if this is not the desired
 * effect, the panel should be removed prior to destroying this dialog.
 *
 */
ToolOptionsDialog::~ToolOptionsDialog()
{
	// tool panels are destroyed by FLTK as children of this window
}

/**
 * Registers the specified option_panel with this ToolOptionsDialog with the specified id.
 * Typically, the id should be the name of the too the options_panel represents.
 * This ToolChangeListener takes responsibility for the added panel, releasing its resources
 * when this dialog is released.
 *
 * @param tool_type the tool type to register the panel against
 * @param options_panel the tool options panel to register
 */
void ToolOptionsDialog::RegisterToolOptionsPanel(const char* tool_type, Fl_Group* options_panel)
{
	m_tool_options_panels.insert(PanelPair_t(std::string(tool_type), options_panel));

	options_panel->hide();
	this->add(options_panel);
}

/**
 * Removes and returns the tool options panel registered with the specified id.
 * If no panel is registered with the specified name, 0 is returned
 *
 * @param tool_type the type of the tool options panel to remove
 * @return the removed tool options panel
 */
Fl_Group* ToolOptionsDialog::RemoveToolOptionsPanel(const char* tool_type)
{
	return(0);
}

/**
 * Displays the tool options panel with the specified id.
 * If no tool is registered with the specified id, the default no options panel is diaplyed
 *
 * @param tool_type the registereed type of the panel to display
 */
void ToolOptionsDialog::ShowToolOptionsPanel(std::string tool_type)
{
	// hide all the other panels
	for(int i = 0; i < this->children(); i++)
	{
		this->child(i)->hide();
	}

	Fl_Group* _tool_panel = 0;

	CPPluginUIDef ui;

	// if tool_type is empty, we do not have a valid tool
	if(!tool_type.empty())
	{
		PanelContainer_t::const_iterator citer = m_tool_options_panels.find(tool_type);
		if(citer != m_tool_options_panels.end())
		{
			// and show the one we want
			_tool_panel = citer->second;
		}
		else
		{
			// Get UIDef from Tool in ToolManager and attempt to build it.
			CinePaintTool* _tool = CinePaintApp::GetInstance().GetToolManager().GetSelectedTool();
			if(_tool)
			{
				_tool->GetUIDefs(ui);
				_tool_panel = CinePaintAppUI::GetInstance().GetWidgetBuilder().BuildGroupFLTK(ui, _tool->GetParamList());

				if(_tool_panel)
				{
					RegisterToolOptionsPanel(tool_type.c_str(), _tool_panel);
				}
			}

			if(!_tool_panel)
			{
				if(!m_no_options_panel)
				{
					m_no_options_panel = new DefaultToolOptPanel(0, 0);
					this->add(m_no_options_panel);
				}
				m_no_options_panel->SetName(_tool->GetName());
				_tool_panel = m_no_options_panel;
			}
		}
	}
	else
	{
		// tool_type is set to 0 when we dont have a valid tool, so display a tool unavailable panel
		if(!m_tool_unavail_panel)
		{
			m_tool_unavail_panel = new Fl_Group(0, 0, 200, (2 * UIUtils::DEFAUTL_ROW_HEIGHT));
			Fl_Box* _label = new Fl_Box(0, 0, 200, (2 * UIUtils::DEFAUTL_ROW_HEIGHT), "Tool Unavailable");
			m_tool_unavail_panel->end();

			this->add(m_tool_unavail_panel);
		}
		_tool_panel = m_tool_unavail_panel;
	}

	if(_tool_panel)
	{
		this->size(_tool_panel->w(), _tool_panel->h());
		this->init_sizes();
		_tool_panel->show();
	}
	else
	{
		// shouldn't happen
		printf("Programmer Error: No Tool Panel Available for %s", tool_type.c_str());
	}

	this->redraw();
}




//----------------------------------
// ToolChangeListener implementation

/**
 * Invoked when the currently selected tool is changed
 *
 * @param id the name of the newly selected tool
 */
void ToolOptionsDialog::ToolChanged(const char* tool_type)
{
	CinePaintAppUI::GetInstance().GetUIThreadEventList().PostEvent(new CinePaintEventFunctor1<ToolOptionsDialog, std::string>(this, &ToolOptionsDialog::ShowToolOptionsPanel, std::string(tool_type), false));
}




//---------------
// Nested Classes

// DefaultToolOptPanel

ToolOptionsDialog::DefaultToolOptPanel::DefaultToolOptPanel(int x, int y)
: Fl_Group(x, y, 200, (2 * UIUtils::DEFAUTL_ROW_HEIGHT) + UIUtils::DEFAULT_Y_PAD)
{
	m_tool_name = UIUtils::CreateLabel(0, 0, 200, UIUtils::DEFAUTL_ROW_HEIGHT, "Tool Name", FL_ALIGN_CENTER);
	Fl_Box* _label = UIUtils::CreateLabel(0, UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD, 200, UIUtils::DEFAUTL_ROW_HEIGHT, "This tool has no Options", FL_ALIGN_CENTER);

	this->end();
}

void ToolOptionsDialog::DefaultToolOptPanel::SetName(const std::string& tool_name)
{
	m_tool_label = tool_name;
	m_tool_name->label(m_tool_label.c_str());
	this->redraw();
}

