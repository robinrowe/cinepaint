/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      PluginFunctionBuilder - Spreasheet plugin string builder dialog
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: PluginFunctionBuilder.h,v 1.2 2006/12/21 11:18:07 robinrowe Exp $
 */

#ifndef _PLUGIN_FUNCTION_BUILDER_H_
#define _PLUGIN_FUNCTION_BUILDER_H_

#include "dll_api.h"
#include <gui/CinePaintWindow.h>
#include <gui/PluginBrowser.h>
#include <plug-ins/pdb/World.h>
#include <list>
#include <string>

class Fl_Hold_Browser;
class Fl_Input;
class Fl_Choice;
class Fl_Check_Button;
class Fl_Box;
class CPSpreadsheet;
class CinePaintPlugin;

class PluginFunctionBuilder;

class SelectionListener : public PluginBrowser::PluginSelectionChangeListener
{	public:
		SelectionListener(PluginFunctionBuilder& builder);
		virtual void SelectionChanged(const char* plugin_name);

	protected:

	private:
		PluginFunctionBuilder& m_builder;
};

class CINEPAINT_GUI_API PluginFunctionBuilder : public CinePaintWindow
{	World* world;
	public:
		/**
		 * Constructs a new PluginBrowser
		 *
		 * @param w the width of the dialog
		 * @param h the geight of the dialog
		 * @param title dialog title
		 */
		PluginFunctionBuilder(CPSpreadsheet& spreadsheet,World* world);

		/**
		 * Destructor
		 */
		virtual ~PluginFunctionBuilder();

		/**
		 * Returns the complete function string built by this dialog
		 *
		 * @return the function string created from this dialog
		 */
		std::string GetFunctionString() const;
		void plugin_changed(const char* plugin_named);
	
		
	protected:
	private:


		//------------------
		// Widget Callbacks
		static void static_add_param_cb(Fl_Widget* w, void* d) { static_cast<PluginFunctionBuilder*>(d)->add_param_cb(); }
		static void static_remove_selected_param_cb(Fl_Widget* w, void* d) { static_cast<PluginFunctionBuilder*>(d)->remove_selected_param_cb(); }
		static void static_clear_params_cb(Fl_Widget* w, void* d) { static_cast<PluginFunctionBuilder*>(d)->clear_params_cb(); }
		static void static_value_input_cb(Fl_Widget* w, void* d) { static_cast<PluginFunctionBuilder*>(d)->value_input_cb(); }
		static void static_ok_btn_cb(Fl_Widget* w, void* d) { static_cast<PluginFunctionBuilder*>(d)->ok_btn_cb(); }
		static void static_cancel_btn_cb(Fl_Widget* w, void* d) { static_cast<PluginFunctionBuilder*>(d)->cancel_btn_cb(); }
		static void static_function_input_cb(Fl_Widget* w, void* d) { static_cast<PluginFunctionBuilder*>(d)->function_input_cb(); }
		static void static_edit_state_func_input_cb(Fl_Widget* w, void* d) { static_cast<PluginFunctionBuilder*>(d)->edit_state_func_input_cb(); }

		void add_param_cb();
		void remove_selected_param_cb();
		void clear_params_cb();
		void value_input_cb();
		void ok_btn_cb();
		void cancel_btn_cb();
		void function_input_cb();
		void edit_state_func_input_cb();
		
		
		//--------
		// Widgets
		PluginBrowser* m_plugin_browser;
		Fl_Input* m_function_input;
		Fl_Hold_Browser* m_added_params_browser;
		Fl_Box* m_plugin_label;
		Fl_Choice* m_param_combo;
		Fl_Input* m_value_input;
		Fl_Choice* m_eval_combo;
		Fl_Check_Button* m_editable_checkbox;

		void set_selected_plugin(const char* plugin_name);
		void populate_param_list(const CinePaintPlugin& plugin);
		void populate_evaulations_list(const std::string& cell_name);


		void build_function_string();
		void auto_build_function_string();


		SelectionListener* m_selection_listener;
		CPSpreadsheet& m_spreadsheet;

		char* m_selected_plugin;
		
		
		struct PluginParamRec
		{
			std::string m_param_name;
			std::string m_value;
			std::string m_evaluation;
		};
		
		std::string build_param_string(const PluginParamRec& rec);

		typedef std::list<PluginParamRec*> ParamList;

		/** container for the plugin data records held by this dialog */
		ParamList m_param_data;
		
		void clear_param_list();
		
		bool m_func_str_user_editable;

} ; /* class PluginFunctionBuilder */

#endif /* _PLUGIN_FUNCTION_BUILDER_H_ */
