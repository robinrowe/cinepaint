/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ColorSelectionDialog - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ColorSelectionDialog.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include <gui/ColorSelectionDialog.h>

#include <gui/UIUtils.h>

#include <FL/Fl.H>
#include <FL/Fl_Button.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Color_Chooser.H>
#include <FL/Fl_Widget.H>

#include <plug-ins/pdb/Color.h>
#include <app/ColorChangeListener.h>
#include <app/ColorManager.h>

#include <algorithm>

// initialize static members
static const char* DEFAULT_DIALOG_TITLE = "Color Selection" ;

// these width and height are the recomendataion from the FLTK docs for the Fl_Color_Chooser
static const int COLOR_CHOOSER_WIDTH = 200;
static const int COLOR_CHOOSER_HEIGHT = 95;

ColorSelectionDialog::ColorSelectionDialog(const char* title)
: Fl_Window(100, 100, 200, 200, title)
{
	create_panel(title);

	m_col_man_worker = 0;
	m_show_and_wait = false;
}

ColorSelectionDialog::ColorSelectionDialog(ColorManager& color_manager, const char* title)
: Fl_Window(100, 100, 200, 200, title)
{
	create_panel(title);

	// create a change listener to listen to this and inform the ColorManager of color change updates
	m_col_man_worker = new ColManChangeListener(color_manager);
	this->AddColorChangeListener(m_col_man_worker);

	m_show_and_wait = false;
}

/**
 * Destructor
 */
ColorSelectionDialog::~ColorSelectionDialog()
{
	if(m_col_man_worker)
	{
		delete m_col_man_worker;
		m_col_man_worker = 0;
	}
}


/**
 * Shows the ColorSelectionDialog and waits for the user to select a Color.
 * This method allows a ColorSelectionDialog to be created and used as a one shot
 * color selection dialog, using GetColor to retrieve the selected Color
 *
 */
void ColorSelectionDialog::ShowAndWait()
{
	// @todo still need a way to unset this
	m_show_and_wait = true;

	this->set_modal();
	this->hotspot(this);
	this->show();
    
	while(this->shown())
	{
		Fl::wait();
    }
}


/**
 * Sets the displayed selected Color to c
 * This method does not update any registered ColorChangeListeners
 *
 * @param c the new Color
 */
void ColorSelectionDialog::SetColor(const Color& c)
{
	m_chooser->rgb(c.GetRed64(), c.GetGreen64(), c.GetBlue64());
	m_new_color_chip->SetColor(c);
	m_old_color_chip->SetColor(c);

	m_new_color_chip->damage(FL_DAMAGE_EXPOSE);
	m_old_color_chip->damage(FL_DAMAGE_EXPOSE);
}

/**
 * Gets the selected Color upon the dialog
 *
 * @return the currently selected color
 */
const Color& ColorSelectionDialog::GetColor() const
{
	return(m_new_color_chip->GetColor());
}


/**
 * Sets the update style of this ColorSelectionDialog.
 * If set true, this dialog will post an update message as the color value is changed.
 * If set false, an update is only posted when the choose button is pressed
 *
 * @param rolling_update auto rolling update style
 */
void ColorSelectionDialog::SetAutoRollingUpdate(bool auto_rolling)
{
	m_auto_roll_update = auto_rolling;
}

/**
 * Sets the ColorChangeListener::ColorType posted by this dialog upon color selection change
 *
 */
void ColorSelectionDialog::SetColorTypeUpdate(ColorChangeListener::ColorType color_type)
{
	m_update_color_type = color_type;
}


//----------------
// Change Listener

/**
 * Adds the specified ColorChangeListener to receive notification upon color selection
 * if the specified ColorChangeListener has already been added it is not added again.
 * The resources of the specified ColorChangeListener are not managed by this class
 * The ColorType of the change is set to ColorChangeLister::NOTYPE_ENUM
 *
 * @param ccl the ColorChangeListener to be added
 */
void ColorSelectionDialog::AddColorChangeListener(ColorChangeListener* ccl)
{
	if(std::find(m_color_change_listeners.begin(), m_color_change_listeners.end(), ccl) == m_color_change_listeners.end())
	{
		m_color_change_listeners.push_back(ccl);
	}
}

/**
 * Removes the specified ColorChangeListener
 * The removed ColorChangeListener will no longer receive Color selection notifications from
 * this class
 *
 * @param ccl the ColorChangeListener to be removed
 */
ColorChangeListener* ColorSelectionDialog::RemoveColorChaneListener(ColorChangeListener* ccl)
{
	ColorChangeListener* ret = 0;

	ListenerList_t::iterator iter = std::find(m_color_change_listeners.begin(), m_color_change_listeners.end(), ccl);

	if(iter != m_color_change_listeners.end())
	{
		ret = ccl;
		m_color_change_listeners.erase(iter);
	}

	return(ret);
}



/**
 * Creates the GUI panel for the Color Selection Dialog
 *
 * @param title the title of this dialog
 */
void ColorSelectionDialog::create_panel(const char* title)
{
	this->begin();

	if(!title)
	{
		this->label(DEFAULT_DIALOG_TITLE);
	}

	m_chooser = new Fl_Color_Chooser(0, 0, COLOR_CHOOSER_WIDTH, COLOR_CHOOSER_HEIGHT);
	m_chooser->callback(static_chooser_cb, this);

	int _chip_spacing = 10;
	int _w = (COLOR_CHOOSER_WIDTH - (2 * UIUtils::DEFAULT_X_PAD) - _chip_spacing) / 2;
	int _posx = UIUtils::DEFAULT_X_PAD;
	int _posy = COLOR_CHOOSER_HEIGHT + UIUtils::DEFAULT_Y_PAD;
	
	m_new_color_chip = new ColorChip(_posx, _posy, _w, UIUtils::BTN_LABEL_HEIGHT);
	_posx += _w + _chip_spacing;
	m_old_color_chip = new ColorChip(_posx, _posy, _w, UIUtils::BTN_LABEL_HEIGHT);

	_w = COLOR_CHOOSER_WIDTH / 3;
	_posx = _w / 3;
	_posy += UIUtils::BTN_LABEL_HEIGHT + UIUtils::DEFAULT_Y_PAD;

	Fl_Button* _choose_button = new Fl_Button(_posx, _posy, _w, UIUtils::BTN_LABEL_HEIGHT, "Choose") ;
	_choose_button->callback(static_choose_button_cb, this) ;
	_posx += _w + (_w / 3);

	Fl_Button* _revert_button = new Fl_Button(_posx, _posy, _w, UIUtils::BTN_LABEL_HEIGHT, "Revert") ;
	_revert_button->callback(static_revert_button_cb, this) ;
	_posy += UIUtils::BTN_LABEL_HEIGHT + UIUtils::DEFAULT_Y_PAD;

	this->size(COLOR_CHOOSER_WIDTH, _posy);
	this->init_sizes();
	this->end();

	m_auto_roll_update = true;
	m_update_color_type = ColorChangeListener::NOTYPE_ENUM;
}

//=================
// Widget callbacks

void ColorSelectionDialog::chooser_cb()
{
	Color c(m_chooser->r(), m_chooser->g(), m_chooser->b(), 1.0);
	m_new_color_chip->SetColor(c);

	if(m_auto_roll_update)
	{
		post_color_changed_update(c);
	}

	m_new_color_chip->damage(FL_DAMAGE_EXPOSE);
}

void ColorSelectionDialog::choose_button_cb()
{
	Color c(m_chooser->r(), m_chooser->g(), m_chooser->b(), 1.0);
	m_old_color_chip->SetColor(c);
	m_old_color_chip->damage(FL_DAMAGE_EXPOSE);
	
	post_color_changed_update(c);

	// if we are in show_and_wait mode, hide the dialog to end the Fl::wait loop
	if(m_show_and_wait)
	{
		this->hide();
	}
}

void ColorSelectionDialog::revert_button_cb()
{
	const Color& _old_color = m_old_color_chip->GetColor();
	m_new_color_chip->SetColor(_old_color);

	m_chooser->rgb(_old_color.GetRed64(), _old_color.GetGreen64(), _old_color.GetBlue64());
	m_new_color_chip->damage(FL_DAMAGE_EXPOSE);

	post_color_changed_update(_old_color);

	// if we are in show_and_wait mode, hide the dialog to end the Fl::wait loop
	if(m_show_and_wait)
	{
		this->hide();
	}
}


/**
 * post a ColorChanged message to all registered ColorChangeListeners
 *
 * @param color_type the color to have changed
 * @param c the new Color
 */
void ColorSelectionDialog::post_color_changed_update(const Color& c)
{
	for(ListenerList_t::const_iterator citer = m_color_change_listeners.begin(); citer != m_color_change_listeners.end(); ++citer)
	{
		(*citer)->ColorChanged(m_update_color_type, c);
	}
}






//---------------
// Nested classes

// ColorChip

ColorSelectionDialog::ColorChip::ColorChip(int x, int y, int w, int h)
: Fl_Widget(x,y,w,h), m_color(0.0, 0.0, 0.0, 0.0)
{
	this->box(FL_ENGRAVED_FRAME);
}

void ColorSelectionDialog::ColorChip::SetColor(const Color& c)
{
	m_color.Set(c);
}

const Color& ColorSelectionDialog::ColorChip::GetColor() const
{
	return(m_color);
}

void ColorSelectionDialog::ColorChip::draw()
{
	if(damage() & FL_DAMAGE_ALL)
	{
		draw_box();
	}
    
	fl_rectf(x() + Fl::box_dx(box()), y() + Fl::box_dy(box()), w() - Fl::box_dw(box()), h() - Fl::box_dh(box()),
		m_color.GetRed8(), m_color.GetGreen8(), m_color.GetBlue8());
}



// ColManChangeListener

ColorSelectionDialog::ColManChangeListener::ColManChangeListener(ColorManager& color_manager)
: m_color_manager(color_manager)
{}

void ColorSelectionDialog::ColManChangeListener::ColorChanged(ColorType color_type, const Color& color)
{
	switch(color_type)
	{
		case ColorChangeListener::FOREGROUND_ENUM:
		{
			m_color_manager.SetForeGround(color);
			break;
		}
		case ColorChangeListener::BACKGROUND_ENUM:
		{
			m_color_manager.SetBackGround(color);
			break;
		}
		case ColorChangeListener::NOTYPE_ENUM:
		default:
			break;
	}

	
}
