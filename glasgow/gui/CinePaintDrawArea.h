/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                     CinePaintDrawArea - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintDrawArea.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CINEPAINT_DRAW_AREA_H_
#define _CINEPAINT_DRAW_AREA_H_

#include "dll_api.h"
#include <FL/Fl_Widget.H>
#include <gui/Projection.h>
#include <plug-ins/pdb/ListenerList.h>
#include <plug-ins/pdb/ImageChangeListener.h>
#include "DrawAreaImageChangeListener.h"

// Forward declarations
class CinePaintMouseListener;
class CinePaintImage;
class CinePaintWidgetListener;
class Drawable;

/**
 * CinePaintDrawArea represents the on screen viewable area of a CinePaaintImage.
 * CinePaintDrawArea is responsible for tracking mouse movement and passing events off to any
 * registered DrawAreaMouseListeners for processing, for example i.e. drawing tools etc.
 *
 */
class CINEPAINT_GUI_API CinePaintDrawArea : public Fl_Widget
{	friend class DrawAreaImageChangeListener;
	public:
		/**
		 * Constructs a new CinePaintDrawArea with the specified size and position
		 *
		 * @param x widget x position
		 * @param y widget y position
		 * @param w widget width
		 * @param h widget height
		 */
		CinePaintDrawArea(int x, int y, int w, int h);

		/**
		 * Constructs a new CinePaintDrawArea with the specified size and position to dispay the specified CinePaintImage
		 *
		 * @param x widget x position
		 * @param y widget y position
		 * @param w widget width
		 * @param h widget height
		 * @param image the CinePaintIamge to display
		 */
		CinePaintDrawArea(int x, int y, int w, int h, CinePaintImage* image, AbstractRenderer* renderer);

		/**
		 * Destructor - destroys only the Draw Area resources, not the underlying CinePaintImage
		 *
		 */
		~CinePaintDrawArea();


		//-----------------------
		// CinePaintImage methods

		/**
		 * Sets the CinePaintImage this CinePaintDrawArea display
		 *
		 * @param image the CinePaintImage to display
		 * @param renderer the AbstractRenderer used to render the image this draw area displays
		 */
		void SetImage(CinePaintImage* image, AbstractRenderer* renderer);

		/**
		 * Returns the CinePaintImage diaplayed by this CinePaintDrawArea
		 *
		 * @return the CinepaintImage being displayed
		 */
		CinePaintImage* GetImage() const;


		/**
		 * Returns access to the Projection used to provide image data for this draw area
		 *
		 * @return access to the Projection used to provide image data
		 */
		Projection& getProjection();

		/**
		 * Returns the Glass Layer for this Draw Area.
		 * The glass layer is a trasnsparent layer ontop of the Projection data.
		 * It may be used for temporary drawing ontop of the projection, for example
		 * to draw a selection area as the user drags the mouse.
		 * The data upon the Glass layer is not safe, another process may override
		 * this data, or clear the layer completely
		 *
		 * @return the glass layer of this DrawArea
		 */
		Drawable* GetGlassLayer();



		//----------------------------
		// Overriden FL_Widget methods

		/**
		 * Resizes and position this Widget to the specified values.
		 * This implementation calls FL_Widget::resize but also informs all registered
		 * CinePaintWidgetListerns of the change.
		 *
		 * @param x the x location
		 * @param y the y location
		 * @param w the widget width
		 * @param h the widget height
		 */
		virtual void resize(int x, int y, int w, int h);

		/**
		 * Sets the size of thie widget to the specified size.
		 * This methods is a convenience method for resize, hiding FL_Widget::size inorder to
		 * provide notifications of widget events to registered CinePaintWidgetListeners
		 *
		 * @param w the widget width
		 * @param h the widget height
		 */
		void size(short w, short h);



		//--------------
		// Canvas Events

		/**
		 * Adds the specified CinePaintMouseListener to receive mouse events from this CinePaintDrawArea
		 * if the specified CinePaintMouseListener has already been added it is not added again.
		 * The resources of the specified CinePaintMouseListener are not managed by this class
		 * It is safe to assume that generated events occur within the UI event loop.
		 *
		 * @param cml the CinePaintMouseListener to be added
		 */
		void AddMouseListener(CinePaintMouseListener* cml);

		/**
		 * Removes the specified CinePaintMouseListener.
		 * The removed CinePaintMouseListener will no longer receive mouse events from this CinePaintDrawArea
		 * It is safe to assume that generated events occur within the UI event loop.
		 *
		 * @param cml the CinePaintMouseListener to be removed
		 */
		CinePaintMouseListener* RemoveMouseListener(CinePaintMouseListener* cml);

		/**
		 * Adds the specified CinePaintWidgetListener to receive widget events from this CinePaintDrawArea
		 * It is NOT  safe to assume that generated events occur within the UI event loop.
		 *
		 * @param cwl the CinePaintWidgetListener to be added
		 */
		void AddWidgetListener(CinePaintWidgetListener* cwl);

		/**
		 * Removes and returns the specified CinePaintWidgetListener.
		 *
		 * @param cwl the CinePaintWidgetListener to be removed
		 */
		CinePaintWidgetListener* RemoveWidgetListener(CinePaintWidgetListener* cwl);
		
		
		virtual void repaint(int x, int y, int w, int h);


	protected:

		/**
		 * handles an FLTK event
		 * This method translates the event into an appropriate CanvasMouseListener event and informs
		 * all registered CanvasMouseListeners
		 *
		 * @param e the FLTK event type
		 * @return non-zero if the event was handled by this class, zero otherwise
		 */
		int handle(int e);

		/**
		 * Draws this CinePaintSpinButton Widget
		 *
		 */
		void draw();

	private:
		/** the image this CinePaintCanvas displays */
		CinePaintImage* m_image;

		/** the Projection of the Image */
		Projection m_projection;

		/** glass layer, transparent layer for drawing temporary (selection drag area etc.) on top of the Projection*/
		Drawable* m_glass_layer;

		/**
		 * Translates the specified coordinates into image space coordinates
		 *
		 */
		void event_to_image_space(int& x, int& y);
		void image_to_event_space(int& x, int& y);

		typedef ListenerList<CinePaintMouseListener> MouseListenerList_t;
		MouseListenerList_t m_mouse_listeners;

		typedef ListenerList<CinePaintWidgetListener> WidgetListenerList_t;
		WidgetListenerList_t m_widget_listeners;

		DrawAreaImageChangeListener m_image_change_listener;
}; /* class CinePaintDrawArea */

#endif /* _CINEPAINT_DRAW_AREA_H_ */
