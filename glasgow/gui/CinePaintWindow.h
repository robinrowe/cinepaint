/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintWindow - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintWindow.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CINEPAINT_WINDOW_H_
#define _CINEPAINT_WINDOW_H_

#include "dll_api.h"
#include <FL/Fl_Window.H>
#include <plug-ins/pdb/enums.h>

/**
 * Extended Window providing a modal show and wait method and auto destruction on window closing
 * Window Auto deletion registeres a callback from the close event to add a further deletion
 * callback which is executed within the FLTK idle loop. The idle callback deletes the resource
 * of this window outside the original window close callback, allowing the resource to be safely
 * deleted
 *.
 * Note: Be clear on what is heppening within the deletion callback, this Window is deleted using
 *       delete, therefore the window must have been constructed with the new operator, if this
 *       was not the case, SetDeleteOnClose must be false, and the user must rely upon whatever
 *       standard c++ technique is appropriate to handle resources.
 */
class CINEPAINT_GUI_API CinePaintWindow : public Fl_Window
{
	public:
		/**
		 * Constructs a new Window of the specified size.
		 *
		 * @param x
		 * @param y
		 * @param title
		 */
		CinePaintWindow(int x, int y, const char* title = 0);

		/**
		 * Destructor
		 */
		virtual ~CinePaintWindow();

		void SetDeleteOnClose(bool delete_on_close);
		bool GetDeleteOnClose() const;

		/**
		 * Shows this window and waits until the window is hidden again.
		 * The return value is used to indicates the close state of this window. For a
		 * 'dialog', the return may be used to indicate the choosen option, for example,
		 * OK or Cancel. For a plain window, this may simply be set to NONE_ENUM.
		 *
		 * @return DialogResponse indicating the
		 */
		DialogResponse ShowAndWait();

	protected:

		void SetResponse(DialogResponse response);
		DialogResponse GetResponse() const;

	private:

		bool m_delete_on_close;

		static void window_close_callback(Fl_Widget* window, void* data);
		static void window_close_idle_callback(void* data);

		DialogResponse m_window_response;

}; /* CinePaintWindow */

#endif /* _CINEPAINT_WINDOW_H_ */
