/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      PluginFunctionBuilder - Spreasheet plugin string builder dialog
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: PluginFunctionBuilder.cpp,v 1.2 2006/12/21 11:18:07 robinrowe Exp $
 */

#include "PluginFunctionBuilder.h"
#include <utility/CellParser.h>
#include "CPSpreadsheet.h"
#include <gui/CinePaintFrame.h>
#include <gui/UIUtils.h>
//#include "CPDAG.h"
#include "CPDAGData.h"
#include "CPDAGEvaluator.h" // req. for IN_PARAM_NAME_MODIFIER
#include <app/CinePaintApp.h>
#include <plug-ins/pdb/PDB.h>
#include <FL/Fl_Hold_Browser.H>
#include <FL/Fl_Input.H>
#include <Fl/Fl_Box.H>
#include <Fl/Fl_Choice.H>
#include <Fl/Fl_Button.H>
#include <FL/Fl_Check_Button.H>

static const int DEFAULT_WINDOW_WIDTH = 600;
static const int DEFAULT_WINDOW_HEIGHT = 600;

static const char* IN_PARAM_NAME_MOD_SUFFIX = "(modified input)";

PluginFunctionBuilder::PluginFunctionBuilder(CPSpreadsheet& spreadsheet,World* world)
: CinePaintWindow(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, "Plugin Function Builder"), m_spreadsheet(spreadsheet)
{	this->world=world;
	int y_pos = 0;
	int x_pos = 0;

	m_func_str_user_editable = false;


	// frame spacing values
	int frame_border = 5;

	// @[Hack] hack for label text height, we cant get the text height until the first time its drawn, so we fake it!!!
	int frame_offset_y = (frame_border * 2) + 20;

	// total additional frame height added to total widget height
	int frame_height_extra = frame_offset_y + (frame_border * 2);
	int frame_internal_x_offset = 2 * frame_border;

	// calc the space required for the various components
	int param_input_h = (4 * UIUtils::DEFAUTL_ROW_HEIGHT) + UIUtils::BTN_LABEL_HEIGHT + (4 * UIUtils::DEFAULT_Y_PAD);

	int plugin_browser_height = DEFAULT_WINDOW_HEIGHT - 
		((param_input_h + frame_height_extra) // param browser
		+ UIUtils::DEFAULT_Y_PAD
		+ ((2 * UIUtils::DEFAUTL_ROW_HEIGHT) + UIUtils::DEFAULT_Y_PAD + frame_height_extra) // func string
		+ UIUtils::DEFAULT_Y_PAD
		+ UIUtils::BTN_LABEL_HEIGHT // panel btns
		+ UIUtils::DEFAULT_Y_PAD);

	int panel_w = (this->w() - UIUtils::DEFAULT_X_PAD) / 2;

	m_plugin_browser = new PluginBrowser(x_pos, y_pos, DEFAULT_WINDOW_WIDTH, plugin_browser_height,0,world);
	y_pos += plugin_browser_height;



	//
	// plugin params input panel
	//
	int tmp_y_pos = y_pos;
	CinePaintFrame* param_input_frame = new CinePaintFrame(x_pos, y_pos, panel_w, param_input_h + frame_height_extra, "Plugin Parameters", FL_ENGRAVED_FRAME);
	int param_label_offset = (panel_w - (2 * frame_internal_x_offset)) / 3;
	int internal_frame_width = panel_w - (2 * frame_internal_x_offset);

	// y pad from bottom of plugin browser
	y_pos += frame_offset_y;
	x_pos += frame_internal_x_offset;

	UIUtils::CreateLabel(x_pos, y_pos, param_label_offset, UIUtils::DEFAUTL_ROW_HEIGHT, "Plugin:", FL_ALIGN_RIGHT);
	m_plugin_label = UIUtils::CreateLabel(x_pos + param_label_offset, y_pos, internal_frame_width - param_label_offset, UIUtils::DEFAUTL_ROW_HEIGHT, 0);
	m_plugin_label->box(FL_DOWN_BOX);
	y_pos += UIUtils::DEFAULT_Y_PAD + UIUtils::DEFAUTL_ROW_HEIGHT;

	m_param_combo = new Fl_Choice(x_pos + param_label_offset, y_pos, internal_frame_width - param_label_offset, UIUtils::DEFAUTL_ROW_HEIGHT, "Parameter:");
	y_pos += UIUtils::DEFAULT_Y_PAD + UIUtils::DEFAUTL_ROW_HEIGHT;

	m_value_input = new Fl_Input(x_pos + param_label_offset, y_pos, internal_frame_width - param_label_offset, UIUtils::DEFAUTL_ROW_HEIGHT, "Value:");
	m_value_input->callback(static_value_input_cb, this);
	m_value_input->when(FL_WHEN_CHANGED);
	y_pos += UIUtils::DEFAULT_Y_PAD + UIUtils::DEFAUTL_ROW_HEIGHT;

	m_eval_combo = new Fl_Choice(x_pos + param_label_offset, y_pos, internal_frame_width - param_label_offset, UIUtils::DEFAUTL_ROW_HEIGHT, "Evaluation:");
	m_eval_combo->deactivate();
	y_pos += UIUtils::DEFAULT_Y_PAD + UIUtils::DEFAUTL_ROW_HEIGHT;

	Fl_Button* add_param_btn = new Fl_Button(x_pos + param_label_offset, y_pos, internal_frame_width - param_label_offset, UIUtils::BTN_LABEL_HEIGHT, "Add");
	add_param_btn->callback(static_add_param_cb, this);
	y_pos += UIUtils::DEFAULT_Y_PAD + UIUtils::BTN_LABEL_HEIGHT;

	y_pos += frame_height_extra - frame_offset_y;
	param_input_frame->end();
	
	y_pos += UIUtils::DEFAULT_Y_PAD;



	//
	// plugin param browser panel
	//
	y_pos = tmp_y_pos;
	int param_frame_x_pos = panel_w + UIUtils::DEFAULT_X_PAD;

	CinePaintFrame* param_frame = new CinePaintFrame(param_frame_x_pos, y_pos, panel_w, param_input_h + frame_height_extra, "Added Parameters", FL_ENGRAVED_FRAME);
	y_pos += frame_offset_y;
	int param_browser_h = param_input_h - (UIUtils::DEFAULT_Y_PAD + UIUtils::BTN_LABEL_HEIGHT);
	m_added_params_browser = new Fl_Hold_Browser(param_frame_x_pos + frame_internal_x_offset, y_pos, panel_w - (2 * frame_internal_x_offset), param_browser_h);

	y_pos += param_browser_h + UIUtils::DEFAULT_Y_PAD;

	// param remove buttons
	int param_btn_width = (panel_w - (2 * frame_internal_x_offset) - UIUtils::DEFAULT_X_PAD) / 2;
	Fl_Button* rem_selected_param_btn = new Fl_Button(param_frame_x_pos + frame_internal_x_offset, y_pos, param_btn_width, UIUtils::BTN_LABEL_HEIGHT, "Remove");
	Fl_Button* rem_all_param_btn = new Fl_Button(param_frame_x_pos  + frame_internal_x_offset + UIUtils::DEFAULT_X_PAD + param_btn_width, y_pos, param_btn_width, UIUtils::BTN_LABEL_HEIGHT, "Clear");
	rem_selected_param_btn->callback(static_remove_selected_param_cb, this);
	rem_all_param_btn->callback(static_clear_params_cb, this);

	y_pos += UIUtils::BTN_LABEL_HEIGHT + (frame_height_extra - frame_offset_y);
	param_frame->end();


	//
	// plugin function string input
	//
	y_pos += UIUtils::DEFAULT_Y_PAD;

	CinePaintFrame* function_frame = new CinePaintFrame(0, y_pos, this->w() - (0 * frame_border), (2 * UIUtils::DEFAUTL_ROW_HEIGHT) + UIUtils::DEFAULT_Y_PAD + frame_height_extra, "Plugin Function", FL_ENGRAVED_FRAME);
	y_pos += frame_offset_y;
	m_function_input = new Fl_Input(frame_internal_x_offset, y_pos, this->w() - (2 * frame_internal_x_offset), UIUtils::DEFAUTL_ROW_HEIGHT, 0);
	m_function_input->callback(static_function_input_cb, this);
	m_function_input->when(FL_WHEN_CHANGED);
	y_pos += UIUtils::DEFAULT_Y_PAD + UIUtils::DEFAUTL_ROW_HEIGHT;
	m_editable_checkbox = new Fl_Check_Button(this->w() - 100 - UIUtils::DEFAULT_X_PAD, y_pos, 100, UIUtils::DEFAUTL_ROW_HEIGHT, "Editable");
	m_editable_checkbox->callback(static_edit_state_func_input_cb, this);
	m_editable_checkbox->value(0);

	y_pos += UIUtils::DEFAUTL_ROW_HEIGHT + (frame_height_extra - frame_offset_y);
	function_frame->end();


	//
	// panal control buttons
	//
	y_pos += UIUtils::DEFAULT_Y_PAD;
	int btn_width = 70;
	int btn_x_pos = (this->w() - (3 * btn_width)) / 2;
	Fl_Button* ok_btn = new Fl_Button(btn_x_pos, y_pos, btn_width, UIUtils::BTN_LABEL_HEIGHT, "OK");
	Fl_Button* cancel_btn = new Fl_Button(btn_x_pos + 2 * btn_width, y_pos, btn_width, UIUtils::BTN_LABEL_HEIGHT, "Cancel");

	ok_btn->callback(static_ok_btn_cb, this);
	cancel_btn->callback(static_cancel_btn_cb, this);

	this->end();

	m_selection_listener = new SelectionListener(*this);
	m_plugin_browser->AddSelectionChangeListener(m_selection_listener);
	
	m_selected_plugin = 0;
}

PluginFunctionBuilder::~PluginFunctionBuilder()
{
	clear_param_list();

	if(m_selection_listener)
	{
		delete m_selection_listener;
		m_selection_listener = 0;
	}
}

std::string PluginFunctionBuilder::GetFunctionString() const
{
	std::string s = m_function_input->value();
	return(s);
}


void PluginFunctionBuilder::add_param_cb()
{
	const char* param_name = m_param_combo->text();
	const char* param_value = m_value_input->value();
	const char* eval_value = m_eval_combo->text();

	if(m_selected_plugin && param_name && param_value)
	{
		PluginParamRec* rec = new PluginParamRec();

		rec->m_param_name = param_name;
		rec->m_value = param_value;

		if(eval_value && m_eval_combo->active())
		{
			std::string eval_s(eval_value);
			if(eval_s.rfind(std::string(IN_PARAM_NAME_MOD_SUFFIX), eval_s.length()) == (eval_s.length() - strlen(IN_PARAM_NAME_MOD_SUFFIX)))
			{
				eval_s.erase(eval_s.length() - strlen(IN_PARAM_NAME_MOD_SUFFIX), strlen(IN_PARAM_NAME_MOD_SUFFIX));
				eval_s.insert(0, CPDAGEvaluator::IN_PARAM_NAME_MODIFIER);
			}

			rec->m_evaluation = eval_s;
		}

		m_param_data.push_back(rec);

		std::string s = build_param_string(*rec);
		m_added_params_browser->add(s.c_str(), rec);

		// rebuild the plugin function string
		auto_build_function_string();
	}
}

std::string PluginFunctionBuilder::build_param_string(const PluginParamRec& rec)
{
	std::string s;

	// build the browser display string
	s.append(rec.m_param_name);
	s.append(" = ");
	s.append(rec.m_value);

	if(!rec.m_evaluation.empty())
	{
		s.append(":(");
		s.append(rec.m_evaluation);
		s.append(")");
	}

	return(s);
}

void PluginFunctionBuilder::remove_selected_param_cb()
{
	int selected = m_added_params_browser->value();

	if(selected)
	{
		std::string s = m_added_params_browser->text(selected);
		m_added_params_browser->remove(selected);

		ParamList::iterator iter = m_param_data.begin();
		bool done = false;

		while(iter != m_param_data.end() && !done)
		{
			if(build_param_string(*(*iter)) == s)
			{
				m_param_data.erase(iter);
				done = true;
			}

			++iter;
		}
	}

	// rebuild the plugin function string
	auto_build_function_string();
}

void PluginFunctionBuilder::clear_params_cb()
{
	m_added_params_browser->clear();
	clear_param_list();

	// rebuild the plugin function string
	auto_build_function_string();
}

void PluginFunctionBuilder::value_input_cb()
{
	std::string s = std::string(m_value_input->value());

	if(!s.empty())
	{
		CellParser::StripWhiteSpace(s, true, true);
		
		if(CellParser::IsCellParam(s))
		{
			CellParser cp;
			std::string cell;
			int offset = 0;
			if(CellParser::ExtractFromParenthesis(s, CellParser::CELL_PARAM_NAME, cell, 1, offset, s.length()))
			{
				populate_evaulations_list(cell);
			}
		}
		else
		{
			m_eval_combo->deactivate();
		}
	}
	else
	{
		m_eval_combo->deactivate();
	}
}

void PluginFunctionBuilder::ok_btn_cb()
{
	SetResponse(OK_ENUM);
	this->hide();
}

void PluginFunctionBuilder::cancel_btn_cb()
{
	SetResponse(CANCEL_ENUM);
	this->hide();
}

void PluginFunctionBuilder::function_input_cb()
{
	m_editable_checkbox->value(1);
	m_func_str_user_editable = true;
}

void PluginFunctionBuilder::edit_state_func_input_cb()
{
	m_func_str_user_editable = (m_editable_checkbox->value() != 0);

	if(!m_func_str_user_editable)
	{
		build_function_string();
	}
}

void PluginFunctionBuilder::plugin_changed(const char* plugin_name)
{	set_selected_plugin(plugin_name);
	PDB* pdb=PDB::GetPDB(world);
	if(!pdb)
	{	return;
	}	
	CinePaintPlugin* plugin = pdb->GetPluginInstance(plugin_name);
	if(plugin)
	{	populate_param_list(*plugin);	
		pdb->ReleaseInstance(plugin);
	}
}

void PluginFunctionBuilder::set_selected_plugin(const char* plugin_name)
{
	if(m_selected_plugin)
	{
		delete[] m_selected_plugin;
		m_selected_plugin = 0;
	}

	if(plugin_name)
	{
		m_selected_plugin = new char[strlen(plugin_name) +1];
		strcpy(m_selected_plugin, plugin_name);
	}

	m_plugin_label->label(m_selected_plugin);
}

void PluginFunctionBuilder::populate_param_list(const CinePaintPlugin& plugin)
{
	// clear the previous params list
	m_param_combo->clear();

	CinePaintPlugin::CPPluginParamDefs defs;

	plugin.GetInParamDefs(defs);
	for(int i = 0; i < defs.m_param_count; i++)
	{
		m_param_combo->add(defs.m_params[i].m_name, 0, 0, 0);
	}

	m_param_combo->value(0);
}

void PluginFunctionBuilder::populate_evaulations_list(const std::string& cell_name)
{	const CPDAGData* data = m_spreadsheet.GetCellData(cell_name);
	if(data)
	{	if(data->GetType() == CPDAGData::DAG_PLUGIN)
		{	m_eval_combo->activate();
			const char* plugin_name = data->GetPluginName();
			PDB* pdb=PDB::GetPDB(world);
			if(!pdb)
			{	return;
			}
			CinePaintPlugin* plugin = pdb->GetPluginInstance(plugin_name);
			if(plugin)
			{	// clear the previous params list
				m_eval_combo->clear();
				CinePaintPlugin::CPPluginParamDefs defs;
				// first add the return params
				plugin->GetReturnParamDefs(defs);
				for(int i = 0; i < defs.m_param_count; i++)
				{	m_eval_combo->add(defs.m_params[i].m_name, 0, 0, 0);
				}
				// now add any input params that are modified by the plugin
				// these should be selectable, as outputs, but we alter the name to reflect that
				// the param is a modified input
				plugin->GetInParamDefs(defs);
				for(int i = 0; i < defs.m_param_count; i++)
				{	if(defs.m_params[i].m_modifies)
					{	std::string s(defs.m_params[i].m_name);
						s.append(IN_PARAM_NAME_MOD_SUFFIX);
						m_eval_combo->add(s.c_str(), 0, 0, 0);
					}
				}
				m_eval_combo->value(0);
				pdb->ReleaseInstance(plugin);
			}
		}
	}
	else
	{
		m_eval_combo->deactivate();
	}
}

void PluginFunctionBuilder::build_function_string()
{
	std::string s;

	if(m_selected_plugin)
	{
		s.append("=");
		s.append(m_selected_plugin);
		
		// open plugin params
		s.append("(");

		ParamList::iterator iter = m_param_data.begin();
		while(iter != m_param_data.end())
		{
			ParamList::iterator next = iter;
			next++;

			s.append((*iter)->m_param_name);
			s.append("=");
			s.append((*iter)->m_value);

			if(!(*iter)->m_evaluation.empty())
			{
				s.append(":");
				s.append("(");
				s.append((*iter)->m_evaluation);
				s.append(")");
			}

			if(next != m_param_data.end())
			{
				s.append(", ");
			}

			iter = next;
		}

		// close the plugin params
		s.append(")");
	}

	m_function_input->value(s.c_str());
}

void PluginFunctionBuilder::auto_build_function_string()
{
	// only build the function string if the input box is not editable
	// otherwise we may lose user added params
	// @TODO there is probably a better way of doing this!
	if(!m_func_str_user_editable)
	{
		build_function_string();
	}
}

void PluginFunctionBuilder::clear_param_list()
{
	ParamList::iterator iter = m_param_data.begin();

	while(iter != m_param_data.end())
	{
		PluginParamRec* rec = *iter;
		delete rec;
		iter = m_param_data.erase(iter);
	}
}

SelectionListener::SelectionListener(PluginFunctionBuilder& builder)
: m_builder(builder)
{}

void SelectionListener::SelectionChanged(const char* plugin_name)
{
	m_builder.plugin_changed(plugin_name);
}

