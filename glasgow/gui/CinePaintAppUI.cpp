/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintAppUI - Main Userinterafce instance 
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintAppUI.cpp,v 1.6 2008/01/28 18:22:28 robinrowe Exp $
 */

#include <pthread.h>
#include <stdlib.h>
#include <string>
 // needed for WIN32 to load IDI_ICON1?
 //#include <FL/x.H>
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/fl_ask.H>
//#include <config.h>
#include "CinePaintAppUI.h"
#include "BrushSelectionDialog.h"
#include "ColorSelectionDialog.h"
#include "DocViewManager.h"
#include "InitDialog.h"
#include "LoadSaveDialog.h"
#include "MainToolbar.h"
#include "NewImageDialog.h"
#include "ProgressDialog.h"
#include "PluginBrowser.h"
#include "PluginDialog.h"
#include "ToolOptionsDialog.h"
#include "WidgetBuilder.h"
#include <app/AppSettings.h>
#include <app/BrushList.h>
#include <app/CinePaintEventList.h>
#include <app/CinePaintApp.h>
#include <app/CinePaintEventFunctor.h>
#include <plug-ins/pdb/CinePaintImage.h>
#include <plug-ins/pdb/World.h>
#include <utility/CommandLineParse.h>

// initialize static members
CinePaintAppUI* CinePaintAppUI::m_app_instance = 0;

// Rely on constructor to initialize this.
pthread_t CinePaintAppUI::m_threadID;

CinePaintAppUI::CinePaintAppUI()
{	m_brush_selection_dialog = 0;
	m_color_selection_dialog = 0;
	m_tool_options_dialog = 0;
	m_plugin_browser = 0;
	m_maintain_dialog_resource = false;

	m_mMainToolBar = 0;
	m_init_listener = 0;
	m_run_flag = false;
#ifdef PTW32_STATIC_LIB
//	if(!m_threadID)
	pthread_win32_process_attach_np();
#endif
	m_threadID = pthread_self();
}


CinePaintAppUI::~CinePaintAppUI()
{
	if(m_brush_selection_dialog)
	{
		delete m_brush_selection_dialog;
		m_brush_selection_dialog = 0;
	}

	if(m_color_selection_dialog)
	{
		delete m_color_selection_dialog;
		m_color_selection_dialog = 0;
	}

	if(m_plugin_browser)
	{
		delete m_plugin_browser;
		m_plugin_browser = 0;
	}
}



//============================
// Application install / Setup

/**
 * Show the help information.
 */
void CinePaintAppUI::ShowHelp()
{	printf("Usage: %s [option ...] [files ...]\n", APP_NAME);
	printf("Valid options are:\n");
	printf("  -h --help              Output this help.\n");
	printf("  -v --version           Output version info.\n");
#ifdef BUILD_SHM
	printf("  --no-shm               Do not use shared memory between GIMP and its plugins.\n");
	printf("  --no-xshm              Do not use the X Shared Memory extension.\n");
#endif
#ifndef _WIN32
	printf("  --display <display>    Use the designated X display.\n\n");
#endif
}

/**
 * Display the version
 */
void CinePaintAppUI::ShowVersion()
{
	printf(APP_NAME " version " APP_VERSION "\n");
}

/**
 * Starts the main UI and waits for the UI event loop to exit.
 * This method starts the toolkit event loop. The current thread becomes the
 * event loop thread. This method returns when the UI event loop has completed,
 * i.e. when all windows are closed.
 * This method waits for the CinePaint Core to initialize before displaying the
 * UI. Run must be called upon CinePaintApp before attempting to start the UI.
 *
 * @return non-zero on error
 *         1 == already running
 */
int CinePaintAppUI::Run(CommandLineParse& clp)
{
	if(m_run_flag)
	{
		// already running, dont Run again
		return(1);
	}

	m_threadID = pthread_self();
	m_run_flag = true;

	// hook up an intitialization method
	// this lets us start the main loop before actually doing anything, and get
	// everything done in the main loop
	Fl::add_check(static_init_callback, &clp);

	// give the fltk a kick
	// after an intitial fltk loop, we should have initialized the core and have a UI to display
	// we need this initial check to do some processing and show a first window before Run is called,
	// otherwise Run simply returns as there are no windows onscreen
	Fl::check();

	// now atsrt the main loop
	// this methods waits here until the main loop exits
	return(Fl::run());
}







//===============================
// Tool Dialog Show/Close methods

/**
 * Sets the visibility of the Brush Selection Dialog
 *
 * @param visible visibility of the dialog
 */
void CinePaintAppUI::SetBrushSelectionDialogVisible(bool visible)
{
	if(visible)
	{
		if(!m_brush_selection_dialog)
		{
			m_brush_selection_dialog = new BrushSelectionDialog(0, 0, 100.0, 1.0, 0.0, 0.0, 0.0, 0) ;
			m_brush_selection_dialog->RefreshBrushList(CinePaintApp::GetInstance().GetBrushList());
		}

		m_brush_selection_dialog->show() ;
	}
	else
	{
		if(m_brush_selection_dialog)
		{
			m_brush_selection_dialog->hide() ;

			// @note [claw] how do we destroy an FLTK window cleanly?
			// do we delete the dialog and recreate, or simply hide?
			//if(!m_maintain_dialog_resource)
			//{
				//delete m_brush_selection_dialog;
				//m_brush_selection_dialog = 0;
			//}
		}
	}
}

void CinePaintAppUI::SetColorSelectionDialogVisible(bool visible)
{
	SetColorSelectionDialogVisible(visible, ColorChangeListener::NOTYPE_ENUM);
}

/**
 * Sets the visibility of the color selection dialog
 *
 * @param visible visibility of the dialog
 */
void CinePaintAppUI::SetColorSelectionDialogVisible(bool visible, ColorChangeListener::ColorType color_type)
{
	if(visible)
	{
		if(!m_color_selection_dialog)
		{
			m_color_selection_dialog = new ColorSelectionDialog(CinePaintApp::GetInstance().GetColorManager(), "Color Selection") ;
		}

		m_color_selection_dialog->show();
		m_color_selection_dialog->SetColorTypeUpdate(color_type);
	}
	else
	{
		if(m_color_selection_dialog)
		{
			m_brush_selection_dialog->hide() ;

			// @note [claw] how do we destroy an FLTK window cleanly?
			// do we delete the dialog and recreate, or simply hide?
			//if(!m_maintain_dialog_resource)
			//{
				//delete m_color_selection_dialog;
				//m_color_selection_dialog = 0;
			//}
		}
	}
}

/**
 * Sets the visibility of the Pallettes Dialog
 *
 * @param visible visibility of the dialog
 */
void CinePaintAppUI::SetPalletteDialogVisible(bool visible)
{}

/**
 * Sets the visibility of the Gradient Editor Dialog
 *
 * @param visible visibility of the dialog
 */
void CinePaintAppUI::SetGradEditorDialogVisible(bool visible)
{}

/**
 * Sets the visibility of the Tool Options Dialog
 *
 * @param visible visibility of the dialog
 */
void CinePaintAppUI::SetToolOptionsDialogVisible(bool visible)
{
	if(visible)
	{
		if(!m_tool_options_dialog)
		{
			m_tool_options_dialog = new ToolOptionsDialog("Tool Selection") ;
			m_tool_options_dialog->callback(static_close_tool_dialog_callback, this);

			// register the dialog receive tool change notifications
			ToolManager& _tool_manager = CinePaintApp::GetInstance().GetToolManager();
			_tool_manager.AddToolChangeListener(m_tool_options_dialog);
			CinePaintTool* _tool = _tool_manager.GetSelectedTool();
			if(_tool)
			{
				m_tool_options_dialog->ShowToolOptionsPanel(_tool->GetToolType());
			}
		}

		m_tool_options_dialog->show();
	}
	else
	{
		if(m_tool_options_dialog)
		{
			m_tool_options_dialog->hide();

			// do we delete the dialog and recreate, or simply hide?
			if(!m_maintain_dialog_resource)
			{
				CinePaintApp::GetInstance().GetToolManager().RemoveToolChangeListener(m_tool_options_dialog);

				// Fl::delete_widget requires FLTK >= 1.1.6
				Fl::delete_widget(m_tool_options_dialog);
				m_tool_options_dialog = 0;
			}
		}
	}
}

/**
 * Sets the visibility of the Device Status Dialog
 *
 * @param visible visibility of the dialog
 */
void CinePaintAppUI::SetDeviceStatusDialogVisible(bool visible)
{}


/**
 * Sets the plugin browser window visible
 *
 * @param visible visibility of the plugin browser
 */
void CinePaintAppUI::SetPluginBrowserVisible(bool visible)
{
	if(visible)
	{
		if(!m_plugin_browser)
		{
			m_plugin_browser = new Fl_Window(600, 300, "plug-in browser");
			World* world=CinePaintApp::GetWorld();
			//BUG: leak
			/*PluginBrowser* plugin_browser = */ new PluginBrowser(0, 0, 600, 300, "plug-in browser",world);
			m_plugin_browser->end();
			m_plugin_browser->callback(static_close_plugin_browser_cb, this);
		}

		m_plugin_browser->show();
	}
	else
	{
		if(m_plugin_browser)
		{
			m_plugin_browser->hide();

			// do we delete the dialog and recreate, or simply hide?
			if(!m_maintain_dialog_resource)
			{
				// Fl::delete_widget requires FLTK >= 1.1.6
				Fl::delete_widget(m_plugin_browser);
				m_plugin_browser = 0;
			}
		}
	}
}

//--------------
// AppUI Control 

void CinePaintAppUI::Quit()
{}	


//-----------------
// Document Control

void CinePaintAppUI::NewDocument()
{
	NewImageDialog _dialog(300, -1, "New Image");
	DialogResponse _response = _dialog.ShowAndWait();

	if(_response == OK_ENUM)
	{
		NewImageDialog::NewImageInfo _info;
		_dialog.GetNewImageInfo(_info);

		bool _alpha = (_info.fill == FILL_TRANS_ENUM);
		CinePaintTag _tag(_info.precision, _info.format, _alpha);

		CinePaintDoc* _doc = CinePaintApp::GetInstance().CreateCinePaintImage(_tag, _info.width, _info.height, _info.fill);

		if(_doc)
		{	m_doc_view_manager.OpenView(*_doc);
			CinePaintApp::GetInstance().GetDocList().SetCurrentDoc(*_doc);
		}
		else
		{
			fl_alert("Construction of the new Image Failed");
		}
	}
}

void CinePaintAppUI::NewSpreadSheet()
{
	CinePaintDoc* _doc = CinePaintApp::GetInstance().CreateCPDAG();
	if(_doc)
	{	m_doc_view_manager.OpenView(*_doc);
		CinePaintApp::GetInstance().GetDocList().SetCurrentDoc(*_doc);
	}
	else
	{
		fl_alert("Construction of the Spreadsheet Failed!");
	}
}

void CinePaintAppUI::NewFrameManager()
{
	CinePaintDoc* _doc = CinePaintApp::GetInstance().CreateFrameManager();
	if(_doc)
	{	m_doc_view_manager.OpenView(*_doc);
		CinePaintApp::GetInstance().GetDocList().SetCurrentDoc(*_doc);
	}
	else
	{
		fl_alert("Construction of the Frame Manager Failed!");
	}
}

void CinePaintAppUI::OpenDocument()
{
	// dialog waits until choice is selected
	LoadSaveDialog _load_save_dialog(false);
	DialogResponse _res = _load_save_dialog.ShowAndWait();
	if ( _res == OK_ENUM)
	{
		const char* _filename = _load_save_dialog.GetFileName();
		if(_filename)
		{
			CinePaintDocList& _doc_list = CinePaintApp::GetInstance().GetDocList();
			CinePaintDoc* _doc = _doc_list.OpenDocument(_filename);
			if(_doc)
			{
				// maintain a local working reference to the document
				_doc_list.RefDocument(*_doc);

				m_doc_view_manager.OpenView(*_doc);
				_doc_list.SetCurrentDoc(*_doc);

				// release our local reference to the document
				_doc_list.UnRefDocument(*_doc);
			}
		}
	}
}

bool CinePaintAppUI::SaveDocument(CinePaintDoc& doc)
{
	bool _ret = false;

	if(doc.GetFileName())
	{
        CinePaintApp::GetInstance().GetDocList().SaveDocument(doc);
		_ret = true;
	}
	else
	{
		_ret = SaveAsDocument(doc);
	}

	return(_ret);
}

bool CinePaintAppUI::SaveAsDocument(CinePaintDoc& doc)
{
	bool _ret = false;

	LoadSaveDialog _load_save_dialog(true);
	DialogResponse _res = _load_save_dialog.ShowAndWait();

	if(_res == OK_ENUM)
	{
		const char* _filename = _load_save_dialog.GetFileName();
		if(_filename)
		{
			CinePaintApp::GetInstance().GetDocList().SaveDocument(doc, _filename);
			_ret = true;
		}
	}

	return(_ret);
}

void CinePaintAppUI::RevertDocument(CinePaintDoc& doc)
{
	CinePaintApp::GetInstance().GetDocList().RevertDocument(doc);
}

/**
 * Query the user if the specified document should be saved, and saves ths Document if appropriate.
 * This method checks if the specified Document is dirty, and will query the user to save it if
 * appropriate, returning a release status for the document. If the user saves the document, or chooses
 * to discard any changes made, the document can be safely released, and this method will return true.
 * If the user cancels the operation, the document should not be released and false is returned.
 * 
 * @note This method does not release the document, it is used to query if a document can be released.
 *       this allows us to display the appropriate dialogs before we release the last reference to a
 *       document
 *.
 * @param doc the Document to save and release.
 */
bool CinePaintAppUI::ReleaseAndSaveDocument(CinePaintDoc& doc)
{
	bool ret = true;

	if(doc.IsDirty())
	{
		int _save =  fl_choice("The Document has been modified!", "Cancel", "Save", "Discard");
				
		switch(_save)
		{
			case 0: // cancel
			{
				ret = false;
				break;
			}
			case 1: // save
			{
				ret = SaveDocument(doc);
				break;
			}
			case 2: // Discard
			default:
			{
				ret = true;
				break;
			}
		}
	}

	return(ret);
}





DocViewManager& CinePaintAppUI::GetDocViewManager()
{
	return(m_doc_view_manager);
}






//===============
// Get Thread ID.

/**
 * Returns the FLTK UI event list
 * The returned event list is processes within the FLTK event thread. This provides a convenient
 * mechanism for arbitary threads to provide a process to be run within the FLTK event thread
 * Note that the event list is not real time, but executed after all other events within the
 * idle loop
 *
 * @return UI thread event process list
 */
CinePaintEventList& CinePaintAppUI::GetUIThreadEventList()
{
	return(m_event_list);
}

/**
 * Returns true if the calling thrad is running within the UI thread.
 * This allows a method to determine if it is currently running within the UI thread, which
 * is particularly important for updating the UI,
 * ALL UI updates must take place within the UI thread.
 *
 * If Run has not yet been called upon CinePaintAppUI, the UI thread is currently invalid. In
 * this case, false is always returned
 * 
 * @return true if the calling thread is running within the UI thread, false otherwise
 */
bool CinePaintAppUI::IsUIThread()
{
	return(pthread_equal(CinePaintAppUI::m_threadID, pthread_self()) != 0);
}













//===============
// Static Methods

/**
 * Returns the singleton instance of the CinepaintApp
 * @return the CinePaintAppUI isntance
 */
CinePaintAppUI& CinePaintAppUI::GetInstance()
{
	if(!m_app_instance)
	{	// Create the user interface instance.
		m_app_instance = new CinePaintAppUI();
	}
	return(*m_app_instance) ;
}




//====================
// Instalation Methods

/**
 * Static FLTK callback to initiate the UI initialization
 * We do this as a callback so we can hook-up the callback, and then start the
 * main FLTK event loop immediatly.
 *
 * @param data the Commend Line Arguments
 */
void CinePaintAppUI::static_init_callback(void* data)
{
	CommandLineParse* _clp = reinterpret_cast<CommandLineParse*>(data);
	Fl::remove_check(static_init_callback, _clp);

	CinePaintAppUI& _ui = CinePaintAppUI::GetInstance();
	_ui.init(*_clp);
}

/**
 * UI initialization
 * Performs the User installation, if required
 * If the core is initialized, show the UI.
 * If the CinePaintApp Coreis not currently initialized, Create an InitializationListener
 * and Initialize the core. The InitializationListener handles showing the UI once the 
 * core initialization is complete.
 *
 */
void CinePaintAppUI::init(CommandLineParse& clp)
{	ShowHelp();
	ShowVersion();
	CinePaintApp& _app = CinePaintApp::GetInstance();
	_app.RegisterDialogListener( new PluginDialog::DialogListener());
	_app.RegisterWidgetBuilder(&m_widget_builder);
	// @TODO handle the other init states
	if(_app.GetInitState() == CinePaintApp::INIT_UNINIT_ENUM)
	{	// register an initilization listener and wait for the core to initialize
		m_init_listener = new InitializationListener(*this);
		if(_app.InitInstance(clp, *m_init_listener))
		{	// listen for Document events, open/close etc. and do something about it
			_app.GetDocList().AddDocListener(&m_doc_view_manager);
		}
//			Sleep(10);
	}
	else
	{	// just show the UI ...
		// ... within the UI thread
		show_ui();
	}
}



//===================================
// general Application helper methods


void CinePaintAppUI::show_ui()
{
	if(!m_mMainToolBar)
	{
		m_mMainToolBar = new MainToolBar();
	}
#ifdef _WIN32
//Sets the icon for the window to the passed pointer. You will need to cast the HICON handle to a char * when calling this method. To set the icon using an icon resource compiled with your application use:
	//You can also use the LoadImage() and related functions to load specific resolutions or create the icon from bitmap data. NOTE: 
//You must call Fl_Window::show(argc, argv) for the icon to be used. The Fl_Window::show() method does not bind the icon to the window.	
extern HINSTANCE fl_display;
#ifdef OLD_WAY
	fl_open_display();
	HWND hwnd=fl_xid(m_mMainToolBar);
	HINSTANCE instance=(HINSTANCE)GetWindowLong(hwnd,GWL_HINSTANCE);
#else
	HINSTANCE instance=fl_display;
#endif
///	LPTSTR resource_name=MAKEINTRESOURCE(IDI_ICON1);
///	HICON icon=LoadIcon(instance,resource_name);
//	if(icon)
//	{	SetClassLong(hwnd,GCL_HICON,(LONG)icon);
//	}
///	m_mMainToolBar->icon((char *)icon);
	m_mMainToolBar->show();//__argc,__argv);//BUG: loses window colors
/*	HWND hwnd=fl_xid(m_mMainToolBar);
	if(icon)
	{	SetClassLong(hwnd,GCL_HICON,(LONG)icon);
	}*/
#else
	m_mMainToolBar->show();
#endif
}



/*
bool CinePaintAppUI::do_user_install()
{

	CinePaintApp& _app = CinePaintApp::GetInstance();
	if(_app.IsInstalled())
	{	return true;
	}
	const char* dotdir = GetPathApp();
	InstallDialog _dlg(dotdir);
	switch(_dlg.InstallDialogDo())
	{	case YES_ENUM:
		{	std::string _install_log;
			if(!_app.UserInstall(_install_log))
			{	return false;
			}
			if(YES_ENUM==_dlg.InstallLogDo(_install_log.c_str()))
			{	return true;
			}
			return false;//BUG
		}
		case NO_ENUM:
		{	// the user does not want to install, but we want to run anyways.
			return true;
		}
		case QUIT_ENUM:
		default:
		{	return false;
		}
	}
	return true;
}
*/

//================
// Nested Classes



//----------------------
// CinePaintUIEventList

/**
 * Default constructor, constructs an intially inactive ui event list
 */
CinePaintAppUI::CinePaintUIEventList::CinePaintUIEventList()
{
	m_active = false;
}

/**
 * Process the first element within this Event list.
 * If after processing an event, the event list is empty, SetActive(false)
 * is called to remove this event list processing from the FLTK idle loop
 *
 * @return true if any events were processed, false otherwise
 */
bool CinePaintAppUI::CinePaintUIEventList::Process()
{
	bool _ret = CinePaintEventList::Process();

	if(Size() == 0)
	{
		SetActive(false);
	}

	return(_ret);
}

/**
 * Post an event into the event list to be processed
 * If this event list is not already active and processing the list, SetActive(true)
 * is called process this list within the FLTK idle loop
 *
 * @param e the event to add
 * @return the number of item within the list
 */
size_t CinePaintAppUI::CinePaintUIEventList::PostEvent(CinePaintEvent* e)
{
	size_t _size = CinePaintEventList::PostEvent(e);

	if((_size > 0) && (!m_active))
	{
		SetActive(true);
	}

	return(_size);
}

/**
 * Sets the processing state of this event list
 * If active is set true, this event list is added to the FLTK idle loop
 * for processing the event list, if not already added, if false, it is removed
 * from the idle loop.
 *
 * @param active the activce state of this event loop
 */
void CinePaintAppUI::CinePaintUIEventList::SetActive(bool active)
{
	if(active)
	{
		if(!m_active)
		{
			Fl::add_idle(static_ui_event_loop_cb, this);
			m_active = true;
		}
	}
	else
	{
		if(m_active)
		{
			Fl::remove_idle(static_ui_event_loop_cb, this);
			m_active = false;
		}
	}

	// give the FLTK event loop a kick...
	Fl::awake();
}


/**
 * FLTK Idle loop callback to process the event list
 *
 * @param data instance of CinePaintUIEventList to process
 */
void CinePaintAppUI::CinePaintUIEventList::static_ui_event_loop_cb(void* data)
{
	CinePaintUIEventList* _instance = reinterpret_cast<CinePaintUIEventList*>(data);

	_instance->Process();
}


//================================
// Init Progress Listener Implementation.

CinePaintAppUI::InitializationListener::InitializationListener(CinePaintAppUI& ui)
: m_ui(ui)
{
	m_init_dialog = 0;
	m_init_dialog = new InitDialog(402, 300, "CinePaint Initalization", "spot.splash.ppm");
	m_init_dialog->show();

}

CinePaintAppUI::InitializationListener::~InitializationListener()
{
	if(m_init_dialog)
	{
		m_init_dialog->hide();
		delete m_init_dialog;
		m_init_dialog = 0;
	}
}

void CinePaintAppUI::InitializationListener::ProgressStart(char* ID, char* msg, int stop, int step)
{
	if(CinePaintAppUI::IsUIThread())
	{
		initialization_start();
	}
	else
	{
		m_ui.GetUIThreadEventList().PostEvent(new CinePaintEventFunctor0<InitializationListener>(this, &InitializationListener::initialization_start, false));
	}
}

void CinePaintAppUI::InitializationListener::MessageChanged(char* ID, char* msg)
{
	m_init_dialog->MessageChanged(ID, msg);
}

void CinePaintAppUI::InitializationListener::LimitsChanged(char* ID, int start, int stop, int step)
{
	m_init_dialog->LimitsChanged(ID, start, stop, step);
}

void CinePaintAppUI::InitializationListener::ProgressUpdate(char* ID)
{
	m_init_dialog->ProgressUpdate(ID);
}

void CinePaintAppUI::InitializationListener::ProgressReset(char* ID)
{
	m_init_dialog->ProgressReset(ID);
}

void CinePaintAppUI::InitializationListener::ProgressPosition(char* ID, int pos)
{
	m_init_dialog->ProgressPosition(ID, pos);
}

void CinePaintAppUI::InitializationListener::ProgressComplete(char* ID)
{
	m_ui.GetUIThreadEventList().PostEvent(new CinePaintEventFunctor0<InitializationListener>(this, &CinePaintAppUI::InitializationListener::initialization_complete, true));
}

void CinePaintAppUI::InitializationListener::initialization_start()
{

}

void CinePaintAppUI::InitializationListener::initialization_complete()
{
	switch(CinePaintApp::GetInstance().GetInitState())
	{
		case CinePaintApp::INIT_COMPLETE_ENUM:
		{
			m_ui.show_ui();
			CinePaintApp::GetInstance().RemoveInitProgressListener(this);
			m_init_dialog->hide();
			break;
		}
		case CinePaintApp::INIT_FAILED_ENUM:
		{
			// initialization failed!
			printf("CinePaint Core Initialization Failed.\n");
			break;
		}
		case CinePaintApp::INIT_UNINIT_ENUM:
		case CinePaintApp::INIT_PROGRESS_ENUM:
		default:
		{
			// shouldn't be here
			printf("CinePaint Error: Unhandled Core Initialization state.\n");
		}
	}
}

