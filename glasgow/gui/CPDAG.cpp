/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CPDAG - Directed acyclic graph for EDL
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Stuart Ford
 *   Colin Law
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * $Id: CPDAG.cpp,v 1.3 2006/12/21 11:18:05 robinrowe Exp $
 */

#include "CPDAG.h"
#include "CPDAGNode.h"
#include "CPDAGData.h"
#include "CPDAGEvaluator.h"
#include "plug-ins/pdb/CinePaintImage.h"
#include "plug-ins/pdb/Layer.h"
#include "utility/CPSocket.h"
#include "plug-ins/pdb/PluginParam.h"
#include <iostream>
#include <string>

const char* CPDAG::mc_Absolute = "A";
const char* CPDAG::mc_Relative = "R";


//-------------------------
// Constructor / Destructor


/**
 * Destructor
 */
CPDAG::~CPDAG()
{
	for(NodesContainer_t::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it)
	{
		delete it->second;
		it->second = 0;
	}

	m_nodes.clear();
}



//---------------
// Graph Addition

/**
 * Create a new unconnected node in this DAG holding the specified int value.
 * Nodes must have distinct names, so if a node already exists with the 
 * given name, the new node will not be created.
 *
 * @param name the name of the new node
 * @param data the int value to be held in the new node
 * @return true if the node was create successfully, false otherwise for example
 *         if the node name name already exists
 */
bool CPDAG::NewNode(const std::string& name, int data)
{
	bool ret = false;

	if(m_nodes.find(name) == m_nodes.end())
	{
		CPDAGNode* node = new CPDAGNode();
		node->get_data().set(data);
		std::pair<NodesContainer_t::iterator, bool> ins_ret = m_nodes.insert(std::pair<std::string, CPDAGNode*>(name, node));
		set_dirty(name, true);

		ret = ins_ret.second;
		if(ret)
		{
			// as the cell is simply data, we can automatically 'evaluate' the cell
			Evaluate(name);
		}
	}

	return(ret);
}

/**
 * Create a new unconnected node in this DAG holding the specified float value.
 * Nodes must have distinct names, so if a node already exists with the 
 * given name, the new node will not be created.
 *
 * @param name the name of the new node
 * @param data the float value to be held in the new node
 * @return true if the node was create successfully, false otherwise for example
 *         if the node name name already exists
 */
bool CPDAG::NewNode(const std::string& name, float data)
{
	bool ret = false;

	if(m_nodes.find(name) == m_nodes.end())
	{
		CPDAGNode* node = new CPDAGNode();
		node->get_data().set(data);
		std::pair<NodesContainer_t::iterator, bool> ins_ret = m_nodes.insert(std::pair<std::string, CPDAGNode*>(name, node));
		set_dirty(name, true);

		ret = ins_ret.second;
		if(ret)
		{
			// as the cell is simply data, we can automatically 'evaluate' the cell
			Evaluate(name);
		}
	}

	return(ret);
}

/**
 * Create a new unconnected node in this DAG holding the specified string value.
 * Nodes must have distinct names, so if a node already exists with the 
 * given name, the new node will not be created.
 * The data string is copied internally by the node holding the actual data.
 *
 * @param name the name of the new node
 * @param data the string value to be held in the new node
 * @return true if the node was create successfully, false otherwise for example
 *         if the node name name already exists
 */
bool CPDAG::NewNode(const std::string& name, const char* data)
{
	bool ret = false;

	if(m_nodes.find(name) == m_nodes.end())
	{
		CPDAGNode* node = new CPDAGNode();

		// the string is copied by the node
		node->get_data().set(data);
		std::pair<NodesContainer_t::iterator, bool> ins_ret = m_nodes.insert(std::pair<std::string, CPDAGNode*>(name, node));
		set_dirty(name, true);

		ret = ins_ret.second;
		if(ret)
		{
			// as the cell is simply data, we can automatically 'evaluate' the cell
			Evaluate(name);
		}
	}

	return(ret);
}

/**
 * Create a new unconnected node in this DAG holding the specified AbstractBuf value.
 * Nodes must have distinct names, so if a node already exists with the 
 * given name, the new node will not be created.
 *
 * @param name the name of the new node
 * @param data the AbstractBuf value to be held in the new node
 * @return true if the node was create successfully, false otherwise for example
 *         if the node name name already exists
 */
bool CPDAG::NewNode(const std::string& name, AbstractBuf* data)
{
	bool ret = false;

	if(m_nodes.find(name) == m_nodes.end())
	{
		CPDAGNode* node = new CPDAGNode();
		node->get_data().set(data);
		std::pair<NodesContainer_t::iterator, bool> ins_ret = m_nodes.insert(std::pair<std::string, CPDAGNode*>(name, node));
		set_dirty(name, true);
		
		ret = ins_ret.second;
		if(ret)
		{
			// as the cell is simply data, we can automatically 'evaluate' the cell
			Evaluate(name);
		}
	}

	return(ret);
}

/**
 * Create a new unconnected node in this DAG holding the specified plugin name
 * Nodes which hold plugin names differ from those that hold plugin data values
 * by having the data type of the node set to DAG_PLUGIN indicating that the
 * node stores a plugin name.
 * Nodes must have distinct names, so if a node already exists with the 
 * given name, the new node will not be created.
 *
 * @param name the name of the new node
 * @param data the int value to be held in the new node
 * @return true if the node was create successfully, false otherwise for example
 *         if the node name name already exists
 */
bool CPDAG::NewPluginNode(const std::string& name, const std::string& plugin)
{
	bool ret = false;

	if(m_nodes.find(name) == m_nodes.end())
	{
		CPDAGNode* node = new CPDAGNode();
		node->get_data().set_plugin(plugin.c_str());
		std::pair<NodesContainer_t::iterator, bool> ins_ret = m_nodes.insert(std::pair<std::string, CPDAGNode*>(name, node));
		set_dirty(name, true);

		ret = ins_ret.second;
	}

	return(ret);

}








//----------------
// Graph Structure

/**
 * Make a directed connection between 2 nodes.
 * In Cinepaint, links in the DAG connect parameters to plugins, both of
 * which are stored in the nodes. Each link between nodes is named, the
 * name represents the parameter name in the plugin call. If making the
 * connection would form a cycle in the DAG, the operation will fail.
 * The node providing the parameter to a plugin node may contain multiple
 * values, for example the results of previous plugin calls. To distinguish
 * between these evaluation values, the required parameter value is also named.
 *
 * Paramater Node (m)  ->  connection name (param)  ->  Plugin Node (n)
 * Evaluations (eval)
 *
 * During construction of the plugin call, the parameter is added to the plugin
 * using the connection name (param) as the name of the parameter to the plugin.
 * The actual parameter value is retrieved from node m, using the evaluation name
 * eval. 
 *
 * @param m name of the node to connect \e from (parameter)
 * @param n name of the node to connect \e to (plugin)
 * @param param	the parameter name in node n the connection represents
 * @param eval the evaluation value name held within node m
 * @param absoluteRow (spreadsheet) indicates if the row is absolute
 * @param absoluteCol (spreadsheet) indicates if the column is absolute
 * @return true on success, else false
 */
bool CPDAG::Attach(const std::string& m, const std::string& n, const std::string& param, const std::string& eval, bool absoluteRow, bool absoluteCol)
{
	bool ret = false;

	NodesContainer_t::iterator nit = m_nodes.find(n);
	if(nit != m_nodes.end())
	{
		NodesContainer_t::iterator mit = m_nodes.find(n);
		mit = m_nodes.find(m);
		if(mit != m_nodes.end())
		{
			mit->second->attach(n, param_embellish(param, absoluteRow, absoluteCol), eval);

			set_dirty(n, true);

			if(contains_cycle())
			{
				mit->second->detach(n, param_embellish(param, absoluteRow, absoluteCol), eval);
				ret = false;
			}
			else
			{
				ret = true;
			}
		}
	}

	return(ret);
}

/**
 * Completely break a directed connection between 2 nodes.
 * Two nodes may be connected multiple times on different parameters, this method
 * completely detaches two nodes on all parameter names.
 * If the connection does not exist no action will be taken and the operation
 * will be considered successful. If a node does not exist the operation
 * will be considered to have failed.
 *
 * @param m name of the node connected \e from
 * @param n name of the node connected \e to
 * @param param	the parameter name in node n the connection represents
 * @param eval the evaluation value name held within node m
 * @param absoluteRow (spreadsheet) indicates if the row is absolute
 * @param absoluteCol (spreadsheet) indicates if the column is absolute
 * @return true on success, else false
 */
bool CPDAG::Detach(const std::string& m, const std::string& n, const std::string& param, const std::string& eval, bool absoluteRow, bool absoluteCol)
{
	bool ret = false;

	NodesContainer_t::iterator nit = m_nodes.find(n);
	NodesContainer_t::iterator mit = m_nodes.find(m);

	if((nit != m_nodes.end()) && (mit != m_nodes.end()))
	{
		set_dirty(n, true);
		mit->second->detach(n, param_embellish(param, absoluteRow, absoluteCol), eval);
		ret = true;
	}

	return(ret);
}

/**
 * Completely break a directed connection between 2 nodes.
 * Two nodes may be connected multiple times on different parameters, this method
 * completely detaches two nodes on all parameter names.
 * If the connection does not exist no action will be taken and the operation
 * will be considered successful. If a node does not exist the operation
 * will be considered to have failed.
 *
 * @param m name of the node connected \e from
 * @param n name of the node connected \e to
 * @return true on success, else false
 */
bool CPDAG::Detach(const std::string& m, const std::string& n)
{
	bool ret = false;

	NodesContainer_t::iterator nit = m_nodes.find(n);
	NodesContainer_t::iterator mit = m_nodes.find(m);

	if((nit != m_nodes.end()) && (mit != m_nodes.end()))
	{
		set_dirty(n, true);
		mit->second->detach(n);
		ret = true;
	}

	return(ret);
}

/**
 * Completely disconnect a node.
 * If the node does not exist the operation will be considered to have failed.
 *
 * @param m name of the node to disconnect
 * @return true on success, else false
 */
bool CPDAG::Detach(const std::string& m)
{
	bool ret = false;

	for(NodesContainer_t::iterator node_it = m_nodes.begin(); node_it != m_nodes.end(); ++node_it)
	{
		if(node_it->first == m)
		{
			// must call set_dirty before detaching nodes
			set_dirty(m, true);

			node_it->second->detach_all();
			ret = true;
		}
		else
		{
			node_it->second->detach(m);
		}
	}

	return(ret);
}

/**
 * Disconnect and delete a node from a graph.
 * If the node does not exist the operation will be considered to have succeeded.
 *
 * @param m name of the node to remove
 * @return true on success, else false
 */
bool CPDAG::Remove(const std::string& m)
{
	// currently this is always true
	// we need to handle cases where removing the node may not be allowed
	bool ret = true;

	NodesContainer_t::iterator iter = m_nodes.find(m);

	if(iter != m_nodes.end())
	{
		Detach(m);
		CPDAGNode* node = iter->second;
		m_nodes.erase(m);

		delete node;
		node = 0;

		ret = true;
	}

	return(ret);
}



/**
 * Returns whether the specified node is contained within this CPDAG
 *
 * @param node_name the node name
 * @return true if the specified node is contained witin this CPDAG,
 *         false otherwise
 */
bool CPDAG::Exists(const std::string& node_name)
{
	return(m_nodes.find(node_name) != m_nodes.end());
}

/**
 * Rename a given node.
 *
 * @param oldName current name of the node to change
 * @param newName new name of the node
 * @return true if the node exists and was renamed, else false
 */
bool CPDAG::Rename(const std::string& oldName, const std::string& newName)
{
	NodesContainer_t::iterator it = m_nodes.find(newName);

	if(it != m_nodes.end())
	{
		return false;
	}

	it = m_nodes.find(oldName);
	if(it == m_nodes.end())
	{
		return false;
	}

	m_nodes[newName] = it->second;

	for(it = m_nodes.begin(); it != m_nodes.end(); ++it)
	{
    	CPDAGNode::neightbour_iterator neighbour = it->second->neighbours_begin();
    	while((neighbour != it->second->neighbours_end()) && (neighbour->first != oldName))
		{
    		neighbour++;
    	}

    	if(neighbour != it->second->neighbours_end())
		{
			// @TODO FIX THIS
    		//neighbour->first = newName;
    	}
	}

	m_nodes.erase(oldName);

	return(true);
}

/**
 * Returns the number of nodes in this CPDAG.
 *
 * @return the number of nodes in this CPDAG.
 */
size_t CPDAG::Size() const
{
	return(m_nodes.size());
}





//------------------------
// Graph Node Modification

/**
 * Set the data in an existing node in this DAG.
 * If a node with the given name does not exist it will first be created
 * using NewNode(const std::string&, int)
 *
 * @param name the name of the node
 * @param data the int value to be held in the node
 * @return true if the data value was set, false otherwise
 * @see NewNode(const std::string &name, int data) 
 */
bool CPDAG::Set(const std::string& name, int data)
{
	bool ret = false;

	NodesContainer_t::iterator it = m_nodes.find(name);

	if(it == m_nodes.end())
	{
		ret = NewNode(name, data);
	}
	else
	{
		it->second->get_data().set(data);

		// as this node may be linked to other, changing this node may effect the result
		// of other, so we need to set this, and all connected node dirty.
		set_dirty(name, true);

		// Once all the connected nodes have been informed of the change, we can evaulate this
		// node as it is simply a data node
		// @TODO this 'automatic evaluation' needs expanded to handle different automatic evaluation settings
		//       ie, no evaluation, partial evaluation (like this) and full evaluation
		Evaluate(name);

		ret = true;
	}

	return(ret);
}

/**
 * Set the data in an existing node in this DAG.
 * If a node with the given name does not exist it will first be created
 * using NewNode(const std::string&, float)
 *
 * @param name the name of the node
 * @param data the float value to be held in the node
 * @return true if the data value was set, false otherwise
 * @see NewNode(const std::string &name, float data) 
 */
bool CPDAG::Set(const std::string& name, float data)
{
	bool ret = false;

	NodesContainer_t::iterator it = m_nodes.find(name);

	if(it == m_nodes.end())
	{
		ret = NewNode(name, data);
	}
	else
	{
		it->second->get_data().set(data);

		// @see Set(const std::string&, int)
		set_dirty(name, true);
		Evaluate(name);

		ret = true;
	}

	return(ret);
}

/**
 * Set the data in an existing node in this DAG.
 * If a node with the given name does not exist it will first be created
 * using NewNode(const std::string&, const char*)
 * The string value is copied internally by the node.
 *
 * @param name the name of the node
 * @param data the string value to be held in the node
 * @return true if the data value was set, false otherwise
 * @see NewNode(const std::string &name, const char* data) 
 */
bool CPDAG::Set(const std::string& name, const char* data)
{
	bool ret = false;

	NodesContainer_t::iterator it = m_nodes.find(name);

	if(it == m_nodes.end())
	{
		ret = NewNode(name, data);
	}
	else
	{
		it->second->get_data().set(data);

		// @see Set(const std::string&, int)
		set_dirty(name, true);
		Evaluate(name);

		ret = true;
	}

	return(ret);
}

/**
 * Set the data in an existing node in this DAG.
 * If a node with the given name does not exist it will first be created
 * using NewNode(const std::string&, AbstractBuf*)
 *
 * @param name the name of the node
 * @param data the AbstractBuf to be held in the node
 * @return true if the data value was set, false otherwise
 * @see NewNode(const std::string &name, AbstractBuf *data) 
 */
bool CPDAG::Set(const std::string& name, AbstractBuf *data)
{
	bool ret = false;

	NodesContainer_t::iterator it = m_nodes.find(name);

	if(it == m_nodes.end())
	{
		ret = NewNode(name, data);
	}
	else
	{
		it->second->get_data().set(data);

		// @see Set(const std::string&, int)
		set_dirty(name, true);
		Evaluate(name);

		ret = true;
	}

	return(ret);
}

/**
 * Set the plugin name in an existing node in this DAG.
 * If a node with the given name does boolnot exist it will first be created
 * using NewPluginNode(const std::string&, const std::string&)
 *
 * @param name the name of the node
 * @param plugin the plugin name to store within the noew
 * @return true if the data value was set, false otherwise
 * @see NewPluginNode(const std::string&, const std::string&) 
 */
bool CPDAG::SetPlugin(const std::string& name, const std::string& plugin)
{
	bool ret = false;

	NodesContainer_t::iterator it = m_nodes.find(name);

	if(it == m_nodes.end())
	{
		ret = NewPluginNode(name, plugin);
	}
	else
	{
		it->second->get_data().set_plugin(plugin.c_str());
		set_dirty(name, true);

		ret = true;
	}

	return(ret);
}





//-----------------
// Node/Data Access

/**
 * Returns the named node from within this CPDAG
 * If the named node is not contained within this CPDAG, 0 is returned.
 *
 * @param name the name of the node
 * @return the specified node or 0, if the node does not exist
 */
const CPDAGNode* CPDAG::GetNode(const std::string& name) const
{
	NodesContainer_t::const_iterator it = m_nodes.find(name);

	if(it == m_nodes.end())
	{
		return(0);
	}

	return(it->second);
}


/**
 * Returns the data element of the specified node.
 * If the named node is not contained within this CPDAG, 0 is returned.
 *
 * @param name the name of the node
 * @return the data in the node or 0, if the node does not exist
 */
const CPDAGData* CPDAG::GetData(const std::string& name) const
{
	const CPDAGData* ret = 0;

	NodesContainer_t::const_iterator cit = m_nodes.find(name);

	if(cit != m_nodes.end())
	{
		ret = &(cit->second->get_data());
	}

	return(ret);
}

/**
 * Returns the default evaluated data from within an existing node in this CPDAG.
 * If the node has not been evaluated #Evaluate will be called on it and
 * the result returned.
 *
 * @param name the name of the node
 * @return the evaluated data in the node, or 0 if the node does not exist
 *         or does not contain any evaluated data
 */
const PluginParam* CPDAG::GetEvaluated(const std::string& name)
{
	const PluginParam* ret = 0;
	NodesContainer_t::const_iterator cit = m_nodes.find(name);

	if(cit != m_nodes.end())
	{
		if(cit->second->IsDirty())
		{
			Evaluate(name); 
		}

		ret = cit->second->GetEvaluation();
	}

	return(ret);
}

/**
 * Returns the named evaluated data from within an existing node in this CPDAG.
 * If the node has not been evaluated #Evaluate will be called on it and
 * the result returned.
 *
 * @param name the name of the node
 * @param eval the name of the evaluation value
 * @return the evaluated data in the node, or 0 if the node does not exist
 *         or does not contain any evaluated data
 */
const PluginParam* CPDAG::GetEvaluated(const std::string& name, const std::string eval)
{
	const PluginParam* ret = 0;
	NodesContainer_t::const_iterator cit = m_nodes.find(name);

	if(cit != m_nodes.end())
	{
		if(cit->second->IsDirty())
		{
			Evaluate(name); 
		}

		ret = cit->second->GetEvaluation(eval);
	}

	return(ret);
}




/**
 * Populates the specified set with the names of all the nodes in this CPDAG.
 * The set is not cleared prior to adding any node names
 *
 * @param nodes the set for holding the names of the nodes
 * @return the set populated with the naems of nodes contained within this CPDAG
 */
std::set<std::string>& CPDAG::Nodes(std::set<std::string>& nodes) const
{
	NodesContainer_t::const_iterator cit = m_nodes.begin();
	while(cit != m_nodes.end())
	{
		nodes.insert(cit->first);
		++cit;
	}

	return(nodes);
}

/**
 * Populates the specified map with a mapping of all the nodes that connect to the specified node
 * The map of node names represents a mapping of the inputs node, or parameters into a plugin node,
 * to a pair representing the parameter name and evaluation name.
 *
 * @param name the name of the node
 * @param inputs a mapping of input nodes to the parameter and evaluation names
 * @return true if the node exists, else false
 */
bool CPDAG::GetInputNodes(const std::string& name, std::map<std::string, std::pair<std::string, std::string> >& inputs) const
{
	bool ret = false;

	NodesContainer_t::const_iterator named_citer = m_nodes.find(name);

	if(named_citer != m_nodes.end())
	{
		ret = true;

		for(NodesContainer_t::const_iterator nodes_citer = m_nodes.begin(); nodes_citer != m_nodes.end(); ++nodes_citer)
		{
			CPDAGNode* node = nodes_citer->second;

			std::pair<CPDAGNode::neightbour_iterator, CPDAGNode::neightbour_iterator> neighbour_range = node->neighbours_range(name);
			while(neighbour_range.first != neighbour_range.second)
			{
				inputs.insert(std::pair<std::string, std::pair<std::string, std::string> >(nodes_citer->first, std::pair<std::string, std::string>(param_strip(neighbour_range.first->second.first), neighbour_range.first->second.second)));
				++neighbour_range.first;
			}
		}
	}

	return(ret);
}

/**
 * Populates the specified map with a mapping of all the nodes the specified node connects to.
 * The map of node names represents a mapping of the inputs node, or parameters into a plugin node,
 * to a pair representing the parameter name and evaluation name.
 *
 * @param name the name of the node
 * @param outputs a mapping of output nodes to the parameter and evaluation names
 * @return true if the node exists, else false
 */
bool CPDAG::GetOutputNodes(const std::string& name, std::map<std::string, std::pair<std::string, std::string> >& outputs) const
{
	bool ret = false;

	NodesContainer_t::const_iterator named_citer = m_nodes.find(name);

	if(named_citer != m_nodes.end())
	{
		ret = true;

		CPDAGNode* node = named_citer->second;
		for(CPDAGNode::neightbour_iterator neighbour_iter = node->neighbours_begin(); neighbour_iter != node->neighbours_end(); ++neighbour_iter)
		{
			outputs.insert(std::pair<std::string, std::pair<std::string, std::string> >(neighbour_iter->first, std::pair<std::string, std::string>(param_strip(neighbour_iter->second.first), neighbour_iter->second.second)));
		}
	}

	return(ret);
}




//-------------------
// Execution  Methods

/**
 * Evaluate the data in the node with the given name.
 * A node can only be evaluated if all the nodes directly prior to it in
 * the graph have been evaluated already.
 *
 * @param node_name the name of the node to evaluate
 * @return true if the node was evaluated successfully, else false
 */
bool CPDAG::Evaluate(const std::string& node_name)
{
	bool ret = false;

	CPDAGEvaluator evaluator(*this);

	if(Exists(node_name))
	{
		ret = evaluator.EvaluateNode(node_name, false,world);
	}

	return(ret);
}






//----------------
// Iterator access

/**
 * Returns an iterator pointing to the start of the DAG.
 * The start of a CPDAG is not unique and is defined as the first 
 * node in a topologically sorted list of the DAG's nodes.
 *
 * @return an iterator pointing to the start of the DAG
 */
CPDAG::const_iterator CPDAG::begin() const
{
	return CPDAGIterator(*this);
}

/**
 * Returns an iterator pointing past the end of the DAG.
 *
 * @return an iterator pointing past the end of the DAG
 */
CPDAG::const_iterator& CPDAG::end() const
{
	static CPDAGIterator end_it;
	return(end_it);
}





//--------------
// Serialization

/**
 * Serialize and write the graph to a given socket.
 * @param socket the socket to write the graph to
 */
void CPDAG::Serialize(CPSocket& socket) const
{
	// std::map<std::string, CPDAGNode*>::iterator it;
	// socket << m_nodes.size();
	// for (it = m_nodes.begin(); it != m_nodes.end(); ++it)
	// {
		// socket << it->first;
		// it->second->Serialize(socket);
	// }
}

/**
 * Read and deserialize the graph from a given socket.
 * @param socket the socket to read the graph from
 */
void CPDAG::Deserialize(CPSocket& socket)
{
	// int i, numNodes;
	// std::string name;

	// socket >> numNodes;

	// for (i = 0; i < numNodes; ++i)
	// {
		// socket >> name;
		// m_nodes[name] = new CPDAGNode(socket);
	// }
}





CPDAG::nodes_iterator CPDAG::find_node(const std::string& name)
{
	return(m_nodes.find(name));
}

CPDAG::nodes_iterator CPDAG::nodes_begin()
{
	return(m_nodes.begin());
}

CPDAG::nodes_iterator CPDAG::nodes_end()
{
	return(m_nodes.end());
}


/**
 * Tests the graph to see if a cycle is present
 *
 * @return true if a cycle exists, else false
 */
bool CPDAG::contains_cycle()
{
	bool ret = false;

	size_t countedNodes = 0;
	for(CPDAG::const_iterator it = begin(); it != end(); ++it)
	{
		countedNodes++;
	}

	if(countedNodes == m_nodes.size())
	{
		ret = true;
	}

	return(ret);
}

/**
 * Sets the dirty status of a node.
 * A dirty node requires evaluation. Setting a node to be dirty will
 * recursively set all the nodes below it in the graph to be dirty also.
 * Making a node clean only affects that node, and no recursion takes place.
 *
 * @param node the name of the node to change
 * @param d the new dirty status of the node
 */
void CPDAG::set_dirty(const std::string& node, bool d)
{
	NodesContainer_t::iterator iter = m_nodes.find(node);

	if(iter != m_nodes.end())
	{
		CPDAGNode* n = iter->second;
		n->set_dirty(d);

		if(d)
		{
			n->clear_evaluations();
			for(CPDAGNode::neightbour_iterator it = n->neighbours_begin(); it != n->neighbours_end() ; ++it)
			{
				set_dirty(it->first, true);
			}

			// if the node is a param data, we can re-evaluate it automatically
			if(n->GetData().GetType() == CPDAGData::DAG_PLUGINPARAM)
			{
				Evaluate(node);
			}
		}
	}
}

/**
 * Embellish a param string with information on the type of reference it uses in a spreadsheet.
 * 2 characters are added to the front of the param, the first indicating if the row in the
 * reference is absolute, the second indicating likewise for the column.
 *
 * @return the embellished param string
 */
std::string CPDAG::param_embellish(const std::string& param, bool absRow, bool absCol)
{
	std::string str = param;
	return(str + (absRow ? mc_Absolute : mc_Relative) + (absCol ? mc_Absolute : mc_Relative));
}

/**
 * Strip a param string of information on the type of reference it uses in a spreadsheet.
 * The first 2 characters of the string are stripped off. This is the complement of the
 * #paramEmbellish function.
 *
 * @return the stripped param string
 */
std::string CPDAG::param_strip(const std::string& param) const
{
	std::string str = param;
	return(str.substr(0, str.size() -2));
}

/**
 * Create a new unconnected node in this DAG.
 * Nodes must have distinct names, so if a node already exists with the
 * given name, the new node will not be created. The constructed Node 
 * initially contains no data.
 *
 * @param name the name of the new node
 * @return the constructed node, or 0 if no new node could be created
 */
CPDAGNode* CPDAG::new_node(const std::string& name)
{
	CPDAGNode* node = 0;

	if(m_nodes.find(name) == m_nodes.end())
	{
		node = new CPDAGNode();
		std::pair<NodesContainer_t::iterator, bool> ret = m_nodes.insert(std::pair<std::string, CPDAGNode*>(name, node));

		if(!ret.second)
		{
			// failed insertion, cleanup
			delete node;
			node = 0;
		}
	}

	return(node);
}

/**
 * Pre-process a node prior to setting its value to determine if the data type of the node has been changed.
 * If the data type of the node has changed, for example a node which was a plugin node
 * is changed to a plugin parameter node, noce connections involving the node may no
 * longer be valid
 *
 * @TODO need to review this
 * @param node_name the node name to be changed
 * @param data_type the new data type
 * @param node_type the new node type
 */
void CPDAG::process_cell_change(const std::string& node_name, CPPluginArgType data_type, CPDAGData::DataType node_type)
{
	// if the cell already exists, and was previously a plugin cell,
	// we need to detatch all the connected nodes
	if(Exists(node_name))
	{
		const CPDAGNode* node = GetNode(node_name);
		if(node)
		{
			if(node->GetData().GetType() == CPDAGData::DAG_PLUGIN)
			{
				if(node_type == CPDAGData::DAG_PLUGIN)
				{
					// changin the plugin definition
					printf("Detaching modified plugin cell\n");
					Detach(node_name);
				}
				else
				{
					// changing a plugin cell into a param cell
					printf("Detaching plugin cell before changin cell type\n");
					Detach(node_name);
				}
			}
			else
			{
				if(node_type == CPDAGData::DAG_PLUGIN)
				{
					// changing a param cell into a param plugin cell
					// detach any cells this pram used to be an input into
					Detach(node_name);
				}
				else
				{
					const PluginParam* param = node->GetData().GetPluginParam();
					if(param && param->GetType() != data_type)
					{
						Detach(node_name);
						printf("Cell type changed - this may effect plugins\n");
					}
				}
			}
		}
	}
}
