/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      MenuFunctorHelper - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: MenuFunctorHelper.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CINEPAINT_MENU_FUNCTOR_HANDLER_H_
#define _CINEPAINT_MENU_FUNCTOR_HANDLER_H_

#include "dll_api.h"
#include <FL/Fl_Menu_.H>

#include <app/CinePaintFunctor.h>

#include <algorithm>
#include <map>
#include <string>

class Fl_Widget;

/**
 * Helper Functions for dynamic menu functors
 * This class acts as a convenient way of constructing menu entries and automatically creating and
 * managing a new CinePaintFunctor object to respond to the menu click.
 * The static method static_menu_item_callback is registered as the callback method for all added
 * menu items, passing the appropriate Functor as the callback void* data. The functor is then
 * invoked by the static method. This provides a convenient method of adding a menu item to call any
 * member function withou the need for a menu class knowing about any other objects.
 * The manage methods simply store pointers to created Functors, all functors automatically created are
 * automatically managed, any user created Functors may also be added. By Calling ManageFunctor, ownership
* of the CinePaintFunctor resource passes to this class, and the resourse will be destroyed when this class
* is destroyed.
*
*/
class CINEPAINT_GUI_API MenuFunctorHelper
{
	public:
		/**
		 * Constructs a new MenuFunctorHelper to manage created Functor Objects
		 *
		 */
		MenuFunctorHelper();

		/**
		 * Destructor - destroys all managed CinePaintFunctors
		 * Note that destroying Functors does not destroy the object they reference.
		 */
		~MenuFunctorHelper();

		/**
		 * Adds a new menu items to the specified menu with the specified menu path.
		 *
		 * @param menu the menu to add the item into
		 * @param shortcut keyboard shortcut to the item
		 * @param menu_path the path in the menu for the new item, eg "File/Open"
		 * @param obj the Object on which the callback should call a member function
		 * @param member_fun the member function to call, takes and returns void
		 * @param flags menu item flags (see FLTK's Fl_Menu_ for details)
		 */
		template <class T>
		void AddMenuItem0(Fl_Menu_& menu, int shortcut, const char* menu_path, T* obj, void (T::*member_fun)(), int flags = 0);

		/**
		 * Adds a new menu items to the specified menu with the specified menu path.
		 * This variant adds an additional data item to be passed to the specified
		 * function pointer. This item is static.
		 *
		 * @param menu the menu to add the item into
		 * @param shortcut keyboard shortcut to the item
		 * @param menu_path the path in the menu for the new item, eg "File/Open"
		 * @param obj the Object on which the callback should call a member function
		 * @param data additional data to be passed to the function to be called
		 * @param member_fun the member function to call, takes and returns void
		 * @param flags menu item flags (see FLTK's Fl_Menu_ for details)
		 */
		template <class T, class D>
		void AddMenuItem1(Fl_Menu_& menu, int shortcut, const char* menu_path, T* obj, D data, void (T::*member_fun)(D), int flags = 0);

		/**
		 * Manages the specified Functor Object
		 * Ownership of the Functor passes to this class, and the object will be destroyed when this class
		 * is destroyed
		 *
		 * @param path the menu path to the menu item the functor is the callback for
		 * @param functor the Functor to manage
		 */
		bool ManageFunctor(const char* path, AbstractFunctor* functor);

		/**
		 * Unmanages the Functor represented with the specified menu path
		 * If release is set true, the functor is automatically deleted, and this method returns 0.
		 * If no Functor is managed for the specified path, 0 is returned, otherwise the managed
		 * functor is returned. Ownership passes to the caller who must manage the resources of the objeect.
		 *
		 * @param path the menu path representing the functor
		 * @param release set true to automatically delete the functor
		 * @return the functor if found, of 0 if found and release is set true, otherwise 0 if not found
		 */
		AbstractFunctor* UnmanageFunctor(const char* path, bool release);

		/**
		 * Returns the Functor representing the specified menu path
		 * If no Functor is managed with the specified path, 0 is returned
		 * The functor resources remain managed by this class.
		 *
		 * @param path the menu path representing the functor
		 * @return the managed Functor, or 0 if the functor is not found
		 */
		AbstractFunctor* getFunctor(const char* path) const;

		/**
		 * Static callback used by all added Manu Items.
		 * This method is set as the callback for all added menu items, passing A Functor
		 * as the void* data parameter. The Functor is then invoked.
		 * This method is automatically set as the menu item callback for all items added with
		 * AddMenuItem, however the method is also available for client use, where a user
		 * functor is passed as the addditional data parameter.
		 * The additional data is assumed to be an AbstractFunctor.
		 *
		 * @param w the fltk menu
		 * @param d the functor to call
		 */
		static void static_menu_item_callback(Fl_Widget* w, void* d);

	protected:

	private:
		typedef std::map<std::string, AbstractFunctor*> FunctorContainer_t;

		/** the managed functors */
		FunctorContainer_t m_managed_functors;

}; /* class MenuFunctorHelper */


template <class T>
void MenuFunctorHelper::AddMenuItem0(Fl_Menu_& menu, int shortcut, const char* menu_path, T* obj, void (T::*member_fun)(), int flags)
{
	// create the functor
	CinePaintFunctor0<T>* _functor = new CinePaintFunctor0<T>(obj, member_fun);

	// add the menu item
	menu.add(menu_path, shortcut, static_menu_item_callback, _functor, flags);

	// mamanage the allocated Functor
	ManageFunctor(menu_path, _functor);
}

template <class T, class D>
void MenuFunctorHelper::AddMenuItem1(Fl_Menu_& menu, int shortcut, const char* menu_path, T* obj, D data, void (T::*member_fun)(D), int flags)
{
	// create the functor
	CinePaintFunctor1<T,D>* _functor = new CinePaintFunctor1<T,D>(obj, data, member_fun);

	// add the menu item
	menu.add(menu_path, shortcut, static_menu_item_callback, _functor, flags);

	// mamanage the allocated Functor
	ManageFunctor(menu_path, _functor);
}

#endif /* _CINEPAINT_MENU_FUNCTOR_HANDLER_H_ */
