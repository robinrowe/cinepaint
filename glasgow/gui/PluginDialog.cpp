/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      PluginDialog - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: PluginDialog.cpp,v 1.2 2006/12/18 08:21:17 robinrowe Exp $
 */

#include <gui/PluginDialog.h>
#include <gui/CinePaintAppUI.h>
#include <string>

#include <FL/Fl_Window.H>

PluginDialog::PluginDialog(char* ID)
:m_response(CANCEL_ENUM)
{ 
	strncpy(m_ID, ID, sizeof(m_ID));
	pthread_t self = pthread_self();

	if ( pthread_equal(CinePaintAppUI::GetInstance().GetUIThreadID(), self) )
		m_main_thread = true;
	else
		m_main_thread = false;
}

PluginDialog::~PluginDialog()
{

}
void PluginDialog::CreatePluginDialog(CPPluginUIDialog *dlg)
{
	if (m_main_thread)
        create_plugin_dialog(dlg);
	else
        CinePaintAppUI::GetInstance().GetUIThreadEventList().PostEvent(new NewPluginDialogEvent(this, dlg));

}

DialogResponse PluginDialog::DoModal()
{
	if(m_main_thread)
		return do_modal();
	else
        CinePaintAppUI::GetInstance().GetUIThreadEventList().PostEvent(new PluginDialogDoModalEvent(this));
	return m_response;
}


//=======================================
// Private methods to actually make things happen.

void PluginDialog::create_plugin_dialog(CPPluginUIDialog *dlg)
{
	m_dlg = dlg;
}


DialogResponse PluginDialog::do_modal()
{
	Fl_Window *dlg = build_dialog();
	dlg->show();
	while (dlg->shown())
		Fl::wait();
	return m_response;	
}


Fl_Window * PluginDialog::build_dialog()
{
	// Here we create the relevant resources and do fl_wait.
	Fl_Window *wnd = new Fl_Window(  m_dlg->GetWidth(),  m_dlg->GetHeight(), m_dlg->GetTitle() );
	CPPluginUIDialog::widgetIter iter = m_dlg->begin();
//	while(iter != m_dlg->end())
//	{
		// Create Widget
//		(*iter)->Create();
//		iter++;
//	}
	wnd->end();
	return wnd;
}

void PluginDialog::hide()
{
}


//=============================
// Progress Dialog Listener Event Implementations

PluginDialog::DialogListener::DialogListener()
{
}
PluginDialog::DialogListener::~DialogListener()
{
}

// rsr:
#undef CreateDialog

void PluginDialog::DialogListener::CreateDialog(char* ID, CPPluginUIDialog *dlg )
{
	PluginDialog *_dlg = new PluginDialog(ID);
	m_dlgList.push_back(_dlg);
	
	_dlg->CreatePluginDialog(dlg);
}

DialogResponse PluginDialog::DialogListener::DoModal( char* ID )
{
	PluginDialog *_dlg = find_dlg(ID);
	return _dlg->DoModal();
}

PluginDialog *PluginDialog::DialogListener::find_dlg(char* ID)
{
	std::list<PluginDialog *>::iterator iter = m_dlgList.begin();
	bool found = false;
	while (!found && iter != m_dlgList.end() )
	{	
		found = strcmp((*iter)->GetID(), ID) == 0;
		if (!found)
			++iter;
	}
	if (found)
		return (*iter);
	else
		return 0;
}


//=============================
// Dialog Event Implementations

// -----------------------------------------
// New Plugin Dialog Window Event Implementation.
PluginDialog::NewPluginDialogEvent::NewPluginDialogEvent( PluginDialog *dlg, CPPluginUIDialog * piDlg )
:CinePaintEvent(true), m_dlg(dlg), m_piDlg(piDlg)
{
}

void PluginDialog::NewPluginDialogEvent::Process()
{
	m_dlg->CreatePluginDialog(m_piDlg );
}

// -----------------------------------------
// Update Progress Window Event Implementation.
PluginDialog::PluginDialogDoModalEvent::PluginDialogDoModalEvent( PluginDialog *dlg)
:CinePaintEvent(true), m_dlg(dlg)
{
}

void PluginDialog::PluginDialogDoModalEvent::Process()
{
	m_dlg->DoModal();
}
