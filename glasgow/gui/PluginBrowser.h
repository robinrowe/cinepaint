/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      BrushSelectionDialog - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: PluginBrowser.h,v 1.2 2006/12/21 11:18:07 robinrowe Exp $
 */

#ifndef _PLUGIN_BROWSER_H_
#define _PLUGIN_BROWSER_H_

#include "dll_api.h"
#include "plug-ins/pdb/CinePaintPlugin.h"
#include "plug-ins/pdb/ListenerList.h"
#include <FL/Fl_Group.H>
#include <functional>
#include <list>
#include <string>

class Fl_Box;
class Fl_Choice;
class Fl_Hold_Browser;
class Fl_Input;
class Fl_Text_Buffer;
class Fl_Text_Display;
class CinePaintPluginInfo;

class CINEPAINT_GUI_API PluginBrowser : public Fl_Group
{	World* world;
	public:
		/**
		 * Constructs a new PluginBrowser
		 *
		 * @param w the width of the dialog
		 * @param h the geight of the dialog
		 * @param title dialog title
		 */
		PluginBrowser(int x, int y, int w, int h, const char* title,World* world) ;

		/**
		 * Destructor
		 */
		virtual ~PluginBrowser();



		//----------------
		// Change Listener

		/**
		 * PluginSelectionChangeListener interface.
		 * Classes interested in receiving Plugin selection updates should implement this nested class
		 */
		class PluginSelectionChangeListener
		{
			public:
				/**
				 * Invoked when the currently selected plugin is changed
				 *
				 * @param plugin_name the name of the newly selected tool
				 */
				virtual void SelectionChanged(const char* plugin_name) = 0;
		};

		/**
		 * Adds the specified PluginSelectionChangeListener to receive notification upon plugin selection changes
		 * if the specified PluginSelectionChangeListener has already been added it is not added again.
		 * The resources of the specified PluginSelectionChangeListener are not managed by this class
		 *
		 * @param pscl the PluginSelectionChangeListener to be added
		 */
		void AddSelectionChangeListener(PluginSelectionChangeListener* pscl);

		/**
		 * Removes the specified PluginSelectionChangeListener
		 * The removed PluginSelectionChangeListener will no longer receive plugin selection change
		 * notifications from this class
		 *
		 * @param pscl the PluginSelectionChangeListener to be removed
		 */
		void RemoveSelectionChangeListener(PluginSelectionChangeListener* pscl);


	protected:
	private:

		/** Available Sort Types */
		enum SortType
		{
		    NAME_ASCN_ENUM,
		    NAME_DESC_ENUM,
		    TYPE_ASCN_ENUM,
		    TYPE_DESC_ENUM
		};

		/**
		 * Structure to store information retrieved from the plugin database.
		 * As the load/save plugins return different structure to the standard
		 * plugin, we need something common to hold common data.
		 *
		 */
		struct PluginRec
		{
			enum PluginType { STD_PLUGIN_ENUM, LOAD_HANDLER_ENUM, SAVE_HANDLER_ENUM };

			std::string m_plugin_name;  // the real plugin name
			std::string m_name;         // a short name derived from tha menu path of the plugin
			std::string m_menu_path;    // the manu path for the plugin
			PluginType m_type;          // type of plugin
		};

		typedef std::list<PluginRec*> PluginList;

		/** container for the plugin data records held by this dialog */
		PluginList m_plugin_data;



		/**
		 * Refreshes the list of Plugin Records from the Plugin database
		 * After a refresh, populate_browser should be called to update the current display
		 *
		 */
		void refresh_plugin_data(World* world);

		/**
		 * Popultes the plugin bowser list from the system plugin database.
		 * This method retrieves an iterator from the plugin database and and adds
		 * each plugin into the browser list, taking into account the current sort
		 * and search data
		 *
		 */
		void populate_browser();

		/**
		 * Updates the plugin count text display to the specified count
		 *
		 * @param count indicates the number of loaded plugins
		 */
		void update_plugin_count(int count);

		/**
		 * Sorts the the specified plugin list according to the SortType
		 *
		 * @param list the list to sort
		 * @param sort_type type of ordering required
		 * @return the plugin list sorted
		 */
		PluginList& sort_plugin_list(PluginList& plugin_list, SortType sort_type);

		/**
		 * Updates the plugin info text buffer with details for the currently selected plugin
		 * Currently displayed are the unique plugin name. plugin menu path, author, year, and copryright.
		 *
		 * @param rec the PluginRec struct detailing the plugin to display
		 * @param buffer the buffer in which to append the heading
		 * @param style the buffer containg the style data
		 */
		void update_plugin_info(PluginRec& rec, Fl_Text_Buffer& buffer, Fl_Text_Buffer& style,World* world);

		/**
		 * Convenience method to add a section heading to the plugins display text buffer
		 *
		 * @param buffer the buffer in which to append the heading
		 * @param style the buffer containg the style data
		 * @param heading the heading to append
		 * @param start the current end point of the buffer
		 * @return the new end point of the buffer
		 */
		int add_plugin_desc_section(Fl_Text_Buffer& buffer, Fl_Text_Buffer& style, const char* heading, int start);

		/**
		 * Convenience method to add a data section to the plugins display text buffer
		 *
		 * @param buffer the buffer in which to append the heading
		 * @param style the buffer containg the style data
		 * @param sub_heading a possible sub-heading to append, may also be 0
		 * @param text the plugin detail to append
		 * @param start the current end point of the buffer
		 * @return the new end point of the buffer
		 */
		int add_plugin_desc_detail(Fl_Text_Buffer& buffer, Fl_Text_Buffer& style, const char* sub_heading, const char* text, int start);

		/**
		 * Convenience method to add a plugin parameter definition to the text buffer
		 *
		 * @param buffer the buffer in which to append the heading
		 * @param style the buffer containg the style data
		 * @param param the plugin parameter definition to add
		 * @param start the current end point of the buffer
		 * @return the new end point of the buffer
		 */
		int add_plugin_param(Fl_Text_Buffer& buffer, Fl_Text_Buffer& style, const CinePaintPlugin::CPPluginParamDef& param, int start);

		/**
		 * Sets the style buffer with the specified style filling between start and end
		 *
		 * @param buf the style buffer to update
		 * @param style the style character from the style table (implementation defined)
		 * @param start the start point withi the buffer
		 * @param end the end point within the buffer
		 */
		void set_style(Fl_Text_Buffer& buf, char style, int start, int end);




		/**
		 * UI method to build the sort order drop down list
		 *
		 * @param menu the drop down list to populate
		 */
		void build_sort_order_menu(Fl_Choice& menu);

		/**
		 * Clears the current plugin record list
		 *
		 */
		void clear_plugin_list();

		//-----------------
		// Widget Callbacks

		static void static_flat_browser_cb(Fl_Widget* w, void* d) { static_cast<PluginBrowser*>(d)->flat_browser_cb(); };
		void flat_browser_cb();

		static void static_sort_order_cb(Fl_Widget* w, void* d) { static_cast<PluginBrowser*>(d)->sort_order_cb(); };
		void sort_order_cb();

		static void static_search_cb(Fl_Widget* w, void* d) { static_cast<PluginBrowser*>(d)->search_cb(); };
		void search_cb();



		//------------
		// Member data

		/** Current sort option */
		SortType m_sort_type;

		/** current search string */
		char* m_search_string;

		/** display currently available plugin count */
		char* m_plugin_count_text;



		//-----------------
		// Change listener

		typedef ListenerList<PluginSelectionChangeListener> ListenerList_t;

		/** listeners interested in being notified upon plugin selection changes - update may happen in an arbitary thread */
		ListenerList_t m_listener_list;


		//--------
		// Widgets
		Fl_Box* m_plugin_count_label;
		Fl_Choice* m_sort_choice;
		Fl_Hold_Browser* m_flat_browser;
		Fl_Input* m_search_input;
		Fl_Text_Display* m_text_display;
		Fl_Text_Buffer* m_text_buffer;
		Fl_Text_Buffer* m_style_buffer;



		/**
		 * Nested function object used to sort the plugin list depending on the selected value type
		 */
		class PluginRecComparator : public std::binary_function<PluginRec*, PluginRec*, bool>
		{
			public:
				enum SortCategory
				{
					NAME_ENUM,
					PATH_ENUM,
					TYPE_ENUM
				};

				PluginRecComparator(SortCategory sort_category);
				result_type operator() (first_argument_type p, second_argument_type q) const;

			private:
				SortCategory m_sort_category;
		};

} ; /* class PluginBrowser */

#endif /* _PLUGIN_BROWSER_H_ */
