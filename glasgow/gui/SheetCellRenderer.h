/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      SheetViewWindow - window class fot holding sheet view
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: SheetCellRenderer.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _SHEET_CELL_RENDERER_
#define _SHEET_CELL_RENDERER_

#include "dll_api.h"

class AbstractBuf;
class CinePaintImage;
class CPDAGData;
class CPDAGNode;
class CPSpreadsheet;
class Drawable;
class PluginParam;
class SheetView;

/**
 * SheetCellRenderer handles the rendering of cell data for a given SheetView.
 *  
 * @todo add mutators for default fonts, colors etc.
 */
class CINEPAINT_GUI_API SheetCellRenderer
{
	public:
		/**
		 * Constructs a new SheetCellRenderer to render cells for the specified SheetView
		 *
		 * @param view the SheetView this SheetCellRenderer renders cells for
		 * @param sheet the CPSpreadsheet interface into the CPDAG backing ths spreadsheet
		 */
		SheetCellRenderer(SheetView& view, CPSpreadsheet& sheet);

		/**
		 * Destructor
		 */
		virtual ~SheetCellRenderer();




		/**
		 * Renders the default cell backgroiund and border for the given cell
		 *
		 * @param row row of the cell to render
		 * @param col colummn of the cell to render
		 * @param x top left x-coordinate of the cell to render on parent SheetView component
		 * @param y top left y-coordinate of the cell to render on parent SheetView component
		 * @param w width of cell to render
		 * @param h height of cell to render
		 */
		void RenderCell(int row, int col, int x, int y, int w, int h);

		/**
		 * Renders the cell background/border based upon the cell type of the given cell
		 *
		 * @param row row of the cell to render
		 * @param col colummn of the cell to render
		 * @param x top left x-coordinate of the cell to render on parent SheetView component
		 * @param y top left y-coordinate of the cell to render on parent SheetView component
		 * @param w width of cell to render
		 * @param h height of cell to render
		 * @param node the CPDAGNode represented by the cell
		 */
		void RenderCellBackground(int row, int col, int x, int y, int w, int h, const CPDAGNode& node);

		/**
		 * Renders the cell data held by the CPDAGNode.
		 * This method handles the rendering for the different data type that a node may handle
		 *
		 * @param row row of the cell to render
		 * @param col colummn of the cell to render
		 * @param x top left x-coordinate of the cell to render on parent SheetView component
		 * @param y top left y-coordinate of the cell to render on parent SheetView component
		 * @param w width of cell to render
		 * @param h height of cell to render
		 * @param node the CPDAGNode represented by the cell
		 */
		void RenderCellData(int row, int col, int x, int y, int w, int h, const CPDAGNode& node);

		/**
		 * Renders a selection rectangle over for the given cell highlighting the selected cell.
		 * If the spcified cell is not selected, no action is taken.
		 *
		 * @param row row of the cell to render
		 * @param col colummn of the cell to render
		 * @param x top left x-coordinate of the cell to render on parent SheetView component
		 * @param y top left y-coordinate of the cell to render on parent SheetView component
		 * @param w width of cell to render
		 * @param h height of cell to render
		 */
		void RenderCellSelection(int row, int col, int x, int y, int w, int h);

	protected:
	private:

		/**
		 * Renders a cell that contains the name of a plugin to be called
		 * The CPSpreadsheet instance is used to rebuild the complete plugin string
		 * from the cell data.
		 *
		 * @param row row of the cell to render
		 * @param col colummn of the cell to render
		 * @param x top left x-coordinate of the cell to render on parent SheetView component
		 * @param y top left y-coordinate of the cell to render on parent SheetView component
		 * @param w width of cell to render
		 * @param h height of cell to render
		 * @param data the cell data to render
		 */
		void render_plugin_cell(int row, int col, int x, int y, int w, int h, const CPDAGNode& data);

		/**
		 * Renders a cell that contains a data value, or a parameter value to a plugin
		 *
		 * @param row row of the cell to render
		 * @param col colummn of the cell to render
		 * @param x top left x-coordinate of the cell to render on parent SheetView component
		 * @param y top left y-coordinate of the cell to render on parent SheetView component
		 * @param w width of cell to render
		 * @param h height of cell to render
		 * @param param the PluginParam data value to render
		 */
		void render_plugin_param(int row, int col, int x, int y, int w, int h, const PluginParam& param);

		/**
		 * Renders a cell that contains an AbstractBuf
		 *
		 * @param x top left x-coordinate of the cell to render on parent SheetView component
		 * @param y top left y-coordinate of the cell to render on parent SheetView component
		 * @param w width of cell to render
		 * @param h height of cell to render
		 * @param buf the AbstractBuf to render
		 */
		void render_abstract_buf_cell(int x, int y, int w, int h, AbstractBuf& buf);

		/**
		 * Renders a cell that contains a Drawable
		 *
		 * @param x top left x-coordinate of the cell to render on parent SheetView component
		 * @param y top left y-coordinate of the cell to render on parent SheetView component
		 * @param w width of cell to render
		 * @param h height of cell to render
		 * @param drawable the Drawable to render
		 */
		void render_drawable_cell(int x, int y, int w, int h, Drawable& drawable);

		/**
		 * Renders a cell that contains a CinePaintImage
		 *
		 * @param x top left x-coordinate of the cell to render on parent SheetView component
		 * @param y top left y-coordinate of the cell to render on parent SheetView component
		 * @param w width of cell to render
		 * @param h height of cell to render
		 * @param image the CinePaintImage to render
		 */
		void render_cpimage_cell(int x, int y, int w, int h, CinePaintImage& image);

		/**
		 * Convenience method to render an unrenderable mesaage representing a cell that cannot be rendered by this class
		 *
		 * @param x top left x-coordinate of the cell to render on parent SheetView component
		 * @param y top left y-coordinate of the cell to render on parent SheetView component
		 * @param w width of cell to render
		 * @param h height of cell to render
		 */
		void render_unrenderable_cell_data(int x, int y, int w, int h);


		/**
		 * Calculates the scaled sizes, maintaining the saspect ratio, to fit within the specified cell size.
		 * The width and height values represent the size of the original, and will be scaled as required
		 * to best fit inside the specied cell_width and cell_height to maintain the aspect ratio of
		 * the original. The input width and height values a re set to the scaled sizes
		 *
		 * @param width original width, set to scaled width
		 * @param height original width, set to scaled height
		 * @param cell_width cell width to fit into
		 * @param cell_height cell hight to fit into
		 */
		void calc_scale_to_fit_cell(int& width, int& height, int cell_width, int cell_height);



		//--------
		// Members

		/** SheetView reference this SheetCellRenderer renders cells for */
		SheetView& m_sheet_view;

		/** CPSpreadsheet Reference for cell data access */
		CPSpreadsheet& m_spreadsheet;



		//----------
		// Constants

		/** default RenderManager renderer name added to a CinePaintImage for rendering the thumbnail */
		static const char* CELL_RENDER_NAME;

		/** default string rendered when this class cannot render a data value */
		static const char* UNRENDERABLE_TEXT_STRING;

		/** Default cell padding between the border and cell data */
		static const int CELL_PADDING;
};

#endif /* _SHEET_CELL_RENDERER_ */
