/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      SheetViewWindow - window class fot holding sheet view
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: SheetCellRenderer.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "SheetCellRenderer.h"
#include "CPSpreadsheet.h"
#include "SheetView.h"
#include "CPDAGNode.h"
#include <plug-ins/pdb/AbstractBuf.h>
#include <plug-ins/pdb/AbstractRenderer.h>
#include <plug-ins/pdb/FlatBuf.h>
#include <plug-ins/pdb/CinePaintImage.h>
#include <plug-ins/pdb/Drawable.h>
#include <plug-ins/pdb/RenderingManager.h>
#include <app/CinePaintApp.h>
#include <plug-ins/pdb/CinePaintScaleOp.h>
#include <plug-ins/pdb/CinePaintRenderOp.h>
#include <plug-ins/pdb/PluginParam.h>
#include <utility/PlatformUtil.h> // req. for snprintf

#include <FL/fl_draw.H>

const char* SheetCellRenderer::CELL_RENDER_NAME = "spreadsheet_cell_renderer";
const char* SheetCellRenderer::UNRENDERABLE_TEXT_STRING = "Unrenderable Data";
const int SheetCellRenderer::CELL_PADDING = 3;


/**
 * Constructs a new SheetCellRenderer to render cells for the specified SheetView
 *
 * @param view the SheetView this SheetCellRenderer renders cells for
 * @param sheet the CPSpreadsheet interface into the CPDAG backing ths spreadsheet
 */
SheetCellRenderer::SheetCellRenderer(SheetView& view, CPSpreadsheet& sheet)
		: m_sheet_view(view), m_spreadsheet(sheet)
{}

/**
 * Destructor
 */
SheetCellRenderer::~SheetCellRenderer()
{}




/**
 * Renders the default cell backgroiund and border for the given cell
 *
 * @param row row of the cell to render
 * @param col colummn of the cell to render
 * @param x top left x-coordinate of the cell to render on parent SheetView component
 * @param y top left y-coordinate of the cell to render on parent SheetView component
 * @param w width of cell to render
 * @param h height of cell to render
 */
void SheetCellRenderer::RenderCell(int row, int col, int x, int y, int w, int h)
{
	Fl_Color prev_col = fl_color();

	fl_push_clip(x, y, w, h);

	// draw the white background
	fl_color(FL_WHITE);
	fl_rectf(x, y, w, h);

	// draw grey border
	fl_color(FL_GRAY);
	fl_line_style(FL_SOLID, 1);
	fl_rect(x, y, w, h);

	fl_pop_clip();

	fl_color(prev_col);
	fl_line_style(0);
}

/**
 * Renders the cell background/border based upon the cell type of the given cell
 *
 * @param row row of the cell to render
 * @param col colummn of the cell to render
 * @param x top left x-coordinate of the cell to render on parent SheetView component
 * @param y top left y-coordinate of the cell to render on parent SheetView component
 * @param w width of cell to render
 * @param h height of cell to render
 * @param node the CPDAGNode represented by the cell
 */
void SheetCellRenderer::RenderCellBackground(int row, int col, int x, int y, int w, int h, const CPDAGNode& node)
{
	Fl_Color prev_col = fl_color();

	fl_push_clip(x, y, w, h);

	if(node.IsDirty())
	{
		fl_color(FL_RED);
		fl_line_style(FL_SOLID, 3);
		fl_rect(x +1, y +1, w -2, h -2);
	}
	else
	{
		switch(node.GetData().GetType())
		{
				case CPDAGData::DAG_PLUGIN:
				{
					fl_color(FL_GREEN);
					fl_line_style(FL_SOLID, 3);
					fl_rect(x +1, y +1, w -2, h -2);
					break;
				}
				case CPDAGData::DAG_PLUGINPARAM:
				default:
				{
					break;
				}
		}
	}

	fl_pop_clip();

	fl_color(prev_col);
	fl_line_style(0);
}

/**
 * Renders the cell data held by the CPDAGNode.
 * This method handles the rendering for the different data type that a node may handle
 *
 * @param row row of the cell to render
 * @param col colummn of the cell to render
 * @param x top left x-coordinate of the cell to render on parent SheetView component
 * @param y top left y-coordinate of the cell to render on parent SheetView component
 * @param w width of cell to render
 * @param h height of cell to render
 * @param node the CPDAGNode represented by the cell
 */
void SheetCellRenderer::RenderCellData(int row, int col, int x, int y, int w, int h, const CPDAGNode& node)
{
	Fl_Color prev_col = fl_color();

	fl_push_clip(x + SheetCellRenderer::CELL_PADDING, y + SheetCellRenderer::CELL_PADDING, w - (2 * SheetCellRenderer::CELL_PADDING), h - (2 * SheetCellRenderer::CELL_PADDING));
	{
		fl_color(FL_BLACK);
		fl_font(FL_HELVETICA, 14);

		switch(node.GetData().GetType())
		{
				case CPDAGData::DAG_PLUGINPARAM:
				{
					// @TODO exaluate the node here and get the evaluation
					const PluginParam* param = node.GetEvaluation();
					if(param)
					{
						render_plugin_param(row, col, x, y, w, h, *param);
					}
					break;
				}
				case CPDAGData::DAG_PLUGIN:
				{
					render_plugin_cell(row, col, x, y, w, h, node);
					break;
				}
				default:
				{
					break;
				}
		}
	}

	fl_pop_clip();
	fl_color(prev_col);
	fl_line_style(0);
}



/**
 * Renders a selection rectangle over for the given cell highlighting the selected cell.
 * If the spcified cell is not selected, no action is taken.
 *
 * @param row row of the cell to render
 * @param col colummn of the cell to render
 * @param x top left x-coordinate of the cell to render on parent SheetView component
 * @param y top left y-coordinate of the cell to render on parent SheetView component
 * @param w width of cell to render
 * @param h height of cell to render
 */
void SheetCellRenderer::RenderCellSelection(int row, int col, int x, int y, int w, int h)
{
	Fl_Color prev_col = fl_color();
	fl_push_clip(x, y, w, h);

	int top, bottom, left, right;
	m_sheet_view.get_selection(top, left, bottom, right);

	// Keyboard nav and mouse selection highlighting
	if(row >= top && row <= bottom && col >= left && col <= right)
	{
		// active cell
		fl_color(FL_BLACK);
		fl_line_style(FL_SOLID, 3);
		fl_rect(x +1, y +1, w -2, h -2);
	}

	fl_pop_clip();
	fl_color(prev_col);
	fl_line_style(0);
}

/**
 * Renders a cell that contains the name of a plugin to be called
 * The CPSpreadsheet instance is used to rebuild the complete plugin string
 * from the cell data.
 *
 * @param row row of the cell to render
 * @param col colummn of the cell to render
 * @param x top left x-coordinate of the cell to render on parent SheetView component
 * @param y top left y-coordinate of the cell to render on parent SheetView component
 * @param w width of cell to render
 * @param h height of cell to render
 * @param data the cell data to render
 */
void SheetCellRenderer::render_plugin_cell(int row, int col, int x, int y, int w, int h, const CPDAGNode& data)
{
	if(data.IsDirty())
	{
		std::string s = m_spreadsheet.RebuildPluginString(row, col);
		fl_draw(s.c_str(), x + SheetCellRenderer::CELL_PADDING, y + SheetCellRenderer::CELL_PADDING, w - (2 * SheetCellRenderer::CELL_PADDING), h - (2 * SheetCellRenderer::CELL_PADDING), FL_ALIGN_LEFT);
	}
	else
	{
		const PluginParam* param = data.GetEvaluation();
		if(param)
		{
			render_plugin_param(row, col, x, y, w, h, *param);
		}
		else
		{
			fl_draw("No Data Returned from Plugin", x + SheetCellRenderer::CELL_PADDING, y + SheetCellRenderer::CELL_PADDING, w - (2 * SheetCellRenderer::CELL_PADDING), h - (2 * SheetCellRenderer::CELL_PADDING), FL_ALIGN_LEFT);
		}
	}
}

/**
 * Renders a cell that contains a data value, or a parameter value to a plugin
 *
 * @param row row of the cell to render
 * @param col colummn of the cell to render
 * @param x top left x-coordinate of the cell to render on parent SheetView component
 * @param y top left y-coordinate of the cell to render on parent SheetView component
 * @param w width of cell to render
 * @param h height of cell to render
 * @param param the PluginParam data value to render
 */
void SheetCellRenderer::render_plugin_param(int row, int col, int x, int y, int w, int h, const PluginParam& param)
{
	switch(param.GetType())
	{
			case PDB_INT:
			{
				char buf[64];
				snprintf(buf, 64, "%d", param.GetArg().pdb_int);
				fl_draw(buf, x + SheetCellRenderer::CELL_PADDING, y + SheetCellRenderer::CELL_PADDING, w - (2 * SheetCellRenderer::CELL_PADDING), h - (2 * SheetCellRenderer::CELL_PADDING), FL_ALIGN_RIGHT);
				break;
			}
			case PDB_FLOAT:
			{
				char buf[64];
				snprintf(buf, 64, "%f", param.GetArg().pdb_float);
				fl_draw(buf, x + SheetCellRenderer::CELL_PADDING, y + SheetCellRenderer::CELL_PADDING, w - (2 * SheetCellRenderer::CELL_PADDING), h - (2 * SheetCellRenderer::CELL_PADDING), FL_ALIGN_RIGHT);
				break;
			}
			case PDB_CSTRING:
			{
				char* _s = param.GetArg().pdb_cstring;
				int _offset = 0;
				if(_s[0] == '`')
				{
					_offset = 1;
				}
				fl_draw(_s + _offset, x + SheetCellRenderer::CELL_PADDING, y + SheetCellRenderer::CELL_PADDING, w - (2 * SheetCellRenderer::CELL_PADDING), h - (2 * SheetCellRenderer::CELL_PADDING), FL_ALIGN_LEFT);
				break;
			}
			case PDB_ABSTRACTBUF:
			{
				AbstractBuf* buf = param.GetArg().pdb_abstractbuf;
				if(buf)
				{
					render_abstract_buf_cell(x, y, w, h, *buf);
				}
				else
				{
					fl_draw("No Image Data", x + SheetCellRenderer::CELL_PADDING, y + SheetCellRenderer::CELL_PADDING, w - (2 * SheetCellRenderer::CELL_PADDING), h - (2 * SheetCellRenderer::CELL_PADDING), FL_ALIGN_LEFT);
				}
				break;
			}
			case PDB_DRAWABLE:
			{
				Drawable* drawable = param.GetArg().pdb_drawable;
				if(drawable)
				{
					render_drawable_cell(x, y, w, h, *drawable);
				}
				else
				{
					fl_draw("No Image Data", x + SheetCellRenderer::CELL_PADDING, y + SheetCellRenderer::CELL_PADDING, w - (2 * SheetCellRenderer::CELL_PADDING), h - (2 * SheetCellRenderer::CELL_PADDING), FL_ALIGN_LEFT);
				}
				break;
			}
			case PDB_CPIMAGE:
			{
				CinePaintImage* image = param.GetArg().pdb_cpimage;
				if(image)
				{
					render_cpimage_cell(x, y, w, h, *image);
				}
				else
				{
					fl_draw("No Image Data", x + SheetCellRenderer::CELL_PADDING, y + SheetCellRenderer::CELL_PADDING, w - (2 * SheetCellRenderer::CELL_PADDING), h - (2 * SheetCellRenderer::CELL_PADDING), FL_ALIGN_LEFT);
				}
				break;
			}
			case PDB_VOIDPOINTER:
			default:
			{
				break;
			}
	}
}

/**
 * Renders a cell that contains an AbstractBuf
 *
 * @param x top left x-coordinate of the cell to render on parent SheetView component
 * @param y top left y-coordinate of the cell to render on parent SheetView component
 * @param w width of cell to render
 * @param h height of cell to render
 * @param buf the AbstractBuf to render
 */
void SheetCellRenderer::render_abstract_buf_cell(int x, int y, int w, int h, AbstractBuf& buf)
{
	// calc the scaled instance
	int scale_w = buf.GetWidth();
	int scale_h = buf.GetHeight();
	calc_scale_to_fit_cell(scale_w, scale_h, w, h);

	// @TODO cache this!
	//       instead of scaling/converting everytime we need to render the cell, it would be far better to
	//       cache the result and only rescale etc. when we need to,
	//       for example, on if the cell is changed, deleted, or the plugin re-rerun
	CinePaintTag tag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false);
	FlatBuf dst_buf(tag, scale_w, scale_h);
	dst_buf.RefPortionReadWrite(0, 0);

	PixelArea* src_area = buf.GetPixelArea(0, 0, buf.GetWidth(), buf.GetHeight());
	PixelArea* dest_area = dst_buf.GetPixelArea(0, 0, dst_buf.GetWidth(), dst_buf.GetHeight());
	CinePaintScaleOp* scale_op = CinePaintApp::GetInstance().GetImageOpManager().GetScaleOp(buf.GetTag());

	if(scale_op)
	{
		scale_op->ScaleArea(*src_area, *dest_area);
		const unsigned char* data = dst_buf.GetPortionData(0, 0);

		if(data)
		{
			// center within the cell
			int pos_x = x;
			int pos_y = y;

			if(w > dst_buf.GetWidth())
			{
				pos_x += (w - dst_buf.GetWidth()) /2;
			}

			if(h > dst_buf.GetHeight())
			{
				pos_y += (h - dst_buf.GetHeight()) /2;
			}

			fl_draw_image(data, pos_x, pos_y, dst_buf.GetWidth(), dst_buf.GetHeight(), 3);
		}
		else
		{
			printf("SheetView::render_abstract_buf_cell: No image data to render!!\n");
		}
	}
	else
	{
		// @TODO we should prbably attept to draw at least some of the image here, just not scaled,
		//       and as a last resort handle as unrenderable.
		render_unrenderable_cell_data(x, y, w, h);
	}

	dst_buf.UnrefPortion(0, 0);
}

/**
 * Renders a cell that contains a Drawable
 *
 * @param x top left x-coordinate of the cell to render on parent SheetView component
 * @param y top left y-coordinate of the cell to render on parent SheetView component
 * @param w width of cell to render
 * @param h height of cell to render
 * @param drawable the Drawable to render
 */
void SheetCellRenderer::render_drawable_cell(int x, int y, int w, int h, Drawable& drawable)
{
	AbstractBuf& src_buf = drawable.GetBuffer();
	render_abstract_buf_cell(x, y, w, h, src_buf);
}

/**
 * Renders a cell that contains a CinePaintImage
 *
 * @param x top left x-coordinate of the cell to render on parent SheetView component
 * @param y top left y-coordinate of the cell to render on parent SheetView component
 * @param w width of cell to render
 * @param h height of cell to render
 * @param image the CinePaintImage to render
 */
void SheetCellRenderer::render_cpimage_cell(int x, int y, int w, int h, CinePaintImage& image)
{
	AbstractRenderer* renderer = image.GetRenderingManager().GetRenderer(CELL_RENDER_NAME);

	if(!renderer)
	{
		printf("No renderer\n");
		CinePaintRenderOp* _op = CinePaintApp::GetInstance().GetImageOpManager().GetRenderOp(image.GetTag());
		if(_op)
		{
			// calc the scaled instance
			int scale_w = image.GetWidth();
			int scale_h = image.GetHeight();
			calc_scale_to_fit_cell(scale_w, scale_h, w, h);

			renderer = _op->CreateScaledScreenRendererInstance(image, scale_w, scale_h);

			if(renderer)
			{
				// install a default renderer
				image.GetRenderingManager().RegisterRenderer(CELL_RENDER_NAME, renderer);
			}
			else
			{
				printf("Error: Cannot instantiate a Renderer to render Image Data\n");
			}
		}
		else
		{
			// no render installed
			printf("Error: No RenderOp Installed, or Format/Precision not supported\n");
		}
	}

	if(renderer)
	{
		renderer->Render();
		const unsigned char* data = renderer->GetRender().GetPortionData(0, 0);
		if(data)
		{
			// center within the cell
			int pos_x = x;
			int pos_y = y;

			if(w > renderer->GetWidth())
			{
				pos_x += (w - renderer->GetWidth()) /2;
			}

			if(h > renderer->GetHeight())
			{
				pos_y += (h - renderer->GetHeight()) /2;
			}

			fl_draw_image(data, pos_x, pos_y, renderer->GetRender().GetWidth(), renderer->GetRender().GetHeight(), 3);
		}
		else
		{
			printf("SheetView::render_cpimage_cell: No image data to render!!\n");
		}
	}
	else
	{
		render_unrenderable_cell_data(x, y, w, h);
	}
}

/**
 * Convenience method to render an unrenderable mesaage representing a cell that cannot be rendered by this class
 *
 * @param x top left x-coordinate of the cell to render on parent SheetView component
 * @param y top left y-coordinate of the cell to render on parent SheetView component
 * @param w width of cell to render
 * @param h height of cell to render
 */
void SheetCellRenderer::render_unrenderable_cell_data(int x, int y, int w, int h)
{
	fl_line(x, y, x + w, y + h);
	fl_line(x + w, y, x, y + h);

	int text_w, text_h;
	fl_measure(UNRENDERABLE_TEXT_STRING, text_w, text_h, 0);

	int text_pos_x = x + (w / 2) - (text_w / 2);
	int text_pos_y = y + (h / 2) - (text_h / 2);
	if((w - (2 * SheetCellRenderer::CELL_PADDING)) < text_w) { text_pos_x = x + SheetCellRenderer::CELL_PADDING; }
	if((h - (2 * SheetCellRenderer::CELL_PADDING)) < text_h) { text_pos_y = y + SheetCellRenderer::CELL_PADDING; }

	// add a background for the text
	// @TODO the background should be a sheet configurable
	Fl_Color c = fl_color();
	fl_color(FL_WHITE);
	fl_rectf(text_pos_x, text_pos_y, text_w, text_h);
	fl_color(c);

	fl_draw(UNRENDERABLE_TEXT_STRING, text_pos_x, text_pos_y, text_w, text_h, FL_ALIGN_CENTER);
}


/**
 * Calculates the scaled sizes, maintaining the saspect ratio, to fit within the specified cell size.
 * The width and height values represent the size of the original, and will be scaled as required
 * to best fit inside the specied cell_width and cell_height to maintain the aspect ratio of
 * the original. The input width and height values a re set to the scaled sizes
 *
 * @param width original width, set to scaled width
 * @param height original width, set to scaled height
 * @param cell_width cell width to fit into
 * @param cell_height cell hight to fit into
 */
void SheetCellRenderer::calc_scale_to_fit_cell(int& width, int& height, int cell_width, int cell_height)
{
	int temp_width = width;
	int temp_height = height;

	float cell_ratio = static_cast<float>(cell_width) / static_cast<float>(cell_height);
	float img_ratio = static_cast<float>(width) / static_cast<float>(height);

	if(cell_ratio > img_ratio)
	{
		height = cell_height;
		width = static_cast<int>((static_cast<float>(height)/static_cast<float>(temp_height)) * static_cast<float>(temp_width));
	}
	else
	{
		width = cell_width;
		height = static_cast<int>((static_cast<float>(width)/static_cast<float>(temp_width)) * static_cast<float>(temp_height));
	}
}
