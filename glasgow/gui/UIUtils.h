/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      UIUtils - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: UIUtils.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _UIUTILS_H_
#define _UIUTILS_H_

#include "dll_api.h"
#include <FL/Enumerations.H>
#include <app/CinePaintEvent.h>

// forward declaration
class AbstractBuf;
class Fl_Box;
class Fl_Image;
class Fl_Value_Slider;
class Fl_Window;

/**
 * Various useful UI methods and constants
 */
class CINEPAINT_GUI_API UIUtils
{
	public:
		~UIUtils() {} ;

		/** general row height for a label entry type combination row */
		static const int DEFAUTL_ROW_HEIGHT;

		/** general test labelled button height */
		static const int BTN_LABEL_HEIGHT;

		/** Default padding between widgets */
		static const int DEFAULT_X_PAD;

		/** Default y padding between widgets */
		static const int DEFAULT_Y_PAD;

		/**
		 * Default layout height ... 
		 * I am using this as a dummy value to set initial Fl_Group heights. When a group is resied, the
		 * child components get moved/resized (FLTK inconsistancies? ...), so we need a known value to work with
		 * initially. This provides some degree of flexibility with all the absolutely postioned widgets.
		 * This might end up causing more trouble than its worth, but I'll go with it for now
		 * ... layout managers?
		 */
		static const int DUMMY_DEFAULT_HEIGHT;

		/**
		 * simple layout helping class to track positioning/sizes/totals of added widgets
		 * There has to be a better way of doing this ... layout managers 
		 */
		class LayoutHelper
		{
			public:
				LayoutHelper();
				LayoutHelper(int x, int y, int sw, int sh);
				int m_total_width;
				int m_total_height;
				int m_posx;
				int m_posy;
				int m_suggested_width;
				int m_suggested_height;
				int m_actual_width;
				int m_actual_height;
		};

		/**
		 * Crates an FL_Image from an AbstractBuf.
		 * The AbstractBuf is expected to contain unsigned char RGB image data, if this is not the case,
		 * this method returns 0
		 *
		 * @param buf the AbstractBuf to create an Fl_Image from
		 * @return the constructed Fl_Image, or 0 if the AbstractBuf is an incompatible format
		 */
		static Fl_Image* CreateFlImage(const AbstractBuf& buf);

		/**
		 * Convenience method to create an aligned textual label
		 *
		 * @param text label text
		 * @return newly constructed Fl_Box with specified label left aligned
		 */
		static Fl_Box* CreateLabel(int x, int y, int w, int h, const char* label, Fl_Align align = FL_ALIGN_LEFT);

		/**
		 * Convenience method to Creates a consistant looking value slider
		 *
		 * @param x x position of the widget
		 * @param y y position of the widget
		 * @param w width of the widget
		 * @param h height of the widget
		 * @param label label of the widget
		 * @param min min value of the slide range
		 * @param max max value of the slide range
		 * @param val initial value of the slide
		 * @param step step increment of the slide
		 * @return a newly allocated Fl_Value_Slider
		 */
		static Fl_Value_Slider* CreateSlider(int x, int y, int w, int h, double min, double max, double val, double step);


		/**
		 * Simple window destructor Event.
		 * Fl_Window does not automatically destroy a window when closed, it is only hidden.
		 * A callback routine may be added to the window, however aws this is a callback from 
		 * the window, it is not advisable to delete the window during its callback.
		 * One approach to actually delete the window is to defer the actual destruction to the
		 * idle loop. This event may be posted to an idle loop event list to destroy the
		 * window.
		 * i.e. register a window callback to taker any specific action, then post this event to destroy
		 * the window outside the window callback.
		 * Note: be aware of what is actually happening here, the window is deleted, there should be no
		 * remaining pointers to the window, use the window callback to set any remainging pointers to 0.
		 */
		class IdleLoopWindowDestructor : public CinePaintEvent
		{
			public:
				IdleLoopWindowDestructor(Fl_Window* window);
				virtual void Process();
			protected:
			private:
				Fl_Window* m_window;
		};


		/**
		 * Convert an fltk key state event into a cinepaint mouse modifier 
		 * @param _state the state as returned by Fl:event_state()
		 * @return the state of modifier keys using CinePaintMouseModifier
		**/
		static unsigned int fltk_event_state_to_cp_mouse_modifier( int _state );
	protected:

	private:
		/**
		 * Disallow instances
		 */
		UIUtils() {} ;

}; /* class UIUtils */

#endif /* _UIUTILS_H_ */
