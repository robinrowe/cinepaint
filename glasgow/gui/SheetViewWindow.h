/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      SheetViewWindow - window class fot holding sheet view
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: SheetViewWindow.h,v 1.2 2006/12/21 11:18:07 robinrowe Exp $
 */

#ifndef _SHEET_VIEW_WINDOW_H_
#define _SHEET_VIEW_WINDOW_H_

#include "dll_api.h"
#include "SheetChangeListener.h"
#include <FL/Fl_Double_Window.H>
#include <plug-ins/pdb/World.h>
#include <string>

class CellParser;
class ConnectionPanel;
class CPDAGDoc;
class CPSpreadsheet;
class Fl_Box;
class Fl_Input;
class Fl_Menu_;
class Fl_Menu_Bar;
class Fl_Menu_Button;
class MenuFunctorHelper;
class PluginParam;
class SheetKeyListener;
class SheetMouseListener;
class SheetSelectionTools;
class SheetView;

/**
 * Main Spreadsheet display window
 * The SheetViewWindow provides the main interface window to the Spreadsheet for Image.
 * Each SheetViewWindow has a single SheetView which represents the actual spreadsheet
 * display and a single CPSpreadsheet used to access the graph data structure backing
 * the spreadsheet.
 *
 */
class CINEPAINT_GUI_API SheetViewWindow : public Fl_Double_Window
{	public:
		/**
		 * Constructs a new SheetViewWindow of the specified size
		 *
		 * @param w the window width
		 * @param h the window height
		 * @param titlel window title
		 */
		SheetViewWindow(int w, int h, const char* title);

		/**
		 * Destructor
		 */
		virtual ~SheetViewWindow();



		//------------------
		// CPDAGDoc handling

		/**
		 * Returns the CPDAGDoc being displayed by this window
		 *
		 * @return the CPDAGDoc displayed by this window
		 */
		CPDAGDoc* GetDAGDoc();
		const CPDAGDoc* GetDAGDoc() const;

		/**
		 * Sets the CPDAGDoc displayed by this window.
		 *
		 * @param doc the CPDAGDoc to display
		 */
		void SetDAGDoc(CPDAGDoc& doc);



		//---------------
		// Dialog Control

		/**
		 * Toggles the visiblity of the cell Connection Dialog
		 *
		 */
		void ToggleConnectionsDialog();

		/**
		 * Sets the visibility of the spreadsheet cell connections dialog
		 *
		 * @param visible set true to display the dialog
		 */
		void SetConnectionsDialogVisible(bool visible);

	protected:

	private:

		/**
		 * Convenience method to build the Window menu
		 *
		 * @param the menu to add items to
		 * @param menu_helper used to manage constructed menu item functors
		 */
		void build_menu(Fl_Menu_Bar& menu, MenuFunctorHelper& menu_helper);

		/**
		 * Convenience method to build the sheet view popup menu
		 *
		 * @param the menu to add items to
		 * @param menu_helper used to manage constructed menu item functors
		 */
		void build_popup_menu(Fl_Menu_& menu, MenuFunctorHelper& menu_helper);


		void update_cur_cell_text(int row, int col);
		void update_cur_cell_text(int start_row, int start_col, int end_row, int end_col);

		void clear_input_bar();
		void update_input_bar(const std::string& data, int row, int col);
		void update_input_bar(const PluginParam& param, int row, int col);


		//-----------------
		// Widget callbacks

		static void static_accept_cell_edit_cb(Fl_Widget* w, void* something) { static_cast<SheetViewWindow*>(something)->accept_cell_edit_cb(); }
		static void static_cancel_cell_edit_cb(Fl_Widget* w, void* something) { static_cast<SheetViewWindow*>(something)->cancel_cell_edit_cb(); }
		static void static_text_input_cb(Fl_Widget* w, void* something) { static_cast<SheetViewWindow*>(something)->text_input_cb(); }
		static void static_display_plugin_function_builder_cb(Fl_Widget* w, void* something) {static_cast<SheetViewWindow*>(something)->display_plugin_function_builder(); }
		static void static_display_connections_cb(Fl_Widget* w, void* something) {static_cast<SheetViewWindow*>(something)->SetConnectionsDialogVisible(true); }
		static void static_filename_selection_cb(Fl_Widget* w, void* something) { static_cast<SheetViewWindow*>(something)->filename_selection_cb(); }
		static void static_evaluate_cell_cb(Fl_Widget* w, void* something) { static_cast<SheetViewWindow*>(something)->evaluate_cell_cb(); }
		static void static_evaluate_sheet_cb(Fl_Widget* w, void* something) { static_cast<SheetViewWindow*>(something)->evaluate_sheet_cb(); }
		static void static_connections_dialog_close_cb(Fl_Widget* w, void* something) { static_cast<SheetViewWindow*>(something)->connections_dialog_close_cb(); }

		void accept_cell_edit_cb();
		void cancel_cell_edit_cb();
		void text_input_cb();
		void display_plugin_function_builder();
		void filename_selection_cb();
		void evaluate_cell_cb();
		void evaluate_sheet_cb();
		void connections_dialog_close_cb();
		void file_close_cb();




		//--------
		// Members

		/** the CPDAGDoc containing the CPDAG */
		CPDAGDoc* m_dag_doc;

		/** CPSpreadsheet interface to the DAG */
		CPSpreadsheet* m_spreadsheet;


		/** handler for mouse events generated by the SheetView */
		SheetMouseListener* m_sheet_mouse_listener;

		/** handler fot key events generated by the SheetView */
		SheetKeyListener* m_sheet_key_listener;

		/** handles selection operations from the sheet view */
		SheetSelectionTools* m_sheet_selection_tools;

		//--------
		// Widgets

		/** manages registerd menu helper functors */
		MenuFunctorHelper* m_menu_functor_helper;

		/** the view of the CPDAG */
		SheetView* m_sheet_view;

		/** text buffer for the currently selected cell */
		char* m_curr_cell_text;

		/** displays the currently selected cell */
		Fl_Box* m_cell_box;

		/** spreadsheet text input bar input bar */
		Fl_Input* m_cell_input;

		/** sheet popup menu */
		Fl_Menu_Button* m_popup_menu;


		// dialogs

		/** spreadsheet cell ceonnetions dialog */
		ConnectionPanel* m_cell_connections_dialog;




		//---------------
		// Nested Classes

		/**
		 * DataInputProcessor processes textual input for entry into the spreadsheet
		 *
		 */
		class DataInputProcessor
		{
			public:
				DataInputProcessor(CPSpreadsheet& sheet, SheetView& view);
				virtual ~DataInputProcessor();

				/**
				 * Process textual input for entry into the spreadsheet.
				 * This method deteremines the type of the textual data and sets the appropriate
				 * data within the spreadsheet
				 *
				 * @param text the input text
				 */
				void ProcessInput(const std::string& text);

			protected:
			private:
				/**
				 * Inserts the textual input as a plugin parameter into the currently selected spreadsheet cell 
				 * The textual data is first converted to the apprpriate data type, depending
				 * upon how the text was parsed.
				 *
				 * @param cp the CellParser used to parse the input string
				 */
				void process_param_text(CellParser& cp);

				/**
				 * Inserts the textual input as a plugin cell into the currently selected spreadsheet cell 
				 * The input string is processed further to attach nodes within the spreadsheet to represent
				 * input into the plugin.
				 *
				 * @param cp the CellParser used to parse the input string
				 */
				void process_plugin_text(CellParser& cp);

				CPSpreadsheet& m_sheet;
				SheetView& m_view;
		};

		/**
		 * SheetChangeListener implementation to monitor for changes made upon the SheetView
		 * Handles updating the currently selected cell and changes to cell data made directly in the
		 * changed cell
		 *
		 */
		class CellChangeListener : public SheetChangeListener
		{
			public:
				CellChangeListener(CPSpreadsheet& sheet, SheetViewWindow& window);

				virtual void CursorCellChanged(int row, int col);
				virtual void RangeChanged(int row_start, int col_start, int row_end, int col_end);
				virtual void ValueChanged(int row, int col, const std::string& val);

			protected:
			private:
				CPSpreadsheet& m_sheet;
				SheetViewWindow& m_sheet_window;
		};

		/** processes textual input for entry into a cell within the spreadsheet */
		DataInputProcessor* m_cell_input_handler;

		/** handles changes upon the SheetView */
		CellChangeListener* m_sheet_change_listener;
};

#endif /* _SHEET_VIEW_WINDOW_H_ */
