/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Projection - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: Projection.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CINEPAINT_PROJECTION_H_
#define _CINEPAINT_PROJECTION_H_

#include "dll_api.h"
#include "plug-ins/pdb/CinePaintTag.h"
#include "utility/CPRect.h"

// forward declaration
class AbstractBuf;
class AbstractRenderer;
class CinePaintImage;

/**
 * Projection manages a 'view' of a rendered CinePaintImage.
 * AbstractRenderers renderors may be used to render a CinePaintImage to an buffer as a final render
 * or composition of the CinePaintImage, however this prcess may be intensive as multiple layers may need
 * combined etc.
 * A Projection is used to convert from a rendered image to a displayable image, and provide simple
 * control over what is viewed, bounds zoom, etc.
 *
 */
class CINEPAINT_GUI_API Projection
{
	public:
		/**
		 * Constructs a Projection with the specified sizes
		 *
		 * @param width the width of this Projection
		 * @param height the height of thie Projection
		 */
		Projection(int width, int height);

		/**
		 * Destructor, destroys the underlying projection image buffer
		 */
		virtual ~Projection();

		/**
		 * Returns the CinePaintTag indicating the data format of this Projection
		 *
		 * @return the CinePaintTag indicating the data format of this Projection
		 */
		const CinePaintTag& GetTag() const;
		CinePaintTag& GetTag();

		/**
		 * Returns the Width of this CinepaintImage
		 *
		 * @return the width of this CinePaintImage
		 */
		int GetWidth() const;

		/**
		 * Returns the height of this CinePaintImage
		 *
		 * @retunr the height of this CinePaintImage
		 */
		int GetHeight() const;

		/**
		 * Sets the size of this projection.
		 * After setting the size, the projection becomes invalid and will require
		 * a re-projection.
		 *
		 * @param int width the new Projection width
		 * @param height the new Projrction height
		 */
		void SetSize(int width, int height);

		/**
		 * Gets the X offset of this Projection over the render being projected
		 *
		 * @return the x offset
		 */
		int GetOffSetX() const;

		/**
		 * Gets the Y offset of this Projection over the render being projected
		 *
		 * @return the y offset
		 */
		int GetOffSetY() const;

		/**
		 * Sets the offset of this Projection over the render being projected
		 *
		 * @param x the x offset
		 * @param y the y offset
		 */
		void SetOffSet(int x, int y);

		/**
		 * Sets the current zoom level of this projection over the render
		 * A 1.0 is considered 1:1. Zoom values <= 0.0 are ignored.
		 *
		 * @param zoom the new zoom level
		 */
		void SetZoom(double zoom);

		/**
		 * Returns the curtrent zoom of this Projection over the render
		 *
		 * @return the current zoom level
		 */
		double GetZoom() const;

		/**
		 * Sets the renderer rendering the CinePaintImage for this projection.
		 *
		 * @param Renderer rendering the CinePaintImage used within this projection
		 */
		void SetRenderer(AbstractRenderer* renderor);

		/**
		 * Returns the AbstractBuf representing this projection ready for display on screen
		 *
		 * @return the projection AbstractBuf to be drawn on screen
		 */
		AbstractBuf* GetProjection();

		void InvalidateImageRegion(int x, int y, int w, int h);
		void InvalidateRegion(int x, int y, int w, int h);

	protected:

	private:

		/**
		 * Constructs the projection data buffer at the specified size
		 * if the projection buffer already exists, it is first destroyed, and a new buffer created.
		 *
		 * @param width the Projection buffer size
		 * @param height the projection buffer height
		 */
		void create_projection(int width, int height);

		/**
		 * Renders this Projection from the set AbstractRenderer data.
		 * If the set renderer is invalid, Render is first called upon the renderer to Render
		 * image data, this Projection is then calculated and rendered using the current offset
		 * and zoom settings
		 *
		 */
		void project(const CPRect& update_region);

		/**
		 * Returns true if this projection is currently valid.
		 * For the prjection to be valid, the invalid region must be empty
		 *
		 * @return true if this Projection is valid
		 */
		bool is_vaild() const;


		/**
		 * Clears the current invalid region
		 *
		 */
		void clear_invalid_region();

		/** image format information */
		CinePaintTag m_tag;

		/** image region displayed by this projection width */
		CPRect m_projection_region;

		/** current zoom level of prjection over render - 1.0 = 1:1 */
		double m_zoom;


		/** indicates the current invalid region, if set - this region is in projectio space */
		CPRect m_invalid_region;

		/** The renderor rendering the CinePaintImage -- layers & channels */
		AbstractRenderer* m_renderer;

		/** The current projection view, rendered for screen use including zoom, offset etc */
		AbstractBuf* m_projection;

} ; /* class Projection */

#endif /* _CINEPAINT_PROJECTION_H_ */
