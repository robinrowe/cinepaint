/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ConnectionPanel - Spreadsheet connection dialog
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ConnectionPanel.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "ConnectionPanel.h"
#include "CPSpreadsheet.h"
#include "SheetView.h"
#include <gui/CinePaintFrame.h>
#include <gui/UIUtils.h>
#include "CPDAG.h"
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Hold_Browser.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl_Return_Button.H>
#include <map>

/**
 * Constructs a new ConnectionPanel
 *
 * @param w the dialog width
 * @param h the dialog height
 * @param sheet_view the SheetView used to listen for cell cursor changes
 * @param title the dislaog title
 */
ConnectionPanel::ConnectionPanel(int w, int h, SheetView& sheet_view, const char* title)
		: Fl_Window(w, h, title), m_sheet_view(sheet_view)
{
	int y_pos = UIUtils::DEFAULT_Y_PAD;
	int x_pos = UIUtils::DEFAULT_X_PAD;

	int temp_x_pos = x_pos;
	Fl_Pack* cell_pack = new Fl_Pack(x_pos, y_pos, this->w() - (2 * UIUtils::DEFAULT_X_PAD), UIUtils::DEFAUTL_ROW_HEIGHT);
	cell_pack->type(Fl_Pack::HORIZONTAL);
	cell_pack->spacing(UIUtils::DEFAULT_X_PAD);

	UIUtils::CreateLabel(temp_x_pos, y_pos, 50, UIUtils::DEFAUTL_ROW_HEIGHT, "Cell:");
	temp_x_pos += 50 + UIUtils::DEFAULT_X_PAD;
	m_selected_cell_input = new Fl_Input(temp_x_pos, y_pos, 50, UIUtils::DEFAUTL_ROW_HEIGHT);
	m_selected_cell_input->callback(static_change_cell_cb, this);
	temp_x_pos += 50 + UIUtils::DEFAULT_X_PAD;
	Fl_Return_Button* change_btn = new Fl_Return_Button(temp_x_pos, y_pos, UIUtils::BTN_LABEL_HEIGHT, UIUtils::BTN_LABEL_HEIGHT);
	change_btn->callback(static_change_cell_cb, this);
	temp_x_pos += UIUtils::BTN_LABEL_HEIGHT + UIUtils::DEFAULT_X_PAD;
	m_auto_change_ck_btn = new Fl_Check_Button(temp_x_pos, y_pos, 50, UIUtils::BTN_LABEL_HEIGHT, "Auto Change");
	m_auto_change_ck_btn->callback(static_auto_change_cell_cb, this);

	cell_pack->end();
	y_pos += UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD;

	// frame spacing values
	int frame_border = 5;

	// @[Hack] hack for label text height, we cant get the text height until the first time its drawn, so we fake it!!!
	int frame_offset_y = (frame_border * 2) + 20;

	// total additional frame height added to total widget height
	int frame_height_extra = frame_offset_y + (frame_border * 2);
	int frame_internal_x_offset = 2 * frame_border;


	int browser_frame_height = (this->h() - y_pos - (2 * UIUtils::DEFAULT_Y_PAD)) / 2;
	int browser_height = browser_frame_height - frame_height_extra - UIUtils::DEFAULT_Y_PAD - UIUtils::BTN_LABEL_HEIGHT;
	int browser_width = this->w() - (2 * UIUtils::DEFAULT_X_PAD) - (2 * frame_internal_x_offset);

	CinePaintFrame* in_conns_frame = new CinePaintFrame(x_pos, y_pos, this->w() - (2 * UIUtils::DEFAULT_X_PAD), browser_frame_height, "Inward Connections", FL_ENGRAVED_FRAME);
	y_pos += frame_offset_y;
	x_pos += frame_internal_x_offset;

	m_inward_conns = new Fl_Hold_Browser(x_pos, y_pos, browser_width, browser_height);
	y_pos += browser_height + UIUtils::DEFAULT_Y_PAD;

	Fl_Button* in_disconnect_btn = new Fl_Button(x_pos, y_pos, browser_width, UIUtils::BTN_LABEL_HEIGHT, "Disconnect Selected");
	in_disconnect_btn->callback(static_disconnect_inward_conn_cb, this);
	y_pos += UIUtils::BTN_LABEL_HEIGHT + UIUtils::DEFAULT_Y_PAD;
	in_conns_frame->end();

	y_pos += UIUtils::DEFAULT_Y_PAD;

	x_pos = UIUtils::DEFAULT_X_PAD;
	CinePaintFrame* out_conns_frame = new CinePaintFrame(x_pos, y_pos, this->w() - (2 * UIUtils::DEFAULT_X_PAD), browser_frame_height, "Outward Connections", FL_ENGRAVED_FRAME);
	y_pos += frame_offset_y;
	x_pos += frame_internal_x_offset;
	m_outward_conns = new Fl_Hold_Browser(x_pos, y_pos, browser_width, browser_height);
	y_pos += browser_height + UIUtils::DEFAULT_Y_PAD;

	Fl_Button* out_disconnect_btn = new Fl_Button(x_pos, y_pos, browser_width, UIUtils::BTN_LABEL_HEIGHT, "Disconnect Selected");
	out_disconnect_btn->callback(static_disconnect_outward_conn_cb, this);
	y_pos += UIUtils::BTN_LABEL_HEIGHT + UIUtils::DEFAULT_Y_PAD;
	out_conns_frame->end();


	this->end();


	m_sheet_change_listener = 0;
	m_dag = 0;


	// automatically follow the selection by default
	m_auto_change_ck_btn->set();
	m_auto_change_ck_btn->do_callback();
}


/**
 * Destructor
 */
ConnectionPanel::~ConnectionPanel()
{
	if(m_sheet_change_listener)
	{
		m_sheet_view.RemoveSheetChangeListener(m_sheet_change_listener);
		delete m_sheet_change_listener;
		m_sheet_change_listener = 0;
	}
}



/**
 * Sets the CPDAG used connections are displayed for
 *
 * @param dag the CPDAG this dialog displays node connections for
 */
void ConnectionPanel::SetDAG(CPDAG& dag)
{
	m_dag = &dag;

	if(m_auto_change_ck_btn->value())
	{
		handle_cell_changed();
	}
}

/**
 * Returns the CPDAG this dialog is displaying node connections for
 *
 * @return the CPDAG for which node connections are displayed
 */
const CPDAG* ConnectionPanel::GetDAG()
{
	return(m_dag);
}

/**
 * Sets the current cell for which node connections are displayed
 * The specified cell is looked up within the current CPDAG and the inward and
 * outward connections of the node are displayed upon the dialog
 *
 * @param cell the current cell for which to display node connections
 */
void ConnectionPanel::SetCell(const std::string& cell)
{
	m_selected_cell_input->value(cell.c_str());
	handle_cell_changed();
}




//-----------------
// Widget Callbacks
void ConnectionPanel::change_cell_cb()
{
	handle_cell_changed();
}

void ConnectionPanel::auto_change_cell_cb()
{
	if(m_auto_change_ck_btn->value())
	{
		m_selected_cell_input->readonly(true);

		if(!m_sheet_change_listener)
		{
			m_sheet_change_listener = new CellChangeListener(*this);
			m_sheet_view.AddSheetChangeListener(m_sheet_change_listener);
		}

		// handle the intial selection
		std::string s = CPSpreadsheet::CellName(m_sheet_view.get_current_row(), m_sheet_view.get_current_col());
		SetCell(s);
	}
	else
	{
		m_selected_cell_input->readonly(false);

		if(m_sheet_change_listener)
		{
			m_sheet_view.RemoveSheetChangeListener(m_sheet_change_listener);
			delete m_sheet_change_listener;
			m_sheet_change_listener = 0;
		}
	}
}

void ConnectionPanel::disconnect_inward_conn_cb()
{
	int selected = m_inward_conns->value();

	if(selected)
	{
		ConnRec* rec = reinterpret_cast<ConnRec*>(m_inward_conns->data(selected));

		std::string s(m_selected_cell_input->value());
		if(m_dag && !s.empty())
		{
			// @TODO need to handle the absolute column and absolute row values
			m_dag->Detach(rec->m_cell, s, rec->m_param, rec->m_eval, false, false);

			m_in_conn_recs.remove(rec);
			m_inward_conns->remove(selected);
		}
	}
}

void ConnectionPanel::disconnect_outward_conn_cb()
{
	int selected = m_outward_conns->value();

	if(selected)
	{
		ConnRec* rec = reinterpret_cast<ConnRec*>(m_outward_conns->data(selected));

		std::string s(m_selected_cell_input->value());
		if(m_dag && !s.empty())
		{
			// @TODO need to handle the absolute column and absolute row values
			m_dag->Detach(s, rec->m_cell, rec->m_param, rec->m_eval, false, false);

			m_out_conn_recs.remove(rec);
			m_outward_conns->remove(selected);
		}
	}
}

void ConnectionPanel::handle_cell_changed()
{
	// clear the browser widgets
	m_inward_conns->clear();
	m_outward_conns->clear();

	// clear our own internal data structures
	clear_in_conn_recs();
	clear_out_conn_recs();

	if(m_selected_cell_input->value())
	{
		std::string s(m_selected_cell_input->value());

		if(m_dag && !s.empty())
		{
			std::map<std::string, std::pair<std::string, std::string> > connections;
			m_dag->GetInputNodes(s, connections);

			for(std::map<std::string, std::pair<std::string, std::string> >::const_iterator citer = connections.begin(); citer != connections.end(); ++citer)
			{
				std::string display_string = citer->first;
				display_string.append(" : ");
				display_string.append(citer->second.first);

				if(!citer->second.second.empty())
				{
					display_string.append(" : ");
					display_string.append(citer->second.second);
				}

				ConnRec* rec = new ConnRec(citer->first, citer->second.first, citer->second.second);
				m_inward_conns->add(display_string.c_str(), rec);

				m_in_conn_recs.push_back(rec);
			}

			connections.clear();
			m_dag->GetOutputNodes(s, connections);
			for(std::map<std::string, std::pair<std::string, std::string> >::const_iterator citer = connections.begin(); citer != connections.end(); ++citer)
			{
				std::string display_string = citer->first;
				display_string.append(" : ");
				display_string.append(citer->second.first);

				if(!citer->second.second.empty())
				{
					display_string.append(" : ");
					display_string.append(citer->second.second);
				}

				ConnRec* rec = new ConnRec(citer->first, citer->second.first, citer->second.second);
				m_outward_conns->add(display_string.c_str(), rec);

				m_out_conn_recs.push_back(rec);
			}

		}
	}
}


/**
 * Clear the list of incoming connection records
 *
 */
void ConnectionPanel::clear_in_conn_recs()
{
	ConnRecContainer_t::iterator iter = m_in_conn_recs.begin();
	while(iter != m_in_conn_recs.end())
	{
		ConnRec* rec = *iter;
		iter = m_in_conn_recs.erase(iter);

		delete rec;
		rec = 0;
	}
}

/**
 * Clear the list of out going connection records
 *
 */
void ConnectionPanel::clear_out_conn_recs()
{
	ConnRecContainer_t::iterator iter = m_out_conn_recs.begin();
	while(iter != m_out_conn_recs.end())
	{
		ConnRec* rec = *iter;
		iter = m_out_conn_recs.erase(iter);

		delete rec;
		rec = 0;
	}
}






// Nested Classes

ConnectionPanel::CellChangeListener::CellChangeListener(ConnectionPanel& panel)
		: m_panel(panel)
{
}

void ConnectionPanel::CellChangeListener::CursorCellChanged(int row, int col)
{
	std::string s = CPSpreadsheet::CellName(row, col);
	m_panel.SetCell(s);
}

void ConnectionPanel::CellChangeListener::RangeChanged(int row_start, int col_start, int row_end, int col_end)
{

}

void ConnectionPanel::CellChangeListener::ValueChanged(int row, int col, const std::string& val)
{

}

