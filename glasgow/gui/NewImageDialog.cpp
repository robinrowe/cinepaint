/* ***** BEGIN LICENSE BLOCK *****

 * Version: MPL 1.1/GPL 2.0

 *

 * The contents of this file are subject to the Mozilla Public License Version

 * 1.1 (the "License"); you may not use this file except in compliance with

 * the License. You may obtain a copy of the License at

 * http://www.mozilla.org/MPL/

 *

 * Software distributed under the License is distributed on an "AS IS" basis,

 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License

 * for the specific language governing rights and limitations under the

 * License.

 *

 * The Original Code is CinePaint, an image sequence manipulation program

 *                      newImageDialog - GUI Module

 *

 * The Initial Developer of the Original Code is

 * the University of Glasgow.

 * Portions created by the Initial Developer are Copyright (C) 2004

 * the Initial Developer. All Rights Reserved.

 *

 * Contributor(s):

 *   Donald MacVicar

 *   Colin Law

 *   Stuart Ford

 *

 * Alternatively, the contents of this file may be used under the terms of

 * either the GNU General Public License Version 2 or later (the "GPL"),

 * in which case the provisions of the GPL are applicable instead of those

 * above. If you wish to allow use of your version of this file only under

 * the terms of the GPL, and not to allow others to use your version of this

 * file under the terms of the MPL, indicate your decision by deleting the

 * provisions above and replace them with the notice and other provisions

 * required by the GPL. If you do not delete the provisions above, a recipient

 * may use your version of this file under the terms of any one of the MPL,

 * or the GPL.

 *

 * ***** END LICENSE BLOCK *****

 *

 * $Id: NewImageDialog.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $

 */


#include <gui/NewImageDialog.h>


#include <gui/CinePaintFrame.h>

#include <gui/CinePaintSpinButton.h>

#include <gui/UIUtils.h>


#include <FL/Fl_Button.H>

#include <FL/Fl_Check_Button.H>

#include <FL/Fl_Pack.H>



NewImageDialog::NewImageDialog(int w, int h, const char* title)

: CinePaintWindow(w, (h < 0 ? UIUtils::DUMMY_DEFAULT_HEIGHT : h), title)

{

	// all this allows us to specifiy an absolute height, or assume a default row height

	// ... again, all we need is a proper layout manmager]


	UIUtils::LayoutHelper _helper;

	int _frame_border = 5;


	// @[Hack] hack for label text height, we cant get the text height until the first time its drawn, so we fake it!!!

	int _frame_offset_y = (_frame_border * 2) + 20;


	// total additional frame height added to total widget height

	int _frame_height_extra = _frame_offset_y + (_frame_border * 2);

	

	_helper.m_posy = 0;


	// tracks the frame internal widgets - frames themselves use the x paramater value

	int _frame_internal_x_offset = 2 * _frame_border;

	_helper.m_posx = _frame_internal_x_offset;

	

	// sizes

	CinePaintFrame* _size_frame = new CinePaintFrame(0, 0, w, _frame_height_extra + (UIUtils::DEFAUTL_ROW_HEIGHT * 2) + (UIUtils::DEFAULT_Y_PAD * 1), "Image Size", FL_ENGRAVED_FRAME);

	_size_frame->SetBorderWidth(_frame_border);

	_helper.m_posy += _frame_offset_y;


	int _2_col_width = (w - _frame_internal_x_offset - UIUtils::DEFAULT_X_PAD) / 2;

	Fl_Box* _width_label = UIUtils::CreateLabel(_helper.m_posx, _helper.m_posy, _2_col_width, UIUtils::DEFAUTL_ROW_HEIGHT, "Width", FL_ALIGN_LEFT);

	m_width = new CinePaintSpinButton(_helper.m_posx + _2_col_width, _helper.m_posy, _2_col_width, UIUtils::DEFAUTL_ROW_HEIGHT);

	_helper.m_posy += UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD;


	Fl_Box* _height_label = UIUtils::CreateLabel(_helper.m_posx, _helper.m_posy, _2_col_width, UIUtils::DEFAUTL_ROW_HEIGHT, "Height", FL_ALIGN_LEFT);

	m_height = new CinePaintSpinButton(_helper.m_posx + _2_col_width, _helper.m_posy, _2_col_width, UIUtils::DEFAUTL_ROW_HEIGHT);

	_helper.m_posy += UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD;


	_size_frame->end();

	_helper.m_posy += UIUtils::DEFAULT_Y_PAD + (_frame_border * 2);


	// image types

	CinePaintFrame* _type_frame = new CinePaintFrame(0, _helper.m_posy, w, _frame_height_extra + (UIUtils::DEFAUTL_ROW_HEIGHT * 2) + (UIUtils::DEFAULT_Y_PAD * 1), "Image Size", FL_ENGRAVED_FRAME);

	_type_frame->SetBorderWidth(_frame_border);

	_helper.m_posy += _frame_offset_y;


	m_rgb = new Fl_Check_Button(_helper.m_posx, _helper.m_posy, w, UIUtils::DEFAUTL_ROW_HEIGHT, "RGB");

	m_rgb->type(FL_RADIO_BUTTON);

	m_rgb->callback(static_format_cb, this);

	_helper.m_posy += UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD;

	m_greyscale = new Fl_Check_Button(_helper.m_posx, _helper.m_posy, w, UIUtils::DEFAUTL_ROW_HEIGHT, "Greyscale");

	m_greyscale->callback(static_format_cb, this);

	m_greyscale->type(FL_RADIO_BUTTON);

	_helper.m_posy += UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD;

	_type_frame->end();

	_helper.m_posy += UIUtils::DEFAULT_Y_PAD + (_frame_border * 2);


	// precision

	CinePaintFrame* _precision_frame = new CinePaintFrame(0, _helper.m_posy, w, _frame_height_extra + (UIUtils::DEFAUTL_ROW_HEIGHT * 5) + (UIUtils::DEFAULT_Y_PAD * 4), "Precision", FL_ENGRAVED_FRAME);

	_precision_frame->SetBorderWidth(_frame_border);

	_helper.m_posy += _frame_offset_y;


	m_prec_8bit_unsigned = new Fl_Check_Button(_helper.m_posx, _helper.m_posy, w, UIUtils::DEFAUTL_ROW_HEIGHT, "8 Bit Unsigned");

	m_prec_8bit_unsigned->type(FL_RADIO_BUTTON);

	m_prec_8bit_unsigned->callback(static_precision_cb, this);

	_helper.m_posy += UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD;

	m_prec_16bit_unsigned = new Fl_Check_Button(_helper.m_posx, _helper.m_posy, w, UIUtils::DEFAUTL_ROW_HEIGHT, "16 Bit Unsigned");

	m_prec_16bit_unsigned->type(FL_RADIO_BUTTON);

	m_prec_16bit_unsigned->callback(static_precision_cb, this);

	_helper.m_posy += UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD;

	m_prec_16bit_rnh = new Fl_Check_Button(_helper.m_posx, _helper.m_posy, w, UIUtils::DEFAUTL_ROW_HEIGHT, "16 Bit RnH");

	m_prec_16bit_rnh->type(FL_RADIO_BUTTON);

	m_prec_16bit_rnh->callback(static_precision_cb, this);

	_helper.m_posy += UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD;

	m_prec_32bit_float = new Fl_Check_Button(_helper.m_posx, _helper.m_posy, w, UIUtils::DEFAUTL_ROW_HEIGHT, "32 Bit IEEE Float");

	m_prec_32bit_float->type(FL_RADIO_BUTTON);

	m_prec_32bit_float->callback(static_precision_cb, this);

	_helper.m_posy += UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD;

	m_prec_16bit_fixed_point = new Fl_Check_Button(_helper.m_posx, _helper.m_posy, w, UIUtils::DEFAUTL_ROW_HEIGHT, "16 Bit Fixed (0.0 - 2.0)");

	m_prec_16bit_fixed_point->type(FL_RADIO_BUTTON);

	m_prec_16bit_fixed_point->callback(static_precision_cb, this);

	_helper.m_posy += UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD;

	_precision_frame->end();

	_helper.m_posy += UIUtils::DEFAULT_Y_PAD + (_frame_border * 2);


	// fill type

	CinePaintFrame* _fill_frame = new CinePaintFrame(0, _helper.m_posy, w, _frame_height_extra + (UIUtils::DEFAUTL_ROW_HEIGHT * 4) + (UIUtils::DEFAULT_Y_PAD * 3), "Fill", FL_ENGRAVED_FRAME);

	_precision_frame->SetBorderWidth(_frame_border);

	_helper.m_posy += _frame_offset_y;

	m_background = new Fl_Check_Button(_helper.m_posx, _helper.m_posy, w, UIUtils::DEFAUTL_ROW_HEIGHT, "Background");

	m_background->callback(static_fill_cb, this);

	m_background->type(FL_RADIO_BUTTON);

	_helper.m_posy += UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD;

	m_white = new Fl_Check_Button(_helper.m_posx, _helper.m_posy, w, UIUtils::DEFAUTL_ROW_HEIGHT, "White");

	m_white->callback(static_fill_cb, this);

	m_white->type(FL_RADIO_BUTTON);

	_helper.m_posy += UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD;

	m_trans = new Fl_Check_Button(_helper.m_posx, _helper.m_posy, w, UIUtils::DEFAUTL_ROW_HEIGHT, "Transparent");

	m_trans->callback(static_fill_cb, this);

	m_trans->type(FL_RADIO_BUTTON);

	_helper.m_posy += UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD;

	m_foreground = new Fl_Check_Button(_helper.m_posx, _helper.m_posy, w, UIUtils::DEFAUTL_ROW_HEIGHT, "Foreground");

	m_foreground->callback(static_fill_cb, this);

	m_foreground->type(FL_RADIO_BUTTON);

	_helper.m_posy += UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD;

	_fill_frame->end();

	_helper.m_posy += UIUtils::DEFAULT_Y_PAD + (_frame_border * 2);



	// create the action buttons


	int _btn_w = w / 3;

	int _btn_posx = _helper.m_posx + (_btn_w / 3);

	_helper.m_posy += UIUtils::DEFAULT_Y_PAD;

	Fl_Button* _create_button = new Fl_Button(_btn_posx, _helper.m_posy, _btn_w, UIUtils::BTN_LABEL_HEIGHT, "Create") ;

	_create_button->callback(static_create_button_cb, this) ;

	_btn_posx += _btn_w + (_btn_w / 3);

	

	Fl_Button* _cancel_button = new Fl_Button(_btn_posx, _helper.m_posy, _btn_w, UIUtils::BTN_LABEL_HEIGHT, "Cancel") ;

	_cancel_button->callback(static_cancel_button_cb, this);


	_helper.m_posy += UIUtils::BTN_LABEL_HEIGHT + UIUtils::DEFAULT_Y_PAD;


	_helper.m_total_height += _helper.m_posy;

	_helper.m_total_width += w;


	this->size(_helper.m_total_width, _helper.m_total_height);

	this->init_sizes();


	this->end();



	initialize_defaults();

}



NewImageDialog::NewImageInfo& NewImageDialog::GetNewImageInfo(NewImageDialog::NewImageInfo& info) const

{

	info.width = static_cast<int>(m_width->value());

	info.height = static_cast<int>(m_height->value());

	info.format = m_format;

	info.precision = m_precision;

	info.fill = m_fill;


	return(info);

}


void NewImageDialog::initialize_defaults()

{

	m_rgb->value(1);

	m_format = CinePaintTag::FORMAT_RGB_ENUM;


	m_prec_8bit_unsigned->value(1);

	m_precision = CinePaintTag::PRECISION_U8_ENUM;


	m_white->value(1);

	m_fill = FILL_WHITE_ENUM;

}


void NewImageDialog::create_button_cb()

{

	SetResponse(OK_ENUM);

	this->hide();

}


void NewImageDialog::cancel_button_cb()

{

	SetResponse(CANCEL_ENUM);

	this->hide();

}


void NewImageDialog::static_format_cb(Fl_Widget* w, void* dialog)

{

	NewImageDialog* _dialog = reinterpret_cast<NewImageDialog*>(dialog);


	if(w == _dialog->m_greyscale)

	{

		_dialog->m_format = CinePaintTag::FORMAT_GRAY_ENUM;

	}

	else if(w == _dialog->m_rgb)

	{

		_dialog->m_format = CinePaintTag::FORMAT_RGB_ENUM;

	}

}


void NewImageDialog::static_precision_cb(Fl_Widget* w, void* dialog)

{

	NewImageDialog* _dialog = reinterpret_cast<NewImageDialog*>(dialog);


	if(w == _dialog->m_prec_8bit_unsigned)

	{

		_dialog->m_precision = CinePaintTag::PRECISION_U8_ENUM;

	}

	else if(w == _dialog->m_prec_16bit_unsigned)

	{

		_dialog->m_precision = CinePaintTag::PRECISION_U16_ENUM;

	}

	else if(w == _dialog->m_prec_16bit_rnh)

	{

		_dialog->m_precision = CinePaintTag::PRECISION_FLOAT16_ENUM;

	}

	else if(w == _dialog->m_prec_32bit_float)

	{

		_dialog->m_precision = CinePaintTag::PRECISION_FLOAT_ENUM;

	}

	else if (w == _dialog->m_prec_16bit_fixed_point)

	{

		_dialog->m_precision = CinePaintTag::PRECISION_BFP_ENUM;

	}

}


void NewImageDialog::static_fill_cb(Fl_Widget* w, void* dialog)

{

	NewImageDialog* _dialog = reinterpret_cast<NewImageDialog*>(dialog);


	if(w == _dialog->m_background)

	{

		_dialog->m_fill = FILL_BACKGROUND_ENUM;

	}

	else if(w == _dialog->m_white)

	{

		_dialog->m_fill = FILL_WHITE_ENUM;

	}

	else if(w == _dialog->m_trans)

	{

		_dialog->m_fill = FILL_TRANS_ENUM;

	}

	else if(w == _dialog->m_foreground)

	{

		_dialog->m_fill = FILL_FOREGROUND_ENUM;

	}

}

