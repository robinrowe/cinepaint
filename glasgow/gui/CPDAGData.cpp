/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CPDAGData - Directed acyclic graph data unit
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Stuart Ford
 *   Colin Law
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * $Id: CPDAGData.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "CPDAGData.h"
#include "utility/CPSocket.h"
#include "plug-ins/pdb/PluginParam.h"
#include <string>

//-------------
// Constructors

/**
 * Constructor. Creates an object with no data.
 */
CPDAGData::CPDAGData()
{
	m_type = DAG_PLUGINPARAM;
	m_param = 0;
	m_deserialized = false;
	m_type = DAG_PLUGINPARAM;
}

/**
 * Constructor. Creates an object with the given integer data.
 *
 * @param data the integer to be stored in this node
 */
CPDAGData::CPDAGData(int data)
{
	m_type = DAG_PLUGINPARAM;
	CPPluginArg arg;
	arg.pdb_int = data;
	m_param = new PluginParam(PDB_INT, arg);
	m_param->SetManaged(true);
	m_deserialized = false;
}

/**
 * Constructor. Creates an object with the given double data.
 *
 * @param data the float to be stored in this node
 */
CPDAGData::CPDAGData(float data)
{
	m_type = DAG_PLUGINPARAM;
	CPPluginArg arg;
	arg.pdb_float = data;
	m_param = new PluginParam(PDB_FLOAT, arg);
	m_param->SetManaged(true);
	m_deserialized = false;
}

/**
 * Constructor. Creates an object with the given C-string data.
 * The type parameter is used to differentiate between a plugin name string
 * and a plugin parameter string value in orde to set the data type of this
 * CPDAGData correctly
 * In both cases, the string value is copied and managed by this CPDAGData
 *
 * @param data the C-string to be stored in this node
 * @param type the data type if this CPDAGNode
 */
CPDAGData::CPDAGData(const char* data, DataType type)
{
	m_type = type;
	m_deserialized = false;

	switch(type)
	{
			case DAG_PLUGINPARAM:
			{
				CPPluginArg arg;
				arg.pdb_cstring = new char[strlen(data) +1];
				strcpy(arg.pdb_cstring, data);
				m_param = new PluginParam(PDB_CSTRING,  arg);
				m_param->SetManaged(true);
			}
			case DAG_PLUGIN:
			{
				m_plugin = new char[strlen(data) +1];
				strcpy(m_plugin, data);
			}
	}

}

/**
 * Constructor. Creates an object with the given AbstractBuf data.
 * Initially, the AbstractBuf is not managed by PluginParam holding the data value
 *
 * @param data the AbstractBuf pointer to be stored in this node
 */
CPDAGData::CPDAGData(AbstractBuf* data)
{
	m_type = DAG_PLUGINPARAM;
	CPPluginArg arg;
	arg.pdb_abstractbuf = data;
	m_param = new PluginParam(PDB_ABSTRACTBUF, arg);
	m_param->SetManaged(false);
	m_deserialized = false;
}

/**
 * Constructor. Creates data object by deserializing from the given socket.
 *
 * @param socket the socket to read from
 */
CPDAGData::CPDAGData(CPSocket& socket)
{
	Deserialize(socket);
}

/**
 * Destructor. If a data object was created by deserialization, the data in
 * it will be destroyed when the object is destroyed.
 *
 */
CPDAGData::~CPDAGData()
{
	// @TODO [claw] need to check this makes sense
	//       does the node own the data? or does the CinePaintApp gain responsibility
	//       for deserialized data?
	if(m_type == DAG_PLUGINPARAM)
	{
		if(m_deserialized && m_param)
		{
			// deserialized, so we own the data
			m_param->SetManaged(true);

			delete m_param;
			m_param = 0;
		}
	}
	else
	{
		if(m_plugin)
		{
			delete[] m_plugin;
			m_plugin = 0;
		}
	}
}


//---------------------
// Accessors / Mutators

/**
 * Returns the data type held by this CPDAGData
 *
 * @return	the data type held by this CPDAGData
 */
CPDAGData::DataType CPDAGData::GetType() const
{
	return m_type;
}

/**
 * Returns the plugin name held by this CPDAGData
 * If GetType() is not DAG_PLUGIN this method rturns 0
 *
 * @return the name of the plugin held by this CPDAGData, or 0 if this CPDAGData
 *         does not hold a plugin name
 */
const char* CPDAGData::GetPluginName() const
{
	const char* ret = 0;

	if(m_type == DAG_PLUGIN)
	{
		ret = m_plugin;
	}

	return(ret);
}

/**
 * Returns the PluginParam holding the parameter value to a plugin.
 * if this CPDAGData does not hold a plugin parameter, ie if GetType() does not return 
 * DAG_PLUGINPARAM, this method returns 0
 *
 * @return the PluginParam holding a parameter to a plugin, or 0 if this CPDAGData does
 *         not hold a plugin parameter
 */
const PluginParam* CPDAGData::GetPluginParam() const
{
	const PluginParam* ret = 0;

	if(m_type == DAG_PLUGINPARAM)
	{
		ret = m_param;
	}

	return(ret);
}




/**
 * Serialize and write data to a given socket.
 * @param socket	[i] the socket to write the data to
 */
void CPDAGData::Serialize(CPSocket& socket) const
{
	// socket << m_type;
	//
	// if (m_type == DAG_PLUGIN)
	// {
	// socket << m_data.plugin;
	// }
	// else
	// {
	// socket << m_data.param->type;
	// switch (m_data.param->type)
	// {
	//
	// case PDB_INT :
	// socket << m_data.param->value.pdb_int;
	// break;
	//
	// case PDB_FLOAT :
	// socket << m_data.param->value.pdb_float;
	// break;
	//
	// case PDB_CSTRING :
	// socket << m_data.param->value.pdb_cstring;
	// break;
	//
	// case PDB_ABSTRACTBUF :
	// m_data.param->value.pdb_abstractbuf->Serialize(socket);
	// break;
	//
	// }
	// }
}

/**
 * Read and deserialize data from a given socket.
 * @param socket	[i] the socket to read the data from
 */
void CPDAGData::Deserialize(CPSocket& socket)
{
	// m_deserialized = true;
	//
	// socket >> m_type;
	//
	// if (m_type == DAG_PLUGIN)
	// {
	// socket >> m_data.plugin;
	// }
	// else
	// {
	// m_data.param = new CPPluginParamDef;
	// socket >> m_data.param->type;
	// switch (m_data.param->type)
	// {
	//
	// case PDB_INT :
	// socket >> m_data.param->value.pdb_int;
	// break;
	//
	// case PDB_FLOAT :
	// socket >> m_data.param->value.pdb_float;
	// break;
	//
	// case PDB_CSTRING :
	// socket >> m_data.param->value.pdb_cstring;
	// break;
	//
	// case PDB_ABSTRACTBUF :
	// m_data.param->value.pdb_abstractbuf->Deserialize(socket);
	// break;
	//
	// }
	// }
}


/**
 * Sets the data held within this CPDAGData to the specified int value
 * The data type of this CPDAGData is set to DAG_PLUGINPARAM
 *
 * @param data the int value held by this CPDAGData
 */
void CPDAGData::set(int data)
{
	set_type(CPDAGData::DAG_PLUGINPARAM);
	m_param->Set(data);
	m_param->SetManaged(true);
}


/**
 * Sets the data held within this CPDAGData to the specified float value
 * The data type of this CPDAGData is set to DAG_PLUGINPARAM
 *
 * @param data the float value held by this CPDAGData
 */
void CPDAGData::set(float data)
{
	set_type(CPDAGData::DAG_PLUGINPARAM);
	m_param->Set(data);
	m_param->SetManaged(true);
}

/**
 * Sets the data held within this CPDAGData to the specified string
 * The specified string is copied by this CPDAGData, and the holding
 * PluginParam is set to manage the copied string.
 * The data type of this CPDAGData is set to DAG_PLUGINPARAM
 *
 * @param data the string value held by this CPDAGData
 */
void CPDAGData::set(const char* data)
{
	set_type(CPDAGData::DAG_PLUGINPARAM);
	char* managed_buf = new char[strlen(data) +1];
	strcpy(managed_buf, data);

	m_param->Set(managed_buf);
	m_param->SetManaged(true);
}

/**
 * Sets the data held within this CPDAGData to the specified AbstractBuf
 * The PluginParam holding the actual data value is initally set to
 * NOT manage the specified AbstractBuf.
 * The data type of this CPDAGData is set to DAG_PLUGINPARAM
 *
 * @param data the AbstractBuf held by this CPDAGData
 */
void CPDAGData::set(AbstractBuf* data)
{
	set_type(CPDAGData::DAG_PLUGINPARAM);
	m_param->Set(data);
	m_param->SetManaged(false);
}

/**
 * Sets the plugin name held within this CPDAGData to the specified string
 * The char* is copied and managed by this CPDAGData
 * The data type of this CPDAGData is set to DAG_PLUGIN
 *
 * @param data the int value held by this CPDAGData
 */
void CPDAGData::set_plugin(const char* data)
{
	set_type(CPDAGData::DAG_PLUGIN);

	m_plugin = new char[strlen(data) +1];
	strcpy(m_plugin, data);
}

/**
 * Convenience method to set the data type helf by this CPDAGData
 *
 * @param type the data type held by this CPDAGData
 */
void CPDAGData::set_type(DataType type)
{
	if(type != m_type)
	{
		if(m_type == CPDAGData::DAG_PLUGINPARAM)
		{
			delete m_param;
			m_param = 0;
		}
		else
		{
			delete[] m_plugin;
			m_plugin = 0;
			m_param = 0;
		}

		m_type = type;
	}

	switch(type)
	{
			case CPDAGData::DAG_PLUGINPARAM:
			{
				if(!m_param)
				{
					m_param = new PluginParam();
				}
				break;
			}
			case CPDAGData::DAG_PLUGIN:
			{
				// dont knwo how big the name is, so cant allocate any space
				if(m_plugin)
				{
					delete[] m_plugin;
					m_plugin = 0;
				}
				break;
			}
	}
}

