/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CPDAGScheduler - Directed acyclic graph sequential node scheduler
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Stuart Ford
 *   Colin Law
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * $Id: CPDAGScheduler.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CPDAGSCHEDULER_H_
#define _CPDAGSCHEDULER_H_

#include "dll_api.h"
#include <map>
#include <string>
#include <vector>

class CPDAG;


/**
 *
 */
class CINEPAINT_CORE_API CPDAGScheduler
{
	public:

		// FUNCTIONS

		/**
		 * Default constructor.
		 */
		CPDAGScheduler();

		/**
		 * Copy constructor.
		 */
		//CPDAGScheduler(CPDAGScheduler& sched);

		/**
		 * Assignment operator.
		 * @param rhs the scheduler to copy to the current scheduler
		 * @return the current scheduler, now a copy of the given one
		 */
		CPDAGScheduler &operator=(const CPDAGScheduler& rhs);

		/**
		 * Constructor. Creates a scheduler for a given CPDAG.
		 * @param graph the CPDAG to schedule
		 */
		CPDAGScheduler(const CPDAG& graph);

		/**
		 * Schedule the given graph. Any previous scheduling is discarded.
		 * @param graph the CPDAG to schedule
		 */
		void Schedule(const CPDAG& graph);

		/**
		 * Get the next node to be evaluated. If a node is waiting to be evaluated
		 * it will be returned here. If no node is returned it does not necessarily
		 * mean that scheduling has finished (use #isEmpty to test this), only that
		 * no more nodes can be evaluated until other current evaluations have
		 * completed.
		 * @param node holder for the name of the node to be evaluated
		 * return true if a node was returned, else false
		 */
		bool Next(std::string& node);

		/**
		 * Indicates whether all the nodes of the CPDAG have been scheduled.
		 * @return true if all nodes have been scheduled, else false
		 */
		bool IsEmpty(void);

		/**
		 * Inform the scheduler that evaluation of a node has completed.
		 * @param node the name of the node that has finished being evaluated
		 */
		void Completed(const std::string& node);

		/**
		 * Get the number of incomplete jobs currently farmed out. When this number
		 * is zero and #IsEmpty returns true, the entire graph has been evaluated.
		 * @return the number of incomplete jobs
		 */
		int IncompleteJobs();


	private:

		// MEMBERS

		/**
		 * Pointer to the CPDAG being scheduled.
		 */
		const CPDAG* m_pGraph;

		/**
		 * A Q for storing nodes with an indegree of 0.
		 */
		std::vector<std::string> m_indegreeZeroQ;

		/**
		 * A store of the indegrees of the nodes of the CPDAG being scheduled.
		 */
		std::map<const std::string, int> m_indegrees;

		/**
		 * The number of nodes currently being evaluated. this is incremented by
		 * #Next and decremented by #Completed.
		 */
		int m_incomplete;


		// FUNCTIONS

		/**
		 * Initialises the store of node indegrees from the CPDAG, and pushes nodes
		 * with indegree zero onto the Q.
		 */
		void init_indegrees();

		CPDAGScheduler& copy(const CPDAGScheduler &it, bool isConstructor);

};

#endif /* _CPDAGSCHEDULER_H_ */
