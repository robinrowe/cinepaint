/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      SheetSelectionTools - Spreadsheet selection handling
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: SheetSelectionTools.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _SHEET_SELECTION_TOOLS_H_
#define _SHEET_SELECTION_TOOLS_H_

#include "dll_api.h"
#include "plug-ins/pdb/CinePaintImage.h"
#include <sys/types.h>
#include <sys/stat.h>

#include <string>

class SheetView;
class CPSpreadsheet;
class FrameManager;

/**
 * SheetSelectionTools provides the handling methods for processing a selection upon the CinePaint spreadsheet
 * SheetSelectionTools exists to separate the processing of selection made upon the spreadsheet from the
 * spreadsheet itself.
 *
 * @TODO extend this class foe oyher selection operation
 */
class CINEPAINT_GUI_API SheetSelectionTools
{
	public:
		/**
		 * Constructs a new SheetSelectionTools for handling selection made upon the CinePaint spreadsheet
		 *
		 * @param view the view of the spreadsheet
		 * @param sheet spreadsheet interface into the spreadsheet data
		 */
		SheetSelectionTools(SheetView& view, CPSpreadsheet& sheet);

		/**
		 * Destructor
		 */
		virtual ~SheetSelectionTools();


		//-----------------------
		// Frame Manager handling

		/**
		 * Adds the current selection to a CinePaint FrameManger
		 * If multiple FrameManagers exist the user is querid to which FrameManaget the items
		 * should be added
		 * 
		 */
		void AddToFrameManager();


		//void RemoveFromFrameManager();

	protected:
	private:
		/**
		 * Attempts to add the specified cell to the named FrameManager
		 * Cells that contain either a CinePaintImage, or a string that may be the filename
		 * of an image are addedd, all other data is not.
 		 *
		 * @param frame_manager the id of the FrameManaget to add to
		 * @param row the row of tjhe cell to add
		 * @param col the column of the cell to add
		 * @param duration the duration of the new frame within the FrameManager
		 */
		void add_cell_to_frame_manager(const std::string& frame_manager, int row, int col, long duration);
		
		void add_image_to_frame_manager(FrameManager& fm, CinePaintImage* img, long duration);
		void add_filename_to_frame_manager(FrameManager& fm, const char* filename, long duration);

		/**
		 * Gets the id of the FrameManager to to which cell data is to be added
		 * If there is only one FrameManager, the Id of that FrameManager is returned.
		 * If there are multiple FrameManagers, the users is queried for the Id of the FrameManager
		 * If the user cancels, or there are no FrameManagers, an empty string is retuyrned
		 *
		 * @return the id of the FrameManager to add/remove items from
		 */
		std::string get_frame_manager_id() const;

		/**
		 * Determins if the specified filename exists and is a regular file
		 *
		 * @return true if the specified file exists and is a regular file, false otherwise
		 * @todo this needs a common home
		 */
		bool is_filename(const char* filename) const;

		//--------
		// Members

		/** view of the spreadsheet */
		SheetView& m_view;

		/** Spreadsheet interaface into the data graph backing the spreadsheet */
		CPSpreadsheet& m_sheet;

		/** default duration per frame when adding to a FrameManager */
		static const long DEFAULT_FRAME_MANAGER_DURATION;
};

#endif /* _SHEET_SELECTION_TOOLS_H_ */
