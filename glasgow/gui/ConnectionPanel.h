/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ConnectionPanel - Spreadsheet connection dialog
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ConnectionPanel.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CONNECTION_PANEL_H_
#define _CONNECTION_PANEL_H_

#include "dll_api.h"
#include "SheetChangeListener.h"
#include <FL/Fl_Window.H>
#include <list>
#include <string>

class Fl_Check_Button;
class Fl_Hold_Browser;
class Fl_Input;

class CPDAG;
class SheetView;

/**
 * ConnectionPanel provides a small dislaog for displaying connection between nodes in a CPDAG
 *
 */
class CINEPAINT_GUI_API ConnectionPanel : public Fl_Window
{
	public:
		/**
		 * Constructs a new ConnectionPanel
		 *
		 * @param w the dialog width
		 * @param h the dialog height
		 * @param sheet_view the SheetView used to listen for cell cursor changes
		 * @param title the dislaog title
		 */
		ConnectionPanel(int w, int h, SheetView& sheet_view, const char* title = 0);

		/**
		 * Destructor
		 */
		virtual ~ConnectionPanel();



		/**
		 * Sets the CPDAG used connections are displayed for
		 *
		 * @param dag the CPDAG this dialog displays node connections for
		 */
		void SetDAG(CPDAG& dag);

		/**
		 * Returns the CPDAG this dialog is displaying node connections for
		 *
		 * @return the CPDAG for which node connections are displayed
		 */
		const CPDAG* GetDAG();

		/**
		 * Sets the current cell for which node connections are displayed
		 * The specified cell is looked up within the current CPDAG and the inward and
		 * outward connections of the node are displayed upon the dialog
		 *
		 * @param cell the current cell for which to display node connections
		 */
		void SetCell(const std::string& cell);

	protected:

	private:

		//--------
		// Members

		/** the SheetView used to monitor for currently selected cell changes */
		SheetView& m_sheet_view;

		/** the CPDAG from which this dialog displays node connections */
		CPDAG* m_dag;

		//--------
		// Widgets
		Fl_Input* m_selected_cell_input;
		Fl_Hold_Browser* m_inward_conns;
		Fl_Hold_Browser* m_outward_conns;
		Fl_Check_Button* m_auto_change_ck_btn;



		//-----------------
		// Widget Callbacks
		static void static_change_cell_cb(Fl_Widget* w, void* d) { static_cast<ConnectionPanel*>(d)->handle_cell_changed(); }
		void change_cell_cb();

		static void static_auto_change_cell_cb(Fl_Widget* w, void* d) { static_cast<ConnectionPanel*>(d)->auto_change_cell_cb(); }
		void auto_change_cell_cb();

		static void static_disconnect_inward_conn_cb(Fl_Widget* w, void* d) { static_cast<ConnectionPanel*>(d)->disconnect_inward_conn_cb(); }
		void disconnect_inward_conn_cb();

		static void static_disconnect_outward_conn_cb(Fl_Widget* w, void* d) { static_cast<ConnectionPanel*>(d)->disconnect_outward_conn_cb(); }
		void disconnect_outward_conn_cb();

		void handle_cell_changed();


		// Nested Classes

		/**
		 * SheetChangeListener to automatically change the currently displayed node
		 * The set node is updated upon changes to the currently selected cell upon the SheetView
		 */
		class CellChangeListener : public SheetChangeListener
		{
			public:
				CellChangeListener(ConnectionPanel& panel);

				virtual void CursorCellChanged(int row, int col);
				virtual void RangeChanged(int row_start, int col_start, int row_end, int col_end);
				virtual void ValueChanged(int row, int col, const std::string& val);

			protected:
			private:
				ConnectionPanel& m_panel;
		};

		CellChangeListener* m_sheet_change_listener;


		/**
		 * Structure to store details of cell connections
		 *
		 */
		struct ConnRec
		{
			ConnRec(std::string cell, std::string param, std::string eval) : m_cell(cell), m_param(param), m_eval(eval) {};
			std::string m_cell;
			std::string m_param;
			std::string m_eval;
		};

		typedef std::list<ConnRec*> ConnRecContainer_t;
		ConnRecContainer_t m_in_conn_recs;
		ConnRecContainer_t m_out_conn_recs;

		/**
		 * Clear the list of incoming connection records
		 *
		 */
		void clear_in_conn_recs();

		/**
		 * Clear the list of out going connection records
		 *
		 */
		void clear_out_conn_recs();
};

#endif /* _CONNECTION_PANEL_H_ */
