/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      SheetView - Spreadsheet for images ui widget
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: SheetMouseListener.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "SheetMouseListener.h"
#include <gui/CinePaintAppUI.h>
#include "CPSpreadsheet.h"
#include "SheetView.h"
#include "CPDAGData.h"
#include "CPDAGNode.h"
#include <plug-ins/pdb/CinePaintImage.h>
#include <plug-ins/pdb/Drawable.h>
#include <plug-ins/pdb/PluginParam.h>
#include <utility/PlatformUtil.h> // req. for snprintf
#include <string>


SheetMouseListener::SheetMouseListener(SheetView& view, CPSpreadsheet& spreadsheet)
: m_view(view), m_spreadsheet(spreadsheet)
{}

SheetMouseListener::~SheetMouseListener()
{}

void SheetMouseListener::MouseEntered()
{}

void SheetMouseListener::MouseExited()
{}

void SheetMouseListener::MousePressed(int button, int count, int x, int y, unsigned int modifier)
{
	// If its just a single click
	if(count == 0)
	{
		if(m_view.IsInputEntryVisible())
		{
			m_view.ProcessInputEntryText();
		}
		m_view.SetInputEntryVisibile(false, false);
	}
	else
	{
		// 'double' click
		
		int row = m_view.get_current_row();
		int col = m_view.get_current_col();

		m_view.set_selection(row, col, row, col);

		const CPDAGNode* node = m_spreadsheet.GetCellNode(row, col);

		if(node)
		{
			switch(node->GetData().GetType())
			{
				case CPDAGData::DAG_PLUGIN:
				{
					if(node->IsDirty())
					{
						std::string s = m_spreadsheet.RebuildPluginString(row, col);
						m_view.SetInputEntryText(s);
						m_view.SetInputEntryVisibile(true);
					}
					else
					{
						const PluginParam* eval = node->GetEvaluation();
						if(eval)
						{
							switch(eval->GetType())
							{
								case PDB_DRAWABLE:
								{
									Drawable* drawable = eval->GetArg().pdb_drawable;
									if(drawable)
									{
										// @TODO Do something useful to display the Drawable
									}
									break;
								}
								case PDB_CPIMAGE:
								{
									CinePaintImage* img = eval->GetArg().pdb_cpimage;
									if(img)
									{
										CinePaintAppUI::GetInstance().GetDocViewManager().OpenView(*img);
									}
									break;
								}
								default:
								{
									break;
								}
							}
						}
					}
					break;
				}
				case CPDAGData::DAG_PLUGINPARAM:
				{
					const PluginParam* param = node->GetData().GetPluginParam();
					switch(param->GetType())
					{
						case PDB_INT:
						{
							char buf[64];
							snprintf(buf, 64, "%d", param->GetArg().pdb_int);
							m_view.SetInputEntryText(buf);
							m_view.SetInputEntryVisibile(true);
							break;
						}
						case PDB_FLOAT:
						{
							char buf[64];
							snprintf(buf, 64, "%f", param->GetArg().pdb_float);
							m_view.SetInputEntryText(buf);
							m_view.SetInputEntryVisibile(true);
							break;
						}
						case PDB_CSTRING:
						{
							const char* buf = param->GetArg().pdb_cstring; 
							if(buf)
							{
								m_view.SetInputEntryText(buf);
								m_view.SetInputEntryVisibile(true);
							}
							break;
						}
						case PDB_VOIDPOINTER:
						case PDB_ABSTRACTBUF:
						case PDB_DRAWABLE:
						case PDB_CPIMAGE:
						default:
						{
							m_view.SetInputEntryText("");
							break;
						}
					}

					break;
				}
			}
		}
		else
		{
			// No cell data, just show the input entry
			m_view.SetInputEntryText("");
			m_view.SetInputEntryVisibile(true);
		}
	}
}

void SheetMouseListener::MouseReleased(int button, int x, int y)
{}

void SheetMouseListener::MouseDragged(int button, int x, int y)
{}

void SheetMouseListener::MouseMoved(int x, int y)
{}
