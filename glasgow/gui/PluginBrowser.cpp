/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      BrushSelectionDialog - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: PluginBrowser.cpp,v 1.2 2006/12/21 11:18:07 robinrowe Exp $
 */


#include <gui/PluginBrowser.h>

#include <app/CinePaintApp.h>
#include <plug-ins/pdb/CinePaintPlugin.h>
#include <plug-ins/pdb/PDB.h>
#include <app/PluginUtils.h>
#include "UIUtils.h"
#include <utility/PlatformUtil.h>

#include <FL/Fl_Box.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Hold_Browser.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Text_Buffer.H>
#include <FL/Fl_Text_Display.H>

#include <cctype>
#include <string>
#include <algorithm>

static const int LABEL_TEXT_SIZE = 64;

// Define the font styles used within the text buffer
static Fl_Text_Display::Style_Table_Entry STYLE_TABLE[] = {
            { FL_BLACK,   FL_HELVETICA,        10, 0 },  // A - Plain
            { FL_BLACK,   FL_HELVETICA_BOLD,   12, 0 },  // B - Section
            { FL_BLACK,   FL_HELVETICA_ITALIC, 10, 0 }   // C - Sub-Section
        };

/**
 * Constructs a new PluginBrowser
 *
 * @param w the width of the dialog
 * @param h the geight of the dialog
 * @param title dialog title
 */
PluginBrowser::PluginBrowser(int x, int y, int w, int h, const char* title,World* world)
		: Fl_Group(x, y, w, h, title)
{	this->world=world;
	m_sort_type = NAME_ASCN_ENUM;
	m_search_string = 0;

	int _y = UIUtils::DEFAULT_Y_PAD;
	int _x = UIUtils::DEFAULT_X_PAD;
	int _panel_w = (w - (3 * UIUtils::DEFAULT_X_PAD)) / 2;

	m_plugin_count_text = new char[LABEL_TEXT_SIZE];
	memset(m_plugin_count_text, '\0', LABEL_TEXT_SIZE);
	m_plugin_count_label = UIUtils::CreateLabel(_x, _y, _panel_w, UIUtils::DEFAUTL_ROW_HEIGHT, m_plugin_count_text);
	_y += UIUtils::DEFAULT_Y_PAD + UIUtils::DEFAUTL_ROW_HEIGHT;

	int _browser_height = h - (_y + ((2 * UIUtils::DEFAUTL_ROW_HEIGHT) + (3 * UIUtils::DEFAULT_Y_PAD)));
	m_flat_browser = new Fl_Hold_Browser(_x, _y, _panel_w, _browser_height);
	m_flat_browser->callback(static_flat_browser_cb, this);
	_y += UIUtils::DEFAULT_Y_PAD + _browser_height;

	int _label_offset = _panel_w / 3;
	m_sort_choice = new Fl_Choice(_x + _label_offset, _y, _panel_w - _label_offset, UIUtils::DEFAUTL_ROW_HEIGHT, "Sort:");
	build_sort_order_menu(*m_sort_choice);
	m_sort_choice->callback(static_sort_order_cb, this);
	_y += UIUtils::DEFAULT_Y_PAD + UIUtils::DEFAUTL_ROW_HEIGHT;

	m_search_input = new Fl_Input(_x + _label_offset, _y, _panel_w - _label_offset, UIUtils::DEFAUTL_ROW_HEIGHT, "Search:");
	m_search_input->callback(static_search_cb, this);
	m_search_input->when(FL_WHEN_ENTER_KEY|FL_WHEN_NOT_CHANGED);

	m_text_display = new Fl_Text_Display(_panel_w + (2 * UIUtils::DEFAULT_X_PAD), UIUtils::DEFAULT_Y_PAD, _panel_w, h - (2 * UIUtils::DEFAULT_Y_PAD));
	m_text_buffer = new Fl_Text_Buffer();
	m_style_buffer = new Fl_Text_Buffer();
	m_text_display->buffer(m_text_buffer);
	m_text_display->highlight_data(m_style_buffer, STYLE_TABLE, 3, 'A', 0, 0);

	this->end();



	refresh_plugin_data(world);
	populate_browser();
}

/**
 * Destructor
 */
PluginBrowser::~PluginBrowser()
{
	clear_plugin_list();

	if(m_plugin_count_text)
	{
		delete[] m_plugin_count_text;
		m_plugin_count_text = 0;
	}

	if(m_search_string)
	{
		delete[] m_search_string;
		m_search_string = 0;
	}
}


/**
 * Adds the specified PluginSelectionChangeListener to receive notification upon plugin selection changes
 * if the specified PluginSelectionChangeListener has already been added it is not added again.
 * The resources of the specified PluginSelectionChangeListener are not managed by this class
 *
 * @param pscl the PluginSelectionChangeListener to be added
 */
void PluginBrowser::AddSelectionChangeListener(PluginSelectionChangeListener* pscl)
{
	m_listener_list.AddListener(pscl);
}

/**
 * Removes the specified PluginSelectionChangeListener
 * The removed PluginSelectionChangeListener will no longer receive plugin selection change
 * notifications from this class
 *
 * @param pscl the PluginSelectionChangeListener to be removed
 */
void PluginBrowser::RemoveSelectionChangeListener(PluginSelectionChangeListener* pscl)
{
	m_listener_list.RemoveListener(pscl);
}



/**
 * Refreshes the list of Plugin Records from the Plugin database
 * After a refresh, populate_browser should be called to update the current display
 *
 */
void PluginBrowser::refresh_plugin_data(World* world)
{	// clear any existing data
	clear_plugin_list();
	PDB* pdb=PDB::GetPDB(world);
	if(!pdb)
	{	return;
	}
	for(PDB::pdb_const_iterator _citer = pdb->PluginsBegin(); _citer != pdb->PluginsEnd(); ++_citer)
	{	CinePaintPluginInfo* _info = (*_citer).second;
		PluginRec* _rec = new PluginRec;
		_rec->m_type = PluginRec::STD_PLUGIN_ENUM;
		_rec->m_plugin_name = _info->GetName();
		std::string _name = std::string(_info->GetMenuPath());
		_rec->m_name = PluginUtils::StripMenuPath(_name);
		std::string _menu_path = std::string(_info->GetMenuPath());
		_rec->m_menu_path = PluginUtils::StripWindowName(_menu_path);
		m_plugin_data.push_back(_rec);
	}
	for(PDB::fh_const_iterator _citer = pdb->LoadHandlersBegin(); _citer != pdb->LoadHandlersEnd(); ++_citer)
	{	CinePaintFileHandlerInfo* _info = (*_citer).second;
		PluginRec* _rec = new PluginRec;
		_rec->m_type = PluginRec::LOAD_HANDLER_ENUM;
		_rec->m_plugin_name = _info->GetName();
		_rec->m_name = _info->GetName();
		_rec->m_menu_path = "load_handler";
		m_plugin_data.push_back(_rec);
	}
	for(PDB::fh_const_iterator _citer = pdb->SaveHandlersBegin(); _citer != pdb->SaveHandlersEnd(); ++_citer)
	{	CinePaintFileHandlerInfo* _info = (*_citer).second;
		PluginRec* _rec = new PluginRec;
		_rec->m_type = PluginRec::SAVE_HANDLER_ENUM;
		_rec->m_plugin_name = _info->GetName();
		_rec->m_name = _info->GetName();
		_rec->m_menu_path = "save_handler";
		m_plugin_data.push_back(_rec);
	}
}


/**
 * Popultes the plugin bowser list from the system plugin database.
 * This method retrieves an iterator from the plugin database and and adds
 * each plugin into the browser list, taking into account the current sort
 * and search data
 *
 */
void PluginBrowser::populate_browser()
{
	m_flat_browser->clear();

	sort_plugin_list(m_plugin_data, m_sort_type);


	// search string
	std::string _ss(m_search_string != 0 ? m_search_string : "");
	std::transform(_ss.begin(), _ss.end(), _ss.begin(), (int(*)(int))std::toupper);

	for(PluginList::const_iterator _citer = m_plugin_data.begin(); _citer != m_plugin_data.end(); ++_citer)
	{
		PluginRec* _rec = (*_citer);

		// filter on any specified search string - compare uppercase
		std::string _s(_rec->m_name);
		std::transform(_s.begin(), _s.end(), _s.begin(), (int(*)(int))std::toupper);

		if(_s.find(_ss) != std::string::npos)
		{
			m_flat_browser->add(_rec->m_name.c_str(), _rec);
		}
	}

	update_plugin_count(m_flat_browser->size());
}

/**
 * Updates the plugin count text display to the specified count
 *
 * @param count indicates the number of loaded plugins
 */
void PluginBrowser::update_plugin_count(int count)
{
	snprintf(m_plugin_count_text, LABEL_TEXT_SIZE -1, "%d %s Available", count, (count == 1) ? "Plugin" : "Plugins");
	m_plugin_count_label->label(m_plugin_count_text);
}


/**
 * Sorts the the specified plugin list according to the SortType
 *
 * @param list the list to sort
 * @param sort_type type of ordering required
 * @return the plugin list sorted
 */
PluginBrowser::PluginList& PluginBrowser::sort_plugin_list(PluginBrowser::PluginList& plugin_list, SortType sort_type)
{
	switch(sort_type)
	{
		case NAME_DESC_ENUM:
		{
			PluginRecComparator _comparator(PluginRecComparator::NAME_ENUM);
			plugin_list.sort(_comparator);
			plugin_list.reverse();
			break;
		}
		case TYPE_ASCN_ENUM:
		{
			PluginRecComparator _comparator(PluginRecComparator::TYPE_ENUM);
			plugin_list.sort(_comparator);
			break;
		}
		case TYPE_DESC_ENUM:
		{
			PluginRecComparator _comparator(PluginRecComparator::TYPE_ENUM);
			plugin_list.sort(_comparator);
			plugin_list.reverse();
			break;
		}
		case NAME_ASCN_ENUM:
		default:
		{
			PluginRecComparator _comparator(PluginRecComparator::NAME_ENUM);
			plugin_list.sort(_comparator);
			break;
		}
	}

	return(plugin_list);
}

/**
 * Updates the plugin info text buffer with details for the currently selected plugin
 * Currently displayed are the unique plugin name. plugin menu path, author, year, and copryright.
 *
 * @param rec the PluginRec struct detailing the plugin to display
 * @param buffer the buffer in which to append the heading
 * @param style the buffer containg the style data
 */
void PluginBrowser::update_plugin_info(PluginRec& rec, Fl_Text_Buffer& buffer, Fl_Text_Buffer& style,World* world)
{	buffer.remove(0, m_text_buffer->length());
	style.remove(0, m_style_buffer->length());
	// we need an instance of the plugin to get its complete data
	PDB* pdb=PDB::GetPDB(world);
	if(!pdb)
	{	return;
	}	
	CinePaintPlugin* _plugin = 0;
	switch(rec.m_type)
	{	case PluginRec::STD_PLUGIN_ENUM:
		{	_plugin = pdb->CreateInstance(rec.m_plugin_name.c_str());
		}
		case PluginRec::LOAD_HANDLER_ENUM:
		{	CinePaintFileHandlerInfo* _info = pdb->LookupLoadHandler(rec.m_plugin_name.c_str());
			if(_info)
			{	_plugin = _info->GetCtor()();
			}
		}
		case PluginRec::SAVE_HANDLER_ENUM:
		{	CinePaintFileHandlerInfo* _info = pdb->LookupSaveHandler(rec.m_plugin_name.c_str());
			if(_info)
			{	_plugin = _info->GetCtor()();
			}
		}
	}
	int _end = 0;
	_end = add_plugin_desc_section(buffer, style, "Plugin Information:", _end);
	_end = add_plugin_desc_detail(buffer, style, 0, rec.m_name.c_str(), _end);
	if(rec.m_type == PluginRec::STD_PLUGIN_ENUM)
	{	_end = add_plugin_desc_detail(buffer, style, 0, rec.m_menu_path.c_str(), _end);
	}
	// the following come from the plugin itself, and not the PluginRec struct
	if(_plugin)
	{	_end = add_plugin_desc_detail(buffer, style, 0, _plugin->GetBlurb(), _end);
		// blank line for divider
		m_text_buffer->append("\n");
		// Plugina parameters etc
		_end = add_plugin_desc_section(buffer, style, "Plugin Paramaters:", _end);
		_end = add_plugin_desc_detail(buffer, style, "Plugin Inputs", 0, _end);
		CinePaintPlugin::CPPluginParamDefs _defs;
		_plugin->GetInParamDefs(_defs);
		for(int i = 0; i < _defs.m_param_count; i++)
		{	_end = add_plugin_param(buffer, style, _defs.m_params[i], _end);
		}
		_end = add_plugin_desc_detail(buffer, style, "Plugin Returns", 0, _end);
		_plugin->GetReturnParamDefs(_defs);
		for(int i = 0; i < _defs.m_param_count; i++)
		{	_end = add_plugin_param(buffer, style, _defs.m_params[i], _end);
		}
		m_text_buffer->append("\n");
		// Credits Section
		_end = add_plugin_desc_section(buffer, style, "Credits:", _end);
		_end = add_plugin_desc_detail(buffer, style, "Author:", _plugin->GetAuthor(), _end);
		_end = add_plugin_desc_detail(buffer, style, "Date:", _plugin->GetDate(), _end);
		_end = add_plugin_desc_detail(buffer, style, "Copyright:", _plugin->GetCopyright(), _end);
		pdb->ReleaseInstance(_plugin);
	}
	else
	{	_end = add_plugin_desc_section(buffer, style, "Plugin Not Available!", _end);
	}
}


/**
 * Convenience method to add a section heading to the plugins display text buffer
 *
 * @param buffer the buffer in which to append the heading
 * @param style the buffer containg the style data
 * @param heading the heading to append
 * @param start the current end point of the buffer
 * @return the new end point of the buffer
 */
int PluginBrowser::add_plugin_desc_section(Fl_Text_Buffer& buffer, Fl_Text_Buffer& style, const char* heading, int start)
{
	buffer.append(heading);
	buffer.append("\n");
	set_style(style, 'B', start, buffer.length());

	return(buffer.length());
}

/**
 * Convenience method to add a data section to the plugins display text buffer
 *
 * @param buffer the buffer in which to append the heading
 * @param style the buffer containg the style data
 * @param sub_heading a possible sub-heading to append, may also be 0
 * @param text the plugin detail to append
 * @param start the current end point of the buffer
 * @return the new end point of the buffer
 */
int PluginBrowser::add_plugin_desc_detail(Fl_Text_Buffer& buffer, Fl_Text_Buffer& style, const char* sub_heading, const char* text, int start)
{
	if(sub_heading)
	{
		buffer.append(sub_heading);
		buffer.append("\n");
		set_style(style, 'C', start, buffer.length());
	}

	int _tmp_end = buffer.length();

	if(text)
	{
		buffer.append("\t");
		buffer.append(text);
		buffer.append("\n");
		set_style(style, 'A', _tmp_end, buffer.length());
	}

	return(buffer.length());
}

/**
 * Convenience method to add a plugin parameter definition to the text buffer
 *
 * @param buffer the buffer in which to append the heading
 * @param style the buffer containg the style data
 * @param param the plugin parameter definition to add
 * @param start the current end point of the buffer
 * @return the new end point of the buffer
 */
int PluginBrowser::add_plugin_param(Fl_Text_Buffer& buffer, Fl_Text_Buffer& style, const CinePaintPlugin::CPPluginParamDef& param, int start)
{
	buffer.append("\t");
	buffer.append(param.m_name);
	buffer.append(", ");

	switch(param.m_type)
	{
		case PDB_INT:
		{
			buffer.append("int");
			break;
		}
		case PDB_FLOAT:
		{
			buffer.append("float");
			break;
		}
		case PDB_CSTRING:
		{
			buffer.append("String");
			break;
		}
		case PDB_VOIDPOINTER:
		{
			buffer.append("Data");
			break;
		}
		case PDB_ABSTRACTBUF:
		{
			buffer.append("AbstractBuf");
			break;
		}
		case PDB_DRAWABLE:
		{
			buffer.append("Drawable");
			break;
		}
		case PDB_CPIMAGE:
		{
			buffer.append("CinePaintImage");
			break;
		}
		default:
		{
			buffer.append("Unknown Type");
			break;
		}
	}

	buffer.append(", ");
	buffer.append(param.m_description);

	if(param.m_modifies)
	{
		buffer.append(", Modifies Parameter");
	}
	buffer.append("\n");

	set_style(style, 'A', start, buffer.length());

	return(buffer.length());
}

/**
 * Sets the style buffer with the specified style filling between start and end
 *
 * @param buf the style buffer to update
 * @param style the style character from the style table (implementation defined)
 * @param start the start point withi the buffer
 * @param end the end point within the buffer
 */
void PluginBrowser::set_style(Fl_Text_Buffer& buf, char style, int start, int end)
{
	char* _tmp = new char[end - start +1];
	memset(_tmp, style, end - start);
	_tmp[end - start] = '\0';
	buf.append(_tmp);
	delete[] _tmp;
}




/**
 * UI method to build the sort order drop down list
 *
 * @param menu the drop down list to populate
 */
void PluginBrowser::build_sort_order_menu(Fl_Choice& menu)
{
	menu.add("By Name Ascending", "", static_sort_order_cb, this);
	menu.add("By Name Descending", "", static_sort_order_cb, this);
	menu.add("By Type Ascending", "", static_sort_order_cb, this);
	menu.add("By Type Descending", "", static_sort_order_cb, this);

	m_sort_choice->value(0);
}


/**
 * Clears the current plugin record list
 *
 */
void PluginBrowser::clear_plugin_list()
{
	PluginList::iterator _iter = m_plugin_data.begin();

	while(_iter != m_plugin_data.end())
	{
		PluginRec* _rec = *_iter;
		delete _rec;
		_iter = m_plugin_data.erase(_iter);
	}
}








//-----------------
// Widget Callbacks


void PluginBrowser::flat_browser_cb()
{
	int _selected = m_flat_browser->value();
	if(_selected)
	{
		PluginRec* _rec = static_cast<PluginRec*>(m_flat_browser->data(_selected));
		update_plugin_info(*_rec, *m_text_buffer, *m_style_buffer,world);

		// update all regostere change listeners
		for(ListenerList_t::const_iterator citer = m_listener_list.begin(); citer != m_listener_list.end(); ++citer)
		{
			(*citer)->SelectionChanged(_rec->m_plugin_name.c_str());
		}
	}
}

void PluginBrowser::search_cb()
{
	if(m_search_string)
	{
		delete[] m_search_string;
		m_search_string = 0;
	}
	const char* _txt = m_search_input->value();

	if(_txt)
	{
		m_search_string = new char[strlen(_txt) +1];
		strcpy(m_search_string, _txt);
	}

	populate_browser();
}

void PluginBrowser::sort_order_cb()
{
	int _choice = m_sort_choice->value();

	switch(_choice)
	{
		case NAME_ASCN_ENUM:
		case NAME_DESC_ENUM:
		case TYPE_ASCN_ENUM:
		case TYPE_DESC_ENUM:
		{
			m_sort_type = static_cast<SortType>(_choice) ;
			break;
		}
		default:
		{
			m_sort_type = NAME_ASCN_ENUM;
		}
	}

	populate_browser();
}






//---------------
// Netsed Classes

/**
 * Nested function object used to sort the plugin list depending on the selected value type
 */
PluginBrowser::PluginRecComparator::PluginRecComparator(SortCategory sort_category)
{
	m_sort_category = sort_category;
}

PluginBrowser::PluginRecComparator::result_type PluginBrowser::PluginRecComparator::operator() (first_argument_type p, second_argument_type q) const
{
	result_type _ret = false;

	switch(m_sort_category)
	{
		case TYPE_ENUM:
		{
			_ret = (p->m_type < q->m_type);
			break;
		}
		case PATH_ENUM:
		{
			_ret = (p->m_menu_path < q->m_menu_path);
		}
		case NAME_ENUM:
		default:
		{
			_ret = (p->m_name < q->m_name);
			break;
		}
	}

	return(_ret);
}
