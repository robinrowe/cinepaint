/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      BrushSelectionDialog - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: BrushSelectionDialog.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "BrushSelectionDialog.h"
#include "app/CinePaintApp.h"
#include "plug-ins/pdb/ImageOpManager.h"
#include "CinePaintAppUI.h"
#include "UIUtils.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "app/BufferRenderer.h"
#include "app/BufferRendererFactory.h"
#include "plug-ins/pdb/FlatBuf.h"
#include "plug-ins/pdb/PixelArea.h"
#include "plug-ins/pdb/CinePaintScaleOp.h"

#ifndef WIN32
#define NOMINMAX
#endif

#include <FL/Fl_Box.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_Scroll.H>
#include <FL/Fl_Value_Slider.H>

#include <stdio.h>
#include <string.h>

// initialize static members
const char* BrushSelectionDialog::DEFAULT_DIALOG_TITLE = "Brush Selection" ;
const unsigned int BrushSelectionDialog::DEFAULT_CELL_WIDTH = 24 ;
const unsigned int BrushSelectionDialog::DEFAULT_CELL_HEIGHT = 24;
const unsigned int BrushSelectionDialog::DEFAULT_BRUSH_COLUMNS = 5 ;
const unsigned int BrushSelectionDialog::DEFAULT_BRUSH_ROWS = 5 ;

const unsigned int BrushSelectionDialog::DEFAULT_ROW_HEIGHT = 30;
const unsigned int BrushSelectionDialog::DEFAULT_LABEL_WIDTH = 100;

const unsigned int BrushSelectionDialog::DEFAULT_MARGIN_WIDTH = 3;
const unsigned int BrushSelectionDialog::DEFAULT_MARGIN_HEIGHT = 3;

const unsigned int BrushSelectionDialog::DEFAULT_BRUSH_DEPTH = 3;


// Used to add a small scaled instance indicator (a small +) to a brush button
static unsigned int SCALE_INDICATOR_SIZE = 7;
static unsigned char SCALE_INDICATOR_BITS[7][7] = 
{
  { 255, 255, 255, 255, 255, 255, 255 },
  { 255, 255, 255,   0, 255, 255, 255 },
  { 255, 255, 255,   0, 255, 255, 255 },
  { 255,   0,   0,   0,   0,   0, 255 },
  { 255, 255, 255,   0, 255, 255, 255 },
  { 255, 255, 255,   0, 255, 255, 255 },
  { 255, 255, 255, 255, 255, 255, 255 }
};

/**
 * Constructs a new BrushSelectionPanel
 *
 * @param title dialog title
 * @param init_name the intial brush list name
 * @param init_opacity
 * @param init_spacing
 * @param init_noise_freq
 * @param init_noise_step_start
 * @param init_noise_step_width
 * @param init_mode
 */
BrushSelectionDialog::BrushSelectionDialog(const char* title,
										 const char* init_name,
										 double init_opacity,
										 double init_spacing,
										 double init_noise_freq,
										 double init_noise_step_start,
										 double init_noise_step_width,
										 int init_mode)
										 : Fl_Window(100, 100, 600, 400, BrushSelectionDialog::DEFAULT_DIALOG_TITLE)
{
	m_brush_cell_width = BrushSelectionDialog::DEFAULT_CELL_WIDTH;
	m_brush_cell_height = BrushSelectionDialog::DEFAULT_CELL_HEIGHT;

	m_active_brush = 0 ;

	// if we are given a title, use it instead of the default
	if(title)
	{
		this->label(title) ;
	}

	// Because everything is absolutely positioned, we setup some postion variables to track where
	// widgets are added, hopefully this will allow us to alter sizes etc. slightly easier

	// we suggest width and height values and pass them by reference to the panel contruction methods
	// this allows the panel construction method to return adjusted values if required

	// @note [claw] we add 16 to the value to take into account the scrollbar size, this
	//       value comes straight out the FLTK source, there must be a better way of getting this value?
	UIUtils::LayoutHelper _helper;
	_helper.m_suggested_width = (2 * UIUtils::DEFAULT_X_PAD) + (m_brush_cell_width * DEFAULT_BRUSH_COLUMNS) + 16;
	_helper.m_suggested_height = 300;

	create_brush_select_panel(_helper);
	_helper.m_total_width += UIUtils::DEFAULT_X_PAD + _helper.m_actual_width;
	int _brush_height = _helper.m_actual_height;

	_helper.m_suggested_width = 250;
	_helper.m_suggested_height = 300;
	_helper.m_posx = _helper.m_total_width;
	_helper.m_posy = 0;
	create_options_panel(init_opacity, init_spacing, init_noise_freq, init_noise_step_start, init_noise_step_width, _helper);
	int _options_height = _helper.m_actual_height;
	_helper.m_total_width += _helper.m_actual_width;
#undef max
	// the above two panels are side by side, so which panel is taller
	_helper.m_total_height = std::max(_options_height, _brush_height);
	m_brush_scroll->size(m_brush_scroll->w(), _helper.m_total_height);
	m_brush_scroll->init_sizes();

	_helper.m_total_height += UIUtils::DEFAULT_Y_PAD;
		
	_helper.m_suggested_height = (UIUtils::DEFAULT_Y_PAD * 2) + UIUtils::BTN_LABEL_HEIGHT;
	_helper.m_posx = 0;
	_helper.m_posy = _helper.m_total_height + UIUtils::DEFAULT_Y_PAD;
	_helper.m_suggested_width = _helper.m_total_width;
	create_action_area_panel(_helper);
	_helper.m_total_height += _helper.m_actual_height + UIUtils::DEFAULT_Y_PAD;

	this->size(_helper.m_total_width, _helper.m_total_height);
	this->init_sizes();

	end();
}

/**
 * Destructor
 */
BrushSelectionDialog::~BrushSelectionDialog()
{

}



// private

/**
 * Refresh the selectable list of brushes from the specified BrushList
 *
 * @param brush_list the BrushList to refresh the selectable brush list display from
 */
void BrushSelectionDialog::RefreshBrushList(const BrushList& brush_list)
{
	// clear any previous brush images
	m_brush_scroll->clear();

	int xpos = UIUtils::DEFAULT_X_PAD;
	int ypos = UIUtils::DEFAULT_Y_PAD;

	for(unsigned i = 0; i < brush_list.Size(); i++)
	{
		CinePaintBrush* brush = brush_list.GetBrushByIndex(i);

		xpos = (i % BrushSelectionDialog::DEFAULT_BRUSH_COLUMNS)* m_brush_cell_width +1;
		Fl_Button* btn = new Fl_Button(xpos, ypos, m_brush_cell_width, m_brush_cell_height);
		btn->image(create_brush_image(*brush));
		btn->tooltip(brush->GetName());

		// we set FL_NO_BOX and rely upon the parent widget for the background color
		btn->box(FL_NO_BOX);

		m_brush_scroll->add(btn);

		if(((i+1) % BrushSelectionDialog::DEFAULT_BRUSH_COLUMNS) == 0)
		{
			ypos += m_brush_cell_width;
		}
	}

	// update the active selection 
	m_active_brush = brush_list.GetActiveBrush();
	if(m_active_brush)
	{
		
	}

	// fltk won't update the diaplay for us ...
	m_brush_scroll->redraw();
}

Fl_Image* BrushSelectionDialog::create_brush_image(CinePaintBrush& brush)
{
	AbstractBuf* _canvas = brush.GetMask();

	int _cell_width = m_brush_cell_width - (2 * BrushSelectionDialog::DEFAULT_MARGIN_WIDTH);
	int _cell_height = m_brush_cell_height - (2 * BrushSelectionDialog::DEFAULT_MARGIN_HEIGHT);

	bool scale = false;
	int _width = 0;
	int _height = 0;

	AbstractBuf* _brush_buf = 0;
	if(_canvas->GetWidth() > m_brush_cell_width || _canvas->GetHeight() > m_brush_cell_height)
	{
		scale = true;

		double _ratio_x = static_cast<double>(_canvas->GetWidth()) / static_cast<double>(_cell_width);
		double _ratio_y = static_cast<double>(_canvas->GetHeight()) / static_cast<double>(_cell_height);
		double _ratio = std::max(_ratio_x, _ratio_y);

		_width = static_cast<int>((_canvas->GetWidth() / _ratio) + 0.5);
		_height = static_cast<int>((_canvas->GetHeight() / _ratio) + 0.5);

		// create a new buffer for the scaled instance
		_brush_buf = new FlatBuf(_canvas->GetTag(), _width, _height);
		_brush_buf->RefPortionReadWrite(0, 0);

		// @TODO : Make src const - need to make ScaleOp take const for src.
		PixelArea *_src_area = _canvas->GetPixelArea(0, 0, _canvas->GetWidth(), _canvas->GetHeight());
		PixelArea *_dest_area = _brush_buf->GetPixelArea(0, 0, _width, _height);

		// get the scale tool to scale the brush data
		// @TODO should probably cache this to avoid repeatedly accessing it
		CinePaintScaleOp* _scale_op = CinePaintApp::GetInstance().GetImageOpManager().GetScaleOp(_src_area->GetBuffer().GetTag());

		if(_scale_op)
		{
			_scale_op->ScaleArea(*_src_area, *_dest_area);
		}
		else
		{
			printf("Cannot create brush image, no scale op found. %s\n", brush.GetName());
		}
		delete _src_area;
		delete _dest_area;
	}
	else
	{
		_width = _canvas->GetWidth();
		_height = _canvas->GetHeight();
		_brush_buf = _canvas;
	}

	unsigned char* _buf = new unsigned char[_width * _height * BrushSelectionDialog::DEFAULT_BRUSH_DEPTH];

	// Fill the brush display buffer and display it
	Fl_RGB_Image* image = 0;

	std::auto_ptr<BufferRenderer> renderer = BufferRendererFactory::GetBufferRenderer(_brush_buf->GetTag());
	if(renderer.get())
	{
		renderer->RenderMaskRGB(_buf, *_brush_buf);
		// add the scale indicator to the image
		if(scale)
		{
			add_scale_indicator(_buf, _width);
		}


		// @note [claw] we make the Assumption that FLTK manages the buf we add to an image,
		//       This needs checked!
		image = new Fl_RGB_Image(_buf, _width, _height, BrushSelectionDialog::DEFAULT_BRUSH_DEPTH);
	}
	
	return(image);
}

/**
 * Adds a small scaled instance indicator (a small cross) to the top left of the specified image data buffer.
 * The image buffer is assumed to be of BrushSelectionDialog::DEFAULT_BRUSH_DEPTH
 * If the width of the buffer is not large enough for the scale indicator, no indicator is added
 *
 * @param buf the image data buffer to add the indicator to
 * @param width the width of the image buffer
 */
void BrushSelectionDialog::add_scale_indicator(unsigned char* buf, unsigned int width) const
{
	// if the buffer is not big enough for the indicator, bail out
	if(width >= SCALE_INDICATOR_SIZE)
	{
		unsigned char pix;
		unsigned char* _b = buf;

		for(unsigned int y = 0; y < SCALE_INDICATOR_SIZE; y++)
		{
			for(unsigned int x = 0; x < SCALE_INDICATOR_SIZE; x++)
			{
				pix = SCALE_INDICATOR_BITS[x][y];
				for(unsigned int i = 0; i < BrushSelectionDialog::DEFAULT_BRUSH_DEPTH; i++)
				{
					*_b++ = pix;
				}
			}

			// move to the next row witin the buffer
			_b += ((width * BrushSelectionDialog::DEFAULT_BRUSH_DEPTH)
				- (SCALE_INDICATOR_SIZE * BrushSelectionDialog::DEFAULT_BRUSH_DEPTH));
		}
	}
}




/**
 * Createa the brush selection / preview scrolling panel
 *
 */
void BrushSelectionDialog::create_brush_select_panel(UIUtils::LayoutHelper& lh)
{
	//create the top level container
	m_brush_scroll = new Fl_Scroll(lh.m_posx, lh.m_posy, lh.m_suggested_width, lh.m_suggested_height) ;
	m_brush_scroll->type(Fl_Scroll::VERTICAL_ALWAYS);

	// we set the scroll background to white, this gives us a white background
	// for all the brush buttons we may add
	m_brush_scroll->color(FL_WHITE);

	m_brush_scroll->end();

	// return our actual width and height values
	lh.m_actual_width = lh.m_suggested_width;
	lh.m_actual_height = lh.m_suggested_height;
}


/**
 * Creates the widgets upon this BrushSelectionPanel
 *
 */
void BrushSelectionDialog::create_options_panel(double init_opacity,
													 double init_spacing,
													 double init_noise_freq,
													 double init_noise_step_start,
													 double init_noise_step_width,
													 UIUtils::LayoutHelper& lh)
{
	// these track the positions of our widgets since everything is absolutely positioned, yuck!
	int _x_col1 = lh.m_posx;
	int _x_col2 = lh.m_posx + DEFAULT_LABEL_WIDTH + UIUtils::DEFAULT_X_PAD;
	int _col2_width = lh.m_suggested_width - (UIUtils::DEFAULT_X_PAD + DEFAULT_LABEL_WIDTH);
	int _y_offset = lh.m_posy;

	//options box
	Fl_Group* _options_box = new Fl_Group(lh.m_posx, lh.m_posy, lh.m_suggested_width, lh.m_suggested_height) ;

	// Create the active brush label
	m_brush_name_label = UIUtils::CreateLabel(_x_col1, _y_offset, DEFAULT_LABEL_WIDTH, DEFAULT_ROW_HEIGHT, "Active");
	m_brush_size_label = UIUtils::CreateLabel(_x_col2, _y_offset, DEFAULT_LABEL_WIDTH, DEFAULT_ROW_HEIGHT, "(0x0)");
	_y_offset += (DEFAULT_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD);

	// Create the paint mode option menu
	Fl_Box* _mode_label = UIUtils::CreateLabel(_x_col1, _y_offset, DEFAULT_LABEL_WIDTH, DEFAULT_ROW_HEIGHT, "Mode:");
    m_mode_option_menu = new Fl_Choice(_x_col2, _y_offset, _col2_width, DEFAULT_ROW_HEIGHT);
	_y_offset += (DEFAULT_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD);
	create_paint_mode_menu(*m_mode_option_menu) ;

	// Create the opacity scale widget
	Fl_Box* _opacity_label = UIUtils::CreateLabel(_x_col1, _y_offset, DEFAULT_LABEL_WIDTH, DEFAULT_ROW_HEIGHT, "Opacity:");
	m_opacity_slider = new Fl_Value_Slider(_x_col2, _y_offset, _col2_width, DEFAULT_ROW_HEIGHT) ;
	_y_offset += (DEFAULT_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD);
	m_opacity_slider->type(FL_HOR_NICE_SLIDER) ;
	m_opacity_slider->bounds(0.0, 100.0) ;
	m_opacity_slider->step(1.0) ;
	m_opacity_slider->value((m_active_brush)?(init_opacity*100.0):100.0);

	Fl_Box* _spacing_label = UIUtils::CreateLabel(_x_col1, _y_offset, DEFAULT_LABEL_WIDTH, DEFAULT_ROW_HEIGHT, "Spacing:");
	m_spacing_slider = new Fl_Value_Slider(_x_col2, _y_offset, _col2_width, DEFAULT_ROW_HEIGHT) ;
	_y_offset += (DEFAULT_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD);
	m_spacing_slider->type(FL_HOR_NICE_SLIDER) ;
	m_spacing_slider->bounds(1.0, 1000.0) ;
	m_spacing_slider->step(1.0) ;
	m_spacing_slider->value(0.0);


	// The noise toggle
	Fl_Check_Button* _noise_button = new Fl_Check_Button(_x_col1, _y_offset, DEFAULT_LABEL_WIDTH, DEFAULT_ROW_HEIGHT, "Noise");
	_y_offset += (DEFAULT_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD);
	_noise_button->callback(static_noise_button_toggled_cb, this) ;


	// The noise box
	m_noise_box = new Fl_Group(_x_col1, _y_offset, lh.m_suggested_width, 3 * (DEFAULT_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD)) ;
	m_noise_box->begin();

	// noise freq slider
	Fl_Box* _noise_freq_label = UIUtils::CreateLabel(_x_col1, _y_offset, DEFAULT_LABEL_WIDTH, DEFAULT_ROW_HEIGHT, "Noise Freq:");
	m_noise_freq_slider = new Fl_Value_Slider(_x_col2, _y_offset, _col2_width, DEFAULT_ROW_HEIGHT) ;
	_y_offset += (DEFAULT_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD);
	m_noise_freq_slider->type(FL_HOR_NICE_SLIDER) ;
	m_noise_freq_slider->bounds(0.0, 1.0) ;
	m_noise_freq_slider->step(0.001) ;
	m_noise_freq_slider->value(m_active_brush ? init_noise_freq : 0.0);

	/* noise step start slider */
	Fl_Box* _noise_step_start_label = UIUtils::CreateLabel(_x_col1, _y_offset, DEFAULT_LABEL_WIDTH, DEFAULT_ROW_HEIGHT, "Strength:");
	m_noise_step_start_slider = new Fl_Value_Slider(_x_col2, _y_offset, _col2_width, DEFAULT_ROW_HEIGHT) ;
	_y_offset += (DEFAULT_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD);
	m_noise_step_start_slider->type(FL_HOR_NICE_SLIDER) ;
	m_noise_step_start_slider->bounds(0.0, 1.0) ;
	m_noise_step_start_slider->step(0.001) ;
	m_noise_step_start_slider->value(m_active_brush ? init_noise_freq : 0.0);

	/* noise step width slider */
	Fl_Box* _noise_step_width_label = UIUtils::CreateLabel(_x_col1, _y_offset, DEFAULT_LABEL_WIDTH, DEFAULT_ROW_HEIGHT, "Sharpness:");
	m_noise_step_width_slider = new Fl_Value_Slider(_x_col2, _y_offset, _col2_width, DEFAULT_ROW_HEIGHT) ;
	_y_offset += (DEFAULT_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD);
	m_noise_step_width_slider->type(FL_HOR_NICE_SLIDER) ;
	m_noise_step_width_slider->bounds(0.0, 1.0) ;
	m_noise_step_width_slider->step(0.0001) ;
	m_noise_step_width_slider->value(m_active_brush ? init_noise_freq : 0.0);

	m_noise_box->end();

	// Create the edit/new buttons
	//_y_offset += 15; // add some space before the action buttons

	int _btnxpos = _x_col1 + ((lh.m_suggested_width / 3) / 3);
	Fl_Button* _edit_brush_button = new Fl_Button(_btnxpos, _y_offset, DEFAULT_LABEL_WIDTH, UIUtils::BTN_LABEL_HEIGHT, "Edit Brush") ;
	_btnxpos += (lh.m_suggested_width / 3) + ((lh.m_suggested_width / 3) / 3);
	_edit_brush_button->callback(static_edit_brush_button_cb, this) ;
	Fl_Button* _new_brush_button = new Fl_Button(_btnxpos, _y_offset, DEFAULT_LABEL_WIDTH, UIUtils::BTN_LABEL_HEIGHT, "New Brush") ;
	_new_brush_button->callback(static_new_brush_button_cb, this) ;
	_y_offset += UIUtils::BTN_LABEL_HEIGHT;

	_options_box->end();

	// return our actual width and height values
	lh.m_actual_width = lh.m_suggested_width;
	lh.m_actual_height = _y_offset;

	_options_box->size(lh.m_actual_width, lh.m_actual_height);
	_options_box->init_sizes();
}

/**
 * Creates the action area widgets
 * Currently this consists of close and refresh panel buttons
 *
 * @return the constructed panel
 */
void BrushSelectionDialog::create_action_area_panel(UIUtils::LayoutHelper& lh)
{
	Fl_Group* _action_area = new Fl_Group(lh.m_posx, lh.m_posy, lh.m_suggested_width, lh.m_suggested_height) ;

	int _btnw = lh.m_suggested_width / 3;
	int _posx = lh.m_posx + (_btnw / 3);
	int _posy = lh.m_posy + UIUtils::DEFAULT_Y_PAD;
	Fl_Button* _close_button = new Fl_Button(_posx, _posy, _btnw, UIUtils::BTN_LABEL_HEIGHT, "Close") ;
	_close_button->callback(static_close_button_cb, this) ;
	_posx += _btnw + (_btnw / 3);
	
	Fl_Button* _refresh_button = new Fl_Button(_posx, _posy, _btnw, UIUtils::BTN_LABEL_HEIGHT, "Refresh") ;
	_refresh_button->callback(static_refresh_brush_list_button_cb, this) ;

	_action_area->end();

	// return our actual width and height values
	lh.m_actual_width = lh.m_suggested_width;
	lh.m_actual_height = lh.m_suggested_height;
}


//=================
// Widget callbacks

void BrushSelectionDialog::noise_button_toggled_cb(Fl_Check_Button* btn)
{
	if(btn->value())
	{
		//m_noise_box->show();
	}
	else
	{
		//m_noise_box->hide() ;
	}
}

void BrushSelectionDialog::paint_mode_menu_cb(Fl_Choice* choice)
{}

void BrushSelectionDialog::edit_brush_button_cb(Fl_Button* btn)
{
//	printf("Size %d\n", CinePaintApp::GetInstance().GetBrushList().Size());
}

void BrushSelectionDialog::new_brush_button_cb(Fl_Button* btn)
{}

void BrushSelectionDialog::close_button_cb(Fl_Button* btn)
{}

void BrushSelectionDialog::refresh_brush_list_button_cb(Fl_Button* btn)
{
	// @note [claw] need to add this
	//CinePaintApp::GetInstance().ReLoadBrushes();

	RefreshBrushList(CinePaintApp::GetInstance().GetBrushList());
}

//============================
// widget construction helpers
		
/**
 * Creates the paint mode menu options
 * @param menu the menu to add menu option into
 */
Fl_Choice& BrushSelectionDialog::create_paint_mode_menu(Fl_Choice& menu)
{
	menu.add("Normal", "", static_paint_mode_menu_cb, this);
	menu.add("Dissolve", "", static_paint_mode_menu_cb, this);
	menu.add("Behind", "", static_paint_mode_menu_cb, this);
	menu.add("Multiply", "", static_paint_mode_menu_cb, this);
	menu.add("Screen", "", static_paint_mode_menu_cb, this);
	menu.add("Overlay", "", static_paint_mode_menu_cb, this);
	menu.add("Difference", "", static_paint_mode_menu_cb, this);
	menu.add("Addition", "", static_paint_mode_menu_cb, this);
	menu.add("Subtract", "", static_paint_mode_menu_cb, this);
	menu.add("Darken Only", "", static_paint_mode_menu_cb, this);
	menu.add("Lighten Only", "", static_paint_mode_menu_cb, this);

	menu.add("Hue", "", static_paint_mode_menu_cb, this);
	menu.add("Saturation", "", static_paint_mode_menu_cb, this);
	menu.add("NColor", "", static_paint_mode_menu_cb, this);
	menu.add("Value", "", static_paint_mode_menu_cb, this);

	return(menu);
}
