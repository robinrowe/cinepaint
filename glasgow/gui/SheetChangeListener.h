/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      SheetChangeListener - Spreadsheet Change Listener interface
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: SheetChangeListener.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _SHEET_CHANGE_LISTENER_H_
#define _SHEET_CHANGE_LISTENER_H_

#include "dll_api.h"
#include <string>

/**
 * Purely Abstract class defining a SheetChangeListener interface.
 * Classes interested in receiving updates on changes to a SheetView should implement this.
 *
 * @todo review the methods available within this listener and whether uses of the interface
 *       are indeed giving the correct data values as detailed
 */
class CINEPAINT_GUI_API SheetChangeListener
{
	public:
		virtual ~SheetChangeListener() {};

		/**
		 * Invoked when the cursor cell is changed
		 *
		 * @param row the new row position of the cursor
		 * @param col the new column position of the cursor
		 */
		virtual void CursorCellChanged(int row, int col) = 0;

		/**
		 * Invoked when the current selection is changed
		 *
		 * @param row_start the start row of the selection
		 * @param col_start the start column of the selection
		 * @param row_end the end row of the selection
		 * @param col_end the end column of the selection
		 */
		virtual void RangeChanged(int row_start, int col_start, int row_end, int col_end) = 0;

		/**
		 * Invoked when the data within a cell is changed
		 *
		 * @param row the position of the cell being changed
		 * @param col the position of the cell being changed
		 * @param val string representation of the new value of the cell
		 */
		virtual void ValueChanged(int row, int col, const std::string& val) = 0;

	protected:
		SheetChangeListener() {};

}; /* class SheetChangeListener */

#endif /* _SHEET_CHANGE_LISTENER_H_ */
