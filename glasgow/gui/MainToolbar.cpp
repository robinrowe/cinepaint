/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      MainToolBar - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: MainToolbar.cpp,v 1.2 2006/12/21 11:18:06 robinrowe Exp $
 */

#include "MainToolbar.h"
#include <app/CinePaintApp.h>
#include "CinePaintAppUI.h"
#include "CinePaintButton.h"
#include "ColorBox.h"
#include "LoadSaveDialog.h"
#include "MenuFunctorHelper.h"
#include "UIUtils.h"
#include <app/CinePaintFunctor.h>
#include <app/ToolManager.h>

#include <FL/Fl.H>
#include <FL/Fl_Menu_Bar.H>

#include <string>
#include <config.h>

const int width=500;//125
const int height=150;//369//350
	
MainToolBar::MainToolBar()
: Fl_Window(width+3, height-56, APP_NAME " " APP_VERSION)
{
	this->build_main_menu();
	this->build_tool_box();
	new ColorBox(0, 300, width, 50, CinePaintApp::GetInstance().GetColorManager());

	end();


	// register a ToolChangeListener to respond to changes to the current tool
	// inituiated outwith the toolbar buttons
	m_tool_change_listener = new ToolListener(*this);
	CinePaintApp::GetInstance().GetToolManager().AddToolChangeListener(m_tool_change_listener);
}


MainToolBar::~MainToolBar()
{
	if(m_tool_change_listener)
	{
		CinePaintApp::GetInstance().GetToolManager().RemoveToolChangeListener(m_tool_change_listener);

		delete m_tool_change_listener;
		m_tool_change_listener = 0;
	}
}


/**
 * Builds the toolbox buttons panel
 */
void MainToolBar::build_tool_box()
{
	// Create The Buttons for the tools.
	int _xpos = 0;
	int _ypos = 31;
	const int cols=12;
	int _row_count = 0;

	// group for the radio buttons - hard coded sizes... grrr.
	// ... we'll need to redo this, and the layout, to allow a flexible number of tool buttons
	Fl_Group* g = new Fl_Group(_xpos, _ypos, width, height+19);

	ToolManager& _tool_manager = CinePaintApp::GetInstance().GetToolManager();
	for(ToolManager::const_iterator _citer = _tool_manager.begin(); _citer != _tool_manager.end(); ++_citer)
	{
		_xpos = (_row_count % cols) * 42 + 1;
		CinePaintButton* _btn = new CinePaintButton(_xpos, _ypos, 40, 30, 0, _citer->first.c_str());
		_btn->type(FL_RADIO_BUTTON);

		// create the functor
		CinePaintFunctor1<MainToolBar, std::string>* _functor = new CinePaintFunctor1<MainToolBar, std::string>(this, _citer->first, &MainToolBar::tool_button_cb);
		m_menu_helper->ManageFunctor(_citer->first.c_str(), _functor);

		_btn->callback(MenuFunctorHelper::static_menu_item_callback, _functor);
		Fl_Image* _img = UIUtils::CreateFlImage(_citer->second->GetIcon());

		if(_img)
		{
			_btn->image(_img);
		}
		_btn->tooltip(_citer->second->GetToolTip());
		_btn->color(fl_rgb_color(0x92, 0x92,0x92));
		if((_row_count + 1 )% cols == 0)
		{
			_ypos +=32;
		}

		m_toolbar_btns.insert(std::pair<std::string, Fl_Button*>(std::string(_citer->first), _btn));

		++_row_count;
	}
	g->end();
}

/**
 * Build the main Menu bar for the toolbox window.
**/
void MainToolBar::build_main_menu()
{
	// @note [claw] menu items are commented out until methods exist, the menu helper requires a valid method to call
	m_mMainMenu = new Fl_Menu_Bar(0, 0, width, 30);

	// manages all the functors we create
	m_menu_helper = new MenuFunctorHelper();

	// Add the File menu items
	m_menu_helper->AddMenuItem0(*m_mMainMenu, FL_CTRL+'N', "&File/&New...", &CinePaintAppUI::GetInstance(), &CinePaintAppUI::NewDocument);
	m_menu_helper->AddMenuItem0(*m_mMainMenu, FL_CTRL+'E', "&File/&New SpreadSheet", &CinePaintAppUI::GetInstance(), &CinePaintAppUI::NewSpreadSheet);
	m_menu_helper->AddMenuItem0(*m_mMainMenu, FL_CTRL+'E', "&File/&New FrameManager", &CinePaintAppUI::GetInstance(), &CinePaintAppUI::NewFrameManager);
	m_menu_helper->AddMenuItem0(*m_mMainMenu, FL_CTRL+'O', "&File/_&Open...", &CinePaintAppUI::GetInstance(), &CinePaintAppUI::OpenDocument);
	//m_menu_helper->AddMenuItem(*m_mMainMenu, 0, "&File/_&Acquire...", this, 0);
	//m_menu_helper->AddMenuItem(*m_mMainMenu, 0, "&File/_Preferences&...", this, 0);

	// Add Dialog submenu items
	m_menu_helper->AddMenuItem1(*m_mMainMenu, 0, "&File/_&Dialogs/&Brushes...", &CinePaintAppUI::GetInstance(), true, &CinePaintAppUI::SetBrushSelectionDialogVisible);
	m_menu_helper->AddMenuItem1(*m_mMainMenu, 0, "&File/_&Dialogs/&Color Selection...", &CinePaintAppUI::GetInstance(), true, &CinePaintAppUI::SetColorSelectionDialogVisible);
	m_menu_helper->AddMenuItem1(*m_mMainMenu, 0, "&File/_&Dialogs/&Pallettes...", &CinePaintAppUI::GetInstance(), true, &CinePaintAppUI::SetPalletteDialogVisible);
	m_menu_helper->AddMenuItem1(*m_mMainMenu, 0, "&File/_&Dialogs/&Gradient Editor...", &CinePaintAppUI::GetInstance(), true, &CinePaintAppUI::SetGradEditorDialogVisible);
	m_menu_helper->AddMenuItem1(*m_mMainMenu, 0, "&File/_&Dialogs/&Tool Options...", &CinePaintAppUI::GetInstance(), true, &CinePaintAppUI::SetToolOptionsDialogVisible);
	m_menu_helper->AddMenuItem1(*m_mMainMenu, 0, "&File/_&Dialogs/&Device Status...", &CinePaintAppUI::GetInstance(), true, &CinePaintAppUI::SetDeviceStatusDialogVisible);

	m_menu_helper->AddMenuItem0(*m_mMainMenu, FL_CTRL+'Q', "&File/&Quit", &CinePaintAppUI::GetInstance(), &CinePaintAppUI::Quit);

	// Add the root level items
	m_menu_helper->AddMenuItem1(*m_mMainMenu, 0, "&Xtns/Plugin Browser", &CinePaintAppUI::GetInstance(), true, &CinePaintAppUI::SetPluginBrowserVisible);
	//m_menu_helper->AddMenuItem(*m_mMainMenu, 0, "&Help/About...", this, 0);
	//m_menu_helper->AddMenuItem(*m_mMainMenu, 0, "&Help/Bugs & Kudos...", this, 0);
	//m_menu_helper->AddMenuItem(*m_mMainMenu, 0, "&Help/Tips...", this, 0);
	//m_menu_helper->AddMenuItem(*m_mMainMenu, 0, "&Help/DB Browser", this, 0);
}

/*****************************************
 * The callback functions. (Toolbox buttons)
 *****************************************/

/**
 * Callback for a generic tool button
 * We deteremine what to do based upon the action command within the button
 */
void MainToolBar::tool_button_cb(std::string tool_type)
{
	//CinePaintButton* btn = reinterpret_cast<CinePaintButton*>(w);

	if(Fl::event_button() == 3) // right click
	{
		CinePaintAppUI::GetInstance().SetToolOptionsDialogVisible(true);
	}

	// @TODO show selected tool dialog - dynamically created from plugin?
	CinePaintApp::GetInstance().GetToolManager().SetSelectedTool(tool_type.c_str());
}





//---------------
// Nested Classes

/**
 * ToolChangeListner for setting the selected tool button upon tool change events
 *
 */
MainToolBar::ToolListener::ToolListener(MainToolBar& toolbar)
 : m_toolbar(toolbar)
{}

MainToolBar::ToolListener::~ToolListener()
{}

/**
 * Invoked when the currently selected tool is changed
 *
 * @param tool_type the name of the newly selected tool
 */
void MainToolBar::ToolListener::ToolChanged(const char* tool_type)
{
	std::map<std::string, Fl_Button*>::const_iterator citer = m_toolbar.m_toolbar_btns.find(std::string(tool_type));

	if(citer != m_toolbar.m_toolbar_btns.end())
	{
		Fl_Button* _btn = citer->second;
		if(!_btn->value())
		{
			_btn->setonly();
		}
	}
}
