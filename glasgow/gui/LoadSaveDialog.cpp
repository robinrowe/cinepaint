/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      LoadSaveDialog - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: LoadSaveDialog.cpp,v 1.4 2006/12/21 11:18:06 robinrowe Exp $
 */

#include "LoadSaveDialog.h"
#include <app/CinePaintApp.h>
#include "CinePaintAppUI.h"

#include <FL/Fl_File_Chooser.H>

#include <sys/types.h>
#include <sys/stat.h>

LoadSaveDialog::LoadSaveDialog( bool save )
{
//	char* type_string; // = CinePaintApp::GetInstance().GetPluginDB().GetFileTypeString();
	World* world=CinePaintApp::GetWorld();
	m_save = save;
	if (save)
	{	m_fileChooser = new Fl_File_Chooser(".", "Jpeg Images (*.jpg)", Fl_File_Chooser::CREATE, "Save File");
	}
	else
    {	PDB* pdb=PDB::GetPDB(world);
		if(!pdb)
		{	return;
		}
    	m_fileChooser = new Fl_File_Chooser(".", 0 /* BUG: pdb->GetFileTypeString()*/, Fl_File_Chooser::SINGLE, "Open File");
	}
	m_fileChooser->callback(FileSelectStaticCallback, this);
}

DialogResponse LoadSaveDialog::ShowAndWait()
{
//	m_fileChooser->set_modal();
//	m_fileChooser->hotspot(m_fileChooser);
	m_fileChooser->show();
    struct stat buf;
	int _overwrite;
	bool done = false;
	const char* filename;

	while (!done)
	{
		while(m_fileChooser->shown())
		{
			Fl::wait();
	    }

		if (m_save)
		{
			if (m_fileChooser->count() > 0)
			{
				filename = m_fileChooser->value();
				if ( stat(filename, &buf)==0 ) // File exists so check what user wants.
				{
					_overwrite = fl_choice("File Exists Overwrite?", "Yes", "No", NULL);
					if (_overwrite == 0)
						done = true;
					else	
						m_fileChooser->show();
				}
				else
					done = true;
			}
			else 
				done =true;
		}
		else
			done = true;
	}

	if (m_fileChooser->count() > 0)
		return OK_ENUM;
	else
		return CANCEL_ENUM;
}

LoadSaveDialog::~LoadSaveDialog()
{
}

void LoadSaveDialog::FileSelectCallback(Fl_File_Chooser *w)
{
	/* This should just set the preview.*/
	// const char* filename;
	// filename = w->value();
	// if (filename)
	// 	CinePaintApp::GetInstance().GetDocList().OpenDocument(filename);
}

const char* LoadSaveDialog::GetFileName()
{
	if (m_fileChooser->count() > 0)
		return m_fileChooser->value();
	else
		return 0;
}
