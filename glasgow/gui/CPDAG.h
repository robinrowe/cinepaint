/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CPDAG - Directed acyclic graph for EDL
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Stuart Ford
 *   Colin Law
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * $Id: CPDAG.h,v 1.4 2008/01/28 18:22:28 robinrowe Exp $
 */

#ifndef _CPDAG_H_
#define _CPDAG_H_

#include "dll_api.h"
#include <utility/CPSerializable.h>
#include "CPDAGIterator.h"
#include "CPDAGData.h"
#include "plug-ins/pdb/CPParamList.h" // req. for CPPluginArgType
#include "plug-ins/pdb/World.h"
#include <map>
#include <set>
#include <string>

class AbstractBuf;
class CPDAGData;
class CPDAGEvaluator;
class CPDAGNode;
class PluginParam;

/**
 * A representation of a directed acyclic graph to be used as the underlying
 * data structure for combining image operations in Cinepaint. Each node in the
 * graph has a unique name and a piece of data associated with it. Note that the
 * graph is not necessarily fully connected - it may consist of unconnected
 * nodes and sub-graphs.
 */
class CINEPAINT_CORE_API CPDAG : public CPSerializable
{	World* world;
		//--------
		// Friends
		friend class CPDAGScheduler;
		friend class CPDAGEvaluator;

	public:

		//-------------------------
		// Constructor / Destructor

		CPDAG(World* world)
		{	this->world=world;
		}
		CPDAG(CPSocket& socket,World* world)
		{	this->world=world;
			Deserialize(socket);
		}
		virtual ~CPDAG();

		//---------------
		// Graph Addition

		/**
		 * Create a new unconnected node in this DAG holding the specified int value.
		 * Nodes must have distinct names, so if a node already exists with the 
		 * given name, the new node will not be created.
		 *
		 * @param name the name of the new node
		 * @param data the int value to be held in the new node
		 * @return true if the node was create successfully, false otherwise for example
		 *         if the node name name already exists
		 */
		bool NewNode(const std::string& name, int data);

		/**
		 * Create a new unconnected node in this DAG holding the specified float value.
		 * Nodes must have distinct names, so if a node already exists with the 
		 * given name, the new node will not be created.
		 *
		 * @param name the name of the new node
		 * @param data the float value to be held in the new node
		 * @return true if the node was create successfully, false otherwise for example
		 *         if the node name name already exists
		 */
		bool NewNode(const std::string& name, float data);

		/**
		 * Create a new unconnected node in this DAG holding the specified string value.
		 * Nodes must have distinct names, so if a node already exists with the 
		 * given name, the new node will not be created.
		 * The data string is copied internally by the node holding the actual data.
		 *
		 * @param name the name of the new node
		 * @param data the string value to be held in the new node
		 * @return true if the node was create successfully, false otherwise for example
		 *         if the node name name already exists
		 */
		bool NewNode(const std::string& name, const char* data);

		/**
		 * Create a new unconnected node in this DAG holding the specified AbstractBuf value.
		 * Nodes must have distinct names, so if a node already exists with the 
		 * given name, the new node will not be created.
		 *
		 * @param name the name of the new node
		 * @param data the AbstractBuf value to be held in the new node
		 * @return true if the node was create successfully, false otherwise for example
		 *         if the node name name already exists
		 */
		bool NewNode(const std::string& name, AbstractBuf* data);

		/**
		 * Create a new unconnected node in this DAG holding the specified plugin name
		 * Nodes which hold plugin names differ from those that hold plugin data values
		 * by having the data type of the node set to DAG_PLUGIN indicating that the
		 * node stores a plugin name.
		 * Nodes must have distinct names, so if a node already exists with the 
		 * given name, the new node will not be created.
		 *
		 * @param name the name of the new node
		 * @param data the int value to be held in the new node
		 * @return true if the node was create successfully, false otherwise for example
		 *         if the node name name already exists
		 */
		bool NewPluginNode(const std::string& name, const std::string& plugin);








		//----------------
		// Graph Structure

		/**
		 * Make a directed connection between 2 nodes.
		 * In Cinepaint, links in the DAG connect parameters to plugins, both of
		 * which are stored in the nodes. Each link between nodes is named, the
		 * name represents the parameter name in the plugin call. If making the
		 * connection would form a cycle in the DAG, the operation will fail.
		 * The node providing the parameter to a plugin node may contain multiple
		 * values, for example the results of previous plugin calls. To distinguish
		 * between these evaluation values, the required parameter value is also named.
		 *
		 * Paramater Node (m)  ->  connection name (param)  ->  Plugin Node (n)
		 * Evaluations (eval)
		 *
		 * During construction of the plugin call, the parameter is added to the plugin
		 * using the connection name (param) as the name of the parameter to the plugin.
		 * The actual parameter value is retrieved from node m, using the evaluation name
		 * eval. 
		 *
		 * @param m name of the node to connect \e from (parameter)
		 * @param n name of the node to connect \e to (plugin)
		 * @param param	the parameter name in node n the connection represents
		 * @param eval the evaluation value name held within node m
		 * @param absoluteRow (spreadsheet) indicates if the row is absolute
		 * @param absoluteCol (spreadsheet) indicates if the column is absolute
		 * @return true on success, else false
		 */
		bool Attach(const std::string& m, const std::string& n, const std::string& param, const std::string& eval, bool absoluteRow = false, bool absoluteCol = false);

		/**
		 * Completely break a directed connection between 2 nodes.
		 * Two nodes may be connected multiple times on different parameters, this method
		 * completely detaches two nodes on all parameter names.
		 * If the connection does not exist no action will be taken and the operation
		 * will be considered successful. If a node does not exist the operation
		 * will be considered to have failed.
		 *
		 * @param m name of the node connected \e from
		 * @param n name of the node connected \e to
		 * @param param	the parameter name in node n the connection represents
		 * @param eval the evaluation value name held within node m
		 * @return true on success, else false
		 */
		bool Detach(const std::string& m, const std::string& n, const std::string& param, const std::string& eval, bool absoluteRow, bool absoluteCol);

		/**
		 * Completely break a directed connection between 2 nodes.
		 * Two nodes may be connected multiple times on different parameters, this method
		 * completely detaches two nodes on all parameter names.
		 * If the connection does not exist no action will be taken and the operation
		 * will be considered successful. If a node does not exist the operation
		 * will be considered to have failed.
		 *
		 * @param m name of the node connected \e from
		 * @param n name of the node connected \e to
		 * @return true on success, else false
		 */
		bool Detach(const std::string& m, const std::string& n);

		/**
		 * Completely disconnect a node.
		 * If the node does not exist the operation will be considered to have failed.
		 *
		 * @param m name of the node to disconnect
		 * @return true on success, else false
		 */
		bool Detach(const std::string& m);

		/**
		 * Disconnect and delete a node from a graph.
		 * If the node does not exist the operation will be considered to have succeeded.
		 *
		 * @param m name of the node to remove
		 * @return true on success, else false
		 */
		bool Remove(const std::string& m);



		/**
		 * Returns whether the specified node is contained within this CPDAG
		 *
		 * @param node_name the node name
		 * @return true if the specified node is contained witin this CPDAG,
		 *         false otherwise
		 */
		bool Exists(const std::string& node_name);

		/**
		 * Rename a given node.
		 *
		 * @param oldName current name of the node to change
		 * @param newName new name of the node
		 * @return true if the node exists and was renamed, else false
		 */
		bool Rename(const std::string& oldName, const std::string& newName);

		/**
		 * Returns the number of nodes in this CPDAG.
		 *
		 * @return the number of nodes in this CPDAG.
		 */
		size_t Size() const;





		//------------------------
		// Graph Node Modification

		/**
		 * Set the data in an existing node in this DAG.
		 * If a node with the given name does not exist it will first be created
		 * using NewNode(const std::string&, int)
		 *
		 * @param name the name of the node
		 * @param data the int value to be held in the node
		 * @return true if the data value was set, false otherwise
		 * @see NewNode(const std::string &name, int data) 
		 */
		bool Set(const std::string& name, int data);

		/**
		 * Set the data in an existing node in this DAG.
		 * If a node with the given name does not exist it will first be created
		 * using NewNode(const std::string&, float)
		 *
		 * @param name the name of the node
		 * @param data the float value to be held in the node
		 * @return true if the data value was set, false otherwise
		 * @see NewNode(const std::string &name, float data) 
		 */
		bool Set(const std::string& name, float data);

		/**
		 * Set the data in an existing node in this DAG.
		 * If a node with the given name does not exist it will first be created
		 * using NewNode(const std::string&, const char*)
		 * The string value is copied internally by the node.
		 *
		 * @param name the name of the node
		 * @param data the string value to be held in the node
		 * @return true if the data value was set, false otherwise
		 * @see NewNode(const std::string &name, const char* data) 
		 */
		bool Set(const std::string& name, const char* data);

		/**
		 * Set the data in an existing node in this DAG.
		 * If a node with the given name does not exist it will first be created
		 * using NewNode(const std::string&, AbstractBuf*)
		 *
		 * @param name the name of the node
		 * @param data the AbstractBuf to be held in the node
		 * @return true if the data value was set, false otherwise
		 * @see NewNode(const std::string &name, AbstractBuf *data) 
		 */
		bool Set(const std::string& name, AbstractBuf *data);

		/**
		 * Set the plugin name in an existing node in this DAG.
		 * If a node with the given name does not exist it will first be created
		 * using NewPluginNode(const std::string&, const std::string&)
		 *
		 * @param name the name of the node
		 * @param plugin the plugin name to store within the noew
		 * @return true if the data value was set, false otherwise
		 * @see NewPluginNode(const std::string&, const std::string&) 
		 */
		bool SetPlugin(const std::string& name, const std::string& plugin);





		//-----------------
		// Node/Data Access

		/**
		 * Returns the named node from within this CPDAG
		 * If the named node is not contained within this CPDAG, 0 is returned.
		 *
		 * @param name the name of the node
		 * @return the specified node or 0, if the node does not exist
		 */
		const CPDAGNode* GetNode(const std::string& name) const;

		/**
		 * Returns the data element of the specified node.
		 * If the named node is not contained within this CPDAG, 0 is returned.
		 *
		 * @param name the name of the node
		 * @return the data in the node or 0, if the node does not exist
		 */
		const CPDAGData* GetData(const std::string& name) const;

		/**
		 * Returns the default evaluated data from within an existing node in this CPDAG.
		 * If the node has not been evaluated #Evaluate will be called on it and
		 * the result returned.
		 *
		 * @param name the name of the node
		 * @return the evaluated data in the node, or 0 if the node does not exist
		 *         or does not contain any evaluated data
		 */
		const PluginParam* GetEvaluated(const std::string& name);

		/**
		 * Returns the named evaluated data from within an existing node in this CPDAG.
		 * If the node has not been evaluated #Evaluate will be called on it and
		 * the result returned.
		 *
		 * @param name the name of the node
		 * @param eval the name of the evaluation value
		 * @return the evaluated data in the node, or 0 if the node does not exist
		 *         or does not contain any evaluated data
		 */
		const PluginParam* GetEvaluated(const std::string& name, const std::string eval);




		/**
		 * Populates the specified set with the names of all the nodes in this CPDAG.
		 * The set is not cleared prior to adding any node names
		 *
		 * @param nodes the set for holding the names of the nodes
		 * @return the set populated with the naems of nodes contained within this CPDAG
		 */
		std::set<std::string>& Nodes(std::set<std::string>& nodes) const;

		/**
		 * Populates the specified map with a mapping of all the nodes that connect to the specified node
		 * The map of node names represents a mapping of the inputs node, or parameters into a plugin node,
		 * to a pair representing the parameter name and evaluation name.
		 *
		 * @param name the name of the node
		 * @param inputs a mapping of input nodes to the parameter and evaluation names
		 * @return true if the node exists, else false
		 */
		bool GetInputNodes(const std::string& name, std::map<std::string, std::pair<std::string, std::string> >& inputs) const;

		/**
		 * Populates the specified map with a mapping of all the nodes the specified node connects to.
		 * The map of node names represents a mapping of the inputs node, or parameters into a plugin node,
		 * to a pair representing the parameter name and evaluation name.
		 *
		 * @param name the name of the node
		 * @param outputs a mapping of output nodes to the parameter and evaluation names
		 * @return true if the node exists, else false
		 */
		bool GetOutputNodes(const std::string& name, std::map<std::string, std::pair<std::string, std::string> >& outputs) const;




		//-------------------
		// Execution  Methods

		/**
		 * Evaluate the data in the node with the given name.
		 * A node can only be evaluated if all the nodes directly prior to it in
		 * the graph have been evaluated already.
		 *
		 * @param node_name the name of the node to evaluate
		 * @return true if the node was evaluated successfully, else false
		 */
		bool Evaluate(const std::string& node_name);






		//----------------
		// Iterator access

		/**
		 * The iterator class that presents the DAG topologically sorted
		 */
		typedef CPDAGIterator const_iterator;

		/**
		 * Returns an iterator pointing to the start of the DAG.
		 * The start of a CPDAG is not unique and is defined as the first 
		 * node in a topologically sorted list of the DAG's nodes.
		 *
		 * @return an iterator pointing to the start of the DAG
		 */
		const_iterator begin() const;

		/**
		 * Returns an iterator pointing past the end of the DAG.
		 *
		 * @return an iterator pointing past the end of the DAG
		 */
		const_iterator& end() const;





		//--------------
		// Serialization

		/**
		 * Serialize and write the graph to a given socket.
		 * @param socket the socket to write the graph to
		 */
		void Serialize(CPSocket& socket) const;

		/**
		 * Read and deserialize the graph from a given socket.
		 * @param socket the socket to read the graph from
		 */
		void Deserialize(CPSocket& socket);





		//----------
		// Constants

		/** The string representing an absolute reference in spreadsheet params */
		static const char* mc_Absolute;

		/** The string representing a relative reference in spreadsheet params */
		static const char* mc_Relative;




	private:
		typedef std::map<std::string, CPDAGNode*> NodesContainer_t;


	protected:

		/**
		 * Protected copy constructor.
		 * There is probably no need for this copy constructor and there are too many
		 * issues concerning how deep to copy, so don't let it get called publicly
		 *
		 * @param graph
		 */
		CPDAG(const CPDAG& graph)
		{}


		typedef NodesContainer_t::const_iterator nodes_iterator;

		nodes_iterator find_node(const std::string& name);
		nodes_iterator nodes_begin();
		nodes_iterator nodes_end();


	private:


		// Member functions

		/**
		 * Tests the graph to see if a cycle is present
		 *
		 * @return true if a cycle exists, else false
		 */
		bool contains_cycle();

		/**
		 * Sets the dirty status of a node.
		 * A dirty node requires evaluation. Setting a node to be dirty will
		 * recursively set all the nodes below it in the graph to be dirty also.
		 * Making a node clean only affects that node, and no recursion takes place.
		 *
		 * @param node the name of the node to change
		 * @param d the new dirty status of the node
		 */
		void set_dirty(const std::string& node, bool d);

		/**
		 * Embellish a param string with information on the type of reference it uses in a spreadsheet.
		 * 2 characters are added to the front of the param, the first indicating if the row in the
		 * reference is absolute, the second indicating likewise for the column.
		 *
		 * @return the embellished param string
		 */
		std::string param_embellish(const std::string& param, bool absRow, bool absCol);

		/**
		 * Strip a param string of information on the type of reference it uses in a spreadsheet.
		 * The first 2 characters of the string are stripped off. This is the complement of the
		 * #paramEmbellish function.
		 *
		 * @return the stripped param string
		 */
		std::string param_strip(const std::string& param) const;

		/**
		 * Create a new unconnected node in this DAG.
		 * Nodes must have distinct names, so if a node already exists with the
		 * given name, the new node will not be created. The constructed Node 
		 * initially contains no data.
		 *
		 * @param name the name of the new node
		 * @return the constructed node, or 0 if no new node could be created
		 */
		CPDAGNode* new_node(const std::string& name);

		/**
		 * Pre-process a node prior to setting its value to determine if the data type of the node has been changed.
		 * If the data type of the node has changed, for example a node which was a plugin node
		 * is changed to a plugin parameter node, noce connections involving the node may no
		 * longer be valid
		 *
		 * @TODO need to review this
		 * @param node_name the node name to be changed
		 * @param data_type the new data type
		 * @param node_type the new node type
		 */
		void process_cell_change(const std::string& node_name, CPPluginArgType data_type, CPDAGData::DataType node_type);



		//--------
		// Members


		/** The container of nodes comprising this CPDAG, mapped to their names */
		NodesContainer_t m_nodes;

};

#endif /* _CPDAG_H_ */
