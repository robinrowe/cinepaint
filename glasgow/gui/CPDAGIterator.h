/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CPDAGIterator - Directed acyclic graph iterator
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Stuart Ford
 *   Colin Law
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * $Id: CPDAGIterator.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CPDAG_ITERATOR_H_
#define _CPDAG_ITERATOR_H_

#include "dll_api.h"
#include "CPDAGScheduler.h"
#include <iterator>
#include <string>

class CPDAG;

/**
 * An iterator class for the CPDAG class. The iterator traverses a CPDAG in
 * topological order, which is not unique. Inserting/deleting/altering a CPDAG
 * will invalidate any iterator traversing it. The iterator returns the names of
 * the nodes it traverses. A topological ordering over a CPDAG is a list of
 * nodes such that nodes earlier in the list cannot be travelled to from nodes
 * later in it. This allows a CPDAG to be evaluated in a simple manner.
 */
class CINEPAINT_CORE_API CPDAGIterator : public std::iterator<std::forward_iterator_tag, const std::string, ptrdiff_t>
{
	public:

		// FUNCTIONS

		/**
		 * Constructor. Creates an empty, essentially unusable iterator.
		 */
		CPDAGIterator();

		/**
		 * Copy constructor.
		 * @param it the iterator to be copied
		 */
		CPDAGIterator(const CPDAGIterator& it);

		/**
		 * Constructor. Creates an iterator to traverse a given CPDAG.
		 * @param graph a pointer to the CPDAG to traverse
		 */
		CPDAGIterator(const CPDAG& graph);

		/**
		 * Destructor.
		 */
		~CPDAGIterator();

		/**
		 * Equality operator.
		 * @param rhs iterator to compare to the current one
		 * @return true if the iterators are equal, else false
		 */
		bool operator==(const CPDAGIterator& rhs) const;

		/**
		 * Inequality operator.
		 * @param rhs iterator to compare to the current one
		 * @return true if the iterators are unequal, else false
		 */
		bool operator!=(const CPDAGIterator& rhs) const;

		/**
		 * Dereference operator.
		 * @return the name of the node currently pointed to
		 */
		const std::string operator*() const;

		/**
		 * Prefix increment operator. Moves the iterator onto the next node.
		 * @return the current iterator incremented
		 */
		CPDAGIterator &operator++();

		/**
		 * Postfix increment operator. Moves the iterator onto the next node.
		 * @return the current iterator before it is incremented
		 */
		CPDAGIterator operator++(int);

		/**
		 * Assignment operator.
		 * @param rhs the iterator to copy to the current iterator
		 * @return the current iterator, now a copy of the given one
		 */
		CPDAGIterator& operator=(const CPDAGIterator& rhs);


	private:

		// MEMBERS

		/**
		 * The underlying scheduler that does most of the work.
		 */
		CPDAGScheduler m_scheduler;

		/**
		 * The name of the node currently pointed at by the iterator.
		 */
		std::string m_currentNode;


		// FUNCTIONS

		/**
		 * Copies a given iterator to the current one. If this is called from a
		 * constructor, m_indegreeZeroQ and m_indegrees should be copied in the
		 * constructor initializer list.
		 * @param it			[i] the iterator to copy
		 * @param isConstructor	[i] flags whether this is called from a constructor
		 * @return	the current iterator
		 */
		CPDAGIterator& copy(const CPDAGIterator& it, bool isConstructor);


};

#endif /* _CPDAG_ITERATOR_H_ */
