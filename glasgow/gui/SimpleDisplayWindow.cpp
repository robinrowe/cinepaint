/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintImageWindow - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: SimpleDisplayWindow.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include <unistd.h>
#include <FL/fl_draw.H>
#include <gui/SimpleDisplayWindow.h>
#include <gui/CinePaintWidgetListener.h>
#include <plug-ins/pdb/AbstractBuf.h>
#include <plug-ins/pdb/CinePaintTag.h>

SimpleDisplayWindow::SimpleDisplayWindow(int w, int h, const char* title)
		: Fl_Double_Window(w, h, title)
{
	m_buf = 0;

	m_roi.SetRect(0, 0, w, h);

	// allow the window to be resized;
	resizable(this);

	this->end();
}

SimpleDisplayWindow::~SimpleDisplayWindow()
{}

void SimpleDisplayWindow::SetBuffer(const AbstractBuf* buf)
{
	m_buf = buf;

	if(m_buf)
	{
		if((buf->GetTag().GetPrecision() == CinePaintTag::PRECISION_U8_ENUM) && (buf->GetTag().GetFormat() == CinePaintTag::FORMAT_RGB_ENUM))
		{
			this->redraw();
		}
		else
		{
			m_buf = 0;
			printf("Invalid Buffer SbatrsBuf Tag, Format must be U8 RGB\n");
		}
	}
}


//----------------------------
// Overriden FL_Widget methods

/**
 * Resizes and position this Widget to the specified values.
 * This implementation calls FL_Widget::resize but also informs all registered
 * CinePaintWidgetListerns of the change.
 *
 * @param x the x location
 * @param y the y location
 * @param w the widget width
 * @param h the widget height
 */
void SimpleDisplayWindow::resize(int x, int y, int w, int h)
{
	// the region of interest is unused just now, but we need to set it to redraw correctly
	m_roi.SetRect(0, 0, w, h);

	Fl_Double_Window::resize(x, y, w, h);

	for(WidgetListenerList_t::const_iterator citer = m_widget_listeners.begin(); citer != m_widget_listeners.end(); ++citer)
	{
		(*citer)->WidgetResized(w, h);
	}
}

/**
 * Sets the size of thie widget to the specified size.
 * This methods is a convenience method for resize, hiding FL_Widget::size inorder to
 * provide notifications of widget events to registered CinePaintWidgetListeners
 *
 * @param w the widget width
 * @param h the widget height
 */
void SimpleDisplayWindow::size(short w, short h)
{
	resize(this->x(), this->y(), w, h);
}


/**
 * Adds the specified CinePaintWidgetListener to receive widget events from this CinePaintDrawArea
 *
 * @param cwl the CinePaintWidgetListener to be added
 */
void SimpleDisplayWindow::AddWidgetListener(CinePaintWidgetListener* cwl)
{
	m_widget_listeners.AddListener(cwl);
}

/**
 * Removes and returns the specified CinePaintWidgetListener.
 *
 * @param cwl the CinePaintWidgetListener to be removed
 */
CinePaintWidgetListener* SimpleDisplayWindow::RemoveWidgetListener(CinePaintWidgetListener* cwl)
{
	return(m_widget_listeners.RemoveListener(cwl));
}

void SimpleDisplayWindow::draw()
{
	fl_push_clip(m_roi.GetX(), m_roi.GetY(), m_roi.GetWidth(), m_roi.GetHeight());

	// blank the background
	fl_color(FL_BLACK);
	fl_rectf(m_roi.GetX(), m_roi.GetY(), m_roi.GetWidth(), m_roi.GetHeight());

	if(m_buf)
	{
		const unsigned char* data = m_buf->GetPortionData(0, 0);
		if(data)
		{
			int pos_x = 0;
			int pos_y = 0;

			if(this->w() > m_buf->GetWidth())
			{
				pos_x += (this->w() - m_buf->GetWidth()) /2;
			}

			if(this->h() > m_buf->GetHeight())
			{
				pos_y += (this->h() - m_buf->GetHeight()) /2;
			}

			fl_draw_image(data, pos_x, pos_y, m_buf->GetWidth(), m_buf->GetHeight(), 3);
		}
		else
		{
			printf("SimpleDisplayWindow: no data to render.\n");
		}
	}
}
