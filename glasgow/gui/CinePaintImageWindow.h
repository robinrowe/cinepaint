/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintImageWindow - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintImageWindow.h,v 1.2 2006/12/21 11:18:06 robinrowe Exp $
 */

#ifndef _CINEPAINT_IMAGE_WINDOW_H_
#define _CINEPAINT_IMAGE_WINDOW_H_

#include "dll_api.h"
#include <FL/Fl_Double_Window.H>
#include <plug-ins/pdb/CinePaintMouseListener.h>
#include <gui/CinePaintWidgetListener.h>
#include <gui/ToolProxy.h>
#include <plug-ins/pdb/World.h>
#include <app/CinePaintEvent.h>

#include <functional>
#include <string>

class CINEPAINT_GUI_API Fl_Double_Window;

class AbstractRenderer;
class CinePaintDrawArea;
class CinePaintImage;
class ImageInfoWindow;
class Fl_Box;
class Fl_Menu_Button;
class Fl_Menu_Bar;
class Fl_Scrollbar;
class MenuFunctorHelper;

class CINEPAINT_GUI_API CinePaintImageWindow : public Fl_Double_Window
{	
	public:
		/**
		 * Constructs a new CinePaintImageWindow with the specified size and title
		 *
		 * @param w the window width
		 * @param h the window height
		 * @param title the window title
		 */
		CinePaintImageWindow(int w, int h, const char* title);

		/**
		 * Destructor
		 */
		virtual ~CinePaintImageWindow();



		//---------------------
		// Accessors / Mutators

		/**
		 * Returns the drawing area of this ImageWindow
		 *
		 * @return the CinePaintDrawArea displaying the CinePaintImage within this ImageWindow
		 */
		CinePaintDrawArea* GetImageDrawArea();


		/**
		 * Toggles the visibility of the draw area Rulers
		 *
		 */
		void ToggleRulerVisibility();

		/**
		 * Sets the visibility of the draw area rulers
		 *
		 * @param visible set true to show the draw area rulers
		 */
		void SetRulersVisible(bool visible);

		/**
		 * Gets the visibility of the draw area rulers
		 *
		 * @true the current visibility of the draw area rulers
		 */
		bool GetRulersVisible();

		void ToggleInfoWindow();
		void SetInfoWindowVisible(bool visible);


		/**
		 * Sets the CinePaintImage this CinePaintWindows display, and the Renderer handling the rendering of the Image.
		 *
		 * @param image the CinePaintImage to display
		 * @param renderer the AbstractRenderer used to render the image
		 */
		void SetImage(CinePaintImage* image, AbstractRenderer* renderer);

		/**
		 * Returns the image being displayed upon this CinePaintImageWindow
		 *
		 * @return the image being displayed
		 */
		CinePaintImage* GetImage();


		
		//----------------------
		// window sizing methods

		/**
		 * Fits this window to the current image scale.
		 * If the scale is currently smaller than this window, the window is shrunk to disaplay
		 * all of the image with no additional surrounding space. If the image is larger then this
		 * window, this window is enlarged to fit the image.
		 * If the image is larger than the screensize no attempt is made to shrinkwrap
		 *
		 */
		void ShrinkwrapImage();

	protected:
	private:

		/**
		 * callback for plugin menu items to execute a named plugin.
		 * This method get the procedure database and excutes the named plugin.
		 * This method is a required helper for the menu item as the excute method of the prcedure
		 * database returns a value, which is not supported (at this time) by the MenuFunctorHelper
		 * used to create the menu, i.e. it expects a void return value.
		 *
		 */
		void execute_plugin_wrapper(const char* name);

		/**
		 * Callback for the Save menu item, we need to pass the document to the
		 * save handler and so need a callback to do this, since the helper cannot
		 * deal with this extra data.
		**/
		void file_save_callback();

		/**
		 * as file_save_callback but for save as
		**/
		void file_save_as_callback();
		
		void file_close_callback();
		void revert_doc_callback();


		/**
		 * Requests a new view of the CinePaintImage being diaplyed within this CinePaintImageWindow
		 *
		 */
		void open_new_view();

		/**
		 * Updates the scroll bar values of the window based upon the current projection settings of the CinePaintDrawArea
		 *
		 */
		void update_scrollbars();

		/** the CinePaintDrawArea displaying the image and handling mouse movement etc */
		CinePaintDrawArea* m_draw_area;

		/** Info Window - each image window has its own info window */
		ImageInfoWindow* m_info_window;

		/** popup menu widgets */
		Fl_Menu_Button* m_popup_menu;


		/** horizontal scrollbar */
		Fl_Scrollbar* m_horiz_scroll;

		/** vertical scrollbar */
		Fl_Scrollbar* m_vert_scroll;

		/** manages registerd menu helper functors */
		MenuFunctorHelper* m_menu_functor_helper;

		/** indicates if rulers should be displayed */
		bool m_rulers_visible;

		// @hack [hack] placement widgets for the rulers once implemented
		Fl_Box* m_ruler_origin;
		Fl_Box* m_horizontal_ruler;
		Fl_Box* m_vertical_ruler;


		//---------------
		// Nested Classes

		/**
		 * Nested class to handle window specific mouse motions events.
		 * ie handle popup menus, rulers position etc.
		 */
		class ImageWindowMouseHandler : public CinePaintMouseListener
		{
			public:
				ImageWindowMouseHandler(CinePaintImageWindow& image_window);
				~ImageWindowMouseHandler();

				virtual void MouseEntered();
				virtual void MouseExited();
				virtual void MousePressed(int button, int count, int x, int y, unsigned int modifier);
				virtual void MouseReleased(int button, int x, int y);
				virtual void MouseDragged(int button, int x, int y);
				virtual void MouseMoved(int x, int y);

			protected:

			private:
				void update_active_document();
				CinePaintImageWindow& m_image_window;
				bool m_focus_follows_mouse;

		}; /* class ImageWindowMotionHandler */

		ImageWindowMouseHandler* m_draw_area_mouse_handler;


		/**
		 * Nested class to handle widget events.
		 * In particular monitor the DrawArea for changes in size
		 */
		class DrawAreaWidgetListener : public CinePaintWidgetListener
		{
			public:
				DrawAreaWidgetListener(CinePaintImageWindow& image_window);
				~DrawAreaWidgetListener() {} ;

				virtual void WidgetResized(int w, int h);

			protected:
			private:
				CinePaintImageWindow& m_image_window;
		};

		DrawAreaWidgetListener* m_draw_area_widget_listener;


		/**
		 * Builds the Image Window Menu
		 *
		 * @param menu the menu to add items to
		 * @param menu_helper the menu functor helper for managing created menu functors
		 */
		void build_menu(Fl_Menu_Bar& menu, MenuFunctorHelper& menu_helper);

		/**
		 * Info window close callback - hides the window and defers destruction to the idle loop
		 *
		 * @param w the info window being destroyed
		 * @param data a pointer to the CinePaintImageWindow
		 */
		static void info_window_close_callback(Fl_Widget* w, void* data);

		/**
		 * horizontal scrollbar callback
		 * calls horiz_scrollbar_cb upon the specified instance of a CinePaintImageWindow
		 *
		 * @param w the scrollbar
		 * @param data an instance of a CinePaintImageWindow
		 */
		static void static_horiz_scrollbcr_cb(Fl_Widget* w, void* data) { reinterpret_cast<CinePaintImageWindow*>(data)->horiz_scrollbar_cb(); }

		/**
		 * Handles a horizontal scrollbar movement, updating the DrawArea's projection with the new offset value
		 *
		 */
		void horiz_scrollbar_cb();

		/**
		 * Vertical scrollbar callback
		 * calls vert_scrollbar_cb upon the specified instance of a CinePaintImageWindow
		 *
		 * @param w the scrollbar
		 * @param data an instance of a CinePaintImageWindow
		 */
		static void static_vert_scrollbcr_cb(Fl_Widget* w, void* data) { reinterpret_cast<CinePaintImageWindow*>(data)->vert_scrollbar_cb(); }

		/**
		 * Handles a vertical scrollbar movement, updating the DrawArea's projection with the new offset value
		 *
		 */
		void vert_scrollbar_cb();


		/** Selected tool proxy, monitors for changes to the current tool and forwards mouse events */
		ToolProxy m_tool_proxy;



		struct MenuSortHelper
		{
			std::string m_tool_path;
			const char* m_tool_type;
		};

		class MenuPathLess : public std::binary_function<const MenuSortHelper&, const MenuSortHelper&, bool>
		{
			public:
				result_type operator() (first_argument_type p, second_argument_type q) const { return(p.m_tool_path < q.m_tool_path); };
		};


}; /* class ImageWindow */

#endif /* _CINEPAINT_IMAGE_WINDOW_H_ */
