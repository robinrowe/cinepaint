/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      MainToolBar - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: MainToolbar.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _MAIN_TOOLBAR_H_
#define _MAIN_TOOLBAR_H_

#include "dll_api.h"
#include <app/ToolManager.h>
#include <FL/Fl_Window.H>

#include <map>
#include <string>

// dorward declaration
class CINEPAINT_GUI_API Fl_Window;
class MenuFunctorHelper;
class Fl_Button;
class Fl_Menu_Bar;

class CINEPAINT_GUI_API MainToolBar : public Fl_Window
{
public:
	MainToolBar();
	virtual ~MainToolBar();

private:

	/**
	 * Builds the toolbox buttons panel
	 */
	void build_tool_box();

	/**
	 * Build the main Menu bar for the toolbox window.
	**/
	void build_main_menu();

	/*********************
	 * Callback functions for the tool buttons.
	 *********************/

	 /**
	  * Callback for a generic tool button
	  * We deteremine what to do based upon the action command within the button
	  */
	void tool_button_cb(std::string tool_type);

 
	/**********************
	* The Member variables for the various buttons
	**********************/

	Fl_Menu_Bar *m_mMainMenu;

	/** manages the menu helper functors handling the menu callbacks */
	MenuFunctorHelper* m_menu_helper;


	/** tool bar buttons - we need to store a handle to each so we can set the currently selected */
	std::map<std::string, Fl_Button*> m_toolbar_btns;



	// Nested Classes

	/**
	 * ToolChangeListner for setting the selected tool button upon tool change events
	 *
	 */
	class ToolListener : public ToolManager::ToolChangeListener
	{
		public:
			ToolListener(MainToolBar& toolbar);
			virtual ~ToolListener();

			/**
			 * Invoked when the currently selected tool is changed
			 *
			 * @param tool_type the type of the newly selected tool
			 */
			virtual void ToolChanged(const char* tool_type);

		protected:
		private:
			MainToolBar& m_toolbar;
	};
	
	ToolListener* m_tool_change_listener;
};

#endif /* _MAIN_TOOLBAR_H_ */
