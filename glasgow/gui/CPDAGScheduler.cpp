/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CPDAGScheduler - Directed acyclic graph sequential node scheduler
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Stuart Ford
 *   Colin Law
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * $Id: CPDAGScheduler.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */
// created 05/11/04 Stuart Ford


/** @file
 * Class CPDAGScheduler source file
 */

#include "CPDAGScheduler.h"
#include "CPDAGNode.h"
#include "CPDAG.h"
#include <map>
#include <string>
#include <vector>



CPDAGScheduler::CPDAGScheduler()
{
	m_pGraph = 0;
	m_incomplete = 0;

} // default constructor


CPDAGScheduler::CPDAGScheduler(const CPDAG& graph)
{
	Schedule(graph);

} // constructor


//CPDAGScheduler::CPDAGScheduler(const CPDAGScheduler& it)
//: m_indegreeZeroQ(it.m_indegreeZeroQ), m_indegrees(it.m_indegrees)
//{
//
//copy(it, true);
//
//} // constructor


void CPDAGScheduler::Completed(const std::string& node)
{
	// remove this node from the iterator's internal representation of the tree
	// by decrementing the indegrees of nodes it was attached to. If nodes of
	// indegree zero are created by this action, push them onto the Q
	CPDAGNode* pnode = m_pGraph->m_nodes.find(node)->second;
	for(CPDAGNode::neightbour_iterator it = pnode->neighbours_begin(); it != pnode->neighbours_end(); ++it)
	{
		int degree = --m_indegrees[it->first];
		if(degree == 0)
		{
			m_indegreeZeroQ.push_back(it->first);
		}
	}

	m_incomplete--;

} // Completed


CPDAGScheduler& CPDAGScheduler::copy(const CPDAGScheduler &it, bool isConstructor)
{
	if(!isConstructor)
	{
		m_indegreeZeroQ = it.m_indegreeZeroQ;
		m_indegrees = it.m_indegrees;
	}

	m_pGraph = it.m_pGraph;
	m_incomplete = it.m_incomplete;

	return *this;

} // copy


void CPDAGScheduler::init_indegrees()
{
	std::map<std::string, CPDAGNode *>::const_iterator it;

	for (it = m_pGraph->m_nodes.begin(); it != m_pGraph->m_nodes.end(); ++it)
	{
		m_indegrees[it->first] = it->second->get_indegree();
		if(it->second->get_indegree() == 0)
		{
			m_indegreeZeroQ.push_back(it->first);
		}
	}

} // init_indegrees


bool CPDAGScheduler::IsEmpty()
{
	std::map<const std::string, int>::iterator it;

	for(it = m_indegrees.begin(); it != m_indegrees.end(); ++it)
	{
		if(it->second != 0)
		{
			return false;
		}
	}

	return m_indegreeZeroQ.empty();

} // IsEmpty


int CPDAGScheduler::IncompleteJobs(void)
{
	return m_incomplete;

} // incompleteJobs


bool CPDAGScheduler::Next(std::string& node)
{
	if (m_indegreeZeroQ.empty())
	{
		node = "";
		return false;
	}

	// pop the next node with an indegree of zero from the Q
	node = m_indegreeZeroQ.back();
	m_indegreeZeroQ.pop_back();
	m_incomplete++;

	return true;

} // Next


CPDAGScheduler& CPDAGScheduler::operator=(const CPDAGScheduler& rhs)
{
	if (this == &rhs)
	{
		return *this;
	}

	return copy(rhs, false);

} // operator=


void CPDAGScheduler::Schedule(const CPDAG& graph)
{
	m_pGraph = &graph;
} // Schedule
