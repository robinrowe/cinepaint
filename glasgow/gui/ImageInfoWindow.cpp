/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ImageInfoWindow - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ImageInfoWindow.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include <gui/ImageInfoWindow.h>

#include <gui/CinePaintDrawArea.h>
#include <gui/UIUtils.h>

#include <plug-ins/pdb/CinePaintImage.h>

#include <stdio.h>

// maximum number of textual characters we can display
static int MAX_BUFFER_SIZE = 32;

ImageInfoWindow::ImageInfoWindow(int w, int h, const char* title)
: Fl_Window(w, h, title)
{
	UIUtils::LayoutHelper _layout_helper;
	int _num_row = 4;
	int _dummy_row_height = (this->h() - ((_num_row -1) * UIUtils::DEFAULT_Y_PAD)) / _num_row;
	int _real_row_height = h < 0 ? UIUtils::DEFAUTL_ROW_HEIGHT : _dummy_row_height;

	_layout_helper.m_suggested_width = (w - UIUtils::DEFAULT_X_PAD) / 2;

	Fl_Box* _dimension_label = UIUtils::CreateLabel(_layout_helper.m_posx, _layout_helper.m_posy, _layout_helper.m_suggested_width, UIUtils::DEFAUTL_ROW_HEIGHT, "Dimensions");
	m_dimension_label = UIUtils::CreateLabel(_layout_helper.m_posx + _layout_helper.m_suggested_width + UIUtils::DEFAULT_X_PAD, _layout_helper.m_posy, _layout_helper.m_suggested_width, UIUtils::DEFAUTL_ROW_HEIGHT, "");
	_layout_helper.m_posy += UIUtils::DEFAULT_Y_PAD + UIUtils::DEFAUTL_ROW_HEIGHT;

	Fl_Box* _scale_label = UIUtils::CreateLabel(_layout_helper.m_posx, _layout_helper.m_posy, _layout_helper.m_suggested_width, UIUtils::DEFAUTL_ROW_HEIGHT, "Scale Rstio");
	m_scale_label = UIUtils::CreateLabel(_layout_helper.m_posx + _layout_helper.m_suggested_width + UIUtils::DEFAULT_X_PAD, _layout_helper.m_posy, _layout_helper.m_suggested_width, UIUtils::DEFAUTL_ROW_HEIGHT, "");
	_layout_helper.m_posy += UIUtils::DEFAULT_Y_PAD + UIUtils::DEFAUTL_ROW_HEIGHT;

	Fl_Box* _position_label = UIUtils::CreateLabel(_layout_helper.m_posx, _layout_helper.m_posy, _layout_helper.m_suggested_width, UIUtils::DEFAUTL_ROW_HEIGHT, "Position (x,y)");
	m_position_label = UIUtils::CreateLabel(_layout_helper.m_posx + _layout_helper.m_suggested_width + UIUtils::DEFAULT_X_PAD, _layout_helper.m_posy, _layout_helper.m_suggested_width, UIUtils::DEFAUTL_ROW_HEIGHT, "");
	_layout_helper.m_posy += UIUtils::DEFAULT_Y_PAD + UIUtils::DEFAUTL_ROW_HEIGHT;

	Fl_Box* _value_label = UIUtils::CreateLabel(_layout_helper.m_posx, _layout_helper.m_posy, _layout_helper.m_suggested_width, UIUtils::DEFAUTL_ROW_HEIGHT, "Value at (x,y)");
	m_value_label = UIUtils::CreateLabel(_layout_helper.m_posx + _layout_helper.m_suggested_width + UIUtils::DEFAULT_X_PAD, _layout_helper.m_posy, _layout_helper.m_suggested_width, UIUtils::DEFAUTL_ROW_HEIGHT, "");
	_layout_helper.m_posy += UIUtils::DEFAULT_Y_PAD + UIUtils::DEFAUTL_ROW_HEIGHT;

	this->size(w, (_real_row_height * _num_row) + ((_num_row -1) * UIUtils::DEFAULT_Y_PAD));
	this->init_sizes();

	this->end();



	// create the label buffers
	m_dimension_buf = new char[MAX_BUFFER_SIZE + 1];
	m_scale_buf = new char[MAX_BUFFER_SIZE + 1];
	m_position_buf = new char[MAX_BUFFER_SIZE + 1];
	m_value_buf = new char[MAX_BUFFER_SIZE + 1];

	m_drawarea = 0;
}

ImageInfoWindow::~ImageInfoWindow()
{
	if(m_dimension_buf)
	{
		delete[] m_dimension_buf;
		m_dimension_buf = 0;
	}

	if(m_scale_buf)
	{
		delete[] m_scale_buf;
		m_scale_buf = 0;
	}

	if(m_position_buf)
	{
		delete[] m_position_buf;
		m_position_buf = 0;
	}

	if(m_value_buf)
	{
		delete[] m_value_buf;
		m_value_buf = 0;
	}
}

/**
 * Sets the CinePaintImage And CinePaintDrawArea this ImageInfoWindow provides information from.
 * If both are set to 0, this ImageInfoWindow no longer tracks and display image info.
 * 
 * @param image the CinePaintImage we this ImageInfoWindow display information from
 * @param draw_area the CinePaintDrawArea this ImageInfoWindow monitors for mouse movement
 */
void ImageInfoWindow::SetImage(CinePaintImage* image, CinePaintDrawArea* draw_area)
{
	m_image = image;
	m_drawarea = draw_area;

	update_dimensions();
}

void ImageInfoWindow::MouseEntered()
{}

void ImageInfoWindow::MouseExited()
{
	update_position(-1, -1);
}

void ImageInfoWindow::MousePressed(int button, int count, int x, int y, unsigned int modifier)
{}

void ImageInfoWindow::MouseReleased(int button, int x, int y)
{}

void ImageInfoWindow::MouseDragged(int button, int x, int y)
{
	update_position(x, y);
	update_image_value(x, y);
}

void ImageInfoWindow::MouseMoved(int x, int y)
{
	update_position(x, y);
	update_image_value(x, y);
}

/**
 * Update the image dimension display from the set CinepaintImage data
 *
 */
void ImageInfoWindow::update_dimensions()
{
	if(m_image)
	{
		sprintf(m_dimension_buf, "%d x %d", m_image->GetWidth(), m_image->GetHeight());
		m_dimension_label->label(m_dimension_buf);
		m_dimension_label->redraw();
	}
}

/**
 * Updates the current position display with the specified values
 *
 * @param x the x position
 * @param y the y position
 */
void ImageInfoWindow::update_position(int x, int y)
{
	if((x >= 0) && (y >= 0))
	{
		sprintf(m_position_buf, "%d x %d", x, y);
	}
	else
	{
		sprintf(m_position_buf, "Off Image");
	}
	
	m_position_label->label(m_position_buf);
	m_position_label->redraw();
}

/**
 * Updates the current image value at the specified location
 *
 * @param x the x position
 * @param y the y position
 */
void ImageInfoWindow::update_image_value(int x, int y)
{
	// @TODO add this somewhere ...
	//       ... to CinePaintDrawArea or CinePaintImage ... or we need an AbstractBuf?
}

