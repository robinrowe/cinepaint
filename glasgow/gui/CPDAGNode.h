/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CPDAGNode - Directed acyclic graph node
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Stuart Ford
 *   Colin Law
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * $Id: CPDAGNode.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CPDAGNODE_H_
#define _CPDAGNODE_H_

#include "dll_api.h"
#include <utility/CPSerializable.h>
#include <string>
#include <map>

class CPSocket;
class CPDAGData;
class CPPluginParamDef;
class PluginParam;

/**
 * A representation of a node or vertex in a directed acyclic graph. Each node
 * has a piece of data, and a list of immmediately neighbouring nodes that can
 * be travelled to from it. Objects of this class are created and manipulated
 * from within the CPDAG class - they are not intended to be publicly exposed,
 * only to simplify the implementation of CPDAG.
 */
class CINEPAINT_CORE_API CPDAGNode : public CPSerializable
{
		//--------
		// Friends
		friend class CPDAG;
		friend class CPDAGIterator;
		friend class CPDAGScheduler;
		friend class CPDAGEvaluator;

	public:
		/**
		 * Destructor.
		 */
		virtual ~CPDAGNode();

		/** default name of the default evaluation */
		static const std::string DEFAULT_EVALUATION_NAME;

		//---------------------
		// Accessors / Mutators

		/**
		 * Returns the data held by this Node
		 *
		 * @return the data held by this Node
		 */
		const CPDAGData& GetData() const;

		/**
		 * Returns the name of the default evaluation held by this Node
		 *
		 * @return the name of the default evaluation held by this Node
		 */
		const std::string& GetDefaultEvaluationName() const;

		/**
		 * Returns the dfault evaluation held by this Node
		 * If there is no default evaluation, 0 is returned.
		 * Althouh there may be no default evaluation, this does not mean that there
		 * are no evalautions. The default evaluation name may simply not map to an
		 * evaluated value
		 *
		 * @return the default evaluation held by this Node
		 */
		const PluginParam* GetEvaluation() const;

		/**
		 * Returns the named evaluation held by this node
		 * If the named evaluation does not exist, 0 is returned
		 *
		 * @param eval_name the name of the evaluation value to return
		 * @return the named evaluation held by this Node, or 0 is the named evaluation
		 *         does not exist
		 */
		const PluginParam* GetEvaluation(const std::string& eval_name) const;


		/**
		 * Returns whether this CPDAGNode is dirty.
		 * A dirty CPDAGData is one which has had data, or an evaluation changed, but has not been
		 * evaluated. This value is controlled by the parent CPDAG during evaluation
		 *
		 * @return true if this CPDAGNode is dirty, false otherwise
		 */
		bool IsDirty() const;

		/**
		 * Serialize and write a node to a given socket.
		 * @param socket	[i] the socket to write the node to
		 */
		void Serialize(CPSocket& socket) const;

		/**
		 * Read and deserialize a node from a given socket.
		 * @param socket	[i] the socket to read the node from
		 */
		void Deserialize(CPSocket& socket);

	private:

		typedef std::pair<std::string, std::string> ParamEvalPair_t;
		typedef std::multimap<std::string, ParamEvalPair_t > NeighbourContainer_t;
		typedef std::map<std::string, PluginParam*> EvalulationContainer_t;

	protected:

		/**
		 * Protected Default constructor.
		 * Generally, Nodes should only be created by a controlling CPDAG.
		 *
		 */
		CPDAGNode();

		/**
		 * Returns the data held by this Node
		 *
		 * @return the data held by this node
		 */
		CPDAGData& get_data();

		typedef NeighbourContainer_t::iterator neightbour_iterator;
		typedef EvalulationContainer_t::iterator eval_iterator;


		// attachment handling

		/**
		 * Attach a node to this one.
		 * The attach direction is from this Node to the specified node one.
		 * A node cannot be attached to itself.
		 *
		 * Two names are associated with the attachment, the parameter name and the evaluation name.
		 * the parameter name represents the name of the parameter the attached node is providing
		 * to an evaluation of a plugin. Dring the evaluation of a node, there may be multiple values
		 * returned. These values represent the evaluations of the node. The eval name represents the
		 * od the evluatioed value that the attached node is providing.
		 *
		 * @param node the node to attach
		 * @param param the parameter name,
		 * @param eval the evaluation name of the parameter linking from the specified node
		 * @see #CPDAG::Attach
		 */
		void attach(const std::string& node, const std::string& param, const std::string& eval);

		/**
		 * Detach the named node from this node
		 * If the specified node is not attached to this node, no action is taken and the
		 * detach is considered successful.
		 *
		 * @param node the node to detach
		 */
		void detach(const std::string& node, const std::string& param, const std::string& eval);

		/**
		 * Completely detach the named node from this node
		 * If the specified node is not attached to this node, no action is taken and the
		 * detach is considered successful.
		 *
		 * @param node the node to detach
		 */
		void detach(const std::string& node);

		/**
		 * Detach all nodes from this node.
		 *
		 */
		void detach_all();



		// neighbour handling

		/**
		 * Returns an iterator pointing to the specified attched node
		 * If the specified node is not attached to this node, the iterator will be
		 * equal to neighbours_end()
		 * a node may be attached multiple times, see neighbours_range
		 *
		 * @param node_name the name of the attched node
		 * @return an iterator pointing the attched node
		 */
		neightbour_iterator find_neighbour(const std::string& node_name);

		/**
		 * Returns an iterator pointing to the beginning of the neighbouring nodes
		 *
		 * @return an iterator pointing to the beginning of the neighbouring nodes
		 */
		neightbour_iterator neighbours_begin();

		/**
		 * Returns an iterator pointing past the end of the neighbouring nodes
		 *
		 * @return an iterator pointing past the end of the neighbouring nodes
		 */
		neightbour_iterator neighbours_end();

		/**
		 * Returns a pair of iterators representing the start and end of nodes attahced by the specified name
		 * A node may be attached several times to this node, for example if a particular node provides
		 * multiple parameters to this node, therefore the attached node name may not be unique.
		 * This method returns a pair of iterators representing the start and end range of the
		 * named node within the neighbouring nodes.
		 *
		 * @param node_name the attached node name
		 * @return pair of iterators representing the start and end range
		 */
		std::pair<neightbour_iterator, neightbour_iterator> neighbours_range(const std::string& node_name);


		// evaluation handling

		/**
		 * Sets the name of default evaluation value to the specified name
		 *
		 * @param name the name of the default evaluation
		 */
		void set_default_evaluation(const std::string& name);

		/**
		 * Adds the specified value to the list of evaluation values held by this node.
		 * 
		 * @param eval_name the name ampping to the evaluation value
		 * @param param the evaluation value to set
		 */
		bool set_evalulation(const std::string& eval_name, PluginParam* param);

		/**
		 * Returns the named evaluation held by this node
		 * If the named evaluation does not exist, 0 is returned
		 *
		 * @param eval_name the name of the evaluation value to return
		 * @return the named evaluation held by this Node, or 0 is the named evaluation
		 *         does not exist
		 */
		PluginParam* get_evaluation(const std::string& eval_name);

		/**
		 * Clears the evaluations list
		 *
		 */
		void clear_evaluations();

		/**
		 * Returns an interator at the beginning of the evaluation values held by this Node
		 *
		 * @return an iterator at the beginning of the evaluation calues held by this node
		 */
		eval_iterator evaulations_begin();

		/**
		 * Returns an interator past the end of the evaluation values held by this Node
		 *
		 * @return an iterator past the end of the evaluation calues held by this node
		 */
		eval_iterator evaulations_end();




		// indegree handling

		/**
		 *  Sets the indegree of this Node
		 * The indegree is the count of the number of paths into this node
		 * i.e. the number of nodes that connect to this one.
		 *
		 * @param i the nre indegree of this Node
		 */
		void set_indegree(int i);

		/**
		 * Increments the indegree of this node by one
		 *
		 */
		void increment_indegree();

		/**
		 * decremenets the indegree of this node by one
		 *
		 */
		void decrement_indegree();

		/**
		 * Returns the indegree of this node
		 *
		 * @return the indegree of this node
		 */
		int get_indegree() const;




		/**
		 * Sets the dirty state of this Node
		 * A dirty CPDAGData is one which has had data, or an evaluation changed, but has not been
		 * evaluated. This value is controlled by the parent CPDAG during evaluation
		 *
		 * @param dirty the new dirty state.
		 */
		void set_dirty(bool dirty);

	private:

		/**
		 * Container for the immediately neighbouring nodes.
		 * Each elements is a mapping of the node name to the parameter name and evaluation name.
		 */
		NeighbourContainer_t m_neighbours;

		/** container of results of evaulations from this node */
		EvalulationContainer_t m_evaulations;

		/** name of the default evaulation held by this node */
		std::string m_default_evaluation;

		/** The data object held in the node */
		CPDAGData* m_data;



		/** Indicates whether this node is dirty and needs evaluated. */
		bool m_dirty;

		/** count of the number of paths into this node */
		int m_indegree;

};

#endif /* _CPDAGNODE_H_ */
