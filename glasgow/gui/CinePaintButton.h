/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                     CinePaintButton - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintButton.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CINEPAINT_BUTTON_H_
#define _CINEPAINT_BUTTON_H_

#include "dll_api.h"
#include <FL/Fl_Button.H>
#include <string>

/**
 * CinePaintButton defines an extended Fl_Button to hold action data.
 * Sepcifically, CinePaintButton has an additional action command associated with it
 * which may be queried to determine the intended action of the button.
 *
 */
class CINEPAINT_GUI_API CinePaintButton : public Fl_Button
{
	public:
		/**
		 * Constrcusta new CinePaintButton with the specified action command
		 *
		 * @param x the x position of the widget
		 * @param y the y position of the widget
		 * @param w the width of the widget
		 * @param h the height of the widget
		 * @param label text label for the button
		 * @param action_cmd action command associaited with this CinePaintButton
		 */
		CinePaintButton(int x, int y, int w, int h, const char* label, const char* action_cmd);

		/**
		 * Destructor
		 */
		virtual ~CinePaintButton();

		/**
		 * Sets the action command of this CinePaintButton to the specified string
		 * The specified data is not managed by CinePaintButton so must be allocated by the caller in
		 * a suitable manner
		 *
		 * @param sction_smd the new action command of this CinePaintButton
		 */
		void SetActionCommand(const char* action_cmd);

		/**
		 * Return sthe action command of this CinePaintButton
		 *
		 * @return the action command of this CinePaintButton
		 */
		const char* GetActionCommand() const;

	protected:

	private:
		/** the action command string */
		std::string m_action_cmd;

} ; /* class CinePaintButton */

#endif /* _CINEPAINT_BUTTON_H_ */
