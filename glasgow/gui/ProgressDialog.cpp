/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ProgressDialog - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * 
 * $Id: ProgressDialog.cpp,v 1.2 2006/12/18 08:21:17 robinrowe Exp $
 */

#include <gui/ProgressDialog.h>
#include <gui/CinePaintAppUI.h>
#include <string>

#include <FL/Fl_Progress.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Window.H>

ProgressDialog::ProgressDialog(char* ID)
{ 
	strncpy(m_ID, ID, sizeof(m_ID));
	m_pProgressWindow = 0;
	m_pProgressBar = 0;
	
	pthread_t self = pthread_self();

	if ( pthread_equal(CinePaintAppUI::GetInstance().GetUIThreadID(), self) )
		m_main_thread = true;
	else
		m_main_thread = false;
}

ProgressDialog::~ProgressDialog()
{

}
void ProgressDialog::CreateProgressDialog(char* text, int max, int step)
{
	if (m_main_thread)
        create_progress_dialog(text, max, step);
	else
        CinePaintAppUI::GetInstance().GetUIThreadEventList().PostEvent(new NewProgressEvent(this, text, max, step));

}

void ProgressDialog::SetText(char* txt)
{
}

void ProgressDialog::ResetProgress(int max, int step)
{
}

void ProgressDialog::UpdateProgress()
{
	if(m_main_thread)
		update_progress();
	else
        CinePaintAppUI::GetInstance().GetUIThreadEventList().PostEvent(new UpdateProgressEvent(this));
}

void ProgressDialog::SetProgress(int pos)
{
}

bool ProgressDialog::Hide()
{
		if (m_main_thread)
			hide();
		else
            CinePaintAppUI::GetInstance().GetUIThreadEventList().PostEvent(new ProgressCompleteEvent(this));
		return m_main_thread;

}

//=======================================
// Private methods to actually make things happen.

void ProgressDialog::create_progress_dialog(char* text, int max, int step)
{
	m_iProgressPos = 0;
	m_iProgressMax = max;
	m_iProgressStep = step;
	strcpy(m_sText, text);
	m_pProgressBar = 0;

	m_pProgressWindow = new Fl_Window(100, 50, m_sText);

	m_pProgressBar = new Fl_Progress(0, 5, 100, 20);
	m_pProgressBar->maximum(float(max));
	m_pProgressBar->value(0.0f);

	m_pProgressWindow->end();
	m_pProgressWindow->show();
}
void ProgressDialog::set_text(char* text)
{
	strcpy(m_sText, text);
}
void ProgressDialog::reset_progress(int max, int step)
{
	if (m_pProgressBar)
	{
		m_pProgressBar->maximum((float)max);
		m_pProgressBar->value((float)0);
		Fl::wait();
		m_iProgressMax = max;
		m_iProgressStep = step;
		m_iProgressPos = 0;
	}
}
void ProgressDialog::update_progress()
{
	if (m_pProgressBar)
	{
		m_iProgressPos += m_iProgressStep;
		m_pProgressBar->value((float)m_iProgressPos);
		Fl::wait();
	}
}
void ProgressDialog::set_progress(int pos)
{
	m_iProgressPos = pos;
	if (m_pProgressBar)
	{
		m_pProgressBar->value((float)m_iProgressPos);
		Fl::wait();
	}
}
void ProgressDialog::hide()
{
	if (m_pProgressWindow)
	{
		m_pProgressWindow->hide();
		m_pProgressWindow = 0;
	}
}


//=============================
// Progress Dialog Listener Event Implementations


ProgressDialog::ProgressListener::ProgressListener()
{
}
ProgressDialog::ProgressListener::~ProgressListener()
{
}

void ProgressDialog::ProgressListener::ProgressStart(char* ID, char* msg, int stop, int step)
{
	ProgressDialog *_dlg = new ProgressDialog(ID);
	m_dlgList.push_back(_dlg);
	
	_dlg->CreateProgressDialog(msg, stop, step);
}
void ProgressDialog::ProgressListener::MessageChanged(char* ID,  char* msg )
{
}
void ProgressDialog::ProgressListener::LimitsChanged(char* ID, int start, int stop, int step)
{
}
void ProgressDialog::ProgressListener::ProgressUpdate(char* ID)
{
	ProgressDialog *_dlg = find_dlg(ID);
	if (_dlg)
			_dlg->UpdateProgress();
}
void ProgressDialog::ProgressListener::ProgressReset(char* ID)
{
}
void ProgressDialog::ProgressListener::ProgressPosition(char* ID, int pos)
{
}
void ProgressDialog::ProgressListener::ProgressComplete(char* ID)
{
	bool _del;
	ProgressDialog *_dlg = find_dlg(ID);
	if (_dlg)
	{
		_del = _dlg->Hide();
		m_dlgList.remove(_dlg);
		if (_del)
			delete _dlg;
	}

}

ProgressDialog *ProgressDialog::ProgressListener::find_dlg(char* ID)
{
	std::list<ProgressDialog *>::iterator iter = m_dlgList.begin();
	bool found = false;
	while (!found && iter != m_dlgList.end() )
	{	
		found = strcmp((*iter)->GetID(), ID) == 0;
		if (!found)
			++iter;
	}
	if (found)
		return (*iter);
	else
		return 0;
}
//=============================
// Dialog Event Implementations

// -----------------------------------------
// New Progress Window Event Implementation.
ProgressDialog::NewProgressEvent::NewProgressEvent( ProgressDialog *dlg, char* msg, int max, int step )
:CinePaintEvent(true), m_dlg(dlg), m_msg(msg), m_max(max), m_step(step)
{

}

void ProgressDialog::NewProgressEvent::Process()
{
	m_dlg->create_progress_dialog(m_msg, m_max, m_step);
}

// -----------------------------------------
// Update Progress Window Event Implementation.
ProgressDialog::UpdateProgressEvent::UpdateProgressEvent( ProgressDialog *dlg)
:CinePaintEvent(true), m_dlg(dlg)
{
}

void ProgressDialog::UpdateProgressEvent::Process()
{
	m_dlg->update_progress();
}


// -----------------------------------------
// Close Progress Window Event Implementation.
ProgressDialog::ProgressCompleteEvent::ProgressCompleteEvent( ProgressDialog *dlg)
:CinePaintEvent(true), m_dlg(dlg)
{
}

void ProgressDialog::ProgressCompleteEvent::Process()
{
	m_dlg->hide();
	delete m_dlg;
}
