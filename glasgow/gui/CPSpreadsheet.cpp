/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CPSpreadsheet - Spreadsheet for images
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * $Id: CPSpreadsheet.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */


#include "CPSpreadsheet.h"
#include "CPDAG.h"
#include "CPDAGNode.h"
#include <utility/PlatformUtil.h> // req. for snprintf
#include <map>
#include <set>
#include <string>


const char* CPSpreadsheet::mc_LinkSeparator = ":";
const char* CPSpreadsheet::mc_CellSeparator = ":";



//-------------------------
// Constructor / Destructor

/**
 * Default constructor
 */
CPSpreadsheet::CPSpreadsheet()
{
	m_graph = 0;

	m_selection.clear();
	m_clipboard.clear();
}

/**
 * Destructor
 */
CPSpreadsheet::~CPSpreadsheet()
{}




//---------------------
// Accessors / Mutators

/**
 * Sets the CPDAG this CPSpreadsheet acts as an interface to
 *
 * @param dag the CPDAG to interface to
 */
void CPSpreadsheet::SetDAG(CPDAG& dag)
{
	m_graph = &dag;
}

/**
 * Returns the CPDAG this CPSpreadsheet interfaces to
 *
 * @return the CPDAG this CPSpreadsheet interface to.
 */
CPDAG* CPSpreadsheet::GetDAG()
{
	return(m_graph);
}

const CPDAG* CPSpreadsheet::GetDAG() const
{
	return(m_graph);
}



//-----------------
// Cell/Data Access

/**
 * Returns the data held in the specified cell of the spreadsheet.
 * If the specified cell does not contain any data, 0 is returned
 *
 * @param row the row witihn the spreadsheet
 * @param col the column within the spreadsheet
 * @return the data held within the specified cell, or 0
 */
const CPDAGData* CPSpreadsheet::GetCellData(int row, int col)
{
	const CPDAGData* ret = 0;

	if(m_graph)
	{
		std::string cell_name = CPSpreadsheet::CellName(row, col);
		ret = m_graph->GetData(cell_name);
	}

	return(ret);
}

/**
 * Returns the data held in the specified cell of the spreadsheet.
 * If the specified cell does not contain any data, 0 is returned
 * Thie method takes the row and column numbers as a single string
 * representing the cell name
 *
 * @param cell the spreadsheet cell "rowcol"
 * @return the data held within the specified cell, or 0
 */
const CPDAGData* CPSpreadsheet::GetCellData(const std::string& cell)
{
	const CPDAGData* ret = 0;

	if(m_graph)
	{
		ret = m_graph->GetData(cell);
	}

	return(ret);
}

/**
 * Returns the graph node backing the spreadsheet at the specified spreadsheet cell location.
 * If the specified cell does not contain any data, 0 is returned
 *
 * @param row the row witihn the spreadsheet
 * @param col the column within the spreadsheet
 * @return the graph node backing the spreadsheet at the specified cell, or 0
 */
const CPDAGNode* CPSpreadsheet::GetCellNode(int row, int col) const
{
	const CPDAGNode* data = 0;

	if(m_graph)
	{
		std::string cell_name = CellName(row, col);
		data = m_graph->GetNode(cell_name);
	}

	return(data);
}




//--------------------
// Cell Data Modifiers

/**
 * Set the contents of a cell to an integer.
 *
 * @param row the row the cell is in
 * @param col the column the cell is in
 * @param data the integer to store in the cell
 */
void CPSpreadsheet::SetCell(int row, int col, int data)
{
	if(m_graph)
	{
		std::string cell_name = CellName(row, col);
		m_graph->Set(cell_name, data);
	}
}

/**
 * Set the contents of a cell to a floating point number.
 *
 * @param row the row the cell is in
 * @param col the column the cell is in
 * @param data the number to store in the cell
 */
void CPSpreadsheet::SetCell(int row, int col, float data)
{
	if(m_graph)
	{
		std::string cell_name = CellName(row, col);
		m_graph->Set(cell_name, data);
	}
}

/**
 * Set the contents of a cell to a string value
 * The string value is copied by the graph structure backing the spreadsheet
 *
 * @param row the row the cell is in
 * @param col the column the cell is in
 * @param data the string to store in the cell
 */
void CPSpreadsheet::SetCell(int row, int col, const char* data)
{
	if(m_graph)
	{
		std::string cell_name = CellName(row, col);
		m_graph->Set(cell_name, data);
	}
}

/**
 * Set the contents of a cell to an AbstractBuf.
 *
 * @param row the row the cell is in
 * @param col the column the cell is in
 * @param data the image to store in the cell
 */
void CPSpreadsheet::SetCell(int row, int col, AbstractBuf* data)
{
	if(m_graph)
	{
		std::string cell_name = CellName(row, col);
		m_graph->Set(cell_name, data);
	}
}

/**
 * Set the contents of a cell to a plugin name.
 * The plugin name is copied by the graph structure backing the spreadsheet
 *
 * @param row the row the cell is in
 * @param col the column the cell is in
 * @param plugin the name of the plugin to store in the cell
 */
void CPSpreadsheet::SetPluginCell(int row, int col, const char* plugin)
{
	if(m_graph)
	{
		std::string cell_name = CellName(row, col);
		m_graph->SetPlugin(cell_name, plugin);
	}
}





//-------------
// Cell Linking

/**
 * Connects two cells in the graph backing this spreadsheet.
 * The connection of nodes joins the parameter input to a plugin, to the plugin that requires the input
 * parameters.
 * In order to correctly connect cells, the cells must exist. the create_missing flag may be used
 * to automatically create parameter cell and populate them with default data. The default data used
 * is a string representing the parameter name. Plugin cells are not automatically created.
 *
 * @param row the parameter cell row
 * @param col the parameter cell col
 * @param ref_row the plugin cell row
 * @param abs_row indicates if the row is absolute
 * @param ref_col the plugin cell col
 * @param abs_col indicates if the column is absolute
 * @param param the parameter name
 * @param eval_name the evaluation name of the parameter value within the parameter cell
 * @param create_missing set true to create automatically missing cells
 */
void CPSpreadsheet::Reference(int row, int col, int ref_row, bool abs_row, int ref_col, bool abs_col, const std::string& param, const std::string& eval_name, bool create_missing)
{
	Reference(CellName(row, col), CellName(ref_row, ref_col), abs_row, abs_col, param, eval_name, create_missing);
}

/**
 * Connects two cells in the graph backing this spreadsheet.
 * The connection of nodes joins the parameter input to a plugin, to the plugin that requires the input
 * parameters.
 * In order to correctly connect cells, the cells must exist. the create_missing flag may be used
 * to automatically create parameter cell and populate them with default data. The default data used
 * is a string representing the parameter name. Plugin cells are not automatically created.
 *
 * @param cell the parameter cell
 * @param ref_cell the plugin cell
 * @param abs_row indicates if the row is absolute
 * @param abs_col indicates if the column is absolute
 * @param param the parameter name
 * @param eval_name the evaluation name of the parameter value within the parameter cell
 * @param create_missing set true to create automatically missing cells
 */
void CPSpreadsheet::Reference(const std::string& cell, const std::string& ref_cell, bool abs_row, bool abs_col, const std::string& param, const std::string& eval_name, bool create_missing)
{
	if(m_graph && m_graph->Exists(ref_cell))
	{
		if(!m_graph->Exists(cell) && create_missing)
		{
			// if the referenced node does not exist create a new node
			m_graph->NewNode(cell, param.c_str());
		}

		m_graph->Attach(cell, ref_cell, param, eval_name, abs_row, abs_col);
	}
}


/**
 * Disconnects two cells in the graph backing this spreadsheet.
 *
 * @see Reference(int, int, int, bool, int, bool, const std::string&, const std::string&, bool);
 * @param row the parameter cell row
 * @param col the parameter cell col
 * @param ref_row the plugin cell row
 * @param abs_row indicates if the row is absolute
 * @param ref_col the plugin cell col
 * @param abs_col indicates if the column is absolute
 * @param param the parameter name
 * @param eval_name the evaluation name of the parameter value within the parameter cell
 */
void CPSpreadsheet::UnReference(int row, int col, int ref_row, bool abs_row, int ref_col, bool abs_col, const std::string& param, const std::string& eval_name)
{
	UnReference(CellName(row, col), CellName(ref_row, ref_col), abs_row, abs_col, param, eval_name);
}

/**
 * Disconnects two cells in the graph backing this spreadsheet.
 *
 * @see Reference(const std::string&, const std::string&, bool, bool, const std::string&, const std::string&, bool);
 * @param cell the parameter cell
 * @param ref_cell the plugin cell
 * @param abs_row indicates if the row is absolute
 * @param abs_col indicates if the column is absolute
 * @param param the parameter name
 * @param eval_name the evaluation name of the parameter value within the parameter cell
 */
void CPSpreadsheet::UnReference(const std::string& cell, const std::string& ref_cell, bool abs_row, bool abs_col, const std::string& param, const std::string& eval_name)
{
	if(m_graph && m_graph->Exists(cell) && m_graph->Exists(ref_cell))
	{
		m_graph->Detach(cell, ref_cell, param, eval_name, abs_row, abs_col);
	}
}


//---------------
// Data Selection

/**
 * Select a single cell.
 *
 * @param row the row the cell is in
 * @param col the column the cell is in
 */
void CPSpreadsheet::Select(int row, int col)
{
	m_selection.set(row, col, row, col);
}

/**
 * Select a block of cells. A block is defined by 2 cells in opposite
 * corners of the block. Either pair of opposing corners is allowed, and the
 * corners may be in any order. Note that the ordering of corners is
 * significant in some operations, filling for example.
 *
 * @param row1 the row of the the first selected cell
 * @param col1 the column of the first selected cell
 * @param row2 the row of the last selected cell
 * @param col2 the column of the last selected cell
 */
void CPSpreadsheet::Select(int row1, int col1, int row2, int col2)
{
	m_selection.set(row1, col1, row2, col2);
}

/**
 * Select an entire row of cells.
 *
 * @param row the row to select
 */
void CPSpreadsheet::SelectRow(int row)
{
	m_selection.set(row, -1, row, -1);
}

/**
 * Select a number of contiguous rows of cells.
 *
 * @param row1 the first row in the selection
 * @param row2 the last row in the selection
 */
void CPSpreadsheet::SelectRows(int row1, int row2)
{
	m_selection.set(row1, -1, row2, -1);
}

/**
 * Select an entire column of cells.
 *
 * @param col the column to select
 */
void CPSpreadsheet::SelectCol(int col)
{
	m_selection.set(-1, col, -1, col);
}

/**
 * Select a number of contiguous columns of cells.
 *
 * @param col1 the first column in the selection
 * @param col2 the last column in the selection
 */
void CPSpreadsheet::SelectCols(int col1, int col2)
{
	m_selection.set(-1, col1, -1, col2);
}






//--------------------
// Spreadsheet actions

// @TODO implement these methods

/**
 * Erases the contents of the current selection.
 *
 * @return the number of cells erased
 */
//int CPSpreadsheet::Erase();

/**
 * Erases the contents of a single cell.
 *
 * @param row the row number of the cell to erase
 * @param col the column number of the cell to erase
 * @return the number of cells erased
 */
int CPSpreadsheet::Erase(int row, int col)
{
	int ret = 0;

	if(m_graph)
	{
		std::string cell = CellName(row, col);
		if(m_graph->Exists(cell))
		{
			if(m_graph->Remove(cell))
			{
				ret = 1;
			}
		}
	}

	return(ret);
}

/**
 * Cut the contents of the current selection and put them in the clipboard selection.
 *
 * @return the number of cells cut
 */
//int CPSpreadsheet::Cut();

/**
 * Cut the contents of the specified cell and add it to the clipboard selection.
 *
 * @return the number of cells cut
 */
//int CPSpreadsheet::Cut(int row, int col);

/**
 * Copy the contents of the current selection to the clipboard selection.
 *
 * @return the number of cells copied
 */
//int CPSpreadsheet::Copy();

/**
 * Copy the contents of the specified cell and add it to the clipboard selection.
 *
 * @return the number of cells copied
 */
//int CPSpreadsheet::Copy(int row, int col);

/**
 * Paste the contents of the clipboard selection to the current selection.
 *
 * @return the number of cells pasted
 */
//void CPSpreadsheet::Paste();

/**
 * Paste the contents of the clipboard selection into the specified cell.
 *
 * @return the number of cells pasted
 */
//void CPSpreadsheet::Paste(int row, int col);






//----------------------------
// Display Convenience Methods

/**
 * Rebuilds the complete plugin string from the data contained within the specified cell.
 * This method checks the data type contained within the specified cell and if it holds
 * a plugin, it rebuilds a complete displayable string based upon the data held within
 * the cell. This includes the plugin name, the parameters into the plugin - and the 
 * connected cells that parameters come from, and and evaluation names for parameters.
 *
 * @param row the row witihn the spreadsheet
 * @param col the column within the spreadsheet
 * @return the displayable plugin string, or an empty string if the cell does not contain a plugin
 */
std::string CPSpreadsheet::RebuildPluginString(int row, int col) const
{
	std::string s;

	std::string cell_name = CellName(row, col);
	const CPDAGNode* node = m_graph->GetNode(cell_name);

	if(node && node->GetData().GetType() == CPDAGData::DAG_PLUGIN)
	{
		s.append("=").append(node->GetData().GetPluginName());
		s.append("(");

		std::map<std::string, std::pair<std::string, std::string> > inputs;
		if(m_graph->GetInputNodes(cell_name, inputs))
		{
			std::map<std::string, std::pair<std::string, std::string> >::const_iterator citer = inputs.begin();
			while(citer != inputs.end())
			{
				std::map<std::string, std::pair<std::string, std::string> >::const_iterator cnext = citer;
				++cnext;

				s.append(citer->second.first).append("=cell(").append(citer->first).append(")");

				if(!citer->second.second.empty())
				{
					s.append(":(").append(citer->second.second).append(")");
				}

				if(cnext != inputs.end())
				{
					s.append(", ");
				}

				citer = cnext;
			}
		}

		s.append(")");
	}

	return(s);
}

/**
 * Returns a string representation of the cell name from its spreadsheet position
 *
 * @param row the row of the cell
 * @param col the column of the cell
 * @return string representaion of the cell name
 */
std::string CPSpreadsheet::CellName(int row, int col)
{
	char str[64];
	memset(str, '\0', 64);

	char _c = 'A' + col;
	int _r = row + 1; // sheet view starts at 1
	snprintf(str, 64 -1, "%c%d", _c, _r);

	return(std::string(str));
}

/**
 * Returns a string representation of the cell range from its spreadsheet position
 *
 * @param start_row the start row of the range
 * @param start_col the start column of the range
 * @param end_row the end row of the range
 * @param end_col the end column of the range
 * @return string representaion of the cell range
 */
std::string CPSpreadsheet::CellRange(int start_row, int start_col, int end_row, int end_col)
{
	char str[64];
	memset(str, '\0', 64);

	char cs = 'A' + start_col;
	char ce = 'A' + end_col;
	int rs = start_row + 1;
	int re = end_row + 1;

	snprintf(str, 64 -1, "%c%d:%c%d", cs, rs, ce, re);

	return(std::string(str));
}

/**
 * Writes the column name for the specified column position into the given buffer
 * The column name represents the displayable name of the column, this is usually
 * a letter representation 0 = A, 1 = B.
 * The size parameter is used to control how many characters are actually written
 * and is the minimum of the buffer length, or size
 *
 * @param col the column number, 0 offset
 * @return the column name
 */
std::string CPSpreadsheet::ColName(int col)
{
	char str[64];
	memset(str, '\0', 64);
	snprintf(str, 64 -1, "%c", 'A' + col);
	return(std::string(str));
}

/**
 * Writes the row name for the specified column position into the given buffer
 * The row name represents the displayable name of the column, this is usually
 * a number representation. The row name is offset by 1.
 *
 * @param col the row number, 0 offset
 * @return the row name
 */
std::string CPSpreadsheet::RowName(int row)
{
	char str[64];
	memset(str, '\0', 64);
	snprintf(str, 64 -1, "%d", row + 1);
	return(std::string(str));
}

/**
 * Returns the column of a cell from the cell name.
 *
 * @param name the cell name
 * @return the column of the cell
 */
int CPSpreadsheet::ColumnFromName(const std::string& name)
{
	int ret = -1;

	std::string::size_type pos = 0;

	while((!isdigit(name[pos])) && (pos < name.length()))
	{
		pos++;
	}

	std::string colname = name.substr(pos, name.size() - pos);

	char* endptr = 0;
	int i = static_cast<int>(strtol(colname.c_str(), &endptr, 10));
	if(*endptr == '\0')
	{
		ret = i -1;
	}

	return(ret);
}

/**
 * Returns the row of a cell from the cell name.
 *
 * @param name the cell name
 * @return the row of the cell
 */
int CPSpreadsheet::RowFromName(const std::string& name)
{
	int ret = -1;

	std::string::size_type pos = name.length() -1;

	while((!isdigit(name[pos])) && (pos > 0))
	{
		pos--;
	}

	std::string rowname = name.substr(0, name.size() - pos);

	char* endptr = 0;
	int i = static_cast<int>(strtol(rowname.c_str(), &endptr, 10));
	if(*endptr == '\0')
	{
		ret = i -1;
	}

	return(ret);
}

/**
 * Update the spreadsheet by re-evaluating all dirty nodes.
 */
void CPSpreadsheet::update()
{
	for(CPDAG::const_iterator cit = m_graph->begin(); cit != m_graph->end(); ++cit)
	{
		m_graph->Evaluate(*cit);
	}
}

