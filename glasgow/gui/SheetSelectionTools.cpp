/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      SheetSelectionTools - Spreadsheet selection handling
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: SheetSelectionTools.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "SheetSelectionTools.h"
#include "CPSpreadsheet.h"
#include "SheetView.h"
#include <gui/CinePaintAppUI.h>
#include <plug-ins/pdb/CinePaintImage.h>
#include "CPDAGNode.h"
#include <app/CinePaintApp.h>
#include <app/FrameManager.h>
#include <plug-ins/pdb/PluginParam.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

const long SheetSelectionTools::DEFAULT_FRAME_MANAGER_DURATION = 100;


/**
 * Constructs a new SheetSelectionTools for handling selection made upon the CinePaint spreadsheet
 *
 * @param view the view of the spreadsheet
 * @param sheet spreadsheet interface into the spreadsheet data
 */
SheetSelectionTools::SheetSelectionTools(SheetView& view, CPSpreadsheet& sheet)
		: m_view(view), m_sheet(sheet)
{}

/**
 * Destructor
 */
SheetSelectionTools::~SheetSelectionTools()
{}


//-----------------------
// Frame Manager handling

/**
 * Adds the current selection to a CinePaint FrameManger
 * If multiple FrameManagers exist the user is querid to which FrameManaget the items
 * should be added
 * 
 */
void SheetSelectionTools::AddToFrameManager()
{
	// get the selected cell data
	int top, left, bottom, right;
	m_view.get_selection(top, left, bottom, right);

	printf("Selection %d %d %d %d\n", top, left, bottom, right);

	std::string fm_name = get_frame_manager_id();

	if(!fm_name.empty())
	{

		// is the selection a range, or single cell
		if(top == bottom && left == right)
		{
			// single cell selected
			add_cell_to_frame_manager(fm_name, top, left, SheetSelectionTools::DEFAULT_FRAME_MANAGER_DURATION);
		}
		else
		{
			// range selected


			// @TODO this needs expanded to handle different selections
			//       for example if we have two rows selected, one row may contain images/filenames while
			//       the other contains time values. We need to determine what data we have and add it
			//       as appropriate to the FrameManager


			// attempt to add the data in each cell -- see todo note above
			while(top <= bottom)
			{
				while(left <= right)
				{
					add_cell_to_frame_manager(fm_name, top, left, SheetSelectionTools::DEFAULT_FRAME_MANAGER_DURATION);
					left++;
				}
				top++;
			}
		}
	}
}

//void SheetSelectionTools::RemoveFromFrameManager()
//{}




/**
 * Attempts to add the specified cell to the named FrameManager
 * Cells that contain either a CinePaintImage, or a string that may be the filename
 * of an image are addedd, all other data is not.
 *
 * @param frame_manager the id of the FrameManaget to add to
 * @param row the row of tjhe cell to add
 * @param col the column of the cell to add
 * @param duration the duration of the new frame within the FrameManager
 * @todo need to work out how to handle Drawble/AbstractBuf types etc.
 */
void SheetSelectionTools::add_cell_to_frame_manager(const std::string& frame_manager, int row, int col, long duration)
{
	// get the cell data
	const CPDAGData* data = m_sheet.GetCellData(row, col);
	if(data)
	{
		CinePaintDoc* doc = CinePaintApp::GetInstance().GetDocList().GetDocById(frame_manager.c_str());
		if(doc)
		{
			FrameManager* fm = dynamic_cast<FrameManager*>(doc);

			const PluginParam* param = 0;

			switch(data->GetType())
			{
					case CPDAGData::DAG_PLUGINPARAM:
					{
						param = data->GetPluginParam();
						break;
					}
					case CPDAGData::DAG_PLUGIN:
					{
						const CPDAGNode* node = m_sheet.GetCellNode(row, col);

						if(node)
						{
							param = node->GetEvaluation();
						}
						break;
					}
					default:
					{
						break;
					}
			}

			if(param)
			{
				switch(param->GetType())
				{
						case PDB_CSTRING:
						{
							add_filename_to_frame_manager(*fm, param->GetArg().pdb_cstring, duration);
							break;
						}
						case PDB_CPIMAGE:
						{
							add_image_to_frame_manager(*fm, param->GetArg().pdb_cpimage, duration);
							break;
						}
						default:
						{
							printf("Unable to add non-frame data to Frame Manager\n");
							break;
						}
				}
			}
		}
		// else
		// ... no frame manager
	}
	// else
	// ... no data in cell
}

void SheetSelectionTools::add_image_to_frame_manager(FrameManager& fm, CinePaintImage* img, long duration)
{
	if(img)
	{
		// get the id of the image
		const char* tmp_id = CinePaintApp::GetInstance().GetDocList().GetDocId(*img);

		// if we dont have a valid id, set the id to an empty string
		std::string id = tmp_id ? std::string(tmp_id) : std::string("");

		std::string filename = img->HasFileName() ? std::string(img->GetFileName()) : std::string("");
		fm.AddLast(id, filename, duration);
	}
}

void SheetSelectionTools::add_filename_to_frame_manager(FrameManager& fm, const char* filename, long duration)
{
	if(filename)
	{
		// is the string a filename?
		if(is_filename(filename))
		{
			fm.AddLast("", filename, duration);
		}
	}
}

/**
 * Gets the id of the FrameManager to to which cell data is to be added
 * If there is only one FrameManager, the Id of that FrameManager is returned.
 * If there are multiple FrameManagers, the users is queried for the Id of the FrameManager
 * If the user cancels, or there are no FrameManagers, an empty string is retuyrned
 *
 * @return the id of the FrameManager to add/remove items from
 */
std::string SheetSelectionTools::get_frame_manager_id() const
{
	std::string fm_name;

	// Get the list of current FrameManagers
	std::list<const char*> fm_list;
	CinePaintApp::GetInstance().GetDocList().GetDocIdsByType(CinePaintDoc::CINEPAINT_FRAMEMANAGER_ENUM, fm_list);


	if(fm_list.size() == 1)
	{
		// if there is only onr frame manager, add to it
		fm_name = std::string(fm_list.front());
	}
	else
	{
		// otherwise we need to ask the user Which FrameManager to add to
		printf("TODO: ask user for FrameManger Name\n");
	}

	return(fm_name);
}

/**
 * Determins if the specified filename exists and is a regular file
 *
 * @return true if the specified file exists and is a regular file, false otherwise
 * @todo this needs a common home
 */
bool SheetSelectionTools::is_filename(const char* filename) const
{
	bool ret = false;

	struct stat buf;
	if(stat(filename, &buf) == 0)
	{
		ret = S_ISREG(buf.st_mode);
	}
	else
	{
		// error, assume non-existant file
		ret = false;
	}

	return(ret);
}
