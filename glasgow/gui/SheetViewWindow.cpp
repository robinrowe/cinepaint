/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      SheetViewWindow - window class fot holding sheet view
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: SheetViewWindow.cpp,v 1.3 2006/12/21 11:18:07 robinrowe Exp $
 */

#include "SheetViewWindow.h"
#include <gui/CinePaintAppUI.h>
#include <gui/MenuFunctorHelper.h>
#include <gui/UIUtils.h>
#include <utility/CellParser.h>
#include "ConnectionPanel.h"
#include "CPSpreadsheet.h"
#include "PluginFunctionBuilder.h"
#include "SheetMouseListener.h"
#include "SheetKeyListener.h"
#include "SheetSelectionTools.h"
#include "SheetView.h"
#include "CPDAG.h"
#include "CPDAGDoc.h"
#include "CPDAGData.h"

#include <app/AppSettings.h>
#include <app/CinePaintApp.h>
#include <plug-ins/pdb/enums.h>
#include <plug-ins/pdb/PluginParam.h>
#include <utility/PlatformUtil.h> // req. for snprintf

#include <FL/fl_ask.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_File_Chooser.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Menu_Button.H>
#include <FL/Fl_Pack.H>

// @TODO provide user configuration to control the size of the spreadsheet
static const int NUM_TABLE_COLS = 25;
static const int NUM_TABLE_ROWS = 25;

static const int MAX_CURR_CELL_TEXT_CHARS = 17; // assume AAAA9999:ZZZZ9999 is as large as we go!


/**
 * Constructs a new SheetViewWindow of the specified size
 *
 * @param w the window width
 * @param h the window height
 * @param titlel window title
 */
SheetViewWindow::SheetViewWindow(int w, int h, const char* title)
		: Fl_Double_Window(w, h, title)
{	m_spreadsheet = new CPSpreadsheet();

	UIUtils::LayoutHelper layout_helper;
	layout_helper.m_posx = 0;
	layout_helper.m_posy = 0;

	// build the window menu
	Fl_Menu_Bar* menu = new Fl_Menu_Bar(0, 0, w, 30);
	m_menu_functor_helper = new MenuFunctorHelper();
	build_menu(*menu, *m_menu_functor_helper);
	layout_helper.m_posx = UIUtils::DEFAULT_X_PAD;
	layout_helper.m_posy += menu->h();

	// sheet entry boxes
	Fl_Pack* top_pack = new Fl_Pack(layout_helper.m_posx, layout_helper.m_posy, this->w() - (2 * UIUtils::DEFAULT_X_PAD), UIUtils::DEFAUTL_ROW_HEIGHT);
	top_pack->type(Fl_Pack::HORIZONTAL);

	layout_helper.m_posy += UIUtils::DEFAULT_Y_PAD;
	m_curr_cell_text = new char[MAX_CURR_CELL_TEXT_CHARS];
	memset(m_curr_cell_text, '\0', MAX_CURR_CELL_TEXT_CHARS);
	m_cell_box = UIUtils::CreateLabel(layout_helper.m_posx, layout_helper.m_posy, 100, UIUtils::DEFAUTL_ROW_HEIGHT, m_curr_cell_text);
	m_cell_box->box(FL_DOWN_BOX);
	layout_helper.m_posx += 100 + UIUtils::DEFAULT_X_PAD;

	Fl_Button* cancel_btn = new Fl_Button(layout_helper.m_posx, layout_helper.m_posy, UIUtils::DEFAUTL_ROW_HEIGHT, UIUtils::DEFAUTL_ROW_HEIGHT, "C");
	cancel_btn->callback(&static_cancel_cell_edit_cb, this);
	cancel_btn->box(FL_FLAT_BOX);
	cancel_btn->down_box(FL_DOWN_BOX);
	cancel_btn->tooltip("Clear current input");
	layout_helper.m_posx += UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_X_PAD;

	Fl_Button* accept_btn = new Fl_Button(layout_helper.m_posx, layout_helper.m_posy, UIUtils::DEFAUTL_ROW_HEIGHT, UIUtils::DEFAUTL_ROW_HEIGHT, "A");
	accept_btn->callback(&static_accept_cell_edit_cb, this);
	accept_btn->box(FL_FLAT_BOX);
	accept_btn->down_box(FL_DOWN_BOX);
	accept_btn->tooltip("Accept Current Input");
	layout_helper.m_posx += UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_X_PAD;

	m_cell_input = new Fl_Input(layout_helper.m_posx, layout_helper.m_posy, this->w() - (layout_helper.m_posx + UIUtils::DEFAULT_X_PAD), UIUtils::DEFAUTL_ROW_HEIGHT);
	m_cell_input->when(FL_WHEN_CHANGED|FL_WHEN_ENTER_KEY);
	m_cell_input->callback(&static_text_input_cb, this);

	top_pack->resizable(m_cell_input);
	top_pack->end();

	layout_helper.m_posy += UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_Y_PAD;
	int _temp_y = layout_helper.m_posy;

	// control panel buttons
	layout_helper.m_posx = UIUtils::DEFAULT_X_PAD;
	Fl_Pack* side_pack = new Fl_Pack(layout_helper.m_posx, layout_helper.m_posy, UIUtils::DEFAUTL_ROW_HEIGHT, (5 * UIUtils::DEFAUTL_ROW_HEIGHT));
	side_pack->type(Fl_Pack::VERTICAL);

	Fl_Button* plugin_select_btn = new Fl_Button(layout_helper.m_posx, layout_helper.m_posy, UIUtils::DEFAUTL_ROW_HEIGHT, UIUtils::DEFAUTL_ROW_HEIGHT, "P");
	plugin_select_btn->box(FL_FLAT_BOX);
	plugin_select_btn->down_box(FL_DOWN_BOX);
	plugin_select_btn->tooltip("Plugin Function Builder");
	layout_helper.m_posy += UIUtils::DEFAUTL_ROW_HEIGHT;
	plugin_select_btn->callback(static_display_plugin_function_builder_cb, this);

	Fl_Button* file_select_btn = new Fl_Button(layout_helper.m_posx, layout_helper.m_posy, UIUtils::DEFAUTL_ROW_HEIGHT, UIUtils::DEFAUTL_ROW_HEIGHT, "F");
	file_select_btn->box(FL_FLAT_BOX);
	file_select_btn->down_box(FL_DOWN_BOX);
	file_select_btn->tooltip("Select a Filepath");

	layout_helper.m_posy += UIUtils::DEFAUTL_ROW_HEIGHT;
	file_select_btn->callback(static_filename_selection_cb, this);

	Fl_Button* connections_window = new Fl_Button(layout_helper.m_posx, layout_helper.m_posy, UIUtils::DEFAUTL_ROW_HEIGHT, UIUtils::DEFAUTL_ROW_HEIGHT, "L");
	connections_window->box(FL_FLAT_BOX);
	connections_window->down_box(FL_DOWN_BOX);
	connections_window->tooltip("Display Cell Links/Connections Window");
	layout_helper.m_posy += UIUtils::DEFAUTL_ROW_HEIGHT;
	connections_window->callback(static_display_connections_cb, this);

	Fl_Button* eval_cell_btn = new Fl_Button(layout_helper.m_posx, layout_helper.m_posy, UIUtils::DEFAUTL_ROW_HEIGHT, UIUtils::DEFAUTL_ROW_HEIGHT, "Ec");
	eval_cell_btn->box(FL_FLAT_BOX);
	eval_cell_btn->down_box(FL_DOWN_BOX);
	eval_cell_btn->tooltip("Evaluate Current Cell");
	eval_cell_btn->callback(static_evaluate_cell_cb, this);
	layout_helper.m_posy += UIUtils::DEFAUTL_ROW_HEIGHT;

	Fl_Button* eval_all_btn = new Fl_Button(layout_helper.m_posx, layout_helper.m_posy, UIUtils::DEFAUTL_ROW_HEIGHT, UIUtils::DEFAUTL_ROW_HEIGHT, "Ea");
	eval_all_btn->box(FL_FLAT_BOX);
	eval_all_btn->down_box(FL_DOWN_BOX);
	eval_all_btn->tooltip("Evaluate All Cells");
	eval_all_btn->callback(static_evaluate_sheet_cb, this);
	eval_all_btn->deactivate();

	side_pack->end();

	// sheet view
	layout_helper.m_posx += UIUtils::DEFAUTL_ROW_HEIGHT + UIUtils::DEFAULT_X_PAD;
	layout_helper.m_posy = _temp_y;
	m_sheet_view = new SheetView(layout_helper.m_posx, layout_helper.m_posy, this->w() - (layout_helper.m_posx + UIUtils::DEFAULT_X_PAD), this->h() - (layout_helper.m_posy + UIUtils::DEFAULT_Y_PAD), 0, *m_spreadsheet);

	// rows
	m_sheet_view->row_header(1);
	m_sheet_view->row_header_width(70);
	m_sheet_view->row_resize(1);
	m_sheet_view->rows(NUM_TABLE_ROWS);
	m_sheet_view->row_height_all(50);
	m_sheet_view->row_resize_min(10);

	// COLS
	m_sheet_view->col_header(1);
	m_sheet_view->col_header_height(25);
	m_sheet_view->col_resize(1);
	m_sheet_view->cols(NUM_TABLE_COLS);
	m_sheet_view->col_width_all(70);
	m_sheet_view->col_resize_min(10);

	// sheet popup menu
	m_popup_menu = new Fl_Menu_Button(layout_helper.m_posx, layout_helper.m_posy, this->w() - (layout_helper.m_posx + UIUtils::DEFAULT_X_PAD), this->h() - (layout_helper.m_posy + UIUtils::DEFAULT_Y_PAD));
	m_popup_menu->type(Fl_Menu_Button::POPUP3);

	// use the same menu functor handler as used by the main menu bar above
	m_sheet_selection_tools = 0;
	build_popup_menu(*m_popup_menu, *m_menu_functor_helper);

	this->end();
	this->resizable(m_sheet_view);
	this->show();

	m_dag_doc = 0;
	m_cell_connections_dialog = 0;

	m_sheet_change_listener = new CellChangeListener(*m_spreadsheet, *this);
	m_sheet_mouse_listener = new SheetMouseListener(*m_sheet_view, *m_spreadsheet);
	m_sheet_key_listener = new SheetKeyListener(*m_sheet_view, *m_spreadsheet);

	m_sheet_view->AddSheetChangeListener(m_sheet_change_listener);
	m_sheet_view->AddMouseListener(m_sheet_mouse_listener);
	m_sheet_view->AddKeyListener(m_sheet_key_listener);

	m_cell_input_handler = new DataInputProcessor(*m_spreadsheet, *m_sheet_view);
}

/**
 * Destructor
 */
SheetViewWindow::~SheetViewWindow()
{
	if(m_sheet_change_listener)
	{
		m_sheet_view->RemoveSheetChangeListener(m_sheet_change_listener);
		delete m_sheet_change_listener;
		m_sheet_change_listener = 0;
	}

	if(m_sheet_mouse_listener)
	{
		m_sheet_view->RemoveMouseListener(m_sheet_mouse_listener);
		delete m_sheet_mouse_listener;
		m_sheet_mouse_listener = 0;
	}

	if(m_sheet_key_listener)
	{
		m_sheet_view->RemoveKeyListener(m_sheet_key_listener);
		delete m_sheet_key_listener;
		m_sheet_key_listener = 0;
	}

	if(m_curr_cell_text)
	{
		delete[] m_curr_cell_text;
	}

	if(m_spreadsheet)
	{
		delete m_spreadsheet;
		m_spreadsheet = 0;
	}

	if(m_sheet_view)
	{
		delete m_sheet_view;
		m_sheet_view = 0;
	}

	if(m_cell_input_handler)
	{
		delete m_cell_input_handler;
		m_cell_input_handler = 0;
	}

	if(m_sheet_selection_tools)
	{
		delete m_sheet_selection_tools;
		m_sheet_selection_tools = 0;
	}
}



//------------------
// CPDAGDoc handling

/**
 * Returns the CPDAGDoc being displayed by this window
 *
 * @return the CPDAGDoc displayed by this window
 */
CPDAGDoc* SheetViewWindow::GetDAGDoc()
{
	return(m_dag_doc);
}

const CPDAGDoc* SheetViewWindow::GetDAGDoc() const
{
	return(m_dag_doc);
}

/**
 * Sets the CPDAGDoc displayed by this window.
 *
 * @param doc the CPDAGDoc to display
 */
void SheetViewWindow::SetDAGDoc(CPDAGDoc& doc)
{
	m_dag_doc = &doc;

	m_spreadsheet->SetDAG(*m_dag_doc->GetDAG());
	if(m_cell_connections_dialog)
	{
		m_cell_connections_dialog->SetDAG(*m_dag_doc->GetDAG());
	}
}



//---------------
// Dialog Control

/**
 * Toggles the visiblity of the cell Connection Dialog
 *
 */
void SheetViewWindow::ToggleConnectionsDialog()
{
	SetConnectionsDialogVisible(m_cell_connections_dialog ? false : true);
}

/**
 * Sets the visibility of the spreadsheet cell connections dialog
 *
 * @param visible set true to display the dialog
 */
void SheetViewWindow::SetConnectionsDialogVisible(bool visible)
{
	if(visible)
	{
		if(!m_cell_connections_dialog)
		{
			m_cell_connections_dialog = new ConnectionPanel(400, 500, *m_sheet_view, "Cell Connections");
			m_cell_connections_dialog->callback(static_connections_dialog_close_cb, this);
			m_cell_connections_dialog->end();
			m_cell_connections_dialog->SetDAG(*m_dag_doc->GetDAG());
		}

		m_cell_connections_dialog->show();
	}
	else
	{
		if(m_cell_connections_dialog)
		{
			m_cell_connections_dialog->hide();
			Fl::delete_widget(m_cell_connections_dialog);
			m_cell_connections_dialog = 0;
		}
	}
}



/**
 * Convenience method to build the Window menu
 *
 * @param the menu to add items to
 * @param menu_helper used to manage constructed menu item functors
 */
void SheetViewWindow::build_menu(Fl_Menu_Bar& menu, MenuFunctorHelper& menu_helper)
{
	menu_helper.AddMenuItem0(menu, 0, "&File/&New...",      &CinePaintAppUI::GetInstance(), &CinePaintAppUI::NewSpreadSheet);
	//menu_helper.AddMenuItem0(menu, 0, "&File/&Revert",    this, &SheetViewWindow::DUMMY_CALLBACK);
	menu_helper.AddMenuItem0(menu, 0, "&File/&Open...",     &CinePaintAppUI::GetInstance(), &CinePaintAppUI::OpenDocument);
	//menu_helper.AddMenuItem0(menu, 0, "&File/&Save...",   this, &SheetViewWindow::DUMMY_CALLBACK);
	//menu_helper.AddMenuItem0(menu, 0, "&File/&Save As..." this, &SheetViewWindow::DUMMY_CALLBACK);
	menu_helper.AddMenuItem0(menu, 0, "&File/&Close", this, &SheetViewWindow::file_close_cb);
	menu_helper.AddMenuItem0(menu, 0, "&View/&Connections Dialog", this, &SheetViewWindow::ToggleConnectionsDialog);
}

/**
 * Convenience method to build the sheet view popup menu
 *
 * @param the menu to add items to
 * @param menu_helper used to manage constructed menu item functors
 */
void SheetViewWindow::build_popup_menu(Fl_Menu_& menu, MenuFunctorHelper& menu_helper)
{
	if(!m_sheet_selection_tools)
	{
		m_sheet_selection_tools = new SheetSelectionTools(*m_sheet_view, *m_spreadsheet);
	}

	// sheet cell options
	menu_helper.AddMenuItem0(menu, 0, "Select Filename", this, &SheetViewWindow::filename_selection_cb);
	menu_helper.AddMenuItem0(menu, 0, "_Select Plugin", this, &SheetViewWindow::display_plugin_function_builder);
	menu_helper.AddMenuItem0(menu, 0, "Add To Frame Manager", m_sheet_selection_tools, &SheetSelectionTools::AddToFrameManager);
	//menu_helper.AddMenuItem0(menu, 0, "Remove From Frame Manager", m_sheet_selection_tools, &SheetSelectionTools::RemoveFromFrameManager);
}

void SheetViewWindow::update_cur_cell_text(int row, int col)
{
	std::string s = CPSpreadsheet::CellName(row, col);
	snprintf(m_curr_cell_text, MAX_CURR_CELL_TEXT_CHARS -1, "%s", s.c_str());
	m_cell_box->label(m_curr_cell_text);
	m_cell_box->redraw();
}

void SheetViewWindow::update_cur_cell_text(int start_row, int start_col, int end_row, int end_col)
{
	std::string s = CPSpreadsheet::CellRange(start_row, start_col, end_row, end_col);
	snprintf(m_curr_cell_text, MAX_CURR_CELL_TEXT_CHARS -1, "%s", s.c_str());
	m_cell_box->label(m_curr_cell_text);
	m_cell_box->redraw();
}

void SheetViewWindow::clear_input_bar()
{
	m_cell_input->value(0);
}

void SheetViewWindow::update_input_bar(const std::string& data, int row, int col)
{
	m_cell_input->value(data.c_str());
}

void SheetViewWindow::update_input_bar(const PluginParam& param, int row, int col)
{
	switch(param.GetType())
	{
			case PDB_INT:
			{
				char buf[64];
				snprintf(buf, 64, "%d", param.GetArg().pdb_int);
				m_cell_input->value(buf);
				break;
			}
			case PDB_FLOAT:
			{
				char buf[64];
				snprintf(buf, 64, "%f", param.GetArg().pdb_float);
				m_cell_input->value(buf);
				break;
			}
			case PDB_CSTRING:
			{
				m_cell_input->value(param.GetArg().pdb_cstring);
				break;
			}
			case PDB_VOIDPOINTER:
			case PDB_ABSTRACTBUF:
			case PDB_DRAWABLE:
			case PDB_CPIMAGE:
			default:
			{
				break;
			}
	}
}


//-----------------
// Widget callbacks

void SheetViewWindow::accept_cell_edit_cb()
{
	const char* tmp = m_cell_input->value();

	if(tmp)
	{
		std::string s(tmp);
		m_cell_input_handler->ProcessInput(s);
	}

	m_sheet_view->SetInputEntryVisibile(false);
}

void SheetViewWindow::cancel_cell_edit_cb()
{
	m_cell_input->value(0);
	m_sheet_view->SetInputEntryVisibile(false);
}

void SheetViewWindow::text_input_cb()
{
	const char* tmp = m_cell_input->value();

	// was the last keypress the enter key
	if(Fl::event_key(FL_Enter))
	{
		// if so, process the newly entered text
		if(m_sheet_view->IsInputEntryVisible())
		{
			m_sheet_view->SetInputEntryVisibile(false);
		}

		if(tmp)
		{
			std::string s(tmp);
			m_cell_input_handler->ProcessInput(s);
		}
	}
	else
	{
		// otherwise, just update the sheet view
		std::string s = tmp != 0 ? std::string(tmp) : "";
		if(!m_sheet_view->IsInputEntryVisible())
		{
			m_sheet_view->SetInputEntryVisibile(true, false);
		}

		m_sheet_view->SetInputEntryText(tmp);
	}
}

void SheetViewWindow::display_plugin_function_builder()
{	World* world=CinePaintApp::GetWorld();
	PluginFunctionBuilder builder(*m_spreadsheet,world);

	DialogResponse ret = builder.ShowAndWait();

	if(ret == OK_ENUM)
	{
		m_cell_input_handler->ProcessInput(builder.GetFunctionString());
	}
}

void SheetViewWindow::filename_selection_cb()
{
	const char* home_dir = GetPathUserHome();
	Fl_File_Chooser chooser(home_dir , 0, 0, "File Selection");
	chooser.show();

	while(chooser.shown())
	{
		Fl::wait();
	}

	if(chooser.count() > 0)
	{
		const char* filename = chooser.value();
		if(filename)
		{
			std::string s(filename);
			m_cell_input_handler->ProcessInput(s);
		}
	}
}


void SheetViewWindow::evaluate_cell_cb()
{
	CPDAG* dag = m_spreadsheet->GetDAG();
	if(dag)
	{
		std::string plugin_cell = CPSpreadsheet::CellName(m_sheet_view->get_current_row(), m_sheet_view->get_current_col());
		if(!dag->Evaluate(plugin_cell))
		{
			fl_alert("Evaluation of Cell, %s, failed!", plugin_cell.c_str());
		}
	}

	m_sheet_view->redraw();
}

void SheetViewWindow::evaluate_sheet_cb()
{
	// @TODO obviously ...
	printf("Not implemented yet!\n");
}


void SheetViewWindow::connections_dialog_close_cb()
{
	SetConnectionsDialogVisible(false);
}

void SheetViewWindow::file_close_cb()
{
	if(m_dag_doc)
	{
		CinePaintAppUI::GetInstance().GetDocViewManager().CloseView(*m_dag_doc, *this);
	}
}



//---------------
// Nested Classes

SheetViewWindow::DataInputProcessor::DataInputProcessor(CPSpreadsheet& sheet, SheetView& view)
		: m_sheet(sheet), m_view(view)
{}

SheetViewWindow::DataInputProcessor::~DataInputProcessor()
{}

/**
 * Process textual input for entry into the spreadsheet.
 * This method deteremines the type of the textual data and sets the appropriate
 * data within the spreadsheet
 *
 * @param text the input text
 */
void SheetViewWindow::DataInputProcessor::ProcessInput(const std::string& text)
{
	CellParser cp;
	if(cp.Parse(text))
	{
		if(cp.GetDataType() == CellParser::PLUGIN_TYPE_ENUM)
		{
			process_plugin_text(cp);
		}
		else
		{
			process_param_text(cp);
		}
	}
}

/**
 * Inserts the textual input as a plugin parameter into the currently selected spreadsheet cell 
 * The textual data is first converted to the apprpriate data type, depending
 * upon how the text was parsed.
 *
 * @param cp the CellParser used to parse the input string
 */
void SheetViewWindow::DataInputProcessor::process_param_text(CellParser& cp)
{
	switch(cp.GetDataType())
	{
			case CellParser::INT_TYPE_ENUM:
			{
				int i;
				if(cp.GetInt(i))
				{
					m_sheet.SetCell(m_view.get_current_row(), m_view.get_current_col(), i);
					m_view.redraw();
				}
				break;
			}
			case CellParser::FLOAT_TYPE_ENUM:
			{
				float f;
				if(cp.GetFloat(f))
				{
					m_sheet.SetCell(m_view.get_current_row(), m_view.get_current_col(), f);
					m_view.redraw();
				}
				break;
			}
			case CellParser::STRING_TYPE_ENUM:
			{
				std::string s;
				if(cp.GetString(s))
				{
					m_sheet.SetCell(m_view.get_current_row(), m_view.get_current_col(), s.c_str());
					m_view.redraw();
				}
				break;
			}
			case CellParser::PLUGIN_TYPE_ENUM:
			{
				process_plugin_text(cp);
				break;
			}
			case CellParser::NO_TYPE_ENUM:
			case CellParser::UNKNOWN_TYPE_ENUM:
			default:
			{
				printf("Ignoring unknown data type in cell entry\n");
				break;
			}
	}
}

/**
 * Inserts the textual input as a plugin cell into the currently selected spreadsheet cell 
 * The input string is processed further to attach nodes within the spreadsheet to represent
 * input into the plugin.
 *
 * @param cp the CellParser used to parse the input string
 */
void SheetViewWindow::DataInputProcessor::process_plugin_text(CellParser& cp)
{
	const CellParser::PluginData* plugin_data = cp.GetPluginData();
	if(plugin_data)
	{
		m_sheet.SetPluginCell(m_view.get_current_row(), m_view.get_current_col(), plugin_data->GetPluginName().c_str());

		// attach/join the plugin to any cells containing parameters
		for(CellParser::PluginData::const_iterator citer = plugin_data->param_begin(); citer != plugin_data->param_end(); ++citer)
		{
			if(!(*citer).m_param_cell.empty() && !(*citer).m_param_name.empty())
			{
				std::string plugin_cell = CPSpreadsheet::CellName(m_view.get_current_row(), m_view.get_current_col());
				printf("Linking Cells, Plugin Cell=%s, param cell=%s, param name=%s, evluation name=%s\n", plugin_cell.c_str(), (*citer).m_param_cell.c_str(), (*citer).m_param_name.c_str(), (*citer).m_evaluation_name.c_str());
				m_sheet.Reference((*citer).m_param_cell, plugin_cell, false, false, (*citer).m_param_name, (*citer).m_evaluation_name, true);
			}
		}

		m_view.redraw();
	}
}






SheetViewWindow::CellChangeListener::CellChangeListener(CPSpreadsheet& sheet, SheetViewWindow& window)
		: m_sheet(sheet), m_sheet_window(window)
{}

void SheetViewWindow::CellChangeListener::CursorCellChanged(int row, int col)
{
	m_sheet_window.update_cur_cell_text(row, col);

	const CPDAGData* cell_data = m_sheet.GetCellData(row, col);
	if(cell_data)
	{
		if(cell_data->GetType() == CPDAGData::DAG_PLUGIN)
		{
			std::string s = m_sheet.RebuildPluginString(row, col);
			m_sheet_window.update_input_bar(s, row, col);
		}
		else
		{
			const PluginParam* param = cell_data->GetPluginParam();

			if(param)
			{
				m_sheet_window.update_input_bar(*param, row, col);
			}
			else
			{
				m_sheet_window.clear_input_bar();
			}
		}
	}
	else
	{
		m_sheet_window.clear_input_bar();
	}
}

void SheetViewWindow::CellChangeListener::RangeChanged(int row_start, int col_start, int row_end, int col_end)
{
	if((col_start != col_end) || (row_start != row_end))
	{
		m_sheet_window.update_cur_cell_text(row_start, col_start, row_end, col_end);
	}
	else
	{
		// range covers a single cell, so just display the selected cell
		m_sheet_window.update_cur_cell_text(row_start, col_start);
	}
}

void SheetViewWindow::CellChangeListener::ValueChanged(int row, int col, const std::string& val)
{
	m_sheet_window.m_cell_input_handler->ProcessInput(val);
}
