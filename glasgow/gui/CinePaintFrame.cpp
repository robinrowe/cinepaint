/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                     CienPaintFrame - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintFrame.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include <gui/CinePaintFrame.h>

#include <FL/fl_draw.H>

static const int LABEL_PAD_X = 4;
static const int LABEL_OFFSET_X = 10;

/**
 * Constructs a new CinePaintFrame with the specified label and boxtype
 *
 * @param x widget x position
 * @param x widget y position
 * @param x widget width
 * @param x widget height
 * @param label textual frame label
 * @param btype the frame type
 */
CinePaintFrame::CinePaintFrame(int x, int y, int w, int h, const char* label, Fl_Boxtype btype)
: Fl_Group(x, y, w, h, label)
{
	this->box(btype);
	this->align(FL_ALIGN_INSIDE|FL_ALIGN_CENTER);

	m_border_width = 5;
}

/**
 * Destructor
 */
CinePaintFrame::~CinePaintFrame()
{

}

/**
 * Sets the border with of this CinePaintFrame
 * The border with is the distance from the edge of this Widget to the drawn frame
 *
 * @param width the border with
 */
void CinePaintFrame::SetBorderWidth(int width)
{
	m_border_width = width;
}

/**
 * Returns the current Border width of this CinePaintFrame
 *
 * @return the width from the edge of this Widget to the drawn frame
 */
int CinePaintFrame::GetBorderWidth() const
{
	return(m_border_width);
}

/**
 * Draws this widget
 */
void CinePaintFrame::draw()
{
	Fl_Widget* const* a = array();
	if(damage() == FL_DAMAGE_CHILD)
	{ // only redraw some children
		for(int i = children(); i--; a++)
		{
			update_child(**a);
		}
	}
	else
	{ // total redraw
		int _w = 0;
		int _h = 0;
		fl_measure(this->label(), _w, _h);

		draw_box(this->box(), this->x() + m_border_width,
			this->y() + (_h / 2) + m_border_width,
			this->w() - (2 * m_border_width),
			this->h() - (_h / 2) - m_border_width,
			this->color());

		fl_color(this->color());
		fl_rectf(this->x() + m_border_width + LABEL_OFFSET_X,
			this->y() + m_border_width,
			_w + (2 * LABEL_PAD_X),
			_h);

		draw_label(this->x() + m_border_width + LABEL_OFFSET_X + LABEL_PAD_X,
			this->y() + m_border_width,
			_w, _h);
		
		// now draw all the children atop the background
		for (int i = children(); i --; a ++)
		{
			draw_child(**a);
			draw_outside_label(**a);
		}
	}
}
