/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ColorSelectionDialog - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ColorSelectionDialog.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _COLOR_SELECTION_PANEL_H_
#define _COLOR_SELECTION_PANEL_H_

#include "dll_api.h"
#include <FL/Fl_Window.H>
#include <plug-ins/pdb/Color.h>
#include <app/ColorChangeListener.h>

#include <list>

// forward declaration
class ColorManager;
class Fl_Color_Chooser;
class Fl_Group;

/**
 * A dialog allowing auser to select a Color
 */
class CINEPAINT_GUI_API ColorSelectionDialog : public Fl_Window
{
	public:
		/**
		 * Constructs a default ColorSelectionDialog
		 * Inorder to receive color selection notifications, a ColorChangeListener should be added.
		 *
		 * @param title dialog title
		 */
		ColorSelectionDialog(const char* title = 0);

		/**
		 * Constructs a ColorSelectionDialog to automatically update the specified ColorManager
		 *
		 * @param color_manager the ColorManager to update
		 * @param title dialog title
		 */
		ColorSelectionDialog(ColorManager& color_manager, const char* title = 0);

		/**
		 * Destructor
		 */
		virtual ~ColorSelectionDialog();


		/**
		 * Shows the ColorSelectionDialog and waits for the user to select a Color.
		 * This method allows a ColorSelectionDialog to be created and used as a one shot
		 * color selection dialog, using GetColor to retrieve the selected Color
		 *
		 */
		void ShowAndWait();


		/**
		 * Sets the displayed selected Color to c
		 * This method does not update any registered ColorChangeListeners
		 *
		 * @param c the new Color
		 */
		void SetColor(const Color& c);

		/**
		 * Gets the selected Color upon the dialog
		 *
		 * @return the currently selected color
		 */
		const Color& GetColor() const;

		/**
		 * Sets the update style of this ColorSelectionDialog.
		 * If set true, this dialog will post an update message as the color value is changed.
		 * If set false, an update is only posted when the choose button is pressed
		 *
		 * @param rolling_update auto rolling update style
		 */
		void SetAutoRollingUpdate(bool auto_rolling);


		/**
		 * Sets the ColorChangeListener::ColorType posted by this dialog upon color selection change
		 *
		 */
		void SetColorTypeUpdate(ColorChangeListener::ColorType color_type);

		//----------------
		// Change Listener

		/**
		 * Adds the specified ColorChangeListener to receive notification upon color selection
		 * if the specified ColorChangeListener has already been added it is not added again.
		 * The resources of the specified ColorChangeListener are not managed by this class
		 * The ColorType of the change is set to ColorChangeLister::NOTYPE_ENUM
		 *
		 * @param ccl the ColorChangeListener to be added
		 */
		void AddColorChangeListener(ColorChangeListener* ccl);

		/**
		 * Removes the specified ColorChangeListener
		 * The removed ColorChangeListener will no longer receive Color selection notifications from
		 * this class
		 *
		 * @param ccl the ColorChangeListener to be removed
		 */
		ColorChangeListener* RemoveColorChaneListener(ColorChangeListener* tcl);

	protected:

	private:

		/**
		 * Creates the GUI panel for the Color Selection Dialog
		 *
		 * @param title the title of this dialog
		 */
		void create_panel(const char* title);

		/** indicates that this dialog should update listeners continually as the selected Color is changed */
		bool m_auto_roll_update;

		/** indicates the if tis dialog is a one shot dialog, or a persistant window */
		bool m_show_and_wait;

		/** the current update type for the selected Color
		   this is sent to registered listeners to indicate what color type (foreground/background etc) is being changed */
		ColorChangeListener::ColorType m_update_color_type;

		/**
		 * Small Color Chip widget
		 * This is adapted from the FLTK sources Fl_Color_Chooser.cxx
		 */
		class ColorChip : public Fl_Widget
		{
			public:
				ColorChip(int x, int y, int h, int w);
				void SetColor(const Color& c);
				const Color& GetColor() const;

				void draw();

			protected:

			private:
				Color m_color;
		};


		//===============
		// Dialog Widgets

		Fl_Color_Chooser*  m_chooser;
		ColorChip* m_new_color_chip;
		ColorChip* m_old_color_chip;

		//=================
		// Widget callbacks

		static void static_chooser_cb(Fl_Widget* w, void* d) { static_cast<ColorSelectionDialog*>(d)->chooser_cb(); };
		void chooser_cb();

		static void static_choose_button_cb(Fl_Widget* w, void* d) { static_cast<ColorSelectionDialog*>(d)->choose_button_cb(); };
		void choose_button_cb();

		static void static_revert_button_cb(Fl_Widget* w, void* d) { static_cast<ColorSelectionDialog*>(d)->revert_button_cb(); };
		void revert_button_cb();



		//----------------
		// Change Listener

		/**
		 * Internal Change Lister used to update a ColorManager if this dialog is constructed with one
		 *
		 */
		class ColManChangeListener : public ColorChangeListener
		{
			public:
				ColManChangeListener(ColorManager& color_manager);

				virtual void ColorChanged(ColorChangeListener::ColorType color_type, const Color& color);

			protected:

			private:
				ColorManager& m_color_manager;
		};

		/** internal change listener if this dialog automatically updates a ColorManager */
		ColManChangeListener* m_col_man_worker;

		/**
		 * post a ColorChanged message to all registered ColorChangeListeners
		 * The ColorType of the change is set to ColorChangeLister::NOTYPE_ENUM
		 *
		 * @param c the new Color
		 */
		void post_color_changed_update(const Color& c);

		typedef std::list<ColorChangeListener*> ListenerList_t;

		/** list of registered ColorChangeListerner */
		ListenerList_t m_color_change_listeners;

} ; /* class ColorSelectionDialog */

#endif /* _COLOR_SELECTION_PANEL_H_ */
