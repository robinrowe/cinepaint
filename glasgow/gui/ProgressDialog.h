/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ProgressDialog - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ProgressDialog.h,v 1.2 2006/12/18 08:21:17 robinrowe Exp $
 */
#ifndef _PROGRESS_DIALOG_H_
#define _PROGRESS_DIALOG_H_

#include "dll_api.h"
#include "plug-ins/pdb/CinePaintProgressListener.h"
#include "app/CinePaintEvent.h"
#include <list>

// Forward Declarations
class Fl_Progress;
class Fl_Button;
class Fl_Window;

/**
 * This class manages a Progress Dialog box.
 * It is also thread safe - it checks whether it is running in the same thread as the 
 * main UI event loop - if so all Updates and windows creation is done directly,
 * otherwise it posts a message into the event queue.
 * 
**/
class CINEPAINT_GUI_API ProgressDialog
{
	
public:
	ProgressDialog(char* ID);
	~ProgressDialog();

	/**
	 * Create the progress dialog.
	 * With given text and progress bar size.
	**/
	void CreateProgressDialog(char* text, int max, int step);

	/**
	 * Sets the status text displayed by progress window
	 * @param txt The text to display.
	**/
	void SetText( char* txt);
	
	/**
	 * Reset the progress bar 0.
	 * @param max the maximum number of steps
	 * @param step to number of steps to reach the 100%
	**/
	void ResetProgress(int max, int step);
	
	/**
	 * Move the progress bar on a step.
	 * @return the new position of the progress bar.
	**/
	void UpdateProgress();

	/**
	 * Move the progress bar to a specific position.
	 * @param pos the position to set the bar too.
	**/
	void SetProgress(int pos);

	/**
	 * Dialog is finsihed so remove from screen
	 * @return whether it will self delete or not.
	 * returns true if delete is required.
	 **/
	bool Hide();

	/**
	 * Get this dialogs ID.
	 * @return The ID string for this dialog
	**/
	const char* GetID() {return m_ID;};
private:

	Fl_Window *m_pProgressWindow;
	Fl_Progress *m_pProgressBar;
	Fl_Button *m_pProgressInfo;

	int m_iProgressMax;
	int m_iProgressStep;
	int m_iProgressPos;

	char m_sText[500];

	// Unique Identifier.
	char m_ID[50];

	// Whether we are in teh main UI thread or not
	bool m_main_thread;
	
private:
	//========================================
	// Private methods to actually do the work

	void create_progress_dialog(char* text, int max, int step);
	void set_text(char* text);
	void reset_progress(int max, int step);
	void update_progress();
	void set_progress(int pos);
	void hide();

public:
	//===========================
	// ProgressListener for this Dialog.
	class ProgressListener : public CinePaintProgressListener
	{
	public:
		ProgressListener();
		~ProgressListener();

		void ProgressStart(char* ID, char* msg, int stop, int step);
		void MessageChanged(char* ID,  char* msg );
		void LimitsChanged(char* ID, int start, int stop, int step);
		void ProgressUpdate(char* ID);
		void ProgressReset(char* ID);
		void ProgressPosition(char* ID, int pos);
		void ProgressComplete(char* ID);

	private:
		std::list<ProgressDialog *> m_dlgList;
		ProgressDialog *find_dlg(char* ID);

	};

	//==========================
	// Events to post into event loop.
	class NewProgressEvent : public CinePaintEvent
	{
	public:
		NewProgressEvent( ProgressDialog *dlg, char* msg, int max, int step );
		~NewProgressEvent() {};
		void Process();

	private:
		ProgressDialog *m_dlg;
		char* m_msg;
		int m_max;
		int m_step;
	};

	class UpdateProgressEvent : public CinePaintEvent
	{
	public:
		UpdateProgressEvent( ProgressDialog *dlg );
		~UpdateProgressEvent() {};
		void Process();
	private:
		ProgressDialog *m_dlg;

	};
	class ProgressCompleteEvent : public CinePaintEvent
	{
	public:
		ProgressCompleteEvent( ProgressDialog *dlg );
		~ProgressCompleteEvent() {};
		void Process();
	private:
		ProgressDialog *m_dlg;

	};

};

#endif // _PROGRESS_DIALOG_H_
