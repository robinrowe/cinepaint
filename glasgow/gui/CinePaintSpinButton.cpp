/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintSpinButton - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintSpinButton.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include <gui/CinePaintSpinButton.h>

#include <FL/Fl.H>
#include <FL/Fl_Valuator.H>
#include <FL/fl_draw.H>

#include <float.h> // for FLT_MIN and FLT_MAX


// spin acceleration control constants
static int TIMEOUT_STEP_COUNT[] = { 0, 3, 10 };          // nuymber of increment repeats before decreasing timeout value
static double TIMEOUT_STEP_VALUE[] = { 0.5, 0.1, 0.05 }; // timeout callback values

static int DELTA_INCREMENT_COUNT[] = { 0, 40 };          // number of increment repeats before increasing the delta value
static int DELTA_INCREMENT_VALUE[] = { 1, 10 };          // delta value steps


/**
 * Constrcusta new CinePaintButton with the specified action command
 *
 * @param x the x position of the widget
 * @param y the y position of the widget
 * @param w the width of the widget
 * @param h the height of the widget
 * @param label text label for the button
 * @param action_cmd action command associaited with this CinePaintButton
 */
CinePaintSpinButton::CinePaintSpinButton(int x, int y, int w, int h, const char* label)
: Fl_Valuator(x, y, w, h, label)
{
	range(FLT_MIN, FLT_MAX);
	step(1.0);

	m_spin_dir = SPIN_NONE_ENUM;
	m_prev_spin_dir = SPIN_NONE_ENUM;

	m_repeat_count = 0;
}

/**
 * Destructor
 */
CinePaintSpinButton::~CinePaintSpinButton()
{
	// just in case we are destroyed while there is still a timeout to execute
	Fl::remove_timeout(static_timeout_callback, this);
}


/**
 * Draws this CinePaintSpinButton Widget
 *
 */
void CinePaintSpinButton::draw()
{
	int _btn_height = this->h() / 2;
	draw_box(FL_THIN_DOWN_BOX, x(), y(), w() - _btn_height, h(), FL_BACKGROUND2_COLOR);

	Fl_Color selcolor = active_r() ? labelcolor() : fl_inactive(labelcolor());

	Fl_Boxtype _up_box;
	Fl_Boxtype _down_box;
	switch(m_spin_dir)
	{
		case SPIN_UP_ENUM:
		{
			_up_box = FL_DOWN_FRAME;
			_down_box = FL_UP_FRAME;
			break;
		}
		case SPIN_DOWN_ENUM:
		{
			_up_box = FL_UP_FRAME;
			_down_box = FL_DOWN_FRAME;
			break;
		}
		default:
		{
			_up_box = FL_UP_FRAME;
			_down_box = FL_UP_FRAME;
		}
	}

	fl_draw_box(_up_box, this->x() + (this->w() - _btn_height), this->y(), _btn_height, _btn_height, FL_BACKGROUND_COLOR);
	fl_draw_symbol("@#-18>", this->x() + (this->w() - _btn_height), this->y(), _btn_height, _btn_height, selcolor);
	fl_draw_box(_down_box, this->x() + (this->w() - _btn_height), this->y() + _btn_height, _btn_height, _btn_height, FL_BACKGROUND_COLOR);
	fl_draw_symbol("@#-12>", this->x() + (this->w() - _btn_height), this->y() + _btn_height, _btn_height, _btn_height, selcolor);


	char str[128]; // buffer size hard coded in Fl_Valuator::format
	format(str);
	fl_draw(str, x(), y(), (this->w() - _btn_height), h(), FL_ALIGN_LEFT);
	if(Fl::focus() == this)
	{
		draw_focus(FL_THIN_DOWN_BOX, x(), y(), (this->w() - _btn_height), h());
	}
}

/**
 * handles an FLTK event
 *
 * @param e the FLTK event type
 */
int CinePaintSpinButton::handle(int e)
{
	int _ret = 0;

	switch(e)
	{
		case FL_PUSH:
		{
			_ret = process_push();
			break;
		}
		case FL_RELEASE:
		{
			_ret = process_release();
			break;
		}
		case FL_DRAG:
		{
			_ret = process_drag_position();
			break;
		}
		case FL_FOCUS:
		case FL_UNFOCUS:
		{
			_ret = 1; // req to indicate we want keypress events
			break;
		}
		case FL_KEYDOWN:
		{
			_ret = process_key_press();
			break;
		}
		case FL_KEYUP:
		{
			_ret = process_key_release();
			break;
		}
		default:
		{
			set_spin_dir(SPIN_NONE_ENUM);
		}
	}

	return(_ret);
}


/**
 * Handles a button push to determine if the event occurs within the up and down spin areas.
 * If the event occured within the up or down spin action area, add a timeout to continually
 * update the value until the button is released, or the mouse moves off the button
 *
 * @return 1 if the event was consumed, 0 otherwise
 */
int CinePaintSpinButton::process_push()
{
	int _ret = 0;
	
	if(Fl::event_inside(this))
	{
		_ret = 1;

		int _btn_height = this->h() / 2;

		if(Fl::event_inside(this->x() + this->w() - _btn_height, this->y(), _btn_height, _btn_height))
		{
			// inside the up arrow
			set_spin_dir(SPIN_UP_ENUM);
			increment_val(calc_increment_delta());
			Fl::add_timeout(calc_timeout(), &static_timeout_callback, this);
		}	
		else if(Fl::event_inside(this->x() + this->w() - _btn_height, this->y() + _btn_height, _btn_height, _btn_height))
		{
				// inside the down arrow
			set_spin_dir(SPIN_DOWN_ENUM);
			increment_val(calc_increment_delta());
			Fl::add_timeout(calc_timeout(), &static_timeout_callback, this);
		}	
		else
		{
			// not within either the up or down arrow
			m_spin_dir = SPIN_NONE_ENUM;
			Fl::remove_timeout(&static_timeout_callback, this);
		}

		this->redraw();
	}

	return(_ret);
}

/**
 * Stops and automatic update from a mouse click and hold action
 *
 * @return 1 if the last mouse event lies within this widget, 0 otherwise
 */
int CinePaintSpinButton::process_release()
{
	int _ret = 0;

	if(Fl::event_inside(this))
	{
		_ret = 1;
		set_spin_dir(SPIN_NONE_ENUM);
		Fl::remove_timeout(static_timeout_callback, this);

		this->redraw();
	}

	return(_ret);
}

/**
 * Determines if the last mouse event lies within either of the increment arrows.
 * If the last mouse position lies outwith this widget, the continual increment
 * on a mouse click and hold is suspended.
 *
 * @return 0 if the event falls outwith this widget, 1 if within
 */
int CinePaintSpinButton::process_drag_position()
{
	int _ret = 0;
	
	if(Fl::event_inside(this))
	{
		_ret = 1;

		if(Fl::event_inside(this->x() + this->w() - this->h(), this->y(), this->h(), this->h()))
		{
			if(m_spin_dir == SPIN_NONE_ENUM)
			{
				process_push();
			}
		}
	}
	else
	{
		_ret = 0;
		Fl::remove_timeout(static_timeout_callback, this);
		m_spin_dir = SPIN_NONE_ENUM;
		this->redraw();
	}

	return(_ret);
}

/**
 * Process a keydown event to determine if we need to increment the value
 * If either the up or down arrow key was pressed, increment the counter and return
 * 1 to indicate that the event processed, otherwise return 0
 *
 * @return 1 if either the up or doen arrow key was pressed, otherwise 0
 */
int CinePaintSpinButton::process_key_press()
{
	int _ret = 0;

	if(Fl::event_key() == FL_Up)
	{
		set_spin_dir(SPIN_UP_ENUM);
		increment_val(calc_increment_delta());
		this->redraw();
		_ret = 1;
	}
	else if(Fl::event_key() == FL_Down)
	{
		set_spin_dir(SPIN_DOWN_ENUM);
		increment_val(calc_increment_delta());
		this->redraw();
		_ret = 1;
	}

	return(_ret);
}

/**
 * Process a keyup event.
 * If either the up or down arrow key was pressed, we cause a redraw on this widget
 * upon the keyup, and return 1 to indicate this widget reposnded to the event, otherwise return 0
 *
 * @return 1 if either the up or doen arrow key was released, otherwise 0
 */
int CinePaintSpinButton::process_key_release()
{
	int _ret = 0;

	if((Fl::event_key() == FL_Up) || (Fl::event_key() == FL_Down))
	{
		set_spin_dir(SPIN_NONE_ENUM);
		this->redraw();
		_ret = 1;
	}

	return(_ret);
}



/**
 * Increments the current value of this CinePaintSpinButton by the specified delta value
 * delta * step() is added to the current value
 *
 * @param delta delta * step value for addition to value
 */
void CinePaintSpinButton::increment_val(int delta)
{
	double v = increment(value(), delta);
	v = round(v);
	handle_drag(clamp(v));
}

/**
 * Sets the current spin direction and increments the repeat count
 * If the new spin direction differs from the previous value, the repeat count is set to 0
 *
 * @param dir the new Dpin Direction
 */
void CinePaintSpinButton::set_spin_dir(CinePaintSpinButton::SpinDirectionEnum dir)
{
	m_prev_spin_dir = m_spin_dir;
	m_spin_dir = dir;

	if(m_prev_spin_dir != m_spin_dir)
	{
		m_repeat_count = 0;
	}
	else
	{
		m_repeat_count++;
	}
}

/**
 * Calculates the delta increment value based upon the current repeat count
 * This acts as a spin accelerator value
 *
 * @return delta increment value based upon the current repeat count
 */
int CinePaintSpinButton::calc_increment_delta()
{
	int _delta = DELTA_INCREMENT_VALUE[0];

	if(m_repeat_count > DELTA_INCREMENT_COUNT[1])
	{
		_delta = DELTA_INCREMENT_VALUE[1];
	}

	switch(m_spin_dir)
	{
		case SPIN_UP_ENUM:
			_delta = _delta * 1;
			break;
		case SPIN_DOWN_ENUM:
			_delta = _delta * -1;
			break;
		case SPIN_NONE_ENUM:
		default:
			_delta = 0;
			break;
	}

	return(_delta);
}

/**
 * Calculatas the timeout callback delay based upon the current repeat count
 * This method cts as a spin accelerator
 *
 * @return FLTK timeout callback delay in seconds
 */
double CinePaintSpinButton::calc_timeout()
{
	double _timeout = 0.5;
	
	if(m_repeat_count > TIMEOUT_STEP_COUNT[2])
	{
		_timeout = TIMEOUT_STEP_VALUE[2];
	}
	else if(m_repeat_count > TIMEOUT_STEP_COUNT[1])
	{
		_timeout = TIMEOUT_STEP_VALUE[1];
	}
	else // TIMEOUT_STEP_COUNTS[0]
	{
		_timeout = TIMEOUT_STEP_VALUE[0];
	}

	return(_timeout);
}


/**
 * Timeout callback, used to repeatedly increment the value as the mouse is held down
 *
 */
void CinePaintSpinButton::timeout_callback()
{
	switch(m_spin_dir)
	{
		case SPIN_DOWN_ENUM:
		case SPIN_UP_ENUM:
			process_push();
			break;
		case SPIN_NONE_ENUM:
			Fl::remove_timeout(&static_timeout_callback, this);
			this->redraw();
			break;
	}
}
