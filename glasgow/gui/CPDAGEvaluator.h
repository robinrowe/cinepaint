/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CPDAGEvaluator - Directed acyclic graph node evaluator
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * $Id: CPDAGEvaluator.h,v 1.3 2006/12/21 11:18:05 robinrowe Exp $
 */

#ifndef _CPDAGEvaluator_H_
#define _CPDAGEvaluator_H_

#include "dll_api.h"
#include "plug-ins/pdb/CinePaintPlugin.h" // req. for CinePaintPlugin::CPPluginParamDef
#include "plug-ins/pdb/PDB.h"    // req. for Procedure_DB::PluginType
#include "plug-ins/pdb/World.h"
#include <list>
#include <string>

class CPDAG;
class CPDAGNode;
class CPParamList;
class PluginParam;

/**
 * CPDAGEvaluator provides the evaluation method for evaluating node within a CPDAG.
 *
 * @TODO some of this class could likely be fed back into the PluginExecutioner
 */
class CINEPAINT_CORE_API CPDAGEvaluator
{
	public:
		/**
		 * Constructs a new CPDAGEvaluator for the specified CPDAG
		 *
		 * @param dag the CPDAG this CPDAGEvaluator will evaluate nodes from
		 */
		CPDAGEvaluator(CPDAG& dag);

		/**
		 * Destructor
		 *
		 */
		virtual ~CPDAGEvaluator();

		/**
		 * Evaluates the named node within the CPDAG specified in the constructor
		 *
		 * @param node_name the node name to evaluate
		 * @param reval set true to re-evaluate a non-dirty node
		 * @return true if the nodes exists within the CPDAG and evaluation was a success,
		 *         false otherwise
		 */
		bool EvaluateNode(const std::string& node_name, bool reval,World* world);


		/** default modifier to a param name */
		static const char* IN_PARAM_NAME_MODIFIER;

	protected:
	private:

		/**
		 * Evaluates the specified parameter node
		 * Evaluation of a parameter node involves setting the default evaluation of the
		 * node to its data value
		 *
		 * @param node_name the name of the node to be evaluated
		 * @param node the node to be evaluated
		 * @return true if the node was evaluated sucessfully, false otherwise
		 */
		bool evaluate_param_node(const std::string& node_name, CPDAGNode& node);

		/**
		 * Evaluates the specified plugin node
		 * Evaluation of a parameter node involves building the plugin parameter lists
		 * using conected graph nodes, runing the plugin and capturing the results, and setting
		 * evaluated values as appropriate within the node
		 *
		 * @param node_name the name of the node to be evaluated
		 * @param node the node to be evaluated
		 * @return true if the node was evaluated sucessfully, false otherwise
		 */
		bool evaluate_plugin_node(const std::string& node_name, CPDAGNode& node,World* world);



		/**
		 * Struct representing a parameter that has been promoted/demoted prior to evaluation of a plugin
		 *
		 */
		struct PromotionRec
		{
			std::string m_param;          // parameter name
			CPPluginArgType m_from_type;  // orignal type of param prior to promotion/demotion
			CPPluginArgType m_to_type;    // new type of param after promotion/demtoion
		};

		typedef std::list<PromotionRec> PromotionList_t;


		/**
		 * Populates the plugin input param list with connected graph node data prior to running a plugin
		 * The conneted graph nodes which provide input parametes to the plugin are processed and
		 * added to the param list.
		 * If parameter types do not match those specified by the plugin parameter definitions,
		 * an attempt is made to convert the type to the required type, and the conversion is recorded
		 * within the promotion_list.
		 * If the plugin modfies the input parameter, a copy of the parameter is made and the copy
		 * is added to the param list.
		 *
		 * @param plugin the plugin to be run
		 * @param eval_name the node name of the node to be evaluated
		 * @param params the input param list
		 * @param promotion_list populated with details of converted parameter types
		 */
		void populate_plugin_param_list(const CinePaintPlugin& plugin, const std::string& eval_name, CPParamList& params, PromotionList_t& promotion_list);

		/**
		 * Unpopulates the plugin input param list using connected graph node data.
		 * Removes any data item added to the plugin input param list that was added
		 * from connected graph nodes
		 *
		 * After a plugin has been run, the input param list is no longer required. However
		 * once the param list is released, any data item remaining in the list are released. As some
		 * of those data item may have come from connected graph nodes, the data item needs to be removed
		 * and remain managed by the holding node.
		 *
		 * Any data item that was created during populate_plugin_param_list, for exmaple, for a data item
		 * that plugin modifies, the item is removed from the input param list and added to the return
		 * param list.
		 *
		 * The promotion_list is used to determine any data item that had ist type modified during
		 * populate_plugin_param_list, and so the types correspond in the removal process.
		 *
		 * @param plugin the plugin
		 * @param eval_name the node name of the node to be evaluated
		 * @param params the input param list
		 * @param ret_params the return param list.
		 * @param promotion_list populated with details of converted parameter types
		 */
		void unpopulate_plugin_param_list(const CinePaintPlugin& plugin, const std::string& eval_name, CPParamList& params, CPParamList& ret_params, PromotionList_t& promotion_list);

		/**
		 * Adds the specified param value, with the specified parameter namd to the input param list of a plugin
		 * This method is a convenience method to determine the best away to add the param value
		 * to the plugins input parameter list.
		 * If the param value is modified by the plugin, the param is added using add_to_param_list_modified
		 * If the param value type does not match that as defined by the plugins parameter definitions,
		 * the param is added using add_to_param_list_promoted.
		 * Otherwise the param value is simply added using add_to_param_list
		 *
		 * @param plugin the plugin to be run
		 * @param params the input parameter list
		 * @param param_name the parameter name
		 * @param param the parameter value
		 * @param promotion_list populated with details of converted parameter types 
		 */
		void add_to_param_list(const CinePaintPlugin& plugin, CPParamList& params, const std::string& param_name, PluginParam& param, PromotionList_t& promotion_list);

		/**
		 * Adds the spcified param value, with the specified parameter namd to the input param list of a plugin
		 *
		 * @param params the input parameter list
		 * @param param_name the parameter name
		 * @param param the parameter value 
		 */
		void add_to_param_list(CPParamList& params, const std::string& param_name, PluginParam& param);

		/**
		 * Creates a copy of the specified param value and adds the copy to the input param list of a plugin
		 *
		 * @param params the input parameter list
		 * @param param_name the parameter name
		 * @param param the parameter value 
		 */
		void add_to_param_list_modified(CPParamList& params, const std::string& param_name, PluginParam& param);

		/**
		 * Attempts to add the spcified param value to the param list.
		 * This method should be used if the expected param type does not match the current param type.
		 * An attemp is made to convert the plugin param into the expected type, and the converted type
		 * is added to the param list. The conversion is recorded within the promotion_list.
		 *
		 * @param params the input parameter list
		 * @param param_name the parameter name
		 * @param param the parameter value
		 * @param param_def the parameter definition expected by the plugin
		 * @param promotion_list populated with details of converted parameter types 
		 */
		int add_to_param_list_promoted(CPParamList& params, const std::string& param_name, const PluginParam& param, const CinePaintPlugin::CPPluginParamDef& param_def, PromotionList_t& promotion_list);

		/**
		 * Sets the evaluated nodes evaluations with return data from the plugin
		 * Any parameters returned from the plugin, that are referenced by connected nodes within
		 * the CPDAG are coolcted and added as evaluation data to the evaluated node.
		 * Any returned parameters not referenced by existing connected node are not collected
		 * from the plugins return params, as they are not considered to be required.
		 *
		 * @param node_name name of the node being evaluated
		 * @param node the node being evaluated
		 * @param ret_params the return list from the plugin
		 */
		void process_plugin_return_params(const std::string& node_name, CPDAGNode& node, CPParamList& ret_params);

		/**
		 * Process any remaining plugin return parameters to determine the default evaluation value of a node.
		 * The defaule evaluation value of the node is the highest ranking data value.
		 * CinePaintImage -> Drawable -> AbstractBuf -> string -> float -> int
		 * If the node already has the highest ranking data value, no further action is taken, otherwise
		 * the remaining params are searched to determine if a higher ranking item remains uncollected.
		 *
		 * This process allows an image data item that is not referenced by any connecting graph nodes to be
		 * collected and displayed as the returns of an evaluated cell.
		 *
		 * @param node the node being evaluated
		 * @param ret_params the plugin return param list
		 */
		void process_default_evaulation(CPDAGNode& node, CPParamList& ret_params);

		/**
		 * Convenience method to return the named parameter defiintion from the specified plugin
		 * If the named parameter definition does not exist, 0 is returned
		 *
		 * @param plugin the CinePaintPlugin from which to retrieve the parameter definition
		 * @param param_name the name of the parameter definition to retrieve
		 * @return the named parameter definition if specified, otherwise 0
		 */
		const CinePaintPlugin::CPPluginParamDef* get_plugin_param_def(const CinePaintPlugin& plugin, const std::string& param_name);

		/**
		 * Verify that the specified param list contains the defined inward parameters defined by the specified plugin
		 * This method accesses the parameter definition of the speciied plugin, and checks that each parameter
		 * is met by the specified param list.
		 * If each of the parameters expected by the plugin, as defined by the plugins parameter definitions, is
		 * presents and of the cirrect type, this methos returns true, otherwise this method returns false, and
		 * the plugin cannot be run as it does not have sufficient parameters
		 * 
		 * @param plugin the plugin to check parameters against
		 * @param params the param list containing the parameter to be passed to the plugin
		 * @return true if the param lkist contains all required parameters, false otherwise
		 */
		bool verify_param_list(const CinePaintPlugin& plugin, const CPParamList& params);


		/**
		 * Attempts to guess the type of a plugin from its name.
		 * This method checks the plugin name to determine if it contains the words
		 * load or save. If so, the chances are that the named plugin is a load or save
		 * handler. Otherwise, the plugin is expecte to be of standard plugin type.
		 * This data is then used to search the Procedure_DB to determine if the named plugin
		 * exist wihin the respective database. If the plugin is not found the databases for
		 * remaining plugin types are searched for the named plugin. If the plugin is found the
		 * plugin type is returned, otherwise Procedure_DB::NO_TYPE_ENUM is returned
		 * This method is unreliable because a plugin may be registered within the database
		 * for each plugin type with the same name
		 *
		 * @param name the plugin name
		 * @return the type of the plugin, or Procedure_DB::NO_TYPE_ENUM if the type cannot be found.
		 */
		PDB::PluginType guess_plugin_type_from_name(const std::string& name,World* world);

		//--------
		// Members

		/** Reference to the CPDAG this CPDAGEvaluator evaluates */
		CPDAG& m_dag;
};

#endif /* _CPDAGEvaluator_H_ */
