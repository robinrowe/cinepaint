/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      MenuFunctorHelper - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: MenuFunctorHelper.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "MenuFunctorHelper.h"
#include <app/CinePaintFunctor.h>

#include <FL/Fl_Menu_.H>

#include <algorithm>
#include <string>

/**
 * Constructs a new MenuFunctorHelper to manage created Functor Objects
 *
 */
MenuFunctorHelper::MenuFunctorHelper()
{}

/**
 * Destructor - destroys all managed CinePaintFunctors
 * Note that destroying Functors does not destroy the object they reference.
 */
MenuFunctorHelper::~MenuFunctorHelper()
{
	FunctorContainer_t::iterator _iter = m_managed_functors.begin();

	while(_iter != m_managed_functors.end())
	{
		FunctorContainer_t::iterator _next = _iter;
		_next++;

		AbstractFunctor* _af = _iter->second;
		delete _af;
		_af = 0;

		m_managed_functors.erase(_iter);
		_iter = _next;
	}
}

/**
 * Manages the specified Functor Object
 * Ownership of the Functor passes to this class, and the object will be destroyed when this class
 * is destroyed
 *
 * @param path the menu path to the menu item the functor is the callback for
 * @param functor the Functor to manage
 */
bool MenuFunctorHelper::ManageFunctor(const char* path, AbstractFunctor* functor)
{
	bool _ret = false;

	if(m_managed_functors.find(std::string(path)) == m_managed_functors.end())
	{
		m_managed_functors.insert(std::pair<std::string, AbstractFunctor*>(std::string(path), functor));
		_ret = true;
	}

	return(_ret);
}

/**
 * Unmanages the Functor represented with the specified menu path
 * If release is set true, the functor is automatically deleted, and this method returns 0.
 * If no Functor is managed for the specified path, 0 is returned, otherwise the managed
 * functor is returned. Ownership passes to the caller who must manage the resources of the objeect.
 *
 * @param path the menu path representing the functor
 * @param release set true to automatically delete the functor
 * @return the functor if found, of 0 if found and release is set true, otherwise 0 if not found
 */
AbstractFunctor* MenuFunctorHelper::UnmanageFunctor(const char* path, bool release)
{
	AbstractFunctor* _ret = 0;

	FunctorContainer_t::iterator _iter = m_managed_functors.find(std::string(path));

	if(_iter != m_managed_functors.end())
	{
		AbstractFunctor* _functor = _iter->second;
        m_managed_functors.erase(_iter);

		if(release)
		{
			delete _functor;
			_functor = 0;
		}
		else
		{
			_ret = _functor;
		}
	}

	return(_ret);
}

/**
 * Returns the Functor representing the specified menu path
 * If no Functor is managed with the specified path, 0 is returned
 * The functor resources remain managed by this class.
 *
 * @param path the menu path representing the functor
 * @return the managed Functor, or 0 if the functor is not found
 */
AbstractFunctor* MenuFunctorHelper::getFunctor(const char* path) const
{
	AbstractFunctor* _ret = 0;
	FunctorContainer_t::const_iterator _iter = m_managed_functors.find(std::string(path));

	if(_iter != m_managed_functors.end())
	{
		_ret = _iter->second;
	}

	return(_ret);
}

/**
 * Static callback used by all added Manu Items.
 * This method is set as the callback for all added menu items, passing A Functor
 * as the void* data parameter. The Functor is then invoked.
 * This method is automatically set as the menu item callback for all items added with
 * AddMenuItem, however the method is also available for client use, where a user
 * functor is passed as the addditional data parameter.
 * The additional data is assumed to be an AbstractFunctor.
 *
 * @param w the fltk menu
 * @param d the functor to call
 */
void MenuFunctorHelper::static_menu_item_callback(Fl_Widget* w, void* d)
{
	AbstractFunctor* _functor = reinterpret_cast<AbstractFunctor*>(d);
	if(_functor)
	{
		(*_functor)();
	}
}
