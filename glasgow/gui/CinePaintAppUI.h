/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintAppUI - Main user interface class
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintAppUI.h,v 1.5 2006/12/21 11:18:06 robinrowe Exp $
 */

#ifndef _CINEPAINTAPPUI_H_
#define _CINEPAINTAPPUI_H_

#include "dll_api.h"
#include <FL/Fl.H>
#include <pthread.h>
#include "Version.h"
#include "gui/DocViewManager.h"
#include "gui/InstallDialog.h"
#include "gui/WidgetBuilder.h"
#include "app/CinePaintEventList.h"
#include "app/CinePaintEvent.h"
#include "plug-ins/pdb/CinePaintProgressListener.h"
#include "plug-ins/pdb/World.h"
#include "app/ColorManager.h"
#include "app/ColorChangeListener.h"

// forward declarations
class BrushSelectionDialog;
class CommandLineParse;
class CinePaintApp;
class CinePaintEventList;
class ColorSelectionDialog;
class InitDialog;
class MainToolBar;
class PluginBrowser;
class ToolOptionsDialog;
class Fl_Window;

/**
 * Main application class used to manage applicaton level data
 * parse the command line, initalise GUI, etc...
**/
class CINEPAINT_GUI_API CinePaintAppUI
{	
public:
	/**
	 * Default Destructor
	**/
	virtual ~CinePaintAppUI();



	//============================
	// Application install / Setup

	/**
	 * Show the help information.
	 */
	void ShowHelp();

	/**
	 * Display the version
	 */
	void ShowVersion();

	/**
	 * Starts the main UI and waits for the UI event loop to exit.
	 * This method starts the toolkit event loop. The current thread becomes the
	 * event loop thread. This method returns when the UI event loop has completed,
	 * i.e. when all windows are closed.
	 * This method waits for the CinePaint Core to initialize before displaying the
	 * UI. Run must be called upon CinePaintApp before attempting to start the UI.
	 *
	 * @return non-zero on error
	 *         1 == already running
	 */
	virtual int Run(CommandLineParse& clp);



	//===============================
	// Tool Dialog Show/Close methods

	/**
	 * Sets the visibility of the Brush Selection Dialog
	 *
	 * @param visible visibility of the dialog
	 */
	void SetBrushSelectionDialogVisible(bool visible);

	/**
	 * Sets the visibility of the color selection dialog
	 *
	 * @param visible visibility of the dialog
	 * @param color_type set the color type the color dialog is used to update
	 */
	void SetColorSelectionDialogVisible(bool visible);
	void SetColorSelectionDialogVisible(bool visible, ColorChangeListener::ColorType color_type);

	/**
	 * Sets the visibility of the Pallettes Dialog
	 *
	 * @param visible visibility of the dialog
	 */
	void SetPalletteDialogVisible(bool visible);

	/**
	 * Sets the visibility of the Gradient Editor Dialog
	 *
	 * @param visible visibility of the dialog
	 */
	void SetGradEditorDialogVisible(bool visible);

	/**
	 * Sets the visibility of the Tool Options Dialog
	 *
	 * @param visible visibility of the dialog
	 */
	void SetToolOptionsDialogVisible(bool visible);

	/**
	 * Sets the visibility of the Device Status Dialog
	 *
	 * @param visible visibility of the dialog
	 */
	void SetDeviceStatusDialogVisible(bool visible);


	/**
	 * Sets the plugin browser window visible
	 *
	 * @param visible visibility of the plugin browser
	 */
	void SetPluginBrowserVisible(bool visible);


	//==========================================
	// Accessor method for widget builder object
	WidgetBuilder &GetWidgetBuilder() { return m_widget_builder; };

	//===========================
	// Object to assist in building dialog UIs.
	WidgetBuilder m_widget_builder;


	//--------------
	// AppUI Control 

	void Quit();


	//-----------------
	// Document Control

	void NewDocument();
	void NewSpreadSheet();
	void NewFrameManager();

	void OpenDocument();
	bool SaveDocument(CinePaintDoc& doc);
	bool SaveAsDocument(CinePaintDoc& doc);
	void RevertDocument(CinePaintDoc& doc);

	/**
	 * Query the user if the specified document should be saved, and saves ths Document if appropriate.
	 * This method checks if the specified Document is dirty, and will query the user to save it if
	 * appropriate, returning a release status for the document. If the user saves the document, or chooses
	 * to discard any changes made, the document can be safely released, and this method will return true.
	 * If the user cancels the operation, the document should not be released and false is returned.
	 * 
	 * @note This method does not release the document, it is used to query if a document can be released.
	 *       this allows us to display the appropriate dialogs before we release the last reference to a
	 *       document
	 *.
	 * @param doc the Document to save and release.
	 */
	bool ReleaseAndSaveDocument(CinePaintDoc& doc);


	DocViewManager& GetDocViewManager();


	//===============
	// Get Thread ID.

	/**
	 * Returns the FLTK UI event list
	 * The returned event list is processes within the FLTK event thread. This provides a convenient
	 * mechanism for arbitary threads to provide a process to be run within the FLTK event thread
	 * Note that the event list is not real time, but executed after all other events within the
	 * idle loop
	 *
	 * @return UI thread event process list
	 */
	CinePaintEventList& GetUIThreadEventList();
	
	/**
	 * Returns the thread ID of the UI thread
	 * The thread ID may be used to determine if a process is currently running within the
	 * UI thread or another thread. This is particularly important for updating the UI,
	 * ALL UI updates must take place within the UI thread.
	 *
	 * @return the thread ID if the UI thread
	 */
	pthread_t GetUIThreadID() const { return m_threadID; };

	/**
	 * Returns true if the calling thrad is running within the UI thread.
	 * This allows a method to determine if it is currently running within the UI thread, which
	 * is particularly important for updating the UI,
	 * ALL UI updates must take place within the UI thread.
	 *
	 * If Run has not yet been called upon CinePaintAppUI, the UI thread is currently invalid. In
	 * this case, false is always returned
	 * 
	 * @return true if the calling thread is running within the UI thread, false otherwise
	 */
	static bool IsUIThread();



	//===============
	// Static Methods

	/**
	 * Returns the singleton instance of the CinepaintApp
	 * @return the CinePaintApp isntance
	 */
	static CinePaintAppUI& GetInstance();

private:

	//====================
	// Private Constructor

	/**
	 * Default Constructor
	 */
	CinePaintAppUI();

	//====================
	// Instalation Methods

	/**
	 * Static FLTK callback to initiate the UI initialization
	 * We do this as a callback so we can hook-up the callback, and then start the
	 * main FLTK event loop immediatly.
	 *
	 * @param data the Commend Line Arguments
	 */
	static void static_init_callback(void* data);

	/**
	 * UI initialization
	 * Performs the User installation, if required
	 * If the core is initialized, show the UI.
	 * If the CinePaintApp Coreis not currently initialized, Create an InitializationListener
	 * and Initialize the core. The InitializationListener handles showing the UI once the 
	 * core initialization is complete.
	 *
	 */
	void init(CommandLineParse& clp);



	//===================================
	// general Application helper methods

	void show_ui();

	//==============
	// UI Components 

	/** the main toolbar/menubar */
	MainToolBar* m_mMainToolBar;


	//=================
	// Init Event Listener Class.
	class InitializationListener : public CinePaintProgressListener
	{
		public:
			InitializationListener(CinePaintAppUI& ui);
			virtual ~InitializationListener();
			

			void ProgressStart(char* ID, char* msg, int stop, int step);
			void MessageChanged(char* ID, char* msg );
			void LimitsChanged(char* ID, int start, int stop, int step);
			void ProgressUpdate(char* ID );
			void ProgressReset(char* ID );
			void ProgressPosition(char* ID, int pos);
			void ProgressComplete(char* ID);

		protected:
		private:
			void initialization_start();
			void initialization_complete();
			
			/** reference to the CinePaintAppUI */
			CinePaintAppUI& m_ui;

			/** initialization dialog/splash/load screen */
			InitDialog* m_init_dialog;
	};

	/** manages displayed view over CinepaintDocs */
	DocViewManager m_doc_view_manager;

	// Tool Dialogs

	// indicates whether dialogs are maintained upon a close(hide) or deleted
	// this flag allows the dialog to be deleted, or just hidden when a user click the
	// closeing 'x' on the dialog window. This allows us to maintain the dialog if the
	// user closes it, and show again anext time, or rebuild it again as required.
	bool m_maintain_dialog_resource ;

	/** the brush selection dialog */
	BrushSelectionDialog* m_brush_selection_dialog;

	/** Color Selection Dialog */
	ColorSelectionDialog* m_color_selection_dialog;

	/** Tool options dialog */
	ToolOptionsDialog* m_tool_options_dialog;

	/** plugin browser window */
	Fl_Window* m_plugin_browser;


	//--------------------------------------
	// Callbacks for closing various dialogs

	static void static_close_tool_dialog_callback(Fl_Widget *w, void* d) { reinterpret_cast<CinePaintAppUI*>(d)->SetToolOptionsDialogVisible(false); }
	static void static_close_plugin_browser_cb(Fl_Widget *w, void* d) { reinterpret_cast<CinePaintAppUI*>(d)->SetPluginBrowserVisible(false); }


	//========================
	// UI Event Handling Loop

	/**
	 * CinePaint UI extension of a CinePaintEventList
	 * This class extends CinePaintEventList to provide access to the FLTK idle
	 * loop for processing items added to the list.
	 * CinePaintUIEventList automatically adds itself to th FLTK idle loop upon
	 * adding a CinePaintEvent to be processed, and removes itself once there are
	 * no more events to process. This solves the case where the idle loop repeatedly
	 * loops and consuming CPU time where there are no event to process.
	 *
	 */
	class CinePaintUIEventList : public CinePaintEventList
	{
		public:
			/**
			 * Default constructor, constructs an intially inactive ui event list
			 */
			CinePaintUIEventList();

			// default destructor is ok

			/**
			 * Process the first element within this Event list.
			 * If after processing an event, the event list is empty, SetActive(false)
			 * is called to remove this event list processing from the FLTK idle loop
			 *
			 * @return true if any events were processed, false otherwise
			 */
			bool Process();

			/**
			 * Post an event into the event list to be processed
			 * If this event list is not already active and processing the list, SetActive(true)
			 * is called process this list within the FLTK idle loop
			 *
			 * @param e the event to add
			 * @return the number of item within the list
			 */
			size_t PostEvent(CinePaintEvent* e);

			/**
			 * Sets the processing state of this event list
			 * If active is set true, this event list is added to the FLTK idle loop
			 * for processing the event list, if not already added, if false, it is removed
			 * from the idle loop.
			 *
			 * @param active the activce state of this event loop
			 */
			void SetActive(bool active);

		protected:

		private:

			/** indicate sthe active/processing state of this event list */
			bool m_active;

			/**
			 * FLTK Idle loop callback to process the event list
			 *
			 * @param data instance of CinePaintUIEventList to process
			 */
			static void static_ui_event_loop_cb(void* data);
	};

	/** Event list to process within the UI event loop */
	CinePaintUIEventList m_event_list;


	//===============
	// Static Members

	/** The Thread_id which the UI is running under. */
	static pthread_t m_threadID;

	/** A pointer to the initialization listener so it can be deleted. */
	InitializationListener* m_init_listener;

	/** indicates if Run has been called upon this CinePaintAppUI */
	bool m_run_flag;

	/** the singleton CinePaintApp instance */
	static CinePaintAppUI* m_app_instance ;


};

#endif /* _CINEPAINTAPPUI_H_ */
