/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      FrameManagerWindow - FrameManager UI
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: FrameManagerWindow.h,v 1.2 2006/12/21 11:18:06 robinrowe Exp $
 */

#ifndef _FRAME_MANAGER_WINDOW_H_
#define _FRAME_MANAGER_WINDOW_H_

#include "dll_api.h"
#include <FL/Fl_Window.H>
#include <FL/Fl_Table_Row.H>
#include <app/FrameManagerListener.h>
#include <gui/CinePaintWidgetListener.h>
#include <plug-ins/pdb/World.h>
#include <string>

class Fl_Menu_Bar;
class FrameManager;
class FrameStore;
class MenuFunctorHelper;
class SimpleDisplayWindow;
class FlipFrameController;
class FlipFrameDisplayHandler;

class CINEPAINT_GUI_API FrameManagerWindow : public Fl_Window
{
	public:
		/**
		 * Constructs a new FrameManagerWindow to disply the specified FrameManager
		 * The FrameManagerWindow provides a sinple UI to the FrameManager allowing addition,
		 * removal and re-ordering along with the controls for a simple flipbook presentation
		 *
		 * @param w window width
		 * @param h window height
		 * @param title window title
		 * @param fm FrameManaget to display
		 */
		FrameManagerWindow(int w, int h, const char* title, FrameManager& fm);

		/**
		 * Destructor
		 */
		virtual ~FrameManagerWindow();

		/**
		 * Sets the selected row upon this FrameManger display window
		 * All other rows are deselected
		 *
		 * @param index the index of the row to set selected.
		 */
		void SetSelected(size_t index);

		/**
		 * Returns the FrameManager being displayed by this FrameManagerWindow
		 *
		 * @return the FrameManager being displayed
		 */
		FrameManager& GetFrameManager() const;

	protected:

	private:

		//---------------
		// Nested Classes

		/**
		 * Table providing the main view over the FrameManager records
		 *
		 */
		class FrameManagerTable : public Fl_Table_Row
		{
			public:
				FrameManagerTable(int x, int y, int w, int h, const char* title, FrameManager& fm);
				virtual ~FrameManagerTable();

				static const int TABLE_COLS;

				enum TableColType {
				    FILENAME_ENUM = 0,
				    ID_ENUM = 1,
				    LOADED_ENUM = 2,
				    PRE_LOADED_ENUM = 3,
				    MODIFIED_ENUM = 4,
				    ACTIVE_ENUM = 5,
			};

			protected:
				void draw_cell(TableContext context, int row, int col, int x, int y, int w, int h);

			private:

				char** m_col_headers;
				FrameManager& m_frame_manager;
		};

		/**
		 * FrameManagerListener to respond to changes within the FrameManager and update the FrameManagerWindow
		 *
		 */
		class FrameListener : public FrameManagerListener
		{
			public:
				FrameListener(FrameManagerWindow& win);

				virtual void SelectionChanged(const FrameManager::FrameRecord* frame, size_t position);
				virtual void FrameAdded(const FrameManager::FrameRecord& frame, size_t position);
				virtual void FrameRemoved(const FrameManager::FrameRecord& frame, size_t position);
				virtual void FrameUpdated(const FrameManager::FrameRecord& frame, size_t position);
				virtual void FrameIndexChanged(const FrameManager::FrameRecord& frame, size_t old_pos, size_t new_pos);

			protected:
			private:
				FrameManagerWindow& m_win;
		};

		/**
		 * CinePaintWidgetListener implementation to respond to changes in soze of the display window
		 *
		 */
		class DisplayResizeHandler : public CinePaintWidgetListener
		{
			public:
				DisplayResizeHandler(FrameStore& store);
				virtual ~DisplayResizeHandler();

				virtual void WidgetResized(int w, int h);

			protected:
			private:
				FrameStore& m_frame_store;
		};









		//--------
		// Widgets

		/** main Frame display widget */
		FrameManagerTable* m_frame_view;

		MenuFunctorHelper* m_menu_functor_helper;
		void build_menu(Fl_Menu_Bar& menu, MenuFunctorHelper& menu_helper);



		//-----------------
		// Widget Callbacks

		/** @todo remove once we have all the menu callbacks in in place */
		void DUMMY_CB();

		static void static_frame_view_cb(Fl_Widget* w, void* d) { reinterpret_cast<FrameManagerWindow*>(d)->frame_view_cb(); }

		static void static_step_back_cb(Fl_Widget* w, void* d) { reinterpret_cast<FrameManagerWindow*>(d)->step_back_cb(); }
		static void static_play_backward_cb(Fl_Widget* w, void* d) { reinterpret_cast<FrameManagerWindow*>(d)->play_backward_cb(); }
		static void static_stop_cb(Fl_Widget* w, void* d) { reinterpret_cast<FrameManagerWindow*>(d)->stop_cb(); }
		static void static_play_cb(Fl_Widget* w, void* d) { reinterpret_cast<FrameManagerWindow*>(d)->play_cb(); }
		static void static_step_forward_cb(Fl_Widget* w, void* d) { reinterpret_cast<FrameManagerWindow*>(d)->step_forward_cb(); }

		static void static_add_file_source_cb(Fl_Widget* w, void* d) { reinterpret_cast<FrameManagerWindow*>(d)->add_file_source_cb(); }

		static void static_shift_up_cb(Fl_Widget* w, void* d) { reinterpret_cast<FrameManagerWindow*>(d)->shift_up_cb(); }
		static void static_shift_down_cb(Fl_Widget* w, void* d) { reinterpret_cast<FrameManagerWindow*>(d)->shift_down_cb(); }
		static void static_add_frames_cb(Fl_Widget* w, void* d) { reinterpret_cast<FrameManagerWindow*>(d)->add_frames_cb(); }
		static void static_remove_frame_cb(Fl_Widget* w, void* d) { reinterpret_cast<FrameManagerWindow*>(d)->remove_frames_cb(); }


		void frame_view_cb();

		void step_back_cb();
		void play_backward_cb();
		void stop_cb();
		void play_cb();
		void step_forward_cb();

		void add_file_source_cb();

		void shift_up_cb();
		void shift_down_cb();
		void add_frames_cb();
		void remove_frames_cb();

		void close_view_cb();

		void handle_cell_selection(int row, int col);





		/** the FrameManager this to display */
		FrameManager& m_frame_manager;

		/** listener implementation for responding to changes within the FrameManager */
		FrameListener* m_frame_manager_listener;

		/** provides image pre-loading for the FrameManager */
		FrameStore* m_frame_store;

		/** controls the flip book/frame flipping presentation */
		FlipFrameController* m_frame_controller;

		/** handles updating the disply during frame flipping */
		FlipFrameDisplayHandler* m_display_handler;

		/** display the current image during frame flipping */
		SimpleDisplayWindow* m_display_window;

		/** Component listener to update the FrameStore on the curent size of the display wondow */
		DisplayResizeHandler* m_display_resize_handler;

		/** last accessed directory for adding a frame source */
		std::string m_prev_file_select_dir;

};

#endif /* _FRAME_MANAGER_WINDOW_H_ */
