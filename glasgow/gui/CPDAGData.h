/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CPDAGData - Directed acyclic graph data unit
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Stuart Ford
 *   Colin Law
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * $Id: CPDAGData.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _CPDAGDATA_H_
#define _CPDAGDATA_H_

#include "dll_api.h"
#include <utility/CPSerializable.h>
#include <string>

class AbstractBuf;
class CPSocket;
class PluginParam;

/**
 * A wrapper class for storing the various different types of data that
 * Cinepaint accepts as arguments in its image processing chain, including
 * names of other plugins.
 *
 * A CPDAGData object either holds a parameter value to a plugin, or the
 * name of a plugin to be called. If this CPDAGData holds a parameter
 * value to a plugin, the data itself is a PluginParam, which 
 * indicates its parameter type and value.
 */
class CINEPAINT_CORE_API CPDAGData : public CPSerializable
{
		// Friends
		friend class CPDAG;


	public:

		/** The type of value that can be held in a CPDAGData object. */
		enum DataType
		{
		    DAG_PLUGINPARAM,    // plugin parameter type
		    DAG_PLUGIN          // plugin type
		};


		//-------------
		// Constructors

		/**
		 * Constructor. Creates an object with no data.
		 */
		CPDAGData();

		/**
		 * Constructor. Creates an object with the given integer data.
		 *
		 * @param data the integer to be stored in this node
		 */
		CPDAGData(int data);

		/**
		 * Constructor. Creates an object with the given double data.
		 *
		 * @param data the float to be stored in this node
		 */
		CPDAGData(float data);

		/**
		 * Constructor. Creates an object with the given C-string data.
		 * The type parameter is used to differentiate between a plugin name string
		 * and a plugin parameter string value in orde to set the data type of this
		 * CPDAGData correctly
		 * In both cases, the string value is copied and managed by this CPDAGData
		 *
		 * @param data the C-string to be stored in this node
		 * @param type the data type if this CPDAGNode
		 */
		CPDAGData(const char* data, DataType type);

		/**
		 * Constructor. Creates an object with the given AbstractBuf data.
		 *
		 * @param data the AbstractBuf pointer to be stored in this node
		 */
		CPDAGData(AbstractBuf* data);

		/**
		 * Constructor. Creates data object by deserializing from the given socket.
		 *
		 * @param socket the socket to read from
		 */
		CPDAGData(CPSocket& socket);

		/**
		 * Destructor. If a data object was created by deserialization, the data in
		 * it will be destroyed when the object is destroyed.
		 *
		 */
		virtual ~CPDAGData();



		//---------------------
		// Accessors / Mutators

		/**
		 * Returns the data type held by this CPDAGData
		 *
		 * @return	the data type held by this CPDAGData
		 */
		CPDAGData::DataType GetType() const;

		/**
		 * Returns the plugin name held by this CPDAGData
		 * If GetType() is not DAG_PLUGIN this method rturns 0
		 *
		 * @return the name of the plugin held by this CPDAGData, or 0 if this CPDAGData
		 *         does not hold a plugin name
		 */
		const char* GetPluginName() const;

		/**
		 * Returns the PluginParam holding the parameter value to a plugin.
		 * if this CPDAGData does not hold a plugin parameter, ie if GetType() does not return 
		 * DAG_PLUGINPARAM, this method returns 0
		 *
		 * @return the PluginParam holding a parameter to a plugin, or 0 if this CPDAGData does
		 *         not hold a plugin parameter
		 */
		const PluginParam* GetPluginParam() const;




		/**
		 * Serialize and write data to a given socket.
		 * @param socket	[i] the socket to write the data to
		 */
		void Serialize(CPSocket& socket) const;

		/**
		 * Read and deserialize data from a given socket.
		 * @param socket	[i] the socket to read the data from
		 */
		void Deserialize(CPSocket& socket);

	protected:

		/**
		 * Sets the data held within this CPDAGData to the specified int value
		 * The data type of this CPDAGData is set to DAG_PLUGINPARAM
		 *
		 * @param data the int value held by this CPDAGData
		 */
		void set(int data);

		/**
		 * Sets the data held within this CPDAGData to the specified float value
		 * The data type of this CPDAGData is set to DAG_PLUGINPARAM
		 *
		 * @param data the float value held by this CPDAGData
		 */
		void set(float data);

		/**
		 * Sets the data held within this CPDAGData to the specified string
		 * The specified string is copied by this CPDAGData, and the holding
		 * PluginParam is set to manage the copied string.
		 * The data type of this CPDAGData is set to DAG_PLUGINPARAM
		 *
		 * @param data the string value held by this CPDAGData
		 */
		void set(const char* data);

		/**
		 * Sets the data held within this CPDAGData to the specified AbstractBuf
		 * The PluginParam holding the actual data value is initally set to
		 * NOT manage the specified AbstractBuf.
		 * The data type of this CPDAGData is set to DAG_PLUGINPARAM
		 *
		 * @param data the AbstractBuf held by this CPDAGData
		 */
		void set(AbstractBuf* data);

		/**
		 * Sets the plugin name held within this CPDAGData to the specified string
		 * The char* is copied and managed by this CPDAGData
		 * The data type of this CPDAGData is set to DAG_PLUGIN
		 *
		 * @param data the int value held by this CPDAGData
		 */
		void set_plugin(const char* data);


	private:

		/**
		 * A type representing the value of data held in a CPDAGData object.
		 */
		union
		{
			PluginParam* m_param;    // plugin parameter
			char* m_plugin;          // plugin name
		};


		/**
		 * Indicates the type of data in an object, either a plugin or a plugin
		 * parameter.
		 */
		DataType m_type;

		/**
		 * Convenience method to set the data type helf by this CPDAGData
		 *
		 * @param type the data type held by this CPDAGData
		 */
		void set_type(DataType type);

		/**
		 * Indicates whether this object was created by deserialization. If it was
		 * then this object owns the data within it, and will destroy this data when
		 * it is destroyed itself.
		 */
		bool m_deserialized;

}; // class CPDAGData


#endif // _CPDAGDATA_H_


