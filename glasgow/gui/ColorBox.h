/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ColorBox - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ColorBox.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _COLOR_BOX_H_
#define _COLOR_BOX_H_

#include "dll_api.h"
#include <FL/Fl_Widget.H>
#include <plug-ins/pdb/Color.h>
#include <app/ColorChangeListener.h>

// Forward declaration
class ColorManager;

/**
 * Simple foreground/background Color indicator for a specified ColorManager
 *
 * @todo This is a bespoke widget, that knows what to do upon events, although it might be usefull
 *       to separate this into the widget and handler, it isn't required for CinePaint.
 */
class CINEPAINT_GUI_API ColorBox : public Fl_Widget, public ColorChangeListener
{
	public:
		/**
		 * Constructs a new ColorBox using the specified ColorManager for initialization values
		 * The ColorBox constructor registers the newly constructed ColorBox as a ColorChangeListener
		 * of the specified ColorManager to recieve notifications of color change events.
		 *
		 * @param x widget x position
		 * @param y widget y position
		 * @param w widget width
		 * @param h widget height
		 * @param color_manager
		 */
		ColorBox(int x, int y, int w, int h, ColorManager& color_manager);

		/**
		 * Destructor
		 * The ColorBox is removed as a ColrChangeListener from the ColorManager
		 */
		virtual ~ColorBox();

		void draw();
		virtual int handle(int event);


		//-----------------------------------
		// ColorChangeListener implementation

		/**
		 * Invoked when the a Color change is made by the ColorManager
		 *
		 * @param color_type the color which has changed
		 * @param color the new color
		 */
		virtual void ColorChanged(ColorType color_type, const Color& color);

	protected:

	private:
		/** manages the current foregound/background colors */
		ColorManager& m_color_manager;

		/**
		 * Processes a click up0on this widgets and updates the ColorManager as required
		 *
		 */
		void process_click();

}; /* class ColorBox */


#endif /* _COLOR_BOX_H_ */
