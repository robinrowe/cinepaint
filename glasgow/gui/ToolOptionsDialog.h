/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ToolOptionsDialog - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ToolOptionsDialog.h,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#ifndef _TOOL_OPTIONS_DIALOG_H_
#define _TOOL_OPTIONS_DIALOG_H_

#include "dll_api.h"

#include <FL/Fl_Window.H>
#include <app/ToolManager.h>
#include <app/CinePaintEvent.h>

#include <map>
#include <string>

// forward declaration
class Fl_Box;
class Fl_Group;

/**
 * Tool Options Dialog - display the tool options panel for the currently selected tool
 * The Tool Option Panels should be registered with the ToolOptionsDialog, and selected for display
 * by calling ShowToolOptionsPanel. Typically tool option panels should be registered with their tool
 * name, although this is not required.
 * The ToolOptionsDialog also implement ToolChangeListener so it can receive tool change notifications
 * from a ToolManager once registered as a listener. In this way, the dialog will automatically update 
 * the currently displayed panel upon a tool change event.
 *
 */
class CINEPAINT_GUI_API ToolOptionsDialog : public Fl_Window, public ToolManager::ToolChangeListener
{
	public:
		/**
		 * Constructs a new ShowToolOptionsPanel with the specified title
		 * The dialog initially contains no tool panels to display
		 *
		 * @param title the dialog title
		 */
		ToolOptionsDialog(const char* title) ;

		/**
		 * Destructor
		 * All managed tool panels are destroyed with the ShowToolOptionsPanel, if this is not the desired
		 * effect, the panel should be removed prior to destroying this dialog.
		 *
		 */
		virtual ~ToolOptionsDialog();

		/**
		 * Registers the specified option_panel with this ToolOptionsDialog with the specified id.
		 * Typically, the id should be the name of the too the options_panel represents.
		 * This ToolChangeListener takes responsibility for the added panel, releasing its resources
		 * when this dialog is released.
		 *
		 * @param id the id/name to register the panel against
		 * @param options_panel the tool options panel to register
		 */
		void RegisterToolOptionsPanel(const char* id, Fl_Group* options_panel);

		/**
		 * Removes and returns the tool options panel registered with the specified id.
		 * If no panel is registered with the specified name, 0 is returned
		 *
		 * @param id the id of the tool options panel to remove
		 * @return the removed tool options panel
		 */
		Fl_Group* RemoveToolOptionsPanel(const char* id);

		/**
		 * Displays the tool options panel with the specified id.
		 * If no tool is registered with the specified id, the default no options panel is diaplyed
		 *
		 * @param tool_type the registereed name of the panel to display
		 */
		void ShowToolOptionsPanel(std::string tool_type);



		//----------------------------------
		// ToolChangeListener implementation

		/**
		 * Invoked when the currently selected tool is changed
		 *
		 * @param id the name of the newly selected tool
		 */
		virtual void ToolChanged(const char* tool_type);

	protected:

	private:

		typedef std::map<std::string, Fl_Group*> PanelContainer_t;
		typedef std::pair<std::string, Fl_Group*> PanelPair_t;

		/** container for registered tool options panals - keys should generally be the tool name */
		PanelContainer_t m_tool_options_panels;


		/** 
		 * Nested class for a default, no tool options panel
		 */
		class DefaultToolOptPanel : public Fl_Group
		{
			public:
				DefaultToolOptPanel(int x, int y);
				void SetName(const std::string& tool_name);
			protected:
			private:
				Fl_Box* m_tool_name;
				std::string m_tool_label;
		};

		/** default no tool options panel, shown if the currently selected tool has no options */
		DefaultToolOptPanel* m_no_options_panel;

		/** default unavailable tool panel, shown if the named tool is not available */
		Fl_Group* m_tool_unavail_panel;


} ; /* class ToolOptionsDialog */

#endif /* _TOOL_OPTIONS_DIALOG_H_ */
