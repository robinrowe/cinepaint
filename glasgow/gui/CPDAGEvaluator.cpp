/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CPDAGEvaluator - Directed acyclic graph node evaluator
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * $Id: CPDAGEvaluator.cpp,v 1.2 2006/12/21 11:18:05 robinrowe Exp $
 */

#include "CPDAGEvaluator.h"
#include "CPDAG.h"
#include "CPDAGData.h"
#include "CPDAGNode.h"
#include "plug-ins/pdb/CinePaintImage.h"
#include "plug-ins/pdb/Drawable.h"
#include "plug-ins/pdb/Layer.h"
#include "app/CinePaintApp.h"
#include "app/CinePaintDocList.h"
#include "plug-ins/pdb/CinePaintPlugin.h"
#include "plug-ins/pdb/PluginParam.h"
#include "plug-ins/pdb/PDB.h"
#include "cctype"


const char* CPDAGEvaluator::IN_PARAM_NAME_MODIFIER = "__IN_MOD.";

/**
 * Constructs a new CPDAGEvaluator for the specified CPDAG
 *
 * @param dag the CPDAG this CPDAGEvaluator will evaluate nodes from
 */
CPDAGEvaluator::CPDAGEvaluator(CPDAG& dag)
		: m_dag(dag)
{}

/**
 * Destructor
 *
 */
CPDAGEvaluator::~CPDAGEvaluator()
{}

/**
 * Evaluates the named node within the CPDAG specified in the constructor
 *
 * @param node_name the node name to evaluate
 * @param reval set true to re-evaluate a non-dirty node
 * @return true if the nodes exists within the CPDAG and evaluation was a success,
 *         false otherwise
 */
bool CPDAGEvaluator::EvaluateNode(const std::string& node_name, bool reval,World* world)
{
	bool ret = false;

	CPDAG::nodes_iterator iter = m_dag.find_node(node_name);
	if(iter != m_dag.m_nodes.end())
	{
		CPDAGNode* node = iter->second;

		if(node && (node->IsDirty() || reval))
		{
			switch(node->GetData().GetType())
			{
					case CPDAGData::DAG_PLUGINPARAM:
					{
						ret = evaluate_param_node(node_name, *node);
						break;
					}
					case CPDAGData::DAG_PLUGIN:
					{
						ret = evaluate_plugin_node(node_name, *node,world);
						break;
					}
					default:
					{
						break;
					}
			}
		}
		else
		{
			// already evaluated
			ret = true;
		}
	}

	return(ret);
}



/**
 * Evaluates the specified parameter node
 * Evaluation of a parameter node involves setting the default evaluation of the
 * node to its data value
 *
 * @param node_name the name of the node to be evaluated
 * @param node the node to be evaluated
 * @return true if the node was evaluated sucessfully, false otherwise
 */
bool CPDAGEvaluator::evaluate_param_node(const std::string& node_name, CPDAGNode& node)
{
	bool ret = false;

	PluginParam* param = node.get_evaluation(CPDAGNode::DEFAULT_EVALUATION_NAME);
	if(!param)
	{
		param = new PluginParam();
		node.set_evalulation(CPDAGNode::DEFAULT_EVALUATION_NAME, param);
		node.set_default_evaluation(CPDAGNode::DEFAULT_EVALUATION_NAME);
	}

	// set the parameter
	const PluginParam* node_param = node.GetData().GetPluginParam();
	if(node_param)
	{
		param->Set(*node_param);

		// as this is a plugin param cell, the data must have been entered directly, so
		// the cell PluginParam owns the data.
		// the evaluation is a copy, and terefore does should not release the data when it is released.
		// @TODO how do we handle image data coming from another part of the CinePaint app?
		//       should the cell own that data? is that a valid thing to want to do with a
		//       param cell anyway?
		param->SetManaged(false);

		// finally, as we are evaluated, set the dirty state
		node.set_dirty(false);

		ret = true;
	}
	else
	{
		ret = false;
	}

	return(ret);
}

/**
 * Evaluates the specified plugin node
 * Evaluation of a parameter node involves building the plugin parameter lists
 * using conected graph nodes, runing the plugin and capturing the results, and setting
 * evaluated values as appropriate within the node
 *
 * @param node_name the name of the node to be evaluated
 * @param node the node to be evaluated
 * @return true if the node was evaluated sucessfully, false otherwise
 */
bool CPDAGEvaluator::evaluate_plugin_node(const std::string& node_name, CPDAGNode& node,World* world)
{	bool ret = false;
	std::string plugin_name = node.GetData().GetPluginName();
	printf("evaluate_plugin_node: Plugin Name %s\n", plugin_name.c_str());
	PDB* pdb=PDB::GetPDB(world);
	if(!pdb)
	{	return false;
	}
	CinePaintPlugin* plugin = pdb->GetPluginInstance(plugin_name.c_str(), guess_plugin_type_from_name(plugin_name,world));
	if(plugin)
	{	CPParamList in_params;
		CPParamList ret_params;
		PromotionList_t promotion_list;
		// setup the plugina params
		populate_plugin_param_list(*plugin, node_name, in_params, promotion_list);
		if(verify_param_list(*plugin, in_params))
		{	// run the plugin
			plugin->run(in_params, ret_params);
			// remove anything we added to the plugin param list to avoid it being deleted
			unpopulate_plugin_param_list(*plugin, node_name, in_params, ret_params, promotion_list);
			process_plugin_return_params(node_name, node, ret_params);
			process_default_evaulation(node, ret_params);
			// @TODO [claw] theres not actually a way of determining if a plugin suceeded or failed
			//       perhaps we need a success flag as a return parameter?
			m_dag.set_dirty(node_name, false);
			ret = true;
		}
		else
		{	// we still need to remove anything we added to the param list
			printf("The required plugin parameters were NOT met!\n");
			unpopulate_plugin_param_list(*plugin, node_name, in_params, ret_params, promotion_list);
			ret = false;
		}
	}
	return(ret);
}



/**
 * Populates the plugin input param list with connected graph node data prior to running a plugin
 * The conneted graph nodes which provide input parametes to the plugin are processed and
 * added to the param list.
 * If parameter types do not match those specified by the plugin parameter definitions,
 * an attempt is made to convert the type to the required type, and the conversion is recorded
 * within the promotion_list.
 * If the plugin modfies the input parameter, a copy of the parameter is made and the copy
 * is added to the param list.
 *
 * @param plugin the plugin to be run
 * @param eval_name the node name of the node to be evaluated
 * @param params the input param list
 * @param promotion_list populated with details of converted parameter types
 */
void CPDAGEvaluator::populate_plugin_param_list(const CinePaintPlugin& plugin, const std::string& eval_name, CPParamList& params, PromotionList_t& promotion_list)
{
	// loop over all the nodes in the graph searching for any neighbours of our specified node,
	for(CPDAG::nodes_iterator iter = m_dag.nodes_begin(); iter != m_dag.nodes_end(); ++iter)
	{
		CPDAGNode* graph_node = iter->second;

		std::pair<CPDAGNode::neightbour_iterator, CPDAGNode::neightbour_iterator> neighbour_range = graph_node->neighbours_range(eval_name);
		while(neighbour_range.first != neighbour_range.second)
		{
			// OK, found node(s) that has an inupt parameter to the node we want to evalulate
			std::string param_s = m_dag.param_strip(neighbour_range.first->second.first);
			std::string param_eval = neighbour_range.first->second.second;
			if(param_eval.empty())
			{
				param_eval = CPDAGNode::DEFAULT_EVALUATION_NAME;
			}

			PluginParam* param = graph_node->get_evaluation(param_eval);

			if(param)
			{
				// we need to call unpopulate_plugin_param_list once we are finished to
				// remove anything we add to the param list, else it'll get deleted when
				// the param list is deleted
				add_to_param_list(plugin, params, param_s, *param, promotion_list);
				printf("Adding params to plugin: from cell=%s param name=%s  eval=%s\n", iter->first.c_str(), neighbour_range.first->second.first.c_str(), neighbour_range.first->second.second.c_str());
			}
			else
			{
				printf("Failed to add param to plugin: from cell=%s param name=%s  eval=%s\n", iter->first.c_str(), neighbour_range.first->second.first.c_str(), neighbour_range.first->second.second.c_str());
			}

			++neighbour_range.first;
		}
	}
}

/**
 * Unpopulates the plugin input param list using connected graph node data.
 * Removes any data item added to the plugin input param list that was added
 * from connected graph nodes
 *
 * After a plugin has been run, the input param list is no longer required. However
 * once the param list is released, any data item remaining in the list are released. As some
 * of those data item may have come from connected graph nodes, the data item needs to be removed
 * and remain managed by the holding node.
 *
 * Any data item that was created during populate_plugin_param_list, for exmaple, for a data item
 * that plugin modifies, the item is removed from the input param list and added to the return
 * param list.
 *
 * The promotion_list is used to determine any data item that had ist type modified during
 * populate_plugin_param_list, and so the types correspond in the removal process.
 *
 * @param plugin the plugin
 * @param eval_name the node name of the node to be evaluated
 * @param params the input param list
 * @param ret_params the return param list.
 * @param promotion_list populated with details of converted parameter types
 */
void CPDAGEvaluator::unpopulate_plugin_param_list(const CinePaintPlugin& plugin, const std::string& eval_name, CPParamList& params, CPParamList& ret_params, PromotionList_t& promotion_list)
{
	// anything we put into the param list, we need to remove, or else the param list
	// will destroy it on the param lists destruction

	// the exception is parameters which are modified by the plugin.
	// in that case, the parameter was created as a copy of the original as must be managed.
	// we remove the parameter from the plugin inward parameters and add it to the return params.
	// this allows further processing of the return params, and if the param is not claimed, the
	// return param list will automatically release the created params resources

	// loop over all the nodes in the graph searching for any neighbours of our specified node,
	for(CPDAG::nodes_iterator iter = m_dag.nodes_begin(); iter != m_dag.nodes_end(); ++iter)
	{
		CPDAGNode* graph_node = iter->second;

		std::pair<CPDAGNode::neightbour_iterator, CPDAGNode::neightbour_iterator> neighbour_range = graph_node->neighbours_range(eval_name);
		while(neighbour_range.first != neighbour_range.second)
		{
			// OK, found a node that has an input parameter to the node we want to evalulate
			std::string param_s = m_dag.param_strip(neighbour_range.first->second.first);
			std::string param_eval = neighbour_range.first->second.second;
			if(param_eval.empty())
			{
				param_eval = CPDAGNode::DEFAULT_EVALUATION_NAME;
			}

			PluginParam* param = graph_node->get_evaluation(param_eval);

			if(param)
			{
				// remove the data from the Param List, and rely upon the graph to correctly handle the resource
				CPPluginArg arg;
				CPPluginArgType param_type = param->GetType();

				// was the parameter one promoted/demoted
				// if so, we need use the promoted/demoted parameter type
				PromotionList_t::iterator promotion_iter = promotion_list.begin();
				bool done = false;
				while(!done && promotion_iter != promotion_list.end())
				{
					if(param_s == (*promotion_iter).m_param)
					{
						param_type = (*promotion_iter).m_to_type;
						done = true;
					}
					++promotion_iter;
				}

				if(params.RemoveParam(param_s.c_str(), arg, param_type) != 1)
				{
					printf("Failed to remove plugin param cell=%s param name=%s return name=%s\n", iter->first.c_str(), neighbour_range.first->second.first.c_str(), neighbour_range.first->second.second.c_str());
				}
				else
				{
					// removed param from plugin in params
					// if the plugin modifies the param, the param need to be managed
					// we need to preserve the param

					const CinePaintPlugin::CPPluginParamDef* def = get_plugin_param_def(plugin, param_s);
					if(def->m_modifies)
					{
						printf("Removed modified plugin param cell=%s param name=%s return name=%s\n", iter->first.c_str(), neighbour_range.first->second.first.c_str(), neighbour_range.first->second.second.c_str());

						// the parameter was modified by the plugin
						// add the param to the return params

						// modifiy the name to reflect it is a modified input parameter
						param_s.insert(0, IN_PARAM_NAME_MODIFIER);

						// add the parameter to the return list
						ret_params.AddParam(param_s.c_str(), &arg, param_type);
					}
					else
					{
						// the param came from an exising graph node which own the param
						printf("Removed plugin param cell=%s param name=%s return name=%s\n", iter->first.c_str(), neighbour_range.first->second.first.c_str(), neighbour_range.first->second.second.c_str());
					}
				}
			}

			++neighbour_range.first;
		}
	}
}

/**
 * Adds the specified param value, with the specified parameter namd to the input param list of a plugin
 * This method is a convenience method to determine the best away to add the param value
 * to the plugins input parameter list.
 * If the param value is modified by the plugin, the param is added using add_to_param_list_modified
 * If the param value type does not match that as defined by the plugins parameter definitions,
 * the param is added using add_to_param_list_promoted.
 * Otherwise the param value is simply added using add_to_param_list
 *
 * @param plugin the plugin to be run
 * @param params the input parameter list
 * @param param_name the parameter name
 * @param param the parameter value
 * @param promotion_list populated with details of converted parameter types 
 */
void CPDAGEvaluator::add_to_param_list(const CinePaintPlugin& plugin, CPParamList& params, const std::string& param_name, PluginParam& param, PromotionList_t& promotion_list)
{
	const CinePaintPlugin::CPPluginParamDef* plugin_param_def = get_plugin_param_def(plugin, param_name);

	if(plugin_param_def)
	{
		if(plugin_param_def->m_type != param.GetType())
		{
			// @TODO add promotion/demotion user configuration control
			printf("Param type mis-match, %s\n", param_name.c_str());

			// also attempts to handle if the param is modified
			add_to_param_list_promoted(params, param_name, param, *plugin_param_def, promotion_list);
		}
		else
		{
			if(plugin_param_def->m_modifies)
			{
				add_to_param_list_modified(params, param_name, param);
			}
			else
			{
				// not modified, just add the parameter
				add_to_param_list(params, param_name, param);
			}
		}
	}
	else
	{
		// unrequired plugin parameter, just add the parameter as is
		printf("Unrequired plugin param %s\n", param_name.c_str());

		add_to_param_list(params, param_name, param);
	}
}

/**
 * Adds the spcified param value, with the specified parameter to the input param list of a plugin
 *
 * @param params the input parameter list
 * @param param_name the parameter name
 * @param param the parameter value 
 */
void CPDAGEvaluator::add_to_param_list(CPParamList& params, const std::string& param_name, PluginParam& param)
{
	CPPluginArg arg = param.GetArg();
	params.AddParam(param_name.c_str(), &arg, param.GetType());
}

/**
 * Creates a copy of the specified param value and adds the copy to the input param list of a plugin
 *
 * @param params the input parameter list
 * @param param_name the parameter name
 * @param param the parameter value 
 * @TODO this needs to be fully implemented
 */
void CPDAGEvaluator::add_to_param_list_modified(CPParamList& params, const std::string& param_name, PluginParam& param)
{
	switch(param.GetType())
	{
			case PDB_INT:
			case PDB_FLOAT:
			{
				// fundamental types are automatically copied
				break;
			}
			case PDB_CSTRING:
			case PDB_ABSTRACTBUF:
			{
				// @TOOO
				printf("CPDAGEvaluator::add_to_param_list: TODO copy AbstractBuf\n");
				break;
			}
			case PDB_DRAWABLE:
			{
				printf("Creating copy of modified Drawable param, %s\n", param_name.c_str());
				PluginParam param_copy;
				param_copy.SetManaged(false);
				Drawable* drawable = new Drawable(*(param.GetArg().pdb_drawable));
				param_copy.Set(drawable);
				add_to_param_list(params, param_name, param_copy);
				break;
			}
			case PDB_CPIMAGE:
			{
				printf("CPDAGEvaluator::add_to_param_list: TODO copy CinePaintImage\n");
				break;
			}
			case PDB_VOIDPOINTER:
			default:
			{
				break;
			}
	}
}

/**
 * Attempts to add the spcified param value to the param list.
 * This method should be used if the expected param type does not match the current param type.
 * An attemp is made to convert the plugin param into the expcted type, and the converted type
 * is added to the param list. The conversion is recorded within the promotion_list.
 *
 * @param params the input parameter list
 * @param param_name the parameter name
 * @param param the parameter value
 * @param param_def the parameter definition expected by the plugin
 * @param promotion_list populated with details of converted parameter types
 * @TODO this needs to be fully implemented
 */
int CPDAGEvaluator::add_to_param_list_promoted(CPParamList& params, const std::string& param_name, const PluginParam& param, const CinePaintPlugin::CPPluginParamDef& param_def, PromotionList_t& promotion_list)
{
	int promoted = 0;

	PluginParam promotion;
	promotion.SetManaged(false);

	// switch first on the param type expected by the plugin
	// ... then on the type we have to determine any possible conversion between the two types
	switch(param_def.m_type)
	{
			case PDB_INT:
			case PDB_FLOAT:
			case PDB_CSTRING:
			case PDB_ABSTRACTBUF:
			case PDB_DRAWABLE:
			{
				switch(param.GetType())
				{
						case PDB_CPIMAGE:
						{
							// is there an active layer
							CinePaintImage* image = param.GetArg().pdb_cpimage;
							Drawable* drawable = image->GetLayers().GetActiveLayer();

							if(!drawable)
							{
								// no active layer, is there a background layer?
								drawable = image->GetLayers().GetBackgroundLayer();
							}

							if(drawable)
							{
								promotion.Set(drawable);
								promoted = -1;
							}
							else
							{
								promoted = 0;
							}

							break;
						}
						case PDB_INT:
						case PDB_FLOAT:
						case PDB_CSTRING:
						case PDB_ABSTRACTBUF:
						case PDB_DRAWABLE:
						case PDB_VOIDPOINTER:
						default:
						{
							promoted = 0;
							break;
						}
				}

				break;
			}
			case PDB_CPIMAGE:
			case PDB_VOIDPOINTER:
			default:
			{
				promoted = 0;
				break;
			}
	}

	if(promoted != 0)
	{
		if(promoted < 0)
		{
			printf("Demoted param, %s\n", param_name.c_str());
		}
		else
		{
			printf("Promoted param, %s\n", param_name.c_str());
		}

		PromotionRec rec;
		rec.m_param = param_name;
		rec.m_from_type = param.GetType();
		rec.m_to_type = promotion.GetType();
		promotion_list.push_back(rec);

		// is the parameter modified?
		if(param_def.m_modifies)
		{
			add_to_param_list_modified(params, param_name, promotion);
		}
		else
		{
			add_to_param_list(params, param_name, promotion);
		}
	}
	else
	{
		printf("Couldn't promote param %s\n", param_name.c_str());
	}

	return(promoted);
}


/**
 * Sets the evaluated nodes evaluations with return data from the plugin
 * Any parameters returned from the plugin, that are referenced by connected nodes within
 * the CPDAG are coolcted and added as evaluation data to the evaluated node.
 * Any returned parameters not referenced by existing connected node are not collected
 * from the plugins return params, as they are not considered to be required.
 *
 * @param node_name name of the node being evaluated
 * @param node the node being evaluated
 * @param ret_params the return list from the plugin
 */
void CPDAGEvaluator::process_plugin_return_params(const std::string& node_name, CPDAGNode& node, CPParamList& ret_params)
{
	// clear any previous evaluations
	node.clear_evaluations();

	for(CPDAGNode::neightbour_iterator iter = node.neighbours_begin(); iter != node.neighbours_end(); ++iter)
	{
		// anything node is connected to is interested in a return param, so keep hold of it
		std::string s = iter->second.second;

		CPPluginArg arg;
		CPPluginArgType type;
		if(ret_params.GetParamType(s.c_str(), type))
		{
			if(ret_params.RemoveParam(s.c_str(), arg, type) == 1)
			{
				printf("Collecting Param %s\n", s.c_str());
				PluginParam* param = new PluginParam(type, arg);

				// who owns the plugin data?
				switch(type)
				{
						case PDB_INT:
						case PDB_FLOAT:
						case PDB_CSTRING:
						case PDB_DRAWABLE:
						{
							param->SetManaged(true);
							break;
						}
						case PDB_CPIMAGE:
						{
							param->SetManaged(false);
							CinePaintApp::GetInstance().GetDocList().AddDocument(*arg.pdb_cpimage);
							break;
						}
						case PDB_VOIDPOINTER:
						default:
						{
							break;
						}
				}

				node.set_evalulation(s, param);
			}
			else
			{
				printf("Failed to get param %s\n", s.c_str());
			}
		}
		else
		{
			printf("Expected Return param not present in return params list: %s\n", s.c_str());
		}
	}
}

/**
 * Process any remaining plugin return parameters to determine the default evaluation value of a node.
 * The defaule evaluation value of the node is the highest ranking data value.
 * CinePaintImage -> Drawable -> AbstractBuf -> string -> float -> int
 * If the node already has the highest ranking data value, no further action is taken, otherwise
 * the remaining params are searched to determine if a higher ranking item remains uncollected.
 *
 * This process allows an image data item that is not referenced by any connecting graph nodes to be
 * collected and displayed as the returns of an evaluated cell.
 *
 * @param node the node being evaluated
 * @param ret_params the plugin return param list
 */
void CPDAGEvaluator::process_default_evaulation(CPDAGNode& node, CPParamList& ret_params)
{
	// search the node evaluations first
	std::string default_param;

	bool type_found = false;

	// we dont consider this type
	CPPluginArgType top_type = PDB_VOIDPOINTER;

	for(CPDAGNode::eval_iterator iter = node.evaulations_begin(); iter != node.evaulations_end(); ++iter)
	{
		CPPluginArgType tmp_type = iter->second->GetType();
		if(!type_found && (tmp_type != PDB_VOIDPOINTER))
		{
			top_type = tmp_type;
			default_param = iter->first;
		}
		else
		{
			if(tmp_type > top_type)
			{
				top_type = tmp_type;
				default_param = iter->first;
			}
		}
	}


	bool param_needs_saved = false;
	if(top_type != PDB_CPIMAGE)
	{
		// is there anything left within the param list that is of a higher order parameter?
		std::set<std::string> param_names;
		ret_params.GetParamNames(param_names);

		CPPluginArgType tmp_type = PDB_VOIDPOINTER;

		for(std::set<std::string>::const_iterator citer = param_names.begin(); citer != param_names.end(); ++citer)
		{
			if(ret_params.GetParamType((*citer).c_str(), tmp_type))
			{
				if(!type_found && (tmp_type != PDB_VOIDPOINTER))
				{
					top_type = tmp_type;
					default_param = *citer;
					param_needs_saved = true;
				}
				else
				{
					if(tmp_type > top_type)
					{
						top_type = tmp_type;
						default_param = *citer;
						param_needs_saved = true;
					}
				}
			}
		}
	}

	if(param_needs_saved)
	{
		CPPluginArg arg;
		if(ret_params.RemoveParam(default_param.c_str(), arg, top_type) == 1)
		{
			printf("Collected default parameter: %s\n", default_param.c_str());
			PluginParam* param = new PluginParam(top_type, arg);

			// who owns the plugin data?
			switch(top_type)
			{
					case PDB_INT:
					case PDB_FLOAT:
					case PDB_CSTRING:
					case PDB_DRAWABLE:
					{
						param->SetManaged(true);
						break;
					}
					case PDB_CPIMAGE:
					{
						param->SetManaged(false);
						CinePaintApp::GetInstance().GetDocList().AddDocument(*arg.pdb_cpimage);
						break;
					}
					case PDB_VOIDPOINTER:
					default:
					{
						break;
					}
			}

			node.set_evalulation(default_param, param);
		}
	}

	printf("Default Parameter type: %s\n", default_param.c_str());
	node.set_default_evaluation(default_param);
}


/**
 * Convenience method to return the named parameter defiintion from the specified plugin
 * If the named parameter definition does not exist, 0 is returned
 *
 * @param plugin the CinePaintPlugin from which to retrieve the parameter definition
 * @param param_name the name of the parameter definition to retrieve
 * @return the named parameter definition if specified, otherwise 0
 */
const CinePaintPlugin::CPPluginParamDef* CPDAGEvaluator::get_plugin_param_def(const CinePaintPlugin& plugin, const std::string& param_name)
{
	const CinePaintPlugin::CPPluginParamDef* def = 0;

	CinePaintPlugin::CPPluginParamDefs plugin_defs;
	plugin.GetInParamDefs(plugin_defs);

	for(int i = 0; i < plugin_defs.m_param_count; i++)
	{
		const CinePaintPlugin::CPPluginParamDef* tmp_def = &plugin_defs.m_params[i];

		if(strcmp(tmp_def->m_name, param_name.c_str()) == 0)
		{
			def = tmp_def;
		}
	}

	return(def);
}

/**
 * Verify that the specified param list contains the defined inward parameters defined by the specified plugin
 * This method accesses the parameter definition of the speciied plugin, and checks that each parameter
 * is met by the specified param list.
 * If each of the parameters expected by the plugin, as defined by the plugins parameter definitions, is
 * presents and of the cirrect type, this methos returns true, otherwise this method returns false, and
 * the plugin cannot be run as it does not have sufficient parameters
 * 
 * @param plugin the plugin to check parameters against
 * @param params the param list containing the parameter to be passed to the plugin
 * @return true if the param lkist contains all required parameters, false otherwise
 */
bool CPDAGEvaluator::verify_param_list(const CinePaintPlugin& plugin, const CPParamList& params)
{
	// get the plugina param defs
	CinePaintPlugin::CPPluginParamDefs plugin_defs;
	plugin.GetInParamDefs(plugin_defs);

	// build our work list of the plugins params
	std::map<std::string, CPPluginArgType> def_list;
	for(int i = 0; i < plugin_defs.m_param_count; i++)
	{
		def_list.insert(std::pair<std::string, CPPluginArgType>(plugin_defs.m_params[i].m_name, plugin_defs.m_params[i].m_type));
	}

	std::map<std::string, CPPluginArgType>::iterator iter = def_list.begin();
	while(iter != def_list.end())
	{
		std::map<std::string, CPPluginArgType>::iterator next = iter;
		next++;

		printf("Checking Param %s ... ", iter->first.c_str());

		CPPluginArgType test_type;
		if(params.GetParamType(iter->first.c_str(), test_type))
		{
			if(test_type == iter->second)
			{
				// types match, remove the item from the chack map
				printf("OK.\n");
				def_list.erase(iter);
			}
			else
			{
				printf("type mismatch!\n");
			}
		}
		else
		{
			printf("not present.\n");
		}

		iter = next;
	}

	return(def_list.empty());
}

/**
 * Attempts to guess the type of a plugin from its name.
 * This method checks the plugin name to determine if it contains the words
 * load or save. If so, the chances are that the named plugin is a load or save
 * handler. Otherwise, the plugin is expecte to be of standard plugin type.
 * This data is then used to search the PDB to determine if the named plugin
 * exist wihin the respective database. If the plugin is not found the databases for
 * remaining plugin types are searched for the named plugin. If the plugin is found the
 * plugin type is returned, otherwise PDB::NO_TYPE_ENUM is returned
 * This method is unreliable because a plugin may be registered within the database
 * for each plugin type with the same name
 *
 * @param name the plugin name
 * @return the type of the plugin, or PDB::NO_TYPE_ENUM if the type cannot be found.
 */
PDB::PluginType CPDAGEvaluator::guess_plugin_type_from_name(const std::string& name,World* world)
{
	PDB::PluginType ret = PDB::NO_TYPE_ENUM;

	PDB::PluginType search_order[3];

	std::string name_uc(name);
	std::transform(name_uc.begin(), name_uc.end(), name_uc.begin(), (int(*)(int))std::toupper);

	if(name_uc.find("LOAD") != std::string::npos)
	{
		search_order[0] = PDB::LOAD_HANDLER_ENUM;
		search_order[1] = PDB::SAVE_HANDLER_ENUM;
		search_order[2] = PDB::STD_PLUGIN_ENUM;
	}
	else if(name_uc.find("SAVE") != std::string::npos)
	{
		search_order[0] = PDB::SAVE_HANDLER_ENUM;
		search_order[1] = PDB::LOAD_HANDLER_ENUM;
		search_order[2] = PDB::STD_PLUGIN_ENUM;
	}
	else
	{
		search_order[0] = PDB::STD_PLUGIN_ENUM;
		search_order[1] = PDB::LOAD_HANDLER_ENUM;
		search_order[2] = PDB::SAVE_HANDLER_ENUM;
	}

	int count = 0;
	bool done = false;
	PDB* pdb=PDB::GetPDB(world);
	if(!pdb)
	{	return ret;
	}

	while(count < 3 && !done)
	{
		switch(search_order[count])
		{
				case PDB::STD_PLUGIN_ENUM:
				{
					if(pdb->LookupPlugin(name.c_str()))
					{
						ret = PDB::STD_PLUGIN_ENUM;
						done = true;
						break;
					}
				}
				case PDB::LOAD_HANDLER_ENUM:
				{
					if(pdb->LookupLoadHandler(name.c_str()))
					{
						ret = PDB::LOAD_HANDLER_ENUM;
						done = true;
						break;
					}
				}
				case PDB::SAVE_HANDLER_ENUM:
				{
					if(pdb->LookupSaveHandler(name.c_str()))
					{
						ret = PDB::SAVE_HANDLER_ENUM;
						done = true;
						break;
					}
				}
				default:
				{
					ret = PDB::NO_TYPE_ENUM;
					done = true;
				}
		}

		count++;
	}

	return(ret);
}
