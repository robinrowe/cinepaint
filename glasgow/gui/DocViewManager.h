/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      DocViewManager - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: DocViewManager.h,v 1.2 2006/12/21 11:18:06 robinrowe Exp $
 */

#ifndef _CINEPAINT_DOC_VIEW_MANAGER_H_
#define _CINEPAINT_DOC_VIEW_MANAGER_H_

#include "dll_api.h"
#include "app/CinePaintDocListListener.h"
#include "plug-ins/pdb/World.h"
#include <map>

// Forward Declaration
class CinePaintDoc;
class CinePaintImage;
class CinePaintImageWindow;
class CPDAGDoc;
class FrameManager;
class FrameManagerWindow;
class SheetViewWindow;

class Fl_Widget;
class Fl_Window;

/**
 * Managing class for views of various open CinepaintDocs
 * A particular open document may have multiple view associated with it.
 * DocViewManager implements CinePaintDocListListener to automatically respond to document
 * events, creating and closing views as required.
 *
 */
class CINEPAINT_GUI_API DocViewManager : public CinePaintDocListListener
{
	public:
		/**
		 * Constructs an intially empty DocViewManager
		 */
		DocViewManager();

		/**
		 * Destructor - destroys all managed docuemnt views
		 */
		virtual ~DocViewManager();


		/**
		 * Opens and registers a new view for the specified CinePaintDoc.
		 * Multiple views may be opened for a particular view.
		 * Currently this implementation only handles CinePaintImage and the associated
		 * CinePaintImageWindows view.
		 * The reference count for the CinePaintDoc is incremented by one within the CinePaintDocList
		 *
		 *
		 * @param doc the CinePaintDoc to open the view for
		 */
		virtual void OpenView(CinePaintDoc& doc);

		/**
		 * Closes and unregisters the specified view of the specified CinePaintDoc.
		 * Currently this implementation only handles CinePaintImage and the associated
		 * CinePaintImageWindows view.
		 * If the view is the last remaining reference of the Document within the CinePaintDoc
		 * the document will be closed. If the Document has been modified, the user is prompted
		 * if they wish to save the document
		 *
		 * @param doc the CinePaintDoc to open the view for
		 * @param view the view of the CinePaintDoc
		 */
		virtual void CloseView(CinePaintDoc& doc, Fl_Window& view);



		// CinePaintDocListListener methods

		/**
		 * Invoked when a CinePaintDoc is opened.
		 * If the CinePaintDoc is a supported type a view for the document is created and displayed.
		 * Currently this implementation only supports a CinePaintImage
		 *
		 * @param doc_id the unique id of the opened document
		 * @param doc the newly opened CinePaintDoc
		 */
		virtual void DocOpened(const char* doc_id, CinePaintDoc& doc);

		/**
		 * Invoked when a CinePaintDoc is closed.
		 * Closes any open views over the specified CinePaintDoc
		 *
		 * @param doc_id the unique id of the closed document
		 * @param doc the closed CinePaintDoc
		 */
		virtual void DocClosed(const char* doc_id, CinePaintDoc& doc);

		/**
		 * Invoked when the current document changes.
		 *
		 * @param doc_id the unique id of the current document
		 * @param doc the newly selected current document.
		 */
		virtual void CurrentDocChanged(const char* doc_id, CinePaintDoc* doc) {};


	protected:

	private:

		typedef std::multimap<CinePaintDoc*, Fl_Window*> DocViewContainer_t;
		typedef std::pair<CinePaintDoc*, Fl_Window*> DocViewPair_t;

		/** Associative Container of CinePaintDoc and views */
		DocViewContainer_t m_doc_views;


		//------------------------
		// CinePaintImage Handling

		/**
		 * Creates a display window for the specified image and displays it.
		 * The new window is registered as a view over the specified CinePaintImage
		 *
		 * @param image the CinePaintImage to display
		 */
		void open_image_window(CinePaintImage* image);

		/**
		 * Unregisters and Closes the specified CinePaintImageWindow.
		 * The window is unregisters as a view over its CinePaintImage. If the view was the
		 * last remaining open view, a request is made to close the CinePaintImage. If the
		 * Document was indeed closed, this DocViewmanager will receice notification and delete
		 * the view, otherwise the view is left open. If the view is not the last open view, it
		 * is simply uinregistered, closed, and deleted
		 *
		 * @param window the window being closed
		 */
		void close_view(CinePaintImageWindow* window);


		/**
		 * static callback on closing a window
		 * This callback expects the window and the widget parameter and the DocViewManager as the additional data.
		 * Calls close_view on the DocViewmanager instance
		 *
		 * @param window the window being closed
		 * @param data the DocViewmanager
		 */
		static void static_image_window_close_cb(Fl_Widget* window, void* data);



		//-------------------
		// CPDAGDoc Handling

		/**
		 * Creates and displays a sheet view window to display the DAG
		 * The new window is registerd as a view over the specified CPDAGDoc
		 *
		 * @param sheet the CPDAGDoc to display
		 */
		void open_sheet_view_window(CPDAGDoc* sheet);

		/**
		 * Unregisters and closes the specified SheetViewWindow
		 * The windows is unregistered as a view of the CPDAGDoc. If the view held the last
		 * remaining reference to the CPDAGDoc, and the DAG has been modified, the user is queried
		 * to save the document.
		 *
		 * @param sheet the view to close
		 */
		void close_view(SheetViewWindow* sheet);

		/**
		 * static callback on closing a SheetViewWindow
		 * This callback expects the window and the widget parameter and the DocViewManager as the additional data.
		 * Calls close_view on the DocViewmanager instance
		 *
		 * @param window the window being closed
		 * @param data the DocViewmanager
		 */
		static void static_sheet_view_window_close_cb(Fl_Widget* window, void* data);



		//----------------------
		// FrameManager Handling

		/**
		 * Creates and displays a new Frame Manager view window to display a FrameManager
		 * The new window is registerd as a view over the specified FrameManager
		 *
		 * @param fm the FrameManager to display
		 */
		void open_frame_manager_view(FrameManager* sheet);

		/**
		 * Unregisters and closes the specified FrameManagerWindow
		 * The windows is unregistered as a view of the FrameManager. If the view held the last
		 * remaining reference to the FrameManager, and the FrameManager has been modified,
		 * the user is queried to save the document.
		 *
		 * @param fmw the view to close
		 */
		void close_view(FrameManagerWindow* sheet);

		/**
		 * static callback on closing a FrameManagerWindow
		 * This callback expects the window and the widget parameter and the DocViewManager as the additional data.
		 * Calls close_view on the DocViewmanager instance
		 *
		 * @param window the window being closed
		 * @param data the DocViewmanager
		 */
		static void static_frame_manager_window_close_cb(Fl_Widget* window, void* data);

}; /* class DocViewManager */

#endif /* _CINEPAINT_DOC_VIEW_MANAGER_H_ */
