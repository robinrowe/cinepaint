/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Projection - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: Projection.cpp,v 1.1 2006/01/04 20:18:33 robinrowe Exp $
 */

#include "Projection.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/CinePaintImage.h"
#include "plug-ins/pdb/PixelArea.h"
#include "plug-ins/pdb/AbstractRenderer.h"
#include "app/CinePaintApp.h"
#include "plug-ins/pdb/CinePaintConversionOp.h"
#include "plug-ins/pdb/CinePaintCompositeOp.h"
#include <memory>
#include <exception>
#include <algorithm> 

/**
 * Constructs a Projection with the specified sizes
 *
 * @param width the width of this Projection
 * @param height the height of thie Projection
 */
Projection::Projection(int width, int height)
{
	m_renderer = 0;
	m_projection = 0;

	m_projection_region.SetRect(0, 0, width, height);
	m_zoom = 1.0;

	clear_invalid_region();

	create_projection(width, height);
}

Projection::~Projection()
{
	if(m_projection)
	{
		m_projection->UnrefPortion(0,0);
		delete m_projection;
		m_projection = 0;
	}
}

/**
 * Returns the CinePaintTag indicating the data format of this Projection
 *
 * @return the CinePaintTag indicating the data format of this Projection
 */
CinePaintTag& Projection::GetTag()
{
	return(m_tag);
}

const CinePaintTag& Projection::GetTag() const
{
	return(m_tag);
}

/**
 * Returns the Width of this CinepaintImage
 *
 * @return the width of this CinePaintImage
 */
int Projection::GetWidth() const
{
	return(m_projection_region.GetWidth());
}

/**
 * Returns the height of this CinePaintImage
 *
 * @retunr the height of this CinePaintImage
 */
int Projection::GetHeight() const
{
	return(m_projection_region.GetHeight());
}

/**
 * Sets the size of this projection.
 * After setting the size, the projection becomes invalid and will require
 * a re-projection.
 *
 * @param int width the new Projection width
 * @param height the new Projrction height
 */
void Projection::SetSize(int width, int height)
{
	m_projection_region.SetWidth(width);
	m_projection_region.SetHeight(height);
	create_projection(width, height);

	// invalidate the projection
	m_invalid_region.SetRect(m_projection_region.GetX(), m_projection_region.GetY(), m_projection_region.GetWidth(), m_projection_region.GetHeight());
}


/**
 * Gets the X offset of this Projection over the render being projected
 *
 * @return the x offset
 */
int Projection::GetOffSetX() const
{
	return(m_projection_region.GetX());
}

/**
 * Gets the Y offset of this Projection over the render being projected
 *
 * @return the y offset
 */
int Projection::GetOffSetY() const
{
	return(m_projection_region.GetY());
}

/**
 * Sets the offset of this Projection over the render being projected
 *
 * @param x the x offset
 * @param y the y offset
 */
void Projection::SetOffSet(int x, int y)
{
	m_projection_region.SetX(x);
	m_projection_region.SetY(y);

	// invalidate the entire projection
	m_invalid_region.SetRect(m_projection_region.GetX(), m_projection_region.GetY(), m_projection_region.GetWidth(), m_projection_region.GetHeight());
}

/**
 * Sets the current zoom level of this projection over the render
 * A 1.0 is considered 1:1. Zoom values <= 0.0 are ignored.
 *
 * @param zoom the new zoom level
 */
void Projection::SetZoom(double zoom)
{
	m_zoom = zoom;

	// invalidate the entire projection
	m_invalid_region.SetRect(m_projection_region.GetX(), m_projection_region.GetY(), m_projection_region.GetWidth(), m_projection_region.GetHeight());
}

/**
 * Returns the curtrent zoom of this Projection over the render
 *
 * @return the current zoom level
 */
double Projection::GetZoom() const
{
	return(m_zoom);
}


/**
 * Sets the renderer rendering the CinePaintImage for this projection.
 *
 * @param Renderer rendering the CinePaintImage used within this projection
 */
void Projection::SetRenderer(AbstractRenderer* renderer)
{
	m_renderer = renderer;

	// invalidate the entire projection
	m_invalid_region.SetRect(m_projection_region.GetX(), m_projection_region.GetY(), m_projection_region.GetWidth(), m_projection_region.GetHeight());
}

/**
 * Returns the AbstractBuf representing this projection ready for display on screen
 *
 * @return the projection AbstractBuf to be drawn on screen
 */
AbstractBuf* Projection::GetProjection()
{
	// check if we need to re-project the CinePaintImage data, for zoom/offset changes etc.
	if(m_renderer)
	{
		if(!is_vaild())
		{
			project(m_invalid_region);
			m_invalid_region.SetRect(0, 0, 0, 0);
		}
	}

	return(m_projection);
}


void Projection::InvalidateImageRegion(int x, int y, int w, int h)
{
	CPRect _image_space(x, y, w, h);

	CPRect _invalid_region;
	_image_space.Intersect(m_projection_region, _invalid_region);

	m_invalid_region.Add(_invalid_region);
}

void Projection::InvalidateRegion(int x, int y, int w, int h)
{
	// convert the specified region into our projection space,
	// ie the x and y values assume the origin is out prjection offset
	// @TODO need to do a liitle more to handle zooming.
	CPRect _invalid_region(m_projection_region.GetX() + x, m_projection_region.GetY() + y, w, h);
	m_invalid_region.Add(x, y, w, h);
}


/**
 * Constructs the projection data buffer at the specified size
 * if the projection buffer already exists, it is first destroyed, and a new buffer created.
 *
 * @param width the Projection buffer size
 * @param height the projection buffer height
 */
void Projection::create_projection(int width, int height)
{
	// delete any previous projection data
	if(m_projection)
	{
		m_projection->UnrefPortion(0, 0);
		delete m_projection;
		m_projection = 0;
	}

	// as this Projection is meant for the screen, we always assume the format
	CinePaintTag _tag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false);
	std::auto_ptr<AbstractBuf> _projection = AbstractBufFactory::CreateBuf(_tag, width, height, AbstractBufFactory::STORAGE_FLAT);
	m_projection = _projection.release();
	m_projection->RefPortionReadWrite(0,0);

	memset(m_projection->GetPortionData(0,0), 0, height * width * _tag.GetBytes());
}

/**
 * Renders this Projection from the set AbstractRenderer data.
 * If the set renderer is invalid, Render is first called upon the renderer to Render
 * image data, this Projection is then calculated and rendered using the current offset
 * and zoom settings
 *
 */
void Projection::project(const CPRect& update_region)
{
	if(!update_region.IsEmpty())
	{
		// if the render is invalid, it needs to be re-rendered
		if(!m_renderer->IsValid(update_region))
		{
			m_renderer->Render(update_region);
		}

		//
		// @TODO [claw] We need to handle the (possible) difference between the render
		//       format and our projection format here. This potentially involves
		//       conversion one buffer format into another
		//
		//


		// get the renderered source image data
		const AbstractBuf& _render_buffer = m_renderer->GetRender();
		const unsigned char* _src_buf = _render_buffer.GetPortionData(0, 0);

		// Get the connvertor for the given buffer.
		CinePaintConversionOp *_convertor = CinePaintApp::GetInstance().GetImageOpManager().GetConversionOp(_render_buffer.GetTag());

		// Build Area of Interest in image buffer.
		
		int _src_x = update_region.GetX();// - static_cast<unsigned int>(m_projection_region.GetX());
		int _src_y = update_region.GetY();
#undef min
		int _src_width = std::min(update_region.GetWidth(), m_renderer->GetWidth());
		int _src_height = std::min(update_region.GetHeight(), m_renderer->GetHeight());

		int _dst_x = update_region.GetX() - m_projection_region.GetX();
		int _dst_y = update_region.GetY() - m_projection_region.GetY();
		int _dst_width = _src_width;
		int _dst_height = _src_height;

		const PixelArea *_src_area = _render_buffer.GetPixelArea(_src_x, _src_y, _src_width, _src_height);
		PixelArea *_dst_area = m_projection->GetPixelArea(_dst_x, _dst_y, _dst_width, _dst_height);
		// @TODO Check we got these.
		if ( _convertor )
           _convertor->CopyConvert(*_dst_area, *_src_area);
		else
			printf("Error: No converstion from render to projection found\n");
		
		
		// Now we need to draw the glass layer on top.
		const AbstractBuf &_glass = m_renderer->GetGlassLayer();
		const PixelArea *_glass_area = _glass.GetPixelArea(_src_x, _src_y, _src_width, _src_height);

		CinePaintCompositeOp *_composite = CinePaintApp::GetInstance().GetImageOpManager().GetCompositeOp(m_projection->GetTag());
		if (_composite)
			_composite->CompositeOverlay(*_dst_area, *_glass_area);
		else
			printf("ERROR: No Composite found for projection glass layer\n");

		delete _src_area;
		delete _glass_area;
		delete _dst_area;
	}
}


/**
 * Returns true if this projection is currently valid.
 * For the prjection to be valid, the invalid region must be empty
 *
 * @return true if this Projection is valid
 */
bool Projection::is_vaild() const
{
	return(m_invalid_region.IsEmpty());
}

/**
 * Clears the current invalid region
 *
 */
void Projection::clear_invalid_region()
{
	m_invalid_region.SetRect(0, 0, 0, 0);
}
