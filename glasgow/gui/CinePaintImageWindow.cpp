/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintImageWindow - GUI Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintImageWindow.cpp,v 1.2 2006/12/21 11:18:06 robinrowe Exp $
 */

#include "CinePaintImageWindow.h"
#include "CinePaintDrawArea.h"
#include "DocViewManager.h"
#include "ImageInfoWindow.h"
#include "MenuFunctorHelper.h"
#include "CinePaintAppUI.h"
#include "Projection.h"
#include <plug-ins/pdb/CinePaintImage.h>
#include <FL/Fl_Box.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Menu_Button.H>
#include <FL/Fl_Scrollbar.H>
#include <app/CinePaintEventList.h>
#include <app/CinePaintApp.h>
#include <app/ToolManager.h>
#include <app/PluginUtils.h>
#include <plug-ins/pdb/PDB.h>
#include "UIUtils.h"
#include <string>

CinePaintImageWindow::CinePaintImageWindow(int w, int h, const char* title)
: Fl_Double_Window(w, h, title)
{
	Fl_Menu_Bar* _menu = new Fl_Menu_Bar(0, 0, w, 30);
	m_menu_functor_helper = new MenuFunctorHelper();

	build_menu(*_menu, *m_menu_functor_helper);


	// @note [claw] this is an awful method of handling a popup menu... FLTK grrr!
	//       this widget should be the top widget in the heirarchy according to the FLTK docs,
	//       but that messes up the tracking of mouse motion by the CinePaintCanvas widget,
	//       so we place the dodgy-popup at the bottom and handle a mouse event ourselves to show
	//       the popup
	m_popup_menu = new Fl_Menu_Button(0, 30, w - 16, h - 46);
	m_popup_menu->type(Fl_Menu_Button::POPUP3);
	m_popup_menu->menu(_menu->menu());

	// create some scrollbars - we manage the 'scrolling' ourselves
	m_horiz_scroll = new Fl_Scrollbar(0, h - 16, w - 16, 16);
	m_horiz_scroll->type(FL_HORIZONTAL);
	m_horiz_scroll->callback(static_horiz_scrollbcr_cb, this);
	m_vert_scroll = new Fl_Scrollbar(w - 16, 30, 16, h - 30 - 16);
	m_vert_scroll->type(FL_VERTICAL);
	m_vert_scroll->callback(static_vert_scrollbcr_cb, this);

	// @hack [claw] dummy positional widgets for the rulers once imlemented
	//m_ruler_width = 16; // this needs defined/configured within the ruler class
	m_ruler_origin = new Fl_Box(0, 30, 16, 16);
	m_ruler_origin->box(FL_THIN_UP_FRAME);
	m_horizontal_ruler = new Fl_Box(16, 30, w - 16 -16, 16);
	m_horizontal_ruler->box(FL_THIN_UP_FRAME);
	m_vertical_ruler = new Fl_Box(0, 30 + 16, 16, h - 30 - 16 - 16);
	m_vertical_ruler->box(FL_THIN_UP_FRAME);
	m_rulers_visible = true;

	// create the draw area to display the image
	m_draw_area = new CinePaintDrawArea(16, 30 + 16, w - 16 - 16, h - 46 - 16);
	m_draw_area_mouse_handler = new ImageWindowMouseHandler(*this);
	m_draw_area->AddMouseListener(m_draw_area_mouse_handler);
	
	CinePaintApp::GetInstance().GetToolManager().AddToolChangeListener(&m_tool_proxy);
	m_draw_area->AddMouseListener(&m_tool_proxy);
	
	// register a CinePaintWidgetListener to update the scrollbars if the DrawArea changes size
	m_draw_area_widget_listener = new DrawAreaWidgetListener(*this);
	m_draw_area->AddWidgetListener(m_draw_area_widget_listener);


	// allow the draw area to be resized if the window size is changed
	resizable(m_draw_area);

	this->end();

	m_info_window = 0;
}

CinePaintImageWindow::~CinePaintImageWindow()
{
	if(m_draw_area)
	{
		if(m_draw_area_mouse_handler)
		{
			m_draw_area->RemoveMouseListener(m_draw_area_mouse_handler);
		}
		delete m_draw_area;
		m_draw_area = 0;
	}

	if(m_draw_area_mouse_handler)
	{
		delete m_draw_area_mouse_handler;
		m_draw_area_mouse_handler = 0;
	}

	if(m_menu_functor_helper)
	{
		delete m_menu_functor_helper;
		m_menu_functor_helper = 0;
	}

	if(m_info_window)
	{
		delete m_info_window;
		m_info_window = 0;
	}
	CinePaintApp::GetInstance().GetToolManager().RemoveToolChangeListener(&m_tool_proxy);
}


/**
 * Returns the drawing area of this ImageWindow
 *
 * @return the CinePaintDrawArea displaying the CinePaintImage within this ImageWindow
 */
CinePaintDrawArea* CinePaintImageWindow::GetImageDrawArea()
{
	return(m_draw_area);
}


/**
 * Toggles the visibility of the draw area Rulers
 *
 */
void CinePaintImageWindow::ToggleRulerVisibility()
{
	SetRulersVisible(!m_rulers_visible);
}

/**
 * Sets the visibility of the canvas rulers
 *
 * @param visible set true to show the canvas rulers
 */
void CinePaintImageWindow::SetRulersVisible(bool visible)
{
	if(m_rulers_visible != visible)
	{
        m_rulers_visible = visible;

		if(m_rulers_visible)
		{
			m_ruler_origin->show();
			m_horizontal_ruler->show();
			m_vertical_ruler->show();

			m_draw_area->position(m_vertical_ruler->w(), 30 + m_horizontal_ruler->h());
		}
		else
		{
			m_ruler_origin->hide();
			m_horizontal_ruler->hide();
			m_vertical_ruler->hide();

			m_draw_area->position(0, 30);
		}
	}

	this->redraw();
}

/**
 * Gets the visibility of the canvas rulers
 *
 * @true the current visibility of the canvas rulers
 */
bool CinePaintImageWindow::GetRulersVisible()
{
	return(m_rulers_visible);
}

void CinePaintImageWindow::ToggleInfoWindow()
{
	SetInfoWindowVisible(m_info_window ? false : true);
}

void CinePaintImageWindow::SetInfoWindowVisible(bool visible)
{
	if(visible)
	{
        if(!m_info_window)
		{
			m_info_window = new ImageInfoWindow(200, -1, "Image Details");
			m_draw_area->AddMouseListener(m_info_window);
			if(m_draw_area->GetImage())
			{
				m_info_window->SetImage(m_draw_area->GetImage(), m_draw_area);
			}
			m_info_window->callback(info_window_close_callback, this);
		}

		m_info_window->show();
	}
	else
	{
		if(m_info_window)
		{
			m_draw_area->RemoveMouseListener(m_info_window);
			m_info_window->hide();
			delete m_info_window;
			m_info_window = 0;
		}
	}
}


/**
 * Sets the CinePaintImage this CinePaintDrawArea display
 *
 * @param image the CinePaintImage to display
 * @param renderer the AbstractRenderer used to render the image this draw area displays
 */
void CinePaintImageWindow::SetImage(CinePaintImage* image, AbstractRenderer* renderer)
{
	m_draw_area->SetImage(image, renderer);

	if(m_info_window)
	{
		m_info_window->SetImage(m_draw_area->GetImage(), m_draw_area);
	}

	update_scrollbars();
}

/**
 * Returns the image being displayed upon this CinePaintImageWindow
 *
 * @return the image being displayed
 */
CinePaintImage* CinePaintImageWindow::GetImage()
{
	return(m_draw_area->GetImage());
}


//----------------------
// window sizing methods

/**
 * Fits this window to the current image scale.
 * If the scale is currently smaller than this window, the window is shrunk to disaplay
 * all of the image with no additional surrounding space. If the image is larger then this
 * window, this window is enlarged to fit the image.
 * If the image is larger than the screensize no attempt is made to shrinkwrap
 *
 */
void CinePaintImageWindow::ShrinkwrapImage()
{
	if(m_draw_area->GetImage())
	{
		CinePaintImage* _image = m_draw_area->GetImage();

		// calcualte the full image size, taking into account the projection zoom
		int _im_width = static_cast<int>(_image->GetWidth() * m_draw_area->getProjection().GetZoom());
		int _im_height = static_cast<int>(_image->GetHeight() * m_draw_area->getProjection().GetZoom());

		// would the new window size fit the screen size?
		// we use the draw area x and y position as a shortcut for the scrollbar size (if visible)
		// and the menubar
		int _win_width = m_draw_area->x() + _im_width + m_vert_scroll->w();
		int _win_height = m_draw_area->y() + _im_height + m_horiz_scroll->h();

		if((_win_width <= Fl::w()) && (_win_height <= Fl::h()))
		{
			this->size(_win_width, _win_height);
		}
	}
}


/**
 * Builds the Image Window Menu
 *
 * @param menu the menu to add items to
 * @param menu_helper the menu functor helper for managing created menu functors
 */
void CinePaintImageWindow::build_menu(Fl_Menu_Bar& menu, MenuFunctorHelper& menu_helper)
{	menu_helper.AddMenuItem0(menu, 0, "&File/&New...", &CinePaintAppUI::GetInstance(), &CinePaintAppUI::NewDocument);
	menu_helper.AddMenuItem0(menu, 0, "&File/&Revert", this, &CinePaintImageWindow::revert_doc_callback);
	menu_helper.AddMenuItem0(menu, 0, "&File/&Open...", &CinePaintAppUI::GetInstance(), &CinePaintAppUI::OpenDocument);
	menu_helper.AddMenuItem0(menu, 0, "&File/&Save...", this, &CinePaintImageWindow::file_save_callback);
	menu_helper.AddMenuItem0(menu, 0, "&File/&Save As...", this, &CinePaintImageWindow::file_save_as_callback);

	menu_helper.AddMenuItem0(menu, 0, "_&File/&Close", this, &CinePaintImageWindow::file_close_callback);
	
	menu_helper.AddMenuItem0(menu, 0, "&View/&Toggle Info Window", this, &CinePaintImageWindow::ToggleInfoWindow);
	menu_helper.AddMenuItem0(menu, 0, "&View/&Toggle Rulers", this, &CinePaintImageWindow::ToggleRulerVisibility);
	menu_helper.AddMenuItem0(menu, 0, "&View/&New View", this, &CinePaintImageWindow::open_new_view);

	menu_helper.AddMenuItem0(menu, 0, "&View/&Shrinkwrap", this, &CinePaintImageWindow::ShrinkwrapImage);


	// Build the tools menu
	// we iterate over the registered tools to get the tool name and groups so we can sort the menu
	std::list<MenuSortHelper> _menu_path_list;
	ToolManager& _tool_manager = CinePaintApp::GetInstance().GetToolManager();
	for(ToolManager::const_iterator _citer = _tool_manager.begin(); _citer != _tool_manager.end(); ++_citer)
	{
		std::string _menu_path("&Tools/");

		if(_citer->second->GetGroup())
		{
			std::string _group(_citer->second->GetGroup());
			if(_group[_group.length() -1] != '/')
			{
				_group.append("/");
			}
			_menu_path.append(_group);
		}

		_menu_path.append(_citer->second->GetName());
		MenuSortHelper _helper;
		_helper.m_tool_path = _menu_path;
		_helper.m_tool_type = _citer->second->GetToolType();

		_menu_path_list.push_back(_helper);
	}

	_menu_path_list.sort(MenuPathLess());

	for(std::list<MenuSortHelper>::const_iterator _citer = _menu_path_list.begin(); _citer != _menu_path_list.end(); ++_citer)
	{
		// the tool type const char* is statically allocated within the tool so there is no need to copy the string
		menu_helper.AddMenuItem1(menu, 0, (*_citer).m_tool_path.c_str(), &_tool_manager, (*_citer).m_tool_type, &ToolManager::SetSelectedTool);
	}



	// build the plugin menu
	World* world=CinePaintApp::GetWorld();
	PDB* pdb=PDB::GetPDB(world);
	if(!pdb)
	{	return;
	}
	for(PDB::pdb_const_iterator _citer = pdb->PluginsBegin();_citer != pdb->PluginsEnd();++_citer)
	{
		PDB::PluginPair _pair = *_citer;

		// format the menu path to remove the legacy window name i.e. '<image>'
		// assumes that all the paths a re prefixed in some way by <...>
		std::string _menu_path(_pair.second->GetMenuPath());
		PluginUtils::StripWindowName(_menu_path);

		// we call execure_plugin_wrapper here rather than Excute it self on the prcide database because
		// the excute method returns a value. The functor used within the menu does not support return value.
		// (does a return value to a menu click make sense anyway?)
		menu_helper.AddMenuItem1(menu, 0, _menu_path.c_str(), this, _pair.second->GetName(), &CinePaintImageWindow::execute_plugin_wrapper);
	}
}

/**
 * callback for plugin menu items to execute a named plugin.
 * This method get the procedure database and excutes the named plugin.
 * This method is a required helper for the menu item as the excute method of the prcedure
 * database returns a value, which is not supported (at this time) by the MenuFunctorHelper
 * used to create the menu, i.e. it expects a void return value.
 *
 */
void CinePaintImageWindow::execute_plugin_wrapper(const char* name)
{	World* world=CinePaintApp::GetWorld();
	CinePaintApp::GetInstance().GetPluginExecutioner().Execute(name, *GetImage(),world);
}

/**
 * Callback for the Save menu item, we need to pass the document to the
 * save handler and so need a callback to do this, since the helper cannot
 * deal with this extra data.
**/
void CinePaintImageWindow::file_save_callback()
{
	if(m_draw_area->GetImage())
	{
		CinePaintAppUI::GetInstance().SaveDocument(*m_draw_area->GetImage());
	}
}

/**
 * as file_save_callback but for save as
**/
void CinePaintImageWindow::file_save_as_callback()
{
	if(m_draw_area->GetImage())
	{
		CinePaintAppUI::GetInstance().SaveAsDocument(*m_draw_area->GetImage());
	}
}

void CinePaintImageWindow::file_close_callback()
{
	// we dont close the document explicitly. we simply close this view
	// and if that so happens to be the only reference, the document is
	// closed.

	if(m_draw_area->GetImage())
	{
		CinePaintAppUI::GetInstance().GetDocViewManager().CloseView(*m_draw_area->GetImage(), *this);
	}
}

void CinePaintImageWindow::revert_doc_callback()
{
	if(m_draw_area->GetImage())
	{
		CinePaintAppUI::GetInstance().RevertDocument(*m_draw_area->GetImage());
	}
}

/**
 * Requests a new view of the CinePaintImage being diaplyed within this CinePaintImageWindow
 *
 */
void CinePaintImageWindow::open_new_view()
{
	if(m_draw_area->GetImage())
	{	CinePaintAppUI::GetInstance().GetDocViewManager().OpenView(*m_draw_area->GetImage());
	}
}

/**
 * Updates the scroll bar values of the window based upon the current projection settings of the CinePaintDrawArea
 *
 */
void CinePaintImageWindow::update_scrollbars()
{
	int _x_value = m_draw_area->getProjection().GetOffSetX();
	int _y_value = m_draw_area->getProjection().GetOffSetY();

	if((_x_value + m_draw_area->getProjection().GetWidth()) > m_draw_area->GetImage()->GetWidth())
	{
		if(m_draw_area->GetImage()->GetWidth() < m_draw_area->getProjection().GetWidth())
		{
			m_draw_area->getProjection().SetOffSet(0, m_draw_area->getProjection().GetOffSetY());
			_x_value = 0;
		}
		else
		{
			m_draw_area->getProjection().SetOffSet(m_draw_area->GetImage()->GetWidth() - m_draw_area->getProjection().GetWidth(), m_draw_area->getProjection().GetOffSetY());
			_x_value = m_draw_area->GetImage()->GetWidth() - m_draw_area->getProjection().GetWidth();
		}
	}

	if((_y_value + m_draw_area->getProjection().GetHeight()) > m_draw_area->GetImage()->GetHeight())
	{
		if(m_draw_area->GetImage()->GetHeight() < m_draw_area->getProjection().GetHeight())
		{
			m_draw_area->getProjection().SetOffSet(m_draw_area->getProjection().GetOffSetX(), 0);
			_y_value = 0;
		}
		else
		{
			m_draw_area->getProjection().SetOffSet(m_draw_area->getProjection().GetOffSetX(), m_draw_area->GetImage()->GetHeight() - m_draw_area->getProjection().GetHeight());
			_y_value = m_draw_area->GetImage()->GetHeight() - m_draw_area->getProjection().GetHeight();
		}
	}

	m_horiz_scroll->value(_x_value, m_draw_area->w(), 0, m_draw_area->GetImage()->GetWidth());
	m_vert_scroll->value(_y_value, m_draw_area->h(), 0, m_draw_area->GetImage()->GetHeight());
}

/**
 * Info window close callback - hides the window and defers destruction to the idle loop
 *
 * @param w the info window being destroyed
 * @param data a pointer to the CinePaintImageWindow
 */
void CinePaintImageWindow::info_window_close_callback(Fl_Widget* w, void* data)
{
	CinePaintImageWindow* _image_window = (CinePaintImageWindow*)data;
	_image_window->m_info_window->hide();
	_image_window->m_draw_area->RemoveMouseListener(_image_window->m_info_window);

	CinePaintAppUI::GetInstance().GetUIThreadEventList().PostEvent(new UIUtils::IdleLoopWindowDestructor(_image_window->m_info_window));

	// we set our reference to 0, the idle loop event contains the only remaining reference to the
	// window we want destroyed, which will be destroyed outside the window callback.
	_image_window->m_info_window = 0;
}

/**
 * Handles a horizontal scrollbar movement, updating the DrawArea's projection with the new offset value
 *
 */
void CinePaintImageWindow::horiz_scrollbar_cb()
{
	m_draw_area->getProjection().SetOffSet(m_horiz_scroll->value(), m_draw_area->getProjection().GetOffSetY());
	m_draw_area->redraw();
}

/**
 * Handles a vertical scrollbar movement, updating the DrawArea's projection with the new offset value
 *
 */
void CinePaintImageWindow::vert_scrollbar_cb()
{
	m_draw_area->getProjection().SetOffSet(m_draw_area->getProjection().GetOffSetX(), m_vert_scroll->value());
	m_draw_area->redraw();
}




//---------------
// Nested Classes

// ImageWindowMouseHandler

CinePaintImageWindow::ImageWindowMouseHandler::ImageWindowMouseHandler(CinePaintImageWindow& image_window)
: m_image_window(image_window)
{
	m_focus_follows_mouse = false;
}

CinePaintImageWindow::ImageWindowMouseHandler::~ImageWindowMouseHandler()
{

}

void CinePaintImageWindow::ImageWindowMouseHandler::MouseEntered()
{
	if(m_focus_follows_mouse)
	{
		update_active_document();
	}
}

void CinePaintImageWindow::ImageWindowMouseHandler::MouseExited()
{}

void CinePaintImageWindow::ImageWindowMouseHandler::MousePressed(int button, int count, int x, int y, unsigned int modifier)
{
	update_active_document();
	
	if(button == FL_RIGHT_MOUSE)
	{
		m_image_window.m_popup_menu->popup();
	}
}

void CinePaintImageWindow::ImageWindowMouseHandler::MouseReleased(int button, int x, int y)
{}

void CinePaintImageWindow::ImageWindowMouseHandler::MouseDragged(int button, int x, int y)
{}

void CinePaintImageWindow::ImageWindowMouseHandler::MouseMoved(int x, int y)
{}

void CinePaintImageWindow::ImageWindowMouseHandler::update_active_document()
{
	// set the current image
	CinePaintImage* _image = m_image_window.GetImage();
	if(_image)
	{
		CinePaintApp::GetInstance().GetDocList().SetCurrentDoc(*_image);
	}
}




// DrawAreaWidgetListener

CinePaintImageWindow::DrawAreaWidgetListener::DrawAreaWidgetListener(CinePaintImageWindow& image_window)
: m_image_window(image_window)
{}

void CinePaintImageWindow::DrawAreaWidgetListener::WidgetResized(int w, int h)
{
	m_image_window.update_scrollbars();
}
