/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      SimpleRenderer - Render background only
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: SimpleRenderer.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "SimpleRenderer.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include "plug-ins/pdb/Drawable.h"
#include "plug-ins/pdb/CinePaintImage.h"
#include "plug-ins/pdb/Layer.h"
#include "plug-ins/pdb/LayerManager.h"
#include "plug-ins/pdb/SelectionManager.h"
//#include <app/CinePaintApp.h"
#include "plug-ins/pdb/ImageOpManager.h"
#include "plug-ins/pdb/CinePaintCompositeOp.h"

#include <memory>

/**
 * Constructs a new SimpleRenderer to render a CinePaintImage to its final rendered image.
 * 
 * @note [claw] this is a cimple test implementation
 */
SimpleRenderer::SimpleRenderer(CinePaintImage& image)
: m_image(image)
{
	m_render_buffer = 0;
	m_glass_buffer = 0;
	m_invalid_region.SetRect(0, 0, image.GetWidth(), image.GetHeight());
}

/**
 * Destructor
 */
SimpleRenderer::~SimpleRenderer()
{
	if(m_render_buffer)
	{
		delete m_render_buffer;
		m_render_buffer = 0;
	}
}

void SimpleRenderer::Render()
{
	// render the entire image
	Render(CPRect(0, 0, m_image.GetWidth(), m_image.GetHeight()));
}

void SimpleRenderer::Render(const CPRect& render_bounds)
{
	// get/create our layer render buffer
	AbstractBuf* _render_buffer = get_render_buffer();

	render_visible_layers(m_image, *_render_buffer, render_bounds);

	ClearInvalidRegion(render_bounds);
}


/**
 * Returns the AbsrtactBuf representing the rendered version of the item this AbsractRenderer is rendering
 * This method does not check the valid state before returning the rendered buffer, the caller should therefore
 * check that the buffer is valid with a call to IsValid() and call Render() as required
 *
 * @return the rendering buffer
 */
const AbstractBuf& SimpleRenderer::GetRender()
{
	//
	// @TODO [claw] we need to handle if the allocation of the buffer failed
	//

	AbstractBuf* _buf = get_render_buffer();

	return(*_buf);
}


/**
 * Get the glass layer for the associated CinePaintImage.
 * @return the AbstractBuf containing the GlassLayer required for this image.
**/
const AbstractBuf& SimpleRenderer::GetGlassLayer()
{
	AbstractBuf* _buf = get_glass_buffer();

	return(*_buf);	
}

/**
 * Returns whether this CinePaintRenderer contains a vlid render.
 * If the item being rendered has not changed since the last time the item was rendered,
 * true is returned, otherwise false is returned.
 *
 * @return true if this renderer contains a valid render, false otherwise
 */
bool SimpleRenderer::IsValid() const
{
	return(m_invalid_region.IsEmpty());
}

/**
 * Returns whether the specified region of this render is valid.
 * If the item being rendered has not modified the specified region since its was
 * last rendered this method returns true, otherwise false is returned.
 *
 * @param bounds the region to check as being valid
 * @return true if the specified region is valid, false otherwise
 */
bool SimpleRenderer::IsValid(const CPRect& bounds) const
{
	return(!(m_invalid_region.Intersects(bounds)));
}

/**
 * Sets the current render invalid, or out of date.
 * An out of date render should be re-rendered before drawn on screen
 *
 */
void SimpleRenderer::SetInvalid()
{
	m_invalid_region.SetRect(0, 0, m_image.GetWidth(), m_image.GetHeight());
}

/**
 * Sets the specified area invalid, or out of date, or so required a re-render
 * A renderor implementation may be able to optimize its re-rendering based upon
 * the area the is invalid
 *
 * @param invalid_bounds the region to set invalid
 */
void SimpleRenderer::SetInvalid(const CPRect& invalid_bounds)
{
	m_invalid_region.Add(invalid_bounds);
}





/**
 * Clears the current invalid region
 *
 */
void SimpleRenderer::ClearInvalidRegion()
{
	m_invalid_region.SetRect(0, 0, 0, 0);
}

/**
 * Sets the valid state of this AbstractRenderer
 *
 * @param region the region to invalidate
 */
void SimpleRenderer::ClearInvalidRegion(const CPRect& region)
{
	// @TODO need to fix this, this needs subtracted from the rectangle we have
	//       for now we simple check if the specified region contains our entire
	//       invalid region.

	if(region.Contains(m_invalid_region))
	{
		m_invalid_region.SetRect(0, 0, 0, 0);
	}
}

/**
 * Returns the width of this Renderer.
 * The width is usually the width of the Drawable being rendered
 *
 * @return the width of this Renderer
 */
int SimpleRenderer::GetWidth() const
{
	return(m_image.GetWidth());
}

/**
 * Returns the height of thie Renderer.
 * The height is usually the height of the Drawable being rendered
 *
 * @return the height of thie Renderer
 */
int SimpleRenderer::GetHeight() const
{
	return(m_image.GetHeight());
}






/**
 * Convenience method to return (and possibly construct) the current render buffer
 * If no render buffer has been allocated, or the current buffer size does not
 * match the size of the current image, this method constructs a new render
 * bufferer, deleting any previous buffer. The current render buffer is then
 * returned. If construction fails, 0 is returned
 *
 * @return the current render buffer, or 0 if the buffer cannot be constructed
 */
AbstractBuf* SimpleRenderer::get_render_buffer()
{
	AbstractBuf* _buf = 0;

	if(!m_render_buffer || (m_render_buffer->GetWidth() != m_image.GetWidth()) || (m_render_buffer->GetHeight() != m_image.GetHeight()))
	{
		if(m_render_buffer)
		{
			delete m_render_buffer;
			m_render_buffer = 0;
		}

		m_render_buffer = create_render_buffer(m_image);
		_buf = m_render_buffer;
	}
	else
	{
		_buf = m_render_buffer;
	}

	return(_buf);
}


AbstractBuf* SimpleRenderer::get_glass_buffer()
{

	//CinePaintTag _tag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false);
	//if ( !m_glass_buffer)
	//{
	//	std::auto_ptr<AbstractBuf> _tmp_buf = AbstractBufFactory::CreateBuf(_tag, m_image.GetWidth(), m_image.GetHeight(), AbstractBufFactory::STORAGE_FLAT);
	//	if(_tmp_buf.get())
	//	{
	//		m_glass_buffer = _tmp_buf.release();
	//		m_glass_buffer->RefPortionReadWrite(0, 0);
	//	}
	//}

	Layer *_layer = m_image.GetLayers().GetActiveLayer();
	AbstractBuf& glass = _layer->GetSelectionManager().GetSelectionMask();
	return(&glass);
}

/**
 * Allocates the Rendered Image data buffer.
 * The Rendered Image Buffer stores the final rendered composition of the CinePaintImage
 * this renderer is rendering with all relevant Layers combined etc.
 * The buffer is constructed to match the size of the specified image.
 * If construction fails, 0 is returned.
 *
 * @todo need to look at the format options for the render buffer
 * @param image the CinePaintImage to render
 * @return constructed AbstractBuf to render the image into
 */
AbstractBuf* SimpleRenderer::create_render_buffer(CinePaintImage& image)
{
	AbstractBuf* _buf = 0;

	CinePaintTag _tag(image.GetTag());

	// @note [claw] I'm skipping this for now
	//       I'm not sure what the correct behaviour should actually be
	//_tag.SetAlpha(true);

	if(_tag.GetFormat() == CinePaintTag::FORMAT_INDEXED_ENUM)
	{
		_tag.SetFormat(CinePaintTag::FORMAT_RGB_ENUM);
	}

	std::auto_ptr<AbstractBuf> _tmp_buf = AbstractBufFactory::CreateBuf(_tag, image.GetWidth(), image.GetHeight(), AbstractBufFactory::STORAGE_FLAT);

	if(_tmp_buf.get())
	{
		_buf = _tmp_buf.release();
		_buf->RefPortionReadWrite(0, 0);
	}

	return(_buf);
}





/**
 * Renders all the currently visible layers within the current image into the current render buffer
 * The layers are combind using the standard composite image op
 *
 * @param render_bounds area to be rendered
 */
void SimpleRenderer::render_visible_layers(CinePaintImage& image, AbstractBuf& _render_buffer, const CPRect& render_bounds)
{
	// build a list of the current visisble layers
	std::list<Layer*> _layer_list;
	for(LayerManager::const_iterator _citer = image.GetLayers().begin(); _citer != image.GetLayers().end(); ++_citer)
	{
		Layer* _layer = *_citer;
		if(_layer->GetVisible())
		{
			_layer_list.push_back(_layer);
		}
	}

	if(_layer_list.empty())
	{
		// @TODO nothing to render - fill with the default transparent checkerboard pattern
	}
	else if(_layer_list.size() == 1)
	{
		// render only the background layer
		Layer* _background_layer = image.GetLayers().GetBackgroundLayer();

		if(_background_layer && _background_layer->GetVisible())
		{
			//
			// @TODO [claw] we really need a general copy PixelArea method here
			//

			// get the intersection of the layer and the update bounds
			CPRect _layer_bounds(_background_layer->GetOffsetX(), _background_layer->GetOffsetY(), _background_layer->GetWidth(), _background_layer->GetHeight());
			CPRect _intersect_bounds;
			render_bounds.Intersect(_layer_bounds, _intersect_bounds);

			AbstractBuf& _buf = _background_layer->GetBuffer();
			unsigned char* _src_data = _buf.GetPortionData(0, 0);
			unsigned char* _src_row = _src_data + _buf.GetTag().GetBytes() * _buf.GetWidth() * _intersect_bounds.GetY();

			unsigned char* _dst_data = _render_buffer.GetPortionData(0, 0);
			unsigned char* _dst_row = _dst_data + _render_buffer.GetTag().GetBytes() * _render_buffer.GetWidth() * _intersect_bounds.GetY();

			for(int i = 0; i < (int)_intersect_bounds.GetHeight(); i++)
			{
				unsigned char* _src_buf = _src_row + (_buf.GetTag().GetBytes() * (_intersect_bounds.GetX() - _layer_bounds.GetX()));
				unsigned char* _dst_buf = _dst_row + (_render_buffer.GetTag().GetBytes() * _intersect_bounds.GetX());

				memcpy(_dst_buf, _src_buf, _render_buffer.GetTag().GetBytes() * _intersect_bounds.GetWidth());

				_src_row += _render_buffer.GetTag().GetBytes() * _render_buffer.GetWidth();
				_dst_row += _buf.GetTag().GetBytes() * _buf.GetWidth();
			}
		}
	}
	else
	{
		// get the composite image op and merge the visible layers
		ImageOpManager* iom = ImageOpManager::GetImageOpManager();
		if(!iom)
		{	return;
		}
		CinePaintCompositeOp* _composite_op = iom->GetCompositeOp(_render_buffer.GetTag());

		if(_composite_op)
		{
			// composite the visible layers using the current render bounds
			_composite_op->CompositeLayers(_layer_list, _render_buffer, render_bounds);
		}
		else
		{
			// couldn't get the merge handling image-op, cant render the image correctly.
			printf("SimpleRenderer::Render: Couldn't get CinePaintCompositeOp, cannot Composite image\n");
		}
	}
}
