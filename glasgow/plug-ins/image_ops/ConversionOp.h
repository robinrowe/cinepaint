/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ScaleOp - Scale an image buffer
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ConversionOp.h,v 1.2 2006/12/18 08:21:22 robinrowe Exp $
 */
 
#ifndef _CONVERSION_OP_H_
#define _CONVERSION_OP_H_

#include "dll_api.h"
#include "plug-ins/pdb/CinePaintImageOp.h"
#include "plug-ins/pdb/CinePaintConversionOp.h"
#include "plug-ins/pdb/CinePaintTag.h"

// forward delcaration
class AbstractBuf;
class PixelArea;

extern "C" IMAGE_CINEPAINT_OPS_API CinePaintImageOp* CreateConversionOpObject();


/**
 * CinePaintConversionOp implementation.
 * ScaleOp provides the core CinePaintScaleOp as a CinePaintImageOp plugin. This provides the
 * core scale operation within CinePaint.
 *
 */
class IMAGE_CINEPAINT_OPS_API ConversionOp : public CinePaintImageOp, public CinePaintConversionOp
{
	public:
		/**
		 * Constructs a new CinePaint RenderOp
		 *
		 */
		ConversionOp();

		/**
		 * Destructor
		 */
		~ConversionOp() {};


		//================================
		// Overrides from CinePaintConversionOp 

		virtual bool CopyConvert(AbstractBuf &dst, const AbstractBuf &src) ;

		virtual bool CopyConvert(PixelArea &dst, const PixelArea &src);

		virtual AbstractBuf &Convert(AbstractBuf &buf, CinePaintTag &tag);


		//================================
		// Overrides from CinePaintImageOp
		/**
		 * Returns the Image Op Type of this CinePaintImageOp
		 * If this CinePaintImageOp provides a known operation set, by implementing one of the
		 * defined ImageOp interfaces, this method shopuld return one of the defined Image Op 
		 * strings, otherwise, the image op type may be user defined
		 *
		 * @return the Image Op type of this CinePaintImageOp
		**/
		virtual const char* GetImageOpType() const;

		/**
		 * Returns true if this CinePaintImageOp supports operations on Image data of the specified CinePaintTag
		 * A particular implementation of a CinePaintImageOp may choose to operate on one or more image
		 * data types. There may be multiple CinePaintImageOp for a known Image Op type that handle various
		 * data formats, this method may be used to determine if this CinePaintImageOp handles the specified
		 * format
		 *
		 * @return trus if this CinePaintImageOp supports the spoecified CinePaintTag
		**/
		virtual bool SupportsTag(const CinePaintTag& tag) const;

		// Nested Classes for different source data formats.

private:


	//====================================
	// Nested classes for format handling.

	/**
	 * BufferConvertor is an Abstract Base class for all Classes capable of converting image data
	 * in one format into another format.
	 * BufferConvertor implemenations are intended to convert image data contained in various 
	 * supported data formats into other formats. This may require double conversion if a direct
	 * route is not available.
	**/
	class BufferConvertor
	{
	public:
		/**
		* Destructor
		*/
		virtual ~BufferConvertor() {};

		/**
		 * Convert the specified row of the src abstractbuf to the dst abstractbuf.
		 * The step parameter may be used to set the step value between successive pixel from src,
		 * this allows the src to be reduced in size by skipping, and only converting every step pixels
		 * This method inverts an image mask.
		 * 
		 * @todo error/bounds checking what do we do if we try to draw past the end of the canvas?
		 * @param buf the data buffer to render the AbstractBuf data into
		 * @param canvas the AbstractBuf to render
		 * @param row the canvas row to render
		 * @param offset an offset from the left of the row to render
		 * @param width the number of pixels to render from the specified row
		 * @param step a step value between successive pixels.
		**/
		virtual void ConvertMaskRow(AbstractBuf& dst, AbstractBuf& src, int row, int offset = 0, int width = -1, int step = 1) = 0;
		/**
		 * As above but without inverting the data.
		**/
		virtual void ConvertRow(AbstractBuf& dst, const AbstractBuf& src, int row, int offset = 0, int width = -1, int step = 1) = 0;

		/**
		 * Covert the specified AbstractBuf into buf.
		 * This method inverts an image mask for display
		 *
		 * @param buf the data buffer to render the AbstractBuf into
		 * @param canvas the AbstractBuf to render
		 * @param width the number of pixels to render from the specified row
		 * @param step a step value between successive pixels.
		**/
		virtual void ConvertMask(AbstractBuf& dst, AbstractBuf& src, int width = -1, int step = 1) = 0;

		/**
		 * As ConvertMask, without inverting image data
		**/
		virtual void Convert(AbstractBuf& dst, AbstractBuf& src, int width = -1, int step = 1) = 0;
		virtual void Convert(PixelArea& dst, const PixelArea& src) = 0;

	protected:
		/**
		 * Dis-allow direct instances of this abstract level class
		*/
		BufferConvertor() {};

		/** Function Pointer for pixel conversion routine.
		 * Perhaps not the best way to do this, but another set of nested
		 * classes seems too much
		**/
		typedef void ( BufferConvertor::*PFNPixelConvertor)(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre) ; 

		/**
		 * Fuction to retrieve correct pixel conversion function, based on 
		 * the format of the twop buffers.
		**/
		PFNPixelConvertor get_pixel_convertor(CinePaintTag::Format dst);
		/**
		 * Single Pixel conversion routines.
		**/
		virtual void to_rgb(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre) = 0;
		virtual void to_grey(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre) = 0;
		virtual void to_index(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre) = 0;

	}; /* class BufferConvertor */

	/**
	 * Returns a BufferConvertor suitable for the specfied CinePaintTag
	 *
	 * @param tag the Image data type CinePaintTag
	 * @return a BufferConvertor suitable for the specified Tag
	**/
	static BufferConvertor *get_buffer_convertor(const CinePaintTag& tag);

	/**
	 * BufferConvertor implementation to support U8_RGB format conversions.
	**/
	class BufferConvertorU8_GRAY : public BufferConvertor
	{
		public:
			BufferConvertorU8_GRAY() {};
			virtual ~BufferConvertorU8_GRAY() {};

			virtual void ConvertMaskRow(AbstractBuf& dst, AbstractBuf& src, int row, int offset = 0, int width = -1, int step = 1);
			virtual void ConvertRow(AbstractBuf& dst, const AbstractBuf& src, int row, int offset = 0, int width = -1, int step = 1);
			virtual void ConvertMask(AbstractBuf& dst, AbstractBuf& src, int width = -1, int step = 1);
			virtual void Convert(AbstractBuf& dst, AbstractBuf& src, int width = -1, int step = 1);
			virtual void Convert(PixelArea& dst, const PixelArea& src);

			virtual void to_rgb(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre);
			virtual void to_grey(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre);
			virtual void to_index(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre);
	}; /* class BufferConvertorU8_GRAY */

	/**
	 * BufferConvertor implementation to support U8_RGB format conversions.
	**/
	class BufferConvertorU8_RGB : public BufferConvertor
	{
		public:
			BufferConvertorU8_RGB() {};
			virtual ~BufferConvertorU8_RGB() {};

			virtual void ConvertMaskRow(AbstractBuf& dst, AbstractBuf& src, int row, int offset = 0, int width = -1, int step = 1);
			virtual void ConvertRow(AbstractBuf& dst, const AbstractBuf& src, int row, int offset = 0, int width = -1, int step = 1);
			virtual void ConvertMask(AbstractBuf& dst, AbstractBuf& src, int width = -1, int step = 1);
			virtual void Convert(AbstractBuf& dst, AbstractBuf& src, int width = -1, int step = 1);
			virtual void Convert(PixelArea& dst, const PixelArea& src);

			virtual void to_rgb(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre);
			virtual void to_grey(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre);
			virtual void to_index(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre);
	}; /* class BufferConvertorU8_RGB */

	/**
	 * BufferConvertor implementation to support U16 Gray format conversions.
	**/
	class BufferConvertorU16_GRAY : public BufferConvertor
	{
		public:
			BufferConvertorU16_GRAY() {};
			virtual ~BufferConvertorU16_GRAY() {};

			virtual void ConvertMaskRow(AbstractBuf& dst, AbstractBuf& src, int row, int offset = 0, int width = -1, int step = 1);
			virtual void ConvertRow(AbstractBuf& dst, const AbstractBuf& src, int row, int offset = 0, int width = -1, int step = 1);
			virtual void ConvertMask(AbstractBuf& dst, AbstractBuf& src, int width = -1, int step = 1);
			virtual void Convert(AbstractBuf& dst, AbstractBuf& src, int width = -1, int step = 1);
			virtual void Convert(PixelArea& dst, const PixelArea& src);

			virtual void to_rgb(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre);
			virtual void to_grey(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre);
			virtual void to_index(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre);
	}; /* class BufferConvertorU16_GRAY */


	/**
	 * BufferConvertor implementation to support U16_RGB format conversions.
	**/
	class BufferConvertorU16_RGB : public BufferConvertor
	{
		public:
			BufferConvertorU16_RGB() {};
			virtual ~BufferConvertorU16_RGB() {};

			virtual void ConvertMaskRow(AbstractBuf& dst, AbstractBuf& src, int row, int offset = 0, int width = -1, int step = 1);
			virtual void ConvertRow(AbstractBuf& dst, const AbstractBuf& src, int row, int offset = 0, int width = -1, int step = 1);
			virtual void ConvertMask(AbstractBuf& dst, AbstractBuf& src, int width = -1, int step = 1);
			virtual void Convert(AbstractBuf& dst, AbstractBuf& src, int width = -1, int step = 1);
			virtual void Convert(PixelArea& dst, const PixelArea& src);

			virtual void to_rgb(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre);
			virtual void to_grey(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre);
			virtual void to_index(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre);
	}; /* class BufferConvertorU16_RGB */


	//class BufferConvertorFloat16 : public BufferConvertor
	//{
	//	public:
	//		BufferConvertorFloat16() {};
	//		virtual ~BufferConvertorFloat16() {};

	//		virtual void ConvertMaskRow(AbstractBuf& dst, AbstractBuf& src, int row, int offset = 0, int width = -1, int step = 1);
	//		virtual void ConvertRow(AbstractBuf& dst, AbstractBuf& src, int row, int offset = 0, int width = -1, int step = 1);
	//		virtual void ConvertMask(AbstractBuf& dst, AbstractBuf& src, int width = -1, int step = 1);
	//		virtual void Convert(AbstractBuf& dst, AbstractBuf& src, int width = -1, int step = 1);
	//}; /* class BufferConvertorFloat16 */


	/**
	 * BufferConvertor implementation to support FLOAT_GRAY format conversions.
	**/
	class BufferConvertorFloat_GRAY : public BufferConvertor
	{
		public:
			BufferConvertorFloat_GRAY() {};
			virtual ~BufferConvertorFloat_GRAY() {};

			virtual void ConvertMaskRow(AbstractBuf& dst, AbstractBuf& src, int row, int offset = 0, int width = -1, int step = 1);
			virtual void ConvertRow(AbstractBuf& dst, const AbstractBuf& src, int row, int offset = 0, int width = -1, int step = 1);
			virtual void ConvertMask(AbstractBuf& dst, AbstractBuf& src, int width = -1, int step = 1);
			virtual void Convert(AbstractBuf& dst, AbstractBuf& src, int width = -1, int step = 1);
			virtual void Convert(PixelArea& dst, const PixelArea& src);

			virtual void to_rgb(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre);
			virtual void to_grey(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre);
			virtual void to_index(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre);

		private:

			/**
			 * Helper function to find max and min values in buffer.
			 * @param buf The buffer to find max and min in.
			 * @param _min recieves the minimum value found.
			 * @param _max recieves the maximum value found.
			**/
			void find_min_max(const AbstractBuf &buf, float *_min, float *_max);

			float m_scale_factor;
			float m_offset;
	}; /* class BufferConvertorFloat_GRAY */

	/**
	 * BufferConvertor implementation to support FLOAT_GRAY format conversions.
	**/
	class BufferConvertorFloat_RGB : public BufferConvertor
	{
		public:
			BufferConvertorFloat_RGB() {m_scale_factor= 255; m_offset = 0;};
			virtual ~BufferConvertorFloat_RGB() {};

			virtual void ConvertMaskRow(AbstractBuf& dst, AbstractBuf& src, int row, int offset = 0, int width = -1, int step = 1);
			virtual void ConvertRow(AbstractBuf& dst, const AbstractBuf& src, int row, int offset = 0, int width = -1, int step = 1);
			virtual void ConvertMask(AbstractBuf& dst, AbstractBuf& src, int width = -1, int step = 1);
			virtual void Convert(AbstractBuf& dst, AbstractBuf& src, int width = -1, int step = 1);
			virtual void Convert(PixelArea& dst, const PixelArea& src);

			virtual void to_rgb(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre);
			virtual void to_grey(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre);
			virtual void to_index(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre);

			/**
			 * Helper function to find max and min values in buffer.
			 * @param buf The buffer to find max and min in.
			 * @param _min recieves the minimum value found.
			 * @param _max recieves the maximum value found.
			**/
			void find_min_max(const AbstractBuf &buf, float *_min, float *_max);

			float m_scale_factor;
			float m_offset;
	}; /* class BufferConvertorFloat_RGB */

	//class BufferConvertorBFP : public BufferConvertor
	//{
	//	public:
	//		BufferConvertorBFP() {};
	//		virtual ~BufferConvertorBFP() {};

	//		virtual void ConvertMaskRow(AbstractBuf& dst, AbstractBuf& src, int row, int offset = 0, int width = -1, int step = 1);
	//		virtual void ConvertRow(AbstractBuf& dst, AbstractBuf& src, int row, int offset = 0, int width = -1, int step = 1);
	//		virtual void ConvertMask(AbstractBuf& dst, AbstractBuf& src, int width = -1, int step = 1);
	//		virtual void Convert(AbstractBuf& dst, AbstractBuf& src, int width = -1, int step = 1);
	//}; /* class BufferConvertorBFP */

	/**
	 * static instances for supported types
	**/
	static BufferConvertorU8_RGB		m_buffer_convertor_u8_rgb;
	static BufferConvertorU8_GRAY		m_buffer_convertor_u8_gray;
	static BufferConvertorU16_RGB		m_buffer_convertor_u16_rgb;
	static BufferConvertorU16_GRAY		m_buffer_convertor_u16_gray;
	static BufferConvertorFloat_GRAY	m_buffer_convertor_float_gray;
	static BufferConvertorFloat_RGB		m_buffer_convertor_float_rgb;
	//static BufferConvertorFloat16 m_buffer_convertor_float16;
	//static BufferConvertorBFP     m_buffer_convertor_bfp;

};

#endif /* _RENDER_OP_H_ */
