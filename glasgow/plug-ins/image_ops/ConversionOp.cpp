/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ScaleOp - Scale a buffer
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ConversionOp.cpp,v 1.2 2006/12/18 08:21:22 robinrowe Exp $
 */
 

#include "ConversionOp.h"
#include "plug-ins/pdb/CinePaintTag.h"
#include "plug-ins/pdb/CinePaintImage.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/PixelArea.h"
#include <cmath>

ConversionOp::BufferConvertorU8_RGB		ConversionOp::m_buffer_convertor_u8_rgb;
ConversionOp::BufferConvertorU8_GRAY	ConversionOp::m_buffer_convertor_u8_gray;
ConversionOp::BufferConvertorU16_RGB	ConversionOp::m_buffer_convertor_u16_rgb;
ConversionOp::BufferConvertorU16_GRAY	ConversionOp::m_buffer_convertor_u16_gray;
ConversionOp::BufferConvertorFloat_GRAY	ConversionOp::m_buffer_convertor_float_gray;
ConversionOp::BufferConvertorFloat_RGB	ConversionOp::m_buffer_convertor_float_rgb;

/**
 * This is the main entry point used to create an instance of the ImageOp.
**/
extern "C" IMAGE_CINEPAINT_OPS_API CinePaintImageOp* CreateConversionOpObject()
{
	return(new ConversionOp());
}

ConversionOp::ConversionOp()
: CinePaintImageOp("conversion_op", "convert between image formats", "help", "Donald MacVicar, Colin Law", "2004", "2004")
{
}

/**
 * Returns the Image Op Type of this CinePaintImageOp
 * If this CinePaintImageOp provides a known operation set, by implementing one of the
 * defined ImageOp interfaces, this method shopuld return one of the defined Image Op 
 * strings, otherwise, the image op type may be user defined
 *
 * @return the Image Op type of this CinePaintImageOp
 */
const char* ConversionOp::GetImageOpType() const
{
	return(CinePaintImageOp::GetIMAGE_OP_TYPE_CONVERSION());
}

/**
 * @return true if this CinePaintImageOp supports the spoecified CinePaintTag
 */
bool ConversionOp::SupportsTag(const CinePaintTag& tag) const
{
	bool _ret = false;

	switch(tag.GetPrecision())
	{

		case CinePaintTag::PRECISION_U8_ENUM:
		{
            switch(tag.GetFormat())
            {
				case CinePaintTag::FORMAT_GRAY_ENUM:
				case CinePaintTag::FORMAT_RGB_ENUM:
				{
					_ret=true;
					break;
				}
				case CinePaintTag::FORMAT_INDEXED_ENUM:
				default:
					_ret=false;
			}
			break;
		}
		case CinePaintTag::PRECISION_U16_ENUM:
            switch(tag.GetFormat())
            {
				case CinePaintTag::FORMAT_GRAY_ENUM:
				case CinePaintTag::FORMAT_RGB_ENUM:
				{
					_ret=true;
					break;
				}
				case CinePaintTag::FORMAT_INDEXED_ENUM:
				default:
					_ret=false;
			}
			break;
		case CinePaintTag::PRECISION_FLOAT_ENUM:
			switch(tag.GetFormat())
            {
				case CinePaintTag::FORMAT_GRAY_ENUM:
				case CinePaintTag::FORMAT_RGB_ENUM:				
				{
					_ret=true;
					break;
				}				
				case CinePaintTag::FORMAT_INDEXED_ENUM:
				default:
					_ret=false;
			}
			break;
		case CinePaintTag::PRECISION_FLOAT16_ENUM:
		case CinePaintTag::PRECISION_BFP_ENUM:
		case CinePaintTag::PRECISION_NONE_ENUM:
		default:
		{
				_ret = false;
		}
	}
	return (_ret);
}
#undef min

bool ConversionOp::CopyConvert(AbstractBuf &dst, const AbstractBuf &src)
{
	bool _res = false;

	ConversionOp::BufferConvertor *_conv = get_buffer_convertor(src.GetTag());
	int n_lines = std::min(src.GetHeight(), dst.GetHeight());
	int w = std::min(src.GetWidth(), dst.GetWidth());
	if (_conv)
	{
		for( int i=0; i < n_lines; ++i)
			_conv->ConvertRow(dst, src, i, 0,w);
		_res = true;
	}
	
	return _res;
}

bool ConversionOp::CopyConvert(PixelArea &dst, const PixelArea &src)
{
	bool _res = false;

	ConversionOp::BufferConvertor *_conv = get_buffer_convertor( src.GetBuffer().GetTag() );
	if ( _conv)
	{    _conv->Convert(dst, src);
		_res = true;
	}
	return _res;
}

AbstractBuf &ConversionOp::Convert(AbstractBuf &buf, CinePaintTag &tag)
{
	return buf;
}


/**
 * Returns a BufferConvertor suitable for the specfied CinePaintTag
 *
 * @param tag the Image data type CinePaintTag
 * @return a BufferConvertor suitable for the specified Tag
**/
ConversionOp::BufferConvertor *ConversionOp::get_buffer_convertor(const CinePaintTag& tag)
{
	ConversionOp::BufferConvertor* _conv = 0;

	switch(tag.GetPrecision())
	{
	case CinePaintTag::PRECISION_U8_ENUM:
		{
			switch(tag.GetFormat())
			{
			case CinePaintTag::FORMAT_GRAY_ENUM:
				_conv = &ConversionOp::m_buffer_convertor_u8_gray;
				break;
			case CinePaintTag::FORMAT_RGB_ENUM:
				_conv = &ConversionOp::m_buffer_convertor_u8_rgb;
				break;
			case CinePaintTag::FORMAT_INDEXED_ENUM:
			case CinePaintTag::FORMAT_NONE_ENUM:
			default:
				printf("ConversionOp::get_buffer_convertor(): bad format");
				break;
			}
		}	
		break;
	case CinePaintTag::PRECISION_U16_ENUM:
		{
			switch(tag.GetFormat())
			{
			case CinePaintTag::FORMAT_GRAY_ENUM:
				_conv = &ConversionOp::m_buffer_convertor_u16_gray;
				break;
			case CinePaintTag::FORMAT_RGB_ENUM:
				_conv = &ConversionOp::m_buffer_convertor_u16_rgb;
				break;
			case CinePaintTag::FORMAT_INDEXED_ENUM:
			case CinePaintTag::FORMAT_NONE_ENUM:
			default:
				printf("ConversionOp::get_buffer_convertor(): bad format");
				break;
			}
		}	
		break;
	case CinePaintTag::PRECISION_FLOAT_ENUM:
		{
			switch(tag.GetFormat())
			{
			case CinePaintTag::FORMAT_GRAY_ENUM:
				_conv = &ConversionOp::m_buffer_convertor_float_gray;
				break;
			case CinePaintTag::FORMAT_RGB_ENUM:
				_conv = &ConversionOp::m_buffer_convertor_float_rgb;
				break;
			case CinePaintTag::FORMAT_INDEXED_ENUM:
			case CinePaintTag::FORMAT_NONE_ENUM:
			default:
				printf("ConversionOp::get_buffer_convertor(): bad format");
				break;
			}
		}		break;
	case CinePaintTag::PRECISION_FLOAT16_ENUM:
		//			_conv = &ConversionOp::m_buffer_convertor_float16;
		break;
	case CinePaintTag::PRECISION_BFP_ENUM:
		//			_conv = &ConversionOp::m_buffer_convertor_bfp;
		break;
	case CinePaintTag::PRECISION_NONE_ENUM:
	default:
		printf("ConversionOp::get_buffer_convertor(): bad precision");
		break;
	}
	return (_conv);
}


/**
 * Pixel Convertor Function pointer retrieval.
 * Given a tag for the dst format returns the appropriate 
 * member function pointer for use in converting pixel to the
 * given format from the format handled by this class.
 * 
**/
ConversionOp::BufferConvertor::PFNPixelConvertor ConversionOp::BufferConvertor::get_pixel_convertor(CinePaintTag::Format dst)
{
	PFNPixelConvertor _res = 0;
	switch (dst)
	{
	case CinePaintTag::FORMAT_GRAY_ENUM:
		_res = &BufferConvertor::to_grey;
		break;
	case CinePaintTag::FORMAT_RGB_ENUM:
		_res = &BufferConvertor::to_rgb;
		break;
	case CinePaintTag::FORMAT_INDEXED_ENUM:
		_res = &BufferConvertor::to_index;
		break;
	default:
		break;
	}

	return _res;
}

//===========================================
// U8 RGB Conversion Implementation.
void ConversionOp::BufferConvertorU8_RGB::ConvertMaskRow(AbstractBuf& dst, AbstractBuf& src, int row, int offset, int width, int step)
{

}

void ConversionOp::BufferConvertorU8_RGB::ConvertRow(AbstractBuf& dst, const AbstractBuf& src, int row, int offset, int width, int step)
{
	const unsigned char* src_p; // Pointer into source data.
	unsigned char* dst_p; // Pointer into dst data.
	// Ensure src buffer is actually a U8
	PFNPixelConvertor _pix_conv = get_pixel_convertor(dst.GetTag().GetFormat());
	if (src.GetTag().GetPrecision() == CinePaintTag::PRECISION_U8_ENUM )
	{
		
		// Get pointer to start of required row.
		src_p = src.GetPortionData(0,row);
		dst_p = dst.GetPortionData(0,row);
		// Jump to any offset.
		src_p += offset * src.GetTag().GetBytes();
		dst_p += offset * dst.GetTag().GetBytes();
		// Loop over each pixel and convert.
		for ( int i=0; i<width; i++)
		{
			(this->*_pix_conv)(dst_p, src_p, dst.GetTag().GetPrecision());
			src_p += src.GetTag().GetBytes();
			dst_p += dst.GetTag().GetBytes();
		}
	}
	else
		printf("ERROR : ConversionOp::BufferConvertorU8 Src buffer not U8 format\n");
}

void ConversionOp::BufferConvertorU8_RGB::ConvertMask(AbstractBuf& dst, AbstractBuf& src, int width, int step)
{
}

void ConversionOp::BufferConvertorU8_RGB::Convert(AbstractBuf& dst, AbstractBuf& src, int width, int step)
{
}

void ConversionOp::BufferConvertorU8_RGB::Convert(PixelArea& dst, const PixelArea& src)
{
	// Get AbstractBufs which the PixelAreas refer to.
	const AbstractBuf &_src_buf = src.GetBuffer();
	AbstractBuf &_dst_buf = dst.GetBuffer();

	unsigned int ws = _src_buf.GetWidth();
	unsigned int wd = _dst_buf.GetWidth();
	
	// Get appropriate pixel convertor, based to dst format.
	PFNPixelConvertor _pix_conv = get_pixel_convertor(_dst_buf.GetTag().GetFormat());
	CinePaintTag::Precision _pre  = _dst_buf.GetTag().GetPrecision();

	const unsigned char* _srcp = _src_buf.GetPortionData(0,0);
	unsigned char* _dstp = _dst_buf.GetPortionData(0,0);
	unsigned int _src_bytes = _src_buf.GetTag().GetBytes();
	unsigned int _dst_bytes = _dst_buf.GetTag().GetBytes();

	unsigned int dst_line = dst.GetX();

	// Move src and dst pointers to start point for windows.


	for(int src_line = src.GetY(); src_line < src.GetY()+src.GetHeight()-1; ++src_line)
	{
		_srcp =  _src_buf.GetPortionData(0,0) + (( src_line*ws + src.GetX() ) * _src_bytes);
		_dstp =  _dst_buf.GetPortionData(0,0) + (( dst_line*wd + dst.GetX() ) * _dst_bytes);	
		for (int i = src.GetX(); i < src.GetX()+src.GetWidth()-1; ++i )
		{
			(this->*_pix_conv)(_dstp, _srcp, _pre );
			_srcp += _src_bytes;
			_dstp += _dst_bytes;
		}
		dst_line++;
	}
}

void ConversionOp::BufferConvertorU8_RGB::to_rgb(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre)
{
	// Converting from RGB to RGB Only need to worry about precision of dst.
	unsigned int i, bytes = 3;
	switch(pre)
	{
	case CinePaintTag::PRECISION_U8_ENUM: // Both the same so just copy. bytes.
		for(i=0; i<bytes; i++)
		{
			*dst = *src; 
			++dst; 
			++src;
		}
		break;
	case CinePaintTag::PRECISION_U16_ENUM: // Convert 8bit to 16??
		for(i=0; i<bytes; i++)
		{
			*dst=0;	++dst; // Set High byte = 0. [@TODO (DWM) : Proper coversion maths]
			*dst = *src; 
			++dst; 
			++src;
		}
		break;
	case CinePaintTag::PRECISION_FLOAT16_ENUM:
	case CinePaintTag::PRECISION_FLOAT_ENUM:
	case CinePaintTag::PRECISION_BFP_ENUM:
	default:
		break;
	}
}
void ConversionOp::BufferConvertorU8_RGB::to_grey(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre)
{
}

void ConversionOp::BufferConvertorU8_RGB::to_index(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre)
{
}

//===========================================
// U8_GRAY Conversion Implementation
void ConversionOp::BufferConvertorU8_GRAY::ConvertMaskRow(AbstractBuf& dst, AbstractBuf& src, int row, int offset, int width, int step)
{

}

void ConversionOp::BufferConvertorU8_GRAY::ConvertRow(AbstractBuf& dst, const AbstractBuf& src, int row, int offset, int width, int step)
{
	const unsigned char* src_p; // Pointer into source data.
	unsigned char* dst_p; // Pointer into dst data.
	// Ensure src buffer is actually a U8
	PFNPixelConvertor _pix_conv = get_pixel_convertor(dst.GetTag().GetFormat());
	if (src.GetTag().GetPrecision() == CinePaintTag::PRECISION_U8_ENUM )
	{
		
		// Get pointer to start of required row.
		src_p = src.GetPortionData(0,row);
		dst_p = dst.GetPortionData(0,row);
		// Jump to any offset.
		src_p += offset * src.GetTag().GetBytes();
		dst_p += offset * dst.GetTag().GetBytes();
		// Loop over each pixel and convert.
		for ( int i=0; i<width; i++)
		{
			(this->*_pix_conv)(dst_p, src_p, dst.GetTag().GetPrecision());
			src_p += src.GetTag().GetBytes();
			dst_p += dst.GetTag().GetBytes();
		}
	}
	else
		printf("ERROR : ConversionOp::BufferConvertorU8 Src buffer not U8 format\n");
}

void ConversionOp::BufferConvertorU8_GRAY::ConvertMask(AbstractBuf& dst, AbstractBuf& src, int width, int step)
{
}

void ConversionOp::BufferConvertorU8_GRAY::Convert(AbstractBuf& dst, AbstractBuf& src, int width, int step)
{
}

void ConversionOp::BufferConvertorU8_GRAY::Convert(PixelArea& dst, const PixelArea& src)
{
	// Get AbstractBufs which the PixelAreas refer to.
	const AbstractBuf &_src_buf = src.GetBuffer();
	AbstractBuf &_dst_buf = dst.GetBuffer();

	unsigned int ws = _src_buf.GetWidth();
	unsigned int wd = _dst_buf.GetWidth();
	
	// Get appropriate pixel convertor, based to dst format.
	PFNPixelConvertor _pix_conv = get_pixel_convertor(_dst_buf.GetTag().GetFormat());
	CinePaintTag::Precision _pre  = _dst_buf.GetTag().GetPrecision();

	const unsigned char* _srcp = _src_buf.GetPortionData(0,0);
	unsigned char* _dstp = _dst_buf.GetPortionData(0,0);
	unsigned int _src_bytes = _src_buf.GetTag().GetBytes();
	unsigned int _dst_bytes = _dst_buf.GetTag().GetBytes();

	unsigned int dst_line = dst.GetX();

	// Move src and dst pointers to start point for windows.


	for(int src_line = src.GetY(); src_line < src.GetY()+src.GetHeight()-1; ++src_line)
	{
		_srcp =  _src_buf.GetPortionData(0,0) + (( src_line*ws + src.GetX() ) * _src_bytes);
		_dstp =  _dst_buf.GetPortionData(0,0) + (( dst_line*wd + dst.GetX() ) * _dst_bytes);	
		for (int i = src.GetX(); i < src.GetX()+src.GetWidth()-1; ++i)
		{
			(this->*_pix_conv)(_dstp, _srcp, _pre );
			_srcp += _src_bytes;
			_dstp += _dst_bytes;
		}
		dst_line++;
	}
}

void ConversionOp::BufferConvertorU8_GRAY::to_rgb(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre)
{
	unsigned int i, bytes=3;
	// Converting from GRAY to RGB Only need to worry about precision of dst.
	switch(pre)
	{
	case CinePaintTag::PRECISION_U8_ENUM: // Both the same so just copy. bytes.
		for(i=0; i<bytes; i++)
		{
			*dst = *src; 
			++dst; 
		}
		break;
	case CinePaintTag::PRECISION_U16_ENUM: // Convert 8bit to 16??
		for( i=0; i<bytes; i++)
		{
			*dst=0;	++dst; // Set High byte = 0. [@TODO (DWM) : Proper coversion maths]
			*dst = *src; 
			++dst; 
		}
		break;
	case CinePaintTag::PRECISION_FLOAT16_ENUM:
	case CinePaintTag::PRECISION_FLOAT_ENUM:
	case CinePaintTag::PRECISION_BFP_ENUM:
	default:
		break;
	}
}

void ConversionOp::BufferConvertorU8_GRAY::to_grey(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre)
{
}

void ConversionOp::BufferConvertorU8_GRAY::to_index(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre)
{
}

//===========================================
// U16 RGB Conversion Implementation.
void ConversionOp::BufferConvertorU16_RGB::ConvertMaskRow(AbstractBuf& dst, AbstractBuf& src, int row, int offset, int width, int step)
{

}

void ConversionOp::BufferConvertorU16_RGB::ConvertRow(AbstractBuf& dst, const AbstractBuf& src, int row, int offset, int width, int step)
{
	const unsigned char* src_p; // Pointer into source data.
	unsigned char* dst_p; // Pointer into dst data.
	// Ensure src buffer is actually a U8
	PFNPixelConvertor _pix_conv = get_pixel_convertor(dst.GetTag().GetFormat());
	if (src.GetTag().GetPrecision() == CinePaintTag::PRECISION_U8_ENUM )
	{
		
		// Get pointer to start of required row.
		src_p = src.GetPortionData(0,row);
		dst_p = dst.GetPortionData(0,row);
		// Jump to any offset.
		src_p += offset * src.GetTag().GetBytes();
		dst_p += offset * dst.GetTag().GetBytes();
		// Loop over each pixel and convert.
		for ( int i=0; i<width; i++)
		{
			(this->*_pix_conv)(dst_p, src_p, dst.GetTag().GetPrecision());
			src_p += src.GetTag().GetBytes();
			dst_p += dst.GetTag().GetBytes();
		}
	}
	else
		printf("ERROR : ConversionOp::BufferConvertorU8 Src buffer not U8 format\n");
}

void ConversionOp::BufferConvertorU16_RGB::ConvertMask(AbstractBuf& dst, AbstractBuf& src, int width, int step)
{
}

void ConversionOp::BufferConvertorU16_RGB::Convert(AbstractBuf& dst, AbstractBuf& src, int width, int step)
{
}

void ConversionOp::BufferConvertorU16_RGB::Convert(PixelArea& dst, const PixelArea& src)
{
	// Get AbstractBufs which the PixelAreas refer to.
	const AbstractBuf &_src_buf = src.GetBuffer();
	AbstractBuf &_dst_buf = dst.GetBuffer();

	unsigned int ws = _src_buf.GetWidth();
	unsigned int wd = _dst_buf.GetWidth();
	
	// Get appropriate pixel convertor, based to dst format.
	PFNPixelConvertor _pix_conv = get_pixel_convertor(_dst_buf.GetTag().GetFormat());
	CinePaintTag::Precision _pre  = _dst_buf.GetTag().GetPrecision();

	const unsigned char* _srcp = _src_buf.GetPortionData(0,0);
	unsigned char* _dstp = _dst_buf.GetPortionData(0,0);
	unsigned int _src_bytes = _src_buf.GetTag().GetBytes();
	unsigned int _dst_bytes = _dst_buf.GetTag().GetBytes();

	unsigned int dst_line = dst.GetX();

	// Move src and dst pointers to start point for windows.


	for(int src_line = src.GetY(); src_line < src.GetY()+src.GetHeight()-1; ++src_line)
	{
		_srcp =  _src_buf.GetPortionData(0,0) + (( src_line*ws + src.GetX() ) * _src_bytes);
		_dstp =  _dst_buf.GetPortionData(0,0) + (( dst_line*wd + dst.GetX() ) * _dst_bytes);	
		for (int i = src.GetX(); i < src.GetX()+src.GetWidth()-1; ++i)
		{
			(this->*_pix_conv)(_dstp, _srcp, _pre );
			_srcp += _src_bytes;
			_dstp += _dst_bytes;
		}
		dst_line++;
	}
}

void ConversionOp::BufferConvertorU16_RGB::to_rgb(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre)
{
	// Converting from RGB to RGB Only need to worry about precision of dst.
	unsigned int i, samples = 3;
	switch(pre)
	{
	case CinePaintTag::PRECISION_U8_ENUM: // Scale 8bit to 16bit
		for(i=0; i<samples; i++)
		{
			// Skip high byte. [@TODO (DWM) : Proper coversion maths]
			++src;
			*dst = *src; 
			++dst; 
			++src;
		}
		break;
	case CinePaintTag::PRECISION_U16_ENUM: // Straight copy.
		for(i=0; i<samples*2; i++)
		{
			*dst = *src; 
			++dst; 
			++src;
		}
		break;
	case CinePaintTag::PRECISION_FLOAT16_ENUM:
	case CinePaintTag::PRECISION_FLOAT_ENUM:
	case CinePaintTag::PRECISION_BFP_ENUM:
	default:
		break;
	}
}
void ConversionOp::BufferConvertorU16_RGB::to_grey(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre)
{
}

void ConversionOp::BufferConvertorU16_RGB::to_index(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre)
{
}

//===========================================
// U16_GRAY Conversion Implementation
void ConversionOp::BufferConvertorU16_GRAY::ConvertMaskRow(AbstractBuf& dst, AbstractBuf& src, int row, int offset, int width, int step)
{

}

void ConversionOp::BufferConvertorU16_GRAY::ConvertRow(AbstractBuf& dst, const AbstractBuf& src, int row, int offset, int width, int step)
{
	const unsigned char* src_p; // Pointer into source data.
	unsigned char* dst_p; // Pointer into dst data.
	// Ensure src buffer is actually a U8
	PFNPixelConvertor _pix_conv = get_pixel_convertor(dst.GetTag().GetFormat());
	if (src.GetTag().GetPrecision() == CinePaintTag::PRECISION_U16_ENUM )
	{
		
		// Get pointer to start of required row.
		src_p = src.GetPortionData(0,row);
		dst_p = dst.GetPortionData(0,row);
		// Jump to any offset.
		src_p += offset * src.GetTag().GetBytes();
		dst_p += offset * dst.GetTag().GetBytes();
		// Loop over each pixel and convert.
		for ( int i=0; i<width; i++)
		{
			(this->*_pix_conv)(dst_p, src_p, dst.GetTag().GetPrecision());
			src_p += src.GetTag().GetBytes();
			dst_p += dst.GetTag().GetBytes();
		}
	}
	else
		printf("ERROR : ConversionOp::BufferConvertorU8 Src buffer not U8 format\n");
}

void ConversionOp::BufferConvertorU16_GRAY::ConvertMask(AbstractBuf& dst, AbstractBuf& src, int width, int step)
{
}

void ConversionOp::BufferConvertorU16_GRAY::Convert(AbstractBuf& dst, AbstractBuf& src, int width, int step)
{
}

void ConversionOp::BufferConvertorU16_GRAY::Convert(PixelArea& dst, const PixelArea& src)
{
	// Get AbstractBufs which the PixelAreas refer to.
	const AbstractBuf &_src_buf = src.GetBuffer();
	AbstractBuf &_dst_buf = dst.GetBuffer();

	unsigned int ws = _src_buf.GetWidth();
	unsigned int wd = _dst_buf.GetWidth();
	
	// Get appropriate pixel convertor, based to dst format.
	PFNPixelConvertor _pix_conv = get_pixel_convertor(_dst_buf.GetTag().GetFormat());
	CinePaintTag::Precision _pre  = _dst_buf.GetTag().GetPrecision();

	const unsigned char* _srcp = _src_buf.GetPortionData(0,0);
	unsigned char* _dstp = _dst_buf.GetPortionData(0,0);
	unsigned int _src_bytes = _src_buf.GetTag().GetBytes();
	unsigned int _dst_bytes = _dst_buf.GetTag().GetBytes();

	unsigned int dst_line = dst.GetX();

	// Move src and dst pointers to start point for windows.


	for(int src_line = src.GetY(); src_line < src.GetY()+src.GetHeight()-1; ++src_line)
	{
		_srcp =  _src_buf.GetPortionData(0,0) + (( src_line*ws + src.GetX() ) * _src_bytes);
		_dstp =  _dst_buf.GetPortionData(0,0) + (( dst_line*wd + dst.GetX() ) * _dst_bytes);	
		for (int i = src.GetX(); i < src.GetX()+src.GetWidth()-1; ++i)
		{
			(this->*_pix_conv)(_dstp, _srcp, _pre );
			_srcp += _src_bytes;
			_dstp += _dst_bytes;
		}
		dst_line++;
	}
}

void ConversionOp::BufferConvertorU16_GRAY::to_rgb(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre)
{
	unsigned int i, samples=3;
	// Converting from GRAY to RGB Only need to worry about precision of dst.
	switch(pre)
	{
	case CinePaintTag::PRECISION_U8_ENUM: // Down convert from 16 to 8.
		for(i=0; i<samples; i++)
		{
			*dst = *src;	// [@TODO (DWM) : Proper coversion maths]
			++dst; 
		}
		break;
	case CinePaintTag::PRECISION_U16_ENUM: // Same size - just grey to rgb.
		for( i=0; i<samples; i++)
		{
			*dst= *src;  ++dst; // copy both bytes.
			*dst = *src+1; 
			++dst; 
		}
		break;
	case CinePaintTag::PRECISION_FLOAT16_ENUM:
	case CinePaintTag::PRECISION_FLOAT_ENUM:
	case CinePaintTag::PRECISION_BFP_ENUM:
	default:
		break;
	}
}

void ConversionOp::BufferConvertorU16_GRAY::to_grey(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre)
{
}

void ConversionOp::BufferConvertorU16_GRAY::to_index(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre)
{
}
//===========================================
// Float16 Conversion Implementation.

//===========================================
// Float GRAY Conversion Implementation.
void ConversionOp::BufferConvertorFloat_GRAY::ConvertMaskRow(AbstractBuf& dst, AbstractBuf& src, int row, int offset, int width, int step)
{

}

void ConversionOp::BufferConvertorFloat_GRAY::ConvertRow(AbstractBuf& dst, const AbstractBuf& src, int row, int offset, int width, int step)
{
	const unsigned char* src_p; // Pointer into source data.
	unsigned char* dst_p; // Pointer into dst data.
	// Ensure src buffer is actually a U8
	PFNPixelConvertor _pix_conv = get_pixel_convertor(dst.GetTag().GetFormat());

	float src_max, src_min;
	find_min_max(src, &src_min, &src_max);
	if (src_min >=0 && src_min <= 1.0 && src_max >=0 && src_max <= 1.0)
		m_scale_factor = 255;
	else
		m_scale_factor = 255.0f/fabs(src_max-src_min);
	m_offset = 0;
	if (src_min <0 || src_min > 1)
		m_offset = src_min;

	if (src.GetTag().GetPrecision() == CinePaintTag::PRECISION_FLOAT_ENUM )
	{
		
		// Get pointer to start of required row.
		src_p = src.GetPortionData(0,row);
		dst_p = dst.GetPortionData(0,row);
		// Jump to any offset.
		src_p += offset * src.GetTag().GetBytes();
		dst_p += offset * dst.GetTag().GetBytes();
		// Loop over each pixel and convert.
		for ( int i=0; i<width; i++)
		{
			(this->*_pix_conv)(dst_p, src_p, dst.GetTag().GetPrecision());
			src_p += src.GetTag().GetBytes();
			dst_p += dst.GetTag().GetBytes();
		}
	}
	else
		printf("ERROR : ConversionOp::BufferConvertorFloat_Gray Src buffer not Float format\n");
}

void ConversionOp::BufferConvertorFloat_GRAY::ConvertMask(AbstractBuf& dst, AbstractBuf& src, int width, int step)
{
}

void ConversionOp::BufferConvertorFloat_GRAY::Convert(AbstractBuf& dst, AbstractBuf& src, int width, int step)
{
}

void ConversionOp::BufferConvertorFloat_GRAY::Convert(PixelArea& dst, const PixelArea& src)
{
	// Get AbstractBufs which the PixelAreas refer to.
	const AbstractBuf &_src_buf = src.GetBuffer();
	AbstractBuf &_dst_buf = dst.GetBuffer();

	unsigned int ws = _src_buf.GetWidth();
	unsigned int wd = _dst_buf.GetWidth();
	
	// Get appropriate pixel convertor, based to dst format.
	PFNPixelConvertor _pix_conv = get_pixel_convertor(_dst_buf.GetTag().GetFormat());
	CinePaintTag::Precision _pre  = _dst_buf.GetTag().GetPrecision();

	const unsigned char* _srcp = _src_buf.GetPortionData(0,0);
	unsigned char* _dstp = _dst_buf.GetPortionData(0,0);
	unsigned int _src_bytes = _src_buf.GetTag().GetBytes();
	unsigned int _dst_bytes = _dst_buf.GetTag().GetBytes();

	unsigned int dst_line = dst.GetX();

	// Move src and dst pointers to start point for windows.


	for(int src_line = src.GetY(); src_line < src.GetY()+src.GetHeight()-1; ++src_line)
	{
		_srcp =  _src_buf.GetPortionData(0,0) + (( src_line*ws + src.GetX() ) * _src_bytes);
		_dstp =  _dst_buf.GetPortionData(0,0) + (( dst_line*wd + dst.GetX() ) * _dst_bytes);	
		for (int i = src.GetX(); i < src.GetX()+src.GetWidth()-1; ++i)
		{
			(this->*_pix_conv)(_dstp, _srcp, _pre );
			_srcp += _src_bytes;
			_dstp += _dst_bytes;
		}
		dst_line++;
	}
}

void ConversionOp::BufferConvertorFloat_GRAY::to_rgb(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre)
{
	unsigned int i, samples=3;
	float val;
	// Converting from GRAY to RGB Only need to worry about precision of dst.
	switch(pre)
	{
	case CinePaintTag::PRECISION_U8_ENUM: // Down convert from float to 8bit ints.
		val = *(reinterpret_cast<const float *>(src));
		val = m_offset + (m_scale_factor*val);
		for(i=0; i<samples; i++)
		{
			*dst = static_cast<unsigned int>(val);	// [@TODO (DWM) : Proper coversion maths]
			++dst; 
		}
		break;
	case CinePaintTag::PRECISION_U16_ENUM: // Same size - just grey to rgb.
		for( i=0; i<samples; i++)
		{
			*dst= *src;  ++dst; // copy both bytes.
			*dst = *src+1; 
			++dst; 
		}
		break;
	case CinePaintTag::PRECISION_FLOAT16_ENUM:
	case CinePaintTag::PRECISION_FLOAT_ENUM:
	case CinePaintTag::PRECISION_BFP_ENUM:
	default:
		break;
	}
}

void ConversionOp::BufferConvertorFloat_GRAY::to_grey(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre)
{
}

void ConversionOp::BufferConvertorFloat_GRAY::to_index(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre)
{
}

void ConversionOp::BufferConvertorFloat_GRAY::find_min_max(const AbstractBuf &buf, float *_min, float *_max)
{
	*_min = 0;
	*_max = 0;

	const unsigned char* p = buf.GetPortionData(0,0);
	unsigned int samples = buf.GetWidth() * buf.GetHeight();
	unsigned int bytes = buf.GetTag().GetBytes();

	for (unsigned int i=0; i<samples; i++, p+=bytes)
	{
		if (*(reinterpret_cast<const float *>(p)) < *_min )
			*_min = *(reinterpret_cast<const float *>(p));
		if (*(reinterpret_cast<const float *>(p)) > *_max )
			*_max = *(reinterpret_cast<const float *>(p));
	}

}
//===========================================
// Float RGB Conversion Implementation.
void ConversionOp::BufferConvertorFloat_RGB::ConvertMaskRow(AbstractBuf& dst, AbstractBuf& src, int row, int offset, int width, int step)
{

}

void ConversionOp::BufferConvertorFloat_RGB::ConvertRow(AbstractBuf& dst, const AbstractBuf& src, int row, int offset, int width, int step)
{
	const unsigned char* src_p; // Pointer into source data.
	unsigned char* dst_p; // Pointer into dst data.
	// Ensure src buffer is actually a U8
	PFNPixelConvertor _pix_conv = get_pixel_convertor(dst.GetTag().GetFormat());

	float src_max, src_min;
	find_min_max(src, &src_min, &src_max);
	if (src_min >=0 && src_min <= 1.0 && src_max >=0 && src_max <= 1.0)
		m_scale_factor = 255;
	else
		m_scale_factor = 255.0f/fabs(src_max-src_min);
	m_offset = 0;
	if (src_min <0 || src_min > 1)
		m_offset = src_min;

	if (src.GetTag().GetPrecision() == CinePaintTag::PRECISION_FLOAT_ENUM )
	{
		
		// Get pointer to start of required row.
		src_p = src.GetPortionData(0,row);
		dst_p = dst.GetPortionData(0,row);
		// Jump to any offset.
		src_p += offset * src.GetTag().GetBytes();
		dst_p += offset * dst.GetTag().GetBytes();
		// Loop over each pixel and convert.
		for ( int i=0; i<width; i++)
		{
			(this->*_pix_conv)(dst_p, src_p, dst.GetTag().GetPrecision());
			src_p += src.GetTag().GetBytes();
			dst_p += dst.GetTag().GetBytes();
		}
	}
	else
		printf("ERROR : ConversionOp::BufferConvertorFloat_Gray Src buffer not Float format\n");
}

void ConversionOp::BufferConvertorFloat_RGB::ConvertMask(AbstractBuf& dst, AbstractBuf& src, int width, int step)
{
}

void ConversionOp::BufferConvertorFloat_RGB::Convert(AbstractBuf& dst, AbstractBuf& src, int width, int step)
{
}

void ConversionOp::BufferConvertorFloat_RGB::Convert(PixelArea& dst, const PixelArea& src)
{
	// Get AbstractBufs which the PixelAreas refer to.
	const AbstractBuf &_src_buf = src.GetBuffer();
	AbstractBuf &_dst_buf = dst.GetBuffer();

	unsigned int ws = _src_buf.GetWidth();
	unsigned int wd = _dst_buf.GetWidth();
	
	// Get appropriate pixel convertor, based to dst format.
	PFNPixelConvertor _pix_conv = get_pixel_convertor(_dst_buf.GetTag().GetFormat());
	CinePaintTag::Precision _pre  = _dst_buf.GetTag().GetPrecision();

	const unsigned char* _srcp = _src_buf.GetPortionData(0,0);
	unsigned char* _dstp = _dst_buf.GetPortionData(0,0);
	unsigned int _src_bytes = _src_buf.GetTag().GetBytes();
	unsigned int _dst_bytes = _dst_buf.GetTag().GetBytes();

	unsigned int dst_line = dst.GetX();

	// Move src and dst pointers to start point for windows.


	for(int src_line = src.GetY(); src_line < src.GetY()+src.GetHeight()-1; ++src_line)
	{
		_srcp =  _src_buf.GetPortionData(0,0) + (( src_line*ws + src.GetX() ) * _src_bytes);
		_dstp =  _dst_buf.GetPortionData(0,0) + (( dst_line*wd + dst.GetX() ) * _dst_bytes);	
		for (int i = src.GetX(); i < src.GetX()+src.GetWidth()-1; ++i)
		{
			(this->*_pix_conv)(_dstp, _srcp, _pre );
			_srcp += _src_bytes;
			_dstp += _dst_bytes;
		}
		dst_line++;
	}
}

void ConversionOp::BufferConvertorFloat_RGB::to_rgb(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre)
{
	unsigned int i, samples=3;
	float val;
	// Converting from GRAY to RGB Only need to worry about precision of dst.
	switch(pre)
	{
	case CinePaintTag::PRECISION_U8_ENUM: // Down convert from float to 8bit ints.
		for(i=0; i<samples; i++)
		{
			val = *(reinterpret_cast<const float *>(src));
			val = m_offset + (m_scale_factor*val);

			*dst = static_cast<unsigned int>(val);	// [@TODO (DWM) : Proper coversion maths]
			++dst; 
			src += sizeof(float);
		}
		break;
	case CinePaintTag::PRECISION_U16_ENUM: // Same size - just grey to rgb.
		for( i=0; i<samples; i++)
		{
			*dst= *src;  ++dst; // copy both bytes.
			*dst = *src+1; 
			++dst; 
		}
		break;
	case CinePaintTag::PRECISION_FLOAT16_ENUM:
	case CinePaintTag::PRECISION_FLOAT_ENUM:
	case CinePaintTag::PRECISION_BFP_ENUM:
	default:
		break;
	}
}

void ConversionOp::BufferConvertorFloat_RGB::to_grey(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre)
{
}

void ConversionOp::BufferConvertorFloat_RGB::to_index(unsigned char* dst, const unsigned char* src, CinePaintTag::Precision pre)
{
}

void ConversionOp::BufferConvertorFloat_RGB::find_min_max(const AbstractBuf &buf, float *_min, float *_max)
{
	*_min = 0;
	*_max = 0;

	const unsigned char* p = buf.GetPortionData(0,0);
	unsigned int samples = buf.GetWidth() * buf.GetHeight();
	unsigned int bytes = buf.GetTag().GetBytes();

	for (unsigned int i=0; i<samples; i++, p+=bytes)
	{
		if (*(reinterpret_cast<const float *>(p)) < *_min )
			*_min = *(reinterpret_cast<const float *>(p));
		if (*(reinterpret_cast<const float *>(p)) > *_max )
			*_max = *(reinterpret_cast<const float *>(p));
	}

}

//===========================================
// BFP Conversion Implementation.
