/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ScaleOp - Scale an image buffer
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: RenderOp.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */
 
#ifndef _RENDER_OP_H_
#define _RENDER_OP_H_

#include "dll_api.h"
#include "plug-ins/pdb/CinePaintImageOp.h"
#include "plug-ins/pdb/CinePaintRenderOp.h"

// forward delcaration
#include "plug-ins/pdb/CinePaintTag.h"
class PixelArea;

extern "C" IMAGE_CINEPAINT_OPS_API CinePaintImageOp* CreateRenderOpObject();


/**
 * CinePaintScaleOp implementation.
 * ScaleOp provides the core CinePaintScaleOp as a CinePaintImageOp plugin. This provides the
 * core scale operation within CinePaint.
 *
 */
class IMAGE_CINEPAINT_OPS_API RenderOp : public CinePaintImageOp, public CinePaintRenderOp
{
	public:
		/**
		 * Constructs a new CinePaint RenderOp
		 *
		 */
		RenderOp();

		/**
		 * Destructor
		 */
		~RenderOp() {};


		//================================
		// Overrides from CinePaintRenderOp

		virtual AbstractRenderer* CreateDefaultRendererInstance(CinePaintImage& image);
		virtual AbstractRenderer* CreateScaledScreenRendererInstance(CinePaintImage& image, int width, int height);


		//================================
		// Overrides from CinePaintImageOp
		/**
		 * Returns the Image Op Type of this CinePaintImageOp
		 * If this CinePaintImageOp provides a known operation set, by implementing one of the
		 * defined ImageOp interfaces, this method shopuld return one of the defined Image Op 
		 * strings, otherwise, the image op type may be user defined
		 *
		 * @return the Image Op type of this CinePaintImageOp
		 */
		virtual const char* GetImageOpType() const;

		/**
		 * Returns true if this CinePaintImageOp supports operations on Image data of the specified CinePaintTag
		 * A particular implementation of a CinePaintImageOp may choose to operate on one or more image
		 * data types. There may be multiple CinePaintImageOp for a known Image Op type that handle various
		 * data formats, this method may be used to determine if this CinePaintImageOp handles the specified
		 * format
		 *
		 * @return trus if this CinePaintImageOp supports the spoecified CinePaintTag
		 */
		virtual bool SupportsTag(const CinePaintTag& tag) const;

};

#endif /* _RENDER_OP_H_ */
