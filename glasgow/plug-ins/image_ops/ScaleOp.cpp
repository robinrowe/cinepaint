/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ScaleOp - Scale a buffer
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ScaleOp.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "ScaleOp.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include "plug-ins/pdb/Float16.h"
#include "plug-ins/pdb/PixelArea.h"
#include "plug-ins/pdb/PixelRow.h"
#include "plug-ins/pdb/CinePaintTag.h"
#include "plug-ins/pdb/ImageUtils.h"
#include "utility/Transform.h"
#include <float.h>
#include <limits.h>

const double ScaleOp::EPSILON = 0.0001;

ScaleOp::ScaleOpRowResamplerU8 ScaleOp::m_row_resampler_u8;
ScaleOp::ScaleOpRowResamplerU16 ScaleOp::m_row_resampler_u16;
ScaleOp::ScaleOpRowResamplerFloat ScaleOp::m_row_resampler_float;
ScaleOp::ScaleOpRowResamplerFloat16 ScaleOp::m_row_resampler_float16;
ScaleOp::ScaleOpRowResamplerBFP ScaleOp::m_row_resampler_BFP;

/**
 * This is the main entry point used to create an instance of the ImageOp.
**/
extern "C" IMAGE_CINEPAINT_OPS_API CinePaintImageOp* CreateScaleOpObject()
{
	return(new ScaleOp());
}

ScaleOp::ScaleOp()
: CinePaintImageOp("scale_op", "scale an object", "help", "CinePaint Team", "2004", "2004")
{
}

/**
 * Returns the Image Op Type of this CinePaintImageOp
 * If this CinePaintImageOp provides a known operation set, by implementing one of the
 * defined ImageOp interfaces, this method shopuld return one of the defined Image Op 
 * strings, otherwise, the image op type may be user defined
 *
 * @return the Image Op type of this CinePaintImageOp
 */
const char* ScaleOp::GetImageOpType() const
{
	return(CinePaintImageOp::GetIMAGE_OP_TYPE_SCALE());
}

/**
 * Returns true if this CinePaintImageOp supports operations on Image data of the specified Tag
 * A particular implementation of a CinePaintImageOp may choose to operate on one or more image
 * data types. There may be multiple CinePaintImageOp for a known Image Op type that handle various
 * data formats, this method may be used to determine if this CinePaintImageOp handles the specified
 * format
 *
 * @return trus if this CinePaintImageOp supports the spoecified CinePaintTag
 */
bool ScaleOp::SupportsTag(const CinePaintTag& tag) const
{
	bool _ret = false;

	switch(tag.GetPrecision())
	{
		case CinePaintTag::PRECISION_U8_ENUM:
		case CinePaintTag::PRECISION_U16_ENUM:
		case CinePaintTag::PRECISION_FLOAT_ENUM:
		case CinePaintTag::PRECISION_FLOAT16_ENUM:
		case CinePaintTag::PRECISION_BFP_ENUM:
		{
			_ret = true;
			break;
		}
		case CinePaintTag::PRECISION_NONE_ENUM:
		default:
		{
			_ret = false;
		}
	}

	return(_ret);
}






/**
 * Scale PixelArea src_area into dest_area.
 *
 * @param src_area the source PixelArea to scale
 * @param dest_area the destination PixelArea for scaled image data
 */
void ScaleOp::ScaleArea(PixelArea& src_area, PixelArea& dest_area)
{
	int _src_width = src_area.GetWidth();
	int _src_height = src_area.GetHeight();
	int _dest_width = dest_area.GetWidth();
	int _dest_height = dest_area.GetHeight();
	CinePaintTag& _src_tag = src_area.GetBuffer().GetTag();
	CinePaintTag& _dest_tag = dest_area.GetBuffer().GetTag();


	// find the ratios of old x to new x and old y to new y
	double _x_rat = static_cast<double>(_src_width) / static_cast<double>(_dest_width);
	double _y_rat = static_cast<double>(_src_height) / static_cast<double>(_dest_height);

	// determine the scale type
	ScaleType _scale_type;
	if((_x_rat < 1.0) && (_y_rat < 1.0))
	{
		_scale_type = MagnifyX_MagnifyY;
	}
	else if((_x_rat < 1.0) && (_y_rat >= 1.0))
	{
		_scale_type = MagnifyX_MinifyY;
	}
	else if((_x_rat >= 1.0) && (_y_rat < 1.0))
	{
		_scale_type = MinifyX_MagnifyY;
	}
	else
	{
		_scale_type = MinifyX_MinifyY;
	}

	// allocate an array to help with the calculations
	double* _row_accum = new double[_dest_width * _dest_tag.GetNumChannels()];
	double* _x_frac = new double[_dest_width + _src_width];

	// initialize the pre-calculated pixel fraction array
	int _src_col_num = 0;
	double _x_cum = 0.0;
	double _x_last = 0.0;

	for(int i = 0; i < (_dest_width + _src_width); i++)
	{
		if(_x_cum + _x_rat <= (_src_col_num + 1 + EPSILON))
		{
			_x_cum += _x_rat;
			_x_frac[i] = _x_cum - _x_last;
		}
		else
		{
			_src_col_num ++;
			_x_frac[i] = _src_col_num - _x_last;
		}
		_x_last += _x_frac[i];
	}

	// clear the "row" array
	for(int i = 0; i < (_dest_width * _dest_tag.GetNumChannels()); i++)
	{
        _row_accum[i] = 0;
	}

	// counters zero... 
	int _src_row_num = 0;
	int _dest_row_num = 0;
	double _y_cum = 0.0;
	double _y_last = 0.0;

	// temporary PixelRows used for shuffling data around
	PixelRow _src_row(_src_tag, _src_width);
	PixelRow _src_p1_row(_src_tag, _src_width);
	PixelRow _src_p2_row(_src_tag, _src_width);
	PixelRow _src_m1_row(_src_tag, _src_width);

	// Get the first src row
	src_area.GetPixelRow(_src_row, _src_row_num);

	// Get the next two if possible
	if(_src_row_num < (_src_height - 1))
	{
		src_area.GetPixelRow(_src_p1_row, _src_row_num +1);
	}
	if((_src_row_num + 1) < (_src_height - 1))
	{
		src_area.GetPixelRow(_src_p2_row, _src_row_num +2);
	}



	// Scale the selected region
	ScaleOp::ScaleOpRowResampler* _src_resampler = get_scale_op_row_resampler(_src_tag);
	ScaleOp::ScaleOpRowResampler* _dest_resampler = get_scale_op_row_resampler(_dest_tag);

	int _height_loop = _dest_height;
	while(_height_loop)
	{
		bool _advance_dest_y;
		double _dy;
		double _y_frac;

		// determine the fraction of the src pixel we are using for y
		if((_y_cum + _y_rat) <= (_src_row_num + 1 + EPSILON))
		{
			_y_cum += _y_rat;
			_dy = _y_cum - _src_row_num;
			_y_frac = _y_cum - _y_last;
			_advance_dest_y = true;
		}
		else
		{
			_y_frac = (_src_row_num + 1) - _y_last;
			_dy = 1.0;
			_advance_dest_y = false;
		}

		_y_last += _y_frac;

		// set up generic guchar pointers to correct data
		unsigned char* _src = _src_row.GetData();
		unsigned char* _src_m1 = (_src_row_num > 0) ? _src_m1_row.GetData() : _src_row.GetData();
		unsigned char* _src_p1 = (_src_row_num < (_src_height - 1)) ? _src_p1_row.GetData() : _src_row.GetData();
		unsigned char* _src_p2 = ((_src_row_num + 1) < (_src_height - 1)) ? _src_p2_row.GetData() : _src_p1;

		// scale and accumulate into row_accum array
		// @note [claw] we need to pass in the RC value for cubic interpolation as the last param
		_src_resampler->RowResample(_src, _src_m1, _src_p1, _src_p2, _dy, _x_frac, _y_frac, _x_rat, _row_accum, _dest_tag.GetNumChannels(), _src_width, _dest_width, _scale_type, false); 

		if(_advance_dest_y)
		{
			double tot_frac = 1.0 / (_x_rat * _y_rat);

			// copy row_accum to "dest"
			for(int x= 0; x < _dest_width * _dest_tag.GetNumChannels(); x++)
			{
				_row_accum[x] *= tot_frac; 
			}

			// this sets dest row from array row_accum
			_dest_resampler->SetDestRow(dest_area, _dest_row_num, _dest_width, _row_accum);

			// clear row_accum
			for(int i = 0; i < (_dest_width * _dest_tag.GetNumChannels()); i++)
			{
                _row_accum[i] = 0;
			}

			_dest_row_num++;
			_height_loop--;
		}
		else
		{
			// Shuffle data in the rows

			unsigned char* s = _src_m1_row.GetData();
            _src_m1_row.SetData(_src_row.GetData(), _src_width);
			_src_row.SetData(_src_p1_row.GetData(), _src_width);
			_src_p1_row.SetData(_src_p2_row.GetData(), _src_width);
			_src_p2_row.SetData(s, _src_width);

			_src_row_num++;
			if((_src_row_num + 1) < (_src_height - 1))
			{
				src_area.GetPixelRow(_src_p2_row, _src_row_num +2);
			}
		}
	}


	// free up temporary arrays
	delete[] _row_accum;
	delete[] _x_frac;
}



/**
 * Returns a ScaleOpRowResampler suitable for the specfied CinePaintTag
 *
 * @param tag the Image data type CinePaintTag
 * @return a ScaleOpRowResampler suitable for the specified CinePaintTag
 */
ScaleOp::ScaleOpRowResampler* ScaleOp::get_scale_op_row_resampler(const CinePaintTag& tag)
{
	ScaleOp::ScaleOpRowResampler* _resampler = 0;

	switch(tag.GetPrecision())
	{
		case CinePaintTag::PRECISION_U8_ENUM:
			_resampler = &ScaleOp::m_row_resampler_u8;
			break;
		case CinePaintTag::PRECISION_U16_ENUM:
			_resampler = &ScaleOp::m_row_resampler_u16;
			break;
		case CinePaintTag::PRECISION_FLOAT_ENUM:
			_resampler = &ScaleOp::m_row_resampler_float;
			break;
		case CinePaintTag::PRECISION_FLOAT16_ENUM:
			_resampler = &ScaleOp::m_row_resampler_float16;
			break;
		case CinePaintTag::PRECISION_BFP_ENUM:
			_resampler = &ScaleOp::m_row_resampler_BFP;
			break;
		case CinePaintTag::PRECISION_NONE_ENUM:
		default:
			printf("ScaleOp::get_scale_op_row_resampler: bad precision");
			break;
	}

	return(_resampler);
}










//--------------
// Nested classes

//----------------------
// ScaleOpRowResamplerU8
void ScaleOp::ScaleOpRowResamplerU8::RowResample(unsigned char* src, unsigned char* src_m1, unsigned char* src_p1, unsigned char* src_p2, double dy, double* x_frac, double y_frac, double x_rat, double* row_accum, int num_channels, int orig_width, int width, ScaleOp::ScaleType scale_type, bool cubic_interpolation)
{
	// cast the data to the right type
	unsigned char* _s = reinterpret_cast<unsigned char*>(src);
	unsigned char* _s_m1 = reinterpret_cast<unsigned char*>(src_m1);
	unsigned char* _s_p1 = reinterpret_cast<unsigned char*>(src_p1);
	unsigned char* _s_p2 = reinterpret_cast<unsigned char*>(src_p2);

	int _src_col_num = 0;
	double _x_cum = 0.0;
	double *_r = row_accum;   /* the acumulated array so far */ 
	int _frac = 0;
	int _j = width;
	double _dx = 0.0;
	bool _advance_dest_x = false;

	// loop through the x values in dest
	while(_j)
	{
		if(_x_cum + x_rat <= (_src_col_num + 1 + ScaleOp::EPSILON))
		{
			_x_cum += x_rat;
			_dx = _x_cum - _src_col_num;
			_advance_dest_x = true;
		}
		else
		{
			_dx = 1.0;
			_advance_dest_x = false;
		}

		double _tot_frac = x_frac[_frac++] * y_frac;

		int _minus_x = (_src_col_num > 0) ? -num_channels : 0;
		int _plus_x = (_src_col_num < (orig_width - 1)) ? num_channels : 0;
		int _plus2_x = ((_src_col_num + 1) < (orig_width - 1)) ? num_channels * 2 : _plus_x;

		if(cubic_interpolation)
		{
			switch(scale_type)
			{
				case MagnifyX_MagnifyY:
					for(int b = 0; b < num_channels; b++)
					{
						_r[b] += Transform::cubic(dy, 
							Transform::cubic(_dx, _s_m1[b + _minus_x], _s_m1[b], _s_m1[b + _plus_x], _s_m1[b + _plus2_x], 255.0, 0.0),
							Transform::cubic(_dx, _s[b + _minus_x], _s[b], _s[b + _plus_x], _s[b + _plus2_x], 255.0, 0.0),
							Transform::cubic(_dx, _s_p1[b + _minus_x], _s_p1[b], _s_p1[b + _plus_x], _s_p1[b + _plus2_x], 255.0, 0.0),
							Transform::cubic(_dx, _s_p2[b + _minus_x], _s_p2[b], _s_p2[b + _plus_x], _s_p2[b + _plus2_x], 255.0, 0.0), 
						255.0, 
						0.0) * _tot_frac;
					}
					break;
				case MagnifyX_MinifyY:
					for(int b = 0; b < num_channels; b++)
					{
						_r[b] += Transform::cubic(_dx, _s[b + _minus_x], _s[b], _s[b + _plus_x], _s[b + _plus2_x], 255.0, 0.0) * _tot_frac;
					}
					break;
				case MinifyX_MagnifyY:
					for(int b = 0; b < num_channels; b++)
					{
						_r[b] += Transform::cubic(dy, _s_m1[b], _s[b], _s_p1[b], _s_p2[b], 255.0, 0.0) * _tot_frac;
					}
					break;
				case MinifyX_MinifyY:
					for(int b = 0; b < num_channels; b++)
					{
						_r[b] += _s[b] * _tot_frac;
					}
					break;
			}
		}
		else
		{
			switch(scale_type)
			{
			case MagnifyX_MagnifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += ((1 - dy) * ((1 - _dx) * _s[b] + _dx * _s[b + _plus_x]) + dy * ((1 - _dx) * _s_p1[b] + _dx * _s_p1[b + _plus_x])) * _tot_frac;
				}
				break;
			case MagnifyX_MinifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += (_s[b] * (1 - _dx) + _s[b + _plus_x] * _dx) * _tot_frac;
				}
				break;
			case MinifyX_MagnifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += (_s[b] * (1 - dy) + _s_p1[b] * dy) * _tot_frac;
				}
				break;
			case MinifyX_MinifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += _s[b] * _tot_frac;
				}
				break;
			}
		}

		if(_advance_dest_x)
		{
			_r += num_channels;
			_j--;
		}
		else
		{
			_s_m1 += num_channels;
			_s    += num_channels;
			_s_p1 += num_channels;
			_s_p2 += num_channels;
			_src_col_num++;
		}
	}
}

void ScaleOp::ScaleOpRowResamplerU8::SetDestRow(PixelArea& area, unsigned int row, unsigned int width, double* row_accum)
{
	PixelRow _pixelrow(area.GetBuffer().GetTag(), width);
	unsigned char* _dest = static_cast<unsigned char*>(_pixelrow.GetData());

	for(unsigned int x = 0; x < (width * area.GetBuffer().GetTag().GetNumChannels()); x++)
    {
		_dest[x] = static_cast<unsigned int>(row_accum[x]);
    }

	area.WritePixelRow(_pixelrow, row);
}





// ScaleOpRowResamplerU16
void ScaleOp::ScaleOpRowResamplerU16::RowResample(unsigned char* src, unsigned char* src_m1, unsigned char* src_p1, unsigned char* src_p2, double dy, double* x_frac, double y_frac, double x_rat, double* row_accum, int num_channels, int orig_width, int width, ScaleOp::ScaleType scale_type, bool cubic_interpolation)
{
	// cast the data to the right type
	unsigned short* _s = reinterpret_cast<unsigned short*>(src);
	unsigned short* _s_m1 = reinterpret_cast<unsigned short*>(src_m1);
	unsigned short* _s_p1 = reinterpret_cast<unsigned short*>(src_p1);
	unsigned short* _s_p2 = reinterpret_cast<unsigned short*>(src_p2);

	int _src_col_num = 0;
	double _x_cum = 0.0;
	double *_r = row_accum;   /* the acumulated array so far */ 
	int _frac = 0;
	int _j = width;
	double _dx = 0.0;
	bool _advance_dest_x = false;
  
	// loop through the x values in dest
	while(_j)
    {
		if(_x_cum + x_rat <= (_src_col_num + 1 + ScaleOp::EPSILON))
		{
			_x_cum += x_rat;
			_dx = _x_cum - _src_col_num;
			_advance_dest_x = true;
		}
		else
		{
			_dx = 1.0;
			_advance_dest_x = false;
		}

		double _tot_frac = x_frac[_frac++] * y_frac;

		int _minus_x = (_src_col_num > 0) ? -num_channels : 0;
		int _plus_x = (_src_col_num < (orig_width - 1)) ? num_channels : 0;
		int _plus2_x = ((_src_col_num + 1) < (orig_width - 1)) ? num_channels * 2 : _plus_x;

		if(cubic_interpolation)
		{
			switch(scale_type)
			{
			case MagnifyX_MagnifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += Transform::cubic(dy, 
						Transform::cubic(_dx, _s_m1[b + _minus_x], _s_m1[b], _s_m1[b + _plus_x], _s_m1[b + _plus2_x], USHRT_MAX, 0.0),
						Transform::cubic(_dx, _s[b + _minus_x], _s[b],	_s[b + _plus_x], _s[b + _plus2_x], USHRT_MAX, 0.0),
						Transform::cubic(_dx, _s_p1[b + _minus_x], _s_p1[b], _s_p1[b + _plus_x], _s_p1[b + _plus2_x], USHRT_MAX, 0.0),
						Transform::cubic(_dx, _s_p2[b + _minus_x], _s_p2[b], _s_p2[b + _plus_x], _s_p2[b + _plus2_x], USHRT_MAX, 0.0), 
						USHRT_MAX, 
						0.0) * _tot_frac;
				}
				break;
			case MagnifyX_MinifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += Transform::cubic(_dx, _s[b + _minus_x], _s[b], _s[b + _plus_x], _s[b + _plus2_x], USHRT_MAX, 0.0) * _tot_frac;
				}
				break;
			case MinifyX_MagnifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += Transform::cubic(dy, _s_m1[b], _s[b], _s_p1[b], _s_p2[b], USHRT_MAX, 0.0) * _tot_frac;
				}
				break;
			case MinifyX_MinifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += _s[b] * _tot_frac;
				}
				break;
			}
		}
		else
			switch (scale_type)
		{
			case MagnifyX_MagnifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += ((1 - dy) * ((1 - _dx) * _s[b] + _dx * _s[b + _plus_x]) + dy  * ((1 - _dx) * _s_p1[b] + _dx * _s_p1[b + _plus_x])) * _tot_frac;
				}
				break;
			case MagnifyX_MinifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += (_s[b] * (1 - _dx) + _s[b + _plus_x] * _dx) * _tot_frac;
				}
				break;
			case MinifyX_MagnifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += (_s[b] * (1 - dy) + _s_p1[b] * dy) * _tot_frac;
				}
				break;
			case MinifyX_MinifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += _s[b] * _tot_frac;
				}
				break;
		}

		if(_advance_dest_x)
		{
			_r += num_channels;
			_j--;
		}
		else
		{
			_s_m1 += num_channels;
			_s    += num_channels;
			_s_p1 += num_channels;
			_s_p2 += num_channels;
			_src_col_num++;
		}
	}
}

void ScaleOp::ScaleOpRowResamplerU16::SetDestRow(PixelArea& area, unsigned int row, unsigned int width, double* row_accum)
{
	PixelRow _pixelrow(area.GetBuffer().GetTag(), width);
	unsigned short* _dest = reinterpret_cast<unsigned short*>(_pixelrow.GetData());

	for(unsigned int x = 0; x < (width * area.GetBuffer().GetTag().GetNumChannels()); x++)
	{
		_dest[x] = static_cast<unsigned short>(row_accum[x]);
    }

	area.WritePixelRow(_pixelrow, row);
}





// ScaleOpRowResamplerFloat
void ScaleOp::ScaleOpRowResamplerFloat::RowResample(unsigned char* src, unsigned char* src_m1, unsigned char* src_p1, unsigned char* src_p2, double dy, double* x_frac, double y_frac, double x_rat, double* row_accum, int num_channels, int orig_width, int width, ScaleOp::ScaleType scale_type, bool cubic_interpolation)
{
	// cast the data to the right type
	float* _s = reinterpret_cast<float*>(src);
	float* _s_m1 = reinterpret_cast<float*>(src_m1);
	float* _s_p1 = reinterpret_cast<float*>(src_p1);
	float* _s_p2 = reinterpret_cast<float*>(src_p2);

	int _src_col_num = 0;
	double _x_cum = 0.0;
	double *_r = row_accum;   /* the acumulated array so far */ 
	int _frac = 0;
	int _j = width;
	double _dx = 0.0;
	bool _advance_dest_x = false;

	// loop through the x values in dest
	while(_j)
    {
		if(_x_cum + x_rat <= (_src_col_num + 1 + ScaleOp::EPSILON))
		{
			_x_cum += x_rat;
			_dx = _x_cum - _src_col_num;
			_advance_dest_x = true;
		}
		else
		{
			_dx = 1.0;
			_advance_dest_x = false;
		}

		double _tot_frac = x_frac[_frac++] * y_frac;

		int _minus_x = (_src_col_num > 0) ? -num_channels : 0;
		int _plus_x = (_src_col_num < (orig_width - 1)) ? num_channels : 0;
		int _plus2_x = ((_src_col_num + 1) < (orig_width - 1)) ? num_channels * 2 : _plus_x;

		if(cubic_interpolation)
		{
			switch (scale_type)
			{
			case MagnifyX_MagnifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += Transform::cubic(dy, 
						Transform::cubic(_dx, _s_m1[b + _minus_x], _s_m1[b], _s_m1[b + _plus_x], _s_m1[b + _plus2_x], FLT_MAX, FLT_MIN),
						Transform::cubic(_dx, _s[b + _minus_x], _s[b],	_s[b + _plus_x], _s[b + _plus2_x], FLT_MAX, FLT_MIN),
						Transform::cubic(_dx, _s_p1[b + _minus_x], _s_p1[b], _s_p1[b + _plus_x], _s_p1[b + _plus2_x], FLT_MAX, FLT_MIN),
						Transform::cubic(_dx, _s_p2[b + _minus_x], _s_p2[b], _s_p2[b + _plus_x], _s_p2[b + _plus2_x], FLT_MAX, FLT_MIN), 
					FLT_MAX,
					FLT_MIN) * _tot_frac;
				}
				break;
			case MagnifyX_MinifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += Transform::cubic(_dx, _s[b + _minus_x], _s[b], _s[b + _plus_x], _s[b + _plus2_x], FLT_MAX ,FLT_MIN) * _tot_frac;
				}
				break;
			case MinifyX_MagnifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += Transform::cubic(dy, _s_m1[b], _s[b], _s_p1[b], _s_p2[b], FLT_MAX, FLT_MIN) * _tot_frac;
				}
				break;
			case MinifyX_MinifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += _s[b] * _tot_frac;
				}
				break;
			}
		}
		else
			switch (scale_type)
		{
			case MagnifyX_MagnifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += ((1 - dy) * ((1 - _dx) * _s[b] + _dx * _s[b + _plus_x]) + dy  * ((1 - _dx) * _s_p1[b] + _dx * _s_p1[b + _plus_x])) * _tot_frac;
				}
				break;
			case MagnifyX_MinifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += (_s[b] * (1 - _dx) + _s[b + _plus_x] * _dx) * _tot_frac;
				}
				break;
			case MinifyX_MagnifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += (_s[b] * (1 - dy) + _s_p1[b] * dy) * _tot_frac;
				}
				break;
			case MinifyX_MinifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += _s[b] * _tot_frac;
				}
				break;
		}

		if(_advance_dest_x)
		{
			_r += num_channels;
			_j--;
		}
		else
		{
			_s_m1 += num_channels;
			_s    += num_channels;
			_s_p1 += num_channels;
			_s_p2 += num_channels;
			_src_col_num++;
		}
	}
}

void ScaleOp::ScaleOpRowResamplerFloat::SetDestRow(PixelArea& area, unsigned int row, unsigned int width, double* row_accum)
{
	PixelRow _pixelrow(area.GetBuffer().GetTag(), width);
	float* _dest = reinterpret_cast<float*>(_pixelrow.GetData());

	for(unsigned int x = 0; x < (width * area.GetBuffer().GetTag().GetNumChannels()); x++)
	{
		_dest[x] = static_cast<float>(row_accum[x]);
    }

	area.WritePixelRow(_pixelrow, row);
}





// ScaleOpRowResamplerFloat16
void ScaleOp::ScaleOpRowResamplerFloat16::RowResample(unsigned char* src, unsigned char* src_m1, unsigned char* src_p1, unsigned char* src_p2, double dy, double* x_frac, double y_frac, double x_rat, double* row_accum, int num_channels, int orig_width, int width, ScaleOp::ScaleType scale_type, bool cubic_interpolation)
{
	// cast the data to the right type
	unsigned short* _s = reinterpret_cast<unsigned short*>(src);
	unsigned short* _s_m1 = reinterpret_cast<unsigned short*>(src_m1);
	unsigned short* _s_p1 = reinterpret_cast<unsigned short*>(src_p1);
	unsigned short* _s_p2 = reinterpret_cast<unsigned short*>(src_p2);

	int _src_col_num = 0;
	double _x_cum = 0.0;
	double *_r = row_accum;   /* the acumulated array so far */ 
	int _frac = 0;
	int _j = width;
	double _dx = 0.0;
	bool _advance_dest_x = false;

	// loop through the x values in dest
 
	while(_j)
    {
		if(_x_cum + x_rat <= (_src_col_num + 1 + ScaleOp::EPSILON))
		{
			_x_cum += x_rat;
			_dx = _x_cum - _src_col_num;
			_advance_dest_x = true;
		}
		else
		{
			_dx = 1.0;
			_advance_dest_x = false;
		}

		double _tot_frac = x_frac[_frac++] * y_frac;

		int _minus_x = (_src_col_num > 0) ? -num_channels : 0;
		int _plus_x = (_src_col_num < (orig_width - 1)) ? num_channels : 0;
		int _plus2_x = ((_src_col_num + 1) < (orig_width - 1)) ? num_channels * 2 : _plus_x;

		if(cubic_interpolation)
		{
			switch(scale_type)
			{
			case MagnifyX_MagnifyY:
				for(int b = 0; b < num_channels; b++)
				{
					double _cubic1 = Transform::cubic(_dx, Float16::ToFloat(_s_m1[b + _minus_x]), Float16::ToFloat(_s_m1[b]),
						Float16::ToFloat(_s_m1[b + _plus_x]), Float16::ToFloat(_s_m1[b + _plus2_x]), FLT_MAX, FLT_MIN);
					double _cubic2 = Transform::cubic(_dx, Float16::ToFloat(_s[b + _minus_x]), Float16::ToFloat(_s[b]), 
						Float16::ToFloat(_s[b + _plus_x]), Float16::ToFloat(_s[b + _plus2_x]), FLT_MAX, FLT_MIN);
					double _cubic3 = Transform::cubic(_dx, Float16::ToFloat(_s_p1[b + _minus_x]), Float16::ToFloat(_s_p1[b]), 
						Float16::ToFloat(_s_p1[b + _plus_x]), Float16::ToFloat(_s_p1[b + _plus2_x]), FLT_MAX, FLT_MIN);
					double _cubic4 = Transform::cubic(_dx, Float16::ToFloat(_s_p2[b + _minus_x]), Float16::ToFloat(_s_p2[b]), 
						Float16::ToFloat(_s_p2[b + _plus_x]), Float16::ToFloat(_s_p2[b + _plus2_x]), FLT_MAX, FLT_MIN); 
					_r[b] += Transform::cubic(dy, _cubic1, _cubic2, _cubic3, _cubic4, FLT_MAX, FLT_MIN) * _tot_frac;
				}
				break;
			case MagnifyX_MinifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += Transform::cubic(_dx, Float16::ToFloat(_s[b + _minus_x]), Float16::ToFloat(_s[b]), 
						Float16::ToFloat(_s[b + _plus_x]), Float16::ToFloat(_s[b + _plus2_x]), FLT_MAX , FLT_MIN) * _tot_frac;
				}
				break;
			case MinifyX_MagnifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += Transform::cubic(dy, Float16::ToFloat(_s_m1[b]), Float16::ToFloat(_s[b]), 
						Float16::ToFloat(_s_p1[b]), Float16::ToFloat(_s_p2[b]), FLT_MAX, FLT_MIN) * _tot_frac;
				}
				break;
			case MinifyX_MinifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += Float16::ToFloat(_s[b]) * _tot_frac;
				}
				break;
			}
		}
		else
		{
			switch(scale_type)
			{
			case MagnifyX_MagnifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += ((1 - dy) * ((1 - _dx) * Float16::ToFloat(_s[b]) + _dx * Float16::ToFloat(_s[b + _plus_x])) + 
						dy  * ((1 - _dx) * Float16::ToFloat(_s_p1[b]) + _dx * Float16::ToFloat(_s_p1[b + _plus_x]))) * _tot_frac;
				}
				break;
			case MagnifyX_MinifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += (Float16::ToFloat(_s[b]) * (1 - _dx) + Float16::ToFloat(_s[b + _plus_x]) * _dx) * _tot_frac;
				}
				break;
			case MinifyX_MagnifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += (Float16::ToFloat(_s[b]) * (1 - dy) + Float16::ToFloat(_s_p1[b]) * dy) * _tot_frac;
				}
				break;
			case MinifyX_MinifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += Float16::ToFloat(_s[b]) * _tot_frac;
				}
				break;
			}
		}

		if(_advance_dest_x)
		{
			_r += num_channels;
			_j--;
		}
		else
		{
			_s_m1 += num_channels;
			_s    += num_channels;
			_s_p1 += num_channels;
			_s_p2 += num_channels;
			_src_col_num++;
		}
	}
}

void ScaleOp::ScaleOpRowResamplerFloat16::SetDestRow(PixelArea& area, unsigned int row, unsigned int width, double* row_accum)
{
	PixelRow _pixelrow(area.GetBuffer().GetTag(), width);
	unsigned short* _dest = reinterpret_cast<unsigned short*>(_pixelrow.GetData());

	for(unsigned int x = 0; x < (width * area.GetBuffer().GetTag().GetNumChannels()); x++)
    {
		_dest[x] = Float16::ToFloat16(static_cast<float>(row_accum[x]));
    }

	area.WritePixelRow(_pixelrow, row);
}

// ScaleOpRowResamplerBFP
void ScaleOp::ScaleOpRowResamplerBFP::RowResample(unsigned char* src, unsigned char* src_m1, unsigned char* src_p1, unsigned char* src_p2, double dy, double* x_frac, double y_frac, double x_rat, double* row_accum, int num_channels, int orig_width, int width, ScaleOp::ScaleType scale_type, bool cubic_interpolation)
{
	// cast the data to the right type
	unsigned short* _s = reinterpret_cast<unsigned short*>(src);
	unsigned short* _s_m1 = reinterpret_cast<unsigned short*>(src_m1);
	unsigned short* _s_p1 = reinterpret_cast<unsigned short*>(src_p1);
	unsigned short* _s_p2 = reinterpret_cast<unsigned short*>(src_p2);

	int _src_col_num = 0;
	double _x_cum = 0.0;
	double *_r = row_accum;   /* the acumulated array so far */ 
	int _frac = 0;
	int _j = width;
	double _dx = 0.0;
	bool _advance_dest_x = false;

	// loop through the x values in dest
	while(_j)
    {
		if(_x_cum + x_rat <= (_src_col_num + 1 + ScaleOp::EPSILON))
		{
			_x_cum += x_rat;
			_dx = _x_cum - _src_col_num;
			_advance_dest_x = true;
		}
		else
		{
			_dx = 1.0;
			_advance_dest_x = false;
		}

		double _tot_frac = x_frac[_frac++] * y_frac;

		int _minus_x = (_src_col_num > 0) ? -num_channels : 0;
		int _plus_x = (_src_col_num < (orig_width - 1)) ? num_channels : 0;
		int _plus2_x = ((_src_col_num + 1) < (orig_width - 1)) ? num_channels * 2 : _plus_x;

		if(cubic_interpolation)
		{
			switch (scale_type)
			{
			case MagnifyX_MagnifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += Transform::cubic(dy, 
						Transform::cubic(_dx, _s_m1[b + _minus_x], _s_m1[b], _s_m1[b + _plus_x], _s_m1[b + _plus2_x], USHRT_MAX, 0.0),
						Transform::cubic(_dx, _s[b + _minus_x], _s[b],	_s[b + _plus_x], _s[b + _plus2_x], USHRT_MAX, 0.0),
						Transform::cubic(_dx, _s_p1[b + _minus_x], _s_p1[b], _s_p1[b + _plus_x], _s_p1[b + _plus2_x], USHRT_MAX, 0.0),
						Transform::cubic(_dx, _s_p2[b + _minus_x], _s_p2[b], _s_p2[b + _plus_x], _s_p2[b + _plus2_x], USHRT_MAX, 0.0), 
						USHRT_MAX, 0.0) * _tot_frac;
				}
				break;
			case MagnifyX_MinifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += Transform::cubic(_dx, _s[b + _minus_x], _s[b], _s[b + _plus_x], _s[b + _plus2_x], USHRT_MAX, 0.0) * _tot_frac;
				}
				break;
			case MinifyX_MagnifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += Transform::cubic(dy, _s_m1[b], _s[b], _s_p1[b], _s_p2[b], USHRT_MAX, 0.0) * _tot_frac;
				}
				break;
			case MinifyX_MinifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += _s[b] * _tot_frac;
				}
				break;
			}
		}
		else
		{
			switch (scale_type)
			{
			case MagnifyX_MagnifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += ((1 - dy) * ((1 - _dx) * _s[b] + _dx * _s[b + _plus_x]) + dy  * ((1 - _dx) * _s_p1[b] + _dx * _s_p1[b + _plus_x])) * _tot_frac;
				}
				break;
			case MagnifyX_MinifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += (_s[b] * (1 - _dx) + _s[b + _plus_x] * _dx) * _tot_frac;
				}
				break;
			case MinifyX_MagnifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += (_s[b] * (1 - dy) + _s_p1[b] * dy) * _tot_frac;
				}
				break;
			case MinifyX_MinifyY:
				for(int b = 0; b < num_channels; b++)
				{
					_r[b] += _s[b] * _tot_frac;
				}
				break;
			}
		}

		if(_advance_dest_x)
		{
			_r += num_channels;
			_j--;
		}
		else
		{
			_s_m1 += num_channels;
			_s    += num_channels;
			_s_p1 += num_channels;
			_s_p2 += num_channels;
			_src_col_num++;
		}
	}
}

void ScaleOp::ScaleOpRowResamplerBFP::SetDestRow(PixelArea& area, unsigned int row, unsigned int width, double* row_accum)
{
	PixelRow _pixelrow(area.GetBuffer().GetTag(), width);
	unsigned short* _dest = reinterpret_cast<unsigned short*>(_pixelrow.GetData());

	for(unsigned int x = 0; x < (width * area.GetBuffer().GetTag().GetNumChannels()); x++)
    {
		_dest[x] = static_cast<unsigned short>(row_accum[x]);
    }

	area.WritePixelRow(_pixelrow, row);
}
