/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ScaledScreenRenderer - renderer for scaled screen instances
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ScaledScreenRenderer.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _CINEPAINT_SCALED_SCREEN_RENDERER_H_
#define _CINEPAINT_SCALED_SCREEN_RENDERER_H_

#include "dll_api.h"
#include "plug-ins/pdb/AbstractRenderer.h"
#include "utility/CPRect.h"

class AbstractBuf;
class CinePaintImage;
class Drawable;
class CinePaintConversionOp;

/**
 * ScaledScreenRenderer is a render suitable for generating a thumbnail representation of an image
 *
 * @note this still needs some thought ...
 */
class IMAGE_CINEPAINT_OPS_API ScaledScreenRenderer : public AbstractRenderer
{
	public:
		ScaledScreenRenderer(CinePaintImage& image, int width, int height);
		virtual ~ScaledScreenRenderer();

		virtual void Render();
		virtual void Render(const CPRect& render_bounds);

		/**
		 * Returns the AbsrtactBuf representing the rendered version of the item this AbsractRenderer is rendering
		 * This method does not check the valid state before returning the rendered buffer, the caller should therefore
		 * check that the buffer is valid with a call to IsValid() and call Render() as required
		 *
		 * @return the rendering buffer
		 */
		virtual const AbstractBuf& GetRender();

		/**
		 * Get the glass layer for the associated CinePaintImage.
		 (
		 * @return the AbstractBuf containing the GlassLayer required for this image.
		 */
		virtual const AbstractBuf& GetGlassLayer();


		/**
		 * Returns whether this CinePaintRenderer contains a valid render.
		 * If the item being rendered has not changed since the last time the item was rendered,
		 * true is returned, otherwise false is returned.
		 *
		 * @return true if this renderor contains a valid render, false otherwise
		 */
		virtual bool IsValid() const;

		/**
		 * Returns whether the specified region of this render is valid.
		 * If the item being rendered has not modified the specified region since its was
		 * last rendered this method returns true, otherwise false is returned.
		 *
		 * @param bounds the region to check as being valid
		 * @return true if the specified region is valid, false otherwise
		 */
		virtual bool IsValid(const CPRect& bounds) const;

		/**
		 * Sets the current render invalid, or out of date.
		 * An out of date render should be re-rendered before drawn on screen
		 *
		 */
		virtual void SetInvalid();

		/**
		 * Sets the specified area invalid, or out of date, or so required a re-render
		 * A renderor implementation may be able to optimize its re-rendering based upon
		 * the area the is invalid
		 *
		 * @param invalid_bounds the region to set invalid
		 */
		virtual void SetInvalid(const CPRect& invalid_bounds);

		/**
		 * Returns the width of this Renderer.
		 * The width is usually the width of the Drawable being rendered
		 *
		 * @return the width of this Renderer
		 */
		virtual int GetWidth() const;

		/**
		 * Returns the height of this Renderer.
		 * The height is usually the height of the Drawable being rendered
		 *
		 * @return the height of thie Renderer
		 */
		virtual int GetHeight() const;

	protected:

		/**
		 * Clears the current invalid region
		 *
		 */
		void ClearInvalidRegion();

		/**
		 * Sets the valid state of this AbstractRenderer
		 *
		 * @param region the region to invalidate
		 */
		void ClearInvalidRegion(const CPRect& region);

	private:

		/**
		 * Convenience method to return (and possibly construct) the current render buffer
		 * If no render buffer has been allocated, or the current buffer size does not
		 * match the size of the current image, this method constructs a new render
		 * bufferer, deleting any previous buffer. The current render buffer is then
		 * returned. If construction fails, 0 is returned
		 *
		 * @return the current render buffer, or 0 if the buffer cannot be constructed
		 */
		AbstractBuf* get_render_buffer();

		/**
		 * Allocates the Rendered Image data buffer.
		 * The Rendered Image Buffer stores the final rendered composition of the CinePaintImage
		 * this renderer is rendering with all relevant Layers combined etc.
		 * The buffer is constructed to match the size of the specified image.
		 * If construction fails, 0 is returned.
		 *
		 * @todo need to look at the format options for the render buffer
		 * @param image the CinePaintImage to render
		 * @return constructed AbstractBuf to render the image into
		 */
		AbstractBuf* create_render_buffer(int width, int height);

		AbstractBuf* get_glass_buffer();

		/**
		 * Renders all the currently visible layers within the current image into the current render buffer
		 * The layers are combind using the standard composite image op
		 *
		 * @param render_bounds area to be rendered
		 */
		void render_visible_layers(CinePaintImage& image, AbstractBuf& render_buf, const CPRect& render_bounds);
		void render_buffer(AbstractBuf& render_buffer, AbstractBuf& render, const CPRect& render_bounds);


		/** the rendering area */
		AbstractBuf* m_render_buffer;

		/** The Glass Layer area */
		AbstractBuf* m_glass_buffer;

		/** the CinePaintImage we are rendering */
		CinePaintImage& m_image;

		/** the current invalid region of the object being rendered */
		CPRect m_invalid_region;

		int m_width;
		int m_height;
		CinePaintConversionOp* m_convertor;

}
; /* class ScaledScreenRenderer */

#endif /* _CINEPAINT_SCALED_SCREEN_RENDERER_H_ */
