/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CompositeOp - Various composition operations
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CompositeOp.cpp,v 1.2 2006/12/18 08:21:22 robinrowe Exp $
 */
 

#include "CompositeOp.h"

#include "plug-ins/pdb/Pixel.h"
#include "plug-ins/pdb/PixelArea.h"
#include "plug-ins/pdb/Color.h"

#define NOMINMAX

/**
 * This is the main entry point used to create an instance of the ImageOp.
**/
extern "C" IMAGE_CINEPAINT_OPS_API CinePaintImageOp* CreateCompositeOpObject()
{
	return(new CompositeOp());
}

CompositeOp::CompositeOp()
: CinePaintImageOp("composite_op", "composite operations", "help", "Donald MacVicar, Colin Law, Stuart Ford", "2004", "2004")
{
}

/**
 * Returns the Image Op Type of this CinePaintImageOp
 * If this CinePaintImageOp provides a known operation set, by implementing one of the
 * defined ImageOp interfaces, this method shopuld return one of the defined Image Op 
 * strings, otherwise, the image op type may be user defined
 *
 * @return the Image Op type of this CinePaintImageOp
 */
const char* CompositeOp::GetImageOpType() const
{
	return(CinePaintImageOp::GetIMAGE_OP_TYPE_COMPOSITE());
}

/**
 * Returns true if this CinePaintImageOp supports operations on Image data of the specified Tag
 * A particular implementation of a CinePaintImageOp may choose to operate on one or more image
 * data types. There may be multiple CinePaintImageOp for a known Image Op type that handle various
 * data formats, this method may be used to determine if this CinePaintImageOp handles the specified
 * format
 *
 * @return trus if this CinePaintImageOp supports the spoecified CinePaintTag
 */
bool CompositeOp::SupportsTag(const CinePaintTag& tag) const
{
	bool _ret = false;

	switch(tag.GetPrecision())
	{
		case CinePaintTag::PRECISION_U8_ENUM:
		{
			_ret = true;
			break;
		}
		case CinePaintTag::PRECISION_NONE_ENUM:
		case CinePaintTag::PRECISION_U16_ENUM:
		case CinePaintTag::PRECISION_FLOAT_ENUM:
		case CinePaintTag::PRECISION_FLOAT16_ENUM:
		case CinePaintTag::PRECISION_BFP_ENUM:
		default:
		{
			_ret = false;
		}
	}

	return(_ret);
}

bool CompositeOp::CompositeLayers(const std::list<Layer*>& layer_list, AbstractBuf& render_buffer, const CPRect& merge_bounds)
{
	return false;
}


bool CompositeOp::CompositeOverlay(PixelArea &dst, const PixelArea &src)
{
	if ( src.GetBuffer().GetTag().GetFormat() == CinePaintTag::FORMAT_GRAY_ENUM )
	{
		unsigned char* _dst = dst.GetBuffer().GetPortionData(0,0);
		const unsigned char* _src = src.GetBuffer().GetPortionData(0,0);

		unsigned int _dst_bytes = dst.GetBuffer().GetTag().GetBytes();
		unsigned int _src_bytes = src.GetBuffer().GetTag().GetBytes();
		unsigned int _src_w = src.GetBuffer().GetWidth();
		unsigned int _dst_w = dst.GetBuffer().GetWidth();
		unsigned int _src_x_offset = src.GetX();

		unsigned int _ymax = ( src.GetY()+src.GetHeight()> dst.GetBuffer().GetHeight()) ? dst.GetBuffer().GetHeight() : src.GetY()+src.GetHeight();
		unsigned int _xmax = ( src.GetX()+src.GetWidth() > dst.GetBuffer().GetWidth()) ? dst.GetBuffer().GetWidth(): src.GetX()+src.GetWidth();

		const unsigned char* _srcp = _src + (((src.GetY() * _src_w) + src.GetX()) * _src_bytes);
		unsigned char* _dstp = _dst + (((dst.GetY() * _dst_w) + dst.GetX()) * _dst_bytes);
		for (unsigned int y=src.GetY(); y < _ymax; y++)
		{
			for (unsigned int x=src.GetX(); x < _xmax; x++)
			{
				if ( *_srcp == 255 ) // White.
				{
					for(unsigned int c=0; c<_dst_bytes; c++)
					{
						*_dstp = *_srcp; 
						++_dstp;
					}
				}
				else
					_dstp += _dst_bytes;
				_srcp += _src_bytes;
			}
			_srcp = _src + (((y * _src_w) + src.GetX()) * _src_bytes);
			_dstp = _dst + (((y * _dst_w) + dst.GetX()) * _dst_bytes);

		}


	}
	else
	{
		printf(" Error: CompositeOp::CompositeOverlay buffer types do not match\n");
		return false;
	}
	return true;    
}

