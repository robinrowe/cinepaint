/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      FillOp - Fills a buffer
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: FillOp.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */
 

#include "FillOp.h"
#include "plug-ins/pdb/Pixel.h"
#include "plug-ins/pdb/PixelArea.h"
#include "plug-ins/pdb/Color.h"

/**
 * This is the main entry point used to create an instance of the ImageOp.
**/
extern "C" IMAGE_CINEPAINT_OPS_API CinePaintImageOp* CreateFillOpObject()
{
	return(new FillOp());
}

FillOp::FillOp()
: CinePaintImageOp("fill_op", "fills an area", "help", "CinePaint Team", "2004", "2004")
{
}

/**
 * Returns the Image Op Type of this CinePaintImageOp
 * If this CinePaintImageOp provides a known operation set, by implementing one of the
 * defined ImageOp interfaces, this method shopuld return one of the defined Image Op 
 * strings, otherwise, the image op type may be user defined
 *
 * @return the Image Op type of this CinePaintImageOp
 */
const char* FillOp::GetImageOpType() const
{
	return(CinePaintImageOp::GetIMAGE_OP_TYPE_FILL());
}

/**
 * Returns true if this CinePaintImageOp supports operations on Image data of the specified Tag
 * A particular implementation of a CinePaintImageOp may choose to operate on one or more image
 * data types. There may be multiple CinePaintImageOp for a known Image Op type that handle various
 * data formats, this method may be used to determine if this CinePaintImageOp handles the specified
 * format
 *
 * @return trus if this CinePaintImageOp supports the spoecified CinePaintTag
 */
bool FillOp::SupportsTag(const CinePaintTag& tag) const
{
	bool _ret = false;

	switch(tag.GetPrecision())
	{
		case CinePaintTag::PRECISION_U8_ENUM:
		case CinePaintTag::PRECISION_U16_ENUM:
		case CinePaintTag::PRECISION_FLOAT_ENUM:
		case CinePaintTag::PRECISION_FLOAT16_ENUM:
		case CinePaintTag::PRECISION_BFP_ENUM:
		{
			_ret = true;
			break;
		}
		case CinePaintTag::PRECISION_NONE_ENUM:
		default:
		{
			_ret = false;
		}
	}

	return(_ret);
}

void FillOp::FillArea(PixelArea& fill_area, const Color& fill_color)
{
	Pixel _color_pixel(fill_area.GetBuffer().GetTag(), fill_color);

	for(int _y = 0; _y < fill_area.GetHeight(); _y++)
	{
		for(int _x = 0; _x < fill_area.GetWidth(); _x++)
		{
			fill_area.WritePixel(_color_pixel, _x, _y);
		}
	}
}

