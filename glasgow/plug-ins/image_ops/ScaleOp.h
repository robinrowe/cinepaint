/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ScaleOp - Scale an image buffer
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ScaleOp.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */
 
#ifndef _SCALE_OP_H_
#define _SCALE_OP_H_

#include "dll_api.h"
#include "plug-ins/pdb/CinePaintImageOp.h"
#include "plug-ins/pdb/CinePaintScaleOp.h"

// forward delcaration
#include "plug-ins/pdb/CinePaintTag.h"
class PixelArea;

extern "C" IMAGE_CINEPAINT_OPS_API CinePaintImageOp* CreateScaleOpObject();


/**
 * CinePaintScaleOp implementation.
 * ScaleOp provides the core CinePaintScaleOp as a CinePaintImageOp plugin. This provides the
 * core scale operation within CinePaint.
 *
 */
class IMAGE_CINEPAINT_OPS_API ScaleOp : public CinePaintImageOp, public CinePaintScaleOp
{
	public:
		/**
		 * Constructs a new CinePaint ScaleOp
		 *
		 */
		ScaleOp();

		/**
		 * Destructor
		 */
		~ScaleOp() {};

		/** Defined scale types */
		enum ScaleType
		{
			MinifyX_MinifyY,
			MinifyX_MagnifyY,
			MagnifyX_MinifyY,
			MagnifyX_MagnifyY
		};


		// CinePaintImageOp

		/**
		 * Returns the Image Op Type of this CinePaintImageOp
		 * If this CinePaintImageOp provides a known operation set, by implementing one of the
		 * defined ImageOp interfaces, this method shopuld return one of the defined Image Op 
		 * strings, otherwise, the image op type may be user defined
		 *
		 * @return the Image Op type of this CinePaintImageOp
		 */
		virtual const char* GetImageOpType() const;

		/**
		 * Returns true if this CinePaintImageOp supports operations on Image data of the specified Tag
		 * A particular implementation of a CinePaintImageOp may choose to operate on one or more image
		 * data types. There may be multiple CinePaintImageOp for a known Image Op type that handle various
		 * data formats, this method may be used to determine if this CinePaintImageOp handles the specified
		 * format
		 *
		 * @return trus if this CinePaintImageOp supports the spoecified Tag
		 */
		virtual bool SupportsTag(const CinePaintTag& tag) const;




		// CinePaintScaleOp

		/**
		 * Scale PixelArea src_area into dest_area.
		 *
		 * @param src_area the source PixelArea to scale
		 * @param dest_area the destination PixelArea for scaled image data
		 */
		virtual void ScaleArea(PixelArea& src_area, PixelArea& dest_area);


	private:

		static const double EPSILON;



		/**
		 * Nested worker class to perform scaling work for the various data formats
		 * If a new image format is required, this interface should be implemented
		 * to handle the new format
		 *
		 */
		class ScaleOpRowResampler
		{
			public:
				virtual ~ScaleOpRowResampler() {};

				virtual void RowResample(unsigned char* src, unsigned char* src_m1, unsigned char* src_p1, unsigned char* src_p2, double dy, double* x_frac, double y_frac, double x_rat, double* row_accum, int num_channels, int orig_width, int width, ScaleOp::ScaleType scale_type, bool cubic_interpolation) = 0;
				virtual void SetDestRow(PixelArea& area, unsigned int row, unsigned int width, double* row_accum) = 0;

			protected:
				/**
				 * Disallow instances
				 */
				ScaleOpRowResampler() {};

			private:

		}; /* class ScaleOpRowResampler */


		/**
		 * Returns a ScaleOpRowResampler suitable for the specfied CinePaintTag
		 *
		 * @param tag the Image data type CinePaintTag
		 * @return a ScaleOpRowResampler suitable for the specified Tag
		 */
		static ScaleOpRowResampler* get_scale_op_row_resampler(const CinePaintTag& tag);


		//----------------------------------------------------------
		// Nested classes for the various data formats we knwo about

		class ScaleOpRowResamplerU8 : public ScaleOpRowResampler
		{
			public:
				virtual void RowResample(unsigned char* src, unsigned char* src_m1, unsigned char* src_p1, unsigned char* src_p2, double dy, double* x_frac, double y_frac, double x_rat, double* row_accum, int num_channels, int orig_width, int width, ScaleOp::ScaleType scale_type, bool cubic_interpolation);
				virtual void SetDestRow(PixelArea& area, unsigned int row, unsigned int width, double* row_accum);
			protected:
			private:

		}; /* class ScaleOpRowResamplerU8 */

		class ScaleOpRowResamplerU16 : public ScaleOpRowResampler
		{
			public:
				virtual void RowResample(unsigned char* src, unsigned char* src_m1, unsigned char* src_p1, unsigned char* src_p2, double dy, double* x_frac, double y_frac, double x_rat, double* row_accum, int num_channels, int orig_width, int width, ScaleOp::ScaleType scale_type, bool cubic_interpolation);
				virtual void SetDestRow(PixelArea& area, unsigned int row, unsigned int width, double* row_accum);
			protected:
			private:

		}; /* class ScaleOpRowResamplerU16 */

		class ScaleOpRowResamplerFloat : public ScaleOpRowResampler
		{
			public:
				virtual void RowResample(unsigned char* src, unsigned char* src_m1, unsigned char* src_p1, unsigned char* src_p2, double dy, double* x_frac, double y_frac, double x_rat, double* row_accum, int num_channels, int orig_width, int width, ScaleOp::ScaleType scale_type, bool cubic_interpolation);
				virtual void SetDestRow(PixelArea& area, unsigned int row, unsigned int width, double* row_accum);
			protected:
			private:

		}; /* class ScaleOpRowResamplerFloat */

		class ScaleOpRowResamplerFloat16 : public ScaleOpRowResampler
		{
			public:
				virtual void RowResample(unsigned char* src, unsigned char* src_m1, unsigned char* src_p1, unsigned char* src_p2, double dy, double* x_frac, double y_frac, double x_rat, double* row_accum, int num_channels, int orig_width, int width, ScaleOp::ScaleType scale_type, bool cubic_interpolation);
				virtual void SetDestRow(PixelArea& area, unsigned int row, unsigned int width, double* row_accum);
			protected:
			private:

		}; /* class ScaleOpRowResamplerFloat16 */

		class ScaleOpRowResamplerBFP : public ScaleOpRowResampler
		{
			public:
				virtual void RowResample(unsigned char* src, unsigned char* src_m1, unsigned char* src_p1, unsigned char* src_p2, double dy, double* x_frac, double y_frac, double x_rat, double* row_accum, int num_channels, int orig_width, int width, ScaleOp::ScaleType scale_type, bool cubic_interpolation);
				virtual void SetDestRow(PixelArea& area, unsigned int row, unsigned int width, double* row_accum);
			protected:
			private:

		}; /* class ScaleOpRowResamplerBFP */



		// Static instances for the various supported image formats
		static ScaleOpRowResamplerU8 m_row_resampler_u8;
		static ScaleOpRowResamplerU16 m_row_resampler_u16;
		static ScaleOpRowResamplerFloat m_row_resampler_float;
		static ScaleOpRowResamplerFloat16 m_row_resampler_float16;
		static ScaleOpRowResamplerBFP m_row_resampler_BFP;

};

#endif /* _SCALE_OP_H_ */
