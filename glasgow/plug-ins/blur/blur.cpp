
/*
 *
 *   Simple Blur filter for The CinePaint -- a sequence manipulation program
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Modified from the GIMP original blur plugin 20/10/2004 Donald MacVicar
 *
 * $Id:
 */

/*
 * Macros...
 */

#define MIN(a,b)		(((a) < (b)) ? (a) : (b))
#define MAX(a,b)		(((a) > (b)) ? (a) : (b))
#define ROUND(x) 		(int)((x)+.5)

/*
 * Constants...
 */

#define PLUG_IN_NAME		"plug_in_blur"
#define PLUG_IN_VERSION		"April 1999"

#include "dll_api.h"
#include "blur.h"
#include "plug-ins/pdb/PDB.h"
#include "plug-ins/pdb/CPPluginUIProgress.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/PixelRow.h"
#include "plug-ins/pdb/Drawable.h"
#include "plug-ins/pdb/Float16.h"
#include <Half/half.h>

typedef unsigned char u8;
typedef unsigned short u16;
typedef float f32;
typedef half f16;


template <class DataType> 
class GGTrait {
};

template <> 
class GGTrait<u8> {
  public:
    typedef int ExtendedType;;
    template <class T2>
      static unsigned char clamp(T2 x) {
        if(x < 0) return 0;
	if(x > 255) return 255;
	return x;
      }
};

template <> 
class GGTrait<u16> {
  public:
    typedef int ExtendedType;
    template <class T2>
      static u16 clamp(T2 x) {
        if(x < 0) return 0;
	const unsigned short umx = 65535;
	if(x > 65535) 
	  return umx;
	return (u16)x;
      }
};

template <> 
class GGTrait<f16> {
  public:
    typedef float ExtendedType;
    template <class T2>
      static f16 clamp(T2 x) {
        if(x > HALF_MAX) return HALF_MAX;
	if(x < -HALF_MAX) return -HALF_MAX;
	return x;
      }
};

template <> 
class GGTrait<f32> {
  public:
    typedef float ExtendedType;
    template <class T2>
      static float clamp(T2 x) {
	return x;
      }
};

template <class DType>
class TypedPixelRow : public PixelRow {
   protected:
     int nBytes;
   public:
     TypedPixelRow(const CinePaintTag& tag, unsigned int width) : PixelRow(tag, width) {
        nBytes = m_tag.GetBytes();
      }
      
     DType *operator[](int i) { return (DType *)(m_data + i*nBytes); }
};

/*
 * filter() - Blur a row. 
 */

template <class DataType>
void BlurPlugin::filter   (int    width,
	    int    num_channels,
	    int    has_alpha,
            DataType* dest,
            DataType* prev,
            DataType* current,
            DataType* next)
{
  typedef typename GGTrait<DataType>::ExtendedType HighPrecision;
  int count;
  HighPrecision sum;
  HighPrecision s0, s1, s2;
  DataType* p;
  DataType* c;
  DataType* n;
  DataType* d;
  int i;

  std::cerr << "New blur";

  i = num_channels;
  while(i--)
  {

    p = prev + i; 
    c = current + i;
    n = next + i;
    d = dest + i;

    s0 = (HighPrecision)*p + (HighPrecision)*c + (HighPrecision)*n;   /* first partial sum */
    *d = *c;             /*copy first pixel*/
	
    p += num_channels;   /* go to second pixel */
    c += num_channels;
    n += num_channels;
    d += num_channels;
   
    s1 = (HighPrecision)*p + (HighPrecision)*c + (HighPrecision)*n;  /* second partial sum */

    p += num_channels;   /*these go to third pixel */
    c += num_channels;
    n += num_channels;

    count = width - 2;
    while (count--)
    {
      s2 = (HighPrecision)*p + (HighPrecision)*c + (HighPrecision)*n; 
      sum = s0 + s1 + s2;
      s0 = s1;
      s1 = s2;

      *d = GGTrait<DataType>::clamp(.111111*sum); 

      p+=num_channels;
      c+=num_channels;
      n+=num_channels;
      d+=num_channels;
    }
  } 

  /* 
     Set the last pixel on the row. 
  */

  c = current;
  c += (width-1)*num_channels;

  i = num_channels;
  while(i--)
   *d++ = *c++;    
}

/*
 * Exports for CinePaint API.
 */
extern "C" CINEPAINT_BLUR_API CinePaintPlugin* CreateBlurPluginObject()
{
 return new BlurPlugin();
}

// List of classes that this DLL/Plugin provides, MUST MUST be null terminated.
CinePaintPlugin::PFNCreateCinePaintPlugin PluginClassList[] = {CreateBlurPluginObject, 0};

// Exported function to get class constructors list.
extern "C" CINEPAINT_BLUR_API CinePaintPlugin::PFNCreateCinePaintPlugin *GetPluginClassList()
{
	return PluginClassList;
};

static CinePaintPluginInfo info(CreateBlurPluginObject, PLUG_IN_NAME, "<Image>/Filters/Blur/Blur",
								"RGB*, GRAY*, U16_RGB*, U16_GRAY*, FLOAT_RGB*, FLOAT_GRAY*, FLOAT16_RGB*, FLOAT16_GRAY*" );
					

static const CinePaintPlugin::CPPluginParamDef INPUT_PARAM_DEFS[] = { {PDB_DRAWABLE, "drawable", "drawable to blur", true}};

/*************
 * Constructor & Destructor 
 */
BlurPlugin::BlurPlugin()
:CinePaintPlugin(PLUG_IN_NAME,
                 "Blur the image",
                 "This plug-in performs a blur convolution filter on an image.",
                 "Calvin Williamson",
                 "",
                 PLUG_IN_VERSION )

{
}

BlurPlugin::~BlurPlugin() 
{
}

/**
 * The query(World* world) method is called by the core at startup 
 * the plugin calls the appropriate Register() method of the Plugin Database
 * this adds this plugin to the list allowing the rest of the application
 * access to it.
**/
void BlurPlugin::query(World* world)
{//	PDB* pdb=world->pdb;
	PDB* pdb=PDB::GetPDB(world);
	if(!pdb)
	{	return;
	}	
	pdb->Register(&info);
	printf("BlurPlugin::query\n");
}

/**
 * Called by the framework to execute the functionality encapsulated by this plugin.
**/
void BlurPlugin::run(CPParamList& _inParams, CPParamList& _return)
{
	printf("BlurPlugin::Run()\n");

	// Retrieve the parameters from the Parameter list.
	// The plugin base class maintians an Arg List and a Return list,
	// The core is responsible for placeing arguments onto the Arg list before calling run.
	// The return value of GetParam( ) should be checked to ensure that a valid parameter was
	// returned.
	int _res;
	Drawable* _drawable;
	_res = _inParams.GetParam("drawable", &_drawable);
	
	if(_res)
	{
		blur(_drawable->GetBuffer());
	}
	else
	{
		printf("ERROR: Blur::run() No Image parameter given\n");
	}
}



// Plugin parameter definitions

CinePaintPlugin::CPPluginParamDefs& BlurPlugin::GetInParamDefs(CPPluginParamDefs& param_defs) const
{
	param_defs.m_params = INPUT_PARAM_DEFS;
	param_defs.m_param_count = sizeof(INPUT_PARAM_DEFS) / sizeof(CPPluginParamDef);
	return(param_defs);
}
CinePaintPlugin::CPPluginParamDefs& BlurPlugin::GetReturnParamDefs(CPPluginParamDefs& param_defs) const
{
	param_defs.m_params = 0;
	param_defs.m_param_count = 0;
	return(param_defs);
}

/*
 * 'blur()' - Blur an image using a convolution filter.
 */
 
template <class Type>
void blur(AbstractBuf &srcBuf, AbstractBuf &dstBuf)
{
	/*
	* Let the user know what we're doing...
	*/
	CPPluginUIProgress *_prog;
	

	const CinePaintTag& tag = srcBuf.GetTag();
	int num_channels = tag.GetNumChannels();
 
	int sel_width, sel_height, sel_x1, sel_y1, sel_x2, sel_y2;

	// gimp_drawable_mask_bounds(drawable->id, &sel_x1, &sel_y1, &sel_x2, &sel_y2);
        sel_x1 = 0;
	sel_x2 = srcBuf.GetWidth();
	sel_y1 = 0;
	sel_y2 = srcBuf.GetHeight();

	sel_width     = sel_x2 - sel_x1;
	sel_height    = sel_y2 - sel_y1;

	/* bail if too small */
	if (sel_width < 3 || sel_height < 3)
		return;
		
	_prog = new CPPluginUIProgress("blur", "Blurring...", sel_height, 1);

	TypedPixelRow<Type> *cr, *pr, *nr;
 
 
	cr  = new TypedPixelRow<Type> (tag, sel_width);
	pr  = new TypedPixelRow<Type> (tag, sel_width);
	nr  = new TypedPixelRow<Type> (tag, sel_width);
 
        TypedPixelRow<Type> dr(tag, sel_width);

	srcBuf.GetPixelRow(*pr, sel_y1, sel_x1, sel_width);
	srcBuf.GetPixelRow(*cr, sel_y1, sel_x1, sel_width);
	
	typedef typename GGTrait<Type>::ExtendedType HighPrec;
#undef min	
	for(int i = 0; i < sel_height; ++i) {
	   srcBuf.GetPixelRow(*nr, sel_y1+std::min(i+1,  sel_height), sel_x1, sel_width);
	   HighPrec sp, sc,sn;
	   //Loop over the channels
	   for(int k = 0; k < num_channels; ++k) {
	     sp = sc = (*pr)[0][k]+(*cr)[0][k]+(*nr)[0][k];
	     for(int j = 0; j < sel_width; ++j) {
	       int jp = std::min(j+1,sel_width);
	       sn = (*pr)[jp][k]+(*cr)[jp][k]+(*nr)[jp][k];
	       
	       HighPrec sum = sp+sc+sn;
	       
	       dr[j][k] = GGTrait<Type>::clamp(.111111*sum);
	       
	       sp = sc;
	       sc = sn;
	     }
		_prog->UpdateProgress();
	   }
	   dstBuf.WriteRow(dr, sel_y1+i, sel_x1, sel_width);
	   // Rotate the buffered rows
	   TypedPixelRow<Type> *t = pr;
	   pr = cr;
	   cr = nr;
	   nr = t;
	}
	
	delete nr;
	delete cr;
	delete pr;
	
	_prog->Hide();
	
	delete _prog;
}

void BlurPlugin::blur(AbstractBuf &buf)
{
	unsigned char        *current,       /* data for current row */
		*previous,      /* data for prev row */ 
		*next, 		 /* data for next row */ 
		*dest,          /* data for dest row */
		*tmp;           /* tmp pointer for data */
	int y; 								 /* scanline to process */
	int bytes;							 /* Byte width of the image */

	const CinePaintTag& tag = buf.GetTag();
	int num_channels = tag.GetNumChannels(); 
	int bpp = tag.GetBytes();

	int sel_width, sel_height;  //, sel_x1, sel_y1, sel_x2, sel_y2;
	int has_alpha = tag.HasAlpha();

	// TODO : Fix Precision Type ...
	//Precision precision; // = tag_precision(tag);
	//precision = tag_precision(tag);


	// gimp_drawable_mask_bounds(drawable->id, &sel_x1, &sel_y1, &sel_x2, &sel_y2);

	//  sel_width     = sel_x2 - sel_x1;
	sel_width = buf.GetWidth();
	//  sel_height    = sel_y2 - sel_y1;
	sel_height = buf.GetHeight();

	/* bail if too small */
	if (sel_width < 3 || sel_height < 3)
		return;

	/*
	* Let the user know what we're doing...
	*/
	CPPluginUIProgress *_prog;

	_prog = new CPPluginUIProgress("blur", "Blurring...", buf.GetHeight(), 1);

	/*
	* Setup for filter...
	*/
	bytes = sel_width * bpp;

	/* DWM
	* Get the data from the AbstractBuf 
	*/
	unsigned char* src_data = buf.GetPortionData(0,0);

	std::auto_ptr<AbstractBuf> dst_image = AbstractBufFactory::CreateBuf(buf.GetTag(), sel_width, sel_height, AbstractBufFactory::STORAGE_FLAT);
	/* Take control of the memory allocated for the buffer */
	AbstractBuf *dst_buf = dst_image.release();
	dst_buf->RefPortionReadWrite(0,0);
	unsigned char* dst_data = dst_buf->GetPortionData(0,0);

	/* do the first row*/ 
		// Copy first row from src to dst.
	unsigned char* dstp = dst_data;
	unsigned char* srcp = src_data;

	switch(tag.GetPrecision())
	{
	case CinePaintTag::PRECISION_U8_ENUM:
		::blur<u8>(buf,*dst_buf);
		break;
	case CinePaintTag::PRECISION_U16_ENUM:
		::blur<u16>(buf,*dst_buf);
		break;
	case CinePaintTag::PRECISION_FLOAT_ENUM:
		::blur<f32>(buf,*dst_buf);
		break;
	case CinePaintTag::PRECISION_FLOAT16_ENUM: 
		::blur<f16>(buf,*dst_buf);
		break;
	case CinePaintTag::PRECISION_BFP_ENUM:
	case CinePaintTag::PRECISION_NONE_ENUM:
		return;
	}

	/* OK, we're done.  Free all memory used...	*/
	//	Copy data back to input buf.

	srcp = dst_data;
	dstp = src_data;
	for(int i=0; i<sel_width*sel_height*bpp; i++)
	{
		*dstp = *srcp;
		dstp++; srcp++;
	}

}

///*
// * 'run()' - Run the filter...
// */
//
//static void
//run(char   *name,		/* I - Name of filter program. */
//    int    nparams,		/* I - Number of parameters passed in */
//    GParam *param,		/* I - Parameter values */
//    int    *nreturn_vals,	/* O - Number of return values */
//    GParam **return_vals)	/* O - Return values */
//{
//  GRunModeType	run_mode;	/* Current run mode */
//  GStatusType	status;		/* Return status */
//  GParam	*values;	/* Return values */
//
//
// /*
//  * Initialize parameter data...
//  */
//
//  status   = STATUS_SUCCESS;
//  run_mode = param[0].data.d_int32;
//
//  values = g_new(GParam, 1);
//
//  values[0].type          = PARAM_STATUS;
//  values[0].data.d_status = status;
//
//  *nreturn_vals = 1;
//  *return_vals  = values;
//
// /*
//  * Get drawable information...
//  */
//
//  drawable = gimp_drawable_get(param[2].data.d_drawable);
//
// /*
//  * See how we will run
//  */
//
//  switch (run_mode)
//  {
//    case RUN_INTERACTIVE :
//    case RUN_NONINTERACTIVE :
//    case RUN_WITH_LAST_VALS :
//	break;
//    default :
//        status = STATUS_CALLING_ERROR;
//        break;
//  }
//
// /*
//  * Blur the image...
//  */
//
//  if (status == STATUS_SUCCESS)
//  {
//    if ((gimp_drawable_color(drawable->id) ||
//	 gimp_drawable_gray(drawable->id)))
//    {
//     /*
//      * Set the tile cache size...
//      */
//
//      gimp_tile_cache_ntiles(2 * (drawable->width + gimp_tile_width() - 1) /
//                             gimp_tile_width() + 1);
//
//     /*
//      * Run!
//      */
//
//      blur(drawable);
//
//     /*
//      * If run mode is interactive, flush displays...
//      */
//
//      if (run_mode != RUN_NONINTERACTIVE)
//        gimp_displays_flush();
//
//    }
//    else
//      status = STATUS_EXECUTION_ERROR;
//  }
//
// /*
//  * Reset the current run status...
//  */
//
//  values[0].data.d_status = status;
//
// /*
//  * Detach from the drawable...
//  */
//
//  gimp_drawable_detach(drawable);
//}
//
//
///*
// * 'blur()' - Blur an image using a convolution filter.
// */
//
//static void
//blur(TileDrawable * drawable)
//{
//  GPixelRgn	src_rgn,	/* Source image region */
//		dst_rgn;	/* Destination image region */
//  guchar        *current,       /* data for current row */
//		*previous,      /* data for prev row */ 
//		*next, 		/* data for next row */ 
//		*dest,          /* data for dest row */
//		*tmp;           /* tmp pointer for data */
//  gint  y; 			/* scanline to process */
//  gint	bytes;		        /* Byte width of the image */
//
//  gint num_channels = gimp_drawable_num_channels (drawable->id);
//  gint bpp = gimp_drawable_bpp (drawable->id);
//  gint sel_width, sel_height, sel_x1, sel_y1, sel_x2, sel_y2;
//  gint has_alpha = gimp_drawable_has_alpha(drawable->id);
//  GPrecisionType precision = gimp_drawable_precision (drawable->id);
//
//  gimp_drawable_mask_bounds(drawable->id, &sel_x1, &sel_y1, &sel_x2, &sel_y2);
//
//  sel_width     = sel_x2 - sel_x1;
//  sel_height    = sel_y2 - sel_y1;
//
//  /* bail if too small */
//  if (sel_width < 3 || sel_height < 3)
//	return;
//
// /*
//  * Let the user know what we're doing...
//  */
//  gimp_progress_init("Blurring..");
//
// /*
//  * Setup for filter...
//  */
//
//  gimp_pixel_rgn_init(&src_rgn, drawable, sel_x1, sel_y1, sel_width, sel_height,
//                      FALSE, FALSE);
//  gimp_pixel_rgn_init(&dst_rgn, drawable, sel_x1, sel_y1, sel_width, sel_height,
//                      TRUE, TRUE);
//
//  bytes = sel_width * bpp;
//
//  /* set up previous, current, next, dst_row */
//
//  previous = g_malloc(bytes * sizeof(guchar));
//  current = g_malloc(bytes * sizeof(guchar));
//  next = g_malloc(bytes * sizeof(guchar));
//  dest = g_malloc(bytes * sizeof(guchar));
//
//  /* do the first row*/ 
//  gimp_pixel_rgn_get_row(&src_rgn, previous, sel_x1, sel_y1, sel_width);
//  gimp_pixel_rgn_set_row(&dst_rgn, previous, sel_x1, sel_y1, sel_width);
//
// /*
//  * Load the current row for the filter...
//  */
//  
//  gimp_pixel_rgn_get_row(&src_rgn, current, sel_x1, sel_y1+1, sel_width);
//
//
//  for (y = sel_y1+1; y <= sel_y2-2; y ++)
//  {
//
//   /*
//    * Load next row.
//    */
//     gimp_pixel_rgn_get_row(&src_rgn, next, sel_x1, y + 1, sel_width);
//  
//   /*
//    * Blur
//    */
//
//    switch(precision)
//    {
//	case PRECISION_U8:
//          filter_u8(sel_width, num_channels, has_alpha, dest, previous, current, next);
//	  break;
//	case PRECISION_U16:
//          filter_u16(sel_width, num_channels, has_alpha, dest, previous, current, next);
//	  break;
//	case PRECISION_FLOAT:
//          filter_float(sel_width, num_channels, has_alpha, dest, previous, current, next);
//	  break;
//	case PRECISION_FLOAT16: 
//          filter_float16(sel_width, num_channels, has_alpha, dest, previous, current, next);
//	  break;
//	case PRECISION_NONE:
//	return;
//    }
//
//   
//   /*
//    * Save the dest row. 
//    */
//
//    gimp_pixel_rgn_set_row(&dst_rgn, dest, sel_x1, y, sel_width);
//
//   
//   /* 
//    * Shuffle the rows
//   */ 
//    tmp = previous; 
//    previous = current; 
//    current = next;
//    next = tmp;
//
//    if ((y & 15) == 0)
//      gimp_progress_update((double)(y - sel_y1) / (double)sel_height);
//  }
//
//  /* and the last one */
//  gimp_pixel_rgn_set_row(&dst_rgn, current, sel_x1, sel_y2-1, sel_width);
//
// /*
//  * OK, we're done.  Free all memory used...
//  */
//
//  g_free(dest);
//  g_free(previous);
//  g_free(current);
//  g_free(next);
//
// /*
//  * Update the screen...
//  */
//
//  gimp_drawable_flush(drawable);
//  gimp_drawable_merge_shadow(drawable->id, TRUE);
//  gimp_drawable_update(drawable->id, sel_x1, sel_y1, sel_width, sel_height);
//}
