/*
 *
 *   Simple Blur filter for The CinePaint -- a sequence manipulation program
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Modified from the GIMP original blur plugin 20/10/2004 Donald MacVicar
 *
 * $id
 */
#ifndef _BLUR_PLUGIN_H_
#define _BLUR_PLUGIN_H_

#include "plug-ins/pdb/CinePaintPlugin.h"

class AbstractBuf;
// __declspec( dllexport )
class  BlurPlugin : public CinePaintPlugin
{

public:
	BlurPlugin();
	~BlurPlugin();

	void query(World* world);
	void run(CPParamList& _inParams, CPParamList& _return);

	virtual CPPluginParamDefs& GetInParamDefs(CPPluginParamDefs& param_defs) const;
	virtual CPPluginParamDefs& GetReturnParamDefs(CPPluginParamDefs& param_defs) const;

private:
	int load_image (char* filename);
//	void blur(TileDrawable *drawable);
	template <class Type>
	  void filter(int, int, int, Type*, Type*, Type*, Type*);
	void blur(AbstractBuf &img);

};


#endif // _BLUR_PLUGIN_H_
