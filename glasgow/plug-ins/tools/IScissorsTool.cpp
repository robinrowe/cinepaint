/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      IScissorsTool - Core tools module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: IScissorsTool.cpp,v 1.2 2006/01/05 03:08:14 robinrowe Exp $
 */

#include "IScissorsTool.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include "plug-ins/pdb/ImageUtils.h"
#include "plug-ins/pdb/CPPluginUIBool.h"
#include "plug-ins/pdb/CPPluginUIDialog.h"
#include "plug-ins/pdb/CPPluginUINumber.h"
#include "plug-ins/pdb/CPPluginUIWidget.h"

/*  GIMP icon image format -- S. Kimball, P. Mattis  */
/*  Image name: magnify  */


#define ISCISSORS_WIDTH 22
#define ISCISSORS_HEIGHT 22
static char* ISCISSORS_BITS[] =
{
  "......................",
  ".eaae.....aaa.........",
  ".aaaa...eee.ea........",
  ".aaaa...a....a........",
  ".eaae..ea....aha......",
  "..a....e..ha..hae.....",
  "..a....a..hae.hae.....",
  "..a....a..hae.hae.....",
  "..ee...a...hahaee.....",
  "...a..ee...hahae......",
  "...a..a.....haae......",
  "...a..a.....haae......",
  "...eeea.....haha......",
  ".....a.....haeha......",
  "...........hae.ha.....",
  "..........haee..haa...",
  ".........haae..haeha..",
  "........haeha..haehae.",
  "........haehae..haaee.",
  ".........haaee....ee..",
  "...........ee.........",
  "......................"
};

const char* IScissorsTool::TOOL_TIP = "Select shapes from image";

AbstractBuf* IScissorsTool::ISCISSORS_ICON = 0;
unsigned int IScissorsTool::TOOL_COUNT = 0;

// user interface definitions.
static CPPluginUIDialog _dlg("iscissors_options", "Intelligent Sccissors Options", 100, 100);
static CPPluginUIBool _anti_alias("anti_alias", "Anit Alias", false);
static CPPluginUIBool _feather("feather", "Feather", false);
static CPPluginUINumber _feather_radius("feathe_radius", "Feather Radius", 0, 100, 0.1, 50, CPPluginUINumber::SLIDER);
static CPPluginUINumber _curve_resolution("curve_resolution", "Curve Resolution", 0, 200, 0.1, 50, CPPluginUINumber::SLIDER);
static CPPluginUINumber _eddge_detect_thresh("threshold","Edge Detect Threshold", 0, 255, 0.1, 50, CPPluginUINumber::SLIDER);
static CPPluginUINumber _elasticity("elasticity", "Elasticity", 0, 1, 0.1, 0.5, CPPluginUINumber::SLIDER);
static int num_widgets = 7;
static CPPluginUIWidget *ui_widgets[] = {&_dlg, &_anti_alias, &_feather, &_feather_radius, &_curve_resolution, &_eddge_detect_thresh, &_elasticity};

/**
 * This is the main entry point used to create an instance of the ImageOp.
**/
extern "C" CINEPAINT_TOOLS_API CinePaintTool* CreateIScissorsToolObject()
{
	return(new IScissorsTool());
}

/**
 * Constructs a new IScissorsTool instance
 *
 */
IScissorsTool::IScissorsTool()
: CinePaintTool("Intelligent Scissors", "Select Tools", "Select shapes from image", "help", "CinePaint Team", "copyright", "date")
{
	IScissorsTool::TOOL_COUNT++;
}

/**
 * Destructor
 */
IScissorsTool::~IScissorsTool()
{
	if(--IScissorsTool::TOOL_COUNT == 0)
	{
		delete IScissorsTool::ISCISSORS_ICON;
		IScissorsTool::ISCISSORS_ICON = 0;
	}
}

/**
 * Returns the type of this CinePaintTool
 *
 * @return the type of this CinePaintTool
 */
const char* IScissorsTool::GetToolType() const
{
	return(CinePaintTool::TOOL_TYPE_ISCISSORS);
}

/**
 * Returns an Icon representation of this CinePaintTool
 * The icon representation is suitable for displlay upon a tool button or menu.
 *
 * @return an Icon representation of this CinePaintTool
 */
const AbstractBuf& IScissorsTool::GetIcon() const
{
	if(!ISCISSORS_ICON)
	{
		CinePaintTag _tag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false);
		std::auto_ptr<AbstractBuf> _buf = AbstractBufFactory::CreateBuf(_tag, ISCISSORS_WIDTH, ISCISSORS_HEIGHT, AbstractBufFactory::STORAGE_FLAT);
        ISCISSORS_ICON = _buf.release();
		ISCISSORS_ICON->RefPortionReadWrite(0,0);

		ImageUtils::GimpIconToAbstractBuf(*ISCISSORS_ICON, ISCISSORS_BITS, ISCISSORS_WIDTH, ISCISSORS_HEIGHT);
	}

	return(*ISCISSORS_ICON);
}

/**
 * Returns a tool tip for this CinePaintTool
 *
 * @return tooltip for this CinePaintTool
 */
const char* IScissorsTool::GetToolTip() const
{
	return(IScissorsTool::TOOL_TIP);
}

/**
 * Returns the UI definition for this CinePaintTool
 *
 * @param ui populated with the definition of this tools ui panel
 * @return ui paramater populated with thie tools ui definition
 */
CPPluginUIDef& IScissorsTool::GetUIDefs(CPPluginUIDef& ui)
{
	ui.num = num_widgets;
	ui.widgets = ui_widgets;
	return(ui);
}


//--------------------------------------
// CinePaintMouseListener implementation

/**
 * Invoked when the mouse pointer moves into an active area
 *
 */
void IScissorsTool::MouseEntered()
{}

/**
 * Invoked when the mouse pointer leaves an active area
 *
 */
void IScissorsTool::MouseExited()
{}

/**
 * Invoked when a mouse button is pressed.
 *
 * @param button the button that was pressed
 * @param count the number of button clicks
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void IScissorsTool::MousePressed(int button, int count, int x, int y, unsigned int modifier)
{}

/**
 * Invoked when a mouse button is released
 *
 * @param button the mouse button released
 */
void IScissorsTool::MouseReleased(int button, int x, int y)
{}

/**
 * Invoked when the mouse pointer is dragged with a mouse button held pressed
 *
 * @param button the held mouse button
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void IScissorsTool::MouseDragged(int button, int x, int y)
{}

/**
 * Invoked when the mouse pointer is moved within an active area
 *
 * @param x the current mouse position
 * @param y the current mouse position
 */
void IScissorsTool::MouseMoved(int x, int y)
{}
