/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CropTool - Core tools module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CropTool.cpp,v 1.2 2006/01/05 03:08:14 robinrowe Exp $
 */

#include "CropTool.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include "plug-ins/pdb/ImageUtils.h"

/*  GIMP icon image format -- S. Kimball, P. Mattis  */
/*  Image name: magnify  */


#define CROP_WIDTH 22
#define CROP_HEIGHT 22
static char* CROP_BITS[] =
{
  "....a.........a.......",
  "....a.........a.......",
  "....a.........a.......",
  "aaaaaaaaaaaaaaaaaaaaaa",
  "....aaa.....aaa.......",
  "....aaa.....aaa.......",
  "....a.........a.......",
  "....a.........a.......",
  "....a.........a.......",
  "....a.........a.......",
  "....a.........a.......",
  "....a.........a.......",
  "....a.........a.......",
  "....aaa.....aaa.......",
  "....aaa.....aaa.......",
  "aaaaaaaaaaaaaaaaaaaaaa",
  "....a.........a.......",
  "....a.........a.......",
  "....a.........a.......",
  "....a.........a.......",
  "....a.........a.......",
  "....a.........a......."
};

const char* CropTool::TOOL_TIP = "Crop the image";

AbstractBuf* CropTool::CROP_ICON = 0;
unsigned int CropTool::TOOL_COUNT = 0;


/**
 * This is the main entry point used to create an instance of the ImageOp.
**/
extern "C" CINEPAINT_TOOLS_API CinePaintTool* CreateCropToolObject()
{
	return(new CropTool());
}

/**
 * Constructs a new CropTool instance
 *
 */
CropTool::CropTool()
: CinePaintTool("Crop", "Transform Tools", "Crop the image", "help", "CinePaint Team", "copyright", "date")
{
	CropTool::TOOL_COUNT++;
}

/**
 * Destructor
 */
CropTool::~CropTool()
{
	if(--CropTool::TOOL_COUNT == 0)
	{
		delete CropTool::CROP_ICON;
		CropTool::CROP_ICON = 0;
	}
}

/**
 * Returns the type of this CinePaintTool
 *
 * @return the type of this CinePaintTool
 */
const char* CropTool::GetToolType() const
{
	return(CinePaintTool::TOOL_TYPE_CROP);
}

/**
 * Returns an Icon representation of this CinePaintTool
 * The icon representation is suitable for displlay upon a tool button or menu.
 *
 * @return an Icon representation of this CinePaintTool
 */
const AbstractBuf& CropTool::GetIcon() const
{
	if(!CROP_ICON)
	{
		CinePaintTag _tag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false);
		std::auto_ptr<AbstractBuf> _buf = AbstractBufFactory::CreateBuf(_tag, CROP_WIDTH, CROP_HEIGHT, AbstractBufFactory::STORAGE_FLAT);
        CROP_ICON = _buf.release();
		CROP_ICON->RefPortionReadWrite(0,0);

		ImageUtils::GimpIconToAbstractBuf(*CROP_ICON, CROP_BITS, CROP_WIDTH, CROP_HEIGHT);
	}

	return(*CROP_ICON);
}

/**
 * Returns a tool tip for this CinePaintTool
 *
 * @return tooltip for this CinePaintTool
 */
const char* CropTool::GetToolTip() const
{
	return(CropTool::TOOL_TIP);
}




//--------------------------------------
// CinePaintMouseListener implementation

/**
 * Invoked when the mouse pointer moves into an active area
 *
 */
void CropTool::MouseEntered()
{}

/**
 * Invoked when the mouse pointer leaves an active area
 *
 */
void CropTool::MouseExited()
{}

/**
 * Invoked when a mouse button is pressed.
 *
 * @param button the button that was pressed
 * @param count the number of button clicks
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void CropTool::MousePressed(int button, int count, int x, int y, unsigned int modifier)
{}

/**
 * Invoked when a mouse button is released
 *
 * @param button the mouse button released
 */
void CropTool::MouseReleased(int button, int x, int y)
{}

/**
 * Invoked when the mouse pointer is dragged with a mouse button held pressed
 *
 * @param button the held mouse button
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void CropTool::MouseDragged(int button, int x, int y)
{}

/**
 * Invoked when the mouse pointer is moved within an active area
 *
 * @param x the current mouse position
 * @param y the current mouse position
 */
void CropTool::MouseMoved(int x, int y)
{}
