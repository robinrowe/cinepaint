/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      BucketFillTool - Core tools module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: BucketFillTool.cpp,v 1.2 2006/01/05 03:08:14 robinrowe Exp $
 */

#include "BucketFillTool.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include "plug-ins/pdb/ImageUtils.h"

/*  GIMP icon image format -- S. Kimball, P. Mattis  */
/*  Image name: magnify  */


#define BUCKET_FILL_WIDTH 22
#define BUCKET_FILL_HEIGHT 22
static char* BUCKET_FILL_BITS[] =
{
  "......................",
  "......................",
  ".........ee...........",
  "........eeee..........",
  ".......eeggee.........",
  ".......eegaee.........",
  ".......eeahee.........",
  ".......aahheeaa.......",
  ".......ahhgeegaaa.....",
  "......ahhghaeggaaa....",
  ".....ahhghagaggeaaa...",
  "....ahhghggaggeeaaae..",
  "...ahhghgggggeeaaaae..",
  "...ahghgggggeeaeaaae..",
  "....ahgggggeeaeeaaae..",
  ".....ahgggeeaee.aaae..",
  "......aggeeaee..aaee..",
  ".......aeeaee...aee...",
  "........aaee.....e....",
  ".........ee...........",
  "......................",
  "......................"
};

const char* BucketFillTool::TOOL_TIP = "Fill with a color or pattern";

AbstractBuf* BucketFillTool::BUCKET_FILL_ICON = 0;
unsigned int BucketFillTool::TOOL_COUNT = 0;


/**
 * This is the main entry point used to create an instance of the ImageOp.
**/
extern "C" CINEPAINT_TOOLS_API CinePaintTool* CreateBucketFillToolObject()
{
	return(new BucketFillTool());
}

/**
 * Constructs a new BucketFillTool instance
 *
 */
BucketFillTool::BucketFillTool()
: CinePaintTool("Bucket Fill", "Paint Tools", "Fill with a color or pattern", "help", "CinePaint Team", "copyright", "date")
{
	BucketFillTool::TOOL_COUNT++;
}

/**
 * Destructor
 */
BucketFillTool::~BucketFillTool()
{
	if(--BucketFillTool::TOOL_COUNT == 0)
	{
		delete BucketFillTool::BUCKET_FILL_ICON;
		BucketFillTool::BUCKET_FILL_ICON = 0;
	}
}

/**
 * Returns the type of this CinePaintTool
 *
 * @return the type of this CinePaintTool
 */
const char* BucketFillTool::GetToolType() const
{
	return(CinePaintTool::TOOL_TYPE_BUCKET_FILL);
}

/**
 * Returns an Icon representation of this CinePaintTool
 * The icon representation is suitable for displlay upon a tool button or menu.
 *
 * @return an Icon representation of this CinePaintTool
 */
const AbstractBuf& BucketFillTool::GetIcon() const
{
	if(!BUCKET_FILL_ICON)
	{
		CinePaintTag _tag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false);
		std::auto_ptr<AbstractBuf> _buf = AbstractBufFactory::CreateBuf(_tag, BUCKET_FILL_WIDTH, BUCKET_FILL_HEIGHT, AbstractBufFactory::STORAGE_FLAT);
        BUCKET_FILL_ICON = _buf.release();
		BUCKET_FILL_ICON->RefPortionReadWrite(0,0);

		ImageUtils::GimpIconToAbstractBuf(*BUCKET_FILL_ICON, BUCKET_FILL_BITS, BUCKET_FILL_WIDTH, BUCKET_FILL_HEIGHT);
	}

	return(*BUCKET_FILL_ICON);
}

/**
 * Returns a tool tip for this CinePaintTool
 *
 * @return tooltip for this CinePaintTool
 */
const char* BucketFillTool::GetToolTip() const
{
	return(BucketFillTool::TOOL_TIP);
}




//--------------------------------------
// CinePaintMouseListener implementation

/**
 * Invoked when the mouse pointer moves into an active area
 *
 */
void BucketFillTool::MouseEntered()
{}

/**
 * Invoked when the mouse pointer leaves an active area
 *
 */
void BucketFillTool::MouseExited()
{}

/**
 * Invoked when a mouse button is pressed.
 *
 * @param button the button that was pressed
 * @param count the number of button clicks
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void BucketFillTool::MousePressed(int button, int count, int x, int y, unsigned int modifier)
{}

/**
 * Invoked when a mouse button is released
 *
 * @param button the mouse button released
 */
void BucketFillTool::MouseReleased(int button, int x, int y)
{}

/**
 * Invoked when the mouse pointer is dragged with a mouse button held pressed
 *
 * @param button the held mouse button
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void BucketFillTool::MouseDragged(int button, int x, int y)
{}

/**
 * Invoked when the mouse pointer is moved within an active area
 *
 * @param x the current mouse position
 * @param y the current mouse position
 */
void BucketFillTool::MouseMoved(int x, int y)
{}
