/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ScaleTool - Core tools module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ScaleTool.cpp,v 1.2 2006/01/05 03:08:14 robinrowe Exp $
 */

#include "ScaleTool.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include "plug-ins/pdb/ImageUtils.h"
#include "plug-ins/pdb/CPPluginUIBool.h"
#include "plug-ins/pdb/CPPluginUIDialog.h"
#include "plug-ins/pdb/CPPluginUIList.h"
#include "plug-ins/pdb/CPPluginUINumber.h"
#include "plug-ins/pdb/CPPluginUIWidget.h"

/*  GIMP icon image format -- S. Kimball, P. Mattis  */
/*  Image name: scale  */

#define SCALE_WIDTH 22
#define SCALE_HEIGHT 22
static char* SCALE_BITS[] =
{
	"......................",
	"......................",
	"..aaaaaaa.............",
	"..aeeeeeaeaeaea.......",
	"..aeeeeeaggggge.......",
	"..aeeggggaaaaaaaaaa...",
	"..aeegggegggggegggae..",
	"..aaageeegggggagggae..",
	"...egaggggggggegggae..",
	"...agaggggggggegggae..",
	"...egagggggggaggggae..",
	"...aeaeaeaeaegagagae..",
	".....agggggggggaagae..",
	".....aggggggggaaagae..",
	".....aggggggggggggae..",
	".....aaaaaaaaaaaaaae..",
	"......eeeeeeeeeeeeee..",
	"......................",
	"......................",
	"......................",
	"......................",
	"......................"
};

const char* ScaleTool::TOOL_TIP = "Transform the layer or selection";

AbstractBuf* ScaleTool::SCALE_ICON = 0;
unsigned int ScaleTool::TOOL_COUNT = 0;

// user interface definitions.
static CPPluginUIDialog _dlg("transform_options", "Transform Options", 100, 100);
static char* _transform_opts[] = {"Rotation", "Scaling", "Shearing", "Perspecitve"};
static CPPluginUIList _transform("transform", "Transform", _transform_opts, 4, 1);
static CPPluginUIBool _smoothing("smoothing", "Smoothing", false);
static int num_widgets = 3;
static CPPluginUIWidget* ui_widgets[] = {&_dlg, &_transform, &_smoothing};


/**
 * This is the main entry point used to create an instance of the ImageOp.
**/
extern "C" CINEPAINT_TOOLS_API CinePaintTool* CreateScaleToolObject()
{
	return(new ScaleTool());
}

/**
 * Protcted Constructor - disallow instances
 *
 * @param name the name of this CinePaintTool
 * @param blurb 
 * @param help 
 * @param author author of This CinePaintTool
 * @param copyright copryright notice for this CinePaintTool plugin
 * @param date date this CinePaintTool was written
 */
ScaleTool::ScaleTool()
: CinePaintTool("Scale", "Transform Tools", "Transform the layer or selection", "help", "CinePaint Team", "copyright", "date")
{
	ScaleTool::TOOL_COUNT++;
}

/**
 * Destructor
 */
ScaleTool::~ScaleTool()
{
	if(--ScaleTool::TOOL_COUNT == 0)
	{
		delete ScaleTool::SCALE_ICON;
		ScaleTool::SCALE_ICON = 0;
	}
}

/**
 * Returns the type of this CinePaintTool
 *
 * @return the type of this CinePaintTool
 */
const char* ScaleTool::GetToolType() const
{
	return(CinePaintTool::TOOL_TYPE_SCALE);
}

/**
 * Returns an Icon representation of this CinePaintTool
 * The icon representation is suitable for displlay upon a tool button or menu.
 *
 * @return an Icon representation of this CinePaintTool
 */
const AbstractBuf& ScaleTool::GetIcon() const
{
	if(!SCALE_ICON)
	{
		CinePaintTag _tag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false);
		std::auto_ptr<AbstractBuf> _buf = AbstractBufFactory::CreateBuf(_tag, SCALE_WIDTH, SCALE_HEIGHT, AbstractBufFactory::STORAGE_FLAT);
        SCALE_ICON = _buf.release();
		SCALE_ICON->RefPortionReadWrite(0,0);

		ImageUtils::GimpIconToAbstractBuf(*SCALE_ICON, SCALE_BITS, SCALE_WIDTH, SCALE_HEIGHT);
	}

	return(*SCALE_ICON);
}

/**
 * Returns a tool tip for this CinePaintTool
 *
 * @return tooltip for this CinePaintTool
 */
const char* ScaleTool::GetToolTip() const
{
	return(ScaleTool::TOOL_TIP);
}

/**
 * Returns the UI definition for this CinePaintTool
 *
 * @param ui populated with the definition of this tools ui panel
 * @return ui paramater populated with thie tools ui definition
 */
CPPluginUIDef& ScaleTool::GetUIDefs(CPPluginUIDef& ui)
{
	ui.num = num_widgets;
	ui.widgets = ui_widgets;
	return(ui);
}


//--------------------------------------
// CinePaintMouseListener implementation

/**
 * Invoked when the mouse pointer moves into an active area
 *
 */
void ScaleTool::MouseEntered()
{}

/**
 * Invoked when the mouse pointer leaves an active area
 *
 */
void ScaleTool::MouseExited()
{}

/**
 * Invoked when a mouse button is pressed.
 *
 * @param button the button that was pressed
 * @param count the number of button clicks
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void ScaleTool::MousePressed(int button, int count, int x, int y, unsigned int modifier)
{}

/**
 * Invoked when a mouse button is released
 *
 * @param button the mouse button released
 */
void ScaleTool::MouseReleased(int button, int x, int y)
{}

/**
 * Invoked when the mouse pointer is dragged with a mouse button held pressed
 *
 * @param button the held mouse button
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void ScaleTool::MouseDragged(int button, int x, int y)
{}

/**
 * Invoked when the mouse pointer is moved within an active area
 *
 * @param x the current mouse position
 * @param y the current mouse position
 */
void ScaleTool::MouseMoved(int x, int y)
{}
