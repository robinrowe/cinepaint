/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      FuzzySelectTool - Core tools module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: FuzzySelectTool.cpp,v 1.2 2006/01/05 03:08:14 robinrowe Exp $
 */

#include "FuzzySelectTool.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include "plug-ins/pdb/ImageUtils.h"
#include "plug-ins/pdb/CPPluginUIBool.h"
#include "plug-ins/pdb/CPPluginUIDialog.h"
#include "plug-ins/pdb/CPPluginUINumber.h"
#include "plug-ins/pdb/CPPluginUIWidget.h"

/*  GIMP icon image format -- S. Kimball, P. Mattis  */
/*  Image name: fuzzy  */


#define FUZZY_WIDTH 22
#define FUZZY_HEIGHT 22
static char* FUZZY_BITS[] =
{
  "......................",
  "......................",
  "......h...............",
  "....hgagh.............",
  "....ahgha.............",
  "...hgahagh............",
  "...aghahga............",
  "....hagah.............",
  "....aghga.e...........",
  "......a..ege..........",
  "........eghge.........",
  ".........eghge........",
  "..........egaae.......",
  "...........eaaae......",
  "............eaaae.....",
  ".............eaaae....",
  "..............eaaae...",
  "...............eaaae..",
  "................eae...",
  ".................e....",
  "......................",
  "......................"
};

const char* FuzzySelectTool::TOOL_TIP = "Select contiguous regions";

AbstractBuf* FuzzySelectTool::TOOL_ICON = 0;
unsigned int FuzzySelectTool::TOOL_COUNT = 0;

// user interface definitions.
static CPPluginUIDialog _dlg("fuzzy_options", "Fuzzy Options", 100, 100);
static CPPluginUIBool _sample_merged("samples_merged", "Sample merged", false);
static CPPluginUIBool _antialias("anti_alias", "Anti Aliasing", false);
static CPPluginUIBool _feather("feather", "Feather", false);
static CPPluginUINumber _feather_radius("feather_radius", "Feather Radius", 0, 100, 0.1, 50, CPPluginUINumber::SLIDER);
static CPPluginUINumber _threshold("threshold", "Threshold", 0, 100, 0.001, 50, CPPluginUINumber::SLIDER);
static int num_widgets = 6;
static CPPluginUIWidget* ui_widgets[] = {&_dlg, &_sample_merged, &_antialias, &_feather, &_feather_radius, &_threshold};


/**
 * This is the main entry point used to create an instance of the ImageOp.
**/
extern "C" CINEPAINT_TOOLS_API CinePaintTool* CreateFuzzySelectToolObject()
{
	return(new FuzzySelectTool());
}

/**
 * Constructs a new FuzzySelectTool instance
 *
 */
FuzzySelectTool::FuzzySelectTool()
: CinePaintTool("Fuzzy Selection", "Select Tools", "Select contiguous regions", "help", "CinePaint Team", "copyright", "date")
{
	FuzzySelectTool::TOOL_COUNT++;
}

/**
 * Destructor
 */
FuzzySelectTool::~FuzzySelectTool()
{
	if(--FuzzySelectTool::TOOL_COUNT == 0)
	{
		delete FuzzySelectTool::TOOL_ICON;
		FuzzySelectTool::TOOL_ICON = 0;
	}
}

/**
 * Returns the type of this CinePaintTool
 *
 * @return the type of this CinePaintTool
 */
const char* FuzzySelectTool::GetToolType() const
{
	return(CinePaintTool::TOOL_TYPE_FUZZY_SELECT);
}

/**
 * Returns an Icon representation of this CinePaintTool
 * The icon representation is suitable for displlay upon a tool button or menu.
 *
 * @return an Icon representation of this CinePaintTool
 */
const AbstractBuf& FuzzySelectTool::GetIcon() const
{
	if(!TOOL_ICON)
	{
		CinePaintTag _tag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false);
		std::auto_ptr<AbstractBuf> _buf = AbstractBufFactory::CreateBuf(_tag, FUZZY_WIDTH, FUZZY_HEIGHT, AbstractBufFactory::STORAGE_FLAT);
        TOOL_ICON = _buf.release();
		TOOL_ICON->RefPortionReadWrite(0,0);

		ImageUtils::GimpIconToAbstractBuf(*TOOL_ICON, FUZZY_BITS, FUZZY_WIDTH, FUZZY_HEIGHT);
	}

	return(*TOOL_ICON);
}

/**
 * Returns a tool tip for this CinePaintTool
 *
 * @return tooltip for this CinePaintTool
 */
const char* FuzzySelectTool::GetToolTip() const
{
	return(FuzzySelectTool::TOOL_TIP);
}

/**
 * Returns the UI definition for this CinePaintTool
 *
 * @param ui populated with the definition of this tools ui panel
 * @return ui paramater populated with thie tools ui definition
 */
CPPluginUIDef& FuzzySelectTool::GetUIDefs(CPPluginUIDef& ui)
{
	ui.num = num_widgets;
	ui.widgets = ui_widgets;
	return(ui);
}



//--------------------------------------
// CinePaintMouseListener implementation

/**
 * Invoked when the mouse pointer moves into an active area
 *
 */
void FuzzySelectTool::MouseEntered()
{}

/**
 * Invoked when the mouse pointer leaves an active area
 *
 */
void FuzzySelectTool::MouseExited()
{}

/**
 * Invoked when a mouse button is pressed.
 *
 * @param button the button that was pressed
 * @param count the number of button clicks
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void FuzzySelectTool::MousePressed(int button, int count, int x, int y, unsigned int modifier)
{}

/**
 * Invoked when a mouse button is released
 *
 * @param button the mouse button released
 */
void FuzzySelectTool::MouseReleased(int button, int x, int y)
{}

/**
 * Invoked when the mouse pointer is dragged with a mouse button held pressed
 *
 * @param button the held mouse button
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void FuzzySelectTool::MouseDragged(int button, int x, int y)
{}

/**
 * Invoked when the mouse pointer is moved within an active area
 *
 * @param x the current mouse position
 * @param y the current mouse position
 */
void FuzzySelectTool::MouseMoved(int x, int y)
{}
