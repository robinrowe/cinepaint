/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      AirBrushTool - Core tools module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: AirBrushTool.h,v 1.2 2006/01/05 03:08:14 robinrowe Exp $
 */

#ifndef _CINEPAINT_AIR_BRUSH_TOOL_H_
#define _CINEPAINT_AIR_BRUSH_TOOL_H_

#include "dll_api.h"
#include "plug-ins/pdb/CinePaintTool.h"

// Forward Declaration
class AbstractBuf;

extern "C" CINEPAINT_TOOLS_API CinePaintTool* CreateAirBrushToolObject();

/**
 * CinePaintTool defines an abstract interface for core CinePaint Tools
 * CinePaintTool provides an plugin mechanism to load system tools that may be applied to an image.
 * CinePaintTools convert from mouse events, taking into account tool settings and apply a process
 * to an image vie call to registered system operations
 *
 */
class CINEPAINT_TOOLS_API AirBrushTool : public CinePaintTool
{
	public:
		/**
		 * Constructs a new AirBrushTool instance
		 *
		 */
		AirBrushTool();

		/**
		 * Destructor
		 */
		virtual ~AirBrushTool();

		/**
		 * Returns the type of this CinePaintTool
		 *
		 * @return the type of this CinePaintTool
		 */
		virtual const char* GetToolType() const;

		/**
		 * Returns an Icon representation of this CinePaintTool
		 * The icon representation is suitable for display upon a tool button or menu.
		 *
		 * @return an Icon representation of this CinePaintTool
		 */
		virtual const AbstractBuf& GetIcon() const;

		/**
		 * Returns a tool tip for this CinePaintTool
		 *
		 * @return tooltip for this CinePaintTool
		 */
		virtual const char* GetToolTip() const;

		/**
		 * Returns the UI definition for this CinePaintTool
		 *
		 * @param ui populated with the definition of this tools ui panel
		 * @return ui paramater populated with thie tools ui definition
		 */
		virtual CPPluginUIDef& GetUIDefs(CPPluginUIDef& ui);




		//--------------------------------------
		// CinePaintMouseListener implementation
		
		/**
		 * Invoked when the mouse pointer moves into an active area
		 *
		 */
		virtual void MouseEntered();

		/**
		 * Invoked when the mouse pointer leaves an active area
		 *
		 */
		virtual void MouseExited();

		/**
		 * Invoked when a mouse button is pressed.
		 *
		 * @param button the button that was pressed
		 * @param count the number of button clicks
		 * @param x the current mouse position
		 * @param y the current mouse poeition
		 */
		virtual void MousePressed(int button, int count, int x, int y, unsigned int modifier);

		/**
		 * Invoked when a mouse button is released
		 *
		 * @param button the mouse button released
		 * @param x the current mouse position
		 * @param y the current mouse poeition
		 */
		virtual void MouseReleased(int button, int x, int y);

		/**
		 * Invoked when the mouse pointer is dragged with a mouse button held pressed
		 *
		 * @param button the held mouse button
		 * @param x the current mouse position
		 * @param y the current mouse poeition
		 */
		virtual void MouseDragged(int button, int x, int y);

		/**
		 * Invoked when the mouse pointer is moved within an active area
		 *
		 * @param x the current mouse position
		 * @param y the current mouse position
		 */
		virtual void MouseMoved(int x, int y);

	protected:

	private:

		/** tool button icon */
		static AbstractBuf* TOOL_ICON;

		/** static count of instances of this tool, this allows us to delete the icon when the last istance is destroyed */
		static unsigned int TOOL_COUNT;

		/** default toolbutton tooltip */
		static const char* TOOL_TIP;

};

#endif //_CINEPAINT_AIR_BRUSH_TOOL_H_
