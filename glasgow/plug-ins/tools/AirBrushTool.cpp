/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      AirBrushTool - Core tools module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: AirBrushTool.cpp,v 1.3 2006/12/18 08:21:24 robinrowe Exp $
 */

#include "AirBrushTool.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include "plug-ins/pdb/ImageUtils.h"
#include "plug-ins/pdb/CPPluginUIBool.h"
#include "plug-ins/pdb/CPPluginUIDialog.h"
#include "plug-ins/pdb/CPPluginUINumber.h"
#include "plug-ins/pdb/CPPluginUIWidget.h"

/*  GIMP icon image format -- S. Kimball, P. Mattis  */
/*  Image name: bezier  */


#define AIRBRUSH_WIDTH 22
#define AIRBRUSH_HEIGHT 22
static char* AIRBRUSH_BITS [] =
{
  "......................",
  "......................",
  "......................",
  "................aaa...",
  "...............aaaae..",
  "..............aaaaae..",
  ".............aaaaaee..",
  "............aaaaaee...",
  ".......aa..aaaaaee....",
  "......aa..aaaaaee.....",
  "......a.aaaaaaee......",
  "........ahaaaee.......",
  ".......ahhgaee........",
  "......ahhgaee.........",
  ".....ahhgaae.....aa...",
  "....ahhgaeea....a.....",
  "....ahgaee.a....a.....",
  "...ahaaee...a..a......",
  "...aaeee.....aa.......",
  "....ee................",
  "......................",
  "......................"
};

const char* AirBrushTool::TOOL_TIP = "Airbrush with variable pressure";

AbstractBuf* AirBrushTool::TOOL_ICON = 0;
unsigned int AirBrushTool::TOOL_COUNT = 0;

// user interface definitions.
static CPPluginUIDialog _dlg("airbrush_options", "Airbrush Options", 100, 100);
static CPPluginUIBool _incremental("incremantal", "Incremental", false);
static CPPluginUIBool _hardness("hardness", "Hardness", false);
static CPPluginUIBool _rate("rate", "Rate", false);
static CPPluginUIBool _size("size", "Size", false);
static CPPluginUINumber _rate_val("rate_val", "Rate", 0, 100, 0.1, 50, CPPluginUINumber::SLIDER);
static CPPluginUINumber _pressure("pressure_val", "Pressure", 0, 100, 0.1, 50, CPPluginUINumber::SLIDER);
static int num_widgets = 7;
static CPPluginUIWidget *ui_widgets[] = {&_dlg, &_incremental, &_hardness, &_rate, &_size, &_rate_val, &_pressure};



/**
 * This is the main entry point used to create an instance of the ImageOp.
**/
extern "C" CINEPAINT_TOOLS_API CinePaintTool* CreateAirBrushToolObject()
{
	return(new AirBrushTool());
}

/**
 * Constructs a new AirBrushTool instance
 *
 */
AirBrushTool::AirBrushTool()
: CinePaintTool("AirBrush", "Paint Tools", "Airbrush with variable pressure", "help", "CinePaint Team", "copyright", "date")
{
	AirBrushTool::TOOL_COUNT++;
}

/**
 * Destructor
 */
AirBrushTool::~AirBrushTool()
{
	if(--AirBrushTool::TOOL_COUNT == 0)
	{
		delete AirBrushTool::TOOL_ICON;
		AirBrushTool::TOOL_ICON = 0;
	}
}

/**
 * Returns the type of this CinePaintTool
 *
 * @return the type of this CinePaintTool
 */
const char* AirBrushTool::GetToolType() const
{
	return(CinePaintTool::TOOL_TYPE_AIRBRUSH);
}

/**
 * Returns an Icon representation of this CinePaintTool
 * The icon representation is suitable for displlay upon a tool button or menu.
 *
 * @return an Icon representation of this CinePaintTool
 */
const AbstractBuf& AirBrushTool::GetIcon() const
{
	if(!TOOL_ICON)
	{
		CinePaintTag _tag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false);
		std::auto_ptr<AbstractBuf> _buf = AbstractBufFactory::CreateBuf(_tag, AIRBRUSH_WIDTH, AIRBRUSH_HEIGHT, AbstractBufFactory::STORAGE_FLAT);
        TOOL_ICON = _buf.release();
		TOOL_ICON->RefPortionReadWrite(0,0);

		ImageUtils::GimpIconToAbstractBuf(*TOOL_ICON, AIRBRUSH_BITS, AIRBRUSH_WIDTH, AIRBRUSH_HEIGHT);
	}

	return(*TOOL_ICON);
}

/**
 * Returns a tool tip for this CinePaintTool
 *
 * @return tooltip for this CinePaintTool
 */
const char* AirBrushTool::GetToolTip() const
{
	return(AirBrushTool::TOOL_TIP);
}

/**
 * Returns the UI definition for this CinePaintTool
 *
 * @param ui populated with the definition of this tools ui panel
 * @return ui paramater populated with thie tools ui definition
 */
CPPluginUIDef& AirBrushTool::GetUIDefs(CPPluginUIDef& ui)
{
	ui.num = num_widgets;
	ui.widgets = ui_widgets;
	return(ui);
}



//--------------------------------------
// CinePaintMouseListener implementation

/**
 * Invoked when the mouse pointer moves into an active area
 *
 */
void AirBrushTool::MouseEntered()
{}

/**
 * Invoked when the mouse pointer leaves an active area
 *
 */
void AirBrushTool::MouseExited()
{}

/**
 * Invoked when a mouse button is pressed.
 *
 * @param button the button that was pressed
 * @param count the number of button clicks
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void AirBrushTool::MousePressed(int button, int count, int x, int y, unsigned int modifier)
{}

/**
 * Invoked when a mouse button is released
 *
 * @param button the mouse button released
 */
void AirBrushTool::MouseReleased(int button, int x, int y)
{}

/**
 * Invoked when the mouse pointer is dragged with a mouse button held pressed
 *
 * @param button the held mouse button
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void AirBrushTool::MouseDragged(int button, int x, int y)
{}

/**
 * Invoked when the mouse pointer is moved within an active area
 *
 * @param x the current mouse position
 * @param y the current mouse position
 */
void AirBrushTool::MouseMoved(int x, int y)
{}

