/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      RectSelectTool - Core tools module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: RectSelectTool.cpp,v 1.2 2006/01/05 03:08:14 robinrowe Exp $
 */

#define NOMINMAX

#include "RectSelectTool.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include "plug-ins/pdb/CinePaintImage.h"
#include "plug-ins/pdb/ImageUtils.h"
#include "plug-ins/pdb/Layer.h"
#include "plug-ins/pdb/SelectionManager.h"
//#include <sys/app/CinePaintApp.h"
#include "plug-ins/pdb/CinePaintDoc.h"
//#include "plug-ins/pdb/CinePaintDocList.h"
#include "plug-ins/pdb/ModifierEnums.h"
#include "plug-ins/pdb/CPPluginUIBool.h"
#include "plug-ins/pdb/CPPluginUIDialog.h"
#include "plug-ins/pdb/CPPluginUINumber.h"
#include "plug-ins/pdb/CPPluginUIWidget.h"
#include "utility/CPRect.h"
#include <algorithm>
#include <cmath>

/*  GIMP icon image format -- S. Kimball, P. Mattis  */
/*  Image name: rect  */


#define RECT_WIDTH 22
#define RECT_HEIGHT 22
static char* RECT_BITS[] =
{
  "......................",
  "......................",
  "......................",
  "......................",
  "......................",
  "....aahhaahhaahha.....",
  "....hggggggggggga.....",
  "....hgggggggggggh.....",
  "....agggggggggggh.....",
  "....aggggggggggga.....",
  "....hggggggggggga.....",
  "....hgggggggggggh.....",
  "....agggggggggggh.....",
  "....aggggggggggga.....",
  "....hggggggggggga.....",
  "....haahhaahhaahh.....",
  "......................",
  "......................",
  "......................",
  "......................",
  "......................",
  "......................"
};

const char* RectSelectTool::TOOL_TIP = "Select rectangular regions";

AbstractBuf* RectSelectTool::TOOL_ICON = 0;
unsigned int RectSelectTool::TOOL_COUNT = 0;


// user interface definitions.

static CPPluginUIDialog _dlg("rectangular_select_options", "Rectangular Select Options", 100, 100);
static CPPluginUIBool _feather("feather", "Feather", false);
static CPPluginUINumber _feather_radius("feather_radius", "Feather Radius", 0, 100, 0.1, 50, CPPluginUINumber::SLIDER);
static int num_widgets = 3;
static CPPluginUIWidget* ui_widgets[] = {&_dlg, &_feather, &_feather_radius};


/**
 * This is the main entry point used to create an instance of the ImageOp.
**/
extern "C" CINEPAINT_TOOLS_API CinePaintTool* CreateRectSelectToolObject()
{
	return(new RectSelectTool());
}

/**
 * Constructs a new RectSelectTool instance
 *
 */
RectSelectTool::RectSelectTool()
: CinePaintTool("Rectangluar Selection", "Select Tools", "Select rectangular regions", "help", "CinePaint Team", "copyright", "date")
{
	RectSelectTool::TOOL_COUNT++;
	
	m_region_start.m_x = 0;
	m_region_start.m_y = 0;

	m_region_end.m_x = 0;
	m_region_end.m_y = 0;

	m_selection_type = SELECTION_REPLACE_ENUM;
}

/**
 * Destructor
 */
RectSelectTool::~RectSelectTool()
{
	if(--RectSelectTool::TOOL_COUNT == 0)
	{
		delete RectSelectTool::TOOL_ICON;
		RectSelectTool::TOOL_ICON = 0;
	}
}

/**
 * Returns the type of this CinePaintTool
 *
 * @return the type of this CinePaintTool
 */
const char* RectSelectTool::GetToolType() const
{
	return(CinePaintTool::TOOL_TYPE_RECT_SELECT);
}

/**
 * Returns an Icon representation of this CinePaintTool
 * The icon representation is suitable for displlay upon a tool button or menu.
 *
 * @return an Icon representation of this CinePaintTool
 */
const AbstractBuf& RectSelectTool::GetIcon() const
{
	if(!TOOL_ICON)
	{
		CinePaintTag _tag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false);
		std::auto_ptr<AbstractBuf> _buf = AbstractBufFactory::CreateBuf(_tag, RECT_WIDTH, RECT_HEIGHT, AbstractBufFactory::STORAGE_FLAT);
        TOOL_ICON = _buf.release();
		TOOL_ICON->RefPortionReadWrite(0,0);

		ImageUtils::GimpIconToAbstractBuf(*TOOL_ICON, RECT_BITS, RECT_WIDTH, RECT_HEIGHT);
	}

	return(*TOOL_ICON);
}

/**
 * Returns a tool tip for this CinePaintTool
 *
 * @return tooltip for this CinePaintTool
 */
const char* RectSelectTool::GetToolTip() const
{
	return(RectSelectTool::TOOL_TIP);
}

/**
 * Returns the UI definition for this CinePaintTool
 *
 * @param ui populated with the definition of this tools ui panel
 * @return ui paramater populated with thie tools ui definition
 */
CPPluginUIDef& RectSelectTool::GetUIDefs(CPPluginUIDef& ui)
{
	ui.num = num_widgets;
	ui.widgets = ui_widgets;
	return(ui);
}






//--------------------------------------
// CinePaintMouseListener implementation

/**
 * Invoked when the mouse pointer moves into an active area
 *
 */
void RectSelectTool::MouseEntered()
{
	CinePaintImage* _img = GetActiveImage();
	if (_img)
	{
		Layer* _layer = _img->GetLayers().GetActiveLayer();
		if(_layer)
		{
			CPRect _r;
			_layer->GetSelectionManager().GetSelectionBounds(_r);
		}
	}
}

/**
 * Invoked when the mouse pointer leaves an active area
 *
 */
void RectSelectTool::MouseExited()
{}

/**
 * Invoked when a mouse button is pressed.
 *
 * @param button the button that was pressed
 * @param count the number of button clicks
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void RectSelectTool::MousePressed(int button, int count, int x, int y, unsigned int modifier)
{
	m_region_start.m_x = x;
	m_region_start.m_y = y;

	if (modifier & CP_MOD_SHIFT_MASK_ENUM)
		printf(" Info: Mouse Down with Shift\n");
	CinePaintImage* _img = GetActiveImage();
	if (_img)
	{
		Layer* _layer = _img->GetLayers().GetActiveLayer();
		if(_layer)
		{
			_layer->GetSelectionManager().ClearSelection();
		}
	}
}

/**
 * Invoked when a mouse button is released
 *
 * @param button the mouse button released
 */
void RectSelectTool::MouseReleased(int button, int x, int y)
{
	m_region_end.m_x = x;
	m_region_end.m_y = y;

	int _x = std::min(m_region_start.m_x, m_region_end.m_x);
	int _y = std::min(m_region_start.m_y, m_region_end.m_y);
	int _w = std::abs(m_region_start.m_x - m_region_end.m_x);
	int _h = std::abs(m_region_start.m_y - m_region_end.m_y);

	select_rect(_x, _y, _w, _h);
}

/**
 * Invoked when the mouse pointer is dragged with a mouse button held pressed
 *
 * @param button the held mouse button
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void RectSelectTool::MouseDragged(int button, int x, int y)
{
	m_region_end.m_x = x;
	m_region_end.m_y = y;

	int _x = std::min(m_region_start.m_x, m_region_end.m_x);
	int _y = std::min(m_region_start.m_y, m_region_end.m_y);
	int _w = std::abs(m_region_start.m_x - m_region_end.m_x);
	int _h = std::abs(m_region_start.m_y - m_region_end.m_y);

	CinePaintImage* _img = GetActiveImage();
	if (_img)
	{
		Layer* _layer = _img->GetLayers().GetActiveLayer();
		if(_layer)
		{
			_layer->GetSelectionManager().UpdateSelectionRect(_x, _y, _w, _h);
			_layer->Update(_x, _y, _w, _h);
		}
	}
}

/**
 * Invoked when the mouse pointer is moved within an active area
 *
 * @param x the current mouse position
 * @param y the current mouse position
 */
void RectSelectTool::MouseMoved(int x, int y)
{}

void RectSelectTool::select_rect(unsigned int x, unsigned int y, unsigned int w, unsigned int h)
{
	CinePaintImage* _img = GetActiveImage();
	if (_img)
	{
		Layer* _layer = _img->GetLayers().GetActiveLayer();
		if(_layer)
		{
			_layer->GetSelectionManager().ClearSelection();
			_layer->GetSelectionManager().AddSelectionRect(x, y, w, h);
		}
	}
}

