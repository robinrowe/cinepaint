/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      FlipTool - Core tools module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: FlipTool.cpp,v 1.2 2006/01/05 03:08:14 robinrowe Exp $
 */

#include "FlipTool.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include "plug-ins/pdb/ImageUtils.h"
#include "plug-ins/pdb/CPPluginUIDialog.h"
#include "plug-ins/pdb/CPPluginUIList.h"
#include "plug-ins/pdb/CPPluginUIWidget.h"

/*  GIMP icon image format -- S. Kimball, P. Mattis  */
/*  Image name: magnify  */


#define FLIP_WIDTH 22
#define FLIP_HEIGHT 22
static char* FLIP_BITS[] =
{
  "......................",
  "......................",
  "......................",
  "......................",
  "......................",
  "......................",
  "......................",
  "......a........a......",
  ".....aa........aa.....",
  "....aaaaaaaaaaaaaa....",
  "...aaaeeeeeeeeeeaaa...",
  "....aaaaaaaaaaaaaaee..",
  ".....aaeeeeeeeeaaee...",
  "......ae.......aee....",
  ".......e........e.....",
  "......................",
  "......................",
  "......................",
  "......................",
  "......................",
  "......................",
  "......................"
};

const char* FlipTool::TOOL_TIP = "Flip the layer or selection";

AbstractBuf* FlipTool::FLIP_ICON = 0;
unsigned int FlipTool::TOOL_COUNT = 0;

// user interface definitions.
static CPPluginUIDialog _dlg("flip_options", "Flip Options", 100, 100);
static char* _flip_opts[] = {"Horitzontal", "Vertical"};
static CPPluginUIList _flip_type("flip_type", "Flip Type", _flip_opts, 2, 1);
static int num_widgets = 2;
static CPPluginUIWidget* ui_widgets[] = {&_dlg, &_flip_type};


/**
 * This is the main entry point used to create an instance of the ImageOp.
**/
extern "C" CINEPAINT_TOOLS_API CinePaintTool* CreateFlipToolObject()
{
	return(new FlipTool());
}

/**
 * Constructs a new FlipTool instance
 *
 */
FlipTool::FlipTool()
: CinePaintTool("Flip", "Transform Tools", "Flip the layer or selection", "help", "CinePaint Team", "copyright", "date")
{
	FlipTool::TOOL_COUNT++;
}

/**
 * Destructor
 */
FlipTool::~FlipTool()
{
	if(--FlipTool::TOOL_COUNT == 0)
	{
		delete FlipTool::FLIP_ICON;
		FlipTool::FLIP_ICON = 0;
	}
}

/**
 * Returns the type of this CinePaintTool
 *
 * @return the type of this CinePaintTool
 */
const char* FlipTool::GetToolType() const
{
	return(CinePaintTool::TOOL_TYPE_FLIP);
}

/**
 * Returns an Icon representation of this CinePaintTool
 * The icon representation is suitable for displlay upon a tool button or menu.
 *
 * @return an Icon representation of this CinePaintTool
 */
const AbstractBuf& FlipTool::GetIcon() const
{
	if(!FLIP_ICON)
	{
		CinePaintTag _tag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false);
		std::auto_ptr<AbstractBuf> _buf = AbstractBufFactory::CreateBuf(_tag, FLIP_WIDTH, FLIP_HEIGHT, AbstractBufFactory::STORAGE_FLAT);
        FLIP_ICON = _buf.release();
		FLIP_ICON->RefPortionReadWrite(0,0);

		ImageUtils::GimpIconToAbstractBuf(*FLIP_ICON, FLIP_BITS, FLIP_WIDTH, FLIP_HEIGHT);
	}

	return(*FLIP_ICON);
}

/**
 * Returns a tool tip for this CinePaintTool
 *
 * @return tooltip for this CinePaintTool
 */
const char* FlipTool::GetToolTip() const
{
	return(FlipTool::TOOL_TIP);
}

/**
 * Returns the UI definition for this CinePaintTool
 *
 * @param ui populated with the definition of this tools ui panel
 * @return ui paramater populated with thie tools ui definition
 */
CPPluginUIDef& FlipTool::GetUIDefs(CPPluginUIDef& ui)
{
	ui.num = num_widgets;
	ui.widgets = ui_widgets;
	return(ui);
}


//--------------------------------------
// CinePaintMouseListener implementation

/**
 * Invoked when the mouse pointer moves into an active area
 *
 */
void FlipTool::MouseEntered()
{}

/**
 * Invoked when the mouse pointer leaves an active area
 *
 */
void FlipTool::MouseExited()
{}

/**
 * Invoked when a mouse button is pressed.
 *
 * @param button the button that was pressed
 * @param count the number of button clicks
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void FlipTool::MousePressed(int button, int count, int x, int y, unsigned int modifier)
{}

/**
 * Invoked when a mouse button is released
 *
 * @param button the mouse button released
 */
void FlipTool::MouseReleased(int button, int x, int y)
{}

/**
 * Invoked when the mouse pointer is dragged with a mouse button held pressed
 *
 * @param button the held mouse button
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void FlipTool::MouseDragged(int button, int x, int y)
{}

/**
 * Invoked when the mouse pointer is moved within an active area
 *
 * @param x the current mouse position
 * @param y the current mouse position
 */
void FlipTool::MouseMoved(int x, int y)
{}
