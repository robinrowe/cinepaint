/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Tool DLL Main entry points - Core tools module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ToolsPluginMain.cpp,v 1.2 2006/01/05 03:08:14 robinrowe Exp $
 */

#include "plug-ins/pdb/CinePaintTool.h"
#include "BezierSelectTool.h"
#include "EllipseSelectTool.h"
#include "FreeSelectTool.h"
#include "FuzzySelectTool.h"
#include "RectSelectTool.h"
#include "ScaleTool.h"
#include "AirBrushTool.h"
#include "CloneTool.h"
#include "ConvolveTool.h"
#include "DodgeTool.h"
#include "EraserTool.h"
#include "PaintTool.h"
#include "PencilTool.h"
#include "SmudgeTool.h"
#include "CropTool.h"
#include "MoveTool.h"
#include "ColorPickerTool.h"
#include "TextTool.h"
#include "BlendTool.h"
#include "BucketFillTool.h"
#include "IScissorsTool.h"
#include "FlipTool.h"
#include "ZoomTool.h"
#include "MeasureTool.h"

// List of classes that this DLL provides, MUST MUST be null terminated.
CinePaintTool::PFNCreateCinePaintTool ToolClassList[] = { \
	CreateScaleToolObject,
	CreateZoomToolObject,
	CreateRectSelectToolObject,
	CreateEllipseSelectToolObject,
	CreateFreeSelectToolObject,
	CreateFuzzySelectToolObject,
	CreateBezierSelectToolObject,
	CreateAirBrushToolObject,
	CreateCloneToolObject,
	CreateConvolveToolObject,
	CreateDodgeToolObject,
	CreateEraserToolObject,
	CreatePaintToolObject,
	CreatePencilToolObject,
	CreateSmudgeToolObject,
	CreateCropToolObject,
	CreateMoveToolObject,
	CreateColorPickerToolObject,
	CreateTextToolObject,
	CreateBlendToolObject,
	CreateBucketFillToolObject,
	CreateIScissorsToolObject,
	CreateFlipToolObject,
	CreateMeasureToolObject,
	0};

// Exported function to 
extern "C" CINEPAINT_TOOLS_API CinePaintTool::PFNCreateCinePaintTool *GetToolClassList()
{
	return(ToolClassList);
}
