/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      BlendTool - Core tools module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: BlendTool.cpp,v 1.2 2006/01/05 03:08:14 robinrowe Exp $
 */

#include "BlendTool.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include "plug-ins/pdb/ImageUtils.h"
#include "plug-ins/pdb/CPPluginUIDialog.h"
#include "plug-ins/pdb/CPPluginUIList.h"
#include "plug-ins/pdb/CPPluginUINumber.h"
#include "plug-ins/pdb/CPPluginUIWidget.h"

/*  GIMP icon image format -- S. Kimball, P. Mattis  */
/*  Image name: magnify  */


#define BLEND_WIDTH 22
#define BLEND_HEIGHT 22
static char* BLEND_BITS[] =
{
 "......................",
  "......................",
  "......................",
  "..eeeeeeeeeeeeeeeeee..",
  "..aaaeeeegggggggghhe..",
  "..aaeaeegegggggghghe..",
  "..aaaeeeegggggggghhe..",
  "..aaeaeegegggggghghe..",
  "..aaaeeeegggggggghhe..",
  "..aaeaeegegggggghghe..",
  "..aaaeeeegggggggghhe..",
  "..aaeaeegegggggghghe..",
  "..aaaeeeegggggggghhe..",
  "..aaeaeegegggggghghe..",
  "..aaaeeeegggggggghhe..",
  "..aaeaeegegggggghghe..",
  "..aaaeeeegggggggghhe..",
  "..aaaaeegegggggghghe..",
  "..eeeeeeeeeeeeeeeeee..",
  "......................",
  "......................",
  "......................"
};

const char* BlendTool::TOOL_TIP = "Fill with a color gradient";

AbstractBuf* BlendTool::BLEND_ICON = 0;
unsigned int BlendTool::TOOL_COUNT = 0;

// user interface definitions.
static CPPluginUIDialog _dlg("blend_options", "Blend Options", 100, 100);
static CPPluginUINumber _capacity("capacity","Capacity", 0, 100, 0.1, 50, CPPluginUINumber::SLIDER);
static CPPluginUINumber _offset("offset", "Offset", 0, 100, 0.1, 50, CPPluginUINumber::SLIDER);
static char* _mode_opts[] = {"Normal", "Dissolve", "Behind", "Multiply", "Screen", "Overlay", "Difference", "Addition", "Subtract", "Darken Only", "Lighten Only", "Hue", "Saturation", "NColor", "Value"};
static CPPluginUIList _mode("mode", "Mode", _mode_opts, 15, 0);
static char* _clend_opts[] = {"FG to BG (RGB)", "FG to BG (HSV)", "FG to Transparent", "Custom From Editor"};
static CPPluginUIList _blend("blend", "Blend", _clend_opts, 4, 0);
static char* _gradient_opts[] = {"Linear", "Bi-Linear", "Radeal", "Square", "Conical (Symmetric)", "Conical (Asymmetric)", "Log", "Film Log"};
static CPPluginUIList _gradient("gradient", "Gradient", _gradient_opts, 8, 0);
static char* _repeat_opts[] = {"None", "Sawtooth Wave", "Triangular Wave"};
static CPPluginUIList _repeat("repeat", "Repeat", _repeat_opts, 3, 0);
static int num_widgets = 7;
static CPPluginUIWidget* ui_widgets[] = {&_dlg, &_capacity, &_offset, &_mode, &_blend, &_gradient, &_repeat};



/**
 * This is the main entry point used to create an instance of the ImageOp.
**/
extern "C" CINEPAINT_TOOLS_API CinePaintTool* CreateBlendToolObject()
{
	return(new BlendTool());
}

/**
 * Constructs a new BlendTool instance
 *
 */
BlendTool::BlendTool()
: CinePaintTool("Blend", "Paint Tools", "Fill with a color gradient", "help", "CinePaint Team", "copyright", "date")
{
	BlendTool::TOOL_COUNT++;
}

/**
 * Destructor
 */
BlendTool::~BlendTool()
{
	if(--BlendTool::TOOL_COUNT == 0)
	{
		delete BlendTool::BLEND_ICON;
		BlendTool::BLEND_ICON = 0;
	}
}

/**
 * Returns the type of this CinePaintTool
 *
 * @return the type of this CinePaintTool
 */
const char* BlendTool::GetToolType() const
{
	return(CinePaintTool::TOOL_TYPE_BLEND);
}

/**
 * Returns an Icon representation of this CinePaintTool
 * The icon representation is suitable for displlay upon a tool button or menu.
 *
 * @return an Icon representation of this CinePaintTool
 */
const AbstractBuf& BlendTool::GetIcon() const
{
	if(!BLEND_ICON)
	{
		CinePaintTag _tag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false);
		std::auto_ptr<AbstractBuf> _buf = AbstractBufFactory::CreateBuf(_tag, BLEND_WIDTH, BLEND_HEIGHT, AbstractBufFactory::STORAGE_FLAT);
        BLEND_ICON = _buf.release();
		BLEND_ICON->RefPortionReadWrite(0,0);

		ImageUtils::GimpIconToAbstractBuf(*BLEND_ICON, BLEND_BITS, BLEND_WIDTH, BLEND_HEIGHT);
	}

	return(*BLEND_ICON);
}

/**
 * Returns a tool tip for this CinePaintTool
 *
 * @return tooltip for this CinePaintTool
 */
const char* BlendTool::GetToolTip() const
{
	return(BlendTool::TOOL_TIP);
}

/**
 * Returns the UI definition for this CinePaintTool
 *
 * @param ui populated with the definition of this tools ui panel
 * @return ui paramater populated with thie tools ui definition
 */
CPPluginUIDef& BlendTool::GetUIDefs(CPPluginUIDef& ui)
{
	ui.num = num_widgets;
	ui.widgets = ui_widgets;
	return(ui);
}




//--------------------------------------
// CinePaintMouseListener implementation

/**
 * Invoked when the mouse pointer moves into an active area
 *
 */
void BlendTool::MouseEntered()
{}

/**
 * Invoked when the mouse pointer leaves an active area
 *
 */
void BlendTool::MouseExited()
{}

/**
 * Invoked when a mouse button is pressed.
 *
 * @param button the button that was pressed
 * @param count the number of button clicks
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void BlendTool::MousePressed(int button, int count, int x, int y, unsigned int modifier)
{}

/**
 * Invoked when a mouse button is released
 *
 * @param button the mouse button released
 */
void BlendTool::MouseReleased(int button, int x, int y)
{}

/**
 * Invoked when the mouse pointer is dragged with a mouse button held pressed
 *
 * @param button the held mouse button
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void BlendTool::MouseDragged(int button, int x, int y)
{}

/**
 * Invoked when the mouse pointer is moved within an active area
 *
 * @param x the current mouse position
 * @param y the current mouse position
 */
void BlendTool::MouseMoved(int x, int y)
{}
