/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      EraserTool - Core tools module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: EraserTool.cpp,v 1.3 2006/12/18 08:21:24 robinrowe Exp $
 */

#include "EraserTool.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include "plug-ins/pdb/ImageUtils.h"

/*  GIMP icon image format -- S. Kimball, P. Mattis  */
/*  Image name: erase  */


#define ERASE_WIDTH 22
#define ERASE_HEIGHT 22
static char* ERASE_BITS [] =
{
  "......................",
  "......................",
  "......................",
  "......................",
  "......................",
  "......................",
  "..........aaaaaaaa....",
  ".........ahhhhhhaae...",
  "........ahhhhhhagae...",
  ".......ahhhhhhaggae...",
  "......ahhhhhhaggaee...",
  ".....ahhhhhhaggaee....",
  "....ahhhhhhaggaee.....",
  "...aaaaaaaaggaee......",
  "...aggggggagaee.......",
  "...aggggggaaee........",
  "...aaaaaaaaee.........",
  "....eeeeeeee..........",
  "......................",
  "......................",
  "......................",
  "......................"
};

const char* EraserTool::TOOL_TIP = "Erase to background or transparency";

AbstractBuf* EraserTool::TOOL_ICON = 0;
unsigned int EraserTool::TOOL_COUNT = 0;


/**
 * This is the main entry point used to create an instance of the ImageOp.
**/
extern "C" CINEPAINT_TOOLS_API CinePaintTool* CreateEraserToolObject()
{
	return(new EraserTool());
}

/**
 * Constructs a new EraserTool instance
 *
 */
EraserTool::EraserTool()
: CinePaintTool("Eraser", "Paint Tools", "Erase to background or transparency", "help", "CinePaint Team", "copyright", "date")
{
	EraserTool::TOOL_COUNT++;
}

/**
 * Destructor
 */
EraserTool::~EraserTool()
{
	if(--EraserTool::TOOL_COUNT == 0)
	{
		delete EraserTool::TOOL_ICON;
		EraserTool::TOOL_ICON = 0;
	}
}

/**
 * Returns the type of this CinePaintTool
 *
 * @return the type of this CinePaintTool
 */
const char* EraserTool::GetToolType() const
{
	return(CinePaintTool::TOOL_TYPE_ERASER);
}

/**
 * Returns an Icon representation of this CinePaintTool
 * The icon representation is suitable for displlay upon a tool button or menu.
 *
 * @return an Icon representation of this CinePaintTool
 */
const AbstractBuf& EraserTool::GetIcon() const
{
	if(!TOOL_ICON)
	{
		CinePaintTag _tag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false);
		std::auto_ptr<AbstractBuf> _buf = AbstractBufFactory::CreateBuf(_tag, ERASE_WIDTH, ERASE_HEIGHT, AbstractBufFactory::STORAGE_FLAT);
        TOOL_ICON = _buf.release();
		TOOL_ICON->RefPortionReadWrite(0,0);

		ImageUtils::GimpIconToAbstractBuf(*TOOL_ICON, ERASE_BITS, ERASE_WIDTH, ERASE_HEIGHT);
	}

	return(*TOOL_ICON);
}

/**
 * Returns a tool tip for this CinePaintTool
 *
 * @return tooltip for this CinePaintTool
 */
const char* EraserTool::GetToolTip() const
{
	return(EraserTool::TOOL_TIP);
}



//--------------------------------------
// CinePaintMouseListener implementation

/**
 * Invoked when the mouse pointer moves into an active area
 *
 */
void EraserTool::MouseEntered()
{}

/**
 * Invoked when the mouse pointer leaves an active area
 *
 */
void EraserTool::MouseExited()
{}

/**
 * Invoked when a mouse button is pressed.
 *
 * @param button the button that was pressed
 * @param count the number of button clicks
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void EraserTool::MousePressed(int button, int count, int x, int y, unsigned int modifier)
{}

/**
 * Invoked when a mouse button is released
 *
 * @param button the mouse button released
 */
void EraserTool::MouseReleased(int button, int x, int y)
{}

/**
 * Invoked when the mouse pointer is dragged with a mouse button held pressed
 *
 * @param button the held mouse button
 * @param x the current mouse position
 * @param y the current mouse poeition
 */
void EraserTool::MouseDragged(int button, int x, int y)
{}

/**
 * Invoked when the mouse pointer is moved within an active area
 *
 * @param x the current mouse position
 * @param y the current mouse position
 */
void EraserTool::MouseMoved(int x, int y)
{}

