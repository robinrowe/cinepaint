/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      png_plugin - png load/save plugin
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: png_plugin.cpp,v 1.11 2006/12/18 08:21:22 robinrowe Exp $
 */

//#include "PNGAPI.h"
#include "png_plugin.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include "plug-ins/pdb/CinePaintImage.h"
#include "app/CinePaintApp.h"
#include "plug-ins/pdb/CPPluginUIProgress.h"
#include <png.h>
#include <cmath>

// number of signature bytes to check
static const size_t PNG_BYTES_TO_CHECK = 4;

static const CinePaintPlugin::CPPluginParamDef LOAD_INPUT_PARAM_DEFS[] = { {PDB_CSTRING, "filename", "png file to load", false} };
static const CinePaintPlugin::CPPluginParamDef LOAD_RETURN_PARAM_DEFS[] = { {PDB_CPIMAGE, "image", "the loaded png image", false} };

//-------------------------
// Standard Plugin-In Setup

/**
 * Main entry point into the plugin
 * Creates and returns a new PNGLoadPlugin object for loading PNG image data
 *
 */
extern "C" CINEPAINT_PNG_API CinePaintPlugin* CreatePNGLoadObject()
{
	return(new PNGLoadPlugin());
}

/**
 * Factory delete method to safely delete the specified PNGLoadPlugin
 */
extern "C" CINEPAINT_PNG_API void DeletePNGLoadObject(CinePaintPlugin* plugin)
{
	delete plugin;
	plugin = 0;
}

/** Null terminated list of the plugin factory-create entry points provided by this plugin */
static CinePaintPlugin::PFNCreateCinePaintPlugin PluginClassList[] = {CreatePNGLoadObject, 0};

/**
 * Returns the list of plugin factory-create entry points provided by this plugin
 */
extern "C" CINEPAINT_PNG_API CinePaintPlugin::PFNCreateCinePaintPlugin* GetPluginClassList()
{
	return(PluginClassList);
}

/**
 * Plugin Load info describing this plugin and how to create an instance
 * Inserted into the CinePaint plugin database
 */
static CinePaintFileHandlerInfo load_info(CreatePNGLoadObject, "file_png_load", "png", "", "");




//-------------------------
// Constructor & Destructor

/**
 * Constructs a new PNGLoadPlugin to load PNG image data 
 */
PNGLoadPlugin::PNGLoadPlugin()
		: CinePaintPlugin("file_png_load", "loads image files in PNG format", "loads image files in PNG format", "Colin Law", "Colin Law", "2005")
{}

/**
 * Destructor
 */
PNGLoadPlugin::~PNGLoadPlugin()
{}



//------------------------
// Standard plugin methods

/**
 * Rgisters this plugin within the CinePaint plugin database
 *
 */
void PNGLoadPlugin::query(World* world)
{
	//FIXME
	//CinePaintApp::GetInstance().GetPluginDB().RegisterLoadHandler(&load_info);
}


/**
 * Returns a function pointer to the factory delete function for this CinePaintPlugin
 * Each CinePaintPlugin contains an entry point into the plugin in order to create a new
 * instance of that plugin. Each plugin should therefore provide an associated delete
 * factory method to safely and correctly delete an instance of a plugin created via
 * the create function
 *
 * @return the facory delete function pinter for this plugin
 */
CinePaintPlugin::PFNDeleteCinePaintPlugin PNGLoadPlugin::GetDeleteFunc()
{
	return(DeletePNGLoadObject);
}





//-----------
// Plugin RUN

/**
 * Runs this plugin.
 * This implementation attempts to load an image in PNG format.
 * This method expects a filename to load passed via the
 * CPParamList mechanism of CinePaintPlugin, and will return a new
 * CinePaintImage representing the loaded PNG image data vis the return
 * CPParamList.
 *
 * @param in plugin input parameter list
 * @param ret plugin return parameter list, populated with plugin return values
 */
void PNGLoadPlugin::run(CPParamList& in, CPParamList& ret)
{
	char* _filename;
	in.GetParam("filename", &_filename);

	if(_filename)
	{
		CPPluginUIProgress* _progress = new CPPluginUIProgress("file_load_png", "Loading...", 100, 1);

		CinePaintImage* _img = load_png(_filename, *_progress);
		if(_img)
		{
			CPPluginArg _arg;
			_arg.pdb_cpimage = _img;
			ret.AddParam("image", &_arg, PDB_CPIMAGE);
		}
		_progress->Hide();
		delete _progress;
	}
	else
	{
		printf("PNGLoadPlugin: no file name specified\n");
	}
}




/**
 * Returns the parameter definitions of expected inputs to this CinePaintPlugin
 *
 * @param param_defs to be populated with the input parameter definitions of this plugin
 * @return param_defs the input parameters of this plugin
 */
CinePaintPlugin::CPPluginParamDefs& PNGLoadPlugin::GetInParamDefs(CPPluginParamDefs& param_defs) const
{
	param_defs.m_params = LOAD_INPUT_PARAM_DEFS;
	param_defs.m_param_count = sizeof(LOAD_INPUT_PARAM_DEFS) / sizeof(CPPluginParamDef);
	return(param_defs);
}

/**
 * Returns the parameters definitions of the parameters returned by this CinePaintPlugin
 *
 * @param param_defs to be populated with the return parameter definitions of this plugin
 * @return param_defs the return parameters of this plugin
 */
CinePaintPlugin::CPPluginParamDefs& PNGLoadPlugin::GetReturnParamDefs(CPPluginParamDefs& param_defs) const
{
	param_defs.m_params = LOAD_RETURN_PARAM_DEFS;
	param_defs.m_param_count = sizeof(LOAD_INPUT_PARAM_DEFS) / sizeof(CPPluginParamDef);
	return(param_defs);
}


/**
 * private worker method to perform the PNG file loading
 *
 * @param filename the PNG file to load
 * @param progress ui progress meter to be updated as the image is loaded
 * @return a new CinePaintImage representing the loaded file
 * @todo handle and indicate error/failure states
 *       - how do plugins in general handle this?
 */
CinePaintImage* PNGLoadPlugin::load_png(const char* filename, CPPluginUIProgress& progress)
{
	// open the specified file
	FILE* _fp = 0;

	progress.SetText("Opening Image File");
	if((_fp = fopen(filename, "rb")) == 0)
	{
		// failed to open file
		printf("PngLoadPlugin: Failed to open image file %s\n", filename);
		return(0);
	}

	// check its a png
	progress.SetText("Checking File Signature");
	png_byte _chk_buf[PNG_BYTES_TO_CHECK];
	if(fread(_chk_buf, 1, PNG_BYTES_TO_CHECK, _fp) != PNG_BYTES_TO_CHECK)
	{
		// failed to read file
		printf("PngLoadPlugin: Failed to read check bytes from image file %s\n", filename);
		fclose(_fp);
		return(0);
	}

	if(png_sig_cmp(_chk_buf, (png_size_t)0, PNG_BYTES_TO_CHECK))
	{
		printf("PngLoadPlugin: Image does not appear to be a valid PNG %s\n", filename);
		fclose(_fp);
		return(0);
	}



	//
	// Setup
	//

	progress.SetText("Initializing...");

	// create the appropriate png and info structures
	png_structp _png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
	if(!_png_ptr)
	{
		fclose(_fp);
		return(0);
	}

	png_infop _info_ptr = png_create_info_struct(_png_ptr);
	if(!_info_ptr)
	{
		png_destroy_read_struct(&_png_ptr, (png_infopp)NULL, (png_infopp)NULL);
		fclose(_fp);
		return(0);
	}

	png_infop _end_info = png_create_info_struct(_png_ptr);
	if(!_end_info)
	{
		png_destroy_read_struct(&_png_ptr, &_info_ptr, (png_infopp)NULL);
		fclose(_fp);
		return(0);
	}

	// handle errors - yuck!
	if(setjmp(png_jmpbuf(_png_ptr)))
	{
		// free the png and info structs, close our file and return
		png_destroy_read_struct(&_png_ptr, &_info_ptr, &_end_info);
		fclose(_fp);
		return(0);
	}


	//
	// initilization
	//

	// initialize the png io
	png_init_io(_png_ptr, _fp);

	// tell libpng that we have already read some signature data
	png_set_sig_bytes(_png_ptr, PNG_BYTES_TO_CHECK);

	png_read_info(_png_ptr, _info_ptr);

	// apply tranformations
	// @TODO [claw] will need to review these

	// transofmr greyscale data < 8 bits to 8 bit data
	if((png_get_color_type(_png_ptr, _info_ptr) == PNG_COLOR_TYPE_GRAY) && (png_get_bit_depth(_png_ptr, _info_ptr) < 8))
	{
		png_set_expand_gray_1_2_4_to_8(_png_ptr);
	}

	// if there is trnaparency data in a tRNS chunk, add a full alpha channel
	if(png_get_valid(_png_ptr, _info_ptr, PNG_INFO_tRNS))
	{
		png_set_tRNS_to_alpha(_png_ptr);
	}

	// expand packed pixels to occupy a full byte without changing the pixel value
	if(png_get_bit_depth(_png_ptr, _info_ptr) < 8)
	{
		png_set_packing(_png_ptr);
	}

	// tell libpng tp handle interlacing
	// _number_of_passes is the number of passes we need to handle interlacing
	// if the image is not interlaced, this is just 1
	int _number_of_passes = png_set_interlace_handling(_png_ptr);

	// finshed setting transforms etc, tell libpng to update out struct
	png_read_update_info(_png_ptr, _info_ptr);


	//
	// Read the image
	//

	progress.ResetProgress(static_cast<double>(_number_of_passes * png_get_image_height(_png_ptr, _info_ptr)), 1.0);
	progress.SetText("Reading Image...");

	// first setup the tag
	CinePaintTag _tag;
	if(!set_tag_from_png_info(_png_ptr, _info_ptr, _tag))
	{
		// invalid tag
		png_destroy_read_struct(&_png_ptr, &_info_ptr, &_end_info);
		fclose(_fp);
		return(0);
	}

	// create an AbstractBuf to hold tha image data
	std::auto_ptr<AbstractBuf> _image = AbstractBufFactory::CreateBuf(_tag, png_get_image_width(_png_ptr, _info_ptr), png_get_image_height(_png_ptr, _info_ptr), AbstractBufFactory::STORAGE_FLAT);
	AbstractBuf* _img = _image.release();
	_img->RefPortionReadWrite(0, 0);

	if(!_img)
	{
		png_destroy_read_struct(&_png_ptr, &_info_ptr, &_end_info);
		fclose(_fp);
		return(0);
	}

	// setup the image row buffers to read data into
	unsigned char* _pixels = _img->GetPortionData(0, 0);
	unsigned char** _rows = new unsigned char*[png_get_image_height(_png_ptr, _info_ptr)];

	for(png_uint_32 i = 0; i < png_get_image_height(_png_ptr, _info_ptr); i++)
	{
		_rows[i] = _pixels + (png_get_image_width(_png_ptr, _info_ptr) * i * _tag.GetBytes());
	}

	// loop to handle interlacing
	// if thw image isn't interlaced, we just loop the once
	for(int i = 0; i < _number_of_passes; i++)
	{
		for(png_uint_32 j = 0; j < png_get_image_height(_png_ptr, _info_ptr); j++)
		{
			// read each row stright into our abstract buf
			png_read_row(_png_ptr, _rows[j], 0);
			progress.UpdateProgress();
		}
	}

	// finished with the file
	png_read_end(_png_ptr, _info_ptr);
	png_destroy_read_struct(&_png_ptr, &_info_ptr, &_end_info);

	fclose(_fp);

	// Put the result image into the return args list.
	CinePaintImage* _ret_img = new CinePaintImage(_img);
	_ret_img->SetFileName(filename);

	return(_ret_img);
}

/**
 * Sets the specified CinePaintTag depending upon the details from the PNG file
 *
 * @param png_ptr the png  struct
 * @param info PNG info struct detailing the PNG file to read
 * @param tag the CinePaintTag to set
 * @return true if the info describes an valid image format handled by this this plugin,
 *         false otherwise
 */
bool PNGLoadPlugin::set_tag_from_png_info(png_structp png_ptr, png_infop info, CinePaintTag& tag)
{
	bool _ret = true;

	if(png_get_bit_depth(png_ptr, info) == 16)
	{
		tag.SetPrecision(CinePaintTag::PRECISION_U16_ENUM);
	}
	else
	{
		tag.SetPrecision(CinePaintTag::PRECISION_U8_ENUM);
	}

	switch(png_get_color_type(png_ptr, info))
	{
			case PNG_COLOR_TYPE_RGB:
			{
				tag.SetFormat(CinePaintTag::FORMAT_RGB_ENUM);
				tag.SetAlpha(false);
				break;
			}
			case PNG_COLOR_TYPE_RGB_ALPHA:
			{
				tag.SetFormat(CinePaintTag::FORMAT_RGB_ENUM);
				tag.SetAlpha(true);
				break;
			}
			case PNG_COLOR_TYPE_GRAY:
			{
				tag.SetFormat(CinePaintTag::FORMAT_GRAY_ENUM);
				tag.SetAlpha(false);
				break;
			}
			case PNG_COLOR_TYPE_GRAY_ALPHA:
			{
				tag.SetFormat(CinePaintTag::FORMAT_GRAY_ENUM);
				tag.SetAlpha(true);
				break;
			}
			case PNG_COLOR_TYPE_PALETTE:
			{
				tag.SetFormat(CinePaintTag::FORMAT_INDEXED_ENUM);
				tag.SetAlpha(false);
				break;
			}
			default:
			{
				_ret = false;
				printf("PNGLoadPlugin: Unknown color model\n");
			}
	};

	return(_ret);
}
