/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      png_plugin - png load/save plugin
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004-2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: png_plugin.h,v 1.5 2006/12/18 08:21:22 robinrowe Exp $
 */
#ifndef _PNG_PLUGIN_H_
#define _PNG_PLUGIN_H_

#include "plug-ins/pdb/CinePaintPlugin.h"
#include <png.h> // req for png_infop

class CinePaintImage;
#include "plug-ins/pdb/CinePaintTag.h"
class CPPluginUIProgress;

// @TODO implement the save handler

/**
 * CinePaint Load plugin for handling PNG image date
 * The run method should be called upon this plugin to load image data.
 * This plugin implementation expects a filename to load passed via the
 * CPParamList mechanism of CinePaintPlugin, and will return a new
 * CinePaintImage representing the loaded PNG image data vis the return
 * CPParamList.
 *
 */
class PNGLoadPlugin : public CinePaintPlugin
{
	public:
		/**
		 * Constructs a new PNGLoadPlugin to load PNG image data 
		 */
		PNGLoadPlugin();

		/**
		 * Destructor
		 */
		virtual ~PNGLoadPlugin();

		/**
		 * Rgisters this plugin within the CinePaint plugin database
		 *
		 */
		virtual void query(World* world);

		/**
		 * Returns a function pointer to the factory delete function for this CinePaintPlugin
		 * Each CinePaintPlugin contains an entry point into the plugin in order to create a new
		 * instance of that plugin. Each plugin should therefore provide an associated delete
		 * factory method to safely and correctly delete an instance of a plugin created via
		 * the create function
		 *
		 * @return the facory delete function pinter for this plugin
		 */
		virtual PFNDeleteCinePaintPlugin GetDeleteFunc();

		/**
		 * Runs this plugin.
		 * This implementation attempts to load an image in PNG format.
		 * This method expects a filename to load passed via the
		 * CPParamList mechanism of CinePaintPlugin, and will return a new
		 * CinePaintImage representing the loaded PNG image data vis the return
		 * CPParamList.
		 *
		 * @param in plugin input parameter list
		 * @param ret plugin return parameter list, populated with plugin return values
		 */
		virtual void run(CPParamList& in, CPParamList& ret);

		/**
		 * Returns the parameter definitions of expected inputs to this CinePaintPlugin
		 *
		 * @param param_defs to be populated with the input parameter definitions of this plugin
		 * @return param_defs the input parameters of this plugin
		 */
		virtual CPPluginParamDefs& GetInParamDefs(CPPluginParamDefs& param_defs) const;

		/**
		 * Returns the parameters definitions of the parameters returned by this CinePaintPlugin
		 *
		 * @param param_defs to be populated with the return parameter definitions of this plugin
		 * @return param_defs the return parameters of this plugin
		 */
		virtual CPPluginParamDefs& GetReturnParamDefs(CPPluginParamDefs& param_defs) const;

	protected:

	private:
		/**
		 * private worker method to perform the PNG file loading
		 *
		 * @param filename the PNG file to load
		 * @param progress ui progress meter to be updated as the image is loaded
		 * @return a new CinePaintImage representing the loaded file
		 * @todo handle and indicate error/failure states
		 *       - how do plugins in general handle this?
		 */
		CinePaintImage* load_png(const char* filename, CPPluginUIProgress& progress);

		/**
		 * Sets the specified CinePaintTag depending upon the details from the PNG file
		 *
		 * @param png_ptr the png  struct
		 * @param info PNG info struct detailing the PNG file to read
		 * @param tag the CinePaintTag to set
		 * @return true if the info describes an valid image format handled by this this plugin,
		 *         false otherwise
		 */
		bool set_tag_from_png_info(png_structp png_ptr, png_infop info, CinePaintTag& tag);

};

#endif /* _PNG_PLUGIN_H_ */
