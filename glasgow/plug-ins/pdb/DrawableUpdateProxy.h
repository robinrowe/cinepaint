/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintImage - Main image class.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: DrawableUpdateProxy.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _DRAWABLE_UPDATE_PROXY_H_
#define _DRAWABLE_UPDATE_PROXY_H_

#include "DrawableChangeListener.h"

class CinePaintImage;

/** rsr un-nested:
	* Nested class to monitor all managed Drawable within the LayerManager for updates.
	* This listener responds to Drawable change events and generates an image level
	* event based upon received Drawable events.
	* This listener exists so that changes within a managed Drawable, which effect this
	* CinePaintImage, are noticed, and forwarded to all registered listeners interested
	* in image level events only. We only need to register with the image rather than each
	* drawable that composes the image to receive change events
	*/
class DrawableUpdateProxy : public DrawableChangeListener
{
	public:
		DrawableUpdateProxy()
		{	m_image=0;
		}
		void SplitInit(CinePaintImage* image);
		virtual ~DrawableUpdateProxy()
		{}

		virtual void Updated(Drawable& drawable, int x, int y, int w, int h);

	protected:
	private:
		CinePaintImage* m_image;

}; /* DrawableUpdateProxy */

#endif

