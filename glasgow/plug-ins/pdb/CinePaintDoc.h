/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintDoc Abstract document Interface
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintDoc.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _CINEPAINTDOC_H_
#define _CINEPAINTDOC_H_

#include "dll_api.h"
#include "CinePaintPlugin.h"
#include <string>

/**
 * CinePaintDocList acts as the main management structure for holding all of the open images,
 * their drawables, data etc can all be accessed through this list.
 */
class CINEPAINT_CORE_INTERFACES_API CinePaintDoc
{
public:

	/** Defined Cosument Types */
	enum DocType { CINEPAINT_IMAGE_ENUM, CINEPAINT_DAG_ENUM, CINEPAINT_FRAMEMANAGER_ENUM };

	virtual ~CinePaintDoc()
	{}

	/**
	 * Returns the DocType of this CinePaintDoc
	 *
	 * @return the DocType of this CinePaintDoc
	 */
	DocType GetDocType() const
	{
		return(m_doc_type);
	}

	/**
	 * Sets implementation specific save arguments that are passed to save handling plugin. 
	 * This method must be overridden by subclasses to set implementation specific parameters that
	 * are passed to the save handler plugin.
	 *
	 * @param params paramaeter list to used to pass parameter to the save plugin
	 */
	virtual void SetSaveArgs(CPParamList& params) = 0;

	/**
	 * UnSets implementation specific save arguments, reclaiming ownership of any implementation specific resources
	 * This method must be overridden by subclasses to unset, or take back ownership of implementation specific
	 * parameters that were set in a call to SetSaveArgs.
	 *
	 * @param params paramaeter list that was used to pass parameter to the save plugin
	 */
	virtual void UnSetSaveArgs(CPParamList& params) = 0;

	/**
	 * Returns true if thie Image has been modified
	 *
	 * @return true if this image has been modified, false otherwise
	 */
	bool IsDirty() const
	{
		return(m_dirty);
	}
	/**
	 * Sets the Dirty state of this CinePaintImage.
	 * a Dirty CinePaintImage indicates that it has been modified since it was last saved.
	 *
	 * @param dirty set true if this CinePaintImage has been modified since lst saved
	 */
	void SetDirty(bool dirty)
	{
		m_dirty = dirty;
	}

	/**
	 * Returns the file name of this CinePaintDoc.
	 * If no filename is set, foe example if this CinePaintDoc was not constructed from file
	 * and has not yet been saved, 0 is returned.
	 *
	 * @return the filename of this ConePaintDoc, or 0 if no fiename is set
	 */
	const char* GetFileName() const
	{
		return(m_filename.empty() ? 0 : m_filename.c_str());
	}

	/**
	 * Sets the filename of this CionePaintDoc
	 *
	 * @param file_name the new filename of this CinePaintDoc
	 */
	void SetFileName(const char* file_name)
	{
		m_filename = std::string(file_name);
	}
	/**
	 * Returns true if a filename has been set upon this CinePaintDoc
	 *
	 * @return true if a filename has been set upon this CinePaintDoc, false otherwise
	 */
	bool HasFileName() const
	{
		return(!m_filename.empty());
	}
protected:
	/**
	 * Construtor
	 * The Id parameter is a unique identifier of this CinePaintDoc within CinePaint.
	 *
	 * @param file_name the orginal document file name, set to 0 if not created from file
	 * @param doc_type the DocType of this CinePaintDoc
	 */

	CinePaintDoc(const char* file_name, DocType doc_type)
	{
		m_filename = file_name == 0 ? "" : std::string(file_name);
		m_doc_type = doc_type;
		m_dirty = false;
	}

private:
	/** document type - our own type information */
	DocType m_doc_type;

	/** original filename */
	std::string m_filename;

	/** indicates if this CinePaintImage is dirty or not */
	bool m_dirty;


}; /* class CinePaintDoc */

#endif /* _CINEPAINTDOC_H_ */
