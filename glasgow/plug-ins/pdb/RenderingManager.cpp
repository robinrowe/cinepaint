/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      RenderingManager - Manages rendering
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: RenderingManager.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "RenderingManager.h"
#include "AbstractRenderer.h"
#include "CinePaintImage.h"
#include <algorithm>
#include <string>

const char* RenderingManager::DEFAULT_NAME = "Default Renderer";

/**
 * Constructs a new Renderingmanager for the specified CinePaintImage
 * The RenderingManager initially contains zero renderers
 *
 */
RenderingManager::RenderingManager(CinePaintImage& image)
: m_image(image)
{
	m_default_renderer = 0;
}

/**
 * Destructor - destroys all registered Renderers
 */
RenderingManager::~RenderingManager()
{
	// delete all the registered renderers
	RendererContainer_t::iterator _iter = m_renderers.begin();

	while(_iter != m_renderers.end())
	{
		RendererContainer_t::iterator _next = _iter;
		++_next;

		AbstractRenderer* _renderer = (*_iter).second;
		m_renderers.erase(_iter);

		if(_renderer)
		{
			delete _renderer;
			_renderer = 0;
		}

		_iter = _next;
	}
}

//---------------------
// Accessors / Mutators

/**
 * Sets the rendered image within each registered renderer invalid
 * Conveinience method for a group update.
 *
 */
void RenderingManager::SetInvalid()
{
	for(RendererContainer_t::const_iterator _citer = m_renderers.begin(); _citer != m_renderers.end(); ++_citer)
	{
		((*_citer).second)->SetInvalid();
	}
}

/**
 * Registers the specified renderer with this ReneringManager.
 * This renderingManager assumes responsibility for the renderer, and will destroy the resource
 * when this RenderingManager is destroyed.
 * If a renderer is already registered with the specified name, the renderer is not registered and false is returned.
 *
 * @param name the name to register the renderer against
 * @param renderer the AbstractRenderer to register
 */ 
bool RenderingManager::RegisterRenderer(const char* name, AbstractRenderer* renderer)
{
	bool _ret = false;

	if(m_renderers.find(std::string(name)) == m_renderers.end())
	{
		m_renderers.insert(RenderingContainerPair_t(std::string(name), renderer));
		_ret = true ;
	}

	return(_ret);
}

/**
 * Unregisters and returns the Renderer registered with the specified name
 * Ownership of the Renderer resource passes back to the caller.
 * If no Renderer is registered with the specified name, no firther action is taken.
 *
 * @param name the registered Renderer name.
 * @return the unregistered Renderer
 */
AbstractRenderer* RenderingManager::UnregisterRenderer(const char* name)
{
	AbstractRenderer* _ret = 0;

	RendererContainer_t::iterator _iter = m_renderers.find(std::string(name));

	if(_iter != m_renderers.end())
	{
		_ret = (*_iter).second;
		m_renderers.erase(_iter);

		// handle if the unregisterd renderer is the 'default' renderer
		if(_ret == m_default_renderer)
		{
			m_default_renderer = 0;
		}
	}

	return(_ret);
}

/**
 * returns the AbstractRenderer registered with the specified name.
 * If no AbsractRenderer is registered, this method returns 0
 *
 * @param name the registered name of the AbstactRenderer to return
 * @return the AbstractRenderer registered against the specified name
 */
AbstractRenderer* RenderingManager::GetRenderer(const char* name) const
{
	AbstractRenderer* _ret = 0;

	RendererContainer_t::const_iterator _citer = m_renderers.find(std::string(name));

	if(_citer != m_renderers.end())
	{
		_ret = (*_citer).second;
	}

	return(_ret);
}

/**
 * Returns true if this Renderingmanager contains a renderer registered with the specified name.
 * If no renderer is found with the specified name, 0 is returned.
 *
 * @param name the name of the registered Renderer
 * @param the Renderer registered with the specified name, or 0 if no Renderer is registered
 */
bool RenderingManager::HasRenderer(const char* name) const
{
	return(m_renderers.find(std::string(name)) == m_renderers.end() ? false : true);
}

/**
 * Sets the default renderer of this RenderingManager.
 * If this Renderingmanager does not manage the specified Renderer, it is first registered
 * using RegisterRenderer with the name Renderingmanager::DEFAULT_NAME before being set as
 * the default renderer.
 *
 * @param renderer the renderer to set as the default renderer
 * @todo handle a default name clash
 */
void RenderingManager::SetDefaultRenderer(AbstractRenderer* renderer)
{
	// do we need to register the renderer?
	bool _found = false;
	for(RendererContainer_t::const_iterator _citer = m_renderers.begin(); _citer != m_renderers.end(); ++_citer) 
	{
		if((*_citer).second == renderer)
		{
			_found = true;
			break;
		}
	}

	if(!_found)
	{
		RegisterRenderer(RenderingManager::DEFAULT_NAME, renderer);
	}

	m_default_renderer = renderer;
}

/**
 * Returns the default renderer of thie renderingManager
 * If no default Renderer is set, 0 is returned.
 *
 * @return the default renteror
 */
AbstractRenderer* RenderingManager::getDefaultRenderer() const
{
	return(m_default_renderer);
}
