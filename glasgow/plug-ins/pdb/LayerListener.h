/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintImage - Main image class.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: LayerListener.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _LAYER_LISTENER_H_
#define _LAYER_LISTENER_H_

class CinePaintImage;

//rsr: 
// Nested Classes are evil. Unnested.

/**
	* Nested class to monitor and respond internally to changes within the LayerManager.
	* This is required because the LayerManager is in fact separate, so we need to be informed
	* of various changes within the LayerManager.
	*
	*/
class LayerListener 
:	public LayerChangeListener
{
	public:
		LayerListener()
		{	m_image=0;
		}
		void SplitInit(CinePaintImage* image)
		{	m_image=image;
		}
		virtual ~LayerListener()
		{}
		virtual void LayerAdded(Layer& layer, int position);
		virtual void LayerRemoved(Layer& layer);

	protected:
	private:
		CinePaintImage* m_image;

}; /* LayerListener */

#endif

