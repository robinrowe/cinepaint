/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintImage - Main image type
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintImage.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "CinePaintImage.h"
#include "AbstractBufFactory.h"
#include "ImageChangeListener.h"
#include "RenderingManager.h"
#include "SelectionManager.h"
#include "CinePaintTag.h"

/**
 * Constructs a new CinePaintImage of the specified size and data type.
 * The constructed CinePaintimage initially has zero layers.
 *
 * @param tag the image type CinePaintTag
 * @param width the width of the new Image
 * @param height the height of the new Image
 */
CinePaintImage::CinePaintImage(const CinePaintTag& tag, int width, int height)
:	CinePaintDoc(0, CinePaintDoc::CINEPAINT_IMAGE_ENUM)
/*, 
	m_layer_manager(), 
	m_channel_manager(), 
	m_layer_listener(), 
	m_drawable_listener()*/
{	m_layer_listener.SplitInit(this);
	m_drawable_listener.SplitInit(this);
	initialize(tag, width, height);
}

/**
 * Constructs a new CinePaintImage of the specified size and data type
 * The constructed CinePaintimage initially has zero layers.
 *
 * @param filename the filename this CinePaintImage is loaded from/saved to
 * @param tag the image type Tag
 * @param width the width of the new Image
 * @param height the height of the new Image
 */
CinePaintImage::CinePaintImage(const char* filename, const CinePaintTag& tag, int width, int height)
:	CinePaintDoc(filename, CinePaintDoc::CINEPAINT_IMAGE_ENUM)/*, 
	m_layer_manager(*this), 
	m_channel_manager(*this), 
	m_layer_listener(*this), 
	m_drawable_listener(*this)*/
{	m_layer_listener.SplitInit(this);
	m_drawable_listener.SplitInit(this);
	initialize(tag, width, height);
}

CinePaintImage::CinePaintImage(AbstractBuf *buf)
:CinePaintDoc(0, CinePaintDoc::CINEPAINT_IMAGE_ENUM)
	/*, m_layer_manager(*this), 
	m_channel_manager(*this), 
	m_layer_listener(*this), 
	m_drawable_listener(*this)
	*/
{	m_layer_listener.SplitInit(this);
	m_drawable_listener.SplitInit(this);
	initialize(buf->GetTag(), buf->GetWidth(), buf->GetHeight());
	Layer *_layer = new Layer(*buf, "background");
	m_layer_manager.AddLayer(_layer, 0, true);
}

/**
 * Destructor releases all image components, layers, channels.
 */
CinePaintImage::~CinePaintImage()
{
	if(m_rendering_manager)
	{
		delete m_rendering_manager;
		m_rendering_manager = 0;
	}
}




//---------------------
// Accessors / Mutators

/**
 * Returns the Tag indicating the data format of this CinePaintImage
 *
 * @return the Tag indicating the data format of this CinePaintImage
 */
const CinePaintTag& CinePaintImage::GetTag() const
{
	return(m_tag);
}

CinePaintTag& CinePaintImage::GetTag()
{
	return(m_tag);
}

/**
 * Returns the Width of this CinepaintImage
 *
 * @return the width of this CinePaintImage
 */
int CinePaintImage::GetWidth() const
{
	return(m_width);
}

/**
 * Returns the height of this CinePaintImage
 *
 * @retunr the height of this CinePaintImage
 */
int CinePaintImage::GetHeight() const
{
	return(m_height);
}


/**
 * Resets the width, height and tag of this CinePaintImage to the specified values.
 * This is a convenience method for reuse if this CinePaintImage object, returning the
 * object to its initial state with zero layers.
 * Any managed Layers are destroyed
 *
 * @param tag the image type CinePaintTag
 * @param width the width of the new Image
 * @param height the height of the new Image
 */
void CinePaintImage::Reset(const CinePaintTag& tag, int width, int height)
{
	Layer* _layer = 0;
	while((_layer = GetLayers().GetForegroundLayer()) != 0)
	{
		GetLayers().RemoveLayer(_layer);
		delete _layer;
		_layer = 0;
	}
	
	m_tag = tag;
	m_width = width;
	m_height = height;
}

/**
 * Requests that this CinePaintImage is re-drawn on screen
 * This method indicates that the specified area requires redrawn, as such, an event is
 * generated for handling classes to repaint themselves as required.
 *
 * @param x the x coordinate of the update region
 * @param y the y coordinate of the update region
 * @param w the width of the update region
 * @param h the height of the update region
 */
void CinePaintImage::Update(int x, int y, int w, int h)
{
	GetRenderingManager().SetInvalid();
	for(ListenerList_t::const_iterator _citer = m_image_listener_list.begin(); _citer != m_image_listener_list.end(); ++_citer)
	{
		(*_citer)->ImageUpdated(x, y, w, h);
	}
}

//----------------------------
// Render access functions

/**
 * Returns the RenderingManager for this CinePaintImage.
 * The RenderingManagers allows various renderors to be added to produce a 'final'
 * of various stages of a rendered (ie composed) version of this CinePaintImage
 *
 * @return the RenderingManager for this CinePaintImage
 */
RenderingManager& CinePaintImage::GetRenderingManager()
{
	if(!m_rendering_manager)
	{
		m_rendering_manager = new RenderingManager(*this);
	}

	return(*m_rendering_manager);
}



//--------------------
// Convenience Methods 

/**
 * Returns true if this CinePaintImage is flat.
 * A Flat CinePaintImage has;
 *  - Exactly one Layer which
 *    - Matches the dimensions of this CinePaintImage
 *    - has an offset of zero
 *    - has no alpha
 *    - has no layer selection mask
 *    - has no auxiliary channels
 *    - has an opacity value of 1.0
 *
 * @return true if this CinePaintImage is flat, false otherwise
 */
bool CinePaintImage::IsFlat() const
{
	bool _flat = false;

	if(m_layer_manager.GetLayerCount() == 1)
	{
		Layer* _layer = m_layer_manager.GetBackgroundLayer();

		if(!(_layer->GetSelectionManager().HasSelection()))
		{
			if(_layer->GetOpacity() == 1.0)
			{
				if((_layer->GetOffsetX() == 0) && (_layer->GetOffsetY() == 0) && (_layer->GetWidth() == this->GetWidth()) && (_layer->GetHeight() == this->GetHeight()))
				{
					if(( ! _layer->HasAlpha()) && (m_channel_manager.GetChannelCount() == 0))
					{
						_flat = true;
					}
				}
			}
		}
	}

	return(_flat);
}

/**
 * Returns true if thie CinePaintImage has zero Layers
 *
 * @return true if this CinePaintImage has zero Layers, false otherwise
 */
bool CinePaintImage::IsEmpty() const
{
	return((m_layer_manager.GetLayerCount() > 0) ? true : false);
}

//---------------
// layer handling

/**
 * Returns the LayerManager managing the layers within this CinePaintImage
 * The Returned LayerManager provides access to adding and removing layers from
 * this CinePaintImage.
 *
 * @note [claw] I have deliberatly kept the layer hendling separate from any possible 'image'
 *       methods for clarity. If this proves to be unneeded, we can simply collapse this added
 *       Layer of class.
 * @return the LayerManager handling image layer within this CinePaintImage
 */
LayerManager& CinePaintImage::GetLayers()
{
	return(m_layer_manager);
}

/**
 * Convenience method to add a Layer to this CinePaintImage from the specified Buffer of image data
 * The new Layers is added as the foreground of this CinePaintImage, and is set as the
 * currently active Layer.
 * Full Control for Layer handling is provided via the LayerManager returned from
 * GetLayers()
 * The CinePaintTag of the specified AbstractBuf must match that of this CinePaintImage
 *
 * @param buf the image data buffer
 * @param name the name of the new Layer
 * @return true if the addition was a success
 */
bool CinePaintImage::AddLayer(AbstractBuf& buf, const char* name)
{
	bool _ret = false;

	if(buf.GetTag() == this->GetTag())
	{
		Layer* _layer = new Layer(buf, name);

		m_layer_manager.AddLayer(_layer, 0, true);
		_ret = true;
	}

	return(_ret);
}

/**
 * Convenience method to add a Layer to this CinePaintImage.
 * The new Layers is added as the foreground of this CinePaintImage, and is set as the
 * currently active Layer.
 * Full Control for Layer handling is provided via the LayerManager returned from
 * GetLayers()
 * The CinePaintTag of the specified Layer must match that of this CinePaintImage
 *
 * @param layer the layer to add
 * @return true if the addition was a success
 */
bool CinePaintImage::AddLayer(Layer* layer)
{
	bool _ret = false;
	if ( layer->GetBuffer().GetTag() == this->GetTag() )
	{
		m_layer_manager.AddLayer(layer, 0, true);
		_ret = true;
	}
	return _ret;
}

//-----------------
// Channel Handling

/**
 * Returns the ChannelManager managing the Channels within this CinePaintImage
 * The Returned ChannelManager provides access to adding and removing Channels from
 * this CinePaintImage.
 *
 * @return the ChannelManager handling Channels within this CinePaintImage
 */
ChannelManager& CinePaintImage::GetChannels()
{
	return(m_channel_manager);
}


//============================================
// Parasite/Extra(user defined) data handling.

/** 
 * Returns a pointer to the ParasiteList
 * @return the parasite list.
**/
CPParamList *CinePaintImage::GetParasiteList()
{
	if (!m_parasiteList)
		m_parasiteList = new CPParamList;
	return (m_parasiteList);
}


//-------------------
// Listener handling

/**
 * Registers the specified ImageChangeListener to receive image change notifications
 * from this CinePaintImage
 *
 * @param icl the ImageChangeListener to add
 */
void CinePaintImage::AddImageChangeListener(ImageChangeListener* icl)
{
	m_image_listener_list.AddListener(icl);
}

/**
 * Removes the specified ImageChangeListener from those registered as receiving
 * Image change notifications
 *
 * @param icl the ImageChangeListener to remove
 */
void CinePaintImage::RemoveImageChangeListener(ImageChangeListener* icl)
{
	m_image_listener_list.RemoveListener(icl);
}

/**
 * Second phase constructor
 *
 * @param tag Data type tag
 * @param width the width of this CinePaintImage
 * @param height the height of this CinePaintImage
 */
void CinePaintImage::initialize(const CinePaintTag& tag, int width, int height)
{
	m_tag = tag;
	m_width = width;
	m_height = height;
	m_rendering_manager = 0;
	m_parasiteList = 0;

	// register to receive notification of layer changes
	// we need to do this because although closely related, the LayerManager is actually separately hanled 
	m_layer_manager.AddLayerChangeListener(&m_layer_listener);
}


/**
 * Sets implementation specific save arguments that are passed to save handling plugin. 
 * This method must be overridden by subclasses to set implementation specific parameters that
 * are passed to the save handler plugin.
 *
 * @param params paramaeter list to used to pass parameter to the save plugin
 */
void CinePaintImage::SetSaveArgs(CPParamList& params)
{
	//This is where we need to set the paramters for the plugin to 
	// save the data for this document.
	CPPluginArg arg;
	arg.pdb_cpimage = this;
	params.AddParam("image", &arg, PDB_CPIMAGE);

	// @TODO : Need to add Parasites into save param list.
}

/**
 * UnSets implementation specific save arguments, reclaiming ownership of any implementation specific resources
 * This method must be overridden by subclasses to unset, or take back ownership of implementation specific
 * parameters that were set in a call to SetSaveArgs.
 *
 * @param params paramaeter list that was used to pass parameter to the save plugin
 */
void CinePaintImage::UnSetSaveArgs(CPParamList& params)
{
	// reclaim this image from the paramlist, otherwise it is released when the param list is released

	CPPluginArg arg;
	if(params.RemoveParam("image", arg, PDB_CPIMAGE) != 1)
	{
		printf("Failed to reclaim image from save handler param list!\n");
	}
}

