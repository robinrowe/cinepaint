/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      SelectionManager - Manages selections on images
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: SelectionManager.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */
 
#ifndef _SELECTION_MANAGER_H_
#define _SELECTION_MANAGER_H_

#include "dll_api.h"
#include <list>

class AbstractBuf;
class AbstractSelectionShape;
class Drawable;
class CPRect;

/**
 * SelectionManager manages the selections made within a Drawable
 * SelectionManager attempts to maintain a list of selection shapes representing
 * the selected areas of a Drawable, providing iteration over the individual shapes
 * and allowing access to the selection mask.
 *
 * @TODO the idea is that we can contain relatively small shapes, without having to create
 *       the whole selection mask until we are actually required to do so, we can simply
 *       return the shape (often, this may be a simply rectangle)
 *       this (obviously) needs some more thought... how do we subtract areas etc?
 */
class CINEPAINT_IMAGE_API SelectionManager
{
	public:
		/**
		 * Constructs a new SelectionManager to manage selection made over the specified Drawable
		 *
		 * @param Drawable the drawable selection are made upon
		 */
		SelectionManager(Drawable& drawable);

		/**
		 * Destructor
		 */
		virtual ~SelectionManager();


		//---------------------
		// Accessors / Mutators

		/**
		 * Returns a rectangle representing the selection bounds of all selection.
		 *
		 * @param bounds the rectangle to set to the current selection bounds
		 * @return bounds set to the current selection bounds
		 */
		CPRect& GetSelectionBounds(CPRect& bounds) const;

		/**
		 * Adds the specified rectangle to the current selection
		 *
		 * @param x top left coordinate of the selection rectangle
		 * @param y top left coordinate of the selection rectangle
		 * @param w the width of the selection rectangle
		 * @param h the height of the selection rectangle
		 */
		void AddSelectionRect(unsigned int x, unsigned int y, unsigned int w, unsigned int h);
		
		/**
		 * Adds the specified selection shape to the current selection
		 *
		 * @param selection selection shape representing the newly selected area
		 */
		void AddSelection(AbstractSelectionShape* selection);

		/**
		 * Update current selection with new data 
		 * @param x top left coordinate of the selection rectangle
		 * @param y top left coordinate of the selection rectangle
		 * @param w the width of the selection rectangle
		 * @param h the height of the selection rectangle
		 * updates the element on top of the stack, i.e. the last add selection, if no selection 
		 * exists a new one is added.
		**/
		void UpdateSelectionRect(unsigned int x, unsigned int y, unsigned int w, unsigned int h);

		/**
		 * Update current selection with new data 
		 * @param selection selection shape representing modified area.
		 * updates the element on top of the stack, i.e. the last add selection.
		**/
		void UpdateSelection(AbstractSelectionShape* selection);

		/**
		 * Returns trus if this Selection manager contains a valid selection
		 * A valid selection is at least one sdelection area that is not empty.
		 *
		 * @return true if this selection manager contains a valid selection
		 */
		bool HasSelection() const;

		/**
		 * Renders the current selection shape objects as a selection mask and returns the mask
		 * The mask remains owned by this SelectionManager
		 *
		 * @return the selection mask of the current selections
		 */
		AbstractBuf& GetSelectionMask();

		/**
		 * Clears all the current selection
		 *
		 */
		void ClearSelection();

	protected:


	private:
		typedef std::list<AbstractSelectionShape*> SelectionContainer_t;

		/** list of selection shapes */
		SelectionContainer_t m_selections;

		/** the selection mask */
		AbstractBuf* m_selection_mask;

		/** the drawable we manage the selection over */
		Drawable& m_drawable;

}; /* class SelectionManager */

#endif /* _SELECTION_MANAGER_H_ */
