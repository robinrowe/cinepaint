/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintImage - Main image class.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintImage.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _CINEPAINT_IMAGE_H_
#define _CINEPAINT_IMAGE_H_

#include "dll_api.h"
#include "CinePaintDoc.h"
#include "AbstractBuf.h"
#include "CinePaintTag.h"
#include "ChannelManager.h"
#include "LayerManager.h"
#include "LayerChangeListener.h"
#include "Layer.h"
#include "LayerListener.h"
#include "DrawableUpdateProxy.h"
#include "ListenerList.h"

#include <algorithm>

// Forward declaration
class ImageChangeListener;
class RenderingManager;



/**
 * Main Image Compositing Class
 * CinePaintImage is the glue for holding various componets of an 'Image' together.
 * A CinePaintImage is composed of multiple Layers, Channels and associated Masks,
 * and selections
 *
 *
 * @TODO CinePaintImage needs separated from a Document - theres no reason to clutter the
 *       image class with any document information.
 */
class CINEPAINT_IMAGE_API CinePaintImage : public CinePaintDoc
{	friend class LayerListener;
	public:
		/**
		 * Constructs a new CinePaintImage of the specified size and data type.
		 * The constructed CinePaintimage initially has zero layers.
		 *
		 * @param tag the image type CinePaintTag
		 * @param width the width of the new Image
		 * @param height the height of the new Image
		 */
		CinePaintImage(const CinePaintTag& tag, int width, int height);

		/**
		 * Constructs a new CinePaintImage of the specified size and data type
		 * The constructed CinePaintimage initially has zero layers.
		 *
		 * @param filename the filename this CinePaintImage is loaded from/saved to
		 * @param tag the image type CinePaintTag
		 * @param width the width of the new Image
		 * @param height the height of the new Image
		 */
		CinePaintImage(const char* filename, const CinePaintTag& tag, int width, int height);

		/**
		 * Constructs a new CinePaintImage from an AbstractBuf.
		 * The new image has a background Layer(0) as the buffer given
		 * the size and type of the new images is the same as the AbstractBuf
		 *
		 * @param buf The buffer used to create the image.
		 */
		CinePaintImage(AbstractBuf *buf);		
		
		/**
		 * Destructor releases all image components, layers, channels.
		 */
		virtual ~CinePaintImage();




		//---------------------
		// Accessors / Mutators

		/**
		 * Returns the CinePaintTag indicating the data format of this CinePaintImage
		 *
		 * @return the CinePaintTag indicating the data format of this CinePaintImage
		 */
		const CinePaintTag& GetTag() const;
		CinePaintTag& GetTag();

		/**
		 * Returns the Width of this CinepaintImage
		 *
		 * @return the width of this CinePaintImage
		 */
		int GetWidth() const;

		/**
		 * Returns the height of this CinePaintImage
		 *
		 * @retunr the height of this CinePaintImage
		 */
		int GetHeight() const;

		/**
		 * Resets the width, height and tag of this CinePaintImage to the specified values.
		 * This is a convenience method for reuse if this CinePaintImage object, returning the
		 * object to its initial state with zero layers.
		 * Any managed Layers are destroyed
		 *
		 * @param tag the image type CinePaintTag
		 * @param width the width of the new Image
		 * @param height the height of the new Image
		 */
		void Reset(const CinePaintTag& tag, int width, int height);

		/**
		 * Requests that this CinePaintImage is re-drawn on screen
		 * This method indicates that the specified area requires redrawn, as such, an event is
		 * generated for handling classes to repaint themselves as required.
		 *
		 * @param x the x coordinate of the update region
		 * @param y the y coordinate of the update region
		 * @param w the width of the update region
		 * @param h the height of the update region
		 */
		void Update(int x, int y, int w, int h);


		//----------------------------
		// Render access functions

		/**
		 * Returns the RenderingManager for this CinePaintImage.
		 * The RenderingManagers allows various renderors to be added to produce a 'final'
		 * of various stages of a rendered (ie composed) version of this CinePaintImage
		 *
		 * @return the RenderingManager for this CinePaintImage
		 */
		RenderingManager& GetRenderingManager();

		//--------------------
		// Convenience Methods 

		/**
		 * Returns true if this CinePaintImage is flat.
		 * A Flat CinePaintImage has;
		 *  - Exactly one Layer which
		 *    - Matches the dimensions of this CinePaintImage
		 *    - has an offset of zero
		 *    - has no alpha
		 *    - has no layer selection mask
		 *    - has no auxiliary channels
		 *    - has an opacity value of 1.0
		 *
		 * @return true if this CinePaintImage is flat, false otherwise
		 */
		bool IsFlat() const;

		/**
		 * Returns true if thie CinePaintImage has zero Layers
		 *
		 * @return true if this CinePaintImage has zero Layers, false otherwise
		 */
		bool IsEmpty() const;

		//---------------
		// layer handling

		/**
		 * Returns the LayerManager managing the layers within this CinePaintImage
		 * The Returned LayerManager provides access to adding and removing layers from
		 * this CinePaintImage.
		 *
		 * @note [claw] I have deliberatly kept the layer hendling separate from any possible 'image'
		 *       methods for clarity. If this proves to be unneeded, we can simply collapse this added
		 *       Layer of class.
		 * @return the LayerManager handling image layer within this CinePaintImage
		 */
		LayerManager& GetLayers();

		/**
		 * Convenience method to add a Layer to this CinePaintImage from the specified Buffer of image data
		 * The new Layers is added as the foreground of this CinePaintImage, and is set as the
		 * currently active Layer.
		 * Full Control for Layer handling is provided via the LayerManager returned from
		 * GetLayers()
		 * The CinePaintTag of the specified AbstractBuf must match that of this CinePaintImage
		 *
		 * @param buf the image data buffer
		 * @param name the name of the new Layer
		 * @return true if the addition was a success
		 */
		bool AddLayer(AbstractBuf& buf, const char* name);

		/**
		 * Convenience method to add a Layer to this CinePaintImage.
		 * The new Layers is added as the foreground of this CinePaintImage, and is set as the
		 * currently active Layer.
		 * Full Control for Layer handling is provided via the LayerManager returned from
		 * GetLayers()
		 * The CinePaintTag of the specified Layer must match that of this CinePaintImage
		 *
		 * @param layer the layer to add
		 * @return true if the addition was a success
		 */
		bool AddLayer(Layer* layer);

		//-----------------
		// Channel Handling

		/**
		 * Returns the ChannelManager managing the Channels within this CinePaintImage
		 * The Returned ChannelManager provides access to adding and removing Channels from
		 * this CinePaintImage.
		 *
		 * @return the ChannelManager handling Channels within this CinePaintImage
		 */
		ChannelManager& GetChannels();

		
		//============================================
		// Parasite/Extra(user defined) data handling.

		/** 
		 * Returns a pointer to the ParasiteList
		 * @return the parasite list.
		**/
		CPParamList *GetParasiteList();


		//-------------------
		// Listener handling

		/**
		 * Registers the specified ImageChangeListener to receive image change notifications
		 * from this CinePaintImage
		 *
		 * @param icl the ImageChangeListener to add
		 */
		void AddImageChangeListener(ImageChangeListener* icl);

		/**
		 * Removes the specified ImageChangeListener from those registered as receiving
		 * Image change notifications
		 *
		 * @param icl the ImageChangeListener to remove
		 */
		void RemoveImageChangeListener(ImageChangeListener* icl);


		//---------------
		// Over-rides form CinePAintDoc..
		// This is for file handling.

		/**
		 * Sets implementation specific save arguments that are passed to save handling plugin. 
		 * This method must be overridden by subclasses to set implementation specific parameters that
		 * are passed to the save handler plugin.
		 *
		 * @param params paramaeter list to used to pass parameter to the save plugin
		 */
		virtual void SetSaveArgs(CPParamList& params);

		/**
		 * UnSets implementation specific save arguments, reclaiming ownership of any implementation specific resources
		 * This method must be overridden by subclasses to unset, or take back ownership of implementation specific
		 * parameters that were set in a call to SetSaveArgs.
		 *
		 * @param params paramaeter list that was used to pass parameter to the save plugin
		 */
		virtual void UnSetSaveArgs(CPParamList& params);

	protected:

	private:

		/**
		 * Second phase constructor
		 *
		 * @param tag Data type tag
		 * @param width the width of this CinePaintImage
		 * @param height the height of this CinePaintImage
		 */
		void initialize(const CinePaintTag& tag, int width, int height);


		//--------
		// members

		/** image format information */
		CinePaintTag m_tag;

		/** imagre width */
        int m_width;

		/** Image height */
		int m_height;


		//-----------------------
		// Layer/Channel handling

		/** Manages the layers that compose this CinePaintImage */
		LayerManager m_layer_manager;

		/** manages the Channels within this CinePaintImage */
		ChannelManager m_channel_manager;

		/** manages rendered versions of this CinePaintImage */
		RenderingManager* m_rendering_manager;



		//-----------------------
		// Parasite Data - used for things like ICC colour profiles.
		CPParamList *m_parasiteList;


		//------------------
		// Listener handling

		typedef ListenerList<ImageChangeListener> ListenerList_t;

		/** list of registered ImageChangeListener to be notified on Image changes */
		ListenerList_t m_image_listener_list;

		LayerListener m_layer_listener;

		DrawableUpdateProxy m_drawable_listener;

} ; /* class CinePaintImage */

#endif /* _CINEPAINT_IMAGE_H_ */
