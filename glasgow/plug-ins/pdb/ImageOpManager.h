/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ImageOpManager - Manage image operations.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ImageOpManager.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _CINEPAINT_IMAGE_OP_MANAGER_H_
#define _CINEPAINT_IMAGE_OP_MANAGER_H_

#include "dll_api.h"
#include "plug-ins/pdb/ListenerList.h"
#include "plug-ins/pdb/CinePaintImageOp.h"
#include <list>
#include <map>
#include <string>
#include "plug-ins/pdb/CinePaintTag.h"

// Forward Declarations
class CinePaintScaleOp;
class CinePaintFillOp;
class CinePaintCompositeOp;
class CinePaintRenderOp;
class CinePaintConversionOp;

/**
 * Manages the registered Core CinePaint Image Operations.
 * The ImageOpManager is responsible for managing CinePaintImageOps, handling their resource and providing access
 * to specific image ops for various image formats.
 *
 * CinePaintImageOps are registered with a known CinePaint application type/class string. There may be more than one
 * CinePaintImageOp for a particular type, specifically to handle different image formats. Each CinePaintImageOp is
 * registered with its type and this ImageOpManager may be queried to return a particular type CinePaintImageOps for
 * a specified format.
 *
 */
class CINEPAINT_CORE_API ImageOpManager
{	typedef std::multimap<std::string, CinePaintImageOp*> ImageOpContainer_t;
	/** container for all CinePaint tools */
	ImageOpContainer_t m_image_ops;
public:
	/**
		* Constructs a new CinePaintImageOp Manager initially managing no CinePaintImageOps
		*/
	ImageOpManager();

	/**
		* Destructor
		*/
	~ImageOpManager();

	/**
		* Initializes this ImageOpManager by loading available CinePaintImageOp plugins from an application specified path
		*
		*/
	void Initialize();



	//-----------------------------
	// CinePaintImageOp managerment

	/**
		* Register the specified CinePaintImageOp with this ImageOpManager under the spcified Image Op type string.
		* If the specified CinePaintImageOp is a 'core' application operation, one of the image op type strings
		* defined within CinePaintImageOp should be used.
		* This ImageOpManager assumes responsibilty for the resources of the registered CinePaintImageOp.
		*
		* @param op_type the image op type being registered
		* @param op the CinePaintImageOp to register
		*/
	void RegisterImageOp(const char* op_type, CinePaintImageOp* op);

	/**
		* Registers the specified CinePaintImageOp with this ImageOpManager
		* The CinePaintImageOp is registered with the image op type returned by the CinePaintImageOp itself.
		*
		* @param op the CinePaintImageOp to register
		*/
	void RegisterImageOp(CinePaintImageOp* op);

	//----------
	// Accessors

	/**
		* Returns a CinePaintImageOp of the specified type suitable for use with image data of the specified image type tag
		* If no suitable image op is found, for the speficified type suporting the specified CinePaintTag, 0 is returned
		*
		* @param op_type the image op type to return
		* @param tag the image data type tag the image op must support
		* @return a CinePaintImageOp of the specified type suitable for use on image data of the specifice image data type tag,
		*         or 0 if no suitable CinePaintImageOp can be found
		*/
	CinePaintImageOp* GetImageOp(const char* op_type, const CinePaintTag& tag) const;



	//------------------------------------------
	// Specific Named CinePaintImageOp Accessors

	/**
		* Returns a ScaleOp suitable for scaling image data of the specified CinePaintTag type
		* If no suitable ScaleOp is registered for the specified CinePaintTag type, 0 is returned
		*
		* @patam tag image data type
		* @return a ScaleOp suitable for scaling iage data of the specified CinePaintTag type, or 0
		*         if no suitable ScaleOp is found
		*/
	CinePaintScaleOp* GetScaleOp(const CinePaintTag& tag) const;

	CinePaintFillOp* GetFillOp(const CinePaintTag& tag) const;

	CinePaintCompositeOp* GetCompositeOp(const CinePaintTag& tag) const;

	CinePaintRenderOp* GetRenderOp(const CinePaintTag& tag) const;

	CinePaintConversionOp* GetConversionOp(const CinePaintTag& tag) const;


	static ImageOpManager* GetImageOpManager()
	{	static ImageOpManager* singleton;
// This works because a process only loads one copy of a dll data segment.
// (Note that static data won't share this way across processes.)
// rsr: lock here not necessary, loads plug-ins sequentially
		if(!singleton)
		{	singleton=new ImageOpManager;
		}
// unlock
		return singleton;
	}

	protected:

	private:


}; /* class ImageOpManager */

#endif /* _CINEPAINT_IMAGE_OP_MANAGER_H_ */
