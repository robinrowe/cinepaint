/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      plugin parameter data holder
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2005
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * $Id: PluginParam.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "PluginParam.h"
#include "AbstractBuf.h"
#include "Drawable.h"
#include "CinePaintImage.h"

/**
 * Constructors a new PluginParam, initially set to hold an int value of 0
 *
 */
PluginParam::PluginParam()
{
	// dummy values
	m_type = PDB_INT;
	m_value.pdb_int = 0;
	m_manage = false;
}

/**
 * Constructs a new PluginParam to hold the specified CPPluginArgType and CPPluginArg
 * Initially this PluginParam does not manage the set data
 *
 * @param type the data type to hold
 * @param val the data value to hold
 */
PluginParam::PluginParam(CPPluginArgType type, CPPluginArg val)
{
	m_manage = false;
	m_type = type;
	m_value = val;
}

/**
 * Destructor
 *
 */
PluginParam::~PluginParam()
{
	release_resources();
}


//---------------------
// Accessors / Mutators

/**
 * Returns the daat type held by this PluginParam
 *
 * @return the data type held by this PluginParam
 */
CPPluginArgType PluginParam::GetType() const
{
	return(m_type);
}

/**
 * Returns the data held by this PluginParam
 *
 * @return the data held by this PluginParam
 */
const CPPluginArg& PluginParam::GetArg() const
{
	return(m_value);
}

/**
 * Sets this PluginParam to hold the data held in the specified PluginParam.
 * The data is simply copied asis from the specified PluginParam to this PluginParam.
 *
 * @param param the PluginParam to set this PluginParam to
 */
void PluginParam::Set(const PluginParam& param)
{
	release_resources();
	m_type = param.m_type;
	m_value = param.m_value;
}

/**
 * Sets the data held in this PluginParam to the specified int value
 * If GetManaged() is true, any previously held data will be released fist.
 *
 * @param i the int value to be held by this PluginParam
 */
void PluginParam::Set(int i)
{
	release_resources();
	m_type = PDB_INT;
	m_value.pdb_int = i;
}

/**
 * Sets the data held in this PluginParam to the specified float value
 * If GetManaged() is true, any previously held data will be released fist.
 *
 * @param f the float value to be held by this PluginParam
 */
void PluginParam::Set(float f)
{
	release_resources();
	m_type = PDB_FLOAT;
	m_value.pdb_float = f;
}

/**
 * Sets the data held in this PluginParam to the specified string
 * the string is NOT copied, the pointer is held as is.
 * If GetManaged() is true, any previously held data will be released fist.
 *
 * @param c the string value to be held by this PluginParam
 */
void PluginParam::Set(char* c)
{
	release_resources();
	m_type = PDB_CSTRING;
	m_value.pdb_cstring = c;
}

/**
 * Sets the data held in this PluginParam to the specified AbstractBuf
 * If GetManaged() is true, any previously held data will be released fist.
 *
 * @param buf the AbstractBuf to be held by this PluginParam
 */
void PluginParam::Set(AbstractBuf* buf)
{
	release_resources();
	m_type = PDB_ABSTRACTBUF;
	m_value.pdb_abstractbuf = buf;
}

/**
 * Sets the data held in this PluginParam to the specified Drawable
 * If GetManaged() is true, any previously held data will be released fist.
 *
 * @param drawable the Drawable to be held by this PluginParam
 */
void PluginParam::Set(Drawable* drawable)
{
	release_resources();
	m_type = PDB_DRAWABLE;
	m_value.pdb_drawable = drawable;
}

/**
 * Sets the data held in this PluginParam to the specified CinePaintImage
 * If GetManaged() is true, any previously held data will be released fist.
 *
 * @param image the CinePaintImage to be held by this PluginParam
 */
void PluginParam::Set(CinePaintImage* image)
{
	release_resources();
	m_type = PDB_CPIMAGE;
	m_value.pdb_cpimage = image;
}

/**
 * Seys whether this PluginParam manages the held data resource
 * If this is set true, this PluginParam will release the resource
 * when this PluginParam is released, otherwise the data is left asis
 * and must be managed elsewhere by the user
 *
 * @param manage set true to manage the held data item
 */
void PluginParam::SetManaged(bool manage)
{
	m_manage = manage;
}

/**
 * Returns whether this PluginParam manages the held data item
 *
 * @return true if this PluginParam manages the held data item, false otherwise
 */
bool PluginParam::GetManaged() const
{
	return(m_manage);
}

/**
 * if GetManaged(), released the managed resource, otherwise simply returns
 *
 */
void PluginParam::release_resources()
{
	if(m_manage)
	{
		switch(m_type)
		{
				case PDB_INT:
				case PDB_FLOAT:
				{
					printf("release_resources(): releasing inbuilt type\n");
					break;
				}
				case PDB_CSTRING:
				{
					printf("release_resources(): releasing char*\n");
					delete[] m_value.pdb_cstring;
					m_value.pdb_cstring = 0;
					break;
				}
				case PDB_DRAWABLE:
				{
					printf("release_resources(): releasing drawable\n");
					delete m_value.pdb_drawable;
					m_value.pdb_drawable = 0;
					break;
				}
				case PDB_CPIMAGE:
				{
					printf("release_resources(): releasing cp image\n");
					delete m_value.pdb_cpimage;
					m_value.pdb_cpimage = 0;
					break;
				}
				case PDB_VOIDPOINTER:
				default:
				{
					break;
				}
		}
	}
}
