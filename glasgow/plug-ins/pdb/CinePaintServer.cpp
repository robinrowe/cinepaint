/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePainrServer - Client/Server execution module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****

 * $Id: CinePaintServer.cpp,v 1.2 2006/12/21 11:18:08 robinrowe Exp $
 */

#include <unistd.h>
#include "CinePaintServer.h"
#include "CinePaintServerWorker.h"
#include <signal.h>

bool CinePaintServer::InitServer(int port)
{
	m_running = true;
	bool _ret = true;
	int _res = -1;
	try 
	{
		server_socket.Open(port);
		printf("Server Socket Opened on port: %d\n", port);
	}
	catch ( CPSocketException &e )
	{
		printf("ERROR: Socket Open failed : %s\n", e.description().c_str());
		_ret = false;
	}
	if ( _ret )
        _res = pthread_create(&m_listenerThreadId, NULL, static_listener_thread_func, this);
	
	_ret = (_res == 0);
	return _ret;
}


void *CinePaintServer::listener_thread_func()
{
    while( m_running )
	{
		ServerSocket *sock = new ServerSocket();
		try 
		{
			server_socket.accept( *sock );
		    NewClientConnected(*sock);
		}
		catch (CPSocketException &e ) 
		{
			printf("ERROR: Socket accept failed %s\n", e.description().c_str());
		}
	}
	return 0;
}


void CinePaintServer::NewClientConnected(ServerSocket &sock)
{
	CinePaintServerWorker *srv_worker = new CinePaintServerWorker(sock,world);
	m_clientList.push_back(srv_worker);
	printf("INFO: (CinePaintServer::NewClientConnected) New client conected\n");
}
