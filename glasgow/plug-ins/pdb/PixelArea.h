/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      PixelArea - Abstract pixel area 
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: PixelArea.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _PIXEL_AREA_H_
#define _PIXEL_AREA_H_

#include "dll_api.h"
#include "AbstractBuf.h"

/**
 * PixelArea provides a simple Region of Interest over an AbstractBuf
 * The PixelArea (currently) does not itself provide direct access to underlying
 * buffer data. Instead, access is granted through a PixelRow of data.
 *
 * @todo need to look at the referencing - see the originla cinepaint pixelarea.c
 */
class CINEPAINT_IMAGE_API PixelArea
{
	public:

		/**
		 * Destructor
		 */
		~PixelArea();

		/**
		 * Returns the x location of this PixelArea in relation to the set AbstractBuf
		 *
		 * @return the x location of this PixelArea
		 */
		int GetX() const;

		/**
		 * returns the y location of this PixelArea in relation to the set AbstractBuf
		 *
		 * @return the y location of this PixelArea
		 */
		int GetY() const;

		/**
		 * Returns the width of this PixelArea
		 *
		 * @return the width of this PixelArea
		 */
		int GetWidth() const;

		/**
		 * Returns the height of this PixelArea
		 *
		 * @return the height of this PixelArea
		 */
		int GetHeight() const;

		/**
		 * Populates the specified PixelRow with image data from the specified row within this PixelArea
		 * The width of pixelrow indicates how many pixels will be copied. This will commonly match the
		 * width of this PixelArea, although is not required to.
		 *
		 * @param pixelrow the PixelRow to set with image data
		 * @param row the row of data within this PixelArea to copy
		 * @return true if the PixelRow wa spopulates with data, false if the range specified
		 *         falls outside the data area of this AbstractBuf
		 */
		bool GetPixelRow(PixelRow& pixelrow, int row);

		/**
		 * Writes data contained within the specified PixelRow to the AbstractBuf this PixelArea priovides a view over
		 * Data copying is started at the specified row of this PixelArea's view over the underlying AbstractBuf.
		 *
		 * @param pixelrow the PixelRow containing image data to write
		 * @param row the row of data within this PixelArea to write data.
		 * @return true if the PixelRow wa spopulates with data, false if the range specified
		 *         falls outside the data area of this AbstractBuf
		 */
		bool WritePixelRow(PixelRow& pixelrow, int row);

		/**
		 * Populates the specified Pixel with image data from the specified point within this PixelArea
		 * If the specified location falls outwith the area of this PixelArea, no further action is taken
		 * and false is returned.
		 * Pixel is assumed to be in the same format as this PixelArea, if this is not the case, false is
		 * returned and no data is copied
		 *
		 * @param pixel the Pixel to Write data into
		 * @param x the x location in pixels
		 * @param y the y location in pixels
		 * @return false if the Pixel CinePaintTag format and the PixelArea CinePaintTag format do not match
		 */
		bool GetPixel(Pixel& pixel, int x, int y);

		/**
		 * Writes the specified Pixel into the specified location of the AbstractBuf this PixelArea provides a view over.
		 * If the specified location falls outwith this PixelArea, no further action is taken.
		 *
		 * @param pixel the Pixel to Write
		 * @param x the x location in pixels
		 * @param y the y location in pixels
		 * @return false if the Pixel CinePaintTag format and the PixelArea CinePaintTag format do not match
		 */
		bool WritePixel(Pixel& pixel, int x, int y);

		/**
		 * Returns direct access to the specified pixel within the AbstractBuf backing this PixelArea
		 * If the specified location falls outwith this PixelArea 0 is returned.
		 *
		 * @param x the x location in pixels
		 * @param y the y location in pixels
		 */
		unsigned char* GetBufferPixel(int x, int y);

		/**
		 * Returns the AbstractBuf this PixelArea represents a region of interst over
		 *
		 * @return the AbstractBuf this PixelArea represents an area of interest over
		 */
		AbstractBuf& GetBuffer();
		const AbstractBuf& GetBuffer() const;

		
		// @TODO Don't like having to do this but to allow deleteion across DLL boundary
		// this seems to be the only real way.
		void operator delete(void *p) {::operator delete( p);};


	protected:
	private:
		friend class AbstractBuf;
		/**
		 * Constructs a new PixelArea at the specified location and width, height over the specified AbstractBuf of image data
		 *
		 * @param buf the AbstractBuf over which this PixelArea indicates a region of interest
		 * @param x the x location of this PixelArea
		 * @param y the y location of this PixelArea
		 * @param w the width
		 * @param h the height
		 * @param reftype the reference type on the AbstractBuf data
		 */
		PixelArea(AbstractBuf& buf, int x, int y, int w, int h, AbstractBuf::RefType reftype);

		/** the AbstractBuf we provide a Region of Interest over */
		AbstractBuf& m_buffer;

		/** the x coordinate of this PixelArea */
		int m_x;

		/** the y coordinate of this PixelArea */
		int m_y;

		/** the width of this PixelArea */
		int m_width;

		/** the height of this PixelArea */
		int m_height;

		/** how to reference the portion */
		AbstractBuf::RefType m_reftype;


}; /* class PixelArea */

#endif /* _PIXELAREA_H_ */
