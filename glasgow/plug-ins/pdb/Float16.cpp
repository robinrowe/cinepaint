/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Float16 - Float16 image data type (as a class)
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: Float16.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

//#include <config.h>
#include "Float16.h"

/**
 * Constructs a new Float16 from the specified float value
 *
 * @param f float representation to initialize this Float16
 */
Float16::Float16(float f)
{
	m_float16 = f;
}

/**
 * Constructs a new Float16 initialized with the specified unsigned short
 *
 * @param f16 float16 representation stored as a single unsigned short to initialize this Float16
 */
Float16::Float16(unsigned short f16)
{
	m_float16 = Float16::ToFloat(f16);
}

/**
 * Returns the of this Float16 as a float
 *
 * @return the float value of this Float16
 */
float Float16::GetFloat() const
{
	return(m_float16);
}

/**
 * Returns the Float16 value of this Float16 as an unsigned short
 *
 * @return the float16 value of this Float16 as an unsigned short
 */
unsigned short Float16::GetFloat16() const
{
	return(Float16::ToFloat16(m_float16));
}

/**
 * Converts the specified Float16 value stored as a single unsigned short to its float representation
 *
 * @param f16 the float16 value to convert
 * @return the float representation of the specified float16 value
 */
float Float16::ToFloat(unsigned short f16)
{
	float ret = 0;
	ShortsFloat sf;

#if WORDS_BIGENDIAN

	sf.s[0] = f16;
	sf.s[1] = 0;
	ret = static_cast<float>(sf.f);

#else

	sf.s[1] = f16;
	sf.s[0]= 0;
	ret = static_cast<float>(sf.f);

#endif

	return(ret);
}

/**
 * Converts the specified float value to its float16 representation stored as a single unsigned short
 *
 * @param f the float to convert
 * @return the float16 representation stored as a single unsigned short
 */
unsigned short Float16::ToFloat16(float f)
{
	unsigned short ret = 0;
	ShortsFloat sf;

#if WORDS_BIGENDIAN

	sf.f = f;
	ret = sf.s[0])

#else

	sf.f = f;
	ret = sf.s[1];

#endif

	return(ret);
}
