/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Abstract UI Definition Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CPPluginUIProgress.cpp,v 1.3 2008/01/28 18:22:28 robinrowe Exp $
 */

#include "CPPluginUIProgress.h"
#include "CinePaintProgressListener.h"
#include "ListenerList.h"
//#include "CinePaintApp.h"


CPPluginUIProgress::CPPluginUIProgress(char* name, char* text, double max, double step)
:CPPluginUIWidget(CPPluginUIWidget::PROGRESS,name), m_text(text), m_max(max), m_step(step)
{	ListenerList<CinePaintProgressListener>* _list = ListenerList<CinePaintProgressListener>::GetProgressListenerList();
	if(!_list)
	{	return;
	}
	for ( ListenerList<CinePaintProgressListener>::const_iterator citer = _list->begin(); citer != _list->end(); ++citer )
	{	(*citer)->ProgressStart(GetName(), m_text, int(m_max), int(m_step));
	}
}

CPPluginUIProgress::~CPPluginUIProgress()
{

}

void CPPluginUIProgress::SetText(char* text)
{	ListenerList<CinePaintProgressListener>* _list = ListenerList<CinePaintProgressListener>::GetProgressListenerList();
	if(!_list)
	{	return;
	}
	for ( ListenerList<CinePaintProgressListener>::const_iterator citer = _list->begin();citer != _list->end(); ++citer )
	{	(*citer)->MessageChanged(m_name, text);
	}
}


void CPPluginUIProgress::SetProgress(double pos)
{	ListenerList<CinePaintProgressListener>* _list = ListenerList<CinePaintProgressListener>::GetProgressListenerList();
	if(!_list)
	{	return;
	}
	for ( ListenerList<CinePaintProgressListener>::const_iterator citer = _list->begin();citer != _list->end(); ++citer )
	{	(*citer)->ProgressPosition(m_name, int(pos));
	}
}


void CPPluginUIProgress::ResetProgress(double max, double step)
{	ListenerList<CinePaintProgressListener>* _list = ListenerList<CinePaintProgressListener>::GetProgressListenerList();
	if(!_list)
	{	return;
	}
	for ( ListenerList<CinePaintProgressListener>::const_iterator citer = _list->begin();citer != _list->end(); ++citer )
	{	(*citer)->LimitsChanged(m_name, 0,int(max),int(step));
	}
}

void CPPluginUIProgress::UpdateProgress()
{	ListenerList<CinePaintProgressListener>* _list = ListenerList<CinePaintProgressListener>::GetProgressListenerList();
	if(!_list)
	{	return;
	}
	for ( ListenerList<CinePaintProgressListener>::const_iterator citer = _list->begin();citer != _list->end(); ++citer )
	{	(*citer)->ProgressUpdate(m_name);
	}
}

void CPPluginUIProgress::Hide()
{	ListenerList<CinePaintProgressListener>* _list = ListenerList<CinePaintProgressListener>::GetProgressListenerList();
	if(!_list)
	{	return;
	}
	for ( ListenerList<CinePaintProgressListener>::const_iterator citer = _list->begin();citer != _list->end(); ++citer )
	{	(*citer)->ProgressComplete(m_name);
	}
}

void CPPluginUIProgress::Create()
{
}
