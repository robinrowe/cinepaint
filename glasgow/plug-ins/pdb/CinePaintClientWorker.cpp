/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintClientWorker - Class for client side operation of sockets.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintClientWorker.cpp,v 1.2 2006/12/18 08:21:21 robinrowe Exp $
 */


#include "CinePaintClientWorker.h"

CinePaintClientWorker::CinePaintClientWorker(char* host, int port)
:m_host(host), m_port(port), m_sock(host,port)
{
	printf("INFO: (CinePaintClientWorker::CinePaintClientWorker())Starting Wait func\n");
	m_open = true;

	m_job = 0;
}


CinePaintClientWorker::~CinePaintClientWorker()
{
	m_open = false;
	if (m_job)
		delete m_job;
}


void *CinePaintClientWorker::client_thread_func()
{
	m_dataValid = false;

	// @TODO [claw] I've hacked this for demonstration purposes
	//       There needs to be a graceful way of exiting this loop
	//       and closing the socket

	bool done = false;
	while(m_open && !done)
	{
		CPSocket::WaitEndENUM _res = m_sock.BlockingWait();
		if (m_open)
		{
			switch (_res)
			{
				case CPSocket::SOCK_DATA:
				{
					printf("New Data waiting On Client Socket\n");
					OnDataPending();
					done = true;
					break;
				}
				case CPSocket::SOCK_CLOSE:
				{
					m_open = false;
					m_sock.close();
					break;
				}
				case CPSocket::SOCK_ERROR:
				{
					printf("Error: Socket Error in WaitFunc\n");
					m_open = false;
					break;
				}
			}
		}
		else
		{
			printf("INFO: Client Socket Closed\n");
		}
	}
  	return 0;
}


void CinePaintClientWorker::OnDataPending()
{
	if (m_job)
		delete m_job;
	printf("INFO: (CinePaintClientWorker::OnDataPending()) Recieving results...\n");
	m_job = new CinePaintNetJob;
	m_job->DeSerialize(m_sock);          
	printf("INFO: (CinePaintClientWorker::OnDataPending()) Results recieved...\n");
	m_dataValid = true;
}


void CinePaintClientWorker::SendData(CinePaintNetJob &m)
{
	m.Serialize(m_sock);
}

CinePaintNetJob *CinePaintClientWorker::WaitOnData()
{
	printf("INFO (CinePaintClientWorker::WaitOnData()) Waiting on Server result\n");

	client_thread_func();
	if (m_dataValid)
	{
        return m_job;
	}
	else
	{
		return 0;
	}
}
