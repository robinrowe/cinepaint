/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Color - Representation of a Color
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: Color.h,v 1.2 2006/01/05 03:08:14 robinrowe Exp $
 */

#ifndef _CINEPAINT_COLOR_H_
#define _CINEPAINT_COLOR_H_

#include "dll_api.h"
#include <utility/MathUtil.h>

// rsr bug: note that BFP and other implementations here not correct. Doesn't clamp properly.

#define U8_MAX 255
#define U16_MAX 65535
#define BFP16_MID 32767+1

/**
 * An RGB representation of a Color/Colour
 * Internally Color components are represented as doubles between 0.0 - 1.0
 *
 */
class Color
{	double m_red;
	double m_green;
	double m_blue;
	double m_alpha;
public:
	Color(double r, double g, double b, double a = 1.0)
	{	this->SetRGBA(r,g,b,a);
	}
	Color(unsigned char r, unsigned char g, unsigned char b, unsigned char a = 1.0)
	{	this->SetRGBA(r,g,b,a);
	}
	virtual ~Color()
	{}
	void Set(const Color& c)
	{	this->m_red = c.GetRed64();
		this->m_green = c.GetGreen64();
		this->m_blue = c.GetBlue64();
		this->m_alpha = c.GetAlpha64();
	}
	void SetRGBA(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
	{	m_red =   static_cast<double>(r) / static_cast<double>(U8_MAX);
		m_green = static_cast<double>(g) / static_cast<double>(U8_MAX);
		m_blue =  static_cast<double>(b) / static_cast<double>(U8_MAX);
		m_alpha = static_cast<double>(a) / static_cast<double>(U8_MAX);
	}
	void SetRGBA(double r, double g, double b, double a)
	{	m_red =   MathUtil::clamp(r, static_cast<double>(0.0), static_cast<double>(1.0));
		m_green = MathUtil::clamp(g, static_cast<double>(0.0), static_cast<double>(1.0));
		m_blue =  MathUtil::clamp(b, static_cast<double>(0.0), static_cast<double>(1.0));
		m_alpha = MathUtil::clamp(a, static_cast<double>(0.0), static_cast<double>(1.0));
	}
	double GetRed64() const
	{	return(m_red);
	}
	double GetGreen64() const
	{	return(m_green);
	}
	double GetBlue64() const
	{	return(m_blue);
	}
	double GetAlpha64() const
	{ 	return(m_alpha);
	}
	unsigned char GetRed8() const
	{	return(static_cast<unsigned char>(U8_MAX * m_red));
	}
	unsigned char GetGreen8() const
	{	return(static_cast<unsigned char>(U8_MAX * m_green));
	}
	unsigned char GetBlue8() const
	{	return(static_cast<unsigned char>(U8_MAX * m_blue));
	}
	unsigned char GetAlpha8() const
	{ 	return(static_cast<unsigned char>(U8_MAX * m_alpha));
	}
	unsigned short GetRed16() const
	{	return(static_cast<unsigned char>(U16_MAX * m_red));
	}
	unsigned short GetGreen16() const
	{	return(static_cast<unsigned char>(U16_MAX * m_green));
	}
	unsigned short GetBlue16() const
	{	return(static_cast<unsigned char>(U16_MAX * m_blue));
	}
	unsigned short GetAlpha16() const
	{ 	return(static_cast<unsigned char>(U16_MAX * m_alpha));
	}
	void GetRGBA(double& r, double& g, double& b, double& a) const
	{	r = GetRed64();
		g = GetGreen64();
		b = GetBlue64();
		a = GetAlpha64();
	}
	void GetRGBA(unsigned char& r, unsigned char& g, unsigned char& b, unsigned char& a) const
	{	r = GetRed8();
		g = GetGreen8();
		b = GetBlue8();
		a = GetAlpha8();
	}
	bool operator != (const Color& c) const
	{	return((this->m_alpha != c.m_alpha) || (this->m_blue != c.m_blue) || (this->m_green != c.m_green) || (this->m_red != c.m_red));
	}
	static Color GetBLACK()
	{	return Color(0.0, 0.0, 0.0, 1.0);
	}
	static Color GetWHITE()
	{	return Color(1.0, 1.0, 1.0, 1.0);
	}
	static Color GetTRANSPARENT()
	{	return Color(0.0, 0.0, 0.0, 0.0);
	}
}; /* class Color */

#endif /* _CINEPAINT_COLOR_H_ */
