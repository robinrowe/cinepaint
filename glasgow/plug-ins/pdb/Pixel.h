/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Pixel - Abstract pixel container
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: Pixel.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _CINEPAINT_PIXEL_H_
#define _CINEPAINT_PIXEL_H_

#include "dll_api.h"
#include "CinePaintTag.h"

// forward declaration
class Color;

/**
 * An abstract representation of a Pixel within a buffer of image data
 * The format of a Pixel is defined by a CinePaintTag during construction
 *
 */
class CINEPAINT_IMAGE_API Pixel
{
	public:
		/**
		 * Constructs a new Pixel representing a single pixel of image data
		 * A new data buffer is allocated to hold the pixels of type as specified by tag.
		 * This internal buffer remains managed by this Pixel
		 *
		 * @param tag the CinePaintTag indicating the buffer type
		 */
		Pixel(const CinePaintTag& tag);

		/**
		 * Constructs a new Pixel of the specified CinePaintTag format initialized to the specified Color
		 *
		 * @param tag the CinePaintTag indicating the buffer type
		 * @param color the COlor to initizlize the Pixel to
		 */
		Pixel(const CinePaintTag& tag, const Color& color);

		/**
		 * Destructor
		 */
		~Pixel();

		/**
		 * Returns the CinePaintTag of this Pixel
		 *
		 * @return the CinePaintTag of this Pixel
		 */
		const CinePaintTag& GetTag() const;
		CinePaintTag& GetTag();

		/**
		 * Returns direct access to the pixel data
		 * The returned data remains managed by this Pixel and should not be destroyed directly.
		 *
		 * @return direct access to the pixel data
		 */
		unsigned char* GetData();

		/**
		 * Copies the specified image data into this pixel.
		 * The number of bytes copied is determined must match the format of this Pixel as specified
		 * by the CinePaintTag.
		 * 
		 * @param data the new Pixel data
		 */
		void SetData(unsigned char* data);

		// @TODO Don't like having to do this but to allow deleteion across DLL boundary
		// this seems to be the only real way.
		void operator delete(void *p) {::operator delete( p);};
	protected:

	private:
		/** the pixel data */
		unsigned char* m_data;

		/** the buffer type tag */
		CinePaintTag m_tag;
};

#endif /* _CINEPAINT_PIXEL_H_ */
