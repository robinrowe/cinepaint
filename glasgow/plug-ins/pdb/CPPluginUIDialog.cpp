/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Abstract UI Definition Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CPPluginUIDialog.cpp,v 1.3 2006/12/18 08:21:21 robinrowe Exp $
 */

#include "CPPluginUIDialog.h"
#include "CinePaintDialogListener.h"
#include "ListenerList.h"
//#include "CinePaintApp.h"

// CRAPPY Hack to fix CreateDialog vs CreateDialogA problem
#ifdef WIN32
#undef CreateDialog
#endif

CPPluginUIDialog::CPPluginUIDialog(char* name, char* title, int w, int h)
:CPPluginUIWidget(CPPluginUIWidget::DIALOG, name), m_title(title), m_width(w), m_height(h)
{

}

CPPluginUIDialog::~CPPluginUIDialog()
{

}



void CPPluginUIDialog::AddWidget(CPPluginUIWidget *widget)
{
	m_widgetList.push_back(widget);
}

DialogResponse CPPluginUIDialog::DoModal()
{	DialogResponse _res;
	ListenerList<CinePaintDialogListener>* _list = ListenerList<CinePaintDialogListener>::GetDialogListenerList();
	if(!_list)
	{	return NONE_ENUM;
	}
	for ( ListenerList<CinePaintDialogListener>::const_iterator citer = _list->begin(); citer != _list->end(); ++citer )
	{	(*citer)->CreateDialog(m_name, this);
		_res = (*citer)->DoModal(m_name);
	}
	return _res;
}

void CPPluginUIDialog::Create()
{
}

void CPPluginUIDialog::Draw()
{
}

char* CPPluginUIDialog::GetTitle() 
{ return m_title; }

int CPPluginUIDialog::GetWidth() 
{ return m_width;}

int CPPluginUIDialog::GetHeight() 
{ return m_height;}
