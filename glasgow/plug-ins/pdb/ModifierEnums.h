/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Mouse Keyboard modifier Enums
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ModifierEnums.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _MODIFIER_ENUMS_H_
#define _MODIFIER_ENUMS_H_
 
enum CinePaintMouseModifier
{
	CP_MOD_SHIFT_MASK_ENUM       = 1 << 0,
	CP_MOD_CAPS_LOCK_MASK_ENUM   = 1 << 1,
	CP_MOD_CONTROL_MASK_ENUM     = 1 << 2,
	CP_MOD_ALT_MASK_ENUM         = 1 << 3,
	CP_MOD_NUM_LOCK_MASK_ENUM    = 1 << 4,
	CP_MOD_META_MASK_ENUM        = 1 << 5,
	CP_MOD_SCROLL_LOCK_MASK_ENUM = 1 << 6,
	CP_MOD_MOD5_MASK_ENUM        = 1 << 7,
	CP_MOD_BUTTON1_MASK_ENUM     = 1 << 8,
	CP_MOD_BUTTON2_MASK_ENUM     = 1 << 9,
	CP_MOD_BUTTON3_MASK_ENUM     = 1 << 10,
	CP_MOD_BUTTON4_MASK_ENUM     = 1 << 11,
	CP_MOD_BUTTON5_MASK_ENUM     = 1 << 12
};

CinePaintMouseModifier& operator&= (CinePaintMouseModifier& x, CinePaintMouseModifier y);
CinePaintMouseModifier& operator|= (CinePaintMouseModifier& x, CinePaintMouseModifier y);
CinePaintMouseModifier& operator^= (CinePaintMouseModifier& x, CinePaintMouseModifier y);
CinePaintMouseModifier  operator&  (CinePaintMouseModifier  x, CinePaintMouseModifier y);
CinePaintMouseModifier  operator|  (CinePaintMouseModifier  x, CinePaintMouseModifier y);
CinePaintMouseModifier  operator^  (CinePaintMouseModifier  x, CinePaintMouseModifier y);
CinePaintMouseModifier  operator~  (CinePaintMouseModifier  x);


#endif //#ifdef _MODIFIER_ENUMS_H_
