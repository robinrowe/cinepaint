/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      FlatBuf - Flat buffer Image representation
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: FlatBuf.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _FLATBUF_H_
#define _FLATBUF_H_

#include "dll_api.h"
#include "AbstractBuf.h"

// forward declaration
#include "plug-ins/pdb/CinePaintTag.h"

/**
 * FlatBuf is an AbstractBuf implementation for storing image data in a single flat data buffer
 *
 */
class CINEPAINT_IMAGE_API FlatBuf : public AbstractBuf
{
	public:

		/**
		 * Constructs a new FlatBuf to handle image data of the specified CinePaintTag, width and height
		 *
		 * @param tag the CinePaintTag descibibg the image data format
		 * @param w the width in pixels of this FlatBuf
		 * @param h the height in pixels of this FlatBuf
		 */
		FlatBuf(const CinePaintTag& tag, int w, int h) ;

		/**
		 * Destructor
		 */
		virtual ~FlatBuf();

		// See AbstractBuf for complete inherited method descriptions.

		//--------------------------
		// Width / Height / Position

		virtual int GetPortionX(int x, int y) const;
		virtual int GetPortionY(int x, int y) const;

		virtual int GetPortionWidth(int x, int y) const;
		virtual int GetPortionHeight(int x, int y) const;

		//------------
		// data access
		virtual unsigned char* GetPortionData(int x, int y);
		virtual const unsigned char* GetPortionData(int x, int y) const;
		virtual bool GetPixelRow(PixelRow& pixelrow, int row, int offset, int pixels);
		virtual bool WriteRow(PixelRow& pixelrow, int row, int offset, int pixels);
		virtual bool GetPixel(Pixel& pixel, int x, int y);
		virtual bool WritePixel(Pixel& pixel, int x, int y);
		unsigned char* GetBufferPixel(int x, int y);

		virtual int GetPortionRowstride(int x, int y) const;


		//---------------------------------------------------------
		// allocate and/or swap in the backing store for this pixel

		virtual bool IsPortionAlloced(int x, int y) const;
		virtual bool RefPortionReadOnly(int x, int y);
		virtual bool RefPortionReadWrite(int x, int y);
		virtual bool UnrefPortion(int x, int y);

	protected:

		//-----------------------
		// alloc / dealloc memory

		virtual bool AllocPortion(int x, int y);
		virtual bool UnallocPortion(int x, int y);

	private:

		/**
		 * Returns true if x and y are inside the width and height of this FlatBuf
		 * ie if 0 <= x < GetWidth() and 0 <= y < GetHeight()
		 *
		 * @return true if x and y are inside the width and height of this Buf
		 */
		bool check_xy(int x, int y) const;

		/**
		 * Returns a pointer to the canvas data for the portion with the specified top left corner
		 * This is a helper method for the public GetPortionData methods
		 *
		 * @param x the x value of the top left corner of the portion
		 * @param y the y value of the top left corner of the portion
		 * @return a pointer to the canvas data for the specified portion
		 */
		unsigned char* get_portion_data(int x, int y) const;

		/** indicates if this Buf has been allocated in memory */
		bool m_is_alloced;

		/** the Image Data Buffer */
		unsigned char* m_data;

}; /* class FlatBuf */

#endif /* _FLATBUF_H_ */
