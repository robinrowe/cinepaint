/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      RectSelectShape - Rectangular selection object
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: RectSelectShape.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "RectSelectShape.h"
#include "AbstractBuf.h"
#include "Pixel.h"
#include "Color.h"
#include <utility/CPRect.h>

RectSelectShape::RectSelectShape(int x, int y, int width, int height)
		: m_x(x), m_y(y), m_width(width), m_height(height)
{}

/**
 * Destructor
 */
RectSelectShape::~RectSelectShape()
{}


/**
 * Draws this selection shape into the specified selection mask
 *
 * @param mask the mask to draw this selection shape onto
 */
void RectSelectShape::DrawMask(AbstractBuf& mask) const
{
	// the selection rect is on the mask area, right?
	if((m_x < mask.GetWidth()) && (m_y < mask.GetHeight()))
	{
		Pixel _pixel(mask.GetTag(), Color::GetWHITE());

		// dont draw over the edge of the mask
		int _y_max = (m_y + m_height) > mask.GetHeight() ? mask.GetHeight() - m_y : m_y + m_height;
		int _x_max = (m_x + m_width) > mask.GetWidth() ? mask.GetWidth() - m_x : m_x + m_width;

		for(int y = m_y; y < _y_max; y++)
		{
			for(int x = m_x; x < _x_max; x++)
			{
				mask.WritePixel(_pixel, x, y);
			}
		}
	}
}

/**
 * Returns the bounding rectangle of this selection shape
 *
 * @param bounds a rectangle set to the bounding box of this selection shape
 * @return the bounding box of this selection shape
 */
CPRect& RectSelectShape::GetBoundingRect(CPRect& bounds) const
{
	bounds.SetRect(m_x, m_y, m_width, m_height);
	return(bounds);
}

/**
 * Returns true if this selection shape contains the specified point
 *
 * @param x the x coordinate
 * @param y the y coordinate
 * @return true if the specified point lies within this selection shape
 */
bool RectSelectShape::Contains(int x, int y) const
{
	if((x < m_x) || (x > (m_x + m_width)) || (y < m_y) || (y > (m_y + m_height)))
	{
		return(false);
	}
	else
	{
		return(true);
	}
}

/**
 * Returns true if this Selection Shape is empty.
 * The definition of empty for a shape is implementation defines, for example
 * if the width or height of a rectangle is zero.
 *
 * @return true if this selection shape is empty, false otherwise
 */
bool RectSelectShape::IsEmpty() const
{
	return((m_height <= 0) || (m_width <= 0));
}

