/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintServer - Client/Server execution module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintServer.h,v 1.2 2006/12/21 11:18:08 robinrowe Exp $
 */

#ifndef _CINEPAINT_SERVER_H_
#define _CINEPAINT_SERVER_H_

#include "utility/ServerSocket.h"
#include "plug-ins/pdb/World.h"
#include <unistd.h>
#include <pthread.h>
#include <list>
#include "dll_api.h"

/**
 * The main class used for running a CinePaint instance as a server.
 * this listens on the specified port for new connections and then
 * opens allows a connection if available.
**/

class CinePaintServerWorker;

class CINEPAINT_CORE_API CinePaintServer 
{	ServerSocket server_socket;
	World* world;
public:
	CinePaintServer(World* world)
	{	this->world=world;
	//	m_listenerThreadId=0;
		m_numConnected = 0;
		m_port = -1;
		m_running = false;
	}

	virtual ~CinePaintServer()
	{
		printf("Killing Server\n");
		m_running = false;	
		server_socket.close();
			
	}
	bool InitServer(int port);

	int GetConnectedCount() { return m_numConnected; };

protected:

private:
	
	
	/**
	 * Static method used as thread start func, simply calls the non-static version.
	 *
	**/
	static void *static_listener_thread_func(void *args) { return ((CinePaintServer *)args)->listener_thread_func(); };
	void *listener_thread_func();


	/**
	 * Add a new connection to the list of connected clients.
	**/
	void NewClientConnected(ServerSocket &sock);

	pthread_t m_listenerThreadId;
	int m_numConnected;
	int m_port;

	bool m_running;

	std::list<CinePaintServerWorker *> m_clientList;
};


#endif // _CINEPAINT_SERVER_H_

