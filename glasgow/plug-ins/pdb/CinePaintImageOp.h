/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintImageOp - Abstract image operation interface class.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintImageOp.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _CINEPAINT_IMAGE_OP_H_
#define _CINEPAINT_IMAGE_OP_H_

#include "dll_api.h"
#include "plug-ins/pdb/CinePaintTag.h"

/**
 * CinePaintImageOp defines an interface for core CinePaint Image Operation plugins
 * A CinePaintImageOp provides a plugin mechanism to load various core image operations for various
 * image formats within CinePaint. This allows new image formats to be added by simply creating a plugin
 * to handle various image operations for the new format. This system also allows an installation to
 * to provide a complete reimplementation of the core image operation set, if requied
 *
 */
//CINEPAINT_CORE_INTERFACES_API

class CinePaintImageOp
{
	public:

		// Type defs for function pointers.
		/** Function prototype to create an image op plugin. */
		typedef CinePaintImageOp * (*PFNCreateCinePaintImageOp)();

		/** plugin entry point */
		typedef PFNCreateCinePaintImageOp * (*PFNGetImageOpClassList)();


		/**
		 * Destructor
		 */
		virtual ~CinePaintImageOp()
		{}

		// Named Image Operation Types, defines the 'application' known operation class types
		// Although this is slightly less efficient than an enum containaing all image op types,
		// it is slightly more flexible, allowing new image op types to be easily added. and also
		// supporting user defined image op types.
		// if this proves too inefficient, or prone to errors, switching to an enum based
		// implementation is reasonably clean and easy.

		static const char* GetIMAGE_OP_TYPE_NONE()
		{	return "cp.imageop.none";
		}
		static const char* GetIMAGE_OP_TYPE_SCALE()
		{	return "cp.imageop.scale";
		}
		static const char* GetIMAGE_OP_TYPE_FILL()
		{	return "cp.imageop.fill";
		}
		static const char* GetIMAGE_OP_TYPE_COMPOSITE()
		{	return "cp.imageop.composite";
		}
		static const char* GetIMAGE_OP_TYPE_RENDER()
		{	return "cp.imageop.render";
		}
		static const char* GetIMAGE_OP_TYPE_CONVERSION()
		{	return "cp.imageop.conversion";
		}

		/**
		 * Returns the Image Op Type of this CinePaintImageOp
		 * If this CinePaintImageOp provides a known operation set, by implementing one of the
		 * defined ImageOp interfaces, this method shopuld return one of the defined Image Op 
		 * strings, otherwise, the image op type may be user defined
		 *
		 * @return the Image Op type of this CinePaintImageOp
		 */
		virtual const char* GetImageOpType() const = 0;

		/**
		 * Returns true if this CinePaintImageOp supports operations on Image data of the specified CinePaintTag
		 * A particular implementation of a CinePaintImageOp may choose to operate on one or more image
		 * data types. There may be multiple CinePaintImageOp for a known Image Op type that handle various
		 * data formats, this method may be used to determine if this CinePaintImageOp handles the specified
		 * format
		 *
		 * @return trus if this CinePaintImageOp supports the spoecified CinePaintTag
		 */
		virtual bool SupportsTag(const CinePaintTag& tag) const = 0;


	protected:
		/**
		 * Protcted Constructor - disallow instances
		 *
		 * @param name the name of this CinePaintImageOp
		 * @param blurb 
		 * @param help 
		 * @param author author of This CinePaintImageOp
		 * @param copyright copryright notice for this CinePaintImageOp plugin
		 * @param date date this CinePaintImageOp was written
		 */
		CinePaintImageOp(char* name, char* blurb, char* help, char* author, char* copyright, char* date)
		{}

	private:

		
};

#endif //_CINEPAINT_IMAGE_OP_H_
