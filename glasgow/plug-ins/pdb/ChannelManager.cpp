/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ChannelManager - Image module.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ChannelManager.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "ChannelManager.h"
#include "CinePaintImage.h"
#include "Channel.h"
#include <algorithm>
#include <stdio.h>

//-------------------------
// Channel Addition / Removal

/**
 * Adds the secified Channel to this ChannelManager at the specified position
 * Once added, the ChannelManager assumes responsibility for the Channel until the Channel is removed.
 * If the Channel is already managed by this ChannelManager, the Channel is not added a second time and
 * false is returned, otherwise the Channel is added and true is returned.
 *
 * @param chennal the Channel to add
 * @param active set true if the newly added Channel is the new active Channel
 * @return true if the Channel is added, false otherwise
 */
bool ChannelManager::AddChannel(Channel* channel, bool active)
{
	bool _ret = false;

	if(!has_channel(channel))
	{
		m_channels.push_back(channel);
		_ret = true;

		if(active)
		{
			SetActiveChannel(channel);
		}

		if(channel->GetVisible())
		{
			channel->Update(0, 0, channel->GetWidth(), channel->GetHeight());
		}
	}

	return(_ret);
}

/**
 * Removes and returns the specified Channel from this ChannelManager
 * Resopnsibility for the removed Channel passes to the caller.
 * If the Specified Channel is not managed by this ChannelManager, no further action is taken
 * and 0 is returned
 *
 * @param channel the Channel to be removed
 * @return the removed Channel or 0 if the specified Channel is not managed by this ChannelManager
 */
Channel* ChannelManager::RemoveChannel(Channel* channel)
{
	Channel* _removed = 0;

	if(channel)
	{
		ChannelContainer_t::iterator _iter = std::find(m_channels.begin(), m_channels.end(), channel);

		if(_iter != m_channels.end())
		{
			_removed = *_iter;
			m_channels.erase(_iter);

			if(m_active_channel == channel)
			{
                if(!m_channels.empty())
				{
					SetActiveChannel(m_channels.front());
				}
				else
				{
					SetActiveChannel(0);
				}
			}

			if(channel->GetVisible())
			{
				channel->Update(0, 0, channel->GetWidth(), channel->GetHeight());
			}
		}
	}

	return(_removed);
}

        

//---------------
// Channel Access

/**
 * Returns the currently active Channel
 *
 * @return the currently active Channel
 */
Channel* ChannelManager::GetActiveChannel() const
{
	return(m_active_channel);
}

/**
 * Sets the currently active Channel to the specified Channel
 * If the specified channel is not managed by this ChannelManager, the currently
 * active Channel is set to 0, and false is returned.
 *
 * @param channel the Channel to set as the Active Channel
 * @return false if the Channel is not managed by this ChannelManager, true otherwise
 */
bool ChannelManager::SetActiveChannel(Channel* channel)
{
	bool _ret = false;

	if(has_channel(channel))
	{
		m_active_channel = channel;
		m_active_channel->SetActive(true);

		_ret = true;
	}

	return(_ret);
}


//------------------
// Layer Positioning

/**
 * Raises the specified layer within the Layer stack by one place
 *
 * @param layer the Layer to raise
 */
void ChannelManager::RaiseChannel(Channel* channel)
{
	ChannelContainer_t::iterator _iter = std::find(m_channels.begin(), m_channels.end(), channel);

	if(_iter != m_channels.end())
	{
		if(_iter != m_channels.begin())
		{
			ChannelContainer_t::iterator _temp_iter = _iter;
			--_temp_iter;
            
			std::iter_swap(_iter, _temp_iter);

			channel->Update(0, 0, channel->GetWidth(), channel->GetHeight());
		}
		else
		{
			printf("Channel cannot be raised any further");
		}
	}
}

/**
 * Lowers the specified Layer within the Layer stack by one place
 *
 * @param layer the Layer to lower
 */
void ChannelManager::LowerChannel(Channel* channel)
{
	ChannelContainer_t::iterator _iter = std::find(m_channels.begin(), m_channels.end(), channel);

	if(_iter != m_channels.end())
	{
		ChannelContainer_t::iterator _temp_iter = _iter;
		++_temp_iter;

		if(_temp_iter != m_channels.end())
		{
            std::iter_swap(_iter, _temp_iter);

			channel->Update(0, 0, channel->GetWidth(), channel->GetHeight());
		}
		else
		{
			printf("Channel cannot be lower any further");
		}
	}
}


/**
 * Returns the number opf channels within this ChannelManager
 *
 * @return the number of channels within this Chanenlemanager
 */
size_t ChannelManager::GetChannelCount() const
{
	return(m_channels.size());
}

/**
 * Returns true if this ChannelManager manages the specified Channel
 *
 * @param channel the Channel to search for
 * @return true if this ChannelManager manages the specified Channel, false otherwise
 */
bool ChannelManager::has_channel(const Channel* channel) const
{
	bool _ret = false;

	if(std::find(m_channels.begin(), m_channels.end(), channel) != m_channels.end())
	{
		_ret = true;
	}

	return(_ret);
}
