/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ImageUtils - Image Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****.
 *
 * $Id: ImageUtils.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "ImageUtils.h"
#include "AbstractBuf.h"
#include "Float16.h"
#include "Pixel.h"
#include "CinePaintTag.h"
#include "Color.h"
#include <algorithm>
#include <stdio.h>


static unsigned char gimp_icon_pixmap_colors[8][3] =
{
	{ 0x00, 0x00, 0x00 }, /* a - 0   */
	{ 0x24, 0x24, 0x24 }, /* b - 36  */
	{ 0x49, 0x49, 0x49 }, /* c - 73  */
	{ 0x6D, 0x6D, 0x6D }, /* d - 109 */
	{ 0x92, 0x92, 0x92 }, /* e - 146 */
	{ 0xB6, 0xB6, 0xB6 }, /* f - 182 */
	{ 0xDB, 0xDB, 0xDB }, /* g - 219 */
	{ 0xFF, 0xFF, 0xFF }, /* h - 255 */
};

/**
* Converts the specified Color to a formatted Pixel representation
* The Pixel contains a formatted buffer of image data representing a single pixel of color within an image data buffer.
* The format of the data is specified by Pixel::GetTag() and is therefore suitable for directly writing into a data
* buffer of the same CinePaintTag format.
* If a suitable conversion handler for the CinePaintTag format is known, this method returns true, otherwise false is returned.
*
* @param color the Color to set the specified Pixel
* @param pixel the Pixel to set to a fomatted representation of color
* @return true if the conversion succeeded, false otherwise.
*/
bool ImageUtils::ColorToPixel(const Color& color, Pixel& pixel)
{
	bool _ret = false;

	CinePaintTag _tag = pixel.GetTag();

	// @TODO : Extend format handling - GRAY formats are badly represented.
	if (_tag.GetFormat() == CinePaintTag::FORMAT_RGB_ENUM)
	{
		switch(_tag.GetPrecision())
		{
		case CinePaintTag::PRECISION_U8_ENUM:
			{
				unsigned char* _data = reinterpret_cast<unsigned char*>(pixel.GetData());
				*_data++ = color.GetRed8();
				*_data++ = color.GetGreen8();
				*_data++ = color.GetBlue8();
				if(_tag.HasAlpha())
				{
					*_data = color.GetAlpha8();
				}
				break;
			}
		case CinePaintTag::PRECISION_U16_ENUM:
			{
				unsigned short* _data = reinterpret_cast<unsigned short*>(pixel.GetData());
				*_data++ = color.GetRed16();
				*_data++ = color.GetGreen16();
				*_data++ = color.GetBlue16();
				if(_tag.HasAlpha())
				{
					*_data = color.GetAlpha16();
				}
				break;
			}
		case CinePaintTag::PRECISION_FLOAT_ENUM:
			{
				float* _data = reinterpret_cast<float*>(pixel.GetData());
				*_data++ = static_cast<float>(color.GetRed64());
				*_data++ = static_cast<float>(color.GetGreen64());
				*_data++ = static_cast<float>(color.GetBlue64());
				if(_tag.HasAlpha())
				{
					*_data = static_cast<float>(color.GetAlpha64());
				}
				break;
			}
		case CinePaintTag::PRECISION_FLOAT16_ENUM:
			{
				unsigned short* _data = reinterpret_cast<unsigned short*>(pixel.GetData());
				*_data++ = Float16::ToFloat16(static_cast<float>(color.GetRed64()));
				*_data++ = Float16::ToFloat16(static_cast<float>(color.GetGreen64()));
				*_data++ = Float16::ToFloat16(static_cast<float>(color.GetBlue64()));
				if(_tag.HasAlpha())
				{
					*_data = Float16::ToFloat16(static_cast<float>(color.GetAlpha64()));
				}
				break;
			}
		case CinePaintTag::PRECISION_BFP_ENUM:
			{
				unsigned short* _data = reinterpret_cast<unsigned short*>(pixel.GetData());
				*_data++ = static_cast<unsigned short>(color.GetRed16());
				*_data++ = static_cast<unsigned short>(color.GetGreen16());
				*_data++ = static_cast<unsigned short>(color.GetBlue16());
				if(_tag.HasAlpha())
				{
					*_data = static_cast<unsigned short>(color.GetAlpha16());
				}
				break;
			}
		case CinePaintTag::PRECISION_NONE_ENUM:
		default:
			printf("ColorToPixel::get_scale_op_row_resampler: bad precision");
			break;
		}
	}
	if (_tag.GetFormat() == CinePaintTag::FORMAT_GRAY_ENUM)
	{
		unsigned char* _data = reinterpret_cast<unsigned char*>(pixel.GetData());
		*_data = color.GetRed8();// rsr bug: not green?
	}

	return(_ret);
}

/**
 * Converts from a gimp icon format into an AbstractBuf
 * The gimp icon data specified by the bits parameter is copied into the speciedi AbstractBuf.
 * The minimum of the gimp icon and the AbstractBuf are used as the size to copy.
 *
 * Currently only RGB AbstractBuf's are handled.
 *
 * @param buf the AbstractBuf to write the gimp icon into
 * @param bits the gimp icon data
 * @param width the width of the gimp icon
 * @param height the height of the gimp icon
 */
#undef min

AbstractBuf& ImageUtils::GimpIconToAbstractBuf(AbstractBuf& buf, char** bits, int width, int height)
{
	if(buf.GetTag().GetFormat() == CinePaintTag::FORMAT_RGB_ENUM)
	{
		int _width = std::min(buf.GetWidth(), width);
		int _height = std::min(buf.GetHeight(), height);

		unsigned char* _buf_data = buf.GetPortionData(0, 0);

		unsigned char* op = _buf_data;
		char val;
		unsigned char pix;

		for(int y = 0; y < _height; y++)
		{
			for(int x = 0;  x < _width; x++)
			{
				val = bits[y][x];
				if(val == '.')
				{	
					pix = gimp_icon_pixmap_colors[4][0];
				}
				else
				{
					pix = gimp_icon_pixmap_colors[val - 'a'][0];
				}

				*op = pix; op++;
				*op = pix; op++;
				*op = pix; op++;
			}
		}
	}

	return(buf);
}
