/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Plugin Parameter List
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CPParamList.cpp,v 1.2 2006/12/18 08:21:21 robinrowe Exp $
 */

#include "CPParamList.h"
#include "AbstractBuf.h"
#include "Drawable.h"
#include "CinePaintImage.h"
#include <string>

/**
 * Destructor
**/
CPParamList::~CPParamList()
{
	ParamList_t::iterator it = m_paramList.begin();

	while(it != m_paramList.end())
	{
		ParamList_t::iterator next = it;
		++next;

		switch(it->second->type)
		{
			case PDB_INT:
			case PDB_FLOAT:
			{
				break;
			}
			case PDB_CSTRING:
			{
				delete[] it->second->value.pdb_cstring;
				it->second->value.pdb_cstring = 0;
				break;
			}
			case PDB_ABSTRACTBUF:
			{
				delete it->second->value.pdb_abstractbuf;
				it->second->value.pdb_abstractbuf = 0;
				break;
			}
			case PDB_DRAWABLE:
			{
				delete it->second->value.pdb_drawable;
				it->second->value.pdb_drawable = 0;
				break;
			}
			case PDB_CPIMAGE:
			{
				delete it->second->value.pdb_cpimage;
				it->second->value.pdb_cpimage = 0;
				break;
			}
			case PDB_VOIDPOINTER:
			{
				// @TODO [claw] need to re-think this deleting a void* on clean up isn't a great idea
				//       the compiler warning should act as a reminder.
				// @TODO [donald] Improced void pointer to use small struct. Not used as yet so 
				//       will need to verify this actually works.

				it->second->value.pdb_voidpointer->pfn_delete(it->second->value.pdb_voidpointer->data);
				it->second->value.pdb_voidpointer = 0;
				break;
			}
		}
		
		CPParamDef* param_def = it->second;
		delete param_def;
		param_def = 0;

		m_paramList.erase(it);

		it = next;
	}
}

 //=======================
 // Implementation for parameter handling functions.
 void CPParamList::AddParam(const char* name, CPPluginArg *arg, CPPluginArgType type)
 {
	 m_paramList.insert(std::make_pair(name, new CPParamDef(type, *arg)));

 }


bool CPParamList::SetParam(const char* name, CPPluginArg *arg, CPPluginArgType type)
{
	// Check if it exists.
	ParamList_t::iterator iter;
	iter = m_paramList.find(name);

	if (iter == m_paramList.end()) // Not found so just insert.
	{
		 m_paramList.insert(std::make_pair(name, new CPParamDef(type, *arg)));
		 return true;
	}
	else // We did find the name so check type and update.
	{
		if (iter->second->type == type ) // types is correct so update
		{
			iter->second->value = *arg;
			return true;
		}
		else
		{
			printf("Error: Wrong type for argument in CPParamList::SetParam(...)\n");
			return false;
		}

	}

}

int CPParamList::RemoveParam(const char* name, CPPluginArg& arg, CPPluginArgType type)
{
	int ret = 0;

	// Find param in list.
	ParamList_t::iterator iter = m_paramList.find(name);
	if(iter == m_paramList.end()) // Not Found.
	{
		ret = 0;
	}
	else
	{
		if(iter->second->type != type)
		{
			ret = -1; // Error Wrong type.
		}
		else
		{
			CPParamDef* param_def = iter->second;
			switch(iter->second->type)
			{
				case PDB_INT:
				{
					arg.pdb_int = param_def->value.pdb_int;
					break;
				}
				case PDB_FLOAT:
				{
					arg.pdb_float = param_def->value.pdb_float;
					break;
				}
				case PDB_CSTRING:
				{
					arg.pdb_cstring = param_def->value.pdb_cstring;
					break;
				}
				case PDB_ABSTRACTBUF:
				{
					arg.pdb_abstractbuf = param_def->value.pdb_abstractbuf;
					break;
				}
				case PDB_DRAWABLE:
				{
					arg.pdb_drawable = param_def->value.pdb_drawable;
					break;
				}
				case PDB_CPIMAGE:
				{
					arg.pdb_cpimage = param_def->value.pdb_cpimage;
					break;
				}
				case PDB_VOIDPOINTER:
				{
					arg.pdb_voidpointer = iter->second->value.pdb_voidpointer;
					break;
				}
			}

			// we return the parameter, ownership passes to the call
			// we can now remove the element from the paramlist and destroy the CPParamDef
			m_paramList.erase(iter);
			delete param_def;
			param_def = 0;

			ret = 1;
		}
	}

	return(ret);
}


 int CPParamList::GetParam(const char* name, int *val)
 {
	 // Find param in list.
	ParamList_t::iterator iter;
	iter = m_paramList.find(name);
	if (iter == m_paramList.end()) // Not Found.
		return 0;
	else
		if (iter->second->type != PDB_INT )
			return -1; // Error Wrong type.
		else
		{
			*val = iter->second->value.pdb_int;
			return  1;
		}
 }

 int CPParamList::GetParam(const char* name, float *val)
 {
	 // Find param in list.
	ParamList_t::iterator iter;
	iter = m_paramList.find(name);
	if (iter == m_paramList.end()) // Not Found.
		return 0;
	else
		if (iter->second->type != PDB_FLOAT )
			return -1; // Error Wrong type.
		else
		{
			*val = (float)iter->second->value.pdb_float;
			return  1;
		}
 }

 int CPParamList::GetParam(const char* name, char* *val)
 {
	 // Find param in list.
	ParamList_t::iterator iter;
	iter = m_paramList.find(name);
	if (iter == m_paramList.end()) // Not Found.
		return 0;
	else
		if (iter->second->type != PDB_CSTRING )
			return -1; // Error Wrong type.
		else
		{
			*val = iter->second->value.pdb_cstring;
			return  1;
		}
 }

// @TODO [claw] need to re-think this deleting a void* on clean up isn't a great idea
int CPParamList::GetParam(const char* name, void **val)
{
	// Find param in list.
	ParamList_t::iterator iter;
	iter = m_paramList.find(name);
	if (iter == m_paramList.end()) // Not Found.
		return 0;
	else
		if (iter->second->type != PDB_VOIDPOINTER )
			return -1; // Error Wrong type.
		else
		{
			*val = iter->second->value.pdb_voidpointer;
			return  1;
		}
}

  int CPParamList::GetParam(const char* name, AbstractBuf **val)
 {
	 // Find param in list.
	ParamList_t::iterator iter;
	iter = m_paramList.find(name);
	if (iter == m_paramList.end()) // Not Found.
		return 0;
	else
		if (iter->second->type != PDB_ABSTRACTBUF )
			return -1; // Error Wrong type.
		else
		{
			*val = iter->second->value.pdb_abstractbuf;
			return  1;
		}
 }
 
/**
 * Retrive a Drawable parameter from the list
 * @param name the name of the parameter required.
 * @param val pointer to recieve the value of the parameter.
 * @return The status of the call,  1 => found valid entry, 
 *									 0 => parameter name not found 
 *									-1 => paramtere found but wrong type requested.
 */
int CPParamList::GetParam(const char* name, Drawable **val)
{
	// Find param in list.
	ParamList_t::iterator iter;
	iter = m_paramList.find(name);
	if(iter == m_paramList.end()) // Not Found.
	{
		return 0;
	}
	else
	{
		if(iter->second->type != PDB_DRAWABLE)
		{
			return -1; // Error Wrong type.
		}
		else
		{
			*val = iter->second->value.pdb_drawable;
			return(1);
		}
	}
}

 /**
  * Retrive a CinePaintImage parameter from the list
  * @param name the name of the parameter required.
  * @param val pointer to recieve the value of the parameter.
  * @return The status of the call,  1 => found valid entry, 
  *									 0 => parameter name not found 
  *									-1 => paramtere found but wrong type requested.
  */
int CPParamList::GetParam(const char* name, CinePaintImage **val)
{
	// Find param in list.
	ParamList_t::iterator iter;
	iter = m_paramList.find(name);
	if(iter == m_paramList.end()) // Not Found.
	{
		return 0;
	}
	else
	{
		if(iter->second->type != PDB_CPIMAGE)
		{
			return -1; // Error Wrong type.
		}
		else
		{
			*val = iter->second->value.pdb_cpimage;
			return(1);
		}
	}
}

/**
 * Sets type to the parameter type of the specified parameter.
 * If the specified parameter is held by this CPParamList, the type parameter
 * is set to the parameter type of the specified parameter and true is returned,
 * otherwise false is returned
 *
 * @param name the name of the parameter to determine toe type of
 * @param type set to the type of the named parameter
 * @return true if the parameter is found, false otherwise
 */
bool CPParamList::GetParamType(const char* name, CPPluginArgType& type) const
{
	bool ret = false;

	ParamList_t::const_iterator citer = m_paramList.find(name);
	if(citer != m_paramList.end())
	{
		type = citer->second->type;
		ret = true;
	}

	return(ret);
}

/**
 * Populates the names set with the set of parameter names held by this CPParamList.
 *
 * @param names populated with the names of the parameters held by this CPParamList.
 * @return the names parameter populated with parameter names
 */
std::set<std::string>& CPParamList::GetParamNames(std::set<std::string>& names) const
{
	for(ParamList_t::const_iterator citer = m_paramList.begin(); citer != m_paramList.end(); ++citer)
	{
		names.insert(citer->first);
	}

	return(names);
}


