/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Procedure/Plugin Database
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: PDB.cpp,v 1.4 2006/12/21 11:18:08 robinrowe Exp $
 */ 
 
#include "PDB.h"
#include <string>

using namespace std;

void PDB::Register(CinePaintPluginInfo *info)
{

	m_procDatabase.insert(make_pair(info->GetName(), info));
	printf("PDB::Register() %s\n", info->GetName());

}

/**
 * Register a file load handler in the DB.
**/
void PDB::RegisterLoadHandler(CinePaintFileHandlerInfo *info)
{
	m_fileLoadHandlers.insert(make_pair(info->GetName(), info));
	printf("PDB::RegisterLoadHandler() %s\n", info->GetName());
}

/**
 * Register a file save handler in the DB.
**/
void PDB::RegisterSaveHandler(CinePaintFileHandlerInfo *info)
{
	m_fileSaveHandlers.insert(make_pair(info->GetName(), info));
	printf("PDB::RegisterSaveHandler() %s\n", info->GetName());
}

void PDB::Unregister(char* name)
{
	m_procDatabase.erase(name);
}

CinePaintPluginInfo *PDB::LookupPlugin(const char* name)
{
	ProcMap ::iterator iter;
	iter = m_procDatabase.find(name);
	if (iter != m_procDatabase.end() )
        return iter->second;
	else
		return 0;
}

/**
 * Lookup a load handler plugin within the load handlers database
 *
 * @param name The name of the load handler to look for
 * @return Returns the CinePaintFileHandlerInfo for the load handler with the given name
 *         or 0 if no handler is registered for the given name
 */
CinePaintFileHandlerInfo* PDB::LookupLoadHandler(const char* name)
{
	CinePaintFileHandlerInfo* _ret = 0;

	FileHandlerMap::iterator _iter = m_fileLoadHandlers.find(name);
	if(_iter != m_fileLoadHandlers.end())
	{
		_ret = _iter->second;
	}

	return(_ret);
}

/**
 * Lookup a save handler plugin within the save handlers database
 *
 * @param name The name of the save handler to look for
 * @return Returns the CinePaintFileHandlerInfo for the save handler with the given name
 *         or 0 if no handler is registered for the given name
 */
CinePaintFileHandlerInfo* PDB::LookupSaveHandler(const char* name)
{
	CinePaintFileHandlerInfo* _ret = 0;

	FileHandlerMap::iterator _iter = m_fileSaveHandlers.find(name);
	if(_iter != m_fileSaveHandlers.end())
	{
		_ret = _iter->second;
	}

	return(_ret);
}


CinePaintPlugin *PDB::CreateInstance(const char* name)
{
	CinePaintPlugin *_plugin = 0;
	CinePaintPluginInfo *_info = LookupPlugin(name);

	if(_info)
	{
		_plugin = _info->GetCtor()();
	}

	return(_plugin);
}

CinePaintPlugin* PDB::GetPluginInstance(const char* name, PluginType plugin_type)
{
	CinePaintPlugin* _plugin = 0;

	switch(plugin_type)
	{
		case STD_PLUGIN_ENUM:
		{
			_plugin = CreateInstance(name);
			break;
		}
		case LOAD_HANDLER_ENUM:
		{
			CinePaintFileHandlerInfo* _info = LookupLoadHandler(name);
			if(_info)
			{
				_plugin = _info->GetCtor()();
			}
			break;
		}
		case SAVE_HANDLER_ENUM:
		{
			CinePaintFileHandlerInfo* _info = LookupSaveHandler(name);
			if(_info)
			{
				_plugin = _info->GetCtor()();
			}
			break;
		}
		case PLUGIN_TYPE_UNSPECIFIED:
		default:
		{
			// first try the plugin
			_plugin = CreateInstance(name);
			if(!_plugin)
			{
				_plugin = GetPluginInstance(name, LOAD_HANDLER_ENUM);
			}

			if(!_plugin)
			{
				_plugin = GetPluginInstance(name, SAVE_HANDLER_ENUM);
			}
		}
	}

	return(_plugin);
}

void PDB::ReleaseInstance(CinePaintPlugin *plugin)
{
	if(!plugin)
	{
		return;
	}

	CinePaintPlugin::PFNDeleteCinePaintPlugin _del_func = plugin->GetDeleteFunc();
	if(_del_func)
	{
		_del_func(plugin);
	}
	else
	{
		delete(plugin);
	}

	plugin = 0;
}

const char* PDB::GetFileTypeString()
{
	char* types;
	string a;
	a = "Image Files (*.{";
	FileHandlerMap::iterator iter = m_fileLoadHandlers.begin();
	while (iter != m_fileLoadHandlers.end())
	{
		a.append(iter->second->GetExtensions());
		iter++;
		if ( iter != m_fileLoadHandlers.end())
			a.append(",");
	}
	a.append("})");
	types = new char[a.length()+1];
	strcpy(types, a.c_str());
	return types;

}

CinePaintPlugin::PFNCreateCinePaintPlugin PDB::GetLoadHandler(const char* filename)
{	if(!filename)
	{	return 0;
	}
	const char* ext = get_filename_ext(filename);
	CinePaintFileHandlerInfo *h = find_file_load_handler(ext);
	if(!h)
	{	return 0;
	}
	CinePaintPlugin::PFNCreateCinePaintPlugin ret = h->GetCtor();
	return ret ;
}

CinePaintPlugin::PFNCreateCinePaintPlugin PDB::GetSaveHandler(const char* filename)
{	if(!filename)
	{	return 0;
	}
	const char* ext = get_filename_ext(filename);
	CinePaintFileHandlerInfo *h = find_file_save_handler(ext);
	if(!h)
	{	return 0;
	}
	CinePaintPlugin::PFNCreateCinePaintPlugin ret = h->GetCtor();
	return ret;
}


/**
 * returns an iteraotr pointing to the beginning of the registered plugins
 * The iterator iterates over PDB::PluginPair's, representing a plugin name, and
 * the registered plugin data.
 *
 * @return an iterator pointing to the beginning of the registered plugins
 */
PDB::pdb_const_iterator PDB::PluginsBegin() const
{
	return(m_procDatabase.begin());
}

/**
 * returns an iteraotr pointing past the end of the registered plugins
 * The iterator iterates over PDB::PluginPair's, representing a plugin name, and
 * the registered plugin data.
 *
 * @return an iterator pointing to the beginning of the registered plugins
 */
PDB::pdb_const_iterator PDB::PluginsEnd() const
{
	return(m_procDatabase.end());
}

/**
 * returns an iterator pointing to the beginning of the registered file load handler plugins
 * The iterator iterates over PDB::FileHandlerPair's, representing a plugin name, and
 * the registered plugin data.
 *
 * @return an iterator pointing to the beginning of the registered file load handler plugins
 */
PDB::fh_const_iterator PDB::LoadHandlersBegin() const
{
	return(m_fileLoadHandlers.begin());
}

/**
 * returns an iterator pointing past the end of the registered file load handler plugins
 * The iterator iterates over PDB::FileHandlerPair's, representing a plugin name, and
 * the registered plugin data.
 *
 * @return an iterator pointing to the beginning of the registered file load handler plugins
 */
PDB::fh_const_iterator PDB::LoadHandlersEnd() const
{
	return(m_fileLoadHandlers.end());
}

/**
 * returns an iterator pointing to the beginning of the registered file save handler plugins
 * The iterator iterates over PDB::FileHandlerPair's, representing a plugin name, and
 * the registered plugin data.
 *
 * @return an iterator pointing to the beginning of the registered file save handler plugins
 */
PDB::fh_const_iterator PDB::SaveHandlersBegin() const
{
	return(m_fileSaveHandlers.begin());
}

/**
 * returns an iterator pointing past the end of the registered file save handler plugins
 * The iterator iterates over PDB::FileHandlerPair's, representing a plugin name, and
 * the registered plugin data.
 *
 * @return an iterator pointing to the beginning of the registered file save handler plugins
 */
PDB::fh_const_iterator PDB::SaveHandlersEnd() const
{
	return(m_fileSaveHandlers.end());
}



const char* PDB::get_filename_ext(const char* filepath)
{	const char* dot=strchr(filepath,'.');
	if(!dot)
	{	return 0;
	}
	for(;;)
	{	const char* ptr=strchr(dot+1,'.');
		if(!ptr)
		{	return dot+1;
		}
		dot=ptr;
	}
	return 0;
}


CinePaintFileHandlerInfo *PDB::find_file_load_handler(const char* ext)
{	Dump();
	FileHandlerMap::iterator iter = m_fileLoadHandlers.begin();
	while (iter != m_fileLoadHandlers.end())
	{	if(iter->second->CheckExtension(ext))
		{	return iter->second;
		}
		iter++;
	}
	return 0;
}

CinePaintFileHandlerInfo *PDB::find_file_save_handler(const char* ext)
{	bool found = false;
	FileHandlerMap::iterator iter = m_fileSaveHandlers.begin();
	while (iter != m_fileSaveHandlers.end() && !found)
	{		found = iter->second->CheckExtension(ext);
		if (!found)
            iter++;
	}
	if (found)
		return iter->second;
	return 0;
}

void PDB::Dump()
{	ProcMap ::iterator iter = m_procDatabase.begin();
	puts("PDB::Dump()");
	while (iter != m_procDatabase.end())
	{	printf("Procedure : %s\n", iter->second->GetName());
		iter++;
	}
	for (iter=m_procDatabase.begin(); iter!=m_procDatabase.end(); ++iter)
		printf("Procedure : %s\n", iter->second->GetName());
}
