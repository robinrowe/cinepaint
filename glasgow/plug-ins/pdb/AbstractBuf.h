/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      AbstractBuf - Abstract Image data container class.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * Based on the GIMP original buffer by Spencer Kimball and Peter Mattis
 *
 * $Id: AbstractBuf.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _ABSTRACTBUF_H_
#define _ABSTRACTBUF_H_

#include "dll_api.h"

#include "CinePaintTag.h"

// forward declaration
class Pixel;
class PixelRow;
class PixelArea;

/**
 * AbstractBuf represents the base class for all types of image data buffers.
 * Image data storage buffers should derive from this class creating usable data buffer types
 *
 */
class CINEPAINT_IMAGE_API AbstractBuf
{
	public:
		/**
		 * Destructor
		 */
		virtual ~AbstractBuf();

		/**
		 * Defined reference types
		 */
		enum RefType
		{
			REFTYPE_NONE  = 0,
			REFTYPE_READ  = 1,
			REFTYPE_WRITE = 2
		};

		//-------------
		// Tag / Format

		/**
		 * Returns the CinePaintTag indicating the data format of this AbstractBuf
		 *
		 * @return the CinePaintTag indicating the data format of this AbstractBuf
		 */
		CinePaintTag& GetTag();
		const CinePaintTag& GetTag() const;

		/**
		 * Sets the CinePaintTag indicating the data format of this AbstractBuf
		 *
		 * @param the CinePaintTag indicating the data format of this AbstractBuf
		 */
		void SetTag(const CinePaintTag& tag);



		//--------------------------
		// Width / Height / Position

		/**
		 * Returns the width in pixels of this AbstractBuf
		 *
		 * @return the width in pixels of this AbstractBuf
		 */
		int GetWidth() const;

		/**
		 * Returns the height in pixels of this AbstractBuf
		 * 
		 * @return the height in pixels of this AbstractBuf
		 */
		int GetHeight() const;


		/**
		 * returns the x value of the top left coordinate of the portion the specified pixel lies on
		 *
		 * @param x the x coordinate of the pixel
		 * @param y the y coordinate of the pixel
		 * @return the x value of the top left coordinate the pixel lies on
		 */
		virtual int GetPortionX(int x, int y) const = 0;

		/**
		 * returns the y value of the top left coordinate of the portion the specified pixel lies on
		 *
		 * @param x the x coordinate of the pixel
		 * @param y the y coordinate of the pixel
		 * @return the y value of the top left coordinate the pixel lies on
		 */
		virtual int GetPortionY(int x, int y) const = 0;

		/**
		 * Returns the max width of the portion that has the specified top left pixel
		 * This method returns -1 if either the x or y value is outwith the bounds
		 * of this AbstractBuf
		 *
		 * @param x the x value of the top left corner of the portion
		 * @param y the y value of the top left corner of the portion
		 * @return the width of the portion
		 */
		virtual int GetPortionWidth(int x, int y) const = 0;

		/**
		 * Returns the max height of the portion that has the specified top left pixel
		 * This method returns -1 if either the x or y value is outwith the bounds
		 * of this AbstractBuf
		 *
		 * @param x the x value of the top left corner of the portion
		 * @param y the y value of the top left corner of the portion
		 * @return the height of the portion
		 */
		virtual int GetPortionHeight(int x, int y) const = 0;



		//------------
		// data access

		/**
		 * Returns a pointer to the canvas data for the portion with the specified top left corner
		 *
		 * @param x the x value of the top left corner of the portion
		 * @param y the y value of the top left corner of the portion
		 * @return a pointer to the canvas data for the specified portion
		 */
		virtual unsigned char* GetPortionData(int x, int y) = 0;
		virtual const unsigned char* GetPortionData(int x, int y) const = 0;


		/**
		 * @TODO [DWM] : Make this work properly just a hack for test.
		 * Get a PixelArea for an AbstractBuf, this is really just a ROI,
		 * but needs to be here for const version.
		**/
		virtual const PixelArea* GetPixelArea(int x, int y, int w, int h) const;
		virtual PixelArea* GetPixelArea(int x, int y, int w, int h);


		/**
		 * Returns a PixelRow of data from this AbstractBuf
		 * The PixelRow is a consecutive row of data from this AbsrtactBuf. Usually the
		 * PixelRow represents a single row of data within this AbstractBuf, although
		 * the consecutive area may span multiple rows if the width paramater is greater than
		 * the width of this AbstractBuf.
		 * The PixelRow contains a copy of data from this AbstractBuf at the time this method
		 * is called.
		 *
		 * @note [claw] we cant provide direct access to the backing data because we cant gaurantee
		 *       how the AbstractBuf implementation handles its data. The implementation may require
		 *       to move data around, ie TileBuf, to populate the complete row.
		 * @param pixelrow the PixelRow to set with data from this AbstractBuf
		 * @param row the row within this AbstractBuf the PixelRow Represents
		 * @param offset an offset from the beginning of the row within this AbstractBuf in pixels
		 * @param pixels the number of pixels to copy
		 * @return true if the PixelRow wa spopulates with data, false if the range specified
		 *         falls outside the data area of this AbstractBuf
		 */
		virtual bool GetPixelRow(PixelRow& pixelrow, int row, int offset, int pixels) = 0;

		/**
		 * Copies the data contained within the specified PixelRow into this AbstractBuf.
		 * The data overwrites exsiting data starting at offset pixels into the specified row
		 * within this AbstractBuf for pixels pixels.
		 * If pixels is greater than the width of pixelrow, pixels is limited to the pixelrow width
		 *
		 * @param pixelrow the PixelRow to copy into this AbatractBuf
		 * @param row the row within this AbstractBuf to copy data into
		 * @param offset number of pixels from the start of the AbstractBuf row to copy data into.
		 * @param pixels the number of pixels from pixelrow to copy.
		 * @return true if the PixelRow wa spopulates with data, false if the range specified
		 *         falls outside the data area of this AbstractBuf
		 */
		virtual bool WriteRow(PixelRow& pixelrow, int row, int offset, int pixels) = 0;

		/**
		 * Populates the specified Pixel with image data from the specified point within this AbstractBuf
		 * If the specified location falls outwith the area of this AbstractBuf, no further action is taken
		 * and false is returned.
		 * Pixel is assumed to be in the same format as this AbstractBuf, if this is not the case, false is
		 * returned and no data is copied
		 *
		 * @param pixel the Pixel to Write data into
		 * @param x the x location in pixels
		 * @param y the y location in pixels
		 * @return false if the Pixel Tag format and the AbstractBuf Tag format do not match
		 */
		virtual bool GetPixel(Pixel& pixel, int x, int y) = 0;

		/**
		 * Writes the specified Pixel into the specified location of this AbstractBuf
		 * If the specified location falls outwith this AbstractBuf, no further action is taken.
		 *
		 * @param pixel the Pixel to Write
		 * @param x the x location in pixels
		 * @param y the y location in pixels
		 * @return false if the Pixel Tag format and the AbstractBuf Tag format do not match
		 */
		virtual bool WritePixel(Pixel& pixel, int x, int y) = 0;

		/**
		 * Returns direct access to the specified pixel within the AbstractBuf backing this PixelArea
		 * If the specified location falls outwith this PixelArea 0 is returned.
		 *
		 * @note [claw] this method has the same effect as GetPortionData(x,y), however I want to
		 *       make clear the intended use of each.
		 * @param x the x location in pixels
		 * @param y the y location in pixels
		 */
		virtual unsigned char* GetBufferPixel(int x, int y) = 0;

		/**
		 * Returns the rowstride for the portion with the specified top left corner
		 *
		 * @param x the x value of the top left corner of the portion
		 * @param y the y value of the top left corner of the portion
		 * @return the rowstride of the portion
		 */
		virtual int GetPortionRowstride(int x, int y) const = 0;


		//---------------------------------------------------------
		// allocate and/or swap in the backing store for this pixel

		/**
		 * Returns trus if the portion with the specified top left corner has been allocated
		 *
		 * @return trus if the portion with the specified top left corner is allocated, false otherwise
		 */
		virtual bool IsPortionAlloced(int x, int y) const = 0;

		/**
		 * Allocate and/or swap in the portion for the the specified pixel
		 * The portion if referenced read only
		 *
		 * @param x the x coordinate of the pixel
		 * @param y the y coordinate of the pixel
		 * @return true if the portion was sucessfully referenced, false otherwise
		 */
		virtual bool RefPortionReadOnly(int x, int y) = 0;

		/**
		 * Allocate and/or swap in the portion for the the specified pixel
		 * The portion if referenced read/write only
		 *
		 * @param x the x coordinate of the pixel
		 * @param y the y coordinate of the pixel
		 * @return true if the portion was sucessfully referenced, false otherwise
		 */
		virtual bool RefPortionReadWrite(int x, int y) = 0;

		/**
		 * Unreferences the portion for the the specified pixel
		 *
		 * @param x the x coordinate of the pixel
		 * @param y the y coordinate of the pixel
		 * @return true if the portion was sucessfully unreferenced, false otherwise
		 */
		virtual bool UnrefPortion(int x, int y) = 0;



		/**
		 * Returns whether this AbstractBuf auto allocates new memory for portions/tiles not already in memory
		 *
		 * @return true if this AbstractBuf Auto Allocates
		 */
		virtual bool IsAutoAlloc() const;

		/**
		 * Sets the AutoAlloc state of this AbstractBuf
		 * If AutoAlloc is set true, memory for a tile/portion not already in
		 * memory will be automatically allocated
		 *
		 * @param aa the AutoAlloc state of this AbstractBuf
		 */
		virtual void SetAutoAlloc(bool aa);



		//-----------------------------------
		// Copy Regions of Interest/PixelArea

		// @note [claw] TODO
		//       The regions of interst is a rectangle area over this AbstractBuf
		//       it does not caontain data formatted for its width height etc.
		//       We can provide a copy region method here to copy data as required
		//       so we have a new AbstractBuf with real data



		//--------------------------------------------
		// Initialize the backing store for this pixel

		/**
		 * Portion Initialization Function Object
		 * This class should be subclassed to create usable PortionInitization objects to handle
		 * initializing a portion of an AbstractBuf. Initialization may be as simple as to clear
		 * the entire buffer
		 *
		 */
		class PortionInitializer
		{
			public:
				virtual ~PortionInitializer() {};
				virtual bool InitPortion(AbstractBuf&, int x, int y, int width, int height) = 0;

			protected:
				PortionInitializer() {} ;
			private:
		};


		/**
		 * Sets the current PortionInitializer to initialize portions of this AbstractBuf
		 * If no initialization is required, portion_initializer may be set to zero.
		 * The set PortionInitializer is not managed by this class.
		 *
		 * @param portion_initializer the PortionInitializer used to initialize portions of this AbstrractBuf
		 */
		void SetPortionInitializer(PortionInitializer* portion_initializer);

		/**
		 * Initialize the specified portion of the buffer using the currently set PortionInitializer.
		 * If no PortionInitializer is set, no initialization of the AbstractBuf takes place
		 *
		 * @param x
		 * @param y
		 * @param w
		 * @param h
		 * @return
		 */
		bool InitPortion(int x, int y, int w, int h);


	protected:
		/**
		 * Protected Constructor, this class must be subclassed to create useable types
		 *
		 * @param tag the CinePaintTag descibibg the image data format
		 * @param w the width in pixels of this AbstractBuf
		 * @param h the height in pixels of this AbstractBuf
		 */
		AbstractBuf(const CinePaintTag& tag, int w, int h) ;


		//-----------------------
		// alloc / dealloc memory

		/**
		 * Allocates memory for the portion with the specified top left corner
		 *
		 * @param x the x value of the top left corner of the portion
		 * @param y the y value of the top left corner of the portion
		 * @return true if memory for the portion was successfully allocated
		 */
		virtual bool AllocPortion(int x, int y) = 0;

		/**
		 * Unallocates memory for the portion with the specified top left corner
		 *
		 * @param x the x value of the top left corner of the portion
		 * @param y the y value of the top left corner of the portion
		 * @return true if memory for the portion was successfully Unallocated
		 */
		virtual bool UnallocPortion(int x, int y) = 0;


		//-------------------
		// Reference Counting

		/**
		 * Increment the reference count by one for this AbstractBuf
		 *
		 * @return the new reference count
		 */
		int IncRefCount();

		/**
		 * Decrement the reference count of this AbstractBuf by one
		 *
		 * @return the new reference count
		 */
		int DecRefCount();

		/**
		 * Returns the current reference count of this AbstractBuf
		 *
		 * @return the current reference count of this AbstractBuf
		 */
		int GetRefCount() const;

	private:

		/** should a ref of a non-existent area allocate the memory? */
		bool m_autoalloc;

		/** indicates the image data buffer type */
		CinePaintTag m_tag;

		/** the width in pixels of the whole AbstractBuf */
		int m_width;

		/** the height in pixels of the whole AbstractBuf */
		int m_height;

		/** reference count of this AbstractBuf */
		int m_ref_count;


		/** handles intialiazing a portion of the buffer */
		PortionInitializer* m_portion_initializer;

}; /* class AbstractBuf */


#endif /* _ABSTRACTBUF_H_ */
