// World.h: World object to pass by reference to plug-ins
// Copyright 2006 Robin.Rowe@CinePaint.org
// License OSI MIT

#ifndef WORLD_H
#define WORLD_H

class PDB;

struct World
{	PDB* pdb;
};

#endif
