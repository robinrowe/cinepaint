/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Image Operation plugin loading module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ImageOpManager_unix.cpp,v 1.3 2008/01/28 18:22:28 robinrowe Exp $
 */

#ifndef WIN32

#include <ImageOpManager.h>
#include <CinePaintApp.h>
// #include <utility/PathIterator.h>
#include <app/AppSettings.h>
#include <dirent.h>
#include <sys/types.h>
#include  <dlfcn.h>

static const char* IMAGE_OP_SUFFIX = "image-ops";

/**
 * Initializes this ImageOpManager by loading available CinePaintImageOp plugins from an application specified path
 *
 */
void ImageOpManager::Initialize()
{
	const char* _plugin_full_path=GetPathImageOpsPlugin();

	// Pointer to the create plugin functions
	CinePaintImageOp::PFNCreateCinePaintImageOp* _ImageOpCtor = 0;
	CinePaintImageOp::PFNGetImageOpClassList _ImageOpListFunc = 0;

	const char* _dlErrMsg = 0;
	
        // open the module
	void* _plugin_lib = dlopen(_plugin_full_path, RTLD_LAZY);

	if(_plugin_lib)
	{
		// keep the opened handle so we can clean up properly at shutdown
		//m_open_plugins.push_back(_plugin_lib);

		_ImageOpListFunc = (CinePaintImageOp::PFNGetImageOpClassList)dlsym(_plugin_lib, "GetImageOpClassList");

		if(_ImageOpListFunc)
		{
			_ImageOpCtor = _ImageOpListFunc();
			while(*_ImageOpCtor)
			{
				CinePaintImageOp* _image_op = (*_ImageOpCtor)();

				if(_image_op)
				{
					RegisterImageOp(_image_op);
				}

				//init_progress_update();
				++_ImageOpCtor;
			}
		}
		else
		{
			_dlErrMsg = dlerror();
			printf("Failed to load Image Op Plugin, %s, No entry point not Found\nError is: %s\n", _plugin_full_path, _dlErrMsg);
		}
	}
	else
	{
		_dlErrMsg = dlerror();
		printf("Failed to open Image Op Plugin: %s\nError is: %s\n", _plugin_full_path, _dlErrMsg);
	}


/****  The following code is made to handle many objects in a path. Current code only handles a single dynamically loadable library

	// The directory from which to load the plugins
	char _op_dir[PATH_MAX + 1];
	char* _tmp_dir;

	if(_plugin_full_path)
	{
		strncpy(_op_dir, _tmp_dir, strlen(_tmp_dir) +1);
	}
	else
	{
		printf("Warning: No Image-Op Plugin path defined, trying \"%s/%s/\" instead\n", _app_rc.GetSysDir(), IMAGE_OP_SUFFIX);
		strncpy(_op_dir, _app_rc.GetSysDir(), strlen(_app_rc.GetSysDir()) +1);
		if(strncmp(_op_dir + strlen(_op_dir) -1, "/", 1) != 0)
		{
			strcat(_op_dir, "/");
		}
		strcat(_op_dir, IMAGE_OP_SUFFIX);
	}

	char _full_path[PATH_MAX + 1];
	char _info_txt[PATH_MAX + 1];

	// Pointer to the create plugin functions
	CinePaintImageOp::PFNCreateCinePaintImageOp* _ImageOpCtor = 0;
	CinePaintImageOp::PFNGetImageOpClassList _ImageOpListFunc = 0;

	const char* _dlErrMsg = 0;
	
	
	// there might be more than one path returned, so use the PathIterator to iterate over them
	PathIterator _dir_iter(_op_dir);

	
	_dir_iter.begin();
	while(_dir_iter.path)
	{
		struct dirent* _item;
		DIR* _dir = 0;

		if((_dir = opendir(_dir_iter.path)) != 0)
		{
			//int _plugin_count = 0;
			//init_progress_reset(0, _plugin_count, 1);

			while((_item = readdir(_dir)) != 0)
			{
				// Image Op Plugins have a '.so' extension and are not dotfiles
				// there is no requirement for a 'lib' prefix, which is probably only confusing anyways
				// but if is not refused either
				if((strlen(_item->d_name) > 3) && (strncmp(_item->d_name, ".", 1) != 0) &&
				        (strncmp(_item->d_name + strlen(_item->d_name) - 3, ".so", 3) == 0))
				{
					snprintf(_full_path, sizeof(_full_path), "%s/%s", _dir_iter.path, _item->d_name);
					snprintf(_info_txt, sizeof(_info_txt), "Loading Image: %s", _item->d_name);
					//init_progress_set_text(_info_txt);

					// open the module
					void* _plugin_lib = dlopen(_full_path, RTLD_LAZY);

					if(_plugin_lib)
					{
						// keep the opened handle so we can clean up properly at shutdown
						//m_open_plugins.push_back(_plugin_lib);

						_ImageOpListFunc = (CinePaintImageOp::PFNGetImageOpClassList)dlsym(_plugin_lib, "GetImageOpClassList");

						if(_ImageOpListFunc)
						{
							_ImageOpCtor = _ImageOpListFunc();
							while(*_ImageOpCtor)
							{
								CinePaintImageOp* _image_op = (*_ImageOpCtor)();

								if(_image_op)
								{
									RegisterImageOp(_image_op);
								}

								//init_progress_update();
								++_ImageOpCtor;
							}
						}
						else
						{
							_dlErrMsg = dlerror();
							printf("Failed to load Image Op Plugin, %s, No entry point not Found\nError is: %s\n", _full_path, _dlErrMsg);
						}
					}
					else
					{
						_dlErrMsg = dlerror();
						printf("Failed to open Image Op Plugin: %s\nError is: %s\n", _full_path, _dlErrMsg);
					}
				}
			}

			closedir(_dir);
		}
		else
		{
			printf("Could not open Image Op Plugin Dir: %s\n", _op_dir);
		}

		++_dir_iter;
	}
*/

	return;
}

#endif

