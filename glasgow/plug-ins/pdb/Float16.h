/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Float16 - 16Bit Float pixel representation (as a Class)
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: Float16.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _FLOAT16_H_
#define _FLOAT16_H_

#include "dll_api.h"

/**
 * A 16 Bit float data type
 *
 * The following text is an extract from a post by Drew Hess on the the film-gimp mailing list datad 2003-03-13 02:04.
 * (http://sourceforge.net/mailarchive/forum.php?thread_id=1823849&forum_id=10511)
 *
 * "RnH made a clever hack to create 16-bit floating point numbers.  An IEEE
 *  32-bit floating-point number has 1 sign bit, an 8-bit exponent, and a
 *  23-bit mantissa.  RnH took the sign bit, the 8-bit exponent, and the first
 *  7 most significant bits of the mantissa of a 32-bit float to create their
 *  16-bit float.  It"s a fast conversion, since there"s no special roundng or
 *  range-checking needed.
 * 
 *  The 16-bit floats are only used for storage.  Calculations are done using
 *  the CPU"s FP hardware by upconverting the 16-bit floats to 32-bit floats
 *  first, and then downconverting them back to 16-bit floats when the
 *  calculation is finished.  The 16-bit float bit patterns are stored in
 *  guint16 variables."
 *
 */
class CINEPAINT_IMAGE_API Float16
{
	public:
		/**
		 * Constructs a new Float16 from the specified float value
		 *
		 * @param f float representation to initialize this Float16
		 */
		Float16(float f);

		/**
		 * Constructs a new Float16 initialized with the specified unsigned short
		 *
		 * @param f16 float16 representation stored as a single unsigned short to initialize this Float16
		 */
		Float16(unsigned short f16);

		/**
		 * Returns the of this Float16 as a float
		 *
		 * @return the float value of this Float16
		 */
		float GetFloat() const;

		/**
		 * Returns the Float16 value of this Float16 as an unsigned short
		 *
		 * @return the float16 value of this Float16 as an unsigned short
		 */
		unsigned short GetFloat16() const;

		/**
		 * Converts the specified Float16 value stored as a single unsigned short to its float representation
		 *
		 * @param f16 the float16 value to convert
		 * @return the float representation of the specified float16 value
		 */
		static float ToFloat(unsigned short f16);

		/**
		 * Converts the specified float value to its float16 representation stored as a single unsigned short
		 *
		 * @param f the float to convert
		 * @return the float16 representation stored as a single unsigned short
		 */
		static unsigned short ToFloat16(float f);

	protected:

	private:

		/** Stores the value of this Float16 as a float */
		float m_float16 ;

		/** union used to convert between types */
		union ShortsFloat
		{
			unsigned short s[2];
			float f;
		};

}; /* class Float16 */

#endif /* _FLOAT16_ */
