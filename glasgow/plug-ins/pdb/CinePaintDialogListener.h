/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintDialogListener - Asbtract Dialog listener
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintDialogListener.h,v 1.3 2006/12/18 08:21:21 robinrowe Exp $
 */

#ifndef _CINEPAINT_DIALOG_LISTENER_H_
#define _CINEPAINT_DIALOG_LISTENER_H_

#include "dll_api.h"
#include "CPPluginUIDialog.h"
#include "enums.h"

// rsr
#undef CreateDialog

/**
 * CinePaintDialogListener provides an abstract interface for a listener for dialog boxs
 * required by the plugins.
 * It is up to the listener to ensure that Thread safety on GUI function is maintained, the methods
 * of the Listener may be called in an arbitrary thread.
**/
class CINEPAINT_CORE_API CinePaintDialogListener
{
public:
	/**
	 * Destructor
	**/
	virtual ~CinePaintDialogListener() {};

	/**
	 * Create a new Dialog from the given CPPluginUIDialog
	 * @param dlg the dialog definition to implement.
	**/
	virtual void CreateDialog( char* ID, CPPluginUIDialog *dlg ) = 0;

	/**
	 * Run the dialog in model form
	 * I.e. wait for the user to hit OK or CANCEL
	**/
	virtual DialogResponse DoModal( char* ID ) = 0;

protected:
	/**
	 * Constructs a new CinePaintDialogListener - protected to avoid attempted instances of abstract class
	 *
	**/
	CinePaintDialogListener()
	{}

private:


}; /* class CinePaintDialogListener */

#endif /* _CINEPAINT_DIALOG_LISTENER_H_ */
