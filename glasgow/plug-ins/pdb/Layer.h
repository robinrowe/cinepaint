/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                     Layer - Layer object container
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: Layer.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _CINEPAINT_LAYER_H_
#define _CINEPAINT_LAYER_H_

#include "dll_api.h"
#include "Drawable.h"

class AbstractBuf;
class Boundary;

/**
 * layers extends Drawable to provide a 'Layer' of image data within a composed CinePaintImage.
 * A CinePaintImage may be composed of a stack Layers.
 *
 */
class CINEPAINT_IMAGE_API Layer : public Drawable
{
	public:
		/**
		 * Constructs an initially empty layer within the specified CinePaintImage of width by height
		 *
		 * @param tag CinePaintTag describing the data format
		 * @param width the width of the Layer
		 * @param height the height of the Layer
		 * @param name the name of this Layer
		 * @param opacity
		 */
		Layer(const CinePaintTag& tag, int width, int height, const char* name, float opacity);

		/**
		 * Constructs a new Layer from the specified AbstractBuf.
		 * The new layers copies the data from the specified AbstractBuf, and creates a Layer
		 * of the same CinePaintTag and size as the AbstractBuf.
		 *
		 * @param buf the AbstractBuf to create this Layer from
		 * @param name the name of this Layer
		 */
		Layer(const AbstractBuf& buf, const char* name);

		/**
		 * Constructs a new Layer by copying the specified Layer.
		 * The new Layer is the same size, and has the same CinePaintTag matching that of the specified
		 * Layer. The underlying image buffer is copied into a new buffer. The name of the layer
		 * is also copied as is.
		 * Currently any selections made upon the Layer are NOT copied.
		 *
		 * @param layer the layer to copy
		 */
		Layer(const Layer& layer);

		/**
		 * Destructor - destroys this Layer and the underlying image data buffer
		 */
		virtual ~Layer();



		//---------------------
		// Accessors / Mutators

		/**
		 * Returns the Opacity of this Layer.
		 * 0.0 transparent to 1.0 fully_opaque
		 *
		 * @return the opacity of this Layer
		 */
		float GetOpacity() const;

		/**
		 * Sets the Opacity of this Layer
		 *
		 * @param opacity the new opacity value
		 */
		void SetOpacity(float opacity);


		// Layer Modes
		enum LayerMode
		{
			NORMAL_MODE_ENUM,
			DISSOLVE_MODE_ENUM,
			BEHIND_MODE_ENUM,
			MULTIPLY_MODE_ENUM,
			SCREEN_MODE_ENUM,
			OVERLAY_MODE_ENUM,
			DIFFERENCE_MODE_ENUM,
			ADDITION_MODE_ENUM,
			SUBTRACT_MODE_ENUM,
			DARKEN_ONLY_MODE_ENUM,
			LIGHTEN_ONLY_MODE_ENUM,
			HUE_MODE_ENUM,
			SATURATION_MODE_ENUM,
			COLOR_MODE_ENUM,
			VALUE_MODE_ENUM,
			ERASE_MODE_ENUM,
			REPLACE_MODE_ENUM
		};

		/**
		 * Returns the current LayerMode of this Layer
		 * The LayerMode controls how this Layer is composited with other layers
		 *
		 * @return the current LayerMode of this Layer
		 */
		LayerMode GetLayerMode() const;

		/**
		 * Sets the current LayerMode of this Layer
		 * The LayerMode controls how this Layer is composited with other layers
		 *
		 * @param mode the Current LayerMode of this Layer
		 */
		void SetLayerMode(LayerMode mode);



	protected:

	private:

		/** layer opacity */
        float m_opacity;

		/** current mode of this Layer */
		LayerMode m_mode;


		//---------------
		// Nested classes

}; /* class Layer */


#endif /* _CINEPAINT_LAYER_H_ */
