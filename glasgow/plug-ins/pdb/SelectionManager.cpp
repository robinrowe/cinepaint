/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      SelectionManager - Manages selections made on images
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: SelectionManager.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */
 
#include "SelectionManager.h"
#include "AbstractBuf.h"
#include "AbstractSelectionShape.h"
#include "Drawable.h"
#include "FlatBuf.h"
#include "RectSelectShape.h"
#include "CinePaintTag.h"
#include <utility/CPRect.h>


/**
 * Constructs a new SelectionManager to manage selection made over the specified Drawable
 *
 * @param Drawable the drawable selection are made upon
 */
SelectionManager::SelectionManager(Drawable& drawable)
: m_drawable(drawable)
{
	m_selection_mask = 0;
}

/**
 * Destructor
 */
SelectionManager::~SelectionManager()
{
	if(m_selection_mask)
	{
		delete m_selection_mask;
		m_selection_mask = 0;
	}
}


//---------------------
// Accessors / Mutators

/**
 * Returns a rectangle representing the selection bounds of all selection.
 *
 * @param bounds the rectangle to set to the current selection bounds
 * @return bounds set to the current selection bounds
 */
CPRect& SelectionManager::GetSelectionBounds(CPRect& bounds) const
{
	bounds.SetRect(0, 0, 0, 0);

	CPRect _temp_bounds;
	for(SelectionContainer_t::const_iterator _citer = m_selections.begin(); _citer != m_selections.end(); ++_citer)
	{
		bounds.Add((*_citer)->GetBoundingRect(_temp_bounds));
	}

	return(bounds);
}

/**
 * Adds the specified rectangle to the current selection
 *
 * @param x top left coordinate of the selection rectangle
 * @param y top left coordinate of the selection rectangle
 * @param w the width of the selection rectangle
 * @param h the height of the selection rectangle
 */
void SelectionManager::AddSelectionRect(unsigned int x, unsigned int y, unsigned int w, unsigned int h)
{
	RectSelectShape* _rect = new RectSelectShape(x, y, w, h);
	AddSelection(_rect);
}

/**
 * Adds the specified selection shape to the current selection
 *
 * @param selection selection shape representing the newly selected area
 */
void SelectionManager::AddSelection(AbstractSelectionShape* selection)
{
	CPRect _outer_rect;
	CPRect _inner_rect;

	for(SelectionContainer_t::const_iterator _outer = m_selections.begin(); _outer != m_selections.end(); ++_outer)
	{
		AbstractSelectionShape* _outer_shape = (*_outer);
		_outer_shape->GetBoundingRect(_outer_rect);

		for(SelectionContainer_t::const_iterator _inner = m_selections.begin(); _inner != m_selections.end(); ++_inner)
		{
			if((*_inner) != _outer_shape)
			{
				AbstractSelectionShape* _inner_shape = (*_inner);
				if(_outer_rect.Intersects(_inner_shape->GetBoundingRect(_inner_rect)))
				{
					// @TODO needs some thought here!!!
					//       we either need to do something smart with the selection shapes, ie join them
					//       as a complex selection shape
					//         or
					//       we convert to using just the selction mask
					printf("Selection Shapes intersect\n");
				}
			}
		}
	}

	m_selections.push_back(selection);
}

/**
 * Update current selection with new data 
 * @param x top left coordinate of the selection rectangle
 * @param y top left coordinate of the selection rectangle
 * @param w the width of the selection rectangle
 * @param h the height of the selection rectangle
 * updates the element on top of the stack, i.e. the last add selection, if no selection 
 * exists a new one is added.
**/
void SelectionManager::UpdateSelectionRect(unsigned int x, unsigned int y, unsigned int w, unsigned int h)
{
	RectSelectShape* top;
	// printf("Update Selection: (%d,%d)-> (%d,%d)\n", x, y, w, h );
	if ( m_selections.size() > 0 )
	{
		top = static_cast<RectSelectShape *>(m_selections.back());
		top->SetWidth(w);
		top->SetHeight(h);
	}
	else
	{
		top = new RectSelectShape(x, y, w, h);
        AddSelection(top);
	}	
}

/**
 * Returns trus if this Selection manager contains a valid selection
 * A valid selection is at least one sdelection area that is not empty.
 *
 * @return true if this selection manager contains a valid selection
 */
bool SelectionManager::HasSelection() const
{
	return(!m_selections.empty());
}

/**
 * Renders the current selection shape objects as a selection mask and returns the mask
 * The mask remains owned by this SelectionManager
 *
 * @return the selection mask of the current selections
 */
AbstractBuf& SelectionManager::GetSelectionMask()
{
	if(!m_selection_mask)
	{
		CinePaintTag _tag(m_drawable.GetBuffer().GetTag());
		_tag.SetFormat(CinePaintTag::FORMAT_GRAY_ENUM);
		m_selection_mask = new FlatBuf(_tag, m_drawable.GetWidth(), m_drawable.GetHeight());
		m_selection_mask->RefPortionReadWrite(0,0);
	}

	// clear the current mask
	unsigned char* _data = m_selection_mask->GetPortionData(0, 0);
	memset(_data, 0, m_selection_mask->GetWidth() * m_selection_mask->GetHeight() * m_selection_mask->GetTag().GetBytes());

	// drzw each of outr selection shapes
	for(SelectionContainer_t::const_iterator _citer = m_selections.begin(); _citer != m_selections.end(); ++_citer)
	{
		(*_citer)->DrawMask(*m_selection_mask);
	}

	return(*m_selection_mask);
}

/**
 * Clears all the current selection
 *
 */
void SelectionManager::ClearSelection()
{
	while(!m_selections.empty())
	{
		AbstractSelectionShape* _shape = m_selections.front();
		m_selections.pop_front();
		delete _shape;
		_shape = 0;
	}
}

