/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintCompositeOp - Standard Composite/Layer Merge Handling abstract base
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * $Id: CinePaintCompositeOp.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _CINEPAINT_COMPOSITE_OP_H_
#define _CINEPAINT_COMPOSITE_OP_H_

#include "dll_api.h"

#include <list>

class AbstractBuf;
class PixelArea;
class CPRect;
class Layer;

/**
 * CinePaintCompositeOp defines the core CinePaint Composition/Layer Merge operations.
 * This is a standard 'core' interface. CinePaint usually requires at least one implementation of
 * this interface for a particular image format for normal operation.
 *
*/
//CINEPAINT_CORE_INTERFACES_API

class CinePaintCompositeOp
{
	public:

		/**
		 * Destructor
		 */
		virtual ~CinePaintCompositeOp()
		{}

		enum MergeType
		{
			EXPAND_AS_NECESSARY_ENUM,
			CLIP_TO_IMAGE_ENUM,
			CLIP_TO_BOTTOM_LAYER_ENUM,
			FLATTEN_IMAGE_ENUM
		};

		/**
		 * Composites a specified region of the specified layers into the specified Abstractbuf
		 * The specified Layers are composited into the specified buffer, taking into account
		 * any Layer offsets, and LayerMode that each Layer may have.
		 * The specified region to composite is in image space.
		 *
		 * @param layer_list the list of Layers that are to be composited
		 * @param render_buffer the AbstractBuf to composite the specified Layers into
		 * @param merge_bounds the region to merge in image space.
		 * @return true if the composite operation was a success, false otherwise
		 */
		virtual bool CompositeLayers(const std::list<Layer*>& layer_list, AbstractBuf& render_buffer, const CPRect& merge_bounds) = 0;

		/**
		 * Composite two pixel areas, into one.
		 * @param dst the target pixel area, also used as one input.
		 * @param src the second image for combining.
		 * Overlays second image on first, assumes white == transparent, black==black.
		**/
		virtual bool CompositeOverlay(PixelArea &dst, const PixelArea &src) = 0;

	protected:
		/**
		 * protected Constructor - disallow instances
		 */
		CinePaintCompositeOp()
		{}

};

#endif /* _CINEPAINT_COMPOSITE_OP_H_ */
