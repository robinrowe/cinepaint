/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Procedure/Plugin Database
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: PDB.h,v 1.4 2006/12/21 11:18:08 robinrowe Exp $
 */
 
#ifndef _PDB_H_
#define _PDB_H_

#include "dll_api.h"
#include "CinePaintPlugin.h"
#include <utility/CStringLess.h>

#include <map>


class CINEPAINT_CORE_API PDB
{	/**
	 * Private Type def for the actual database storage.
	 * This uses rhe STL class multimap, which provides efficient lookup, insertion and
	 * deletion of data, usually in O(log n) time.
	**/
	typedef std::multimap<const char* , CinePaintPluginInfo *, CStringLess> ProcMap ;
	typedef std::multimap<const char* , CinePaintFileHandlerInfo *, CStringLess> FileHandlerMap;
	ProcMap	m_procDatabase;
	FileHandlerMap m_fileLoadHandlers;
	FileHandlerMap m_fileSaveHandlers;

	void db_init (void);
	void db_free (void);
	const char* get_filename_ext(const char* filepath);
	CinePaintFileHandlerInfo *find_file_save_handler(const char* ext);
	CinePaintFileHandlerInfo *find_file_load_handler(const char* ext);
	/**
	 * Initalise the procedure database.
	 * @param name The name of the procedure to run
	 * @param nreturn_vals The number of arguments returned.
	 * @param ... Arguments to pass to the procedure.
	 * @return The return values from the procedure
	**/
//	Argument *run_proc( char* name );
	/**
	 * Initalise the procedure database.
	**/
//	Argument *return_args  (ProcRecord *, int);
	/**
	 * Initalise the procedure database.
	**/
//	void destroy_args (Argument *args, int nargs);

public:
	static PDB* GetPDB(World* world)
	{	if(!world)
		{	return 0;
		}
		return world->pdb;
	}
#if 0
	static PDB* GetPDB()
	{	static PDB* singleton;
// rsr: PDB allocated in main app and passed to plug-ins to avoid this
// terrible hack...
// This works because a process only loads one copy of a dll data segment.
// (Note that static data won't share this way across processes.)
		if(!singleton)
		{	singleton=new PDB;
			printf("Creating PDB %p\n",singleton);
		}
		else
		{	printf("Using PDB %p\n",singleton);
		}
		return singleton;
	}
#endif
	/**
	 * Defualt Constructors and Destructors.
	**/
#if 0
	PDB(World* world)
	{	this->world=world;
	}
#else
	PDB()
	{	puts("PDB constructed");
	}
#endif
	~PDB()
	{	Dump();
		puts("PDB destroyed");
	}
	enum PluginType
	{	STD_PLUGIN_ENUM, 
		LOAD_HANDLER_ENUM, 
		SAVE_HANDLER_ENUM, 
		PLUGIN_TYPE_UNSPECIFIED, 
		NO_TYPE_ENUM, 
		UNKNOWN_TYPE_ENUM 
	};

	/**
	 * Register a new procedure in the database.
	 * @param p A pointer the the ProcRecord for the procedure to register.
	**/
	void Register(CinePaintPluginInfo *info);

	/**
	 * Register a file load handler in the DB.
	 **/
	void RegisterLoadHandler(CinePaintFileHandlerInfo *info);
	
	/**
	 * Register a file save handler in the DB.
	 **/
	void RegisterSaveHandler(CinePaintFileHandlerInfo *info);

	/**
	 * Remove a procedure from the database
	 * @param name The name of the procedure to remove
	**/
	void Unregister(char* name);

	/**
	 * Lookup a procedure un the data base
	 * @param name The name of the procedure to search for.
	 * @return Returns the ProcRecord for the procedure with the given name.
	**/
	CinePaintPluginInfo* LookupPlugin(const char* name);

	/**
	 * Lookup a load handler plugin within the load handlers database
	 *
	 * @param name The name of the load handler to look for
	 * @return Returns the CinePaintFileHandlerInfo for the load handler with the given name
	 *         or 0 if no handler is registered for the given name
	 */
	CinePaintFileHandlerInfo* LookupLoadHandler(const char* name);

	/**
	 * Lookup a save handler plugin within the save handlers database
	 *
	 * @param name The name of the save handler to look for
	 * @return Returns the CinePaintFileHandlerInfo for the save handler with the given name
	 *         or 0 if no handler is registered for the given name
	 */
	CinePaintFileHandlerInfo* LookupSaveHandler(const char* name);

	/**
	 * Constructs and returns an instance of the named plugin.
	 * The the specified name does not correspond to a plugin, 0 is returned.
	 * The caller assumes responsibility for the created plugin, and must correctly
	 * destroy once finshed, either through a call to ReleaseInstance, or directly
	 * by accessing the plugins delete function.
	 *
	 * @param name the plugin to create
	 * @return the constructed plugin, or 0
	 */
	CinePaintPlugin* CreateInstance(const char* name);

	/**
	 * Constructs and returns an instance of a named plugin.
	 * If plugin_type is not PLUGIN_TYPE_UNSPECIFIED, this method will search for
	 * the specified type of plugin within the appropriate database only, otherwise
	 * the standard plugin database is searched first, followed by the load, then save
	 * handlers until a plugin matching the specified name is found.
	 *
	 * If the specified name does not correspond to a plugin, 0 is returned.
	 * The caller is responsible for the constructres resource, and must release
	 * the resource once complete, either through a call to ReleaseInstance, or
	 * by directly accessing the plugins specified delete function.
	 *
	 * @param name the name of the register plugin
	 * @param plugin_type the type of the plugin to construct
	 */
	CinePaintPlugin* GetPluginInstance(const char* name, PluginType plugin_type = PLUGIN_TYPE_UNSPECIFIED);

	/**
	 * Deletes the specified plugin/
	 * This method may be used to correctly dispose of a plugin create by a call to CreateInstance.
	 * An attempt is made to access the plugin declared destruction function and use it to delete
	 * the plugin, or if no function is available, a standard delete is performed.
	 *
	 * @param plugin the plugin to delete
	 */
	void ReleaseInstance(CinePaintPlugin *_plugin);

	/**
	 * Returns the string required for file load/save dialogs.
	 * this string is built from all registered load/save handlers.
	 * @param load
	 * @return FLTK format string for File load dialog.
	**/
	const char* GetFileTypeString();

	/**
	 * Return the file load handler given a filename.
	 * @param filename name of the file to open.
	 * @return CinePaintPlugin instance require for given file.
	**/
	CinePaintPlugin::PFNCreateCinePaintPlugin GetLoadHandler(const char* filename);

	/**
	 * Return the file save handler given a filename.
	 * @param filename name of the file to open.
	 * @return CinePaintPlugin instance require for given file.
	**/
	CinePaintPlugin::PFNCreateCinePaintPlugin GetSaveHandler(const char* filename);

	/** 
	 * Dump Contents of Procedure Database
	**/
	void Dump();

	//-----------------------=----------------
	// Iterator access over registered plugins

	typedef ProcMap::const_iterator pdb_const_iterator;
	typedef std::pair<const char*, CinePaintPluginInfo*> PluginPair;
	typedef FileHandlerMap::const_iterator fh_const_iterator;
	typedef std::pair<const char*, CinePaintFileHandlerInfo*> FileHandlerPair;

	/**
	 * returns an iterator pointing to the beginning of the registered plugins
	 * The iterator iterates over PDB::PluginPair's, representing a plugin name, and
	 * the registered plugin data.
	 *
	 * @return an iterator pointing to the beginning of the registered plugins
	 */
	pdb_const_iterator PluginsBegin() const;

	/**
	 * returns an iterator pointing past the end of the registered plugins
	 * The iterator iterates over PDB::PluginPair's, representing a plugin name, and
	 * the registered plugin data.
	 *
	 * @return an iterator pointing to the beginning of the registered plugins
	 */
	pdb_const_iterator PluginsEnd() const;

	/**
	 * returns an iterator pointing to the beginning of the registered file load handler plugins
	 * The iterator iterates over PDB::FileHandlerPair's, representing a plugin name, and
	 * the registered plugin data.
	 *
	 * @return an iterator pointing to the beginning of the registered file load handler plugins
	 */
	fh_const_iterator LoadHandlersBegin() const;

	/**
	 * returns an iterator pointing past the end of the registered file load handler plugins
	 * The iterator iterates over PDB::FileHandlerPair's, representing a plugin name, and
	 * the registered plugin data.
	 *
	 * @return an iterator pointing to the beginning of the registered file load handler plugins
	 */
	fh_const_iterator LoadHandlersEnd() const;

	/**
	 * returns an iterator pointing to the beginning of the registered file save handler plugins
	 * The iterator iterates over PDB::FileHandlerPair's, representing a plugin name, and
	 * the registered plugin data.
	 *
	 * @return an iterator pointing to the beginning of the registered file save handler plugins
	 */
	fh_const_iterator SaveHandlersBegin() const;

	/**
	 * returns an iterator pointing past the end of the registered file save handler plugins
	 * The iterator iterates over PDB::FileHandlerPair's, representing a plugin name, and
	 * the registered plugin data.
	 *
	 * @return an iterator pointing to the beginning of the registered file save handler plugins
	 */
	fh_const_iterator SaveHandlersEnd() const;
};

#endif //_PROCEDURE_DB_H_
