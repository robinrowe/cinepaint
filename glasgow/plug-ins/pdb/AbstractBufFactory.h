/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      AbstractBufFactory - Abstract Image data container class creation.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: AbstractBufFactory.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _ABSTRACT_BUF_FACTORY_H_
#define _ABSTRACT_BUF_FACTORY_H_

#include "dll_api.h"
#include <memory>

// forward declaration
class AbstractBuf;
#include "plug-ins/pdb/CinePaintTag.h"

/**
 * AbstractBufFactory provides a factory object for creating AbstractBuf implementations for a specified StorageType
 *
 */
class CINEPAINT_IMAGE_API AbstractBufFactory
{
	public:
		/**
		 * Defined Storage types for the AbstractBuf
		 */
		enum StorageType
		{
			STORAGE_NONE   = 0,
			STORAGE_FLAT   = 1,
			STORAGE_TILED  = 2
#ifdef BUILD_SHM
			,STORAGE_SHM    = 3
#endif
		};

		/**
		 * Creates and returns a newly allocated AbstractBuf implementation for the specified StorageType
		 * We return a std::auto_ptr here for memory management safety forcing the caller to assume responsibility
		 * for the resource. Failing to take ownership results in the auto_ptr falling out of scope and the resource
		 * automatically getting destroyed.
		 *
		 * @param tag the CinePaintTag indicating the AbstractBuf image data type
		 * @param w the width of the AbstractBuf
		 * @param h the height of the AbstractBuf
		 * @param storage StorageType for the newly allocated AbstractBuf
		 * @return a newly allocated AbstractBuf implementation
		 */
		static std::auto_ptr<AbstractBuf> CreateBuf(const CinePaintTag& tag, int w, int h, StorageType storage);

	protected:

	private:
		/**
		 * Dis-allow instances
		 */
		AbstractBufFactory() {} ;

}; /* class AbstractBufFactory */

#endif /* _ABSTRACT_BUF_FACTORY_H_ */
