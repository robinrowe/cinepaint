/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      AbstractRenderer - Abstract Image rendering class.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: AbstractRenderer.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _CINEPAINT_ABSTRACT_RENDERER_H_
#define _CINEPAINT_ABSTRACT_RENDERER_H_

#include "dll_api.h"

// Forward Declaration
class AbstractBuf;
class CPRect;

/**
 *
 * @note [claw] this still needs some thought ...
 */
class CINEPAINT_IMAGE_API AbstractRenderer
{
	public:
		/**
		 * Destructor
		 */
		virtual ~AbstractRenderer() {};

		/**
		 * Renders the item this AbstractRenderer is rendering into the internal rendering buffer.
		 * Once complete this method sets the valid state to true.
		 *
		 */
		virtual void Render() = 0;

		/**
		 * Renders the specified area of the item this AbstractRenderer is rendering into the internal rendering buffer
		 * Once complete this method sets the valid state to true.
		 *
		 * @note [claw] need to think about the valid flag here...
		 * @param region the region to be rendered
		 */
		virtual void Render(const CPRect& render_bounds) = 0;

		/**
		 * Returns the AbsrtactBuf representing the rendered version of the item this AbsractRenderer is rendering
		 * This method does not check the valid state before returning the rendered buffer, the caller should therefore
		 * check that the buffer is valid with a call to IsValid() and call Render() as required
		 *
		 * @return the rendering buffer
		 */
		virtual const AbstractBuf& GetRender() = 0;

		/**
		 * Returns whether this CinePaintRenderer contains a valid render.
		 * If the item being rendered has not changed since the last time the item was rendered,
		 * true is returned, otherwise false is returned.
		 *
		 * @return true if this renderor contains a valid render, false otherwise
		 */
		virtual bool IsValid() const = 0;

		/**
		 * Returns whether the specified region of this render is valid.
		 * If the item being rendered has not modified the specified region since its was
		 * last renderedm this method returns true, otherwise false is returned.
		 *
		 * @param bounds the region to check as being valid
		 * @return true if the specified region is valid, false otherwise
		 */
		virtual bool IsValid(const CPRect& bounds) const = 0;

		/**
		 * Sets the current render invalid, or out of date.
		 * An out of date render should be re-rendered before drawn on screen
		 *
		 */
		virtual void SetInvalid() = 0;

		/**
		 * Sets the specified area invalid, or out of date, or so required a re-render
		 * A renderor implementation may be able to optimize its re-rendering based upon
		 * the area the is invalid
		 *
		 * @param invalid_bounds the region to set invalid
		 */
		virtual void SetInvalid(const CPRect& invalid_bounds) = 0;

		/**
		 * Returns the width of this Renderer.
		 * The width is usually the width of the Drawable being rendered
		 *
		 * @return the width of this Renderer
		 */
		virtual int GetWidth() const = 0;

		/**
		 * Returns the height of thie Renderer.
		 * The height is usually the height of the Drawable being rendered
		 *
		 * @return the height of thie Renderer
		 */
		virtual int GetHeight() const = 0;

		/**
		 * Get the glass layer for the associated CinePaintImage.
		 * @return the AbstractBuf containing the GlassLayer required for this image.
		**/
		virtual const AbstractBuf& GetGlassLayer() = 0;

	protected:

		/**
		 * Protected constructor - dis-allow instances
		 */
		AbstractRenderer() {};

	private:


} ; /* class AbstractRenderer */

#endif /* _CINEPAINT_ABSTRACT_RENDERER_H_ */
