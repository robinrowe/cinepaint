/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintTool - Abstract tool interface
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintTool.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 *
 */

#ifndef _CINEPAINT_TOOL_H_
#define _CINEPAINT_TOOL_H_

#include "dll_api.h"
#include "CinePaintMouseListener.h"
#include "plug-ins/pdb/CPParamList.h"

// Forward Declaration
class AbstractBuf;
struct CPPluginUIDef;
class CinePaintDoc;
class CinePaintImage;


/**
 * CinePaintTool defines an abstract interface for core CinePaint Tools
 * CinePaintTool provides a plugin mechanism to load system tools that may apply a process to an image.
 * CinePaintTools convert from mouse events, taking into account tool settings and apply a process
 * to an image via call to registered system operations
 *
 */
class CINEPAINT_CORE_INTERFACES_API CinePaintTool : public CinePaintMouseListener
{
	public:

		// Type defs for function pointers.
		/** Function prototype to create a tool - plugin. */
		typedef CinePaintTool * (*PFNCreateCinePaintTool)();

		/** plugin entry point */
		typedef PFNCreateCinePaintTool * (*PFNGetToolClassList)();


		/**
		 * Destructor
		 */
		virtual ~CinePaintTool() {};

		// Named Tool Types, defines the 'application' known tool type classes
		// Although this is slightly less efficient than an enum containaing all tool types,
		// it is slightly more flexible, allowing new tool types to be easily added. and also
		// supporting user defined tools types.
		// if this proves too inefficient, or prone to errors, switching to an enum based
		// implementation is reasonably clean and easy.
		static const char* TOOL_TYPE_NONE;
		static const char* TOOL_TYPE_SCALE;
		static const char* TOOL_TYPE_ZOOM;
		static const char* TOOL_TYPE_RECT_SELECT;
		static const char* TOOL_TYPE_ELLIPSE_SELECT;
		static const char* TOOL_TYPE_FREE_SELECT;
		static const char* TOOL_TYPE_FUZZY_SELECT;
		static const char* TOOL_TYPE_BEZIER_SELECT;
		static const char* TOOL_TYPE_AIRBRUSH;
		static const char* TOOL_TYPE_CLONE;
		static const char* TOOL_TYPE_CONVOLVE;
		static const char* TOOL_TYPE_DODGE;
		static const char* TOOL_TYPE_ERASER;
		static const char* TOOL_TYPE_PAINT;
		static const char* TOOL_TYPE_PENCIL;
		static const char* TOOL_TYPE_SMUDGE;
		static const char* TOOL_TYPE_MOVE;
		static const char* TOOL_TYPE_TEXT;
		static const char* TOOL_TYPE_ISCISSORS;
		static const char* TOOL_TYPE_FLIP;
		static const char* TOOL_TYPE_COLOR_PICKER;
		static const char* TOOL_TYPE_BLEND;
		static const char* TOOL_TYPE_CROP;
		static const char* TOOL_TYPE_BUCKET_FILL;
		static const char* TOOL_TYPE_MEASURE;


		/**
		 * Returns the type of this CinePaintTool
		 *
		 * @return the type of this CinePaintTool
		 * @todo we should probably move this into this base class and pass the
		 *       type string into the constructor
		 */
		virtual const char* GetToolType() const = 0;

		/**
		 * Returns the name of this CinePaintTool
		 *
		 * @return the name of this CinePaintTool
		 */
		const char* GetName() const;

		/**
		 * Returns the tool group of this CinePaintTool
		 * The tool groups is a string of the form "group/subgroup/..." representing
		 * a group of related tools
		 *
		 * @return the tool group
		 */
		const char* GetGroup() const;

		/**
		 * Returns an Icon representation of this CinePaintTool
		 * The icon representation is suitable for display upon a tool button or menu.
		 *
		 * @return an Icon representation of this CinePaintTool
		 */
		virtual const AbstractBuf& GetIcon() const = 0;

		/**
		 * Returns a tool tip for this CinePaintTool
		 *
		 * @return tooltip for this CinePaintTool
		 * @todo we should probably move this into this base class and pass the
		 *       tooltip string into the constructor
		 */
		virtual const char* GetToolTip() const = 0;

		/**
		 * Returns the UI definition for this CinePaintTool
		 *
		 * @param info populated with the definition of this tools ui panel
		 * @return info paramater populated with thie tools ui definition
		 */
		virtual CPPluginUIDef& GetUIDefs(CPPluginUIDef& info);

		/** The possible states for tools */
		enum ToolState { INACTIVE_ENUM, ACTIVE_ENUM, PAUSED_ENUM, INACT_PAUSED_ENUM };

		/**
		 * Sets the current state of this CinePaintTool
		 *
		 * @param state the current CinePaintTool state
		 */
		void SetState(ToolState state);

		/**
		 * Returns the current state of this CinePaintTool
		 *
		 * @return the current state of this CinePaintTool
		 */
		ToolState GetState() const;

		/**
		 * Sets the currently active document.
		**/
		void SetActiveDocument(CinePaintDoc *_doc) { m_doc = _doc; };



		/**
		 * Returns the param list for this tool
		 * @return the param list for this tool
		**/
		CPParamList &GetParamList() { return m_param_list; }

	protected:
		/**
		 * Protected Constructor - disallow instances
		 *
		 * The group parameter is a string of the form "group/subgroup/..." which may be used
		 * to group related tools.
		 *
		 * @param name the name of this CinePaintTool
		 * @param group tool grouping, used to group related tools
		 * @param blurb 
		 * @param help 
		 * @param author author of This CinePaintTool
		 * @param copyright copryright notice for this CinePaintTool plugin
		 * @param date date this CinePaintTool was written
		 */
		CinePaintTool(const char* name, const char* group, const char* blurb, const char* help, const char* author, const char* copyright, const char* date);

		/**
		 * Protected - Get the current document only derived class can get this.
		 * @return returs the current document as a CinePaintImage
		 * returns 0 if not document set or it is not a CinePaintImage.
		**/
		CinePaintImage *GetActiveImage();


	private:

		// plugins details
		const char* m_name;
		const char* m_group;
		const char* m_blurb;
		const char* m_help;
		const char* m_author;
		const char* m_copyright;
		const char* m_date;

		/** State of tool activity */
		ToolState m_state;

		/** paused control count */
		int m_paused_count;

		/** allow scrolling or not */
		bool m_scroll_lock;

		/** should the mouse snap to guides automatically */
		bool m_auto_snap_to;

		/** The param list for this tool **/
		CPParamList m_param_list;

		/** The current document. **/
		CinePaintDoc *m_doc;
		
};

#endif /* _CINEPAINT_TOOL_H_ */
