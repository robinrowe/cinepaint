/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      TileBuf - Tilled image buffer 
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * Spencer Kimball and Peter Mattis
 * Modified for CinePaint by Colin Law 2004/08/11
 *
 * $Id: TileBuf.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "TileBuf.h"
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include "PixelArea.h"

const int TileBuf::DEFAULT_TILE16_WIDTH = 128 ;
const int TileBuf::DEFAULT_TILE16_HEIGHT = 128 ;


/**
 * Constructs a new TileBuf to handle image data of the specified CinePaintTag, width and height
 *
 * @param tag the CinePaintTag descibibg the image data format
 * @param w the width in pixels of this TileBuf
 * @param h the height in pixels of this TileBuf
 */
TileBuf::TileBuf(const CinePaintTag& tag, int w, int h)
: AbstractBuf(tag, w, h)
{
	m_is_alloced = false;

	int n = get_tile16_index((w - 1), (h - 1)) + 1;
	m_tiles16 = new Tile16[n];
}

/**
 * Virtual Destructor
 */
TileBuf::~TileBuf()
{
	if(m_tiles16)
	{
		int n = get_tile16_index(GetWidth() - 1, GetHeight() - 1) + 1;
		while(n--)
		{
            if(m_tiles16[n].IsTile16Alloced())
			{
				m_tiles16[n].UnallocTile16();
			}
		}
        
		delete[] m_tiles16;
		m_tiles16 = 0;
	}
}



//--------------------------
// Width / Height / Position

int TileBuf::GetPortionX(int x, int y) const
{
	int _ret = 0;

	int i = get_tile16_index(x, y);
	if(i >= 0)
	{
		_ret = x - get_tile16_xoffset(x);
	}

	return(_ret);
}

int TileBuf::GetPortionY(int x, int y) const
{
	int _ret = 0;
	int i = get_tile16_index(x,y);
	if(i >= 0)
	{
		_ret = y - get_tile16_yoffset(y);
	}

	return(_ret);
}

int TileBuf::GetPortionWidth(int x, int y) const
{
	int _ret = 0;
	int i = get_tile16_index(x, y);
	if(i >= 0)
	{
		int offset = get_tile16_xoffset(x);
		int base = x - offset;
		if(base + DEFAULT_TILE16_WIDTH > GetWidth())
		{
			_ret = get_tile16_xoffset(GetWidth()) - offset;
		}
		else
		{
			_ret = DEFAULT_TILE16_WIDTH - offset;
		}
	}
	return(_ret);
}

int TileBuf::GetPortionHeight(int x, int y) const
{
	int _ret = 0;
	int i = get_tile16_index(x, y);
	if(i >= 0)
	{
		int offset = get_tile16_yoffset(y);
		int base = y - offset;
		if(base + DEFAULT_TILE16_HEIGHT > GetHeight())
		{
			_ret = get_tile16_yoffset(GetHeight()) - offset;
		}
		else
		{
			_ret = DEFAULT_TILE16_HEIGHT - offset;
		}
	}

	return(_ret);
}

//------------
// data access

unsigned char* TileBuf::GetPortionData(int x, int y)
{
	return(get_portion_data(x, y));
}

const unsigned char* TileBuf::GetPortionData(int x, int y) const
{
	return(get_portion_data(x, y));
}

bool TileBuf::GetPixelRow(PixelRow& pixelrow, int row, int offset, int width)
{
	// @note [claw] fairly self-explanatory!
	printf("TileBuf::GetPixelRow: TODO");
	return(false);
}

bool TileBuf::WriteRow(PixelRow& pixelrow, int row, int offset, int width)
{
	// @note [claw] fairly self-explanatory!
	printf("TileBuf::GetPixelRow: TODO");
	return(false);
}


bool TileBuf::GetPixel(Pixel& pixel, int x, int y)
{
	// @note [claw] fairly self-explanatory!
	printf("TileBuf::GetPixel: TODO");
	return(false);
}

bool TileBuf::WritePixel(Pixel& pixel, int x, int y)
{
	// @note [claw] fairly self-explanatory!
	printf("TileBuf::WritePixel: TODO");
	return(false);
}

unsigned char* TileBuf::GetBufferPixel(int x, int y)
{
	// @note [claw] fairly self-explanatory!
	printf("TileBuf::GetBufferPixel: TODO");
	return(0);
}

int TileBuf::GetPortionRowstride(int x, int y) const
{
	int _rowstride = 0;
	int i = get_tile16_index(x, y);
	if(i >= 0)
	{
		_rowstride = DEFAULT_TILE16_WIDTH * GetTag().GetBytes();
	}

	return(_rowstride);
}


//---------------------------------------------------------
// allocate and/or swap in the backing store for this pixel

bool TileBuf::IsPortionAlloced(int x, int y) const
{
	bool _ret = false;

	int i = get_tile16_index(x, y);
	if(i >= 0)
	{
		Tile16* tile16 = &m_tiles16[i];
		_ret = tile16->IsTile16Alloced();
	}

	return(_ret);
}

bool TileBuf::RefPortionReadOnly(int x, int y)
{
	bool _ret = false;

	int i = get_tile16_index(x, y);
	if(i >= 0)
	{
		Tile16* tile16 = &m_tiles16[i];

		if(!tile16->IsTile16Alloced())
		{
			if(IsAutoAlloc())
			{
				AllocPortion(x, y);
			}
		}

		if(tile16->IsTile16Alloced())
		{
			tile16->IncRefCount();
			_ret = true;
		}
	}

	return(_ret);
}

bool TileBuf::RefPortionReadWrite(int x, int y)
{
	bool _ret = false;

	int i = get_tile16_index(x, y);
	if(i >= 0)
	{
		Tile16* tile16 = &m_tiles16[i];

		if(!tile16->IsTile16Alloced())
		{
            if(IsAutoAlloc())
			{
				AllocPortion(x, y);
			}
		}

		if(tile16->IsTile16Alloced())
		{
			tile16->IncRefCount();
			_ret = true;
		}
	}

	return(_ret);
}

bool TileBuf::UnrefPortion(int x, int y)
{
	bool _ret = false;

	int i = get_tile16_index(x, y);
	if(i >= 0)
	{
		Tile16 * tile16 = &m_tiles16[i];
		if(tile16->GetRefCount() > 0)
		{
			tile16->DecRefCount();
		}
		else
		{
            printf("tilebuf unreffing a tile with ref_count==0");
		}
		_ret = true;
	}

	return(_ret);
}


//-----------------------
// alloc / dealloc memory

bool TileBuf::AllocPortion(int x, int y)
{
	bool _ret = false;
	int i = get_tile16_index(x, y);
	if(i >= 0)
	{
		Tile16* tile16 = &m_tiles16[i];
		if(tile16->IsTile16Alloced())
		{
			_ret = true;
		}
		else
		{
			int n = DEFAULT_TILE16_WIDTH * DEFAULT_TILE16_HEIGHT * GetTag().GetBytes();

			if(tile16->AllocTile16(n))
			{
				if(!InitPortion(x - get_tile16_xoffset(x), y - get_tile16_yoffset(y), DEFAULT_TILE16_WIDTH, DEFAULT_TILE16_HEIGHT))
				{
					// @note [claw] removed as this isn't really failing if theres no initilizer set
					// printf("tilebuf failed to init portion...");
				}
				_ret = true;
			}
		}
	}

	return(_ret);
}

bool TileBuf::UnallocPortion(int x, int y)
{
	bool _ret = false;
	int i = get_tile16_index(x, y);
	if(i >= 0)
	{
		Tile16* tile16 = &m_tiles16[i];
		if(tile16->IsTile16Alloced())
		{
			if(tile16->GetRefCount() != 0)
			{
				printf("Unallocing a reffed tile.  expect a core...\n");
			}
			tile16->UnallocTile16();
			_ret = true;
		}
	}

	return(_ret);
}




/**
 * Returns a pointer to the canvas data for the portion with the specified top left corner
 * This is a helper method for the public GetPortionData methods
 *
 * @param x the x value of the top left corner of the portion
 * @param y the y value of the top left corner of the portion
 * @return a pointer to the canvas data for the specified portion
 */
unsigned char* TileBuf::get_portion_data(int x, int y) const
{
	unsigned char* _data = 0;

	int i = get_tile16_index(x, y);
	if(i >= 0)
	{
		Tile16* tile16 = &m_tiles16[i];
		if(tile16->IsTile16Alloced())
		{
			_data = tile16->GetData() + (GetTag().GetBytes() * ((get_tile16_yoffset(y) * DEFAULT_TILE16_WIDTH) +	get_tile16_xoffset(x)));
		}
	}

	return(_data);
}

int TileBuf::get_tile16_xoffset(int x) const
{
	return(x % DEFAULT_TILE16_WIDTH) ;
}

int TileBuf::get_tile16_yoffset(int y) const
{
	return(y % DEFAULT_TILE16_HEIGHT) ;
}

int TileBuf::get_tile16_index(int x, int y) const
{
	int index = -1;

	if((x < GetWidth()) && (y < GetHeight()))    
	{
		x = x / TileBuf::DEFAULT_TILE16_WIDTH;
		y = y / TileBuf::DEFAULT_TILE16_HEIGHT;
		index = (y * ((GetWidth() + DEFAULT_TILE16_WIDTH - 1) / DEFAULT_TILE16_WIDTH) + x);
	}

	return(index);
}






//------------
// Inner Class

TileBuf::Tile16::Tile16()
{
	m_is_alloced = false;
	m_ref_count = 0;
	m_data = 0;
}

TileBuf::Tile16::~Tile16()
{

}

bool TileBuf::Tile16::IsTile16Alloced() const
{
	return(m_is_alloced) ;
}

bool TileBuf::Tile16::AllocTile16(unsigned int bytes)
{
	bool _ret = false ;

	m_data = new unsigned char[bytes];
	if(m_data)
	{
		memset(m_data, 0, bytes);
		m_is_alloced = true ;
		_ret = true;
	}

	return(_ret);
}

void TileBuf::Tile16::UnallocTile16()
{
	if(m_is_alloced && m_data)
	{
		delete[] m_data;
		m_data = 0;

		m_is_alloced = false;
	}
}

/**
 * Returns duirect access to the contaoned data
 * Do not delete directly, use UnallocTile16
 *
 * @return direct access to the contained data
 */
unsigned char* TileBuf::Tile16::GetData()
{
	return(m_data);
}

int TileBuf::Tile16::GetRefCount() const
{
	return(m_ref_count);
}

int TileBuf::Tile16::IncRefCount()
{
	m_ref_count++;
	return(m_ref_count);
}

int TileBuf::Tile16::DecRefCount()
{
	m_ref_count--;
	return(m_ref_count);
}
