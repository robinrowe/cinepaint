/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      PixelArea - Abstract representation of image area
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: PixelArea.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "PixelArea.h"
#include "CinePaintTag.h"
#include "AbstractBuf.h"
#include <utility/MathUtil.h>

/**
 * Constructs a new PixelArea at the specified location and width, height over the specified AbstractBuf of image data
 *
 * @param buf the AbstractBuf over which this PixelArea indicates a region of interest
 * @param x the x location of this PixelArea
 * @param y the y location of this PixelArea
 * @param w the width
 * @param h the height
 * @param reftype the reference type on the AbstractBuf data
 */
PixelArea::PixelArea(AbstractBuf& buf, int x, int y, int w, int h, AbstractBuf::RefType reftype)
: m_buffer(buf)
{
	// clip the area to the canvas bounds
	m_x = MathUtil::clamp(x, 0, buf.GetWidth());
	m_y = MathUtil::clamp(y, 0, buf.GetHeight());
	m_width = MathUtil::clamp(w, 0, buf.GetWidth());
	m_height = MathUtil::clamp(h, 0, buf.GetHeight());

	m_reftype = reftype;
}

/**
 * Destructor
 */
PixelArea::~PixelArea()
{

}

/**
 * Returns the x location of this PixelArea in relation to the set AbstractBuf
 *
 * @return the x location of this PixelArea
 */
int PixelArea::GetX() const
{
	return(m_x);
}

/**
 * returns the y location of this PixelArea in relation to the set AbstractBuf
 *
 * @return the y location of this PixelArea
 */
int PixelArea::GetY() const
{
	return(m_y);
}

/**
 * Returns the width of this PixelArea
 *
 * @return the width of this PixelArea
 */
int PixelArea::GetWidth() const
{
	return(m_width);
}

/**
 * Returns the height of this PixelArea
 *
 * @return the height of this PixelArea
 */
int PixelArea::GetHeight() const
{
	return(m_height);
}

/**
 * Populates the specified PixelRow with image data from the specified row within this PixelArea
 * The width of pixelrow indicates how many pixels will be copied. This will commonly match the
 * width of this PixelArea, although is not required to.
 *
 * @param pixelrow the PixelRow to set with image data
 * @param row the row of data within this PixelArea to copy
 * @return true if the PixelRow wa spopulates with data, false if the range specified
 *         falls outside the data area of this AbstractBuf
 */
bool PixelArea::GetPixelRow(PixelRow& pixelrow, int row)
{
	// simply get the row from the AbstractBuf with appropriate offsets
	return(m_buffer.GetPixelRow(pixelrow, (m_y + row), m_x, m_width)); 
}

/**
 * Writes data contained within the specified PixelRow to the AbstractBuf this PixelArea priovides a view over
 * Data copying is started at the specified row of this PixelArea's view over the underlying AbstractBuf.
 *
 * @param pixelrow the PixelRow containing image data to write
 * @param row the row of data within this PixelArea to write data.
 * @return true if the PixelRow wa spopulates with data, false if the range specified
 *         falls outside the data area of this AbstractBuf
 */
bool PixelArea::WritePixelRow(PixelRow& pixelrow, int row)
{
	return(m_buffer.WriteRow(pixelrow, (m_y + row), m_x, m_width));
}

/**
 * Populates the specified Pixel with image data from the specified point within this PixelArea
 * If the specified location falls outwith the area of this PixelArea, no further action is taken
 * and false is returned.
 * Pixel is assumed to be in the same format as this PixelArea, if this is not the case, false is
 * returned and no data is copied
 *
 * @param pixel the Pixel to Write data into
 * @param x the x location in pixels
 * @param y the y location in pixels
 * @return false if the Pixel CinePaintTag format and the PixelArea CinePaintTag format do not match
 */
bool PixelArea::GetPixel(Pixel& pixel, int x, int y)
{
	return(m_buffer.GetPixel(pixel, (m_x + x), (m_y + y)));
}

/**
 * Writes the specified Pixel into the specified location of the AbstractBuf this PixelArea provides a view over.
 * If the specified location falls outwith this PixelArea, no further action is taken.
 *
 * @param pixel the Pixel to Write
 * @param x the x location in pixels
 * @param y the y location in pixels
 * @return false if the Pixel CinePaintTag format and the PixelArea CinePaintTag format do not match
 */
bool PixelArea::WritePixel(Pixel& pixel, int x, int y)
{
	return(m_buffer.WritePixel(pixel, (m_x + x), (m_y + y)));
}

/**
 * Returns direct access to the specified pixel within the AbstractBuf backing this PixelArea
 * If the specified location falls outwith this PixelArea 0 is returned.
 *
 * @param x the x location in pixels
 * @param y the y location in pixels
 */
unsigned char* PixelArea::GetBufferPixel(int x, int y)
{
	return(m_buffer.GetBufferPixel(m_x + x, m_y + y));
}

/**
 * Returns the AbstractBuf this PixelArea represents a region of interst over
 *
 * @return the AbstractBuf this PixelArea represents an area of interest over
 */
AbstractBuf& PixelArea::GetBuffer()
{
	return(m_buffer);
}
const AbstractBuf& PixelArea::GetBuffer() const
{
	return(m_buffer);
}
