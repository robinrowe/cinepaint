/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      AbstractBuf - Abstract Image data container class.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * Based on the GIMP original buffer by Spencer Kimball and Peter Mattis
 *
 * $Id: AbstractBuf.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "AbstractBuf.h"
#include "PixelArea.h"
#include "CinePaintTag.h"
#include <stdio.h>

/**
 * Protected Constructor, this class must be subclassed to create useable types
 *
 * @param tag the CinePaintTag descibibg the image data format
 * @param w the width in pixels of this AbstractBuf
 * @param h the height in pixels of this AbstractBuf
 */
AbstractBuf::AbstractBuf(const CinePaintTag& tag, int w, int h)
: m_tag(tag)
{
	m_width = w;
	m_height = h;
	m_ref_count = 0;
	m_autoalloc = true;

	m_portion_initializer = 0;
}


/**
 * Virtual Destructor
 */
AbstractBuf::~AbstractBuf()
{
	// nothing to do
}


//-------------
// Tag / Format

/**
 * Returns the Tag indicating the data format of this AbstractBuf
 *
 * @return the Tag indicating the data format of this AbstractBuf
 */
CinePaintTag& AbstractBuf::GetTag()
{
	return(m_tag);
}

const CinePaintTag& AbstractBuf::GetTag() const
{
	return(m_tag);
}

/**
 * Sets the Tag indicating the data format of this AbstractBuf
 *
 * @param the Tag indicating the data format of this AbstractBuf
 */
void AbstractBuf::SetTag(const CinePaintTag& tag)
{
	m_tag = tag;
}



//--------------------------
// Width / Height / Position

/**
 * Returns the width of this AbstractBuf
 *
 * @return the width of this AbstractBuf
 */
int AbstractBuf::GetWidth() const
{
	return(m_width);
}

/**
 * Returns the height of this AbstractBuf
 * 
 * @return the height of this AbstractBuf
 */
int AbstractBuf::GetHeight() const
{
	return(m_height);
}


const PixelArea* AbstractBuf::GetPixelArea(int x, int y, int w, int h) const
{
	// THIS IS A HACK - const is removed from AbstractBuf in this case //
	// If you find a better way - then fix it. //
	const PixelArea* _res = new PixelArea(*const_cast<AbstractBuf *>(this), x, y, w, h, AbstractBuf::REFTYPE_READ);
	return _res;
}

PixelArea* AbstractBuf::GetPixelArea(int x, int y, int w, int h)
{
	PixelArea* _res = new PixelArea(*this, x, y, w, h, AbstractBuf::REFTYPE_WRITE);
	return _res;
}


/**
 * Returns whether this AbstractBuf auto allocates new memory for tiles not already in memory
 *
 * @return true if this AbstractBuf Auto Allocates
 */
bool AbstractBuf::IsAutoAlloc() const
{
	return(m_autoalloc);
}

/**
 * Sets the AutoAlloc state of this AbstractBuf
 * If AutoAlloc is set true, memory for a tile/portion not already in
 * memory will be automatically allocated
 *
 * @param aa the AutoAlloc state of this AbstractBuf
 */
void AbstractBuf::SetAutoAlloc(bool aa)
{
	m_autoalloc = aa;
}



// initialize the backing store for this pixel

/**
 * Sets the current PortionInitializer to initialize portions of this AbstractBuf
 * If no initialization is required, portion_initializer may be set to zero.
 * The set PortionInitializer is not managed by this class.
 *
 * @param portion_initializer the PortionInitializer used to initialize portions of this AbstrractBuf
 */
void AbstractBuf::SetPortionInitializer(PortionInitializer* portion_initializer)
{
	m_portion_initializer = portion_initializer;
}


/*
   FIXME:

   this doesn;t work right for tiles.  edge tiles only get the
   "on-image" portion inited.  the part that lies off the image isn't
   inited because the portion_width and portion_height ignore it.

   the proper fix for this is to split portion out of AbstractBuf and deal
   only with tiles at the AbstractBuf level.  for now we just memset the
   chunk to zero in portion_alloc
*/


/**
 * Initialize the specified portion of the buffer using the currently set PortionInitializer.
 * If no PortionInitializer is set, no initialization of the AbstractBuf takes place
 *
 * @param x
 * @param y
 * @param w
 * @param h
 * @return
 */
bool AbstractBuf::InitPortion(int x, int y, int w, int h)
{
	bool _ret = false;

	if(m_portion_initializer)
	{
		_ret = m_portion_initializer->InitPortion(*this, x, y, w, h);
	}

	return(_ret);
}



//-------------------
// Reference Counting

/**
 * Increment the reference count by one for this Buf
 *
 * @return the new reference count
 */
int AbstractBuf::IncRefCount()
{
	m_ref_count++;
	return(m_ref_count);
}

/**
 * Decrement the reference count of this Buf by one
 *
 * @return the new reference count
 */
int AbstractBuf::DecRefCount()
{
	m_ref_count--;
	return(m_ref_count);
}

/**
 * Returns the current reference count of this Buf
 *
 * @return the current reference count of this Buf
 */
int AbstractBuf::GetRefCount() const
{
	return(m_ref_count);
}
