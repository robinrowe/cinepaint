/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      FlatBuf - Flat buffer image representation
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: FlatBuf.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "FlatBuf.h"
#include "Pixel.h"
#include "PixelRow.h"
#include "PixelArea.h"
#include <stdio.h>
#include <string.h>


/**
 * Constructs a new FlatBuf to handle image data of the specified CinePaintTag, width and height
 *
 * @param tag the CinePaintTag descibibg the image data format
 * @param w the width in pixels of this FlatBuf
 * @param h the height in pixels of this FlatBuf
 */
FlatBuf::FlatBuf(const CinePaintTag& tag, int w, int h)
: AbstractBuf(tag, w, h)
{
	m_data = 0;
	m_is_alloced = false;
}

/**
 * Virtual Destructor
 */
FlatBuf::~FlatBuf()
{
	UnallocPortion(0, 0);
}


//--------------------------
// Width / Height / Position

int FlatBuf::GetPortionX(int x, int y) const
{
	// always 0
	return(0);
}
int FlatBuf::GetPortionY(int x, int y) const
{
	// always 0
	return(0);
}

int FlatBuf::GetPortionWidth(int x, int y) const
{
	int _width = -1;
	if(check_xy(x, y))
	{
		_width = GetWidth();
	}
	
	return(_width);
}

int FlatBuf::GetPortionHeight(int x, int y) const
{
	int _height = -1;

	if(check_xy(x, y))
	{
		_height = GetHeight();
	}
	
	return(_height);
}

//------------
// data access
unsigned char* FlatBuf::GetPortionData(int x, int y)
{
	return(get_portion_data(x, y));
}

const unsigned char* FlatBuf::GetPortionData(int x, int y) const
{
	return(get_portion_data(x, y));
}

bool FlatBuf::GetPixelRow(PixelRow& pixelrow, int row, int offset, int pixels)
{
	bool _ret = false;

	if((row >= 0) && (row < GetHeight()))
	{
		int _offset = (GetWidth() * GetTag().GetBytes() * row) + offset;
		int _buf_size = GetWidth() * GetHeight() * GetTag().GetBytes();

		// dont let the offset carry us off the buffer data
		// @TODO should we returb just the remaining buffer here?
		if((_offset + (pixels * GetTag().GetBytes())) < _buf_size)
		{
			unsigned char* _rowstart = m_data + _offset;
			pixelrow.SetData(_rowstart, pixels);
			_ret = true;
		}
	}

	return(_ret);
}

bool FlatBuf::WriteRow(PixelRow& pixelrow, int row, int offset, int pixels)
{
	bool _ret = false;
	
	if((row >= 0) && (row < GetHeight()))
	{
		int _offset = (GetWidth() * GetTag().GetBytes() * row) + offset;
		int _buf_size = GetWidth() * GetHeight() * GetTag().GetBytes();

		// dont let the offset carry us off the buffer data
		// @TODO should we returb just the remaining buffer here?
		if((_offset + (pixels * GetTag().GetBytes())) < _buf_size)
		{
			unsigned char* _rowstart = m_data + _offset;
			unsigned char* _data = pixelrow.GetData();

			memcpy(_rowstart, _data, pixels * GetTag().GetBytes());
			_ret = true;
		}
	}

	return(_ret);
}

bool FlatBuf::GetPixel(Pixel& pixel, int x, int y)
{
	bool _ret = false;

	if((x <= this->GetWidth()) && (y <= this->GetHeight()) && (pixel.GetTag() == this->GetTag()))
	{
		unsigned char* _buf_pixel = m_data + (GetWidth() * GetTag().GetBytes() * y) + (x * GetTag().GetBytes());

		pixel.SetData(_buf_pixel);
		_ret = true;
	}

	return(_ret);
}

bool FlatBuf::WritePixel(Pixel& pixel, int x, int y)
{
	bool _ret = false;

	if((x <= this->GetWidth()) && (y <= this->GetHeight()) && (pixel.GetTag() == this->GetTag()))
	{
		unsigned char* _data = pixel.GetData();
		unsigned char* _buf_pixel = m_data + (GetWidth() * GetTag().GetBytes() * y) + (x * GetTag().GetBytes());

		memcpy(_buf_pixel, _data, GetTag().GetBytes());
		_ret = true;
	}

	return(_ret);
}

unsigned char* FlatBuf::GetBufferPixel(int x, int y)
{
	return(GetPortionData(x, y));
}

int FlatBuf::GetPortionRowstride(int x, int y) const
{
	int _rowstride = 0;

	if(check_xy(x, y))
	{
		_rowstride = GetWidth() * GetTag().GetBytes();
	}

	return(_rowstride);
}



//---------------------------------------------------------
// allocate and/or swap in the backing store for this pixel

bool FlatBuf::IsPortionAlloced(int x, int y) const
{
	bool _ret = false;

	if(check_xy(x,y))
	{
		_ret = m_is_alloced;
	}

	return(_ret);
}

bool FlatBuf::RefPortionReadOnly(int x, int y)
{
	bool _ret = false;

	if(check_xy(x,y))
	{
		if(!IsPortionAlloced(x, y))
		{
			if(IsAutoAlloc())
			{
				AllocPortion(x, y);
			}
		}

		if(IsPortionAlloced(x, y))
		{
			IncRefCount();
			_ret = true;
		}
	}

	return(_ret);
}

bool FlatBuf::RefPortionReadWrite(int x, int y)
{
	bool _ret = false;

	if(check_xy(x,y))
	{
		if(!IsPortionAlloced(x, y))
		{
			if(IsAutoAlloc())
			{
				AllocPortion(x, y);
			}
		}

		if(IsPortionAlloced(x, y))
		{
			IncRefCount();
			_ret = true;
		}
	}

	return(_ret);
}

bool FlatBuf::UnrefPortion(int x, int y)
{
	bool _ret = false;

	if(check_xy(x,y))
	{
		if(GetRefCount() > 0)
		{
			DecRefCount();
		}
		else
		{
			printf("FlatBuf unreffing with ref_count==0");
		}

		_ret = true;
	}

	return(_ret);
}


//-----------------------
// alloc / dealloc memory

bool FlatBuf::AllocPortion(int x, int y)
{
	bool _ret = false ;
	if(check_xy(x, y))
	{
		if(IsPortionAlloced(x, y))
		{
			_ret = true;
		}
		else
		{
			int _n = GetTag().GetBytes() * GetWidth() * GetHeight();
			m_data = new unsigned char[_n];
			if(m_data)
			{
				memset(m_data, 0, _n);
				m_is_alloced = true;
				if(!InitPortion(0, 0, GetWidth(), GetHeight()))
				{
					// @note [claw] removed as this isn't really failing if theres no initilizer set
					//printf("FlatBuf failed to init portion...");
				}
				_ret = true;
			}
		}
	}
	else
	{
		_ret = false;
	}

	return(_ret);
}

bool FlatBuf::UnallocPortion(int x, int y)
{
	bool _ret = false ;
	if(check_xy(x, y) && IsPortionAlloced(x, y))
	{
		if(GetRefCount() != 0)
		{
			printf("Unallocing a reffed flatbuf.  expect a core...\n");
		}

		delete[] m_data;
		m_data = 0;
		m_is_alloced = false;
		_ret = true;
	}

	return(_ret);
}


/**
 * Returns true if x and y are inside the width and height of this Buf
 * ie if 0 <= x < GetWidth() and 0 <= y < GetHeight()
 *
 * @return true if x and y are inside the width and height of this Buf
 */
bool FlatBuf::check_xy(int x, int y) const
{
	return((x >=0) && (x < GetWidth()) && (y >= 0) && (y < GetHeight()));
}

/**
 * Returns a pointer to the canvas data for the portion with the specified top left corner
 * This is a helper method for the public GetPortionData methods
 *
 * @param x the x value of the top left corner of the portion
 * @param y the y value of the top left corner of the portion
 * @return a pointer to the canvas data for the specified portion
 */
unsigned char* FlatBuf::get_portion_data(int x, int y) const
{
	unsigned char* _d = 0;

	if(check_xy(x, y) && m_data)
	{
		_d = m_data + ((y * GetWidth()) + x) * GetTag().GetBytes();
	}

	return(_d);
}
