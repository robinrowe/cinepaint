/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      LayerManager - Layer Management module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: LayerManager.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _CINEPAINT_LAYER_MANAGER_H_
#define _CINEPAINT_LAYER_MANAGER_H_

#include "dll_api.h"
#include "ListenerList.h"
#include <list>

// Forward declaration
class CinePaintImage;
class Layer;
class LayerChangeListener;

/**
 * LayerManger manages the stack of Layers that compose a CinePaintImage
 * Layer[0] is the background, Layer[size - 1] is the foreground.
 *
 */
class CINEPAINT_IMAGE_API LayerManager
{
	private:
		// we want a little of both a list and a vector/deque here...
		typedef std::list<Layer*> LayerStack_t;

	public:
		/**
		 * Constructa a new Layermanager to manage a stack of layers within a CinePaintImage
		 * The layer statck is a simple stack of Layers. Layer 0 is the front Layer, or the foreground,
		 * the backgrund layer is LayerManager.GetLayerCount() -1.
		 * LayerManager is intended to be used with an instance of a CinePaintImage to separate
		 * the handling of Layers from the rest of the CinePaintImage class.
		 */
		LayerManager()
		{// rsr: (unused)	m_image=0;
		}

		/** rsr: fix CinePaintImage& 'this' before ctor warning by splitting ctor/Init.
		 *
		 * @param image the CinePaintImage this Layermanager manages Layers for
		**/
/* rsr: unused 
		void Init(CinePaintImage* image)
		{	m_image=image;
		}
*/
		/**
		 * Destructor - destroys all managed Layers
		 */
		virtual ~LayerManager();



		//-------------------------
		// Layer Addition / Removal

		/**
		 * Adds the specified layer to this LayerManager at the specified position
		 * Once added, the Layermanager assumes responsibility for the Layer until the layer is removed.
		 * If the layer is already managed by this Layermanager, the Layer is not added a second time and
		 * false is returned, otherwise the Layer is added and true is returned.
		 *
		 * @param layer the Layer to add
		 * @param position the position within the layer stack to add the layer
		 * @param active set true if the newly added Layer is the new active layer
		 * @return true if the layer is added, false otherwise
		 */
		bool AddLayer(Layer* layer, unsigned int position, bool active);

		/**
		 * Adds the specified Layer to this Layermanager as the foreground layer
		 * Once added, this LayerManager assumes responsibility for the Layer until the layer is removed.
		 * If the layer is already managed by this Layermanager, the Layer is not added a second time and
		 * false is returned, otherwise the Layer is added and true is returned.
		 *
		 * @param layer the Layer to be added
		 * @param active set true to set the newly added layer as the currently active layer
		 * @return true if the layer was added successfully, false otherwise
		 */
		bool AddLayerForeground(Layer* layer, bool active);

		/**
		 * Adds the specified Layer to this Layermanager as the background layer
		 * Once added, this LayerManager assumes responsibility for the Layer until the layer is removed.
		 * If the layer is already managed by this Layermanager, the Layer is not added a second time and
		 * false is returned, otherwise the Layer is added and true is returned.
		 *
		 * @param layer the Layer to be added
		 * @param active set true to set the newly added layer as the currently active layer
		 * @return true if the layer was added successfully, false otherwise
		 */
		bool AddLayerBackground(Layer* layer, bool active);

		/**
		 * Removes and returns the specified Layer from thie LayerManager
		 * Resopnsibility for the removed Layer passes to the caller.
		 * If the Specified Layer is not managed by this LayerManager, no further action is taken
		 * and 0 is returned
		 *
		 * @param layer the Layer to be removed
		 * @return the removed Layer or 0 if the specified Layer is not managed by this LayerManager
		 */
		Layer* RemoveLayer(Layer* layer);



		//-------------
		// Layer Access

		/**
		 * Returns the currently active layer
		 *
		 * @return the currently active layer
		 */
		Layer* GetActiveLayer() const;

		/**
		 * Sets the currently active layer to the specified Layer
		 * If the specified layer is not managed by this layerManager, the currently
		 * active Layer is set to 0, and false is returned.
		 *
		 * @param layer the Layer to set as the Active Layer
		 * @return false if the layer is not managed by this LayerManager, true otherwise
		 */
        bool SetActiveLayer(Layer* layer);

		/**
		 * Returns the layer at the top of the Layer stack
		 *
		 * @return the layer at the top of the layer stack
		 */
		Layer* GetForegroundLayer() const;

		/**
		 * Returns the Layer at the bottom of the Layer stack, the background
		 *
		 * @return the background layer
		 */
		Layer* GetBackgroundLayer() const;

		/**
		 * Returns a Layer by its index into the Layer stack
		 *
		 * @param position the index into the Layer stack
		 * @return the Layer at the specified postion, or 0 if no Layer resides at the specified position
		 */
		Layer* GetLayerByIndex(unsigned int index) const;


		//---------------
		// Layer Iterator

		typedef LayerStack_t::const_iterator const_iterator;

		/**
		 * Returns a constant iterator at the beginning of the managed Layers within this LayerManager
		 *
		 * @return a constant iterator at the beginning of this LayerManagers managed Layers
		 */
		const_iterator begin() const;

		/**
		 * Returns a constant iterator past the end of this LayerManagers managed Layers
		 *
		 * @return a constant iterator past the end of this LayerManagers managed Layers
		 */
		const_iterator end() const;


		//------------------
		// Layer Positioning

		/**
		 * Determines the position within the layer stack at which the specified Layer resides
		 * If the specified layer is not managed by this layerManager, this method will return false
		 * and position is unchanged, otherwise the position is set within the reference parameter
		 * position and this method returns true.
		 *
		 * @param the Layer to determine the position of
		 * @param position set to the position within the layer stack of the specified layer
		 * @return true if the Layer is managed by this Layermanager, false otherwiae
		 */
		bool GetLayerIndex(const Layer* layer, unsigned int& position) const;

		/**
		 * Raises the specified layer within the Layer stack by one place
		 *
		 * @param layer the Layer to raise
		 */
		void RaiseLayer(Layer* layer);

		/**
		 * Lowers the specified Layer within the Layer stack by one place
		 *
		 * @param layer the Layer to lower
		 */
		void LowerLayer(Layer* layer);

		/**
		 * Returns the number of layers within this LayerManager
		 *
		 * @return the number of layers within this LayerManager
		 */
		size_t GetLayerCount() const;





		//-------------------
		// Listener handling

		/**
		 * Registers the specified LayerChangeListener to receive Layer change notifications.
		 *
		 * @param lcl the DrawableChangeListener to add
		 */
		void AddLayerChangeListener(LayerChangeListener* lcl);

		/**
		 * Removes the specified LayerChangeListener from those registered as receiving Layer change notifications
		 *
		 * @param lcl the LayerChangeListener to remove
		 */
		void RemoveLayerChangeListener(LayerChangeListener* lcl);

	protected:

	private:
		/**
		 * Returns true if this LayerManager manages the specified Layer
		 *
		 * @param layer the layer to search for
		 * @return true if this LayerManager manages the specified Layer, false otherwise
		 */
		bool has_layer(const Layer* layer) const;

		/**
		 * Updates the most recently used layer stack, moving the specified Layer to the top
		 *
		 * @param layer the most recently used layer
		 */
		void set_mru(Layer* layer);

		/** the CinePaintImage this layermanager works for */
/* rsr: usused
		CinePaintImage* m_image;
*/
		/** the list of layers in visual order stack */
		LayerStack_t m_layers;

		/** the layers in Most Recently Used (MRU) order */
		LayerStack_t m_layers_mru;

		/** the currently active layer */
        Layer* m_active_layer;



		//------------------
		// Listener handling

		typedef ListenerList<LayerChangeListener> ListenerList_t;

		/** list of registered ImageChangeListener to be notified on Image changes */
		ListenerList_t m_layer_listener_list;


}; /* LayerManager */

#endif /* _CINEPAINT_LAYER_MANAGER_H_ */
