/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Abstract UI Definition Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CPPluginUIProgress.h,v 1.2 2006/12/18 08:21:21 robinrowe Exp $
 */
 
#ifndef _CP_PLUGINUI_PROGRESS_H_
#define _CP_PLUGINUI_PROGRESS_H_

#include "dll_api.h"
#include "CPPluginUIWidget.h"

class CINEPAINT_CORE_API CPPluginUIProgress : public CPPluginUIWidget
{
public:

	CPPluginUIProgress(char* name, char* text, double max, double step);
	~CPPluginUIProgress();

	/**
	 * Sets the status text displayed by the progress output
	 * @param txt The text to display.
	**/
	void SetText( char* txt);
	
	/**
	 * Reset the progress bar to 0.
	 * @param max the maximum number of steps
	 * @param step to number of steps to reach the 100%
	**/
	void ResetProgress(double max, double step);
	
	/**
	 * Move the progress bar on a step.
	 * @return the new position of the progress bar.
	**/
	void UpdateProgress();

	/**
	 * Move the progress bar to a specific position.
	 * @param pos the position to set the bar too.
	**/
	void SetProgress(double pos);

	/**
	 * Dialog is finsihed so remove from screen,
	 **/
	void Hide();


	void Create();
private:

	double m_max;
	double m_step;
	double m_curr;
	char* m_text;
};

#endif //#ifndef _CP_PLUGINUI_PROGRESS_H_
