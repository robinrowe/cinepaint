/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Pixel - Abstract pixel class
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: Pixel.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "Pixel.h"
#include "ImageUtils.h"
#include "CinePaintTag.h"
#include "Color.h"
#include <memory>

/**
 * Constructs a new Pixel representing a single pixel of image data
 * A new data buffer is allocated to hold the pixels of type as specified by tag.
 * This internal buffer remains managed by this Pixel
 *
 * @param tag the CinePaintTag indicating the buffer type
 */
Pixel::Pixel(const CinePaintTag& tag)
{
	m_tag = tag;
	m_data = new unsigned char[m_tag.GetBytes()];
}

/**
 * Constructs a new Pixel of the specified CinePaintTag format initialized to the specified Color
 *
 * @param tag the CinePaintTag indicating the buffer type
 * @param color the COlor to initizlize the Pixel to
 */
Pixel::Pixel(const CinePaintTag& tag, const Color& color)
{
	m_tag = tag;
	m_data = new unsigned char[m_tag.GetBytes()];

	ImageUtils::ColorToPixel(color, *this);
}

/**
 * Desctructor
 */
Pixel::~Pixel()
{
	delete[] m_data;
}

/**
 * Returns the CinePaintTag of this Pixel
 *
 * @return the CinePaintTag of this Pixel
 */
const CinePaintTag& Pixel::GetTag() const
{
	return(m_tag);
}

CinePaintTag& Pixel::GetTag()
{
	return(m_tag);
}

/**
 * Returns direct access to the pixel data
 * The returned data remains managed by this Pixel and should not be destroyed directly.
 *
 * @return direct access to the pixel data
 */
unsigned char* Pixel::GetData()
{
	return(m_data);
}

/**
 * Copies the specified image data into this pixel.
 * The number of bytes copied is determined must match the format of this Pixel as specified
 * by the CinePaintTag.
 * 
 * @param data the new Pixel data
 */
void Pixel::SetData(unsigned char* data)
{
	memcpy(m_data, data, m_tag.GetBytes());
}
