/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ImageUtils = Image module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ImageUtils.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _CINEPAINT_IMAGE_UTILS_H_
#define _CINEPAINT_IMAGE_UTILS_H_

#include "dll_api.h"

// Forward declaration
class AbstractBuf;
class Color;
class Pixel;

/**
 * A Collection of various static Image Utility methods
 *
 */
class CINEPAINT_IMAGE_API ImageUtils
{
	public:
		// default destructor ok

		/**
		 * Converts the specified Color to a formatted Pixel representation
		 * The Pixel contains a formatted buffer of image data representing a single pixel of color within an image data buffer.
		 * The format of the data is specified by Pixel::GetTag() and is therefore suitable for directly writing into a data
		 * buffer of the same Tag format.
		 * If a suitable conversion handler for the Tag format is known, this method returns true, otherwise false is returned.
		 *
		 * @param color the Color to set the specified Pixel
		 * @param pixel the Pixel to set to a fomatted representation of color
		 * @return true if the conversion succeeded, false otherwise.
		 */
		static bool ColorToPixel(const Color& color, Pixel& pixel);

		/**
		 * Converts from a gimp icon format into an AbstractBuf
		 * The gimp icon data specified by the bits parameter is copied into the speciedi AbstractBuf.
		 * The minimum of the gimp icon and the AbstractBuf are used as the size to copy.
		 *
		 * Currently only RGB AbstractBuf's are handled.
		 *
		 * @param buf the AbstractBuf to write the gimp icon into
		 * @param bits the gimp icon data
		 * @param width the width of the gimp icon
		 * @param height the height of the gimp icon
		 */
		static AbstractBuf& GimpIconToAbstractBuf(AbstractBuf& buf, char** bits, int width, int height);


	protected:
		/**
		 * protected Constructor to avoid instances
		 */
		ImageUtils();

	private:

};

#endif /* _CINEPAINT_IMAGE_UTILS_H_ */
