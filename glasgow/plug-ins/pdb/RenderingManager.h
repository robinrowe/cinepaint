/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      RenderingManager - Manages rendering
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: RenderingManager.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _CINEPAINT_RENDERING_MANAGER_H_
#define _CINEPAINT_RENDERING_MANAGER_H_

#include "dll_api.h"

#include <map>
#include <string>

//Forward Declaration
class CinePaintImage;
class AbstractRenderer;

/**
 * RenderingManager manages a set of Rendering objects which 'render' a CinePaintImage in some way.
 * This intention of RenderManager is to separate all of the 'rendering' management control from CinePaintImage,
 * allowing multiple rendering altenatives to be registered, for example, we could register a 'fast'
 * rendering implementation, or a scaled down 'small' preview renderer, or a complete full size, full quality renderer.
 * Rendering need only be initiated if the current render is out of date, and the render is required for display,
 * therefore it is not necessary to render all registered Renderes at every change.
 *
 * @note [claw] this still needs some thought ...
 */
class CINEPAINT_IMAGE_API RenderingManager
{
	public:
		/**
		 * Constructs a new Renderingmanager for the specified CinePaintImage
		 * The RenderingManager initially contains zero renderers
		 *
		 */
		RenderingManager(CinePaintImage& image);

		/**
		 * Destructor - destroys all registered Renderers
		 */
		virtual ~RenderingManager();

		//---------------------
		// Accessors / Mutators

		/**
		 * Sets the rendered image within each registered renderer invalid
		 * Conveinience method for a group update.
		 *
		 */
		void SetInvalid();

		/**
		 * Registers the specified renderer with this ReneringManager.
		 * This renderingManager assumes responsibility for the renderer, and will destroy the resource
		 * when this RenderingManager is destroyed.
		 * If a renderer is already registered with the specified name, the renderer is not registered and false is returned.
		 *
		 * @param name the name to register the renderer against
		 * @param renderer the AbstractRenderer to register
		 */ 
		bool RegisterRenderer(const char* name, AbstractRenderer* renderer);

		/**
		 * Unregisters and returns the Renderer registered with the specified name
		 * Ownership of the Renderer resource passes back to the caller.
		 * If no Renderer is registered with the specified name, no firther action is taken.
		 *
		 * @param name the registered Renderer name.
		 * @return the unregistered Renderer
		 */
		AbstractRenderer* UnregisterRenderer(const char* name);

		/**
		 * returns the AbstractRenderer registered with the specified name.
		 * If no AbsractRenderer is registered, this method returns 0
		 *
		 * @param name the registered name of the AbstactRenderer to return
		 * @return the AbstractRenderer registered against the specified name
		 */
		AbstractRenderer* GetRenderer(const char* name) const;

		/**
		 * Returns true if this Renderingmanager contains a renderer registered with the specified name.
		 * If no renderer is found with the specified name, 0 is returned.
		 *
		 * @param name the name of the registered Renderer
		 * @param the Renderer registered with the specified name, or 0 if no Renderer is registered
		 */
		bool HasRenderer(const char* name) const;

		/**
		 * Sets the default renderer of this RenderingManager.
		 * If this Renderingmanager does not manage the specified Renderer, it is first registered
		 * using RegisterRenderer with the name Renderingmanager::DEFAULT_NAME before being set as
		 * the default renderer.
		 *
		 * @param renderer the renderer to set as the default renderer
		 * @todo handle a default name clash
		 */
		void SetDefaultRenderer(AbstractRenderer* renderer);

		/**
		 * Returns the default renderer of thie renderingManager
		 * If no default Renderer is set, 0 is returned.
		 *
		 * @return the default renteror
		 */
		AbstractRenderer* getDefaultRenderer() const;


		// Static Data

		static const char* DEFAULT_NAME;

	protected:

	private:

		typedef std::map<std::string, AbstractRenderer*> RendererContainer_t;
		typedef std::pair<std::string, AbstractRenderer*> RenderingContainerPair_t;

		/** registered Renderers */
		RendererContainer_t m_renderers;

		/** the CinePaint Image the registered renderers render */
		CinePaintImage& m_image;

		/** the default renderer */
		AbstractRenderer* m_default_renderer;

} ; /* class RenderingManager */

#endif /* _CINEPAINT_RENDERING_MANAGER_H_ */
