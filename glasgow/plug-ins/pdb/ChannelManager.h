/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ChannelManager - Image Module.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ChannelManager.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _CINEPAINT_CHANNEL_MANAGER_H_
#define _CINEPAINT_CHANNEL_MANAGER_H_

#include "dll_api.h"

#include <list>

// Forward declaration
class CinePaintImage;
class Channel;

/**
 * ChannelManager manages the Channels if a CinePaintImage
 * Layer[0] is the background, Layer[size - 1] is the foreground.
 *
 */
class CINEPAINT_IMAGE_API ChannelManager
{
	public:

		/**
		 * Constructa a new ChannelManager to manage a Channels within a CinePaintImage.
		 * ChannelManager is intended to be used with an instance of a CinePaintImage to separate
		 * the handling of Channels from the rest of the CinePaintImage class.
		 *
		 * @param image the CinePaintImage this ChannelManager manages Channels for
		 */
		ChannelManager()
		{//	m_image=0;
			m_active_channel = 0;
		}
/*
		void SplitInit(CinePaintImage* image)
		{	m_image=image;
		}
*/		
		/**
		 * Destructor - destroys all managed Channels
		 */
		virtual ~ChannelManager()
		{}

		//-------------------------
		// Channel Addition / Removal

		/**
		 * Adds the secified Channel to this ChannelManager at the specified position
		 * Once added, the ChannelManager assumes responsibility for the Channel until the Channel is removed.
		 * If the Channel is already managed by this ChannelManager, the Channel is not added a second time and
		 * false is returned, otherwise the Channel is added and true is returned.
		 *
		 * @param chennal the Channel to add
		 * @param active set true if the newly added Channel is the new active Channel
		 * @return true if the Channel is added, false otherwise
		 */
		bool AddChannel(Channel* channel, bool active);

		/**
		 * Removes and returns the specified Channel from this ChannelManager
		 * Resopnsibility for the removed Channel passes to the caller.
		 * If the Specified Channel is not managed by this ChannelManager, no further action is taken
		 * and 0 is returned
		 *
		 * @param channel the Channel to be removed
		 * @return the removed Channel or 0 if the specified Channel is not managed by this ChannelManager
		 */
		Channel* RemoveChannel(Channel* channel);

        

		//---------------
		// Channel Access

		/**
		 * Returns the currently active Channel
		 *
		 * @return the currently active Channel
		 */
		Channel* GetActiveChannel() const;

		/**
		 * Sets the currently active Channel to the specified Channel
		 * If the specified channel is not managed by this ChannelManager, the currently
		 * active Channel is set to 0, and false is returned.
		 *
		 * @param channel the Channel to set as the Active Channel
		 * @return false if the Channel is not managed by this ChannelManager, true otherwise
		 */
		bool SetActiveChannel(Channel* channel);


		//------------------
		// Layer Positioning

		/**
		 * Raises the specified layer within the Layer stack by one place
		 *
		 * @param layer the Layer to raise
		 */
		void RaiseChannel(Channel* channel);

		/**
		 * Lowers the specified Layer within the Layer stack by one place
		 *
		 * @param layer the Layer to lower
		 */
		void LowerChannel(Channel* channel);


		/**
		 * Returns the number opf channels within this ChannelManager
		 *
		 * @return the number of channels within this Chanenlemanager
		 */
		size_t GetChannelCount() const;

	protected:

	private:
		/**
		 * Returns true if this ChannelManager manages the specified Channel
		 *
		 * @param channel the Channel to search for
		 * @return true if this ChannelManager manages the specified Channel, false otherwise
		 */
		bool has_channel(const Channel* channel) const;

		/** the CinePaintImage this ChannelManager works for */
//		CinePaintImage* m_image;

		// we want a little of both a list and a vector/deque here...
		typedef std::list<Channel*> ChannelContainer_t;

		/** the managed channels */
		ChannelContainer_t m_channels;

		/** the currently active layer */
		Channel* m_active_channel;


}; /* ChannelManager */

#endif /* _CINEPAINT_CHANNEL_MANAGER_H_ */
