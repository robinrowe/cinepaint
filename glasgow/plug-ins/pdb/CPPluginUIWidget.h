/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Abstract UI Definition Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CPPluginUIWidget.h,v 1.4 2006/12/18 08:21:21 robinrowe Exp $
 */
 
#ifndef _CP_PLUGINUI_WIDGET_H_
#define _CP_PLUGINUI_WIDGET_H_

#include "dll_api.h"

/**
 * Abstract class to provide to level application functionality.
 * You must derive your own class from this to build an application.
 * The InitInstance method must be overridden to initalise your windows etc.
**/
class /*CINEPAINT_CORE_INTERFACES_API*/ CPPluginUIWidget
{

public:

	enum WidgetClassT { BOOL, DIALOG, LIST, NUMBER, PROGRESS, TEXT };
	virtual ~CPPluginUIWidget() {};

	WidgetClassT GetType() { return m_type; };
	// TODO: Make const
	char*  GetName() { return m_name; };

protected:
	CPPluginUIWidget(WidgetClassT type, char* name)
	: m_type(type), m_name(name)
	{}
	
	const WidgetClassT m_type; 

	// TODO: Make const
	char* m_name;
};

struct CPPluginUIDef
{
	CPPluginUIWidget **widgets;
	int num;

};

#endif //#ifndef _CP_PLUGINUI_WIDGET_H_
