/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                     Channel - Model for basic channels.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: Channel.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "Channel.h"


/**
 * Constructs a new Channel as part of the specified CinePaintImage.
 *
 * @param tag CinePaintTag describing the data format
 * @param width the wieth of this Channel
 * @param height ther height of the Channel
 * @param name the name of this Channel
 * @param opacity the opacity of this Channel (0.0 transparent to 1.0 fully opaque)
 * @param c the Color of this Channel
 */
Channel::Channel(const CinePaintTag& tag, int width, int height, char* name, float opacity, const Color& c)
: Drawable(tag, width, height, name), m_color(c)
{
	m_opacity = opacity;

	m_active = false;
}

/**
 * Destoys this Channels and underlying image buffer
 */
Channel::~Channel()
{
	// Drawable destructor handles eveything that needs done
}



//---------------------
// Accessors / Mutators

/**
 * Sets the active state of this Channel
 *
 * @param active the active state of thie Channel
 */
void Channel::SetActive(bool active)
{
	m_active = active;
}

/**
 * Returns the active state of this Channel
 *
 * @return the active statet of this Channel
 */
bool Channel::GetActive() const
{
	return(m_active);
}






bool Channel::ChannelValidator::InitPortion(AbstractBuf& buf, int x, int y, int width, int height)
{
	unsigned char* _d = buf.GetPortionData(x, y);

	// Set the contents of the tile to empty */
	memset(_d, 0, width * height * buf.GetTag().GetBytes());

	return(true);
}
