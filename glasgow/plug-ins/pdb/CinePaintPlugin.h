/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintPlugin Interface
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintPlugin.h,v 1.3 2006/12/21 11:18:08 robinrowe Exp $
 */
 
#ifndef _CinePaint_PLUGIN_H_
#define _CinePaint_PLUGIN_H_

/******
 * CinePaintPlugin provides an abstract interfaceclass that is used to define 
 * arbitrary plugins, which are install, and executed as required.
 * The constructor is protected to ensure a derived class is created.
 * Each plugin should derive a class from this and export that from its own
 * dynamic library.
*/

#include <map>
#include "dll_api.h"
#include "plug-ins/pdb/CPPluginUIWidget.h"
#include "plug-ins/pdb/CPParamList.h"
#include "World.h"

class AbstractBuf;



class CINEPAINT_CORE_INTERFACES_API CinePaintPlugin
{protected:
#if 0
	World* world;
#endif
	const char* name;
	const char* blurb;
	const char* help;
	const char* author;
	const char* copyright;
	const char* date;
/*	CinePaintPlugin()
	{	name=0;
		blurb=0;
		help=0;
		author=0;
		copyright=0;
		date=0;
		m_run_mode = CinePaintPlugin::INTERACTIVE;
	}

*/
	CinePaintPlugin(const char* name,
								 const char* blurb,
								 const char* help,
                                 const char* author,
								 const char* copyright,
                                 const char* date)
	:name(name), blurb(blurb), help(help),author(author), copyright(copyright), date(date)
{
	m_run_mode = CinePaintPlugin::INTERACTIVE;
}

public:
	enum run_mode_t { INTERACTIVE, NONINTERACTIVE };
	typedef CinePaintPlugin * (*PFNCreateCinePaintPlugin)();
	run_mode_t m_run_mode;
	run_mode_t GetRunMode() { return m_run_mode;};
	void SetRunMode( run_mode_t mode ) { m_run_mode = mode; };
// rsr: this looks like a BUG:
	typedef void (*PFNDeleteCinePaintPlugin)(CinePaintPlugin *);
	typedef PFNCreateCinePaintPlugin * (*PFNGetPluginClassList)();

	virtual ~CinePaintPlugin() {};
	virtual void query(World*) = 0;
//	virtual void Register() = 0;
	 virtual void run(CPParamList& _inParams, CPParamList& _return) = 0;
//	 virtual bool Run(const char* cmd) = 0;

	 /**
	  * Get the user interface object required by this plugin.
	  * @return a CPPluginUIDef struct.
	  **/
	 virtual CPPluginUIDef &GetUIDefs(CPPluginUIDef &ui);
	 virtual PFNDeleteCinePaintPlugin GetDeleteFunc( ) { return 0; };

	 const char* GetName() { return name; };
	 const char* GetBlurb() { return blurb; };
	 const char* GetHelp() { return help; };
	 const char* GetAuthor() { return author; };
	 const char* GetCopyright() { return copyright; };
	 const char* GetDate() { return date; };


	 //==========================================
	 // Plugin Parameter Definitions
	 
	 /**
	  * Struct describing a plugin parameter
	  *
	  */
	 struct CPPluginParamDef
	 {
		 CPPluginArgType m_type;    // the parameter type
		 const char* m_name;        // the parameter name, ie the name within the param list
		 const char* m_description; // brief description of the parameter
		 const bool m_modifies;     // indicates if the plugin modifies an input parameter
                                    // this is usefull only for image data parameter types
                                    // which a plugin may work on inplace, rather than create a copy
	 };

	 /**
	  * Struct used for returning the plugins paramater definitions
	  * @TODO: Perhaps rename to avoid confusion with ProcedureDB struct of same name.
	  */
	 struct CPPluginParamDefs
	 {
		 const CPPluginParamDef* m_params;    // array of 0 or more plugin parameter definitions
		 int m_param_count;                   // number of plugin parameter definitions within the m_params array
	 };

	 /**
	  * Returns the parameter definitions of expected inputs to this CinePaintPlugin
	  *
	  * @param param_defs to be populated with the input parameter definitions of this plugin
	  * @return param_defs the input parameters of this plugin
	  */
	 virtual CPPluginParamDefs& GetInParamDefs(CPPluginParamDefs& param_defs) const = 0;

	 /**
	  * Returns the parameters definitions of the parameters returned by this CinePaintPlugin
	  *
	  * @param param_defs to be populated with the return parameter definitions of this plugin
	  * @return param_defs the return parameters of this plugin
	  */
	 virtual CPPluginParamDefs& GetReturnParamDefs(CPPluginParamDefs& param_defs) const = 0;
};



class CINEPAINT_CORE_INTERFACES_API CinePaintPluginInfo
{
public:
	CinePaintPluginInfo(CinePaintPlugin::PFNCreateCinePaintPlugin ctorFunc,
						const char* name,
                        const char* menupath,
						const char* image_types);
	~CinePaintPluginInfo() {};

	const char* GetName() {return m_name;};
	const char* GetMenuPath() { return m_menupath;};
	CinePaintPlugin::PFNCreateCinePaintPlugin GetCtor() { return m_ctorFunc;};


private:
	CinePaintPlugin::PFNCreateCinePaintPlugin m_ctorFunc;
	const char* m_name;
	const char* m_menupath;
	const char* m_image_types;
};


class CINEPAINT_CORE_INTERFACES_API CinePaintFileHandlerInfo
{
public:
	CinePaintFileHandlerInfo(CinePaintPlugin::PFNCreateCinePaintPlugin ctorFunc,
							 const char* name,
							 const char* extensions,
                             const char* prefixes,
                             const char* magics = "");
	~CinePaintFileHandlerInfo() {};

	const char* GetName() {return m_name;};
	const char* GetExtensions() {return m_extensions;};
	const char* GetPrefixes() {return m_prefixes;};
	const char* GetMagics() {return m_magics;};
	CinePaintPlugin::PFNCreateCinePaintPlugin GetCtor() { return m_ctorFunc;};

	bool CheckExtension(const char* ext);

private:
	CinePaintPlugin::PFNCreateCinePaintPlugin m_ctorFunc;
	const char* m_name;
	const char* m_extensions;
	const char* m_prefixes;
	const char* m_magics;
};


#endif //_CinePaint_PLUGIN_H_
