/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      ImageOpManager - manage image operations.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 * 
 * $Id: ImageOpManager.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "ImageOpManager.h"
#include "plug-ins/pdb/CinePaintImageOp.h"
#include "plug-ins/pdb/CinePaintScaleOp.h"
#include "plug-ins/pdb/CinePaintFillOp.h"
#include "plug-ins/pdb/CinePaintCompositeOp.h"
#include "plug-ins/pdb/CinePaintRenderOp.h"
#include "plug-ins/pdb/CinePaintConversionOp.h"
#include <algorithm>
#include <map>


/**
 * Constructs a new CinePaintImageOp Manager initially managing no CinePaintImageOps
 */
ImageOpManager::ImageOpManager()
{}

/**
 * Destructor
 */
ImageOpManager::~ImageOpManager()
{
	ImageOpContainer_t::iterator iter = m_image_ops.begin() ;

	while(iter != m_image_ops.end())
	{
		ImageOpContainer_t::iterator next = iter;
		++next;

		CinePaintImageOp* _t = iter->second;
		m_image_ops.erase(iter);

		delete _t;
		_t = 0;

		iter = next;
	}
}



//-----------------------------
// CinePaintImageOp managerment

/**
 * Register the specified CinePaintImageOp with this ImageOpManager under the spcified Image Op type string.
 * If the specified CinePaintImageOp is a 'core' application operation, one of the image op type strings
 * defined within CinePaintImageOp should be used.
 * This ImageOpManager assumes responsibilty for the resources of the registered CinePaintImageOp.
 *
 * @param op_type the image op type being registered
 * @param op the CinePaintImageOp to register
 */
void ImageOpManager::RegisterImageOp(const char* op_type, CinePaintImageOp* op)
{
	// @note [claw] do somthing about duplicates
	m_image_ops.insert(std::make_pair(std::string(op_type), op));
}

/**
 * Registers the specified CinePaintImageOp with this ImageOpManager
 * The CinePaintImageOp is registered with the image op type returned by the CinePaintImageOp itself.
 *
 * @param op the CinePaintImageOp to register
 */
void ImageOpManager::RegisterImageOp(CinePaintImageOp* op)
{
	RegisterImageOp(op->GetImageOpType(), op);
}




//----------
// Accessors

/**
 * Returns a CinePaintImageOp of the specified type suitable for use with image data of the specified image type tag
 * If no suitable image op is found, for the speficified type suporting the specified CinePaintTag, 0 is returned
 *
 * @param op_type the image op type to return
 * @param tag the image data type tag the image op must support
 * @return a CinePaintImageOp of the specified type suitable for use on image data of the specifice image data type tag,
 *         or 0 if no suitable CinePaintImageOp can be found
 */
CinePaintImageOp* ImageOpManager::GetImageOp(const char* op_type, const CinePaintTag& tag) const
{
	CinePaintImageOp* _image_op = 0;

	std::pair<ImageOpContainer_t::const_iterator, ImageOpContainer_t::const_iterator> _range = m_image_ops.equal_range(std::string(op_type));

	bool _found = false;
	while(!_found && (_range.first != _range.second))
	{
		CinePaintImageOp* _temp_op = _range.first->second;

		if(_temp_op->SupportsTag(tag))
		{
			_image_op = _temp_op;
			_found = true;
		}

        ++_range.first;
	}

	return(_image_op);
}



//------------------------------------------
// Specific Named CinePaintImageOp Accessors

/**
 * Returns a ScaleOp suitable for scaling image data of the specified CinePaintTag type
 * If no suitable ScaleOp is registered for the specified CinePaintTag type, 0 is returned
 *
 * @patam tag image data type
 * @return a ScaleOp suitable for scaling iage data of the specified CinePaintTag type, or 0
 *         if no suitable ScaleOp is found
 */
CinePaintScaleOp* ImageOpManager::GetScaleOp(const CinePaintTag& tag) const
{
	CinePaintScaleOp* _scale_op = 0;

	CinePaintImageOp* _image_op = GetImageOp(CinePaintImageOp::GetIMAGE_OP_TYPE_SCALE(), tag);

	if(_image_op)
	{
		_scale_op = dynamic_cast<CinePaintScaleOp*>(_image_op);
	}

	return(_scale_op);
}

CinePaintFillOp* ImageOpManager::GetFillOp(const CinePaintTag& tag) const
{
	CinePaintFillOp* _fill_op = 0;

	CinePaintImageOp* _image_op = GetImageOp(CinePaintImageOp::GetIMAGE_OP_TYPE_FILL(), tag);

	if(_image_op)
	{
		_fill_op = dynamic_cast<CinePaintFillOp*>(_image_op);
	}

	return(_fill_op);
}

CinePaintCompositeOp* ImageOpManager::GetCompositeOp(const CinePaintTag& tag) const
{
	CinePaintCompositeOp* _composite_op = 0;

	CinePaintImageOp* _image_op = GetImageOp(CinePaintImageOp::GetIMAGE_OP_TYPE_COMPOSITE(), tag);

	if(_image_op)
	{
		_composite_op = dynamic_cast<CinePaintCompositeOp*>(_image_op);
	}

	return(_composite_op);
}

CinePaintRenderOp* ImageOpManager::GetRenderOp(const CinePaintTag& tag) const
{
	CinePaintRenderOp* _render_op = 0;

	CinePaintImageOp* _image_op = GetImageOp(CinePaintImageOp::GetIMAGE_OP_TYPE_RENDER(), tag);

	if(_image_op)
	{
		_render_op = dynamic_cast<CinePaintRenderOp*>(_image_op);
	}

	return(_render_op);
}

CinePaintConversionOp* ImageOpManager::GetConversionOp(const CinePaintTag& tag) const
{
	CinePaintConversionOp* _conv_op = 0;

	CinePaintImageOp* _image_op = GetImageOp(CinePaintImageOp::GetIMAGE_OP_TYPE_CONVERSION(), tag);

	if(_image_op)
	{
		_conv_op = dynamic_cast<CinePaintConversionOp*>(_image_op);
	}

	return(_conv_op);
}