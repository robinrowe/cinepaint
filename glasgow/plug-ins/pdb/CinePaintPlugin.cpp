/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintPlugin Interface
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintPlugin.cpp,v 1.2 2006/12/18 08:21:21 robinrowe Exp $
 */

#include "CinePaintPlugin.h"
#include <string.h>

 CPPluginUIDef &CinePaintPlugin::GetUIDefs( CPPluginUIDef &info )
 {
	 info.num = 0;
	 info.widgets = 0;
	 return info;
 }


//=============================
// CinePaintPluginInfo Implementations
 
CinePaintPluginInfo::CinePaintPluginInfo(CinePaintPlugin::PFNCreateCinePaintPlugin ctorFunc,
                                         const char* name,
                                         const char* menupath,
                                         const char* image_types)
	:m_ctorFunc(ctorFunc), m_name(name), m_menupath(menupath), m_image_types(image_types)
{
}


 //=============================
 // CinePaintFileHandleInfo Implementations
 CinePaintFileHandlerInfo::CinePaintFileHandlerInfo(CinePaintPlugin::PFNCreateCinePaintPlugin ctorFunc,
							  const char* name,
							  const char* extensions,
                              const char* prefixes,
                              const char* magics )
	:m_ctorFunc(ctorFunc), m_name(name), m_extensions(extensions), m_prefixes(prefixes), m_magics(magics)
 {
 }

 /**
  * Convert buffer to upper case.
 **/
 char* upper(const char* buf)
 {
	 char* res = new char[strlen(buf)+1];
	 for (unsigned int i=0; i<strlen(buf)+1; i++ )
		 if ( buf[i] >= 'a' && buf[i]<='z' )
			 res[i] = buf[i]-'a'+'A';
		 else
			 res[i] = buf[i];
	 return res;
 }

/**
 * Calculate the length of the next extention string
 *
 */
int get_ext_size(const char* buf)
{
	int ret = 0;
	while(*buf && *buf != ',')
	{
		buf++;
		ret++;
	}

	return(ret);
}

bool CinePaintFileHandlerInfo::CheckExtension(const char* e)
{
	bool found = false;

	if(m_extensions && e)
	{
		char* extensions = upper(m_extensions);
		char* ext = upper(e);

		char* curr_ext = extensions;

		int ext_size = get_ext_size(ext);
		int curr_ext_size = get_ext_size(curr_ext);

		while(!found && *curr_ext)
		{
			if(curr_ext_size == ext_size)
			{
				found = strncmp(curr_ext, ext, ext_size) == 0;
			}

			// Find next ext in extension string.
			while(*curr_ext != ',' && *curr_ext)
			{
				curr_ext++;
			}

			// strp over the ',' char
			if(*curr_ext == ',')
			{
				curr_ext++;
				curr_ext_size = get_ext_size(curr_ext);
			}
		}

		// cleanup
		delete[] ext;
		delete[] extensions;
	}

	return(found);
}
