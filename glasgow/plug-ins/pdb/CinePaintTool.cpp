/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintTool - Abstract tool interface
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintTool.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "CinePaintTool.h"
#include "plug-ins/pdb/CinePaintDoc.h"
#include "plug-ins/pdb/CPPluginUIWidget.h"

const char* CinePaintTool::TOOL_TYPE_NONE = "cp.tool.none";
const char* CinePaintTool::TOOL_TYPE_SCALE = "cp.tool.scale";
const char* CinePaintTool::TOOL_TYPE_ZOOM = "cp.tool.zoom";
const char* CinePaintTool::TOOL_TYPE_RECT_SELECT = "cp.tool.rect_select";
const char* CinePaintTool::TOOL_TYPE_ELLIPSE_SELECT = "cp.tool.ellipse_select";
const char* CinePaintTool::TOOL_TYPE_FREE_SELECT = "cp.tool.free_select";
const char* CinePaintTool::TOOL_TYPE_FUZZY_SELECT = "cp.tool.fuzzy_select";
const char* CinePaintTool::TOOL_TYPE_BEZIER_SELECT = "cp.tool.bezier_select";
const char* CinePaintTool::TOOL_TYPE_MOVE = "cp.tool.move";
const char* CinePaintTool::TOOL_TYPE_TEXT = "cp.tool.text";
const char* CinePaintTool::TOOL_TYPE_ISCISSORS = "cp.tool.iscissors";
const char* CinePaintTool::TOOL_TYPE_FLIP = "cp.tool.flip";
const char* CinePaintTool::TOOL_TYPE_COLOR_PICKER = "cp.tool.color_picker";
const char* CinePaintTool::TOOL_TYPE_BLEND = "cp.tool.blend";
const char* CinePaintTool::TOOL_TYPE_CROP = "cp.tool.crop";
const char* CinePaintTool::TOOL_TYPE_BUCKET_FILL = "cp.tool.bucket_fill";
const char* CinePaintTool::TOOL_TYPE_AIRBRUSH = "cp.tool.airbrush";
const char* CinePaintTool::TOOL_TYPE_CLONE = "cp.tool.clone";
const char* CinePaintTool::TOOL_TYPE_CONVOLVE = "cp.tool.convolve";
const char* CinePaintTool::TOOL_TYPE_DODGE = "cp.tool.dodge";
const char* CinePaintTool::TOOL_TYPE_ERASER = "cp.tool.eraser";
const char* CinePaintTool::TOOL_TYPE_PAINT = "cp.tool.paint";
const char* CinePaintTool::TOOL_TYPE_PENCIL = "cp.tool.pencil";
const char* CinePaintTool::TOOL_TYPE_SMUDGE = "cp.tool.smudge";
const char* CinePaintTool::TOOL_TYPE_MEASURE = "cp.tool.measure";

/**
 * Protected Constructor - disallow instances
 *
 * The group parameter is a string of the form "group/subgroup/..." which may be used
 * to group related tools.
 *
 * @param name the name of this CinePaintTool
 * @param group tool grouping, used to group related tools
 * @param blurb 
 * @param help 
 * @param author author of This CinePaintTool
 * @param copyright copryright notice for this CinePaintTool plugin
 * @param date date this CinePaintTool was written
 */
CinePaintTool::CinePaintTool(const char* name, const char* group, const char* blurb, const char* help, const char* author, const char* copyright, const char* date)
 : m_name(name), m_group(group), m_blurb(blurb), m_help(help), m_author(author), m_copyright(copyright), m_date(date)
{
	m_doc = 0;
}

/**
 * Returns the name of this CinePaintTool
 *
 * @return the name of this CinePaintTool
 */
const char* CinePaintTool::GetName() const
{
	return(m_name);
}

/**
 * Returns the tool group of this CinePaintTool
 * The tool groups is a string of the form "group/subgroup/..." representing
 * a group of related tools
 *
 * @return the tool group
 */
const char* CinePaintTool::GetGroup() const
{
	return(m_group);
}


/**
 * Sets the current state of this CinePaintTool
 *
 * @param state the current CinePaintTool state
 */
void CinePaintTool::SetState(CinePaintTool::ToolState state)
{
	m_state = state;
}

/**
 * Returns the current state of this CinePaintTool
 *
 * @retunr the current state of this CinePaintTool
 */
CinePaintTool::ToolState CinePaintTool::GetState() const
{
	return(m_state);
}


/**
 * Returns the UI definition for this CinePaintTool
 *
 * @param info populated with the definition of this tools ui panel
 * @return info paramater populated with thie tools ui definition
 */
CPPluginUIDef& CinePaintTool::GetUIDefs(CPPluginUIDef& info)
{
	info.num = 0;
	info.widgets = 0;
	return info;
}

/**
 * Protected - Get the current document only derived class can get this.
 * @return returs the current document as a CinePaintImage
 * returns 0 if not document set or it is not a CinePaintImage.
**/
CinePaintImage *CinePaintTool::GetActiveImage()
{
	if (m_doc && m_doc->GetDocType() == CinePaintDoc::CINEPAINT_IMAGE_ENUM)
		return ( reinterpret_cast<CinePaintImage*>(m_doc));
	else
		return 0;

}
