/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Abstract UI Definition Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CPPluginUIDialog.h,v 1.3 2006/12/18 08:21:21 robinrowe Exp $
 */
 
#ifndef _CP_PLUGINUI_DIALOG_H_
#define _CP_PLUGINUI_DIALOG_H_

#include "dll_api.h"
#include "CPPluginUIWidget.h"
#include "enums.h"
#include <list>


class CINEPAINT_CORE_API CPPluginUIDialog : public CPPluginUIWidget
{
public:

	typedef std::list<CPPluginUIWidget *>::iterator widgetIter;

	CPPluginUIDialog(char* name, char* title, int w, int h);
	~CPPluginUIDialog();

	/**
	 * Add a widget to this dialog.
	 * @param widget the widget to add.
	**/
	void AddWidget(CPPluginUIWidget *widget);

	/**
	 * Called to wait on the user hitting OK in the dialog.
	**/
	DialogResponse DoModal();

	/**
	 * Called to create the dialog as a modeless dialog.
	**/
	void Create();

	/**
	 * Called to draw the widget 
	 * virtual member from the base class.
	 **/
	virtual void Draw();

	//====================
	// Access funtions for Dialog data members.


/*	char* GetTitle() { return m_title; };
	int GetWidth() { return m_width;};
	int GetHeight() { return m_height;};
*/
	char* GetTitle();
	int GetWidth();
	int GetHeight();
	

private:

	std::list<CPPluginUIWidget *> m_widgetList;

	char* m_title;
	int m_width;
	int m_height;

public:
	widgetIter begin() { return m_widgetList.begin(); }
	widgetIter end() { return m_widgetList.end(); }
};

#endif //#ifndef _CP_PLUGINUI_DIALOG_H_
