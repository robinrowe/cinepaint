/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePintTag Image Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintTag.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */


#include "CinePaintTag.h"
#include <cctype>
#include <iostream>
#include <iomanip>

const std::string CinePaintTag::PRECISION_U8_STRING("8-bit Unsigned Integer");
const std::string CinePaintTag::PRECISION_U16_STRING("16-bit Unsigned Integer");
const std::string CinePaintTag::PRECISION_FLOAT16_STRING("16-bit RnH Short Float");
const std::string CinePaintTag::PRECISION_FLOAT_STRING("32-bit IEEE Float");
const std::string CinePaintTag::PRECISION_BFP_STRING("16-bit Fixed Point 0-2.0");

const std::string CinePaintTag::FORMAT_RGB_STRING("RGB");
const std::string CinePaintTag::FORMAT_GRAY_STRING("Grayscale");
const std::string CinePaintTag::FORMAT_INDEXED_STRING("Indexed");


/**
 * Constructs an intially null CinePaintTag
 * A null as a precision of PRECISION_NONE_ENUM and format FORMAT_NONE_ENUM
 *
 */
CinePaintTag::CinePaintTag()
: m_precision(PRECISION_NONE_ENUM), m_format(FORMAT_NONE_ENUM), m_alpha(false)
{}

/**
 * Constructs a new CinePaintTag with the specified precision, format and alpha value
 *
 * @param precision the data Precision represented by this CinePaintTag
 * @param format the data format represented by this CinePaintTag
 * @param alpha set true if this CinePaintTag represents data with an alpha channel
 */
CinePaintTag::CinePaintTag(Precision precision, Format format, bool alpha)
: m_precision(precision), m_format(format), m_alpha(alpha)
{}

/**
 * Destructor
 */
CinePaintTag::~CinePaintTag()
{}



/**
 * Returns the Precision of the data this CinePaintTag represents
 *
 * @return the Precision value of this CinePaintTag
 */
CinePaintTag::Precision CinePaintTag::GetPrecision() const
{
	return(m_precision);
}

/**
 * Returns the Format of the data this CinePaintTag represents
 * 
 * @return the Format value of this CinePaintTag
 */
CinePaintTag::Format CinePaintTag::GetFormat() const
{
	return(m_format);
}

/**
 * Returns true if this CinePaintTag represents a data format with a data channel
 *
 * @return true if this CinePaintTag represents data with an alpha channel, false otherwise
 */
bool CinePaintTag::HasAlpha() const
{
	return(m_alpha);
}

/**
 * Sets the Presicion of this CinePaintTag to the specified Precision value
 *
 * @param precision the new Precision value of this CinePaintTag
 */
void CinePaintTag::SetPrecision(Precision precision)
{
	m_precision = precision;
}

/**
 * Sets the Format of this CinePaintTag to the specified Format value
 *
 * @param format the new Format value of this CinePaintTag
 */
void CinePaintTag::SetFormat(Format format)
{
	m_format = format;
}

/**
 * Sets whether this CinePaintTag represents an alpha channel
 *
 * @param alpha set true to indicate an alpha channel is present, false otherwise
 */
void CinePaintTag::SetAlpha(bool alpha)
{
	m_alpha = alpha;
}

/**
 * Returns the number of channels represented by this CinePaintTag
 *
 * @return the number of channels represented by this CinePaintTag
 */
int CinePaintTag::GetNumChannels() const
{
	int _channels = 0;

	switch(GetFormat())
	{
		case FORMAT_RGB_ENUM:
		{
			_channels =	3;
			break;
		}
		case FORMAT_GRAY_ENUM:
		{
			case FORMAT_INDEXED_ENUM:
			_channels =	1;
			break;
		}
		case FORMAT_NONE_ENUM:
		default:
		{
			_channels = 0;
		}
	}

	if((_channels > 0) && HasAlpha())
	{
		_channels++;
	}

	return(_channels);
}

/**
 * Returns the number of bytes per pixel for the data represented y this CinePaintTag
 * The number of bytes is calculated as;
 * (number of channels + optional alpha channel) x sizeof data type
 *
 * @return the number of bytes per pixel
 */
int CinePaintTag::GetBytes() const
{
	int _bytes = GetNumChannels();

	switch(GetPrecision())
	{
		case PRECISION_U8_ENUM:
		{
			_bytes *= sizeof(unsigned char);
			break;
		}
		case PRECISION_BFP_ENUM:
		case PRECISION_U16_ENUM:
		{
			_bytes *= sizeof(unsigned short);
			break;
		}
		case PRECISION_FLOAT_ENUM:
		{
			_bytes *= sizeof(float);
			break;
		}
		case PRECISION_FLOAT16_ENUM:
		{
			_bytes *= sizeof(unsigned short);
			break;
		}
		case PRECISION_NONE_ENUM:
		{
			return(0);
		}
	}

	return(_bytes);
}

/**
 * Returns true if the specified CinePaintTag is equal to this CinePaintTag
 * Two CinePaintTags are equal of their Precision, Format and alpha values are equal
 *
 * @param tag the CinePaintTag to compare to this
 * @return trus if this CinePaintTag is equal to the specufued CinePaintTag, false otherwise
 */
bool CinePaintTag::Equals(const CinePaintTag& tag) const
{
	return(CinePaintTag::Equals(*this, tag));
}

/**
 * Returns true if the specified CinePaintTags are equal
 * Two CinePaintTags are equal of their Precision, Format and alpha values are equal
 *
 * @param first the first CinePaintTag to compare
 * @param second the second CinePaintTag to compare
 * @return true if first equals second, false otherwise
 */
bool CinePaintTag::Equals(const CinePaintTag& first, const CinePaintTag& second)
{
	if((first.GetPrecision() == second.GetPrecision())
	        && (first.GetFormat() == second.GetFormat())
	        && (first.HasAlpha() == second.HasAlpha()))
	{
		return(true);
	}
	else
	{
		return(false);
	}
}

/**
 * Returns true if this CinePaintTag is valid.
 * Certain combinations of Precision and Format are illegal, in these cases
 * this method returns false. For example, the Precision may not be a float data
 * type yet the format indexed.
 *
 * @return true if this CinePaintTag is valid, false otherwise
 */
bool CinePaintTag::IsValid() const
{
	bool _valid = true;

	if((GetPrecision() != PRECISION_NONE_ENUM) && (GetFormat() != FORMAT_NONE_ENUM))
	{
		// check for invalid combinations
		switch(GetPrecision())
		{
			case PRECISION_FLOAT16_ENUM:
			case PRECISION_FLOAT_ENUM:
			{
				if(GetFormat() == FORMAT_INDEXED_ENUM)
				{
					_valid = false;
				}
				break;
			}
			case PRECISION_U16_ENUM:
			case PRECISION_U8_ENUM:
			case PRECISION_BFP_ENUM:
			{
				break;
			}
			default:
			{
				_valid = false;
				break;
			}
		}
	}

	return(_valid);
}

/**
 * Returns true if this CinePaintTag is a null CinePaintTag.
 * A null tag is a CinePaintTag has a Precision type of PRECISION_NONE_ENUM
 * and a Format of FORMAT_NONE_ENUM
 *
 * @return true if this CinePaintTag is null
 */
bool CinePaintTag::IsNull() const
{
	if((GetPrecision() == PRECISION_NONE_ENUM) && (GetFormat() == FORMAT_NONE_ENUM))
	{
		return(false);
	}
	else
	{
		return(true);
	}
}




/**
 * Returns a string representation of the specified Precision value
 *
 * @param precision the Precision value to represent as a string
 * @return string representation of the specified Precision
 */
std::string CinePaintTag::GetPrecisionString(Precision precision)
{
	std::string _ret;

	switch(precision)
	{
		case PRECISION_U8_ENUM:
		{
			_ret = PRECISION_U8_STRING;
			break;
		}
		case PRECISION_BFP_ENUM:
		{
			_ret = PRECISION_BFP_STRING;
			break;
		}
		case PRECISION_U16_ENUM:
		{
			_ret = PRECISION_U16_STRING;
			break;
		}
		case PRECISION_FLOAT_ENUM:
		{
			_ret = PRECISION_FLOAT_STRING;
			break;
		}
		case PRECISION_FLOAT16_ENUM:
		{
			_ret = PRECISION_FLOAT16_STRING;
			break;
		}
		case PRECISION_NONE_ENUM:
		{
			_ret = "Uninitialized precision!";
			break;
		}
		default:
		{
			_ret = "Invalid Precision!";
			break;
		}
	}

	return(_ret);
}

/**
 * Returns a string representation of the specified Format value
 *
 * @param format the Format value to represent as a string
 * @return string representation of the specified Format
 */
std::string CinePaintTag::GetFormatString(Format format)
{
	std::string _ret;

	switch(format)
	{
		case FORMAT_NONE_ENUM:
		{
			_ret = "<Uninitialized Format!";
			break;
		}
		case FORMAT_RGB_ENUM:
			_ret = FORMAT_RGB_STRING;
			break;
		case FORMAT_GRAY_ENUM:
			_ret = FORMAT_GRAY_STRING;
			break;
		case FORMAT_INDEXED_ENUM:
			_ret = FORMAT_INDEXED_STRING;
			break;
		default:
			_ret = "Invalid Format!";
	}

	return(_ret);
}



//---------------------
// Operator Overloading

/**
 * Returns true if the specified CinePaintTag is equal to this CinePaintTag
 * Two CinePaintTags are equal of their Precision, Format and alpha values are equal
 *
 * @param tag the CinePaintTag to compare to this
 * @return trus if this CinePaintTag is equal to the specufued CinePaintTag, false otherwise
 */
bool CinePaintTag::operator == (const CinePaintTag& tag) const
{
	return(CinePaintTag::Equals(*this, tag));
}

/**
 * Outputs the serialized form of this CinePaintTag to the specified stream
 *
 * @param os the stream to write to
 * @param tag the CinePaintTag to write to the stream
 * @return the stream parameter
 */
CINEPAINT_IMAGE_API std::ostream& operator<< (std::ostream& os, const CinePaintTag& tag)
{
	os << std::setw(2) << std::setfill('0') << tag.m_precision;
	os << std::setw(2) << std::setfill('0') << tag.m_format;
	os << std::setw(1) << tag.m_alpha;

	return(os);
}

/**
 * Reads the serialized form of a CinePaintTag from the specified stream into the specified CinePaintTag
 * 
 * @param os the stream to read from
 * @param tag the CinePaintTag to deserialize into
 * @return the stream parameter
 */
CINEPAINT_IMAGE_API std::istream& operator>> (std::istream& is, CinePaintTag& tag)
{
	CinePaintTag::Precision _precision = CinePaintTag::PRECISION_NONE_ENUM;
	CinePaintTag::Format _format = CinePaintTag::FORMAT_NONE_ENUM;
	bool _alpha = false;

	char _p[3];
	char _f[3];
	char _a[2];

	// first two chars are the precision
	is.get(_p[0]).get(_p[1]);
	_p[2] = '\0';

	// next two chars are the format
	is.get(_f[0]).get(_f[1]);
	_f[2] = '\0';

	// final char is alpha
	is.get(_a[0]);
	_a[1] = '\0';

	if(isdigit(_p[0]) && isdigit(_p[1]))
	{
		int _pint = atoi(_p);
		switch(_pint)
		{
			case CinePaintTag::PRECISION_U8_ENUM:
			case CinePaintTag::PRECISION_BFP_ENUM:
			case CinePaintTag::PRECISION_U16_ENUM:
			case CinePaintTag::PRECISION_FLOAT_ENUM:
			case CinePaintTag::PRECISION_FLOAT16_ENUM:
			{
				_precision = CinePaintTag::Precision(_pint);
				break;
			}
			case CinePaintTag::PRECISION_NONE_ENUM:
			default:
			{
				_precision = CinePaintTag::PRECISION_NONE_ENUM;
				break;
			}
		}
	}

	if(isdigit(_f[0]) && isdigit(_f[0]))
	{
		int _fint = atoi(_f);

		switch(_fint)
		{
			case CinePaintTag::FORMAT_RGB_ENUM:
			case CinePaintTag::FORMAT_GRAY_ENUM:
			case CinePaintTag::FORMAT_INDEXED_ENUM:
			{
				_format = CinePaintTag::Format(_fint);
				break;
			}
			case CinePaintTag::FORMAT_NONE_ENUM:
			default:
			{
				_format = CinePaintTag::FORMAT_NONE_ENUM;
				break;
			}
		}
	}

	if(isdigit(_a[0]))
	{
		int _aint = atoi(_a);
		_alpha = _aint != 0;
	}

	tag.SetPrecision(_precision);
	tag.SetFormat(_format);
	tag.SetAlpha(_alpha);

	return(is);
}

