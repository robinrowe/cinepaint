/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Plugin Parameter List
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CPParamList.h,v 1.2 2006/12/18 08:21:21 robinrowe Exp $
 */

#ifndef _CP_PARAM_LIST_H_
#define _CP_PARAM_LIST_H_

/**
 *  defines the parameter list as used yb CinePaintPlugin,
 * UI Widgets, Spreadsheet and others.
**/

#include "dll_api.h"

#include <map>
#include <set>
#include <string>

class AbstractBuf;
class Drawable;
class CinePaintImage;


/**
 * Type def for delete dunction pointers,
 * returns true of delete was successful.
**/
typedef bool (*PFNVoidDeleteFunc)(void *);

/**
 * Simple struct to handle passing of user(plugin)-defined data in a memory safe way
 * pointer to block of data and function to delete it, this way the ParamList can
 * clean up any memory as needed.
**/
struct CPVoidPointer {
	void *data;
	PFNVoidDeleteFunc pfn_delete;
};

union CPPluginArg {
    int        pdb_int;           /*  Integer type  */
    double     pdb_float;		  /*  Floating point type  */
    char*      pdb_cstring;		  /*  C-Style char*  string  */
	AbstractBuf *pdb_abstractbuf; /*  Abstract Buffer for image data */
	Drawable *pdb_drawable;       /*  CinePaint Drawable Object */
	CinePaintImage *pdb_cpimage;  /* CinePaintImage Object */
	CPVoidPointer *pdb_voidpointer;	  /*  User defined void *  */
                                  // @TODO [claw] need to re-think this deleting a void* on clean up isn't a great idea
};

enum CPPluginArgType {
	PDB_INT,
	PDB_FLOAT,
	PDB_CSTRING,
	PDB_ABSTRACTBUF,
	PDB_DRAWABLE,
	PDB_CPIMAGE,
	PDB_VOIDPOINTER // @TODO [claw] need to re-think this deleting a void* on clean up isn't a great idea
};

/**
 * CPParamList provides the paramater passing mechanism for plugins.
 * A data parameter is added to this CPParamList, specifying the name of the parameter
 * and the data type using the AddParam and SetParam methods. Once added, the parameter
 * is owned by this CPParamList and will be released once this CPParamList is released.
 * The RemoveParam method may be used to get and remove a parameter from this CPParamList.
 * The RemoveParam caller assumes ownership of the parameter data and must release its
 * resources as appropriate once finished with the data. The GetParam variants provide
 * convenient access to the data held by this CPParamList, the data remains owned by this
 * CPParamList.
 &
 * For fundamental data types, a copy of the parameter is stored within this CPParamList.
 * For other types, a pointer to the object is stored, and this object will be destroyed when
 * this CPParamList is released.
 * char* data is not copied, but stored as-is and will be deleted once this CPParamList is released
 *
 * The ownership rules ensure that all data provided and created by a plugin is automatically
 * released if not claimed by someone interested.
 */
class CINEPAINT_CORE_INTERFACES_API CPParamList
{
public:

	CPParamList()
	{}

	/**
	 * Destructor
	 * released all managed parameter values.
	**/
	~CPParamList();

	/**
	 * Add a parameter to this CPParamList.
	 * The added parameter is owned by the CPParamList untill the parameter is removed,
	 * and will be deleted when this CPParamList is destroyed.
	 * The parameter name is copied by the ParamList, and can therefore be released after
	 * the paramter has been successfully added.
	 *
	 * @param name The name of the parameter - used as index in list.
	 * @param data The data for the parameter.
	 * @param type The type of the data.
	**/
	void AddParam(const char* name, CPPluginArg *arg, CPPluginArgType type);

	/**
	 * Set a parameter in this CPParamList, Adding a new one if the specified name does not exist.
	 * If existing the named parameter exists, but the types do NOT match, no further action
	 * is taken, the parameter is not set and this method returns false. If the parameter does
	 * exist and the types do match, the existing parameter is deleted and the new parameter 
	 * value set.
	 * the name of the parameter is copied by this ParamList.
	 *
	 * @param name The name of the parameter - used as index in list.
	 * @param data The data for the parameter.
	 * @param type The type of the data.
	 * @return true if success false otherwise.
	**/
	bool SetParam(const char* name, CPPluginArg *arg, CPPluginArgType type);

	/**
	 * Removes and returns the named parameter from this CPParamListvia the CPPluginArg arg
	 * The caller assumes responsibility for the returned resource and must
	 * release it as appropriate once finished.
	 *
	 * @param name The name of the parameter - used as index in list.
	 * @param arg set to the returned data values
	 * @param type The type of the data.
	 * @return The status of the call,  1 => found valid entry, 
	 *                                  0 => parameter name not found 
	 *                                 -1 => paramtere found but wrong type requested.
	**/
	int RemoveParam(const char* name, CPPluginArg& arg, CPPluginArgType type);

	/**
	 * Retrieves an integer parameter from this paramList
	 * For fundamental types, a copy of the parameter is made and returned, a copy
	 * remain within this CPParamList. For non fundamental types, a pointer to the
	 * object is returned, this CPParamList still owns and manages the data.
	 *
	 * @param name the name of the parameter required.
	 * @param val pointer to recieve the value of the parameter.
	 * @return The status of the call,  1 => found valid entry, 
	 *                                  0 => parameter name not found 
	 *                                 -1 => paramtere found but wrong type requested.
	**/
	int GetParam(const char* name, int *val);

	/**
	 * Retrieves a float parameter from this paramList
	 * For fundamental types, a copy of the parameter is made and returned, a copy
	 * remain within this CPParamList. For non fundamental types, a pointer to the
	 * object is returned, this CPParamList still owns and manages the data.
	 *
	 * @param name the name of the parameter required.
	 * @param val pointer to recieve the value of the parameter.
	 * @return The status of the call,  1 => found valid entry, 
	 *                                  0 => parameter name not found 
	 *                                 -1 => paramtere found but wrong type requested.
	**/
	int GetParam(const char* name, float *val);

	/**
	 * Retrieves a c-style string parameter from this paramList
	 * For fundamental types, a copy of the parameter is made and returned, a copy
	 * remain within this CPParamList. For non fundamental types, a pointer to the
	 * object is returned, this CPParamList still owns and manages the data.
	 *
	 * @param name the name of the parameter required.
	 * @param val pointer to recieve the value of the parameter.
	 * @return The status of the call,  1 => found valid entry, 
	 *                                  0 => parameter name not found 
	 *                                 -1 => paramtere found but wrong type requested.
	**/
	int GetParam(const char* name, char* *val);

	/**
	 * Retrieves a void* parameter from this paramList
	 * For fundamental types, a copy of the parameter is made and returned, a copy
	 * remain within this CPParamList. For non fundamental types, a pointer to the
	 * object is returned, this CPParamList still owns and manages the data.
	 *
	 * @param name the name of the parameter required.
	 * @param val pointer to recieve the value of the parameter.
	 * @return The status of the call,  1 => found valid entry, 
	 *                                  0 => parameter name not found 
	 *                                 -1 => paramtere found but wrong type requested.
	**/
	int GetParam(const char* name, void **val);

	/**
	 * Retrieves an AbstractBuf parameter from this paramList
	 * For fundamental types, a copy of the parameter is made and returned, a copy
	 * remain within this CPParamList. For non fundamental types, a pointer to the
	 * object is returned, this CPParamList still owns and manages the data.
	 *
	 * @param name the name of the parameter required.
	 * @param val pointer to recieve the value of the parameter.
	 * @return The status of the call,  1 => found valid entry, 
	 *                                  0 => parameter name not found 
	 *                                 -1 => paramtere found but wrong type requested.
	**/
	int GetParam(const char* name, AbstractBuf **val);

	/**
	 * Retrieves a Drawable parameter from this paramList
	 * For fundamental types, a copy of the parameter is made and returned, a copy
	 * remain within this CPParamList. For non fundamental types, a pointer to the
	 * object is returned, this CPParamList still owns and manages the data.
	 *
	 * @param name the name of the parameter required.
	 * @param val pointer to recieve the value of the parameter.
	 * @return The status of the call,  1 => found valid entry, 
	 *                                  0 => parameter name not found 
	 *                                 -1 => paramtere found but wrong type requested.
	 */
	int GetParam(const char* name, Drawable **val);

	/**
	 * Retrieves a CinePaintImage parameter from this paramList
	 * For fundamental types, a copy of the parameter is made and returned, a copy
	 * remain within this CPParamList. For non fundamental types, a pointer to the
	 * object is returned, this CPParamList still owns and manages the data.
	 *
	 * @param name the name of the parameter required.
	 * @param val pointer to recieve the value of the parameter.
	 * @return The status of the call,  1 => found valid entry, 
	 *                                  0 => parameter name not found 
	 *                                 -1 => paramtere found but wrong type requested.
	 */
	int GetParam(const char* name, CinePaintImage **val);

	/**
	 * Sets type to the parameter type of the specified parameter.
	 * If the specified parameter is held by this CPParamList, the type parameter
	 * is set to the parameter type of the specified parameter and true is returned,
	 * otherwise false is returned
	 *
	 * @param name the name of the parameter to determine toe type of
	 * @param type set to the type of the named parameter
	 * @return true if the parameter is found, false otherwise
	 */
	bool GetParamType(const char* name, CPPluginArgType& type) const;

	/**
	 * Populates the names set with the set of parameter names held by this CPParamList.
	 * The parameter names are not copied and remain owned by this CPParamList.
	 *
	 * @param names populated with the names of the parameters held by this CPParamList.
	 * @return the names parameter populated with parameter names
	 */
	std::set<std::string>& GetParamNames(std::set<std::string>& names) const;

private:

	struct CPParamDef
	{
		CPPluginArgType type;
		CPPluginArg		value;
		CPParamDef(CPPluginArgType _type, CPPluginArg _val) { type = _type, value = _val;};
	};

	typedef std::map<std::string, CPParamDef*> ParamList_t ;
	ParamList_t m_paramList;

};


#endif //_CP_PARAM_LIST_H_
