/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintNetJob - Client/Server execution module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintNetJob.cpp,v 1.3 2006/12/21 11:18:08 robinrowe Exp $
 */


#include "CinePaintNetJob.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include "plug-ins/pdb/Drawable.h"
#include "plug-ins/pdb/CinePaintPlugin.h"
#include "plug-ins/pdb/PDB.h"
//#include <pdb/CinePaintApp.h>
#include <sstream>

CinePaintNetJob::CinePaintNetJob()
{
}

CinePaintNetJob::CinePaintNetJob(  char* txt, AbstractBuf *buf)
{
	m_plugin = txt;
	m_buf = buf;
}

CinePaintNetJob::~CinePaintNetJob()
{
}


void CinePaintNetJob::Execute(World* world)
{
	printf("NetJob:Execute() - %s\n", m_plugin.c_str());
	char* name = new char[m_plugin.length()+1];
	strcpy(name, m_plugin.c_str());
	CinePaintPlugin *_p = 0;
	CinePaintPluginInfo *_pinfo = 0;
	PDB* pdb=PDB::GetPDB(world);
	if(pdb)
	{	_pinfo=pdb->LookupPlugin(name);
	}
	CPParamList _in;
	CPParamList _return;
	if ( _pinfo )
		_p = _pinfo->GetCtor()();
	if (_p)
	{
		// Create a temp drawable to use for plugin to work on.
		Drawable _drawable(m_buf->GetTag(), m_buf->GetWidth(), m_buf->GetHeight(), "TEMP");
		// Copy data into temp drawable buffer.
		unsigned char* dst = _drawable.GetBuffer().GetPortionData(0,0);
		unsigned char* src = m_buf->GetPortionData(0,0);
		for (int i=0; i<m_buf->GetWidth()*m_buf->GetHeight()*m_buf->GetTag().GetBytes(); i++)
		{*dst = *src; dst++; src++;}
		// Pass drawable and execute plugin.
		CPPluginArg arg;
		arg.pdb_drawable = &_drawable;
		_in.AddParam("drawable", &arg, PDB_DRAWABLE);
		_p->run(_in, _return);
		// Remove drawable from paramlist to avoid double deletion.
		_in.RemoveParam("drawable", arg, PDB_DRAWABLE);
		// Copy result back.
		dst = m_buf->GetPortionData(0,0);
		src = _drawable.GetBuffer().GetPortionData(0,0);
		for (int i=0; i<m_buf->GetWidth()*m_buf->GetHeight()*m_buf->GetTag().GetBytes(); i++)
		{*dst = *src; dst++; src++;}

	}
	delete name;
}

// Insert data into the socket. i.e Sending.
void CinePaintNetJob::Serialize(CPSocket &sock)
{
	sock << m_plugin;

	std::ostringstream _tag_str;
	_tag_str << m_buf->GetTag();
	sock << _tag_str.str();
	sock << (int)m_buf->GetWidth();
	sock << (int)m_buf->GetHeight();
	sock.send(m_buf->GetPortionData(0,0), m_buf->GetWidth()*m_buf->GetHeight()*m_buf->GetTag().GetBytes());
}

//Extract data from the socket.  i.e. Recieving.
void CinePaintNetJob::DeSerialize(CPSocket &sock)
{
	CinePaintTag _tag;
	int _width;
	int _height;
	std::istringstream _tag_str;
	std::string buf;
		
	sock >> m_plugin;
	sock >> buf;
	_tag_str.str(buf);
	sock >> _width;
	sock >> _height;

	_tag_str >> _tag;

	std::auto_ptr<AbstractBuf> dst_image = AbstractBufFactory::CreateBuf(_tag, _width, _height, AbstractBufFactory::STORAGE_FLAT);
	/* Take control of the memory allocated for the buffer */
	m_buf = dst_image.release();
	m_buf->RefPortionReadWrite(0,0);
	sock.recv(m_buf->GetPortionData(0,0),_width*_height*m_buf->GetTag().GetBytes());
}
