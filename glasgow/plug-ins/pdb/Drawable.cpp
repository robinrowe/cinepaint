/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Drawable - Main drawable object
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: Drawable.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "Drawable.h"
#include "AbstractBuf.h"
#include "DrawableChangeListener.h"
#include "FlatBuf.h"
#include "SelectionManager.h"

/**
 * Constructs a new Drawable as a compoent of the specified CinePaintImage.
 * The new Drawable is initially width by height in size.
 *
 */
Drawable::Drawable(const CinePaintTag& tag, int width, int height, const char* name)
{
	// @note [claw] initially all Drawable are backed by a FlatBuf, we can look at this
	//       again once the basic are in place and working to support tiles etc.
	m_canvas_data = new FlatBuf(tag, width, height);
	m_canvas_data->RefPortionReadWrite(0, 0);

	m_name = name ? std::string(name) : std::string("Unamed");

	m_visible = true;
	m_dirty = false;

	m_offset_x = 0;
	m_offset_y = 0;

	m_selection_manager = 0;
}

Drawable::Drawable( AbstractBuf& buf )
{
	m_canvas_data = &buf;
	m_name = std::string("Unamed");

	m_visible = true;
	m_dirty = false;

	m_offset_x = 0;
	m_offset_y = 0;

	m_selection_manager = 0;
}

/**
 * Constructs a new Drawable by copying the specified Drawable.
 * The new Drawable is the same size and has a CinePaintTag matching that of the specified
 * Drawable. The underlying image buffer is copied into a new buffer. The name of the Drawable
 * is also copied as is.
 * Currently any selections made upon the Drawable are NOT copied.
 *
 * @param drawable the Drawable to copy
 */
Drawable::Drawable(const Drawable& drawable)
{
	// see constructor note above
	m_canvas_data = new FlatBuf(drawable.GetBuffer().GetTag(), drawable.GetWidth(), drawable.GetHeight());
	m_canvas_data->RefPortionReadWrite(0, 0);

	// copy the existing data into our new buffer
	memcpy(m_canvas_data->GetPortionData(0, 0), drawable.GetBuffer().GetPortionData(0, 0), drawable.GetWidth() * drawable.GetHeight() * drawable.GetBuffer().GetTag().GetBytes());

	m_name = drawable.GetName();

	m_visible = drawable.GetVisible();
	m_dirty = drawable.m_dirty;

	m_offset_x = drawable.GetOffsetX();
	m_offset_y = drawable.GetOffsetY();


	// @TODO copy any selection made upon the Drawable.
	m_selection_manager = 0;
}

/**
 * Destructor
 */
Drawable::~Drawable()
{
	if(m_canvas_data)
	{
		delete m_canvas_data;
		m_canvas_data = 0;
	}

	if(m_selection_manager)
	{
		delete m_selection_manager;
		m_selection_manager = 0;
	}
}



/**
 * Requests that this Drawable is re-drawn on screen
 * This method delegates the call to the controlling CinePaintImage for handling
 * taking into account any offset into the 'overall image' this Drawable may have.
 * If is then the responsibility of registereed listeners to reposnd to the CinePaintImage
 * updated event.
 *
 * @param x the x coordinate of the update region
 * @param y the y coordinate of the update region
 * @param w the width of the update region
 * @param h the height of the update region
 */
void Drawable::Update(int x, int y, int w, int h)
{
	for(ListenerList_t::const_iterator _citer = m_drawable_listener_list.begin(); _citer != m_drawable_listener_list.end(); ++_citer)
	{
		(*_citer)->Updated(*this, x, y, w, h);
	}
}



//---------------------
// Accessors / Mutators

/**
 * Sets the name of this Drawable
 *
 * @param name the new name of this Drawable
 */
void Drawable::SetName(const char* name)
{
	m_name = std::string(name);
}

/**
 * Returns the name of this Drawable.
 * The returned data remains owned by thid Drawable
 *
 * @return the name of this Drawable
 */
const char* Drawable::GetName() const
{
	return(m_name.c_str());
}

/**
 * Sets the visible state of this Drawable within the Containing CinePaintImage
 *
 * @param visible set true to indicate this Drawable is visible, false to hide
 */
void Drawable::SetVisible(bool visible)
{
	m_visible = visible;
}

/**
 * Gets the visible state of this Drawable within the Containing CinePaintImage
 *
 * @return true if this Drawable is visble within the containeing CinePaintImage, false if hidden
 */
bool Drawable::GetVisible() const
{
	return(m_visible);
}

/**
 * Returns the X Offset of this Drawable within the containing CinePaintImage
 *
 * @return the x offset of thie Drawable
 */
int Drawable::GetOffsetX() const
{
	return(m_offset_x);
}

/**
 * Returns the Y Offset of this Drawable within the containing CinePaintImage
 *
 * @return the y offset of thie Drawable
 */
int Drawable::GetOffsetY() const
{
	return(m_offset_y);
}

/**
 * Sets the X Offset of this Drawable within the containing CinePaintImage
 *
 * @param x the x offset of this Drawable
 */
void Drawable::SetOffsetX(int x)
{
	m_offset_x = x;
}

/**
 * Sets the Y Offset of this Drawable within the containing CinePaintImage
 *
 * @param y the y offset of this Drawable
 */
void Drawable::SetOffsetY(int y)
{
	m_offset_y = y;
}

/**
 * Returns the SelectionManager managing selections over this Drawable
 *
 * @return the SelectionManager manaing selections over this Drawable
 */
SelectionManager& Drawable::GetSelectionManager()
{
	if(!m_selection_manager)
	{
		m_selection_manager = new SelectionManager(*this);
	}

	return(*m_selection_manager);
}

//--------------------
// Convenience Methods

/**
 * Return sthe width of this Drawable
 *
 * @return the width of this Drawable
 */
int Drawable::GetWidth() const
{
	return(m_canvas_data->GetWidth());
}

/**
 * Returns the height of this Drawable
 *
 * @return the height of this Drawable
 */
int Drawable::GetHeight() const
{
	return(m_canvas_data->GetHeight());
}

/**
 * Returns true if this Drawable contains an Alpha channel
 *
 * @return true if this Drawable has an alpha channel
 */
bool Drawable::HasAlpha() const
{
	return(m_canvas_data->GetTag().HasAlpha());
}


/**
 * Returns the AbstractBuf backing this Drawable
 *
 * @return the AbstractBuf backing this AbstractBuf
 */
AbstractBuf& Drawable::GetBuffer()
{
	// we should always have a buffer!
	return(*m_canvas_data);
}

/**
 * Returns a const reference to the AbstractBuf backing this Drawable
 *
 * @return the AbstractBuf backing this AbstractBuf
 */
const AbstractBuf& Drawable::GetBuffer() const
{
	return(*m_canvas_data);
}


//-------------------
// Listener handling

/**
 * Registers the specified DrawableChangeListener to receive Drawable change notifications.
 *
 * @param dcl the DrawableChangeListener to add
 */
void Drawable::AddDrawableChangeListener(DrawableChangeListener* dcl)
{
	m_drawable_listener_list.AddListener(dcl);
}

/**
 * Removes the specified DrawableChangeListener from those registered as receiving Drawable change notifications
 *
 * @param dcl the DrawableChangeListener to remove
 */
void Drawable::RemoveDrawableChangeListener(DrawableChangeListener* dcl)
{
	m_drawable_listener_list.RemoveListener(dcl);
}

