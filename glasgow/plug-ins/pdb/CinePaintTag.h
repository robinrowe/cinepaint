/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintTag - Image Module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintTag.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _CINEPAINT_TAG_H_
#define _CINEPAINT_TAG_H_

#include "dll_api.h"

#include <iostream>
#include <string>

/**
 *
 */
class CINEPAINT_IMAGE_API CinePaintTag
{
	public:
		/* supported precisions */
		enum Precision
		{
		    PRECISION_NONE_ENUM = 0,
		    PRECISION_U8_ENUM = 1,
		    PRECISION_U16_ENUM = 2,
		    PRECISION_FLOAT_ENUM = 3,
		    PRECISION_FLOAT16_ENUM = 4,
		    PRECISION_BFP_ENUM = 5
		};

		/* the supported formats */
		enum Format
		{
		    FORMAT_NONE_ENUM = 0,
		    FORMAT_RGB_ENUM = 1,
		    FORMAT_GRAY_ENUM = 2,
		    FORMAT_INDEXED_ENUM = 3
		};

		/**
		 * Constructs an intially null CinePaintTag
		 * A null as a precision of PRECISION_NONE_ENUM and format FORMAT_NONE_ENUM
		 *
		 */
		CinePaintTag();

		/**
		 * Constructs a new CinePaintTag with the specified precision, format and alpha value
		 *
		 * @param precision the data Precision represented by this CinePaintTag
		 * @param format the data format represented by this CinePaintTag
		 * @param alpha set true if this CinePaintTag represents data with an alpha channel
		 */
		CinePaintTag(Precision precision, Format format, bool alpha);

		/**
		 * Destructor
		 */
		~CinePaintTag();



		/**
		 * Returns the Precision of the data this CinePaintTag represents
		 *
		 * @return the Precision value of this CinePaintTag
		 */
		Precision GetPrecision() const;

		/**
		 * Returns the Format of the data this CinePaintTag represents
		 * 
		 * @return the Format value of this CinePaintTag
		 */
		Format GetFormat() const;

		/**
		 * Returns true if this CinePaintTag represents a data format with a data channel
		 *
		 * @return true if this CinePaintTag represents data with an alpha channel, false otherwise
		 */
		bool HasAlpha() const;

		/**
		 * Sets the Presicion of this CinePaintTag to the specified Precision value
		 *
		 * @param precision the new Precision value of this CinePaintTag
		 */
		void SetPrecision(Precision precision);

		/**
		 * Sets the Format of this CinePaintTag to the specified Format value
		 *
		 * @param format the new Format value of this CinePaintTag
		 */
		void SetFormat(Format format);

		/**
		 * Sets whether this CinePaintTag represents an alpha channel
		 *
		 * @param alpha set true to indicate an alpha channel is present, false otherwise
		 */
		void SetAlpha(bool alpha);

		/**
		 * Returns the number of channels represented by this CinePaintTag
		 *
		 * @return the number of channels represented by this CinePaintTag
		 */
		int GetNumChannels() const;

		/**
		 * Returns the number of bytes per pixel for the data represented y this CinePaintTag
		 * The number of bytes is calculated as;
		 * (number of channels + optional alpha channel) x sizeof data type
		 *
		 * @return the number of bytes per pixel
		 */
		int GetBytes() const;

		/**
		 * Returns true if the specified CinePaintTag is equal to this CinePaintTag
		 * Two CinePaintTags are equal of their Precision, Format and alpha values are equal
		 *
		 * @param tag the CinePaintTag to compare to this
		 * @return trus if this CinePaintTag is equal to the specufued CinePaintTag, false otherwise
		 */
		bool Equals(const CinePaintTag& tag) const;

		/**
		 * Returns true if the specified CinePaintTags are equal
		 * Two CinePaintTags are equal of their Precision, Format and alpha values are equal
		 *
		 * @param first the first CinePaintTag to compare
		 * @param second the second CinePaintTag to compare
		 * @return true if first equals second, false otherwise
		 */
		static bool Equals(const CinePaintTag& first, const CinePaintTag& second);

		/**
		 * Returns true if this CinePaintTag is valid.
		 * Certain combinations of Precision and Format are illegal, in these cases
		 * this method returns false. For example, the Precision may not be a float data
		 * type yet the format indexed.
		 *
		 * @return true if this CinePaintTag is valid, false otherwise
		 */
		bool IsValid() const;

		/**
		 * Returns true if this CinePaintTag is a null CinePaintTag.
		 * A null tag is a CinePaintTag has a Precision type of PRECISION_NONE_ENUM
		 * and a Format of FORMAT_NONE_ENUM
		 *
		 * @return true if this CinePaintTag is null
		 */
		bool IsNull() const;


		//--------------------
		// Convenience Methods

		/**
		 * Returns a string representation of the specified Precision value
		 *
		 * @param precision the Precision value to represent as a string
		 * @return string representation of the specified Precision
		 */
		static std::string GetPrecisionString(Precision precision);

		/**
		 * Returns a string representation of the specified Format value
		 *
		 * @param format the Format value to represent as a string
		 * @return string representation of the specified Format
		 */
		static std::string GetFormatString(Format format);


		//---------------------
		// Operator Overloading

		/**
		 * Returns true if the specified CinePaintTag is equal to this CinePaintTag
		 * Two CinePaintTags are equal of their Precision, Format and alpha values are equal
		 *
		 * @param tag the CinePaintTag to compare to this
		 * @return trus if this CinePaintTag is equal to the specufued CinePaintTag, false otherwise
		 */
		bool operator == (const CinePaintTag& tag) const;

		/**
		 * Outputs the serialized form of this CinePaintTag to the specified stream
		 *
		 * @param os the stream to write to
		 * @param tag the CinePaintTag to write to the stream
		 * @return the stream parameter
		 */
		CINEPAINT_IMAGE_API friend std::ostream& operator<< (std::ostream& os, const CinePaintTag& tag);

		/**
		 * Reads the serialized form of a CinePaintTag from the specified stream into the specified CinePaintTag
		 * 
		 * @param os the stream to read from
		 * @param tag the CinePaintTag to deserialize into
		 * @return the stream parameter
		 */
		CINEPAINT_IMAGE_API friend std::istream& operator>> (std::istream& os, CinePaintTag& tag);

		//------------
		// Static data

		// Defined string representations of precisions and formats
		static const std::string PRECISION_U8_STRING;
		static const std::string PRECISION_U16_STRING;
		static const std::string PRECISION_FLOAT16_STRING;
		static const std::string PRECISION_FLOAT_STRING;
		static const std::string PRECISION_BFP_STRING;

		static const std::string FORMAT_RGB_STRING;
		static const std::string FORMAT_GRAY_STRING;
		static const std::string FORMAT_INDEXED_STRING;

	protected:

	private:
		/** precision of the data this tag represents */
		Precision m_precision;

		/** format of data this tag represents */
		Format m_format;

		/** indicates is this tag represents data with an aplha channel */
		bool m_alpha;

}
; /* #include "plug-ins/pdb/CinePaintTag.h" */

#endif /* _CINEPAINT_TAG_H_ */
