/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Drawable - Main Drawable Object 
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: Drawable.h,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#ifndef _CINEPAINT_DRAWABLE_H_
#define _CINEPAINT_DRAWABLE_H_

#include "dll_api.h"
#include "ListenerList.h"
#include <string>

class AbstractBuf;
#include "plug-ins/pdb/CinePaintTag.h"
class DrawableChangeListener;
class SelectionManager;

class CINEPAINT_IMAGE_API Drawable
{
	public:
		/**
		 * Constructs a new Drawable as a compoent of the specified CinePaintImage.
		 * The new Drawable is initially width by height in size.
		 * All Drawable are associated with a CinePaintImage.
		 *
		 * @param tag CinePaintTag describing the data format
		 * @param width the width of this Drawable
		 * @param height the height of this Drawable
		 * @param name the name of this Drawable
		 */
		Drawable(const CinePaintTag& tag, int width, int height, const char* name);

		/**
		 *
		 *  Construct a new drawable from an AbstractBuf.
		 *
		 * @param buf Abstract Buf to use for construction.
		**/
		Drawable( AbstractBuf& buf );

		/**
		 * Constructs a new Drawable by copying the specified Drawable.
		 * The new Drawable is the same size and has a CinePaintTag matching that of the specified
		 * Drawable. The underlying image buffer is copied into a new buffer. The name of the Drawable
		 * is also copied as is.
		 * Currently any selections made upon the Drawable are NOT copied.
		 *
		 * @param drawable the Drawable to copy
		 */
		Drawable(const Drawable& drawable);

		/**
		 * Destructor - destroys this Drawable and the underlying image data buffer
		 */
		virtual ~Drawable();

		/**
		 * Requests that this Drawable is re-drawn on screen
		 * This method delegates the call to the controlling CinePaintImage for handling
		 * taking into account any offset into the 'overall image' this Drawable may have.
		 * If is then the responsibility of registereed listeners to reposnd to the CinePaintImage
		 * updated event.
		 *
		 * @param x the x coordinate of the update region
		 * @param y the y coordinate of the update region
		 * @param w the width of the update region
		 * @param h the height of the update region
		 */
		void Update(int x, int y, int w, int h);



		//---------------------
		// Accessors / Mutators

		/**
		 * Sets the name of this Drawable
		 *
		 * @param name the new name of this Drawable
		 */
		void SetName(const char* name);

		/**
		 * Returns the name of this Drawable.
		 * The returned data remains owned by thid Drawable
		 *
		 * @return the name of this Drawable
		 */
		const char* GetName() const;

		/**
		 * Sets the visible state of this Drawable within the Containing CinePaintImage
		 *
		 * @param visible set true to indicate this Drawable is visible, false to hide
		 */
		void SetVisible(bool visible);

		/**
		 * Gets the visible state of this Drawable within the Containing CinePaintImage
		 *
		 * @return true if this Drawable is visble within the containeing CinePaintImage, false if hidden
		 */
		bool GetVisible() const;

		/**
		 * Returns the X Offset of this Drawable within the containing CinePaintImage
		 *
		 * @return the x offset of this Drawable
		 */
		int GetOffsetX() const;

		/**
		 * Returns the Y Offset of this Drawable within the containing CinePaintImage
		 *
		 * @return the y offset of this Drawable
		 */
		int GetOffsetY() const;

		/**
		 * Sets the X Offset of this Drawable within the containing CinePaintImage
		 *
		 * @param x the x offset of this Drawable
		 */
		void SetOffsetX(int x);

		/**
		 * Sets the Y Offset of this Drawable within the containing CinePaintImage
		 *
		 * @param y the y offset of this Drawable
		 */
		void SetOffsetY(int y);


		/**
		 * Returns the SelectionManager managing selections over this Drawable
		 *
		 * @return the SelectionManager manaing selections over this Drawable
		 */
		SelectionManager& GetSelectionManager();

		//--------------------
		// Convenience Methods

		/**
		 * Return sthe width of this Drawable
		 *
		 * @return the width of this Drawable
		 */
		int GetWidth() const;

		/**
		 * Returns the height of this Drawable
		 *
		 * @return the height of this Drawable
		 */
		int GetHeight() const;

		/**
		 * Returns true if this Drawable contains an Alpha channel
		 *
		 * @return true if this Drawable has an alpha channel
		 */
		bool HasAlpha() const;

		/**
		 * Returns the AbstractBuf backing this Drawable
		 *
		 * @return the AbstractBuf backing this AbstractBuf
		 */
		AbstractBuf& GetBuffer();

		/**
		 * Returns a const reference to the AbstractBuf backing this Drawable
		 *
		 * @return the AbstractBuf backing this AbstractBuf
		 */
		const AbstractBuf& GetBuffer() const;



		//-------------------
		// Listener handling

		/**
		 * Registers the specified DrawableChangeListener to receive Drawable change notifications.
		 *
		 * @param dcl the DrawableChangeListener to add
		 */
		void AddDrawableChangeListener(DrawableChangeListener* dcl);

		/**
		 * Removes the specified DrawableChangeListener from those registered as receiving Drawable change notifications
		 *
		 * @param dcl the DrawableChangeListener to remove
		 */
		void RemoveDrawableChangeListener(DrawableChangeListener* dcl);

	protected:

	private:

		/** name of drawable */
		std::string m_name;

		/** drawable image data */
		AbstractBuf* m_canvas_data;

		/** controls visibility of this Drawable*/
        bool m_visible;

		/** dirty flag */
        bool m_dirty;

		/** offset of layer in image */
        int m_offset_x, m_offset_y;

		/** selection manager for this Drawable */
		SelectionManager* m_selection_manager;


		//------------------
		// Listener handling

		typedef ListenerList<DrawableChangeListener> ListenerList_t;

		/** list of registered ImageChangeListener to be notified on Image changes */
		ListenerList_t m_drawable_listener_list;

}; /* class Drawable */

#endif /* _CINEPAINT_DRAWABLE_H_ */
