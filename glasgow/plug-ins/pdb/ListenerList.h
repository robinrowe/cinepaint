/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      Generic Listener list module
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: ListenerList.h,v 1.4 2006/12/17 07:28:54 robinrowe Exp $
 */

#ifndef _CINEPAINT_LISTENER_LIST_H_
#define _CINEPAINT_LISTENER_LIST_H_

#include "dll_api.h"

#include <algorithm>
#include <list>
#include <cassert>

class CinePaintProgressListener;
class CinePaintDialogListener;

/**
 * A simple list storing pointers to regietered Listener interfaces.
 * ListenerList is intended to manages a simple list of pointers to Listener interfaces.
 * within an event/listener model.
 * A Listener may be added to the ListenerList only once, a second attempt to add the listener
 * will silently fail.
 * The ListenerList does not manage the resources of any registered Listener, it maintains only
 * a list of pointers to registered listeners. Upon removing a Listener, the removed item is returned.
 */
template <class T>
class ListenerList
{
	private:
		typedef typename std::list<T*> ListenerList_t;

		/** the managed listener list */
		ListenerList_t m_listener_list;	

	public:

		/**
		 * Constructs an emtpy ListenerList
		 */
		ListenerList() {} ;

		/**
		 * Destructor - nothing to do
		 */
		~ListenerList() {} ;


		/**
		 * Adds the specified item to the Listener list
		 * If the specified listener is already registered within this ListenerList,
		 * it is not added a second time. In this case this method silently fails.
		 *
		 * @param listener the listener item to add
		 */
		void AddListener(T* listener);

		/**
		 * Removes and returns the specified item from the Listener list
		 * If the specified listener is not registered with this ListenerList, 0 is returned
		 *
		 * @param listener the listener item to remove
		 * @return the removed listener or 0 if the specified listener is not registered with this ListenerList
		 */
		T* RemoveListener(T* listener);

		/** public typedef of a constant iterator */
		typedef typename ListenerList_t::const_iterator const_iterator;

		/**
		 * Returns a constant iterator pointing to the first element in this LisenterList
		 *
		 * @return a constant iterator pointing to the first element in this LisenterList
		 */
		const_iterator begin() { return(m_listener_list.begin()); }

		/**
		 * Returns a constant iterator pointing pastr the end of this LisenterList
		 *
		 * @return a constant iterator pointing pastr the end of this LisenterList
		 */
		const_iterator end() { return(m_listener_list.end()); }

		/**
		 *  @return True if the listener list is empty.
		 */
		bool empty(){ return m_listener_list.empty(); }
		/**
		 * Removes and returns the first element of the listener list.
		 * @return The first element.
		 */
		T* pop_front(){ T* f = m_listener_list.front(); m_listener_list.pop_front(); return(f); }

	static ListenerList<CinePaintProgressListener>* GetInitProgressListenerList()
	{	static ListenerList<CinePaintProgressListener>* singleton;
		if(!singleton)
		{	singleton=new ListenerList<CinePaintProgressListener>;
			if(!singleton) return 0;
		}
		assert(singleton);
		return singleton;
	}
	static ListenerList<CinePaintProgressListener>* GetProgressListenerList()
	{	static ListenerList<CinePaintProgressListener>* singleton;
		if(!singleton)
		{	singleton=new ListenerList<CinePaintProgressListener>;
		}
		return singleton;
	}
	static ListenerList<CinePaintDialogListener>* GetDialogListenerList()
	{	static ListenerList<CinePaintDialogListener>* singleton;
		if(!singleton)
		{	singleton=new ListenerList<CinePaintDialogListener>;
		}
		return singleton;
	}
	
	protected:

	private:

}; /* class ListenerList */


// implementation

/**
 * Adds the specified CanvasMouseListener to receive mouse events from this CinePaintCanvas
 * if the specified CanvasMouseListener has already been added it is not added again.
 * The resources of the specified CanvasMouseListener are not managed by this class
 * It is safe to assume that generated events occur within the UI event loop.
 *
 * @param cml the CanvasMouseListener to be added
 */
template <class T>
void ListenerList<T>::AddListener(T* listener)
{
	if(std::find(m_listener_list.begin(), m_listener_list.end(), listener) == m_listener_list.end())
	{
		m_listener_list.push_back(listener);
	}
}

/**
 * Removes the specified CanvasMouseListener.
 * The removed CanvasMouseListener will no longer receive mouse events from this CinePaintCanvas
 * It is safe to assume that generated events occur within the UI event loop.
 *
 * @param cml the CanvasMouseListener to be removed
 */
template <class T>
T* ListenerList<T>::RemoveListener(T* listener)
{
	T* _ret = 0;

	typename ListenerList_t::iterator _iter = std::find(m_listener_list.begin(), m_listener_list.end(), listener);

	if(_iter != m_listener_list.end())
	{
		_ret = *_iter;
		m_listener_list.erase(_iter);
	}

	return(_ret);
}

#endif /* _CINEPAINT_LISTENER_LIST_H_ */
