/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      LayerManager - Manages layers
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: LayerManager.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "LayerManager.h"
#include "CinePaintImage.h"
#include "Layer.h"
#include "LayerChangeListener.h"
#include <algorithm>
#include <stdio.h>

/**
 * Destructor - destroys all managed Layers
 */
LayerManager::~LayerManager()
{
	while(!m_layers.empty())
	{
		Layer* _layer = m_layers.front();
		m_layers.pop_front();

		delete _layer;
		_layer = 0;
	}
}


//-------------------------
// Layer Addition / Removal

/**
 * Adds the secified layer to this LayerManager at the specified position
 * Once added, the Layermanager assumes responsibility for the Layer until the layer is removed.
 * If the layer is already managed by this Layermanager, the Layer is not added a second time and
 * false is returned, otherwise the Layer is added and true is returned.
 *
 * @param layer the Layer to add
 * @param position the position within the layer stack to add the layer (0 is foreground)
 * @return true if the layer is added, false otherwise
 */
bool LayerManager::AddLayer(Layer* layer, unsigned int position, bool active)
{
	bool _ret = false;

	if(!has_layer(layer))
	{
		// find the right place in the stack
		unsigned int _pos = 0;
		LayerStack_t::iterator _iter = m_layers.begin();
		while((_iter != m_layers.end()) && (_pos < position))
		{
			++_iter;
			++_pos;
		}

		m_layers.insert(_iter, layer);

		// add the layers to our Most Recently used List - initially at the back
		// @note [claw] this is where the onionskin stuff belongs if kept - see gimage.c
		m_layers_mru.push_back(layer);


		// is the new Layer the active layer?
		if(active)
		{
			SetActiveLayer(layer);
		}

		for(ListenerList_t::const_iterator _citer = m_layer_listener_list.begin(); _citer != m_layer_listener_list.end(); ++_citer)
		{
			(*_citer)->LayerAdded(*layer, position);
		}

		// update the new layer's area
		layer->Update(0, 0, layer->GetWidth(), layer->GetHeight());
	}

	return(_ret);
}

/**
 * Adds the specified Layer to this Layermanager as the foreground layer
 * Once added, this LayerManager assumes responsibility for the Layer until the layer is removed.
 * If the layer is already managed by this Layermanager, the Layer is not added a second time and
 * false is returned, otherwise the Layer is added and true is returned.
 *
 * @param layer the Layer to be added
 * @param active set true to set the newly added layer as the currently active layer
 * @return true if the layer was added successfully, false otherwise
 */
bool LayerManager::AddLayerForeground(Layer* layer, bool active)
{
	bool _ret = false;

	if(!has_layer(layer))
	{
		m_layers.push_front(layer);

		m_layers_mru.push_back(layer);

		if(active)
		{
			SetActiveLayer(layer);
		}

		for(ListenerList_t::const_iterator _citer = m_layer_listener_list.begin(); _citer != m_layer_listener_list.end(); ++_citer)
		{
			(*_citer)->LayerAdded(*layer, 0);
		}

		layer->Update(0, 0, layer->GetWidth(), layer->GetHeight());
	}

	return(_ret);
}

/**
 * Adds the specified Layer to this Layermanager as the background layer
 * Once added, this LayerManager assumes responsibility for the Layer until the layer is removed.
 * If the layer is already managed by this Layermanager, the Layer is not added a second time and
 * false is returned, otherwise the Layer is added and true is returned.
 *
 * @param layer the Layer to be added
 * @param active set true to set the newly added layer as the currently active layer
 * @return true if the layer was added successfully, false otherwise
 */
bool LayerManager::AddLayerBackground(Layer* layer, bool active)
{
	bool _ret = false;

	if(!has_layer(layer))
	{
		m_layers.push_back(layer);

		m_layers_mru.push_back(layer);

		if(active)
		{
			SetActiveLayer(layer);
		}

		for(ListenerList_t::const_iterator _citer = m_layer_listener_list.begin(); _citer != m_layer_listener_list.end(); ++_citer)
		{
			(*_citer)->LayerAdded(*layer, int(m_layers.size()-1));
		}

		layer->Update(0, 0, layer->GetWidth(), layer->GetHeight());
	}

	return(_ret);
}

/**
 * Removes and returns the specified Layer from thie LayerManager
 * Resopnsibility for the removed Layer passes to the caller.
 * If the Specified Layer is not managed by this LayerManager, no further action is taken
 * and 0 is returned
 *
 * @param layer the Layer to be removed
 * @return the removed Layer or 0 if the specified Layer is not managed by this LayerManager
 */
Layer* LayerManager::RemoveLayer(Layer* layer)
{
	Layer* _ret = 0;

	LayerStack_t::iterator _iter = std::find(m_layers.begin(), m_layers.end(), layer);

	if(_iter != m_layers.end())
	{
		_ret = layer;
		m_layers.erase(_iter);

		// remove the layer from the Most Recently used List
		m_layers_mru.remove(layer);

		// let all registered listener know about the change
		for(ListenerList_t::const_iterator _citer = m_layer_listener_list.begin(); _citer != m_layer_listener_list.end(); ++_citer)
		{
			(*_citer)->LayerRemoved(*layer);
		}
	}
	return(_ret);
}


//-------------
// Layer Access

/**
 * Returns the currently active layer
 *
 * @return the currently active layer
 */
Layer* LayerManager::GetActiveLayer() const
{
	return(m_active_layer);
}

/**
 * Sets the currently active layer to the specified Layer
 * If the specified layer is not managed by this layerManager, the currently
 * active Layer is set to 0, and false is returned.
 *
 * @param layer the Layer to set as the Active Layer
 * @return false if the layer is not managed by this LayerManager, true otherwise
 */
bool LayerManager::SetActiveLayer(Layer* layer)
{
	bool _ret = false;

	if(has_layer(layer))
	{
		m_active_layer = layer;

		// set the active layer as the most recently used layer
		set_mru(layer);

		_ret = true;
	}

	return(_ret);
}

/**
 * Returns the layer at the top of the Layer stack, the foreground
 *
 * @return the layer at the top of the layer stack
 */
Layer* LayerManager::GetForegroundLayer() const
{
	Layer* _layer = 0;

	if(!m_layers.empty())
	{
		_layer = m_layers.front();
	}

	return(_layer);
}

/**
 * Returns the Layer at the bottom of the Layer stack, the background
 *
 * @return the background layer
 */
Layer* LayerManager::GetBackgroundLayer() const
{
	Layer* _layer = 0;

	if(!m_layers.empty())
	{
		_layer = m_layers.back();
	}

	return(_layer);
}

/**
 * Returns a Layer by its index into the Layer stack
 *
 * @param position the index into the Layer stack
 * @return the Layer at the specified postion, or 0 if no Layer resides at the specified position
 */
Layer* LayerManager::GetLayerByIndex(unsigned int index) const
{
	Layer* _layer = 0;

	if(index < GetLayerCount())
	{
		LayerStack_t::const_iterator _citer = m_layers.begin();

		while(index-- > 0)
		{
			_citer++;
		}

		_layer = *_citer;
	}

	return(_layer);
}



//---------------
// Layer Iterator

/**
 * Returns a constant iterator at the beginning of the managed Layers within this LayerManager
 *
 * @return a constant iterator at the beginning of this LayerManagers managed Layers
 */
LayerManager::const_iterator LayerManager::begin() const
{
	return(m_layers.begin());
}

/**
 * Returns a constant iterator past the end of this LayerManagers managed Layers
 *
 * @return a constant iterator past the end of this LayerManagers managed Layers
 */
LayerManager::const_iterator LayerManager::end() const
{
	return(m_layers.end());
}




//------------------
// layer Positioning

/**
 * Determines the position within the layer stack at which the specified Layer resides
 * If the specified layer is not managed by this layerManager, this method will return false
 * and position is unchanged, otherwise the position is set within the reference parameter
 * position and this method returns true.
 *
 * @param the Layer to determine the position of
 * @param position set to the position within the layer stack of the specified layer
 * @return true if the Layer is managed by this Layermanager, false otherwiae
 */
bool LayerManager::GetLayerIndex(const Layer* layer, unsigned int& position) const
{
	bool _ret = false;
	unsigned int _index = 0;

	for(LayerStack_t::const_iterator citer = m_layers.begin(); citer != m_layers.end(); ++citer)
	{
		if(*citer == layer)
		{
			position = _index;
			_ret = true;

			// found the right element, so break from the for loop
			break;
		}
		_index++;
	}

	return(_ret);
}
		
/**
 * Raises the specified layer within the Layer stack by one place
 *
 * @param layer the Layer to raise
 */
void LayerManager::RaiseLayer(Layer* layer)
{
	// get the index of the specified layer
	unsigned int _index;
	
	// we must manage the layer...
	if(GetLayerIndex(layer, _index))
	{
		// ... and we cant raise the top layer
		if(_index > 0)
		{
			LayerStack_t::iterator _to_raise = std::find(m_layers.begin(), m_layers.end(), layer);
			LayerStack_t::iterator _layer_up = _to_raise;

			// move back up the stack one place
			_layer_up--;

			// We can only raise a layer if it has an alpha channel
			if((*_to_raise)->HasAlpha() && (*_layer_up)->HasAlpha())
			{
				std::swap(*_to_raise, *_layer_up);

				// @TODO calculate minimum area to update - see gimage.c
			}
			else
			{
				printf("Layer cannot be raised any further");
		    }
		}
		else // if(_index == 0)
		{
			printf("Layer cannot be raised because it is already the top Layer");
		}
	}
	else
	{
		printf("Layer cannot be raised because because it is not a managed Layer");
	}
}

/**
 * Lowers the specified Layer within the Layer stack by one place
 *
 * @param layer the Layer to lower
 */
void LayerManager::LowerLayer(Layer* layer)
{
	// get the index of the specified layer
	unsigned int _index;
	
	// we must manage the layer...
	if(GetLayerIndex(layer, _index))
	{
		// ... and we cant lower the background layer
		if(_index < (GetLayerCount() -1))
		{
			LayerStack_t::iterator _to_raise = std::find(m_layers.begin(), m_layers.end(), layer);
			LayerStack_t::iterator _layer_down = _to_raise;

			// move back down through the stack one place
			_layer_down++;

			// We can only lower a layer if it has an alpha channel and the layer beneath it has an alpha channel
			if((*_to_raise)->HasAlpha() && (*_layer_down)->HasAlpha())
			{
				std::swap(*_to_raise, *_layer_down);

				// @TODO calculate minimum area to update - see gimage.c
			}
			else
			{
				printf("Layer cannot be lowered any further");
			}
		}
		else// if(_index == 0)
		{
			printf("Layer cannot be lowered because it is already the bottom Layer");
		}
	}
	else
	{
		printf("Layer cannot be lowered because because it is not a managed Layer");
	}
}


/**
 * Returns the number of layers within this LayerManager
 *
 * @return the number of layers within this LayerManager
 */
size_t LayerManager::GetLayerCount() const
{
	return(m_layers.size());
}





//-------------------
// Listener handling

/**
 * Registers the specified LayerChangeListener to receive Layer change notifications.
 *
 * @param lcl the DrawableChangeListener to add
 */
void LayerManager::AddLayerChangeListener(LayerChangeListener* lcl)
{
	m_layer_listener_list.AddListener(lcl);
}

/**
 * Removes the specified LayerChangeListener from those registered as receiving Layer change notifications
 *
 * @param lcl the LayerChangeListener to remove
 */
void LayerManager::RemoveLayerChangeListener(LayerChangeListener* lcl)
{
	m_layer_listener_list.RemoveListener(lcl);
}





/**
 * Returns true if this LayerManager manages the specified Layer
 *
 * @param layer the layer to search for
 * @return true if this LayerManager manages the specified Layer, false otherwise
 */
bool LayerManager::has_layer(const Layer* layer) const
{
	bool _ret = false;

	if(std::find(m_layers.begin(), m_layers.end(), layer) != m_layers.end())
	{
		_ret = true;
	}

	return(_ret);
}

/**
 * Updates the most recently used layer stack, moving the specified Layer to the top
 *
 * @param layer the most recently used layer
 */
void LayerManager::set_mru(Layer* layer)
{
	// we manage the Layer, right?
	LayerStack_t::iterator _iter = std::find(m_layers_mru.begin(), m_layers_mru.end(), layer);

	if(_iter != m_layers_mru.end())
	{
		std::iter_swap(_iter, m_layers_mru.begin());
	}
}
