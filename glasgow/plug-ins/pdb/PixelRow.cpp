/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      PixelRow - A row of pixels.
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * Spencer Kimball and Peter Mattis
 * Modified for CinePaint by Colin Law 2004/08/26
 *
 * $Id: PixelRow.cpp,v 1.1 2006/01/04 20:38:22 robinrowe Exp $
 */

#include "PixelRow.h"
#include "CinePaintTag.h"
#include <memory>

/**
 * Constructs a new PixelRow representing a consecutive row of data
 * A new data buffer is allocated as specified by tag of size width. This buffer
 * is automatically released when the PixelRow is destroyed.
 *
 * @param tag the CinePaintTag indicating the buffer type
 * @param width the width of this PixelRow
 */
PixelRow::PixelRow(const CinePaintTag& tag, unsigned int width)
{
	m_tag = tag;
	m_width = width;
	m_data = new unsigned char[m_tag.GetBytes() * m_width] ;
}

/**
 * Desctructor
 */
PixelRow::~PixelRow()
{
	delete[] m_data;
}

/**
 * Returns the CinePaintTag of the Backing AbstractBuf
 *
 * @return the CinePaintTag of the backing AbstractBuf
 */
const CinePaintTag& PixelRow::GetTag() const
{
	return(m_tag);
}

CinePaintTag& PixelRow::GetTag()
{
	return(m_tag);
}

/**
 * Returns the width of this PixelRow
 *
 * @return the width of this PixelRow
 */
unsigned int PixelRow::GetWidth() const
{
	return(m_width);
}

/**
 * Returns direct access to the row of pixel data within the backing AbstractBuf this PiexlRow represents
 *
 * @return direct access to the pixel data
 */
unsigned char* PixelRow::GetData()
{
	return(m_data);
}

/**
 * Copies image data contained within data to this PixelRow.
 * The number of bytes copies is determined by the width of this PixelRow and the CinePaintTag
 * indicating image data type this PixelRow holds.
 * 
 * @todo need to do some kind of bounds checking here
 * @param data the image data to copy into this PixelRow
 */
void PixelRow::SetData(unsigned char* data)
{
	this->SetData(data, m_width);
}

/**
 * Copies image data contained within data to this PixelRow for the specified number of pixels.
 * If pixels is greater than GetWidth(), pixels is limited to GetWidth().
 * The actual number of bytes copied is determined by the specified number of pixels and the CinePaintTag
 * indicating the type of image data this PixelRow contains
 *
 * @param data the image data to copy into this PixelRow
 * @param pixels the number of pixels of data to copy
 */
void PixelRow::SetData(unsigned char* data, unsigned int pixels)
{
	if(pixels > m_width)
	{
		pixels = m_width;
	}

	memcpy(m_data, data, pixels * m_tag.GetBytes());
}
