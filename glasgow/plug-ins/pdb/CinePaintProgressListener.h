/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is CinePaint, an image sequence manipulation program
 *                      CinePaintProgressListener - Abstract progress update listener
 *
 * The Initial Developer of the Original Code is
 * the University of Glasgow.
 * Portions created by the Initial Developer are Copyright (C) 2004
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Donald MacVicar
 *   Colin Law
 *   Stuart Ford
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"),
 * in which case the provisions of the GPL are applicable instead of those
 * above. If you wish to allow use of your version of this file only under
 * the terms of the GPL, and not to allow others to use your version of this
 * file under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other provisions
 * required by the GPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of any one of the MPL,
 * or the GPL.
 *
 * ***** END LICENSE BLOCK *****
 *
 * $Id: CinePaintProgressListener.h,v 1.2 2006/12/18 08:21:21 robinrowe Exp $
 */

#ifndef _CINEPAINT_PROGRESS_LISTENER_H_
#define _CINEPAINT_PROGRESS_LISTENER_H_

#include "dll_api.h"

/**
 * CinePaintProgressListener provides an abstract interface for a listener for progress 
 * display applications, this is used for progress boxs for plugins through the CPPluginUIProgress
 * class and also for the progress bar on the splash screen.
 * It is up to the listener to ensure that Thread safety on GUI function is maintained, the methods
 * of the Listener may be called in an arbitrary thread.
 */
class CINEPAINT_CORE_API CinePaintProgressListener
{
	public:
		/**
		 * Destructor
		 */
		virtual ~CinePaintProgressListener() {};

		/**
		 * This method is used to create a new progress box if
		 * one does not already exist.
		**/
		virtual void ProgressStart(char* ID, char* msg, int stop, int step) = 0;

		/**
		 * The message to display has changed.
		 * @param msg The new message to display.
		**/
		virtual void MessageChanged(char* ID, char* msg ) = 0;

		/**
		 * The size of the progress bar has changed.
		 * @param start the new start value.
		 * @param stop the new stop value.
		 * @param step the new step value.
		 * The progress bar is also reset - i,e position is set to start.
		**/
		virtual void LimitsChanged(char* ID, int start, int stop, int step) = 0;

		/**
		 * An update event has occured so move the 
		 * progress bar on a bit.
		**/
		virtual void ProgressUpdate(char* ID) = 0;

		/**
		 * An event which sets the progress bar to a specific 
		 * position has occured.
		**/
		virtual void ProgressPosition(char* ID, int pos) = 0;

		/**
		 * The progress has reset, set position back to beginning
		 **/
		virtual void ProgressReset(char* ID) = 0;

		/**
		 * The process has complete.
		 * remove the progress object.
		**/
		virtual void ProgressComplete(char* ID) = 0;

	protected:
		/**
		 * Constrcts a new CinePaintInitListener - protected to avoid attempted instances of abstract class
		 *
		 */
		CinePaintProgressListener() {};

	private:


}; /* class CinepaintEvent */

#endif /* _CINEPAINT_PROGRESS_LISTENER_H_ */
