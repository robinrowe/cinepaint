/* 
 * plugins/jpeg/jpeg.h
 * CinePaint -- an image sequence manipulation program
 * Copyright (C) 2004 The CinePaint Project (Donald MacVicar)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * $Id
 */
#ifndef _JPEG_PLUGIN_H_
#define _JPEG_PLUGIN_H_


#include "plug-ins/pdb/CinePaintPlugin.h"


class  JpegLoadPlugin : public CinePaintPlugin
{

public:
	JpegLoadPlugin();
	~JpegLoadPlugin();

	void query(World* world);
	void run(CPParamList& _inParams, CPParamList& _return);
	virtual PFNDeleteCinePaintPlugin GetDeleteFunc( );

	virtual CPPluginParamDefs& GetInParamDefs(CPPluginParamDefs& param_defs) const;
	virtual CPPluginParamDefs& GetReturnParamDefs(CPPluginParamDefs& param_defs) const;

private:
	int load_image (char* filename);

	double test;
	double test2;
};

typedef struct
{
  float quality;
  float smoothing;
  int optimize;
} JpegSaveVals;

class  JpegSavePlugin : public CinePaintPlugin
{

public:
	JpegSavePlugin();
	~JpegSavePlugin();

	void query(World* world);
	void run(CPParamList& _inParams, CPParamList& _return);

	virtual CPPluginParamDefs& GetInParamDefs(CPPluginParamDefs& param_defs) const;
	virtual CPPluginParamDefs& GetReturnParamDefs(CPPluginParamDefs& param_defs) const;

	virtual CPPluginUIDef &GetUIDefs(CPPluginUIDef &ui);

private:
	int save_image (char* filename);
	int save_dlg( JpegSaveVals &vals );

	double test;
	double test2;

};

#endif //_JPEG_PLUGIN_H_
