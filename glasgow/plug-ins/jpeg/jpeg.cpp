/* The GIMP -- an image manipulation program
* Copyright (C) 1995 Spencer Kimball and Peter Mattis
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/* JPEG loading and saving file filter for the GIMP
*  -Peter Mattis
*
* This filter is heavily based upon the "example.c" file in libjpeg.
* In fact most of the loading and saving code was simply cut-and-pasted
*  from that file. The filter, therefore, also uses libjpeg.
*/

/*
* This filter has been heavily modified to run using the new CinePaint Plugin arch
* it is based on the original and uses the same load/save code from the libjpeg,
* But the interface for the dynamic object is VERY different.
* Donald MacVicar 30/9/04
*/

#include <stdio.h>
#include <setjmp.h>
#include "jpeg.h"
#include "../../dll_api.h"

#ifdef _WIN32
// rsr: Avoid INT32 redef
#define XMD_H
#endif

extern "C" {
#include <jpeglib.h>
}

#include <plug-ins/pdb/PDB.h>
#include <plug-ins/pdb/CPPluginUIProgress.h>
#include <plug-ins/pdb/CPPluginUIDialog.h>
#include <plug-ins/pdb/CPPluginUINumber.h>
#include <plug-ins/pdb/CPPluginUIBool.h>
#include <plug-ins/pdb/AbstractBufFactory.h>
#include <plug-ins/pdb/AbstractBuf.h>
#include <plug-ins/pdb/CinePaintImage.h>
#include <plug-ins/pdb/RenderingManager.h>
#include <plug-ins/pdb/AbstractRenderer.h>

#define SCALE_WIDTH 125


typedef struct
{
	int  run;
} JpegSaveInterface;


typedef struct my_error_mgr {
	struct jpeg_error_mgr pub;	/* "public" fields */

	jmp_buf setjmp_buffer;	/* for return to caller */
} *my_error_ptr;


/*
* Here's the routine that will replace the standard error_exit method:
*/

static void
my_error_exit (j_common_ptr cinfo)
{
	/* cinfo->err really points to a my_error_mgr struct, so coerce pointer */
	my_error_ptr myerr = (my_error_ptr) cinfo->err;

	/* Always display the message. */
	/* We could postpone this until after returning, if we chose. */
	(*cinfo->err->output_message) (cinfo);

	/* Return control to the setjmp point */
	longjmp (myerr->setjmp_buffer, 1);
}

/**
* This is the main entry point used to create an instance of the plugin.
* it must be implemented for all plugins, this allows us to add them to the
* list in the database and execute them as required.
**/
extern "C" CINEPAINT_JPEG_API CinePaintPlugin* CreateJpegLoadObject()
{
	return new JpegLoadPlugin();
}

extern "C" CINEPAINT_JPEG_API void DeleteJpegLoadObject( CinePaintPlugin *p )
{
	delete p;

}

extern "C" CINEPAINT_JPEG_API CinePaintPlugin* CreateJpegSaveObject()
{
	return new JpegSavePlugin();
}

extern "C" CINEPAINT_JPEG_API void DeleteJpegSaveObject( CinePaintPlugin *p )
{
	delete p;
}
// List of classes that this DLL/Plugin provides, MUST MUST be null terminated.
CinePaintPlugin::PFNCreateCinePaintPlugin PluginClassList[] = {CreateJpegLoadObject, CreateJpegSaveObject, 0};

// Exported function to 
extern "C" CINEPAINT_JPEG_API CinePaintPlugin::PFNCreateCinePaintPlugin *GetPluginClassList()
{
	return PluginClassList;
};


static const CinePaintPlugin::CPPluginParamDef LOAD_INPUT_PARAM_DEFS[] = { {PDB_CSTRING, "filename", "jpeg file to load", false} };
static const CinePaintPlugin::CPPluginParamDef LOAD_RETURN_PARAM_DEFS[] = { {PDB_CPIMAGE, "image", "the loaded jpeg image", false} };

//=====================
// Jpeg Load Plugin.
//=====================
static CinePaintFileHandlerInfo load_info(CreateJpegLoadObject, "file_jpeg_load", "jpg,jpeg", "", "6,string,JFIF");
/*************
* Constructor & Destructor 
*/
JpegLoadPlugin::JpegLoadPlugin()
:CinePaintPlugin("file_load_jpeg",
				 "loads files of the jpeg file format",
				 "FIXME: write help for jpeg_load",
				 "Spencer Kimball & Peter Mattis",
				 "Spencer Kimball & Peter Mattis",
				 "1995-1996")

{
	test = 10;
	test = 0.7;
}

JpegLoadPlugin::~JpegLoadPlugin()
{
	printf("Deleteing JpegLoadPlugin\n");
}

/*******************
* The Query method.
*/
void JpegLoadPlugin::query(World* world)
{//	PDB* pdb=world->pdb;
	PDB* pdb=PDB::GetPDB(world);
	if(pdb)
	{	pdb->RegisterLoadHandler(&load_info);
	}
	printf("JpegLoadPlugin::query\n");
}

CinePaintPlugin::PFNDeleteCinePaintPlugin JpegLoadPlugin::GetDeleteFunc( )
{
	return DeleteJpegLoadObject;
}

void JpegLoadPlugin::run( CPParamList& _inParams, CPParamList& _return )
{
	printf("JpegLoadPlugin::run()\n");
	struct jpeg_decompress_struct cinfo;
	struct my_error_mgr jerr;
	FILE *infile;

	int scanlines;
	int i;

	char* filename;
	_inParams.GetParam("filename", &filename);

	/* We set up the normal JPEG error routines. */
	cinfo.err = jpeg_std_error (&jerr.pub);
	jerr.pub.error_exit = my_error_exit;

	if ((infile = fopen (filename, "rb")) == NULL)
	{
		printf ("can't open \"%s\"\n", filename);
		return;
	}

	//name = (char* )malloc (strlen (filename) + 12);
	//sprintf (name, "Loading %s:", filename);
	printf("Loading %s\n", filename);
	//DWM TODDO gimp_progress_init (name);
	CPPluginUIProgress *_prog;

	//  /* Establish the setjmp return context for my_error_exit to use. */
	if (setjmp (jerr.setjmp_buffer))
	{
		/* If we get here, the JPEG code has signaled an error.
		* We need to clean up the JPEG object, close the input file, and return.
		*/
		jpeg_destroy_decompress (&cinfo);
		if (infile)
			fclose (infile);
		return;
	}
	/* Now we can initialize the JPEG decompression object. */
	jpeg_create_decompress (&cinfo);

	/* Step 2: specify data source (eg, a file) */
	jpeg_stdio_src (&cinfo, infile);

	/* Step 3: read file parameters with jpeg_read_header() */
	(void) jpeg_read_header (&cinfo, TRUE);
	/* We can ignore the return value from jpeg_read_header since
	*   (a) suspension is not possible with the stdio data source, and
	*   (b) we passed TRUE to reject a tables-only JPEG file as an error.
	* See libjpeg.doc for more info.
	*/

	/* Step 4: set parameters for decompression */

	/* In this example, we don't need to change any of the defaults set by
	* jpeg_read_header(), so we do nothing here.
	*/

	/* Step 5: Start decompressor */
	jpeg_start_decompress (&cinfo);

	/* We may need to do some setup of our own at this point before reading
	* the data.  After jpeg_start_decompress() we have the correct scaled
	* output image dimensions available, as well as the output colormap
	* if we asked for color quantization.
	* In this example, we need to make an output work buffer of the right size.
	*/
	/* temporary buffer */

	/* ----------------
	* Initialise the progress window.
	*/
	_prog = new CPPluginUIProgress("file_load_jpeg", "Loading...", cinfo.output_width, 1);
	/* Create a new image of the proper size and type.  */
	CinePaintTag tag;
	switch (cinfo.output_components)
	{
	case 1:
		tag.SetPrecision(CinePaintTag::PRECISION_U8_ENUM);
		tag.SetFormat(CinePaintTag::FORMAT_GRAY_ENUM);
		tag.SetAlpha(false);
		break;
	case 3:
		tag.SetPrecision(CinePaintTag::PRECISION_U8_ENUM);
		tag.SetFormat(CinePaintTag::FORMAT_RGB_ENUM);
		tag.SetAlpha(false);
		break;
	default:
		return;
	}

	/* Create the abstract buffer of the correct size and type */
	std::auto_ptr<AbstractBuf> image = AbstractBufFactory::CreateBuf(tag, cinfo.output_width, cinfo.output_height, AbstractBufFactory::STORAGE_FLAT);
	/* Take control of the memory allocated for the buffer */
	AbstractBuf *img = image.release();

	/* Step 6: while (scan lines remain to be read) */
	/*           jpeg_read_scanlines(...); */
	scanlines = cinfo.output_height;
	img->RefPortionReadWrite(0, 0);
	unsigned char* pixel_buf = img->GetPortionData(0,0);
	unsigned char* *row_buf = new unsigned char*[1];
	for (i = 0; i < scanlines; i++)
	{
		row_buf[0] = pixel_buf;
		pixel_buf += cinfo.output_width*cinfo.output_components;
		jpeg_read_scanlines (&cinfo, (JSAMPARRAY)row_buf , 1);

		//DWM TODO     gimp_progress_update ((double) cinfo.output_scanline / (double) cinfo.output_height);
		_prog->UpdateProgress();
	}

	/* Step 7: Finish decompression */

	jpeg_finish_decompress (&cinfo);
	/* We can ignore the return value since suspension is not possible
	* with the stdio data source.
	*/

	/* Step 8: Release JPEG decompression object */

	/* This is an important step since it will release a good deal of memory. */
	jpeg_destroy_decompress (&cinfo);

	/* free up the temporary buffers */
	delete [] row_buf;

	/* After finish_decompress, we can close the input file.
	* Here we postpone it until after no more JPEG errors are possible,
	* so as to simplify the setjmp error logic above.  (Actually, I don't
	* think that jpeg_destroy can do an error exit, but why assume anything...)
	*/
	fclose (infile);

	/* At this point you may want to check to see whether any corrupt-data
	* warnings occurred (test whether jerr.num_warnings is nonzero).
	*/

	// Put the result image into the return args list.
	CinePaintImage *_img = new CinePaintImage(img);
	_img->SetFileName(filename);

	CPPluginArg arg;
	arg.pdb_cpimage = _img;
	_return.AddParam("image", &arg, PDB_CPIMAGE);

	/// Close the progress dialog.
	_prog->Hide();
	delete _prog;
}


// Plugin parameter definitions

CinePaintPlugin::CPPluginParamDefs& JpegLoadPlugin::GetInParamDefs(CPPluginParamDefs& param_defs) const
{
	param_defs.m_params = LOAD_INPUT_PARAM_DEFS;
	param_defs.m_param_count = sizeof(LOAD_INPUT_PARAM_DEFS) / sizeof(CPPluginParamDef);
	return(param_defs);
}
CinePaintPlugin::CPPluginParamDefs& JpegLoadPlugin::GetReturnParamDefs(CPPluginParamDefs& param_defs) const
{
	param_defs.m_params = LOAD_RETURN_PARAM_DEFS;
	param_defs.m_param_count = sizeof(LOAD_INPUT_PARAM_DEFS) / sizeof(CPPluginParamDef);
	return(param_defs);
}





//=====================
// Jpeg Save Plugin.
//=====================

static CinePaintFileHandlerInfo save_info(CreateJpegSaveObject, "file_jpeg_save", "jpg,jpeg", ""); 
CPPluginUIDialog _dlg("jpeg_save_dlg", "Jpeg Save Options", 100, 200);

CPPluginUINumber _qual("jpeg_quality", "Quality", 0, 1, 0.001, 0.75, CPPluginUINumber::SLIDER);
CPPluginUINumber _smooth("jpeg_smoothing", "Smoothing", 0, 1, 0.001, 0, CPPluginUINumber::SLIDER);
CPPluginUIBool _opt("jpeg_optimize", "Optimize", true);


static CPPluginUIWidget *ui_widgets[] = {&_dlg, &_qual, &_smooth, &_opt};
static int num_ui_widgets = 4;

static const CinePaintPlugin::CPPluginParamDef SAVE_INPUT_PARAM_DEFS[] = {
		{PDB_FLOAT, "jpeg_quality", "quality setting", false},
		{PDB_FLOAT, "jpeg_smoothing", "smoothing setting", false},
		{PDB_INT, "jpeg_optimize", "optimize setting", false}
	};

static const CinePaintPlugin::CPPluginParamDef SAVE_RETURN_PARAM_DEFS[] = { {PDB_CPIMAGE, "image", "the loaded jpeg image", false} };

/*************
* Constructor & Destructor 
*/
JpegSavePlugin::JpegSavePlugin()
:CinePaintPlugin("file_save_jpeg",
				 "loads files of the jpeg file format",
				 "FIXME: write help for jpeg_load",
				 "Spencer Kimball & Peter Mattis",
				 "Spencer Kimball & Peter Mattis",
				 "1995-1996")

{
}


JpegSavePlugin::~JpegSavePlugin()
{
}

/*******************
* The Query method.
*/
void JpegSavePlugin::query(World* world)
{//	PDB* pdb=world->pdb;
	PDB* pdb=PDB::GetPDB(world);
	if(pdb)
	{	pdb->RegisterSaveHandler(&save_info);
	}
	printf("JpegSavePlugin::query\n");
}

CPPluginUIDef &JpegSavePlugin::GetUIDefs(CPPluginUIDef &ui)
{
	ui.num = num_ui_widgets;
	ui.widgets = ui_widgets;
	return ui;
}

void JpegSavePlugin::run(CPParamList& _inParams, CPParamList& _return )
{
	char* filename;
	CinePaintImage *img;
	printf("JpegSavePlugin::run()\n");

	JpegSaveVals jsvals;
	jsvals.optimize = 1;
	jsvals.quality = 0.75;
	jsvals.smoothing = 0;

	if (_inParams.GetParam("filename", &filename))
		printf("Save as JPEG %s \n", filename);
	else 
	{
		printf("ERROR: JpegSavePlugin::run : No Filename given\n");
		return;
	}
	if (_inParams.GetParam("image", &img))
		printf("Image of (%d, %d) \n", img->GetWidth(), img->GetHeight());
	else
	{
		printf("ERROR: JpegSavePlugin::run : No Image given\n");
		return;
	}

	// Lets see if we get the param settings from the Dialog.
	_inParams.GetParam("jpeg_quality", &jsvals.quality);
	_inParams.GetParam("jpeg_smoothing", &jsvals.smoothing);
	_inParams.GetParam("jpeg_optimize", &jsvals.optimize);

	printf("JPEG Save vals: %f %f %d\n", jsvals.quality,jsvals.smoothing,jsvals.optimize);

	struct jpeg_compress_struct cinfo;
	struct my_error_mgr jerr;
	FILE *outfile;
	unsigned char* temp, *t;
	const unsigned char* src, *s;
	int has_alpha;
	int rowstride;
	int i, j;

	save_dlg(jsvals);
	CPPluginUIProgress _progress("jpeg_plugin_save", "Saving...", img->GetHeight(),1 );

	/* Step 1: allocate and initialize JPEG compression object */

	/* We have to set up the error handler first, in case the initialization
	* step fails.  (Unlikely, but it could happen if you are out of memory.)
	* This routine fills in the contents of struct jerr, and returns jerr's
	* address which we place into the link field in cinfo.
	*/
	cinfo.err = jpeg_std_error (&jerr.pub);
	jerr.pub.error_exit = my_error_exit;

	outfile = NULL;
	/* Establish the setjmp return context for my_error_exit to use. */
	if (setjmp (jerr.setjmp_buffer))
	{
		/* If we get here, the JPEG code has signaled an error.
		* We need to clean up the JPEG object, close the input file, and return.
		*/
		jpeg_destroy_compress (&cinfo);
		if (outfile)
			fclose (outfile);
		_progress.Hide();
		printf("ERROR: JpegSavePlugin::run() JPEG Error\n");
		return;
	}

	/* Now we can initialize the JPEG compression object. */
	jpeg_create_compress (&cinfo);

	/* Step 2: specify data destination (eg, a file) */
	/* Note: steps 2 and 3 can be done in either order. */

	/* Here we use the library-supplied code to send compressed data to a
	* stdio stream.  You can also write your own code to do something else.
	* VERY IMPORTANT: use "b" option to fopen() if you are on a machine that
	* requires it in order to write binary files.
	*/
	if ((outfile = fopen (filename, "wb")) == NULL)
	{
		fprintf (stderr, "can't open %s\n", filename);
		return;
	}
	jpeg_stdio_dest (&cinfo, outfile);

	/* Get the input image and a pointer to its data.
	*/
	const CinePaintTag& tag = img->GetTag();

	if (tag.GetFormat() == CinePaintTag::FORMAT_RGB_ENUM || tag.GetFormat() ==	CinePaintTag::FORMAT_GRAY_ENUM )
		cinfo.input_components = tag.GetBytes();
	else
	{
		printf("ERROR JpegSavePlugin:run() Cannot Handle image format\n");
		return;
	}
	has_alpha = 0;
	if(tag.HasAlpha())
	{
		printf("WARNING: JpegSavePlugin::run() Dropping Alpha Channel\n");
		cinfo.input_components--;
		has_alpha = 1;
	}

	/* Step 3: set parameters for compression */

	/* First we supply a description of the input image.
	* Four fields of the cinfo struct must be filled in:
	*/
	/* image width and height, in pixels */
	cinfo.image_width = img->GetWidth();
	cinfo.image_height = img->GetHeight();
	/* colorspace of input image */
	cinfo.in_color_space = (tag.GetFormat() == CinePaintTag::FORMAT_RGB_ENUM) ? JCS_RGB : JCS_GRAYSCALE;
	/* Now use the library's routine to set default compression parameters.
	* (You must set at least cinfo.in_color_space before calling this,
	* since the defaults depend on the source color space.)
	*/
	jpeg_set_defaults (&cinfo);
	/* Now you can set any non-default parameters you wish to.
	* Here we just illustrate the use of quality (quantization table) scaling:
	*/
	jpeg_set_quality (&cinfo, (int) (jsvals.quality * 100), TRUE /* limit to baseline-JPEG values */);
	cinfo.smoothing_factor = (int) (jsvals.smoothing * 100);
	cinfo.optimize_coding = jsvals.optimize;

	/* Step 4: Start compressor */

	/* TRUE ensures that we will write a complete interchange-JPEG file.
	* Pass TRUE unless you are very sure of what you're doing.
	*/
	jpeg_start_compress (&cinfo, TRUE);

	/* Step 5: while (scan lines remain to be written) */
	/*           jpeg_write_scanlines(...); */

	/* Here we use the library's state variable cinfo.next_scanline as the
	* loop counter, so that we don't have to keep track ourselves.
	* To keep things simple, we pass one scanline per call; you can pass
	* more if you wish, though.
	*/
	/* JSAMPLEs per row in image_buffer */
	//  rowstride = drawable->bpp * drawable->width;
	temp = new unsigned char[cinfo.image_width * cinfo.input_components];
	//  data = (guchar *) malloc (rowstride * gimp_tile_height ());

	// Get Image Buffer from img , need to render first, JPEG does
	// not handle multilayer images.
	AbstractRenderer *_render = img->GetRenderingManager().getDefaultRenderer();
	if (!_render->IsValid())
		_render->Render();
	const unsigned char* src_buf = _render->GetRender().GetPortionData(0,0);
	s = src_buf;
	while (cinfo.next_scanline < cinfo.image_height)
	{
		t = temp;
		i = cinfo.image_width;

		while (i--)
		{
			for (j = 0; j < cinfo.input_components; j++)
				*t++ = *s++;
			if (has_alpha)  /* ignore alpha channel */
				s++;
		}

		src += rowstride;
		jpeg_write_scanlines (&cinfo, (JSAMPARRAY) &temp, 1);

		//     if ((cinfo.next_scanline % 5) == 0)
		//	gimp_progress_update ((double) cinfo.next_scanline / (double) cinfo.image_height);
		_progress.UpdateProgress();
	}

	/* Step 6: Finish compression */
	jpeg_finish_compress (&cinfo);
	/* After finish_compress, we can close the output file. */
	fclose (outfile);

	/* Step 7: release JPEG compression object */

	/* This is an important step since it will release a good deal of memory. */
	jpeg_destroy_compress (&cinfo);
	/* free the temporary buffer */
	delete temp;

	/* And we're done! */
	/*gimp_do_progress (1, 1);*/
	_progress.Hide();
}


// Plugin parameter definitions

CinePaintPlugin::CPPluginParamDefs& JpegSavePlugin::GetInParamDefs(CPPluginParamDefs& param_defs) const
{
	param_defs.m_params = SAVE_INPUT_PARAM_DEFS;
	param_defs.m_param_count = sizeof(SAVE_INPUT_PARAM_DEFS) / sizeof(CPPluginParamDef);
	return(param_defs);
}
CinePaintPlugin::CPPluginParamDefs& JpegSavePlugin::GetReturnParamDefs(CPPluginParamDefs& param_defs) const
{
	param_defs.m_params = SAVE_RETURN_PARAM_DEFS;
	param_defs.m_param_count = sizeof(SAVE_RETURN_PARAM_DEFS) / sizeof(CPPluginParamDef);
	return(param_defs);
}

int JpegSavePlugin::save_dlg( JpegSaveVals &vals )
{
	return 1;
}

