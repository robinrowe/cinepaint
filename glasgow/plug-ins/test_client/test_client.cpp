/*
 *
 *   Simple Blur filter for The CinePaint -- a sequence manipulation program
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Modified from the GIMP original blur plugin 20/10/2004 Donald MacVicar
 *
 * $Id:
 */

/*
 * Macros...
 */

#define MIN(a,b)		(((a) < (b)) ? (a) : (b))
#define MAX(a,b)		(((a) > (b)) ? (a) : (b))
#define ROUND(x) 		(int)((x)+.5)

/*
 * Constants...
 */

#define PLUG_IN_NAME		"test_client"
#define PLUG_IN_VERSION		"November 22004"

#include "test_client.h"
#include "dll_api.h"
#include "plug-ins/pdb/PDB.h"
#include "plug-ins/pdb/CPPluginUIProgress.h"
#include "plug-ins/pdb/CinePaintClientWorker.h"
#include "plug-ins/pdb/CinePaintNetJob.h"
#include "plug-ins/pdb/Drawable.h"
#include "plug-ins/pdb/AbstractBufFactory.h"
#include "plug-ins/pdb/AbstractBuf.h"
#include "plug-ins/pdb/Float16.h"
/*
 * Exports for CinePaint API.
 */
extern "C" CINEPAINT_TEST_CLIENT_API CinePaintPlugin* CreateTestClientPluginObject()
{
 return new TestClient();
}

// List of classes that this DLL/Plugin provides, MUST MUST be null terminated.
CinePaintPlugin::PFNCreateCinePaintPlugin PluginClassList[] = {CreateTestClientPluginObject, 0};

// Exported function to get class constructors list.
extern "C" CINEPAINT_TEST_CLIENT_API CinePaintPlugin::PFNCreateCinePaintPlugin *GetPluginClassList()
{
	return PluginClassList;
}

static CinePaintPluginInfo info(CreateTestClientPluginObject, PLUG_IN_NAME, "<Image>/Filters/Blur/Client Test",
								"RGB*, GRAY*, U16_RGB*, U16_GRAY*, FLOAT_RGB*, FLOAT_GRAY*, FLOAT16_RGB*, FLOAT16_GRAY*" );

static const CinePaintPlugin::CPPluginParamDef INPUT_PARAM_DEFS[] = { {PDB_DRAWABLE, "drawable", "drawable to blur", true}};

/*************
 * Constructor & Destructor 
 */
TestClient::TestClient()
:CinePaintPlugin(PLUG_IN_NAME,
                 "Test Client connection",
                 "This plug-in performs a blur convolution filter on an image.",
                 "Donald MacVicar",
                 "",
                 PLUG_IN_VERSION )

{
}

TestClient::~TestClient() 
{
}

/**
 * The query(World* world) mehtod is called by the core at startup 
 * the plugin calls the appropriate Register() method of the Plugin Database
 * this adds this plugin to the list allowing the rest of the application
 * access to it.
**/
void TestClient::query(World* world)
{	PDB* pdb=PDB::GetPDB(world);
	if(pdb)
	{	pdb->Register(&info);
	}
	printf("TestClient::query\n");
}

/**
 * Called by the framework to execute the functionality encapsulated by this plugin.
**/
void TestClient::run(CPParamList& _inParams, CPParamList& _return)
{
	printf("TestClient::Run()\n");

	int _res;
	Drawable *_drawable;
	_res = _inParams.GetParam("drawable", &_drawable);
	if (_res)
	{
		AbstractBuf& _img = _drawable->GetBuffer();
		CinePaintClientWorker cli("localhost", 10007);
		CinePaintNetJob j("plug_in_blur",  &_img);
		cli.SendData(j);
		// NetJob is owned by cli - will be deleted when it is.
		CinePaintNetJob *res = cli.WaitOnData();

		// Copy data back to input image, when result is valid.
		if ( res )
		{
			unsigned char* dst = _img.GetPortionData(0,0);
			unsigned char* src = res->Return()->GetPortionData(0,0);

			for (int i=0; i<_img.GetWidth()*_img.GetHeight()*_img.GetTag().GetBytes(); i++)
			{*dst = *src; dst++; src++;}
		}
		else
			printf("ERROR: Test_Client plugin::Run() No data recieved from server\n");
	}
	else
		printf("ERROR: Blur::run() No Image parameter given\n");



}


CinePaintPlugin::CPPluginParamDefs& TestClient::GetInParamDefs(CPPluginParamDefs& param_defs) const
{
	param_defs.m_params = INPUT_PARAM_DEFS;
	param_defs.m_param_count = sizeof(INPUT_PARAM_DEFS) / sizeof(CPPluginParamDef);
	return(param_defs);
}

CinePaintPlugin::CPPluginParamDefs& TestClient::GetReturnParamDefs(CPPluginParamDefs& param_defs) const
{
	param_defs.m_params = 0;
	param_defs.m_param_count = 0;
	return(param_defs);
}
