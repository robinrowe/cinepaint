/*
 * "$Id: info.h,v 1.4 2006/12/18 08:21:24 robinrowe Exp $"
 *
 *   info header for the tiff plug-in in CinePaint
 *
 *   Copyright 2003-2004   Kai-Uwe Behrmann (ku.b@gmx.de)
 *
 *   This program is free software; you can redistribute it and/or modify it
 *   under the terms of the GNU General Public License as published by the Free
 *   Software Foundation; either version 2 of the License, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *   for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _INFO_H_
#define _INFO_H_

#include "tiff_plugin.h"
#include <plug-ins/CinePaintTag.h>
#include <list>

/***   global variables   ***/

extern char color_space_name_[];

/*** struct definitions ***/

class TiffImageInfo
{
public:
	TiffImageInfo() {};
	~TiffImageInfo() {};

	struct TiffSaveVals
	{
		int  use_icc_profile; /* use ICC Profile data for image saving */
		int  compression;		/* compression type */
		int  fillorder;		/* msb versus lsb */
		int  premultiply;		/* assoc versus unassociated alpha */
	} ;


	struct ImageInfo
	{
		int   cols, rows;		/* count of pixel per row/col */
		float   xres, yres;		/* tif resolution */
		unsigned short  res_unit;		/* physical resolution unit */
		float   xpos, ypos;		/* position corresponding to x[y]res */
		int   offx, offy,		/* offsets of IFD */
			max_offx, max_offy,  /* offsets over whole tif */
			viewport_offx, viewport_offy; /* offsets for viewing area */
		int     orientation;         /* need of flipping? */
		int     visible;             /* is this layer visible in the application? */

		int   image_width, image_height;  /* size   over all or viewport  */
		//#if GIMP_MAJOR_VERSION >= 1 && GIMP_MINOR_VERSION >= 2
		//  GimpUnit unit;                /* metric  */
		//#endif
		unsigned short  bps, spp, photomet,	/* infos from tif */
			bps_selected,	/* bits per sample, samples per pixel */
			/* photometric - colortype ( gray, rgb, lab... ) */
			rowsperstrip;        /* rows per strip */
		unsigned short  *red,         /* indexed colour map */
			*grn, *blu;
		double   stonits;             /* candelas/meter^2 for Y = 1.0 of XYZ */
		int     logDataFMT;          /* data type target libtiff shall convert to */
		int    /* image_type,*/ im_t_sel;/* colortype and bitdepht information */
		CinePaintTag tag;	/* Image format tag - replaces image_type */
		int     layer_type;		/* colortype / bitdepht / alpha */
		unsigned short  extra, *extra_types;	/* additional channels */
		int     alpha;		/* extra == alpha channel? */
		int     assoc;		/* alpha is premultiplied - associated ? */
		int    *sign;		/* sign of integers for three channels */
		//#if GIMP_MAJOR_VERSION == 1 && GIMP_MINOR_VERSION == 3
		//  GimpRGB  color;  /* for gimp-1.3 , filmgimp needs: guchar color  */
		//#else
		unsigned char   color[3];		/* cinepaint */
		//#endif
		int     worst_case,
			worst_case_nonreduced;/* fallback switch */

		TiffSaveVals save_vals;	/* storing the save settings */
		//  ChannelData  *channel;        /* some pixel related variables */
		//  GimpParasite *parasite;       /* attachments  */

		unsigned int  tmp;			/* ? */

#ifdef TIFFTAG_ICCPROFILE
		unsigned int   profile_size;        /* size in byte */
		char  *icc_profile;         /* ICC profiles */
		char  *icc_profile_info;    /* ICC profile description */
#endif

		unsigned short   planar;		/* RGBRGBRGB versus RRRGGGBBB */
		int      grayscale;
		int maxval, minval, numcolors;/* grayscale related */
		unsigned short  sampleformat;	/* data type */
		long     pagecount;		/* count of all IFD's */
		long     aktuelles_dir;	/* local IFD counter  */
		unsigned char   filetype;		/* TIFFTAG_(O)SUBFILETYPE */
		unsigned int  pagenumber;          /* IFDs number */
		unsigned int  pagescount;          /* stored IFDs count */
		int      pagecounting;        /* are pagenumbers stored at all? */
		int      pagecounted;         /* is pagenumber stored for this IFD? */
		char    *pagename;		/* IFD page / gimp layer name */
		char    *img_desc;  		/* image description */
		char    *software;  		/* application who created the tiff */
		char    *filename;  		/* current filename */
		char     colorspace[5];
	};


	typedef std::list<ImageInfo *> ImageInfoList;

	ImageInfoList m_infoList;

	ImageInfoList &GetImgList() { return m_infoList; };
	/*** function definitions ***/

	/* --- information list functions --- */
	int   get_tiff_info         (  TIFF            *tif);
	int   get_ifd_info          (  TIFF            *tif);
	// gint   get_image_info        (  gint32           image_ID,
	//                                 gint32           drawable_ID,
	//                                ImageInfo       *info);/
	//gint   get_layer_info        (  gint32           image_ID,
	//                                gint32           drawable_ID,
	//                                ImageInfo       *info);
	int   photometFromICC       (  char*            color_space_name, ImageInfo *inf);

	char* getProfileInfo( cmsHPROFILE  hProfile);
	/* --- variable function --- */
	void   init_info             ( ImageInfo *inf );
	char*  print_info            ( ImageInfo *inf );

	TiffSaveVals tsvals_;
private:
	// Some text processing support functions
	int get_text_field(TIFF *tif, char* tiff_ptr, int TIFFTAG, char* target);
	void txt_add( char* ptr, char* name, int var );
	void txt_add( char* ptr, char* name, float var );
	void txt_add( char* ptr, char* name, char* var );

};



#endif /*_INFO_H_ */
