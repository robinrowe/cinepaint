/*
 * "$Id: tiff_plugin.h,v 1.7 2006/12/18 08:21:24 robinrowe Exp $"
 *
 *   header for the tiff plug-in in CinePaint
 *
 *   Copyright 2002-2004   Kai-Uwe Behrmann (ku.b@gmx.de)
 *
 *   This program is free software; you can redistribute it and/or modify it
 *   under the terms of the GNU General Public License as published by the Free
 *   Software Foundation; either version 2 of the License, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *   for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _TIFF_PLUG_IN_H_
#define _TIFF_PLUG_IN_H_

#include <tiffconf.h>
#include <tiffio.h>
#include <stdlib.h>
#include <lcms.h>

#if (!__unix__ && !__APPLE__)
typedef struct tiff TIFF;
#endif

#ifndef COMPRESSION_JP2000
#define COMPRESSION_JP2000 34712
#endif
#ifndef PHOTOMETRIC_ICCLAB
#define PHOTOMETRIC_ICCLAB 9
#endif
#ifndef PHOTOMETRIC_ITULAB
#define PHOTOMETRIC_ITULAB 10
#endif

#if 0     /* set i/o stream to 16-bit Log or 32-bit float for SGI Log HDR format*/
  #define SGILOGDATAFMT = SGILOGDATAFMT_16BIT_LOG
#else
  #define SGILOGDATAFMT SGILOGDATAFMT_FLOAT
#endif

#define ROUND(x) ((int) ((x) + 0.5))

static void tiff_error(const char* module, const char* fmt, va_list ap);
static void tiff_warning(const char* module, const char* fmt, va_list ap);
static char* version(void);

#include <plug-ins/CinePaintPlugin.h>

// Forward Declarations.

/**
 * Class for loading tiff files.
**/
#include "info.h"
class CinePaintImage;
class Layer;
class  TiffLoadPlugin : public CinePaintPlugin
{

public:
	TiffLoadPlugin();
	~TiffLoadPlugin();

	void query(World* world);
	void run(CPParamList& _inParams, CPParamList& _return);

	virtual CPPluginParamDefs& GetInParamDefs(CPPluginParamDefs& param_defs) const;
	virtual CPPluginParamDefs& GetReturnParamDefs(CPPluginParamDefs& param_defs) const;

private:
	CinePaintImage *load_image (char* filename);
	int load_IFD( TIFF *tif, CinePaintImage *img, TiffImageInfo::ImageInfo *current_info);

	// Load image data from the tiff file,
	// and store it into the given layer.
	void load_rgba ( TIFF *tif, Layer *layer, TiffImageInfo::ImageInfo *current_info);
	void load_lines( TIFF *tif, Layer *layer, TiffImageInfo::ImageInfo *current_info);
	void load_tiles( TIFF *tif, Layer *layer, TiffImageInfo::ImageInfo *current_info);

/*	void   read_separate (   guchar          *source,
		ImageInfo       *current_info,
		gint32           startcol,
		gint32           startrow,
		gint32           cols,
		gint32           rows,
		gint             sample);
	void   read_64bit    (guchar       *source,
		ChannelData  *channel,
		gushort       photomet,
		gint32        startcol,
		gint32        startrow,
		gint32        cols,
		gint32        rows,
		gint          alpha,
		gint          extra,
		gint          assoc,
		gint          align);
	void   read_32bit    (guchar       *source,
		ChannelData  *channel,
		gushort       photomet,
		gint32        startcol,
		gint32        startrow,
		gint32        cols,
		gint32        rows,
		gint          alpha,
		gint          extra,
		gint          assoc,
		gint          align,
		ImageInfo       *current_info);
	void   read_f16bit   (guchar       *source,
		ChannelData  *channel,
		gushort       photomet,
		gint32        startcol,
		gint32        startrow,
		gint32        cols,
		gint32        rows,
		gint          alpha,
		gint          extra,
		gint          assoc,
		gint          align);
	void   read_u16bit   (guchar       *source,
		ChannelData  *channel,
		gushort       photomet,
		gint32        startcol,
		gint32        startrow,
		gint32        cols,
		gint32        rows,
		gint          alpha,
		gint          extra,
		gint          assoc,
		gint          align);
	void   read_8bit     (guchar       *source,
		ChannelData  *channel,
		gushort       photomet,
		gint32        startcol,
		gint32        startrow,
		gint32        cols,
		gint32        rows,
		gint          alpha,
		gint          extra,
		gint          assoc,
		gint          align);
	void   read_default  (guchar       *source,
		ChannelData  *channel,
		gushort       bps,
		gushort       photomet,
		gint32        startcol,
		gint32        startrow,
		gint32        cols,
		gint32        rows,
		gint          alpha,
		gint          extra,
		gint          assoc,
		gint          align);
*/
};

/**
 * Class for saving tiff files
**/
class  TiffSavePlugin : public CinePaintPlugin
{

public:
	TiffSavePlugin();
	~TiffSavePlugin();

	void query(World* world);
	void run(CPParamList& _inParams, CPParamList& _return);

	virtual CPPluginParamDefs& GetInParamDefs(CPPluginParamDefs& param_defs) const;
	virtual CPPluginParamDefs& GetReturnParamDefs(CPPluginParamDefs& param_defs) const;

	virtual CPPluginUIDef &GetUIDefs(CPPluginUIDef &ui);


private:
	bool save_image (char* filename, CinePaintImage *img);
};

#endif /*_TIFF_PLUG_IN_H_*/

