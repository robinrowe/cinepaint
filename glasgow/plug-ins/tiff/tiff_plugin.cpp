/* tiff loading and saving for the GIMP
*  -Peter Mattis
* The TIFF loading code has been completely revamped by Nick Lamb
* njl195@zepler.org.uk -- 18 May 1998
* And it now gains support for tiles (and doubtless a zillion bugs)
* njl195@zepler.org.uk -- 12 June 1999
* LZW patent fuss continues :(
* njl195@zepler.org.uk -- 20 April 2000
* khk@khk.net -- 13 May 2000
* image flipping for RGBA loader fixed
* njl195@zepler.org.uk --  24 January 2002
* Added support for ICCPROFILE tiff tag. If this tag is present in a
* TIFF file, then a parasite is created and vice versa.
* peter@kirchgessner.net -- 29 Oct 2002
* tiff can read and write multipaged files
*  web@tiscali.de -- 11.November 2002
* merged with tiff.c functions from above (as in gimp-1.3.10)
* better layerhandling 
*  web@tiscali.de -- 03 Dezember 2002
* sampleformat-tag for floats is written out, basic Lab loading
* saving and loading of float16_gray without alpha
*  web@tiscali.de -- 18 Dezember 2002
* added dummy layer for marking gimps viewport while saving tif
*  web@tiscali.de -- February 2003
* bugfixes lossy strings
*  web@tiscali.de -- Febr./March/April 2003
* bugfixes for order missmatched function arguments and implementations
*  web@tiscali.de -- 15 April 2003
* infos stored in an list in _ImageInfoList_ to avoid double libtiff calls
*  web@tiscali.de -- 22.April 2003
* changed and added calls to set viewport in cinepaint while opening an tif
*  web@tiscali.de -- 27.April 2003
* <http://cch.loria.fr/documentation/IEEE754/> for 16bit floats no standard
* using first R&H 16bit float - alpha therein is buggy
* Put the TIFF structure to the end of save_image,
*  to avoid exeeding of it's size.
*  web@tiscali.de -- 05.Mai 2003
* we differenciate CIELab and ICCLab. TODO ITULab
* changed alpha handling - assoc must be detected - v.1.3.07
* link against gimp-1.2 with help of the GIMP_VERSION macro
* made image description editable for cinepaint
* load CMYK, pipe through lcms if an ICC profile is embedded - v1.3.08
* fixed extra channels bug - v1.3.09
*  Kai-Uwe Behrmann (web@tiscali.de) -- September/October 2003
* error message for downsampling in gimp added +
* added handling of TIFFTAG_ORIENTATION tag (fixes #96611)
*  Maurits Rijk  <lpeek.mrijk@consunet.nl> - 1. October 2003
* save all layers and count only the visible ones - v1.3.10
*  Kai-Uwe Behrmann (web@tiscali.de)
* made compatible with gimp-1.3.22/23 - v1.3.11
*  Pablo d'Angelo (pablo@mathematik.uni-ulm.de) / Kai-Uwe
* fixed viewport and false profile bug, changed errorhandling of lcms - v1.3.12
* added copyright tag - v1.3.13
*  Kai-Uwe Behrmann (web@tiscali.de) -- November 2003
* added 64-bit integer support - v1.3.14
* added support for non source tree compiling - v1.3.15
* loading of SGI LOG Luv images into (XYZ->)sRGB colour space
*  (taken from libtiff tif_luv.c - Greg Ward Larson) 
* a compile time switch is included to load into 16-bit integer - v1.3.16
*  Kai-Uwe Behrmann (web@tiscali.de) -- December 2003
* add switch for colour transparency premultiplying or alpha (simple copy)
* add artist entry - v1.3.17
* modularize the save function - v1.3.18
* gui is sensible to output type - v1.3.19
* clean ImageInfo list for loading + bugfixes - v1.3.20
* reading separate image planes in all bit depths - v1.4.0
*  Kai-Uwe Behrmann (web@tiscali.de) -- January 2004
* applying an embeded profile is now possible to all colour spaces
* applying the stonits tag to SGI LOG Luv's
* direct XYZ <-> CIE*Lab conversion;
* writing of separate image planes in certain cases - v1.4.1
* TODO more modularizing
* memory allocation check,
* alpha writing bug fixed - v1.4.2 
*  Kai-Uwe Behrmann (ku.b@gmx.de) -- February 2004
* added ICC conversion confirmation dialog for embedded profiles - v1.4.3
* better 1-bit loading
*  Kai-Uwe Behrmann (ku.b@gmx.de) -- March 2004
* fixed type of sampleformat and ifdef'ed canonicalize for osX
* added print_info for debugging - v1.4.4
*  Kai-Uwe Behrmann (ku.b@gmx.de) -- April 2004
* update to CinePaints CMS - v1.4.6
*  Kai-Uwe Behrmann (ku.b@gmx.de) -- June 2004
* TODO the stonits tag needs appropriate PDB calls
* default profiles for known colorspaces - v1.4.8
* correct handling of LOGLuv colorspace - v1.4.9
*  Kai-Uwe Behrmann (ku.b@gmx.de) -- August 2004
* support SGI Log L   Gray scale - v1.4.10
*  Kai-Uwe Behrmann (ku.b@gmx.de) -- November 2004
*
* The code for this filter is based on "tifftopnm" and "pnmtotiff",
*  2 programs that are a part of the netpbm package.
*/

/*
** tifftopnm.c - converts a Tagged Image File to a portable anymap
**
** Derived by Jef Poskanzer from tif2ras.c, which is:
**
** Copyright (c) 1990 by Sun Microsystems, Inc.
**
** Author: Patrick J. Naughton
** naughton@wind.sun.com
**
** Permission to use, copy, modify, and distribute this software and its
** documentation for any purpose and without fee is hereby granted,
** provided that the above copyright notice appear in all copies and that
** both that copyright notice and this permission notice appear in
** supporting documentation.
**
** This file is provided AS IS with no warranties of any kind.  The author
** shall have no liability with respect to the infringement of copyrights,
** trade secrets or any patents by this file or any part thereof.  In no
** event will the author be liable for any lost revenue or profits or
** other special, indirect and consequential damages.
*/

static char Version_[] = "v1.0.0";


/***   some configuration switches   ***/

#if 0
int write_separate_planes = 1;
#else
int write_separate_planes = 0;
#endif

int use_icc = 1;


/* Required headers for CinePaint */
#include "../../dll_api.h"
#include <stdio.h>
#include <setjmp.h>


#include <plug-ins/PDB.h>
#include <plug-ins/CPPluginUIProgress.h>
#include <plug-ins/CPPluginUIDialog.h>
#include <plug-ins/CPPluginUINumber.h>
#include <plug-ins/CPPluginUIBool.h>
#include <plug-ins/AbstractBufFactory.h>
#include <plug-ins/AbstractBuf.h>
#include <plug-ins/CinePaintImage.h>
#include <plug-ins/Layer.h>
#include "tiff_plugin.h"
#include "info.h"

#include <errno.h>

#ifndef __func__
#define __func__ __FUNCTION__
#endif
/**
* This is the main entry point used to create an instance of the plugin.
* it must be implemented for all plugins, this allows us to add them to the
* list in the database and execute them as required.
**/
extern "C" CINEPAINT_TIFF_API CinePaintPlugin* CreateTiffLoadObject()
{
	return new TiffLoadPlugin();
}

extern "C" CINEPAINT_TIFF_API CinePaintPlugin* CreateTiffSaveObject()
{
	return new TiffSavePlugin();
}

// List of classes that this DLL/Plugin provides, MUST MUST be null terminated.
CinePaintPlugin::PFNCreateCinePaintPlugin PluginClassList[] = {CreateTiffLoadObject, CreateTiffSaveObject, 0};

// Exported function to 
extern "C" CINEPAINT_TIFF_API CinePaintPlugin::PFNCreateCinePaintPlugin *GetPluginClassList()
{
	return PluginClassList;
};



static const CinePaintPlugin::CPPluginParamDef LOAD_INPUT_PARAM_DEFS[] = { {PDB_CSTRING, "filename", "tiff file to load", false} };
static const CinePaintPlugin::CPPluginParamDef LOAD_RETURN_PARAM_DEFS[] = { {PDB_CPIMAGE, "image", "the loaded tiff image", false} };


//=====================
// Tiff Load Plugin.
//=====================
static CinePaintFileHandlerInfo load_info(CreateTiffLoadObject, "file_tiff_load", "tif,tiff", "", "0,string,II*\\0,0,string,MM\\0*");
/*************
* Constructor & Destructor 
*/
TiffLoadPlugin::TiffLoadPlugin()
:CinePaintPlugin("file_tiff_load",
				 "loads files of the tiff file format",
				 "This plug-in loades TIFF files.\nIt can handle bit depths up to 16-bit integers per channel with conversion to 8-bit for gimp and up to 16-bit integers and 32-bit floats natively for cinepaint.\nSome color spaces are experimental such as Lab and separated. CMYK can be converted to Lab/RGB if an appropriate profile is embedded. You need lcms configured and installed for the conversion (actually CinePaint only).\nMultilayers are supported.\nWith an special description it can read negative offsets written from CinePaint/GIMP. So be carefully with this in other applications.",
				 "Spencer Kimball & Peter Mattis",
				 "Nick Lamb <njl195@zepler.org.uk>, Kai-Uwe Behrmann, Donald MacVicar",
				 "1995-1996,1998-2000,2002-2004")

{
}

TiffLoadPlugin::~TiffLoadPlugin()
{
}

/*******************
* The Query method.
*/
void TiffLoadPlugin::query(World* world)
{	printf("TiffLoadPlugin::query\n");
//	PDB* pdb=world->pdb;
	PDB* pdb=PDB::GetPDB(world);
	if(pdb)
	{	pdb->RegisterLoadHandler(&load_info);
	}
}

void TiffLoadPlugin::run(CPParamList& _inParams, CPParamList& _return )
{
	printf("TiffLoadPlugin::run()\n");

	char* filename = 0;
	CinePaintImage *img = 0;
	
	if ( _inParams.GetParam("filename", &filename) )
	{
    	img = load_image(filename);
		// Put the result image into the return args list.

		CPPluginArg arg;
		arg.pdb_cpimage = img;
		_return.AddParam("image", &arg, PDB_CPIMAGE);
	}
	else
		printf("ERROR: TiffLoadPlugin::Run - no Filename in Input parameter list\n");
	
}



// Plugin parameter definitions

CinePaintPlugin::CPPluginParamDefs& TiffLoadPlugin::GetInParamDefs(CPPluginParamDefs& param_defs) const
{
	param_defs.m_params = LOAD_INPUT_PARAM_DEFS;
	param_defs.m_param_count = sizeof(LOAD_INPUT_PARAM_DEFS) / sizeof(CPPluginParamDef);
	return(param_defs);
}
CinePaintPlugin::CPPluginParamDefs& TiffLoadPlugin::GetReturnParamDefs(CPPluginParamDefs& param_defs) const
{
	param_defs.m_params = LOAD_RETURN_PARAM_DEFS;
	param_defs.m_param_count = sizeof(LOAD_RETURN_PARAM_DEFS) / sizeof(CPPluginParamDef);
	return(param_defs);
}



CinePaintImage *TiffLoadPlugin::load_image ( char* filename)
{

	TIFF    *tif;
	TiffImageInfo  *info = NULL;          /* top IFD info */
	//char   *name;
	long	  i;//, j;
	int    layer=0;

	/* Error handling */
	TIFFSetWarningHandler (tiff_warning);
	TIFFSetErrorHandler (tiff_error);

	CPPluginUIProgress *_prog;

	tif = TIFFOpen (filename, "rm");
	if (!tif) {
		printf("Could not open '%s' for reading: %s",  filename, "ERRNO");
		return FALSE;
	}	

	_prog = new CPPluginUIProgress("file_load_tiff", "Loading...", 2, 1);


	info = new TiffImageInfo;

	if (!(info->get_tiff_info ( tif )))
	{
		printf ("%s:%d get_tiff_info(...) failed",__FILE__,__LINE__);
		TIFFClose (tif);
		return FALSE;
	}

	//  info->top->filename = g_strdup (filename);
	info->GetImgList().front()->filename = strdup(filename);

	// Create a New Image.
	CinePaintImage *img;

	img = new CinePaintImage(filename, info->GetImgList().front()->tag, info->GetImgList().front()->image_width, info->GetImgList().front()->image_height);
//	if ((image_ID = gimp_image_new ((guint)info->top->image_width,
//		(guint)info->top->image_height, info->top->im_t_sel)) == -1) 
	if (!img)
	{
		printf("TIFF Can't create a new image\n%dx%d %d",
				info->GetImgList().front()->image_width, info->GetImgList().front()->image_height,info->GetImgList().front()->im_t_sel);
		TIFFClose (tif);
			return FALSE;
	}
	if (info->GetImgList().front()->bps == 64)
		printf ("WARNING! This Image has %d bits per channel. CinePaint \n"
		"can only handle 32 bit, so it will be converted for you.\n"
		"Information will be lost because of this conversion."
		,info->GetImgList().front()->bps);

		/* The image is created. Now set informations and attach other data.
		* attach a parasite containing an ICC profile - if found in the TIFF file */

#ifdef TIFFTAG_ICCPROFILE
		if (info->GetImgList().front()->profile_size > 0) 
		{
			CPParamList *parasites = img->GetParasiteList();
			if (parasites)
			{
				CPPluginArg _arg;
				_arg.pdb_cstring = info->GetImgList().front()->icc_profile;
				//_arg.pdb_voidpointer = info->GetImgList().front()->icc_profile;
				parasites->AddParam("icc_profile", &_arg, PDB_CSTRING);
			}
		}
		else 
		{
			printf ("image %s has no profile embedded. searching for an suitable one.\n", filename);
			/* comunicating color spaces from tiffsettings */
			//switch (info->GetImgList().front()->photomet)
			//{
			//case PHOTOMETRIC_LOGLUV:
			//	{ /* creating an special profile with equal energy white point */
			//		cmsHPROFILE p = cmsCreateXYZProfile ();
			//		LPcmsCIEXYZ wtpt = (LPcmsCIEXYZ) calloc (sizeof (cmsCIEXYZ),1);
			//		size_t size = 0;
			//		char* buf;

			//		wtpt->X = 1.0; wtpt->Y = 1.0; wtpt->Z = 1.0;
			//		_cmsAddXYZTag (p, icSigMediaWhitePointTag, wtpt);

			//		_cmsSaveProfileToMem (p, NULL, &size);
			//		buf = (char*) calloc (sizeof(char), size);
			//		_cmsSaveProfileToMem (p, buf, &size);
			//		gimp_image_set_icc_profile_by_mem (image_ID,
			//			(guint32)size, buf);

			//		cmsCloseProfile( p );
			//		free (buf);
			//		printf ("%s:%d set XYZ profile with DE\n",__FILE__,__LINE__);
			//	}
			//	break;
			//case PHOTOMETRIC_RGB:
			//	gimp_image_set_srgb_profile (image_ID);
			//	printf ("%s:%d set sRGB profile\n",__FILE__,__LINE__);
			//	/* TODO: be more specific to:
			//	PrimaryChromacitiesTag
			//	TransferFunction
			//	TransferRange          */
			//	/*cmsHPROFILE cmsCreateRGBProfile(LPcmsCIExyY WhitePoint,
			//	LPcmsCIExyYTRIPLE Primaries,
			//	LPGAMMATABLE TransferFunction[3]); */
			//	break;
			//case PHOTOMETRIC_CIELAB:
			//case PHOTOMETRIC_ICCLAB:
			//case PHOTOMETRIC_ITULAB:
			//	gimp_image_set_lab_profile (image_ID);
			//	printf ("%s:%d set Lab profile\n",__FILE__,__LINE__);
			//	break;
			//case PHOTOMETRIC_SEPARATED:
			//	break;
			//case PHOTOMETRIC_MINISWHITE:
			//case PHOTOMETRIC_MINISBLACK:
			//	/* TODO more specific GRAYSCALE handling */
			//	{ size_t size = 0;
			//	char* buf;
			//	LPGAMMATABLE gamma = cmsBuildGamma(256, 1.0), g;
			//	if (info->photomet == PHOTOMETRIC_MINISWHITE)
			//		g = cmsReverseGamma(256, gamma);
			//	else
			//		g = gamma;
			//	cmsHPROFILE lp = cmsCreateGrayProfile(cmsD50_xyY(), g);
			//	_cmsSaveProfileToMem (lp, NULL, &size);
			//	buf = (char*) calloc (sizeof(char), size);
			//	_cmsSaveProfileToMem (lp, buf, &size);

			//	gimp_image_set_icc_profile_by_mem (image_ID,
			//		(guint32)size,
			//		buf);
			//	cmsCloseProfile( lp );
			//	cmsFreeGamma(gamma);
			//	free (buf);
			//	printf ("%s:%d set gray profile\n",__FILE__,__LINE__);
			//	}
			//	break;
			//}
		}
#endif

// DWM TODO - FIX SAVE OPTIONS.
//		/* attach a parasite containing the compression */
//#if GIMP_MAJOR_VERSION >= 1
//		parasite = gimp_parasite_new ("tiff-save-options", 0,
//			sizeof (info->save_vals),
//			&info->save_vals);
//		gimp_image_parasite_attach (image_ID, parasite);
//		gimp_parasite_free (parasite);
//#endif /* GIMP_HAVE_PARASITES */


		/* Attach a parasite containing the image description.  Pretend to
		* be a gimp comment so other plugins will use this description as
		* an image comment where appropriate. */
// DWM : TODO FIX
//#if GIMP_MAJOR_VERSION >= 1
//		{
//			if (info->img_desc)
//			{
//				int len;
//
//				len = strlen(info->img_desc) + 1;
//				len = MIN(len, 241);
//				info->img_desc[len-1] = '\000';
//
//				parasite = gimp_parasite_new ("gimp-comment",
//					GIMP_PARASITE_PERSISTENT,
//					len, info->img_desc);
//				gimp_image_parasite_attach (image_ID, parasite);
//				gimp_parasite_free (parasite);
//			}
//		}
//#endif /* GIMP_HAVE_PARASITES */

		// DWM TODO : NOT IN CURRENT CINEPAINT
//#if GIMP_MAJOR_VERSION >= 1 && GIMP_MINOR_VERSION >= 2
//		{
//			/* any resolution info in the file? */
//			/* now set the new image's resolution info */
//			info->unit = GIMP_UNIT_PIXEL; /* invalid unit */
//
//			if (info->res_unit) 
//			{
//
//				switch (info->res_unit) 
//				{
//				case RESUNIT_NONE:
//					/* ImageMagick writes files with this silly resunit */
//					printf ("TIFF warning: resolution units meaningless\n");
//					break;
//
//				case RESUNIT_INCH:
//					info->unit = GIMP_UNIT_INCH;
//					break;
//
//				case RESUNIT_CENTIMETER:
//					info->xres *= (gfloat)2.54;
//					info->yres *= (gfloat)2.54;
//					info->unit = GIMP_UNIT_MM; /* as this is our default metric unit  */
//					break;
//
//				default:
//					printf ("TIFF file error: unknown resolution unit type %d, "
//						"assuming dpi\n", info->res_unit);
//					break;
//				}
//			} 
//			else 
//			{ /* no res unit tag */
//				/* old AppleScan software produces these */
//				printf ("TIFF warning: resolution specified without\n"
//					"any units tag, assuming dpi\n");
//			}
//			if (info->res_unit != RESUNIT_NONE)
//			{
//				gimp_image_set_resolution (image_ID, info->xres,
//					info->yres);
//				if (info->res_unit != GIMP_UNIT_PIXEL)
//					gimp_image_set_unit (image_ID, info->unit); 
//			}
//		}
//#endif

		/* Here starts the directory loop.
		* We are starting with the last directory for the buttom layer 
		* and count the tiff-directories down to let the layerstack
		* in gimp grow up.
		*/

// Use a reverse Iterator.

		TiffImageInfo::ImageInfoList::reverse_iterator iter = info->GetImgList().rbegin();

		//info = info->top;	/* select the top of the infos of the first IFD */
		//for ( i = 0 ; i < iter->pagecount ; i++ ) { /* select the last IFD */
		//	if (i > 0)
		//		info = info->next; 
		//}

		for ( i = 0 ; iter != info->GetImgList().rend(); ++iter ) {
			/* We only accept pages with the same colordepth and photometric indent.
			* We could also test against (TIFFTAG_SUBFILETYPE, FILETYPE_PAGE)
			* but this would be insecure.
			* To test against page-numbers would be additional. */
			TiffImageInfo::ImageInfo *inf = (*iter);
			if (!TIFFSetDirectory( tif, (unsigned int) inf->aktuelles_dir ))
			{
				printf("%s:%d IFD %d not found",__FILE__, __LINE__,(int)inf->aktuelles_dir);
				TIFFClose (tif);
				return 0;
			}

			if (inf->filetype & FILETYPE_REDUCEDIMAGE) {/* skip */
				printf ("%s:%d IFD: %ld filetype: FILETYPE_REDUCEDIMAGE \n",
					__FILE__,__LINE__, inf->aktuelles_dir);
			}

			/* load the IFD after certain conditions, divergine IFDs are not converted*/
			if ( (info->GetImgList().front()->photomet == inf->photomet)
				&& (info->GetImgList().front()->bps_selected == inf->bps)
				&& (&(*iter)->cols != NULL)
				&& (&(*iter)->rows != NULL)
				&& ((info->GetImgList().front()->worst_case_nonreduced
				&& (inf->filetype & FILETYPE_REDUCEDIMAGE))
				|| !(inf->filetype & FILETYPE_REDUCEDIMAGE)) )
			{
				/* Allocate ChannelData for all channels, even the background layer */
				/*m_new ( iter->channel, ChannelData, 1 + info->extra, return -1)
					for (j=0; j <= info->extra ; j++) {
						info->channel[j].drawable = NULL;
						info->channel[j].pixels = NULL;
					}*/
					
				if (!load_IFD (tif, img, (*iter)))
				{
					printf("load_IFD with IFD %d failed",
					(int)inf->aktuelles_dir);
					TIFFClose (tif);
					return 0;
				}
			}
			else if ( strcmp (inf->pagename, "viewport")
				&& (inf->filetype & FILETYPE_MASK) )
			{
				printf ("TIFF IFD %d not read\n", (int)inf->aktuelles_dir);
			}
		}
		TIFFClose (tif);
	// DWM TODO : Resize for Viewport.
		//gimp_image_resize ( image_ID, (guint)info->top->image_width,
		//	(guint)info->top->image_height,
		//	- info->top->viewport_offx,
		//	- info->top->viewport_offy);

		delete info;



		return img;
}

int TiffLoadPlugin::load_IFD( TIFF *tif, CinePaintImage *img, TiffImageInfo::ImageInfo *ci)
{
	unsigned short *redmap, *greenmap, *bluemap;
	unsigned char   cmap[768];

	bool flip_horizontal = false;
	bool flip_vertical = false;

	int   i, j;

	// ci->image_ID = image_ID;

	/* GimpParasite *parasite; */ /* not available in filmgimp */

	/* We need to repeat this after every directory change */
	if (ci->photomet == PHOTOMETRIC_LOGLUV
		|| ci->photomet == PHOTOMETRIC_LOGL)
		TIFFSetField(tif, TIFFTAG_SGILOGDATAFMT, ci->logDataFMT);

	/* Install colormap for INDEXED images only */
	if (ci->tag.GetFormat() == CinePaintTag::FORMAT_INDEXED_ENUM)
	{
		if (!TIFFGetField (tif, TIFFTAG_COLORMAP, &redmap, &greenmap, &bluemap)) 
		{
			printf ("TIFF Can't get colormaps\n");
			return 0;
		}
		/* - let it here for maybe debugging */
		/*  if (!TIFFGetField (tif, TIFFTAG_COLORMAP, &redcolormap,
		&greencolormap, &bluecolormap))
		{
		g_print ("error getting colormaps\n");
		gimp_quit ();
		}
		ci->numcolors = ci->maxval + 1;
		ci->maxval = 255;
		ci->grayscale = 0;

		for (i = 0; i < ci->numcolors; i++)
		{
		redcolormap[i] >>= 8;
		greencolormap[i] >>= 8;
		bluecolormap[i] >>= 8;
		}

		if (ci->numcolors > 256) {
		ci->image_type = RGB;
		} else {
		ci->image_type = INDEXED;
		}*/
		for (i = 0, j = 0; i < (1 << ci->bps); i++) 
		{
			cmap[j++] = redmap[i] >> 8;
			cmap[j++] = greenmap[i] >> 8;
			cmap[j++] = bluemap[i] >> 8;
		}
// TDOD ADD COLOUR MAP FOR INDEXED IMAGES.		gimp_image_set_cmap (image_ID, cmap, (1 << ci->bps));
	}

	// Create a new layer.
	Layer *_layer = new Layer( ci->tag, ci->cols, ci->rows, ci->img_desc, 1 );
	//layer = gimp_layer_new      (image_ID, ci->pagename, 
	//	(guint)ci->cols,
	//	(guint)ci->rows,
	//	(guint)ci->layer_type,
	//	100.0, NORMAL_MODE);


	// TODO : Deal With Channels later.
	//ci->channel[0].ID = layer;
	//gimp_image_add_layer (image_ID, layer, 0);
	//ci->channel[0].drawable = gimp_drawable_get (layer);

	//if (ci->extra > 0 && !ci->worst_case) {
	//	/* Add alpha channels as appropriate */
	//	for (i= 1; i <= ci->extra; i++) {
	//		ci->channel[i].ID = gimp_channel_new(image_ID, _("TIFF Channel"),
	//			(guint)ci->cols, (guint)ci->rows,
	//			100.0,
	//			&ci->color[0]
	//			);
	//			gimp_image_add_channel(image_ID, ci->channel[i].ID, 0);
	//			ci->channel[i].drawable= gimp_drawable_get (ci->channel[i].ID);
	//	}
	//}

	if (ci->worst_case) {
		load_rgba  (tif, _layer, ci);
	} else if (TIFFIsTiled(tif)) {
		load_tiles (tif, _layer, ci);
	} else { /* Load scanlines in tile_height chunks */
		load_lines (tif, _layer, ci);
	}

	if ( ci->offx > 0 || ci->offy > 0 ) { /* position the layer */ 
		// gimp_layer_set_offsets( layer, ci->offx, ci->offy);
		_layer->SetOffsetX(ci->offx);
		_layer->SetOffsetY(ci->offy);
	}

	if (ci->pagecounted == 0 && ci->pagecounting == 1) 
	{
        _layer->SetVisible(false); //gimp_layer_set_visible( layer, FALSE);
		printf ("%d|%d layer:%d invisible\n",ci->pagecounted,ci->pagecounting, ci->pagenumber);
	} 
	else 
	{
		printf ("%d|%d layer:%d visible\n",ci->pagecounted,ci->pagecounting, ci->pagenumber);
	}
//
//	if (ci->orientation > -1)
//	{
//		switch (ci->orientation)
//		{
//		case ORIENTATION_TOPLEFT:
//			flip_horizontal = FALSE;
//			flip_vertical   = FALSE;
//			break;
//		case ORIENTATION_TOPRIGHT:
//			flip_horizontal = TRUE;
//			flip_vertical   = FALSE;
//			break;
//		case ORIENTATION_BOTRIGHT:
//			flip_horizontal = TRUE;
//			flip_vertical   = TRUE;
//			break;
//		case ORIENTATION_BOTLEFT:
//			flip_horizontal = FALSE;
//			flip_vertical   = TRUE;
//			break;
//		default:
//			flip_horizontal = FALSE;
//			flip_vertical   = FALSE;
//			g_warning ("Orientation %d not handled yet!", ci->orientation);
//			break;
//		}
//
//#if GIMP_MAJOR_VERSION >= 1 /* gimp TODO for cinepaint */
//		if (flip_horizontal || flip_vertical)
//			gimp_image_undo_disable (image_ID);
//
//		if (flip_horizontal)
//			gimp_flip (layer, GIMP_ORIENTATION_HORIZONTAL);
//
//		if (flip_vertical)
//			gimp_flip (layer, GIMP_ORIENTATION_VERTICAL);
//
//		if (flip_horizontal || flip_vertical)
//			gimp_image_undo_enable (image_ID);
//#endif
//	}

	//for (i= 0; !ci->worst_case && i < (gint)ci->extra; ++i) {
	//	gimp_drawable_flush (ci->channel[i].drawable);
	//	gimp_drawable_detach (ci->channel[i].drawable);
	//}

//	Do Return stuff.
	char layer_name[256];
	if ( ci->pagename )
		sprintf(layer_name, "%s", ci->pagename);
	else 
		if (ci->aktuelles_dir == 0)
			sprintf(layer_name, "background");
		else
			sprintf(layer_name, "Layer %d", ci->aktuelles_dir);
	_layer->SetName(layer_name);
	img->AddLayer(_layer);
	return 1;
}

void TiffLoadPlugin::load_rgba(TIFF *tif, Layer *layer, TiffImageInfo::ImageInfo *ci)
{
	//uint32 row;
	//uint32 *buffer=NULL;

	//gimp_pixel_rgn_init (&(ci->channel[0].pixel_rgn), ci->channel[0].drawable,0,0,
	//	ci->cols, ci->rows,
	//	TRUE, FALSE);
	//m_new ( buffer, uint32, ci->cols * ci->rows, return)
	//	ci->channel[0].pixels = (guchar*) buffer;

	//if (!TIFFReadRGBAImage(tif, ci->cols,
	//	ci->rows, buffer, 0))
	//	printf("TIFF Unsupported layout, no RGBA loader\n");

	//for (row = 0; row < ci->rows; ++row) {
	//	gimp_pixel_rgn_set_rect(&(ci->channel[0].pixel_rgn),
	//		ci->channel[0].pixels + row * ci->cols * 4, 0,
	//		ci->rows -row -1,
	//		ci->cols, 1);
	//	gimp_progress_update ((double) row / (double) ci->rows);
	//}

	// Get the buffer from the layer.
	AbstractBuf &_buf = layer->GetBuffer();
	unsigned char* buffer = _buf.GetPortionData(0,0);

	// Read the actual data from image file.
	if (!TIFFReadRGBAImage(tif, ci->cols, ci->rows, (uint32*)buffer, 0))
		printf("TIFF Unsupported layout, no RGBA loader\n");
}

void TiffLoadPlugin::load_tiles(TIFF *tif, Layer *layer, TiffImageInfo::ImageInfo *ci)
{

	printf("TODO: Sorry Tiled Image loading not supported yet\n");
	//unsigned int tileWidth=0, tileLength=0;
	//unsgined int x, y, rows, cols/*,  **tileoffsets */;
	//unsigned char* buffer=NULL;
	//double progress= 0.0, one_row;
	//unsigned short i;

	//TIFFGetField(tif, TIFFTAG_TILEWIDTH, &tileWidth);
	//TIFFGetField(tif, TIFFTAG_TILELENGTH, &tileLength);

	//one_row = (double) tileLength / (double) ci->rows;

	//m_new ( buffer, char, (size_t)TIFFTileSize(tif), return)

	//	for (i= 0; i <= ci->extra; ++i) {
	//		m_new (ci->channel[i].pixels, char, (size_t)
	//			(tileWidth * tileLength * ci->channel[i].drawable->bpp), return)
	//	}

	//	for (y = 0; y < (uint32)ci->rows; y += tileLength) {
	//		for (x = 0; x < (uint32)ci->cols; x += tileWidth) {
	//			gimp_progress_update (progress + one_row *
	//				( (double) x / (double) ci->cols));
	//			if (!TIFFReadTile(tif, buffer, x, y, 0, 0))
	//				printf("TIFFReadTile failed");

	//			/* TODO set here an profile checking with conversion to color_type_ */

	//			cols= MIN(ci->cols - x, tileWidth);
	//			rows= MIN(ci->rows - y, tileLength);
	//			if          (ci->bps == 64) {
	//				read_64bit(buffer, ci->channel, ci->photomet,
	//					(gint)x, (gint)y, (gint)cols, (gint)rows, ci->alpha,
	//					(gint)ci->extra, (gint)ci->assoc,
	//					(gint)(tileWidth - cols));
	//			} else if   (ci->bps == 32) {
	//				read_32bit(buffer, ci->channel, ci->photomet,
	//					(gint)x, (gint)y, (gint)cols, (gint)rows, ci->alpha,
	//					(gint)ci->extra, (gint)ci->assoc,
	//					(gint)(tileWidth - cols), ci);
	//			} else if  ( ci->bps == 16
	//				&& (ci->sampleformat == SAMPLEFORMAT_IEEEFP
	//				|| ci->sampleformat == SAMPLEFORMAT_VOID) ) {
	//					read_f16bit(buffer, ci->channel, ci->photomet,
	//						(gint)x, (gint)y, (gint)cols, (gint)rows, ci->alpha,
	//						(gint)ci->extra, (gint)ci->assoc,
	//						(gint)(tileWidth - cols));
	//				} else if  ( ci->bps == 16
	//					&& (ci->sampleformat == SAMPLEFORMAT_UINT) ) {
	//						read_u16bit(buffer, ci->channel, ci->photomet,
	//							(gint)x, (gint)y, (gint)cols, (gint)rows, ci->alpha,
	//							(gint)ci->extra, (gint)ci->assoc,
	//							(gint)(tileWidth - cols));
	//					} else if   (ci->bps == 8) {
	//						read_8bit(buffer, ci->channel, ci->photomet,
	//							(gint)x, (gint)y, (gint)cols, (gint)rows, ci->alpha,
	//							(gint)ci->extra, (gint)ci->assoc,
	//							(gint)(tileWidth - cols));
	//					} else {
	//						read_default(buffer, ci->channel, ci->bps, ci->photomet,
	//							(gint)x, (gint)y, (gint)cols, (gint)rows, ci->alpha,
	//							(gint)ci->extra, (gint)ci->assoc,
	//							(gint)(tileWidth - cols));
	//					}
	//		}
	//		progress+= one_row;
	//	}
	//	for (i= 0; i <= ci->extra; ++i) {
	//		m_free (ci->channel[i].pixels)
	//	}

	//	m_free(buffer)
}


void TiffLoadPlugin::load_lines( TIFF *tif, Layer *layer, TiffImageInfo::ImageInfo *ci)
{
	int lineSize,    /* needed for alocating memory */
		cols;//,        /* imageWidth in pixel */
		//rows;        /* an variable for saying how many lines to load*/
	/* **stripoffsets ; */  /* determine the offset */
	unsigned char* buffer=NULL;
	int row;       /* jumping (tile-height) row */
	char   *text=NULL;

	cols = ci->cols;

	lineSize = TIFFScanlineSize(tif);

	// Verify LineSize == Image width
	if (lineSize != ci->cols)
		printf(" TIFF : Tiff scanline width != image width\n");

	AbstractBuf &_buf = layer->GetBuffer();
	buffer = _buf.GetPortionData(0,0);

	unsigned char* bp = buffer;
	unsigned short bytes_per_pix = (ci->bps/8) * ci->spp;

	if (ci->planar == PLANARCONFIG_CONTIG) 
	{
			     // TODO:  	if (ci->channel->drawable->bpp !=
					//(int)((float)ci->bps/8 * (float)ci->spp) && ci->bps < 64)
					//g_warning ("%s:%d %s() drawables and tiffs byte depth are different %d != %01f(%01f*%01f)\n",__FILE__,__LINE__,__func__,
					//ci->channel->drawable->bpp,
					//(float)ci->bps/8.0* (float)ci->spp,
					//(float)ci->bps/8.0, (float)ci->spp);

		for (row = 0; row < ci->rows; ++row )
		{
			if (!TIFFReadScanline(tif, bp,	row, 0))
				printf("Scanline %d not readable", (int)row);
			bp += cols * bytes_per_pix;
		}
	}

		//for (i= 0; i <= ci->extra; ++i) { 
		//	m_new (ci->channel[i].pixels, char,
		//		tile_height * cols * ci->channel[i].drawable->bpp, return)
		//}

		//m_new ( buffer, char, lineSize * tile_height, return)

		//	if (ci->planar == PLANARCONFIG_CONTIG) {
		//		if (ci->channel->drawable->bpp !=
		//			(int)((float)ci->bps/8 * (float)ci->spp) && ci->bps < 64)
		//			g_warning ("%s:%d %s() drawables and tiffs byte depth are different %d != %01f(%01f*%01f)\n",__FILE__,__LINE__,__func__,
		//			ci->channel->drawable->bpp,
		//			(float)ci->bps/8.0* (float)ci->spp,
		//			(float)ci->bps/8.0, (float)ci->spp);

		//		for (j_row = 0; j_row < ci->rows; j_row+= tile_height ) { 
		//			/* jumping progress */
		//			gimp_progress_update ( (double) j_row / (double) ci->rows);
		//			/* is the remainder smaller? - then not tile_height  */
		//			rows = MIN(tile_height, ci->rows - j_row);
		//			/* walking along the rows within tile_height */
		//			for (g_row = 0; g_row < rows; ++g_row)
		//				/* buffer is only for one tile-height (* bpp * cols)  */
		//				if (!TIFFReadScanline(tif, buffer + g_row * lineSize,
		//					(uint32)(j_row + g_row), 0))
		//					printf("Scanline %d not readable", (int)g_row);



		//			if        (ci->bps == 64) {
		//	 			read_64bit(buffer, ci->channel, ci->photomet, 0, j_row,
		//					cols, rows, ci->alpha, (gint)ci->extra,
		//					(gint)ci->assoc, 0);
		//			} else if (ci->bps == 32) {
		//				read_32bit(buffer, ci->channel, ci->photomet, 0, j_row,
		//					cols, rows, ci->alpha, (gint)ci->extra,
		//					(gint)ci->assoc, 0, ci);
		//			} else if (ci->bps == 16
		//				&& (ci->sampleformat == SAMPLEFORMAT_IEEEFP
		//				|| ci->sampleformat == SAMPLEFORMAT_VOID) ) {
		//					read_f16bit (buffer, ci->channel, ci->photomet, 0, j_row,
		//						cols, rows, ci->alpha, (gint)ci->extra,
		//						(gint)ci->assoc, 0);
		//				} else if (ci->bps == 16) {
		//					read_u16bit (buffer, ci->channel, ci->photomet, 0, j_row,
		//						cols, rows, ci->alpha, (gint)ci->extra,
		//						(gint)ci->assoc, 0);
		//				} else if (ci->bps == 8) {
		//					read_8bit   (buffer, ci->channel, ci->photomet, 0, j_row,
		//						cols, rows, ci->alpha, (gint)ci->extra,
		//						(gint)ci->assoc, 0);
		//				} else {
		//					read_default(buffer, ci->channel, ci->bps,
		//						ci->photomet, 0, j_row,
		//						cols, rows, ci->alpha, (gint)ci->extra,
		//						(gint)ci->assoc, 0);
		//				}
		//		}
		//	} else { /* PLANARCONFIG_SEPARATE */
		//		uint16 sample;
		//		gint   start = TRUE;

		//		if (ci->channel->drawable->bpp !=
		//			(int)((float)ci->bps/8 * (float)ci->spp) && ci->bps < 64)
		//			g_warning ("%s:%d %s() drawables and tiffs byte depth are different %d != %01f(%01f*%01f)\n",__FILE__,__LINE__,__func__,
		//			ci->channel->drawable->bpp,
		//			(float)ci->bps/8.0* (float)ci->spp,
		//			(float)ci->bps/8.0, (float)ci->spp);

		//		for (sample = 0; sample < (ci->spp + ci->extra); sample++) {
		//			m_new (text, char, 256, ;)
		//				sprintf (text, "%s '%s' plane %d ...",_("Opening"), ci->top->filename,
		//				sample + 1);

		//			gimp_progress_init (text);

		//			m_free (text)

		//				for (j_row = 0; j_row < ci->rows; j_row += tile_height) {
		//					gimp_progress_update ( (double) j_row / (double) ci->rows);
		//					rows = MIN (tile_height, ci->rows - j_row);

		//					for (g_row = 0;  g_row < rows; ++g_row) {
		//						TIFFReadScanline(tif, buffer + g_row * lineSize,
		//							(uint32)(j_row + g_row), sample);
		//					}

		//					read_separate (buffer, ci, 0, j_row, ci->cols, rows, (gint)sample);
		//					start = FALSE;
		//				}
		//		}
		//	}
		//	for (i= 0; i <= ci->extra; ++i) {
		//		m_free (ci->channel[i].pixels)
		//	}

		//	m_free(buffer)
}

/*
"None", "LZW", "Pack Bits", "Deflate", "JPEG", "JP2000", "Adobe"

			   NULL);
  "Fill Order", "LSB to MSB", "MSB to LSB"
*/

//=====================
// Tiff Save Plugin.
//=====================

CPPluginUIDialog _dlg("tiff_save_dlg", "Tiff Save Options", 100, 200);

CPPluginUINumber _qual("quality", "Quality", 0, 1, 0.001, 0.75, CPPluginUINumber::SLIDER);



static CPPluginUIWidget *ui_widgets[] = {&_dlg, &_qual};
static int num_ui_widgets = 2;

static CinePaintFileHandlerInfo save_info(CreateTiffSaveObject, "file_tiff_save", "tif,tiff", "", "0,string,II*\\0,0,string,MM\\0*");
/*************
* Constructor & Destructor 
*/
TiffSavePlugin::TiffSavePlugin()
:CinePaintPlugin("file_tiff_save",
				 "saves files of the tiff file format",
				 "This plug-in saves TIFF files.\nIt can handle bit depths up to 16-bit integers per channel with conversion to 8-bit for gimp and up to 16-bit integers and 32-bit floats natively for cinepaint.\nSome color spaces are experimental such as Lab and separated. CMYK can be converted to Lab/RGB if an appropriate profile is embedded. You need lcms configured and installed for the conversion (actually CinePaint only).\nMultilayers are supported.\nWith an special description it can read negative offsets written from CinePaint/GIMP. So be carefully with this in other applications.",
				 "Spencer Kimball & Peter Mattis",
				 "Nick Lamb <njl195@zepler.org.uk>, Kai-Uwe Behrmann, Donald MacVicar",
				 "1995-1996,1998-2000,2002-2004")

{
}

TiffSavePlugin::~TiffSavePlugin()
{
}


void TiffSavePlugin:: query(World* world)
{	printf("TiffSavePlugin::query\n");
//	PDB* pdb=world->pdb;
	PDB* pdb=PDB::GetPDB(world);
	if(pdb)
	{	pdb->RegisterSaveHandler(&save_info);
	}
}

void TiffSavePlugin::run(CPParamList& _inParams, CPParamList& _return)
{

	char* filename;
	CinePaintImage *img;

	if ( _inParams.GetParam("filename", &filename) )
		if (_inParams.GetParam("image", &img) )
            save_image(filename, img);
		else
			printf("ERROR: TiffSavePlugin::Run() No Image passed\n");
	else
        printf("ERROR: TiffSavePlugin::Run() No Filename passed\n");
}

/*
** pnmtotiff.c - converts a portable anymap to a Tagged Image File
**
** Derived by Jef Poskanzer from ras2tif.c, which is:
**
** Copyright (c) 1990 by Sun Microsystems, Inc.
**
** Author: Patrick J. Naughton
** naughton@wind.sun.com
**
** Permission to use, copy, modify, and distribute this software and its
** documentation for any purpose and without fee is hereby granted,
** provided that the above copyright notice appear in all copies and that
** both that copyright notice and this permission notice appear in
** supporting documentation.
**
** This file is provided AS IS with no warranties of any kind.  The author
** shall have no liability with respect to the infringement of copyrights,
** trade secrets or any patents by this file or any part thereof.  In no
** event will the author be liable for any lost revenue or profits or
** other special, indirect and consequential damages.
*/

bool TiffSavePlugin::save_image (char   *filename, CinePaintImage *img)
{
	TiffImageInfo       *info = 0;
	TIFF *tif;
 
 
	TIFFSetWarningHandler (tiff_warning);
	TIFFSetErrorHandler (tiff_error);

	// Let the user know what we are doing.
	printf("Saving '%s'...", filename);

	// Open the file as TIFF.
	tif = TIFFOpen (filename, "w");

	if (!tif)
    {
		printf("Could not open '%s' for writing: %s", filename, errno);
		return false;
    }

	// We managed to open the file for writing.
	// So lets write...

	// Iterate over the layers.
	LayerManager::const_iterator layer_iter = img->GetLayers().begin();
	
	while ( layer_iter != img->GetLayers().end() )
	{
		TIFFCreateDirectory (tif);

		/* Set TIFF parameters. */
		TIFFSetField (tif, TIFFTAG_IMAGEWIDTH, img->GetWidth());
		TIFFSetField (tif, TIFFTAG_IMAGELENGTH, img->GetHeight());
		

		// Set SampleFormat from tag.
		unsigned int sampleformat;
		short bps;
		switch( img->GetTag().GetPrecision() )
		{
		case CinePaintTag::PRECISION_U8_ENUM:
			sampleformat = SAMPLEFORMAT_UINT ;
			bps = 8;
			break;
		case CinePaintTag::PRECISION_U16_ENUM:
			sampleformat = SAMPLEFORMAT_UINT ;
			bps = 16;
			break;
		case CinePaintTag::PRECISION_FLOAT_ENUM:
			sampleformat = SAMPLEFORMAT_IEEEFP ;
			bps = 32;
			break;
		case CinePaintTag::PRECISION_BFP_ENUM:
		case CinePaintTag::PRECISION_FLOAT16_ENUM:
			sampleformat = SAMPLEFORMAT_VOID ;
			bps = 16;
			break;
		default:
			sampleformat = SAMPLEFORMAT_VOID ;
			bps = 8;
			break;
		}
		if (sampleformat > 0)
			TIFFSetField (tif, TIFFTAG_SAMPLEFORMAT, sampleformat);
		TIFFSetField (tif, TIFFTAG_BITSPERSAMPLE, bps);
		TIFFSetField (tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT );

 //     TIFFSetField (tif, TIFFTAG_COMPRESSION, info->save_vals.compression);
 //   if ((info->save_vals.compression == COMPRESSION_LZW) && (predictor != 0))
 //       TIFFSetField (tif, TIFFTAG_PREDICTOR, predictor);

      /* do we have an ICC profile? If so, write it to the TIFF file */
#ifdef TIFFTAG_ICCPROFILE
		CPParamList *parasites = img->GetParasiteList();
		char* profile_str;
		if (parasites)
		{

			int res = parasites->GetParam("icc_profile", &profile_str);
			if (res == 1)
				TIFFSetField(tif, TIFFTAG_ICCPROFILE, sizeof(profile_str), profile_str);
		}
#endif


		if ( img->GetTag().HasAlpha() )
			TIFFSetField (tif, TIFFTAG_EXTRASAMPLES, 1, EXTRASAMPLE_ASSOCALPHA);
	
		short photomet;
		short spp;
		switch(img->GetTag().GetFormat() )
		{
		case CinePaintTag::FORMAT_GRAY_ENUM:
			photomet = PHOTOMETRIC_MINISBLACK;
			spp = 1;
			break;
		case CinePaintTag::FORMAT_INDEXED_ENUM:
			photomet = PHOTOMETRIC_PALETTE;
			spp = 1;
			break;
		case CinePaintTag::FORMAT_RGB_ENUM:
			photomet = PHOTOMETRIC_RGB;
			spp = 3;
			break;
		default:
			spp = 0;
		}
		TIFFSetField (tif, TIFFTAG_PHOTOMETRIC, photomet);

		TIFFSetField (tif, TIFFTAG_FILLORDER, FILLORDER_MSB2LSB);


		if (img->GetTag().HasAlpha())
			spp++;
	    TIFFSetField (tif, TIFFTAG_SAMPLESPERPIXEL, spp);
   /* TIFFSetField( tif, TIFFTAG_STRIPBYTECOUNTS, rows / rowsperstrip ); */

		int bytesperrow = img->GetWidth() * img->GetTag().GetBytes();
		long rowsperstrip = (8 * 1024) / bytesperrow;
		if (rowsperstrip == 0)
			rowsperstrip = 1;

      TIFFSetField (tif, TIFFTAG_ROWSPERSTRIP, rowsperstrip);
      TIFFSetField (tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);


      TIFFSetField (tif, TIFFTAG_RESOLUTIONUNIT, RESUNIT_NONE );
      //TIFFSetField (tif, TIFFTAG_XRESOLUTION, info->xres);
      //TIFFSetField (tif, TIFFTAG_YRESOLUTION, info->yres);
      
      TIFFSetField (tif, TIFFTAG_DOCUMENTNAME, filename);
      // TIFFSetField (tif, TIFFTAG_PAGENAME, info->pagename );

      /* The TIFF spec explicitely says ASCII for the image description. */
       // TIFFSetField (tif, TIFFTAG_IMAGEDESCRIPTION, image_comment);
       // TIFFSetField (tif, TIFFTAG_COPYRIGHT, image_copyright);

      TIFFSetField (tif, TIFFTAG_SOFTWARE, version());
      // TIFFSetField (tif, TIFFTAG_ARTIST, image_artist);
	  char name[100];
      sprintf( name, "%s",getenv ("HOSTNAME"));	/*TODO works on mac/win? */
      TIFFSetField (tif, TIFFTAG_HOSTCOMPUTER, name);

		time_t	cutime;         /* Time since epoch */
		struct tm	*gmt;
		char time_str[24];

		cutime = time(NULL); /* time right NOW */
		gmt = gmtime(&cutime);
		strftime(time_str, 24, "%Y/%m/%d %H:%M:%S", gmt);
		TIFFSetField (tif, TIFFTAG_DATETIME, time_str);
      
		
      //if ( info->pagecount > 1 ) { /* Tags for multipages */
      //  TIFFSetField (tif, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
      //  if (info->visible)   /* write TIFFTAG_PAGENUMBER only for visible layers */
      //    TIFFSetField (tif, TIFFTAG_PAGENUMBER,
      //                (unsigned short) info->aktuelles_dir, (unsigned short) info->pagecount);
      //}

                    
      /* array to rearrange data */
 //     m_new ( src, guint8, (size_t)bytesperrow * tile_height, return FALSE)
 //     m_new ( data, guint8, (size_t)bytesperrow, return FALSE)

   //   if (info->planar == PLANARCONFIG_SEPARATE) {
 //       planes = (int)(info->spp);
  //    } else {
  //      planes = 1;
   //   }
   //   g_print ("%s:%d %s() write %d planes for %d samples, bytesperrow %d\n",__FILE__,__LINE__,__func__, planes, info->spp, (int)bytesperrow);
		int planes = 1;
		unsigned char* data;
		int bytes_per_row, success;
		for (int sample = 0 ; sample < planes ; sample++) 
		{

			data = (*layer_iter)->GetBuffer().GetPortionData(0,0);
			bytes_per_row = img->GetWidth()*(*layer_iter)->GetBuffer().GetTag().GetBytes();
			/* Now write the TIFF data as stripes. */
			for ( int row=0; row<img->GetHeight(); row++)
			{
				
				success = (TIFFWriteScanline (tif, data, row, 0) >= 0);
				if (success == 0)
		        {
					printf ("failed a scanline write on row %d\n", row);
					return false;
				}
				data+=bytes_per_row;
			}
		}
        TIFFWriteDirectory (tif); /* for multi-pages */
        TIFFFlushData (tif);

		//Move to next layer..
		++layer_iter;
	} 
	TIFFClose (tif);
	return 1;
}

// Plugin parameter definitions

CinePaintPlugin::CPPluginParamDefs& TiffSavePlugin::GetInParamDefs(CPPluginParamDefs& param_defs) const
{
	param_defs.m_params = 0;
	param_defs.m_param_count = 0;
	return(param_defs);
}
CinePaintPlugin::CPPluginParamDefs& TiffSavePlugin::GetReturnParamDefs(CPPluginParamDefs& param_defs) const
{
	param_defs.m_params = 0;
	param_defs.m_param_count = 0;
	return(param_defs);
}

CPPluginUIDef &TiffSavePlugin::GetUIDefs(CPPluginUIDef &ui)
{
	ui.num = num_ui_widgets;
	ui.widgets = ui_widgets;
	return ui;
}

// ****************************
// ****************************

static void
tiff_warning(const char* module, const char* fmt, va_list ap)
{
#ifdef __unix__
  printf( fmt );
  printf( "      \n" );
#else
//   g_logv ((gchar *)0/*G_LOG_DOMAIN*/, G_LOG_LEVEL_MESSAGE, fmt, ap);
	vprintf(fmt, ap);
#endif
}

static void
tiff_error(const char* module, const char* fmt, va_list ap)
{
#ifdef __unix__
  printf( fmt );
  printf( "       \n" );
#else
  // g_logv ((gchar *)0/*G_LOG_DOMAIN*/, G_LOG_LEVEL_MESSAGE, fmt, ap);
	vprintf(fmt, ap);
#endif
}

/* Version_ */
static char plug_in_version_[64];

static char* version(void)
{

  sprintf (plug_in_version_, "%s %s %s", "TIFF plug-in for", "CinePaint", Version_);
  return &plug_in_version_[0];
}
