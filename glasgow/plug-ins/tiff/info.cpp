/*
 *   info handling for the tiff plug-in in CinePaint
 *
 *   Copyright 2003-2004 Kai-Uwe Behrmann
 *
 *   separate stuff / tiff
 *    Kai-Uwe Behrman (ku.b@gmx.de) --  March 2004
 *
 *   This program is free software; you can redistribute it and/or modify it
 *   under the terms of the GNU General Public License as published by the Free
 *   Software Foundation; either version 2 of the License, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *   for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
/*#  include <config.h> */
#endif

#include <sys/types.h>
#include <sys/stat.h>
//#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "tiff_plugin.h"
#include "info.h"

#include <lcms.h>
#ifdef WIN32
#include <fix_strlen.h>
#endif

#ifndef INKSET_MULTIINK
#define     INKSET_MULTIINK             2       /* !multi-ink or hi-fi color */
#endif


int TiffImageInfo::get_tiff_info(TIFF *tif)
{
	long dircount = -1; 

	if (tif) {
		do { /* get all image informations we need */
			dircount++;
			if (!(get_ifd_info ( tif )))
			{
				printf("%s:%d get_ifd_info(...) failed",__FILE__,__LINE__);
				TIFFClose (tif);
				return 0;
			}
			// Set dir number.
			m_infoList.back()->aktuelles_dir = dircount;

		} while (TIFFReadDirectory(tif)) ;
		TIFFSetDirectory( tif, 0); /* go back to the first IFD */
	}
	return 1;
}

char* TiffImageInfo::getProfileInfo( cmsHPROFILE  hProfile)
{
	char* text;
	char* profile_info;
	cmsCIEXYZ WhitePt;
	int       first = FALSE,
		min_len = 24,
		len, i;

	profile_info = (char* )calloc( sizeof(char), 2048 );
	text = (char* )malloc(128);

	{  
#if LCMS_VERSION > 112
		if (cmsIsTag(hProfile, icSigCopyrightTag)) {
			len = strlen (cmsTakeCopyright(hProfile)) + 16;
			if (len > min_len)
				min_len = len + 1;
		}
#endif
		profile_info[0] = '\000';
		if (cmsIsTag(hProfile, icSigProfileDescriptionTag)) {
			sprintf (text,"Description:    ");
			sprintf (profile_info,"%s%s %s",profile_info,text,
				cmsTakeProductDesc(hProfile));
			if (!first) {
				len = min_len - strlen (profile_info);

				for (i=0; i < len * 2.2; i++) {
					sprintf (profile_info,"%s ",profile_info);
				}
			}
			sprintf (profile_info,"%s\n",profile_info);
		}
		if (cmsIsTag(hProfile, icSigDeviceMfgDescTag)) {
			sprintf (text,"Product:        ");
			sprintf (profile_info,"%s%s %s\n",profile_info,text,
				cmsTakeProductName(hProfile));
		}
#if LCMS_VERSION >= 112
		if (cmsIsTag(hProfile, icSigDeviceMfgDescTag)) {
			sprintf (text,"Manufacturer:   ");
			sprintf (profile_info,"%s%s %s\n",profile_info,text,
				cmsTakeManufacturer(hProfile));
		}
		if (cmsIsTag(hProfile, icSigDeviceModelDescTag)) {
			sprintf (text,"Model:          ");
			sprintf (profile_info,"%s%s %s\n",profile_info,text,
				cmsTakeModel(hProfile));
		}
#endif
#if LCMS_VERSION >= 113
		if (cmsIsTag(hProfile, icSigCopyrightTag)) {
			sprintf (text,"Copyright:      ");
			sprintf (profile_info,"%s%s %s\n",profile_info,text,
				cmsTakeCopyright(hProfile));
		}
#endif
		sprintf (profile_info,"%s\n",profile_info);

		cmsTakeMediaWhitePoint (&WhitePt, hProfile);
		_cmsIdentifyWhitePoint (text, &WhitePt);
		sprintf (profile_info, "%s%s\n", profile_info, text);

	}

#ifdef __unix__  /* reformatting the \n\r s of lcms */
#if 0
	while (strchr(profile_info, '\r')) {
		len = (int)strchr (profile_info, '\r') - (int)profile_info;
		profile_info[len] = 32;
		profile_info[len+1] = 32;
		profile_info[len+2] = 32;
		profile_info[len+3] = '\n';
	}
#endif
#endif
	free (text);

	return profile_info;
}


int TiffImageInfo::get_ifd_info(TIFF *tif)
{
//	unsigned int orientation;
	char* tiff_ptr;

	// Create a new info struct and add too list.
	ImageInfo *inf = new ImageInfo;
	m_infoList.push_back(inf);

	init_info(inf);

	inf->worst_case = 0;

	/* compression */
	inf->save_vals.compression = COMPRESSION_NONE;
	if (TIFFGetField (tif, TIFFTAG_COMPRESSION, &inf->tmp))
		inf->save_vals.compression = (int)inf->tmp;

	if(inf->save_vals.compression == COMPRESSION_SGILOG)  
		printf ("32-bit quality SGI Log encoding\n");
	else if (inf->save_vals.compression == COMPRESSION_SGILOG24)  
		printf ("24-bit SGI Log encoding\n");
	else if (inf->save_vals.compression == COMPRESSION_PIXARLOG)  
		printf ("11-bit PIXAR Log encoding\n");
	else if (inf->save_vals.compression == COMPRESSION_PIXARFILM)  
		printf ("10-bit PIXAR film encoding\n");


	if (!TIFFGetField (tif, TIFFTAG_PHOTOMETRIC, &inf->photomet)) {
		printf("TIFF Can't get photometric\nAssuming min-is-black\n");
		/* old AppleScan software misses out the photometric tag (and
		* incidentally assumes min-is-white, but xv assumes min-is-black,
		* so we follow xv's lead.  It's not much hardship to invert the
		* image later). */
		inf->photomet = PHOTOMETRIC_MINISBLACK;
	}

	if (inf->photomet == PHOTOMETRIC_LOGLUV
		|| inf->photomet == PHOTOMETRIC_LOGL) {
			if (TIFFGetField(tif, TIFFTAG_STONITS, &inf->stonits))
				printf ("stonits: %f in candelas/meter^2 for Y = 1.0\n", inf->stonits);
			else
				inf->stonits = 0.0;

			if (inf->save_vals.compression == COMPRESSION_PIXARLOG
				|| inf->save_vals.compression == COMPRESSION_PIXARFILM) {
					if (SGILOGDATAFMT == SGILOGDATAFMT_8BIT) {
						inf->logDataFMT = PIXARLOGDATAFMT_8BIT;
					} else {
						inf->logDataFMT = PIXARLOGDATAFMT_FLOAT;
					}
					TIFFSetField(tif, TIFFTAG_PIXARLOGDATAFMT, inf->logDataFMT);
				} else {
					inf->logDataFMT = SGILOGDATAFMT;
					TIFFSetField(tif, TIFFTAG_SGILOGDATAFMT, inf->logDataFMT);
				}
		}

		if (!TIFFGetField (tif, TIFFTAG_FILLORDER, &inf->save_vals.fillorder))
			inf->save_vals.fillorder = 0;

		if (!TIFFGetFieldDefaulted (tif, TIFFTAG_BITSPERSAMPLE, &inf->bps))
			inf->bps = 0;
		/* supported bps have to go here */
		if (inf->bps > 8
			&& (inf->bps != 16
			&& inf->bps != 32
			&& inf->bps != 64)) {
				printf("difficult samplevalue: %d\n", inf->bps);
				inf->worst_case = 1; /* Wrong sample width => RGBA */
			}

			if (!TIFFGetFieldDefaulted (tif, TIFFTAG_SAMPLESPERPIXEL, &inf->spp))
				inf->spp = 0;
			if (!TIFFGetField (tif, TIFFTAG_ROWSPERSTRIP, &inf->rowsperstrip))
				inf->rowsperstrip = 1;

			if (!TIFFGetField (tif, TIFFTAG_SAMPLEFORMAT, &inf->sampleformat))
				inf->sampleformat = SAMPLEFORMAT_UINT;

			if (inf->logDataFMT == SGILOGDATAFMT_16BIT
				&& inf->photomet == PHOTOMETRIC_LOGLUV)
				inf->sampleformat = SAMPLEFORMAT_UINT;

			/* HACKING!!! */
			if(inf->sampleformat == 196608) { /* ?? */
				inf->sampleformat = SAMPLEFORMAT_IEEEFP;
			}

			if (!TIFFGetField(tif, TIFFTAG_PLANARCONFIG, &inf->planar))
				inf->planar = PLANARCONFIG_CONTIG; /* PLANARCONFIG_SEPARATE */


			inf->alpha = 0;
			inf->assoc = 0;
			inf->extra = 0;

			TIFFGetField (tif, TIFFTAG_EXTRASAMPLES, &inf->extra, &tiff_ptr);
			if (inf->extra > 0) {
				inf->extra_types = new unsigned short[inf->extra];
				memcpy (inf->extra_types, tiff_ptr, inf->extra * sizeof(unsigned short));
				inf->save_vals.premultiply = inf->extra_types[0];
			} 
			else 
			{
				inf->extra_types = NULL;
				inf->save_vals.premultiply = EXTRASAMPLE_UNSPECIFIED;
			}

			if (!TIFFGetField (tif, TIFFTAG_SUBFILETYPE, &inf->filetype))
				inf->filetype = 0x0 ;

			/* test if the extrasample represents an associated alpha channel... */
			/* These cases handle alpha for extra types not signed as associated alpha */

			if (inf->extra > 0	/* extra_types[0] is written libtiff */
				&& (inf->extra_types[0] == EXTRASAMPLE_ASSOCALPHA)) 
			{
				inf->alpha = 1;
				inf->assoc = 1;
				inf->extra--;
				printf ("alpha is premutliplied %s:%d\n",
					__FILE__, __LINE__);
			} 
			else if (inf->extra > 0
				&& (inf->extra_types[0] == EXTRASAMPLE_UNASSALPHA
				|| inf->extra_types[0] == EXTRASAMPLE_UNSPECIFIED)) 
			{
				inf->alpha = 1;
				inf->assoc = 0;
				inf->extra--;
				printf ("alpha is not premutliplied for one of %d channels %s:%d\n",
					inf->spp, __FILE__, __LINE__);
			}
			else if ((inf->photomet == PHOTOMETRIC_CIELAB
				|| inf->photomet == PHOTOMETRIC_ICCLAB
				|| inf->photomet == PHOTOMETRIC_ITULAB
				|| inf->photomet == PHOTOMETRIC_LOGLUV
				|| inf->photomet == PHOTOMETRIC_RGB 
				|| inf->photomet == PHOTOMETRIC_SEPARATED)
				&& inf->spp > 3) 
			{
				inf->alpha = 1;
				if (inf->extra > 0)
					inf->extra--;
				else
					inf->extra = inf->spp - 4; 
			} 
			else if ((inf->photomet != PHOTOMETRIC_CIELAB
				&& inf->photomet != PHOTOMETRIC_ICCLAB
				&& inf->photomet != PHOTOMETRIC_ITULAB
				&& inf->photomet != PHOTOMETRIC_LOGLUV
				&& inf->photomet != PHOTOMETRIC_RGB
				&& inf->photomet != PHOTOMETRIC_SEPARATED)
				&& inf->spp > 1) 
			{
				inf->alpha = 1;
				if (inf->extra > 0)
					inf->extra--;
				else
					inf->extra = inf->spp - 2;
			} 
			else if ((inf->extra > 0)
				&& (inf->photomet == PHOTOMETRIC_SEPARATED)) 
			{
				printf("This plug-in supports CMYK only without alpha channel.");			
			} 
			else if (inf->photomet == PHOTOMETRIC_SEPARATED) 
			{
				/* This is a workaround to allocate 4 channels. */
				inf->alpha = 1;
				inf->assoc = 0;
				inf->extra = 0;
			}

			if (    (inf->photomet == PHOTOMETRIC_RGB
				|| inf->photomet == PHOTOMETRIC_LOGLUV
				|| inf->photomet == PHOTOMETRIC_CIELAB
				|| inf->photomet == PHOTOMETRIC_ICCLAB
				|| inf->photomet == PHOTOMETRIC_ITULAB
				|| inf->photomet == PHOTOMETRIC_SEPARATED)
				&& inf->spp == 3 ) 
			{
				inf->alpha = 0;
				inf->extra = 0; 
				inf->assoc = 0;
			}

			//DWN:TODO inf->image_type = -1;
			// inf->layer_type = -1;
			/* Gimp accepts layers with different alphas in an image. */
			inf->maxval = (1 << inf->bps) - 1;
			inf->minval = 0; 
			if (inf->maxval == 1 && inf->spp == 1)
			{
				inf->grayscale = 1;
				inf->tag.SetFormat(CinePaintTag::FORMAT_GRAY_ENUM); //
				// inf->image_type = GRAY;
				// inf->layer_type = (alpha) ? GRAYA_IMAGE : GRAY_IMAGE;
			}
			else
			{
				switch (inf->photomet) 
				{
				case PHOTOMETRIC_MINISBLACK:
				case PHOTOMETRIC_LOGL:
					inf->grayscale = 1;
					if (inf->bps <= 8) {
						inf->tag = CinePaintTag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_GRAY_ENUM, false); //inf->image_type = GRAY;
					}
					else if (inf->bps == 16
						&& inf->sampleformat == SAMPLEFORMAT_UINT) { /* uint16 */ 
							inf->tag = CinePaintTag(CinePaintTag::PRECISION_U16_ENUM, CinePaintTag::FORMAT_GRAY_ENUM, false); // inf->image_type = U16_GRAY;
						}
					else if (inf->bps == 16
						&& (inf->sampleformat == SAMPLEFORMAT_IEEEFP
						|| inf->sampleformat == SAMPLEFORMAT_VOID)){/* floats16*/
							inf->tag = CinePaintTag(CinePaintTag::PRECISION_FLOAT16_ENUM, CinePaintTag::FORMAT_GRAY_ENUM, false); // image_type = FLOAT16_GRAY; /* TODO */
						}
					else if (inf->bps == 32
						&& (inf->sampleformat == SAMPLEFORMAT_UINT
						|| inf->sampleformat == SAMPLEFORMAT_IEEEFP)){/* float */
							inf->tag = CinePaintTag(CinePaintTag::PRECISION_FLOAT_ENUM, CinePaintTag::FORMAT_GRAY_ENUM, false); // image_type = FLOAT_GRAY;
						}
					else if (inf->bps == 64
						&& (inf->sampleformat == SAMPLEFORMAT_UINT
						|| inf->sampleformat == SAMPLEFORMAT_IEEEFP)){/* float */
							inf->tag = CinePaintTag(CinePaintTag::PRECISION_FLOAT_ENUM, CinePaintTag::FORMAT_GRAY_ENUM, false); // image_type = FLOAT_GRAY;
						}
						break;

				case PHOTOMETRIC_MINISWHITE:
					inf->grayscale = 1;
					if (inf->bps <= 8) 
					{
						inf->tag = CinePaintTag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_GRAY_ENUM, false); //inf->image_type = GRAY;
					}
					else if (inf->bps == 16
						&& inf->sampleformat == SAMPLEFORMAT_UINT) { /* uint16 */
							inf->tag = CinePaintTag(CinePaintTag::PRECISION_U16_ENUM, CinePaintTag::FORMAT_GRAY_ENUM, false); // image_type = U16_GRAY;
						}
					else if (inf->bps == 16
						&& (inf->sampleformat == SAMPLEFORMAT_IEEEFP
						|| inf->sampleformat == SAMPLEFORMAT_VOID)) {/*floats16*/
							inf->tag = CinePaintTag(CinePaintTag::PRECISION_FLOAT16_ENUM, CinePaintTag::FORMAT_GRAY_ENUM, false); // image_type = FLOAT16_GRAY;
						}
					else if (inf->bps == 32
						&& (inf->sampleformat == SAMPLEFORMAT_UINT
						|| inf->sampleformat == SAMPLEFORMAT_IEEEFP)){/* float */
							inf->tag = CinePaintTag(CinePaintTag::PRECISION_FLOAT_ENUM, CinePaintTag::FORMAT_GRAY_ENUM, false); // image_type = FLOAT_GRAY;
						}
					else if (inf->bps == 64
						&& (inf->sampleformat == SAMPLEFORMAT_UINT
						|| inf->sampleformat == SAMPLEFORMAT_IEEEFP)){/* float */
							inf->tag = CinePaintTag(CinePaintTag::PRECISION_FLOAT_ENUM, CinePaintTag::FORMAT_GRAY_ENUM, false); // image_type = FLOAT_GRAY;
						}
						break;

				case PHOTOMETRIC_PALETTE:
					if (inf->alpha)
						printf ("ignoring alpha channel on indexed color image\n");
					if (inf->bps <= 8) {
						/*if (!TIFFGetField (tif, TIFFTAG_COLORMAP,
						&redcolormap,
						&greencolormap,
						&bluecolormap))
						{
						g_print ("error getting colormaps\n");
						gimp_quit ();
						}*/
						inf->numcolors = inf->maxval + 1;
						inf->maxval = 255;
						inf->grayscale = 0;

						/*for (i = 0; i < numcolors; i++)
						{
						redcolormap[i] >>= 8;
						greencolormap[i] >>= 8;
						bluecolormap[i] >>= 8;
						}*/

						if (inf->numcolors > 256) {
							inf->tag = CinePaintTag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false); // image_type = RGB;
						} 
						else 
						{
							inf->tag = CinePaintTag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_INDEXED_ENUM, false); // image_type = INDEXED;
						}
					}
					else if (inf->bps == 16)
						printf("16bit indexed color image not implemented yet\n");
					break;

				case PHOTOMETRIC_RGB:
				case PHOTOMETRIC_LOGLUV:
				case PHOTOMETRIC_CIELAB:
				case PHOTOMETRIC_ICCLAB:
				case PHOTOMETRIC_ITULAB:
				case PHOTOMETRIC_SEPARATED:
					if (inf->photomet == PHOTOMETRIC_SEPARATED)
						printf("separated datas; bps|spp = %d|%d, sampleformat = %d\n", inf->bps, inf->spp, inf->sampleformat);
					else
						printf("coloured datas; bps|spp = %d|%d, sampleformat = %d\n", inf->bps, inf->spp, inf->sampleformat);

					inf->grayscale = 0;
					if (inf->bps <= 8) {
						inf->tag = CinePaintTag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false); //image_type = RGB;
					}
					else if (inf->bps == 16
						&& inf->sampleformat == SAMPLEFORMAT_UINT) { /* uint16  */
							inf->tag = CinePaintTag(CinePaintTag::PRECISION_U16_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false); //image_type = U16_RGB;
						}
					else if (inf->bps == 16
						&& ((inf->sampleformat == SAMPLEFORMAT_IEEEFP)
						|| (inf->sampleformat == SAMPLEFORMAT_VOID)) ) {/*floats16 */
							inf->tag = CinePaintTag(CinePaintTag::PRECISION_FLOAT16_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false); //image_type = FLOAT16_RGB;
						}
					else if (inf->bps == 32
						&& (inf->sampleformat == SAMPLEFORMAT_UINT
						|| inf->sampleformat == SAMPLEFORMAT_IEEEFP)){/* float */
							inf->tag = CinePaintTag(CinePaintTag::PRECISION_FLOAT_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false); //image_type = FLOAT_RGB;
						}
					else if (inf->bps == 64
						&& (inf->sampleformat == SAMPLEFORMAT_UINT
						|| inf->sampleformat == SAMPLEFORMAT_IEEEFP)){/* float */
							inf->tag = CinePaintTag(CinePaintTag::PRECISION_FLOAT_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false); //image_type = FLOAT_RGB;
						}
						break;

				default:
					inf->worst_case = 1;
					printf("photomet = %d\n",inf->photomet);
				}
#if GIMP_MAJOR_VERSION >= 1
				if (inf->photomet == PHOTOMETRIC_SEPARATED
					&& bps <= 16)  /* This case is handled by libtiff. */
					worst_case = 1;
#endif
			}
			printf ("%s:%d IFD %ld: bps = %d\n",__FILE__,__LINE__,
				inf->aktuelles_dir, inf->bps);

			if (inf->alpha)
				inf->tag.SetAlpha(true);
			else
				inf->tag.SetAlpha(false);

			if (inf->worst_case) {
				printf ("IFD: %ld fall back\n",inf->aktuelles_dir);
				inf->tag = CinePaintTag(CinePaintTag::PRECISION_U8_ENUM, CinePaintTag::FORMAT_RGB_ENUM, false);// image_type = RGB_IMAGE;
				// layer_type = RGBA_IMAGE;
				if (!(inf->filetype & FILETYPE_REDUCEDIMAGE))
					m_infoList.front()->worst_case_nonreduced = 1;
			} else

				TIFFGetFieldDefaulted(tif,TIFFTAG_RESOLUTIONUNIT, &inf->res_unit);

			if (!TIFFGetField (tif, TIFFTAG_IMAGEWIDTH, &inf->cols))
				inf->cols = 0;
			if (!TIFFGetField (tif, TIFFTAG_IMAGELENGTH, &inf->rows))
				inf->rows = 0;
			if (!TIFFGetField (tif, TIFFTAG_XPOSITION, &inf->xpos))
				inf->xpos = (float)0.0;
			if (!TIFFGetField (tif, TIFFTAG_YPOSITION, &inf->ypos))
				inf->ypos = (float)0.0;
			if (!TIFFGetField (tif, TIFFTAG_XRESOLUTION, &inf->xres))
				inf->xres = (float)0.0;
			if (!TIFFGetField (tif, TIFFTAG_YRESOLUTION, &inf->yres))
				inf->yres = (float)0.0;

			inf->image_width = inf->cols;
			inf->image_height = inf->rows;

			/* yres but no xres */
			if ( ( inf->xres == (float)0.0 )
				&& ( inf->yres != (float)0.0 ) ) {
					printf("TIFF warning: no x resolution info, assuming same as y\n");
					inf->xres = inf->yres;
					/* xres but no yres */
				} else if ( ( inf->yres == (float)0.0 )
					&& ( inf->xres != (float)0.0 ) ) { 
						printf("TIFF warning: no y resolution info, assuming same as x\n");
						inf->yres = inf->xres;
					} else {
						/* no res tags => we assume we have no resolution info, so we
						* don't care.  Older versions of this plugin used to write files
						* with no resolution tags at all. */

						/* If it is invalid, instead of forcing 72dpi, do not set the resolution 
						* at all. Gimp will then use the default set by the user */

						/*xres = 72.0;
						yres = 72.0;*/
					}  

					/* position information */
					inf->max_offx = 0;
					inf->max_offy = 0;
					inf->offx = (int)ROUND(inf->xpos * inf->xres);
					inf->offy = (int)ROUND(inf->ypos * inf->yres);

					if (!inf->worst_case) {
						/* size and resolution */
						if ( inf->offx > m_infoList.front()->max_offx ) {
							m_infoList.front()->max_offx = inf->offx;
						}
						if ( inf->offy > m_infoList.front()->max_offy ) {
							m_infoList.front()->max_offy = inf->offy;
						}
						/* bit depth */
						if (inf->bps > m_infoList.front()->bps_selected) {
							// DWM:TODO m_infoList.front()->im_t_sel = image_type;
							m_infoList.front()->bps_selected = inf->bps;
						}
					}

					{     /* string informations */
						int   len = 0;
						char* tiff_ptr = NULL;

						get_text_field (tif, tiff_ptr, TIFFTAG_IMAGEDESCRIPTION, inf->img_desc);
						get_text_field (tif, tiff_ptr, TIFFTAG_PAGENAME, inf->pagename);
						get_text_field (tif, tiff_ptr, TIFFTAG_SOFTWARE, inf->software);
						if (inf->pagename == NULL) {
							inf->pagename = new char[32];
							sprintf (inf->pagename, "%s", "layer" );
							sprintf (inf->pagename, "%s # %ld", inf->pagename, inf->aktuelles_dir );
						}

						/* ICC profile */
						if (TIFFGetField (tif, TIFFTAG_ICCPROFILE, &inf->profile_size,
							&tiff_ptr)) {
								inf->icc_profile = new char[inf->profile_size];
								memcpy (inf->icc_profile, tiff_ptr, inf->profile_size);
								if (inf->profile_size > 0) {
									cmsHPROFILE hIn=NULL;

									hIn = cmsOpenProfileFromMem (inf->icc_profile, inf->profile_size);
									if (hIn) {
										inf->icc_profile_info = getProfileInfo (hIn);
										printf ("%s\n",inf->icc_profile_info);
										cmsCloseProfile (hIn);
#ifdef DEBUG
										saveProfileToFile ("/tmp/cinepaint_tiff_load.icc", inf->icc_profile,
											inf->profile_size);
										printf ("profile saved to /tmp/cinepaint_tiff_load.icc\n")
#endif
									} else {
										inf->profile_size = 0;
										inf->icc_profile = NULL;
									}
								}
							} else {
								inf->profile_size = 0;
								inf->icc_profile = NULL;
							}
					}
					/* searching for viewport position and size */
					inf->viewport_offx = 0;
					inf->viewport_offy = 0;
					if (  inf->pagename != NULL
						&& (strcmp (inf->pagename, "viewport") == 0) /* test for our own */
						&& (inf->filetype & FILETYPE_MASK) )
					{  /*viewport definitions */
						m_infoList.front()->image_height = 
							( inf->rows * m_infoList.front()->yres / inf->yres);
						m_infoList.front()->image_width = 
							( inf->cols * m_infoList.front()->xres / inf->xres);
						m_infoList.front()->viewport_offx = inf->offx;
						m_infoList.front()->viewport_offy = inf->offy;
					} else if (!inf->worst_case) {
												/* show all IFD s */
												/* actual IFD more bottom? */
												if ( ( inf->rows + (inf->ypos * inf->yres) ) >  m_infoList.front()->image_height ) 
													m_infoList.front()->image_height = inf->rows + (inf->ypos * inf->yres);
												/* actual IFD more right? */
												if ( ( inf->cols + (inf->xpos * inf->xres) ) >  m_infoList.front()->image_width ) 
													m_infoList.front()->image_width = inf->cols + (inf->xpos * inf->xres);
											}

											if (TIFFGetField (tif, TIFFTAG_PAGENUMBER,
												&inf->pagenumber, &inf->pagescount)) {
													if (strcmp (inf->pagename, "viewport") != 0)
														m_infoList.front()->pagecounting = 1;  /* IFDs are counted */
													inf->pagecounted = 1;          /* counted IFDs shall be visible */
												} else {
													inf->pagenumber = 0;
													inf->pagescount = 0;
													inf->pagecounted = 0;          /* if pagecounting==1 -> invisible */
												}

												if (TIFFGetField (tif, TIFFTAG_ORIENTATION, &inf->orientation))
													inf->orientation = (int)inf->orientation;
												else
													inf->orientation = -1;

												return 1;
}

//int TiffImageInfo::get_image_info(  gint32           image_ID,
//								  gint32           drawable_ID,
//								  ImageInfo       *info)
//{
//	gint nlayers;
//	long dircount; 
//	gint *layers;
//
//	nlayers = -1;
//
//	/* Die Zahl der Layer / number of layers */
//	layers = gimp_image_get_layers (image_ID, &nlayers);
//	/*g_message ("nlayers = %d", nlayers); */
//
//	if ( nlayers == -1 ) {
//		g_message("no image data to save");
//		return FALSE;
//	}
//
//
//	{
//		aktuelles_dir = -1; /* starting on the top */
//		pagecount = -1;
//
//		for (dircount=0 ; dircount < nlayers ; dircount++)
//		{
//			drawable_ID = layers[dircount];
//
//			if ( dircount == 0 )
//			{
//				top  = info;
//				parent  = NULL;
//				next  = NULL;
//
//				init_info ( info);
//				top->image_height = rows;		/* TODO needed ? */
//				top->image_width = cols;
//
//				pagecounting = 0;
//
//			} else {
//				m_new (next, ImageInfo, 1, return FALSE)
//
//					next->top = top;
//				next->parent = info;
//				next->next = NULL;
//
//				info = next;
//				init_info ( info);
//
//			}
//
//			if (!(get_layer_info (image_ID, drawable_ID, info)))
//			{
//				g_message ("get_layer_info(...) failed");
//				return FALSE;
//			}
//
//			aktuelles_dir = dircount;
//			pagecount = nlayers;
//
//		}
//	}
//
//	info = top;
//
//	for ( dircount=0 ; dircount < nlayers; dircount++) {
//
//		if (dircount > 0)
//			info = next;
//
//		if (aktuelles_dir != dircount) {
//			g_print("%s:%d %s() error in counting layers",__FILE__,__LINE__,__func__);
//			return FALSE;
//		}
//
//		/* Handle offsets and resolution */ 
//		if ( offx != top->max_offx ) {
//			xpos = (gfloat)(offx - top->max_offx) / xres;
//		}
//		if ( offy != top->max_offy ) {
//			ypos = (gfloat)(offy - top->max_offy) / yres;
//		}
//	}
//
//	info = top;
//
//	m_free (layers)
//		return 1;
//}


int TiffImageInfo::photometFromICC (char* color_space_name, ImageInfo *inf)
{
	int BitsPerSample = inf->bps;
	int SamplesPerPixel = inf->spp;
	int photomet = inf->photomet, ink=0;
	int use_icc_profile = inf->save_vals.use_icc_profile;


	if (strcmp (color_space_name, "Gray") == 0) {/*PT_GRAY */
		photomet = PHOTOMETRIC_MINISBLACK;
	} else
	if (strcmp (color_space_name, "Rgb") == 0) {/*PT_RGB */
		if (SamplesPerPixel >= 3)
			photomet = PHOTOMETRIC_RGB;
		else {
			printf ("%s:%d %d SamplesPerPixel dont matches RGB",
				__FILE__,__LINE__,SamplesPerPixel);
			use_icc_profile = FALSE;
		}
	} else
	if (strcmp (color_space_name, "Cmy") == 0) {/*PT_CMY */
		if (SamplesPerPixel >= 3)
			photomet = PHOTOMETRIC_SEPARATED;
		else {
			printf ("%s:%d %d SamplesPerPixel dont matches CMY",
				__FILE__,__LINE__,SamplesPerPixel);
			use_icc_profile = FALSE;
		}
		ink = INKSET_MULTIINK;
			/*break; */
	} else
	if (strcmp (color_space_name, "Cmyk") == 0) { /*CMYK */
		if (SamplesPerPixel >= 4) {
			photomet = PHOTOMETRIC_SEPARATED;
			ink = INKSET_CMYK;
		} else {
			printf ("%s:%d %d SamplesPerPixel dont matches CMYK",
				__FILE__,__LINE__,SamplesPerPixel);
			if (SamplesPerPixel == 3)
				photomet = PHOTOMETRIC_SEPARATED;
			use_icc_profile = FALSE;
		}

			/*break; */
	} else
	if (strcmp (color_space_name, "Lab") == 0) { /*PT_Lab */
		if (SamplesPerPixel >= 3) {
			if (BitsPerSample <= 16) 
				photomet = PHOTOMETRIC_ICCLAB;
			else
				photomet = PHOTOMETRIC_CIELAB;
		} else {
			printf ("%s:%d %d SamplesPerPixel dont matches CIE*Lab",
				__FILE__,__LINE__,SamplesPerPixel);
			use_icc_profile = FALSE;
		}

			/* break; */
	} else
	if (strcmp (color_space_name, "Lab") == 0) { /*PT_XYZ */
		if (BitsPerSample == 32 && SamplesPerPixel >= 3) {
			photomet = PHOTOMETRIC_LOGLUV;
			inf->save_vals.compression = COMPRESSION_SGILOG;
		} else {
			printf ("%s:%d %d XYZ is saveable in float bit depth only",
				__FILE__,__LINE__,SamplesPerPixel);
		}

			/*break; */
	}
	inf->photomet = photomet;
	inf->save_vals.use_icc_profile = use_icc_profile;

	return ink;
}

//int TiffImageInfo::get_layer_info               (  gint32           image_ID,
//							  gint32           drawable_ID,
//							  ImageInfo	*info)
//{
//	guchar *cmap;
//	int colors, i;
//	GimpDrawable *drawable = NULL;
//	GimpImageType drawable_type;
//
//	/* Ist unser Layer sichbar? / is this layer visible? */
//	visible = gimp_layer_get_visible( drawable_ID);
//
//	/* switch for TIFFTAG_SAMPLEFORMAT ; default = 1 = uint */
//	sampleformat = SAMPLEFORMAT_UINT; 
//
//
//	drawable = gimp_drawable_get (drawable_ID);
//	drawable_type = gimp_drawable_type (drawable_ID);
//
//	cols = drawable->width;
//	rows = drawable->height;
//
//	planar = PLANARCONFIG_CONTIG;
//	orientation = ORIENTATION_TOPLEFT;
//
//	switch (drawable_type)
//	{
//	case RGB_IMAGE:
//		spp = 3;
//		bps = 8;
//		photomet = PHOTOMETRIC_RGB;
//		sampleformat = SAMPLEFORMAT_UINT; 
//		alpha = 0;
//		break;
//	case GRAY_IMAGE:
//		spp = 1;
//		bps = 8;
//		photomet = PHOTOMETRIC_MINISBLACK;
//		sampleformat = SAMPLEFORMAT_UINT; 
//		alpha = 0;
//		break;
//	case RGBA_IMAGE:
//		spp = 4;
//		bps = 8;
//		photomet = PHOTOMETRIC_RGB;
//		sampleformat = SAMPLEFORMAT_UINT; 
//		alpha = 1;
//		break;
//	case GRAYA_IMAGE:
//		spp = 2;
//		bps = 8;
//		photomet = PHOTOMETRIC_MINISBLACK;
//		sampleformat = SAMPLEFORMAT_UINT; 
//		alpha = 1;
//		break;
//	case INDEXED_IMAGE:
//		spp = 1;
//		bps = 8;
//		photomet = PHOTOMETRIC_PALETTE;
//		sampleformat = SAMPLEFORMAT_UINT; 
//		alpha = 0;
//
//		cmap = gimp_image_get_cmap (image_ID, &colors);
//		m_new (red, unsigned short,colors, return FALSE)
//			m_new (grn, unsigned short,colors, return FALSE)
//			m_new (blu, unsigned short,colors, return FALSE)
//
//			for (i = 0; i < colors; i++)
//			{
//				red[i] = *cmap << 8; cmap++;
//				grn[i] = *cmap << 8; cmap++;
//				blu[i] = *cmap << 8; cmap++;
//			}
//			break;
//	case INDEXEDA_IMAGE:
//		g_print ("tiff save_image: can't save INDEXEDA_IMAGE\n");
//		m_free (drawable)
//			return 0;
//#if GIMP_MAJOR_VERSION < 1
//	case U16_RGB_IMAGE:
//		spp = 3;
//		bps = 16;
//		photomet = PHOTOMETRIC_ICCLAB;
//		photomet = PHOTOMETRIC_RGB;
//		sampleformat = SAMPLEFORMAT_UINT; 
//		alpha = 0;
//		break;
//	case U16_GRAY_IMAGE:
//		spp = 1;
//		bps = 16;
//		photomet = PHOTOMETRIC_MINISBLACK;
//		sampleformat = SAMPLEFORMAT_UINT; 
//		alpha = 0;
//		break;
//	case U16_RGBA_IMAGE:
//		spp = 4;
//		bps = 16;
//		sampleformat = SAMPLEFORMAT_UINT; 
//		photomet = PHOTOMETRIC_RGB;
//		alpha = 1;
//		break;
//	case U16_GRAYA_IMAGE:
//		spp = 2;
//		bps = 16;
//		photomet = PHOTOMETRIC_MINISBLACK;
//		alpha = 1;
//		break;
//	case U16_INDEXED_IMAGE:
//		g_print ("tiff save_image: U16_INDEXED_IMAGE save not implemented\n");
//		m_free (drawable)
//			return 0;
//		/* 
//		spp = 1;
//		bps = 16;
//		photomet = PHOTOMETRIC_PALETTE;
//		alpha = 0;
//		cmap = gimp_image_get_cmap (image_ID, &colors);
//
//		for (i = 0; i < colors; i++)
//		{
//		red[i] = *cmap++ << 8;
//		grn[i] = *cmap++ << 8;
//		blu[i] = *cmap++ << 8;
//		}
//		*/
//		break;
//	case U16_INDEXEDA_IMAGE:
//		g_print ("tiff save_image: can't save U16_INDEXEDA_IMAGE\n");
//		m_free (drawable)
//			return 0;
//	case FLOAT16_GRAY_IMAGE:
//		g_message("saving 16bit float data is not standard\nbetter switch to 32bit float format");
//		spp = 1;
//		bps = 16;
//		sampleformat = SAMPLEFORMAT_VOID;
//		photomet = PHOTOMETRIC_MINISBLACK;
//		alpha = 0;
//		break;
//	case FLOAT16_RGB_IMAGE:
//		g_message("saving 16bit float data is not standard\nbetter switch to 32bit float format");
//		spp = 3;
//		bps = 16;
//		sampleformat = SAMPLEFORMAT_VOID;
//		photomet = PHOTOMETRIC_RGB;
//		alpha = 0;
//		break;
//	case FLOAT16_GRAYA_IMAGE:
//		g_message("saving 16bit float data is not standard\nbetter switch to 32bit float format");
//		bps = 16;
//		alpha = 1;
//		spp = 1 + alpha;	/* 2 TODO */
//		sampleformat = SAMPLEFORMAT_VOID;
//		photomet = PHOTOMETRIC_MINISBLACK;
//		break;
//	case FLOAT16_RGBA_IMAGE:
//		g_message("saving 16bit float data is not standard\nbetter switch to 32bit float format");
//		bps = 16;
//		alpha = 1;
//		spp = 3 + alpha; /* 4 */
//		sampleformat = SAMPLEFORMAT_VOID;
//		photomet = PHOTOMETRIC_RGB;
//		break;
//	case FLOAT_RGB_IMAGE:
//		spp = 3;
//		bps = 32;
//		sampleformat = SAMPLEFORMAT_IEEEFP;
//		photomet = PHOTOMETRIC_RGB;
//		alpha = 0;
//		break;
//	case FLOAT_GRAY_IMAGE:
//		spp = 1;
//		bps = 32;
//		sampleformat = SAMPLEFORMAT_IEEEFP;
//		photomet = PHOTOMETRIC_MINISBLACK;
//		alpha = 0;
//		break;
//	case FLOAT_RGBA_IMAGE:
//		spp = 4;
//		bps = 32;
//		sampleformat = SAMPLEFORMAT_IEEEFP;
//		photomet = PHOTOMETRIC_RGB;
//		alpha = 1;
//		break;
//	case FLOAT_GRAYA_IMAGE:
//		spp = 2;
//		bps = 32;
//		sampleformat = SAMPLEFORMAT_IEEEFP;
//		photomet = PHOTOMETRIC_MINISBLACK;
//		alpha = 1;
//		break;
//#endif /*  */
//	default:
//		g_print ("Can't save this image type\n");
//		m_free (drawable)
//			return 0;
//	}
//
//#ifndef GIMP_MAJOR_VERSION
//	if (gimp_image_has_icc_profile(image_ID)) {
//		int size, ink;
//		cmsHPROFILE profile=NULL;
//		char* mem_profile=NULL;
//
//		icc_profile = NULL;
//		profile_size = 0;
//		icc_profile_info = NULL;
//		mem_profile = gimp_image_get_icc_profile_by_mem(image_ID, &size);
//		if (mem_profile && size) {
//			profile = cmsOpenProfileFromMem (mem_profile, size);
//			sprintf (color_space_name_,
//				gimp_image_get_icc_profile_color_space_name (image_ID));
//			DBG (color_space_name_)
//
//				ink = photometFromICC(color_space_name_, info);
//
//			profile_size = size;
//			icc_profile = mem_profile;
//			icc_profile_info = calloc (sizeof (char),
//				strlen(gimp_image_get_icc_profile_info(image_ID)));
//			sprintf (icc_profile_info,
//				gimp_image_get_icc_profile_info(image_ID));
//			memcpy (&colorspace[0], color_space_name_, 4);
//			colorspace[4] = '\000';
//			DBG (icc_profile_info)
//		} else {
//			printf ("%s:%d could not get an profile\n",__FILE__,__LINE__);
//			save_vals.use_icc_profile = FALSE;
//		}
//		if (strcmp (gimp_image_get_icc_profile_color_space_name(image_ID) , "XYZ")
//			== 0) {
//				if (bps == 32 && spp >= 3) {
//					photomet = PHOTOMETRIC_LOGLUV;
//					save_vals.compression = COMPRESSION_SGILOG;
//					sprintf (color_space_name_, "XYZ");
//				} else {
//					g_warning ("%s:%d bps:%d spp:%d XYZ is saveable in float bit depth only",
//						__FILE__,__LINE__,bps,spp);
//				}
//			}
//
//#ifdef DEBUG
//			printf ("%s:%d photomet = %d\n",__FILE__,__LINE__,photomet);
//#endif
//	} else
//		save_vals.use_icc_profile = -1;
//#ifdef DEBUG
//	printf ("%s:%d save_vals.compression = %d\n",__FILE__,__LINE__,
//		save_vals.compression);
//#endif
//#endif
//
//	if (alpha) {
//		if ( (photomet == PHOTOMETRIC_MINISWHITE
//			|| photomet == PHOTOMETRIC_MINISBLACK
//			|| photomet == PHOTOMETRIC_RGB
//			|| photomet == PHOTOMETRIC_YCBCR )
//			&& save_vals.premultiply == EXTRASAMPLE_UNSPECIFIED )
//		{
//			save_vals.premultiply = EXTRASAMPLE_ASSOCALPHA;
//		} else if (photomet == PHOTOMETRIC_SEPARATED && spp <= 4) {
//			save_vals.premultiply = EXTRASAMPLE_UNSPECIFIED;
//		} else if ( save_vals.premultiply == EXTRASAMPLE_UNSPECIFIED ) {
//			save_vals.premultiply = EXTRASAMPLE_UNASSALPHA;
//		}
//	} else {
//		save_vals.premultiply = EXTRASAMPLE_UNSPECIFIED;
//	}
//
//
//
//#if GIMP_MAJOR_VERSION >= 1 && GIMP_MINOR_VERSION >= 2
//	/* resolution fields */ 
//	{
//		GimpUnit unit;
//		gfloat   factor;
//
//		res_unit = RESUNIT_INCH;
//
//		gimp_image_get_resolution (orig_image_ID, &xres, &yres);
//		unit = gimp_image_get_unit (orig_image_ID);
//		factor = gimp_unit_get_factor (unit);
//
//		/*  if we have a metric unit, save the resolution as centimeters
//		*/
//		if ((ABS (factor - 0.0254) < 1e-5) ||  /* m  */
//			(ABS (factor - 0.254) < 1e-5) ||   /* dm */
//			(ABS (factor - 2.54) < 1e-5) ||    /* cm */
//			(ABS (factor - 25.4) < 1e-5))      /* mm */
//		{
//			res_unit = RESUNIT_CENTIMETER;
//			xres /= 2.54;
//			yres /= 2.54;
//		}
//
//		if (xres <= 1e-5 && yres <= 1e-5)
//		{
//			xres = 72.0;
//			yres = 72.0;
//		}
//	}
//#endif
//
//
//	/* Handle offsets */ 
//	max_offx = 0;
//	max_offy = 0;
//	gimp_drawable_offsets( drawable_ID, &offx, &offy);
//	if ( offx < top->max_offx )
//		top->max_offx = offx;
//	if ( offy < top->max_offy )
//		top->max_offy = offy;
//
//
//	pagename = gimp_layer_get_name (drawable_ID);
//	gimp_drawable_detach (drawable);
//
//	return 1;
//}


/* --- variable function --- */

void TiffImageInfo::init_info( ImageInfo *inf )
{
	inf->photomet=0;
	inf->spp=0;
	inf->bps=0;
	inf->bps_selected=0;
	inf->save_vals.compression = tsvals_.compression;
	inf->save_vals.fillorder = tsvals_.fillorder;
	inf->save_vals.premultiply = tsvals_.premultiply;
	inf->save_vals.use_icc_profile = tsvals_.use_icc_profile;

	inf->cols=0, inf->rows=0;
	inf->offx = 0;
	inf->offy = 0;
	inf->max_offx = 0;
	inf->max_offy = 0;
	inf->xres = (float)72.0;
	inf->yres = (float)72.0; /* resolution */
	inf->xpos = (float)0.0;
	inf->ypos = (float)0.0;  /* relative position */
	inf->visible=0;
	inf->stonits=0.0;  /*Test: 119.333333; */
	inf->logDataFMT = SGILOGDATAFMT;
	inf->profile_size=0;

//	inf->channel = NULL;
	inf->software = NULL;
	inf->img_desc = NULL;
	inf->pagename = NULL;
	inf->filename = NULL;
	inf->extra_types = NULL;
	inf->icc_profile = NULL;
	inf->icc_profile_info = NULL;
	inf->colorspace[0] = '\000';
}


char* TiffImageInfo::print_info( ImageInfo *inf )
{
	static char text[1024] = {0};
	char* ptr = (char*)&text;
	int i;

	for (i=0; i<1024; i++)
		text[i] = 0;

	txt_add (ptr, "photomet", inf->photomet );
	txt_add (ptr,  "spp", inf->spp );
	txt_add (ptr,  "bps", inf->bps );
	txt_add ( ptr, "top->bps_selected", m_infoList.front()->bps_selected );
	txt_add (ptr,  "rowsperstrip", inf->rowsperstrip);

	txt_add (ptr,  "visible", inf->visible);
	//txt_add (ptr,  "image_type", inf->image_type);
	txt_add (ptr,  "im_t_sel", inf->im_t_sel);
	txt_add (ptr,  "layer_type", inf->layer_type);
	txt_add (ptr,  "extra", inf->extra);
	txt_add (ptr,  "alpha", inf->alpha);
	txt_add (ptr,  "assoc", inf->assoc);
	txt_add (ptr,  "worst_case", inf->worst_case);
	txt_add (ptr,  "filetype", inf->filetype);

	txt_add (ptr, "save_vals.compression", inf->save_vals.compression );
	txt_add (ptr, "save_vals.fillorder", inf->save_vals.fillorder);
	txt_add (ptr, "save_vals.premultiply", inf->save_vals.premultiply );

	txt_add (ptr, "cols", inf->cols );
	txt_add (ptr, "rows", inf->rows);
	txt_add (ptr, "image_width", inf->image_width );
	txt_add (ptr, "image_height", inf->image_height );
	txt_add (ptr, "offx", inf->offx);
	txt_add (ptr, "offy", inf->offy );
	txt_add (ptr, "max_offx", inf->max_offx);
	txt_add (ptr, "max_offy", inf->max_offy );
	txt_add (ptr, "xres", inf->xres );
	txt_add (ptr, "yres", inf->yres); /* resolution */
	txt_add (ptr, "xpos", inf->xpos );
	txt_add (ptr, "ypos", inf->ypos);  /* relative position */
	txt_add (ptr, "planar", (int)inf->planar );
	txt_add (ptr, "grayscale", inf->grayscale );
	txt_add (ptr, "minval", inf->minval);
	txt_add (ptr, "maxval", inf->maxval);
	txt_add (ptr, "numcolors", inf->numcolors);
	txt_add (ptr, "sampleformat", (int)inf->sampleformat);
	txt_add (ptr, "pagecount", (int)inf->pagecount);
	txt_add (ptr, "aktuelles_dir", (int)inf->aktuelles_dir);
	txt_add (ptr, "pagenumber", (int)inf->pagenumber );
	txt_add (ptr, "stonits", (float)inf->stonits);
	txt_add (ptr, "logDataFMT", inf->logDataFMT );

	/*txt_add ( channel ) */
	txt_add (ptr, "filename", inf->filename );
	txt_add (ptr, "software", inf->software );
	txt_add (ptr, "img_desc", inf->img_desc );
	txt_add (ptr, "pagename", inf->pagename );
	/*txt_add ( extra_types ) */
	/*txt_add ( icc_profile ) */
	txt_add (ptr, "profile_size", (int)inf->profile_size);
	txt_add (ptr, "icc_profile_info", inf->icc_profile_info );
	txt_add (ptr, "&colorspace[0]", &inf->colorspace[0] );

	sprintf (ptr , "%s-------------\n", ptr);

	return ptr;
}


int TiffImageInfo::get_text_field(TIFF *tif, char* tiff_ptr, int TIFFTAG, char* target)
{
	int len;
	TIFFGetField (tif, TIFFTAG, &tiff_ptr); 
	if (tiff_ptr != NULL) 
		len = strlen (tiff_ptr); 
	else 
		len = 0; 
	if (len > 0) { 
	target = new char[len + 1];
	sprintf (target, "%s", tiff_ptr); 
//	g_print (#target ": %s   %d\n",target ,(int)target); 
	} else { 
	target = NULL; 
	} 
	if (tiff_ptr != NULL) tiff_ptr[0] = '\000';
	return len;
}

void TiffImageInfo::txt_add( char* ptr, char* name, int var )
{
	sprintf (ptr, "%s %s = %d\n", ptr, name, (int)var);
}
void TiffImageInfo::txt_add( char* ptr, char* name, float var )
{
	sprintf (ptr, "%s %s  = %.4f\n", ptr,name, (float)var);
}
void TiffImageInfo::txt_add( char* ptr, char* name, char* var )
{
	sprintf (ptr, "%s %s  = %.s\n", ptr, name, var);
}