#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "util.h"
#include "draw.h"
/*
  The parameters for clipping give the first pixels that should not be drawn.
  ->clip to [0,w[x[0,h[
  if w==-1 then there is no clipping
*/

/*
  Draw a clipped line.
*/
void line(int x1,int y1,int x2,int y2,int w,int h,PixelSetter *pset,void *data){
  if(x1>x2){
    int tmp;
    tmp=x1;x1=x2;x2=tmp;
    tmp=y1;y1=y2;y2=tmp;
  }
  enum {UP=1,DOWN=2,RIGHT=4,LEFT=8};
  if(w>0)
    for(;;) {
      int oc1=0,oc2=0;
      if(x1<0) oc1|=LEFT; else if(x1>=w) oc1|=RIGHT;
      if(x2<0) oc2|=LEFT; else if(x2>=w) oc2|=RIGHT;
      if(y1<0) oc1|=UP; else if(y1>=h) oc1|=DOWN;
      if(y2<0) oc2|=UP; else if(y2>=h) oc2|=DOWN;
      if(!(oc2|oc1))
	break; //done clipping
      else if(oc2&oc1)
	return; //line is not on screen
      if(oc1&DOWN){
	x1+=(x2-x1)*(h-1-y1)/(y2-y1);
	y1=h-1;
      }else if(oc2&DOWN){
	x2=x1+(x2-x1)*(h-1-y1)/(y2-y1);
	y2=h-1;
      }else if(oc1&UP){
	x1+=(x1-x2)*y1/(y2-y1);
	y1=0;
      }else if(oc2&UP){
	x2=x1+(x1-x2)*y1/(y2-y1);
	y2=0;
      }else if(oc1&RIGHT){
	y1+=(y2-y1)*(w-1-x1)/(x2-x1);
	x1=w-1;
      }else if(oc2&RIGHT){
	y2=y1+(y2-y1)*(w-1-x1)/(x2-x1);
	x2=w-1;
      }else if(oc1&LEFT){
	y1+=(y1-y2)*x1/(x2-x1);
	x1=0;
      }else if(oc2&LEFT){
	y2=y1+(y1-y2)*x1/(x2-x1);
	x2=0;
      }
    }
  
  //draw the line(Bresenham)
  int ya=1,yend=y2;
  int x=x1,y=y1;
  if(y1>y2){    
    ya=-1;;
    int tmp=y1;y1=y2;y2=tmp;
  }
  int dx=x2-x1,dy=y2-y1;
  pset(x,y,data);
  if(dx>dy){
    int d=dy+dy-dx;
    int de=dy<<1,dne=(dy-dx)<<1;
    while(x!=x2){
      if(d<=0){
	d+=de;
      }else{
	d+=dne;
	y+=ya;
      }
      x++;
      pset(x,y,data);
    }
  }else{
    int d=dx+dx-dy;
    int de=dx<<1,dne=(dx-dy)<<1;
    y2+=ya;
    while(y!=yend){
      if(d<=0){
	d+=de;
      }else{
	d+=dne;
	x++;
      }
      y+=ya;
      pset(x,y,data);
    }
  }
}
///////////////////////////////////////////////////////////////////////////////
/*
  For drawing circles
*/
static inline void set8pixels(int x,int y,int xc,int yc,int quadrants,
			      PixelSetter *p,void *data){
  if(quadrants&  1) p(xc+x,yc+y,data);
  if(quadrants&  2) p(xc+y,yc+x,data);
  if(quadrants&  4) p(xc+x,yc-y,data);
  if(quadrants&  8) p(xc+y,yc-x,data);
  if(quadrants& 16) p(xc-x,yc+y,data);
  if(quadrants& 32) p(xc-y,yc+x,data);
  if(quadrants& 64) p(xc-x,yc-y,data);
  if(quadrants&128) p(xc-y,yc-x,data);
}

/*

  draw a circle, center (xc,yc), radius r. There is no checking if the
  circle lies within a bouning box.Make quadrants==255 for complete circle,
  or set any of the lower 8 bits to draw parts of the circle

*/
void circle(int xc,int yc,int r,int quadrants,
	    PixelSetter *p,void *data){
  int x=0,y=r,d=1-r,de=3,dse=5-r-r;
  set8pixels(x,y,xc,yc,quadrants,p,data);
  while(y>x){
    if(d<0){
      d+=de;
      de+=2;
      dse+=2;
    }else{
      d+=dse;
      de+=2;
      dse+=4;
      y--;
    }
    x++;
    set8pixels(x,y,xc,yc,quadrants,p,data);
  }      
}

/*
  Fill the region starting at (x,y), limited by the rectangle [0,w[x[0,h[
  test(xx,yy,data) says if pixel (xx,yy) belongs to the region.
  slice(x1,x2,yy,data) draws a horizontal line, and marks all the pixels in
  it as not belonging to the region.
  Do not use the returned value.
*/
int fill(int x,int y,int w,int h,PixelTester *test,void *data0,
	 SliceSetter *slice,void *data1){
  //find the bounds of this slice, and fill it
  int x1=x,x2=x;
  while(x1>=0&&test(x1,y,data0))
    x1--;
  x1++;
  while(x2<w&&test(x2,y,data0))
    x2++;
  x2--;  
  slice(x1,x2,y,data1);
  
  //scan the upper region
  if(y>0){
    y--;
    for(int xx=x1;xx<=x2;xx++)
      if(test(xx,y,data0))
	xx=fill(xx,y,w,h,test,data0,slice,data1)+1;
    y++;
  }
  //scan the lower region
  y++;
  if(y<h){
    for(int xx=x1;xx<=x2;xx++)
      if(test(xx,y,data0))
	xx=fill(xx,y,w,h,test,data0,slice,data1)+1;
  }
  y--;
  return x2;
 
}
/*
  General conic drawing algorithm
*/
static int DIAGx[] = {1,  1, -1, -1, -1, -1,  1,  1};
static int DIAGy[] = {1,  1,  1,  1, -1, -1, -1, -1};
static int SIDEx[] = {1,  0,  0, -1, -1,  0,  0,  1};
static int SIDEy[] = {0,  1,  1,  0,  0, -1, -1,  0};
static int dval[8][5]={{4,2,1,4,2},
		       {1,2,4,1,4},
		       {1,-2,4,-2,4},
		       {4,-2,1,-4,2},
		       {4,2,1,-4,-2},
		       {1,2,4,-2,-4},
		       {1,-2,4,2,-4},
		       {4,-2,1,4,-2}};
static int uval[8][5]={{2,1,0,2,0},
		       {0,1,2,0,2},
		       {0,-1,2,0,2},
		       {2,-1,0,-2,0},
		       {2,1,0,-2,0},
		       {0,1,2,0,-2},
		       {0,-1,2,0,-2},
		       {2,-1,0,2,0}};
static int vval[8][2]={{0,1},{1,0},{-1,0},{0,1},{0,-1},{-1,0},{1,0},{0,-1}};

#ifndef _WIN32
inline static int64 abs(int64 a){
  return a>0?a:-a;
}
#endif

int getoctant(int64 gx, int64 gy){
  const int upper = abs(gx)>abs(gy);
  if (gx>=0)
    return gy>=0 ? 3-upper:upper;
  else
    return gy>0 ? 4+upper:7-upper;
}

void conic(int xs, int ys, int xe, int ye,
	  int64 A, int64 B, int64 C, int64 D, int64 E, int64 F,
	   PixelSetter *p,void *data){
  
  // Translate start point to origin...
  F+=A*xs*xs+B*xs*ys+C*ys*ys+D*xs+E*ys;
  D+=2*A*xs+B*ys;
  E+=B*xs+2*C*ys;
  
  int octant=getoctant(D,E);  
  int dxS=SIDEx[octant]; 
  int dyS=SIDEy[octant]; 
  int dxD=DIAGx[octant];
  int dyD=DIAGy[octant];
  int64 d=(dval[octant][0]*A+dval[octant][1]*B+dval[octant][2]*C+
     dval[octant][3]*D+dval[octant][4]*E)/4+F;
  int64 u=(uval[octant][0]*A+uval[octant][1]*B+uval[octant][2]*C+
     uval[octant][3]*D+uval[octant][4]*E)/2;
  int64 v=u+vval[octant][0]*D+vval[octant][1]*E;
  
  int k1sign=dyS*dyD;
  int64 k1=2*(A+k1sign*(C-A));
  int Bsign=dxD*dyD;
  int64 k2=k1+Bsign*B;
  int64 k3=2*(A+C+Bsign*B);

  int gxe=xe-xs;
  int gye=ye-ys;
  int gx=int(2*A*gxe+B*gye+D);
  int gy=int(B*gxe+2*C*gye+E);
  
  int octantcount=getoctant(gx,gy)-octant;
  if(octantcount<=0)
    octantcount=octantcount+8;  
  octantcount++;
  int x=xs;
  int y=ys;
  
  while(octantcount>0){    
    if((octant&1)==0){
      while(2*v<=k2){
	p(x,y,data);
	if(d<0){
	  x+=dxS;
	  y+=dyS;
	  u+=k1;
	  v+=k2;
	  d+=u;
	}else{
	  x+=dxD;
	  y+=dyD;
	  u+=k2;
	  v+=k3;
	  d+=v;
	}
      }    
      d+=v/2-u-k2/2+3*k3/8; 
      // error (^) in Foley and van Dam p 959, "2nd ed, revised 5th printing"
      u=v-u-k2/2+k3/2;
      v-=k2-k3/2;
      k1-=2*k2-k3;
      k2=k3-k2;
      int tmp=dxS;dxS=-dyS;dyS=tmp;
    }else{
      while(2*u<k2){
	p(x,y,data);    
	if(d>0){
	  x+=dxS;
	  y+=dyS;
	  u+=k1;
	  v+=k2;
	  d+=u;
	}else{
	  x+=dxD;
	  y+=dyD;
	  u+=k2;
	  v+=k3;
	  d+=v;
	}
      }
      int tmpdk=int(k1-k2);
      d+=u-v+tmpdk;
      v=2*u-v+tmpdk;
      u+=tmpdk;
      k3+=4*tmpdk;
      k2=k1+tmpdk;      
      int tmp=dxD;dxD=-dyD;dyD=tmp;
    }    
    octant=((octant+1)&7);
    octantcount--;
  }
}

/*
  Fill a random ellips (paraboles, hyperboles and degenerates ar not handled.
  The algorithm can be extended to these by handling all the det=0 cases)
  clip to [0,w[ x [0,h[
  A-F: equation of ellips.
  slicer/data draws horizontal slices as in fill.
*/
//There must be a better way to handle this
void fconic(double A,double B,double C,double D,double E,double F,
	    SliceSetter *slice,void *data,int w,int h){
  //A*x*x+B*x*y+C*y*y+D*x+E*y+F==0
  //the line y=? is extremal when, in the loop equation det=0
  //->b*b-4*a*c=0;
  //->B*B*y*y+D*D+2*B*D*y-4*A*(C*y*y+E*y+F)=0
  //->(B*B-4*A*C)*y*y+(2*B*D-4*A*E)*y+D*D-4*A*F=0
  double a=B*B-4*A*C;
  double b=2*B*D-4*A*E;
  double c=D*D-4*A*F;
  double det=b*b-4*a*c;
  if(det<=0) //not an ellipse ?  This is more like a sanity check
    return;
  
  det=sqrt(det);
  int ymin=int((-b-det)/(2*a)),ymax=int((-b+det)/(2*a));
  ORDER(int,ymin,ymax);
  if(h>=0){
    if(ymin<0)
      ymin=0;
    if(ymin>=h||ymax<0)
      return;
    if(ymax>=h)
      ymax=h-1;
  }
  for(int y=ymin;y<=ymax;y++){
    //Strategy now is, push a line y=??? trough the ellipse
    //draw a slice between the 2 sorted crossing points
    a=A;
    b=B*y+D;
    c=(C*y)*y+E*y+F;
    det=b*b-4*a*c;
    if(det<0) //should not happen, except in the beginning
      continue;
    det=sqrt(det);
    int xmin=int((-b-det)/(2*a));
    int xmax=int((-b+det)/(2*a));
    //printf("%lld %lld\n",xmin,xmax);
    ORDER(int,xmin,xmax);
    //clip
    if(w>0){
      if(xmin<0)
	xmin=0;
      if(xmin>=w||xmax<0)
	continue;
      if(xmax>=w)
	xmax=w-1;
    }
    slice(xmin,xmax,y,data);
  }
}
