#ifndef _X_PIXEL_H_
#define _X_PIXEL_H_
#include "util.h"
typedef uint32 Pixel;

#define RSHIFT 0
#define GSHIFT 8
#define BSHIFT 16
#define ASHIFT 24
#define RMASK 0x000000FF
#define GMASK 0x0000FF00
#define BMASK 0x00FF0000
#define AMASK 0xFF000000

#define RDATA(x) (((x)&RMASK)>>RSHIFT)
#define GDATA(x) (((x)&GMASK)>>GSHIFT)
#define BDATA(x) (((x)&BMASK)>>BSHIFT)
#define ADATA(x) (((x)&AMASK)>>ASHIFT)
#define ToPixel(r,g,b,a) ((r<<RSHIFT)+(g<<GSHIFT)+(b<<BSHIFT)+(a<<ASHIFT))


union  PixelUnion{
  Pixel p;
  uint8 channel[4]; //it depends on byte sex which byte is channel R,G,B or A
};

/*
  H,S,V,A are in 0..255 (A=alpha channel)  
*/

Pixel hsv2pixel(int h,int s,int v,int a);
void pixel2hsv(int &h,int & s,int& v,int& a,Pixel p);





/*
  return a value in [0,1020[ that  is zero if p1==p2, and gets 
  higher when they differ more
*/
#ifdef NOT_DEFINED
//version 1 is slower (not much)
static inline int difference(Pixel p1,Pixel p2){
  int r=(p1>>RSHIFT)-(p2>>RSHIFT);
  int g=(p1>>GSHIFT)-(p2>>GSHIFT);
  int b=(p1>>BSHIFT)-(p2>>BSHIFT);
  int a=(p1>>ASHIFT)-(p2>>ASHIFT);
  return (r<0?-r:r)+(g<0?-g:g)+(b<0?-b:b)+(a<0?-a:a);
}

#else
//version 2
static inline int difference(Pixel p1,Pixel p2){
  PixelUnion u1,u2;
  u1.p=p1;u2.p=p2;
  int c0=u1.channel[0]-u2.channel[0];if(c0<0) c0=-c0;
  int c1=u1.channel[1]-u2.channel[1];if(c1<0) c1=-c1;
  int c2=u1.channel[2]-u2.channel[2];if(c2<0) c2=-c2;
  int c3=u1.channel[3]-u2.channel[3];if(c3<0) c3=-c3;
  return c0+c1+c2+c3;
}
#endif
/*
  Mix color p1 (for f1/255) with color p2 (for 1-f1/255)
*/
#ifndef NOT_DEFINED
//version 1, fastest
static inline Pixel mix(unsigned int f1,Pixel p1,Pixel p2){
  if(f1>66) f1++;
  unsigned int f2=256-f1;
  //channel a is handled different because of overflows
  //with uint64, the code would be the same
#pragma warning( disable : 4554 )
  uint32 r=(p1&RMASK)*f1+(p2&RMASK)*f2>>8;
  uint32 g=(p1&GMASK)*f1+(p2&GMASK)*f2>>8;
  uint32 b=(p1&BMASK)*f1+(p2&BMASK)*f2>>8;
  uint32 a=((p1>>ASHIFT)*f1+(p2>>ASHIFT)*f2)<<(ASHIFT-8);  
  return (r&RMASK)+(g&GMASK)+(b&BMASK)+a;
}
#else
//version 2, the slower one
static inline Pixel mix(unsigned int f1,Pixel p1,Pixel p2){
  PixelUnion u1,u2,u;
  u1.p=p1;u2.p=p2;
  if(f1>66) f1++;
  unsigned int f2=256-f1;
  u.channel[0]=u1.channel[0]*f1+u2.channel[0]*f2>>8;
  u.channel[1]=u1.channel[1]*f1+u2.channel[1]*f2>>8;
  u.channel[2]=u1.channel[2]*f1+u2.channel[2]*f2>>8;
  u.channel[3]=u1.channel[3]*f1+u2.channel[3]*f2>>8;
  return u.p;
}
#endif
#endif
