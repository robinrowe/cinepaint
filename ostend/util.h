/*
  util.h (c) 2002 Yperman Hans

  This contains various utility functions and datatypes.

  util.cc & util.h depend on nothing else
*/
#ifndef _X_UTIL_H_
#define _X_UTIL_H_
//#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "config.h"
#define PI 3.14159265358979323848
#define ABS(x) ((x)<0?-(x):(x))
#define MAX(x,y) ((x)<=(y)?(y):(x))
#define MIN(x,y) ((x)>(y)?(y):(x))
//e.g. SWAP(int,a,b) swaps a and b, using a temp
#define SWAP(type,x,y) {type tmp;tmp=(x);(x)=(y);(y)=tmp;}
//order (int,a,b) swaps a & b if a>b
#define ORDER(type,x,y) if((x)>(y)){type tmp;tmp=(x);(x)=(y);(y)=tmp;}
//clip(x,y,x1,y1,x2,y2) clips the point (x,y) in the given rectangle
#define CLIPUP(x,y,x2,y2) {if((x)>(x2)) (x)=(x2);if((y)>(y2)) (y)=(y2);}
#define CLIPDOWN(x,y,x1,y1) {if((x)<(x1)) (x)=(x1);if((y)<(y1)) (y)=(y1);}
#define CLIP(x,y,x1,y1,x2,y2) {CLIPUP(x,y,x2,y2);CLIPDOWN(x,y,x1,y1);}
//size of a static array. e.g. int[48] x; assert(UBOUND(x)==48);
#define UBOUND(array)((int)sizeof(array)/(int)sizeof((array)[0]))
static inline void clamp(double& a,double min=0.0,double max=1.0){
  if(a<min) a=min;
  if(a>max) a=max;  
}

static inline void clampu(uint32& a,uint32 min=0,uint32 max=65535){
  if(a<min) a=min;
  if(a>max) a=max;  
}

static inline void memset32(uint32* data,int c,int l){
  //rep stosd in C++.  Might be a job for the inline assembler?
  while(l-- >0)
    *data++=c;
}

static inline void minimize(int& oldmin,int possiblemin){
  if(possiblemin<oldmin)
    oldmin=possiblemin;
}

//synonymous for s=malloc(enough);sprintf(s,format,...);return s;
char *msprintf(const char *format,...);
char *vmsprintf(const char *format,va_list ap);

void stop(const char *s,...);
void warn(const char *s,...);

//obsolete,replaced by LongString
//PS: buggy under non-linux too.
void strappend(char **s,char *s2);
inline void strappendd(char **s,char *s2){strappend(s,s2);free(s2);}
char *replace(char *s,char c1,char c2);

//use this one
class LongString {
  static const int bloksize=128;
  int len,pos;
  char *data;
 public:
  LongString()
    {   pos=len=0;
        data = 0;
    }
  ~LongString();
  void append(const char *s);
  void append(double d);
  void append(char c);
  inline void appendd(char *s){
    append(s);
    free(s);
  }
  inline void clear(){
    pos=0;
  }
  void printf(const char *format,...);
  const char *result() const {return data;}/* deprecated */
  const char *value() const {return data;} /* this one is better in line
					      with the fltk api*/
  char *release(){len=pos=0;return data;}
  void replace(const char *from,const char *to);
  inline void replaced(const char *from,char *to){
    replace(from,to);
    free(to);
  }
};

//a SIMPLE vector that stores pointers to void*. Not one that adds
//100k to my program.  Only problem is: you should cast everything

//Well, i think at last it's yet better to use vector<..>, even
//thinking about it's size and it's brain-damaged sorting.
class DynArray{
  int items,space;
  static const int space_delta=16;
  void **data;
 public:
  DynArray();
  ~DynArray();
  int add(void *ptr);
  int insert(int index,void *ptr);
  void remove(int index);
  void clear(){items=0;}
  inline void *operator[](int index) const {return data[index];}
  inline int size() const {return items;}
  inline void **get()const{return data;}
};

//A command line parser & temporary space manager.
//It doesn't do any of these tasks very well as yet.  Rewrite !!
class Program{
  int myargc;
  char **myargv;
  bool *touched;
  int file_arg;  //this argument is -- , and afrguments after it are considerd files

  char *tmpdir;

  int tmpnum;
  Program(const Program&){abort();}
 public:
  Program(int argc,char *argv[]);
  ~Program();
  static Program& get();
  int argc() const {return myargc;}
  char **argv() const {return myargv;}
  bool argument(const char * option);
  int intargument(const char * option,int defaultval);
  const char * stringargument(const char * option,const char * defaultval);
  int argumentpos(const char * option);
  int untouchedargument();
  int arguments() const{return myargc;}
  const char * argument(int i);
  const char *tmpfile();
};

#endif
