#ifndef _X_W_IMAGE_H_
#define _X_W_IMAGE_H_
#include <Fl/Fl_Widget.h>
#include <Fl/Fl_Scrollbar.h>
#include <Fl/Fl_Output.h>
#include <Fl/Fl_Valuator.h>
#include <Fl/Fl_Group.h>
#include "canvas.h"

#define SCALE_SHIFT 16
#define SCALE_1  (1<<16)

class W_Canvas:public Fl_Widget{
  Canvas *my_image;
  int base_x,base_y;
  int scale;
  bool tool_busy;

  Fl_Scrollbar *hor,*vert;
  Fl_Valuator *scal;
  Fl_Output *pos;
  Fl_Group *tool_group,*tool_group_parent;
  int selected_pen;
  Pen *my_pen[2];
  Paint *my_paint[2];
  Tool *my_tool;
  
  static void scrollbar_dragged(Fl_Widget *,void *);
  static void scaler_dragged(Fl_Valuator *,void *);
  void reposition(int,int);
  void set_scrollbars(); 
  void set_scaler();
  static void canvasdestroyed(void *p1,void *p2);
  static void canvaspainted(void *p1,void *p2);
 protected:
  virtual void draw();
  virtual int handle(int event);
  virtual void resize(int,int,int,int);
 public:
  W_Canvas(int x,int y,int w,int h,const char *l=0);
  virtual ~W_Canvas();

  static void change_pen_callback(Fl_Widget *,void *);

  void canvas(Canvas *i);
  inline Pen *pen(int n) const {return my_pen[n];}
  inline Paint *paint(int n)const {return my_paint[n];}
  inline void paint(int n,Paint *p){my_paint[n]=p;}
  inline Canvas *canvas()const{return my_image;};
  void tool(Tool *t);

  inline void drawscale(int ms1){scale=ms1;set_scaler();}

  inline int xstart()const{return base_x;}
  inline int ystart()const{return base_y;}
  inline void xstart(int n){base_x=n;}
  inline void ystart(int n){base_y=n;}

  //support widgets
  inline void scroll_h(Fl_Scrollbar *b){
    hor=b;
    b->callback(scrollbar_dragged,this);
  }
  
  inline void scroll_v(Fl_Scrollbar *b){
    vert=b;
    b->callback(scrollbar_dragged,this);
  }
  
  inline void position(Fl_Output *o){
    pos=o;
  }
  
  inline void scaler(Fl_Valuator *v){
    scal=v;
    v->callback((Fl_Callback *)scaler_dragged,this);
  }
  
  inline void toolgroupparent(Fl_Group *g){
    tool_group_parent=g;
  }


  int xscreen(int x) const;
  int yscreen(int y) const;
  int xcanvas(int x) const;
  int ycanvas(int y) const;
};

#endif
