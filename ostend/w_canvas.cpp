#include <stdio.h>
#include <Fl/Fl_Group.h>
#include <Fl/fl_draw.h>
#include <Fl/Fl.h>
#include "w_canvas.h"
#include "pen.h"
#include "paint.h"
#include "tool.h"
#include "win_main.h"
#include "win_paint.h"
#include "pen.h"
#ifdef TEST_BLIT_SPEED
#include <time.h>
#endif
#include <unistd.h>

W_Canvas::W_Canvas(int x,int y,int w,int h,const char *l)
  :Fl_Widget(x,y,w,h,l){
  my_image=0;
  scale=SCALE_1;

  selected_pen=0;
  my_pen[0]=new Pen();
  my_pen[1]=new Pen();
  my_paint[0]=new ColorPaint(0xFF0000);
  my_paint[1]=new ColorPaint(0x0000FF);
  my_tool=0;
  //tool(new ToolDot());
  //my_tool->setup(my_image,my_pen[0],my_paint[0]);
}

W_Canvas::~W_Canvas(){
  canvas(0);
  delete my_pen[0];
  delete my_pen[1];
  delete my_paint[0];
  delete my_paint[1];
  delete my_tool;
}

void W_Canvas::tool(Tool *t){
  if(my_tool!=0){
    if(tool_group!=0)
      tool_group_parent->remove(tool_group);
    my_tool->on_uninstall();
    delete my_tool;
  }
  my_tool=t;
  my_tool->setup(my_image,my_pen[0],my_paint[0]);
  tool_group_parent->begin();
  tool_group=my_tool->on_install();
  if(tool_group!=0){
    tool_group->position(tool_group_parent->x()+3,tool_group_parent->y()+3);
    tool_group->show();
    //tool_group->redraw();
  }
  tool_group_parent->redraw();
  tool_group_parent->end();
}

void W_Canvas::reposition(int sx,int sy){
  if(my_image==0)
    return;

  int wsize=(this->w()-Fl::box_dw(box()))*scale>>SCALE_SHIFT;
  base_x=sx;
  if(base_x+wsize>my_image->width())
    base_x=my_image->width()-wsize;
  if(base_x<0)
    base_x=0;

  int hsize=(this->h()-Fl::box_dh(box()))*scale>>SCALE_SHIFT;
  base_y=sy;
  if(base_y+hsize>my_image->height())
    base_y=my_image->height()-hsize;
  if(base_y<0)
    base_y=0;
  redraw();
}

void W_Canvas::scrollbar_dragged(Fl_Widget *,void *mme){  
  W_Canvas *me=(W_Canvas *)mme;
  me->reposition(me->hor->value(),me->vert->value());
}

void W_Canvas::set_scrollbars(){
  if(my_image==0)
    return;
  int wsize=(this->w()-Fl::box_dw(box()))*scale>>SCALE_SHIFT;
  base_x=hor->value();
  if(base_x+wsize>my_image->width())
    base_x=my_image->width()-wsize;
  if(base_x<0)
    base_x=0;
  hor->value(base_x,wsize,0,my_image->width());

  int hsize=(this->h()-Fl::box_dh(box()))*scale>>SCALE_SHIFT;
  base_y=vert->value();
  if(base_y+hsize>my_image->height())
    base_y=my_image->height()-hsize;
  if(base_y<0)
    base_y=0;
  vert->value(base_y,hsize,0,my_image->height());
}

void W_Canvas::scaler_dragged(Fl_Valuator *v,void *mme){  
  W_Canvas *me=(W_Canvas *)mme;
  double n=v->value();
  if(n<0){
    n=SCALE_1/(1-n);
  }else{
    n=(1+n)*SCALE_1;
  }
  me->drawscale((int)n);
  char s[128];
  if(n>=SCALE_1)
    sprintf(s,"scale:%3.3g",double(n)/SCALE_1);
  else
    sprintf(s,"scale:1/%3.3g",double(SCALE_1)/double(n));
  me->pos->value(s);
  me->pos->redraw();
  me->redraw();
}

void W_Canvas::set_scaler(){
  double n=scale;
  if(n<SCALE_1)
    n=-SCALE_1/n+1;
  else
    n=(n/SCALE_1)-1;
  scal->value(n);
  set_scrollbars();
}

int W_Canvas::xscreen(int x)const{
  return ((x-base_x)<<SCALE_SHIFT)/scale+this->x()+Fl::box_dx(box());
}

int W_Canvas::yscreen(int y)const{
  return ((y-base_y)<<SCALE_SHIFT)/scale+this->y()+Fl::box_dy(box());
}

int W_Canvas::xcanvas(int x)const{
  return ((x-this->x()-Fl::box_dx(box()))*scale>>SCALE_SHIFT)+base_x;
}

int W_Canvas::ycanvas(int y)const{
  return ((y-this->y()-Fl::box_dy(box()))*scale>>SCALE_SHIFT)+base_y;
}

void W_Canvas::draw(){
  //this function is getting dirty
  draw_box();
  int x=this->x()+Fl::box_dx(box());
  int y=this->y()+Fl::box_dy(box());
  int w=this->w()-Fl::box_dw(box());
  int h=this->h()-Fl::box_dh(box())-1;
  //if there's no image, we go for the big gray void
  if(my_image==0){
    fl_rectf(x,y,w,h,127,127,127);
    return;
  }
#ifdef TEST_BLIT_SPEED
  int counter=0;
  clock_t cend=clock()+3*CLOCKS_PER_SEC;
  do {
#endif

    //(wmax,hmax) is the size in screen coordinates  of the image to blit
    int wmax=((my_image->width()-base_x)<<SCALE_SHIFT)/scale;
    int hmax=((my_image->height()-base_y)<<SCALE_SHIFT)/scale;
    if(wmax>w) wmax=w;
    if(hmax>h) hmax=h;

/*
    int xx,yy,ww,hh;
    //clip by dirty screen
    fl_clip_box(x,y,wmax,hmax,xx,yy,ww,hh);
    //clip by dirty image
    int x1,y1,x2,y2;
    my_image->isdirty(x1,y1,x2,y2);
    x1=xscreen(x1);y1=yscreen(y1);
    x2=xscreen(x2+1);y2=yscreen(y2+1)+1;
    if(x1<=x2&&y1<=y2){
      CLIP(xx,yy,x1,y1,x2+1,y2+1);
      CLIPUP(ww,hh,x2-x1,y2-y1);
    }
*/    
    //now intersect this rectangle with whatever is dirty on the screen
    int xx,yy,ww,hh,xx2,yy2;
    fl_clip_box(x,y,wmax,hmax,xx,yy,ww,hh);
    xx2=xx+ww;yy2=yy+hh;

    //get the dirty image size, converted to screen coordinates
    int dx1,dy1,dx2,dy2;
    my_image->isdirty(dx1,dy1,dx2,dy2);
    dx1=xscreen(dx1);dy1=yscreen(dy1);
    dx2=xscreen(dx2+1)+1;dy2=yscreen(dy2+1)+1;

    //clip with it
    if(dx2>dx1&&dy2>dy1){
      CLIP(xx,yy,dx1,dy1,dx2,dy2);
      CLIP(xx2,yy2,dx1,dy1,dx2,dy2);
      ww=xx2-xx;hh=yy2-yy;
    }

    my_image->undirty();
    //if(ww<=0||hh<=0) return;

    //make a cached image we blit (xx,yy)-(xx+ww,yy+hh) (screen coordinates)
    //(imxs,imys)=(xx,yy) in image coordinates, shifted by SCALE_SHIFT
    // there is also a fractional part, if we draw half pixels
    //thus, these lines are:
    //int imxs=xcanvas(xx)+ a tiny bit,imys=ycanvas(yy)+ a tiny bit;
    int imxs=(xx-x)*scale+(base_x<<SCALE_SHIFT);
    int imys=(yy-y)*scale+(base_y<<SCALE_SHIFT);
    uchar *cache=new uchar[ww*hh*3];
    uchar *target=cache;
#ifdef DEBUG_BLIT_UPDATES
    static int q=0;q=(q+8)&255;
#endif
    int row=imys;
    for(int i=0;i<hh;i++){
      int column=imxs;
      Pixel *source=&(my_image->pixel(0,row>>SCALE_SHIFT));
      row+=scale;
      uchar *tend=target+ww*3;
      while(target<tend){
	Pixel p=source[column>>SCALE_SHIFT]
#ifdef DEBUG_BLIT_UPDATES
	  ^q
#endif
	  ;
	*target++=(uchar)(p>>RSHIFT);
	*target++=(uchar)(p>>GSHIFT);
	*target++=(uchar)(p>>BSHIFT);
	column+=scale;
      }
    }

    //blit cached image
    if(wmax<w)
      fl_rectf(x+wmax,y,w-wmax,h,127,127,127);
    if(hmax<h)
      fl_rectf(x,y+hmax,wmax,h-hmax,127,127,127);    
    fl_push_no_clip();
    fl_draw_image(cache,xx,yy,ww,hh,3,0);
    fl_pop_clip();
    delete[] cache;

    //add lines
    int *lines=my_tool->on_draw();  
    if(lines!=0){
      int *l=lines;
      fl_push_clip(xx,yy,ww,hh);
      while(*l!=-1){
	fl_color(0,0,0);
	fl_line_style(FL_SOLID);
	fl_line(xscreen(l[0]),yscreen(l[1]),xscreen(l[2]),yscreen(l[3]));
	fl_color(255,255,255);
	fl_line_style(FL_DASH);
	fl_line(xscreen(l[0]),yscreen(l[1]),xscreen(l[2]),yscreen(l[3]));
	l+=4;
      }
      fl_pop_clip();
      fl_line_style(FL_SOLID);
      delete[] lines;
    }

#ifdef TEST_BLIT_SPEED
    counter++;
  }while(clock()<=cend);
  printf("Test_Blit_Speed:frames/s=%d\n",counter/3);
#endif

}

void W_Canvas::canvaspainted(void *p1,void *){
  W_Canvas *me=(W_Canvas *)p1;
  //Canvas *c=(Canvas *)p2;
  me->redraw();
}

void W_Canvas::canvasdestroyed(void *p1,void *p2){
  W_Canvas *me=(W_Canvas *)p1;
  Canvas *c=(Canvas *)p2;
  if(me->my_image==c)
    me->my_image=0;
  else
    warn(__FILE__ ".canvasdestroyed:Got destroyed message from weird canvas ?\n");
}

//unregister the old canvas, register the a one
//the new canvas can be 0.
void W_Canvas::canvas(Canvas *i){
  if(my_image!=0){
    my_image->deactivate();
    my_image->on_delete().unsubscribe(canvasdestroyed,this);
    my_image->on_paint().unsubscribe(canvaspainted,this);
  }
  my_image=i;
  if(i!=0){
    i->activate();
    i->on_delete().subscribe(canvasdestroyed,this);
    i->on_paint().subscribe(canvaspainted,this);
    my_tool->setup(i,my_pen[selected_pen],my_paint[selected_pen]);
    tool_group_parent->show();
  }else{
    tool_group_parent->hide();
    tool_group_parent->window()->redraw();
    my_tool->setup(0,0,0);
  }
  set_scrollbars();
}

void W_Canvas::resize(int x,int y,int w,int h){
  Fl_Widget::resize(x,y,w,h);
  set_scrollbars();
}
int W_Canvas::handle(int event){
  if(my_image==0)
    return Fl_Widget::handle(event);
  int img_x=xcanvas(Fl::event_x());
  int img_y=ycanvas(Fl::event_y());
  switch(event){
  case FL_PUSH:
  case FL_DRAG:
  case FL_RELEASE:
  case FL_MOVE:
  case FL_ENTER:
  case FL_LEAVE:
    if(img_x<0||img_y<0||
       img_x>=my_image->width()||img_y>=my_image->height()||
       event==FL_LEAVE)
      pos->value("");
    else{
      char s[128];
      sprintf(s,"%5d %5d %8.8x",img_x,img_y,my_image->pixel(img_x,img_y));
      pos->value(s);
      pos->redraw();
    }
  }
  switch(event){
  case FL_PUSH:
    {
      int p=0;
      if(Fl::event_button()==3)
	p=1;
      my_image->base(img_x,img_y);
      my_tool->setup(my_image,my_pen[p],my_paint[p]);
      if(Fl::event_shift()){
	my_paint[p]->reada(img_x,img_y);
	tool_busy=false;
      }else if(Fl::event_ctrl()){
	my_paint[p]->readb(img_x,img_y);
	tool_busy=false;
      }else{
	tool_busy=true;
	if(my_tool->on_click(img_x,img_y))
	  redraw();
      }
    }
    return 1;
  case FL_DRAG:
    if(tool_busy&&my_tool->on_drag(img_x,img_y))
      redraw();
    return 1;
  case FL_RELEASE:
    if(tool_busy&&my_tool->on_release(img_x,img_y))
      redraw();
    return 1;
  case FL_MOVE:
  case FL_ENTER:
  case FL_LEAVE:
    if(my_tool->on_move(img_x,img_y))
      redraw();
    return 1;
  default:
    return Fl_Widget::handle(event);
  }
  return 666;
}


void W_Canvas::change_pen_callback(Fl_Widget *b,void *arg){
  W_Canvas *me=((MainWindow *)b->window()->user_data())->workspace;
  intptr_t pen_nr=(intptr_t)(arg);
  Win_Paint *w=new Win_Paint(me->pen(pen_nr),me->paint(pen_nr));
  Paint *p=w->run();
  if(p!=me->paint(pen_nr)){
    delete me->paint(pen_nr);
    me->paint(pen_nr,p);
  }
  delete w;
}
