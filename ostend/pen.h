#ifndef _X_PEN_H_
#define _X_PEN_H_
#include "canvas.h"
enum PenType{
  PEN_DOT,
  PEN_SQUARE,
  PEN_SLASH,
  PEN_PLUS
};

class Pen {
  Canvas *canvas_;

  int size_,pressure_,falloff_;
  PenType pentype_;

  uint8 *mask;
  void build_mask();
 public:
  Pen();
  ~Pen();
  void pset(int x,int y);

  inline Canvas *canvas()const{return canvas_;};
  inline void canvas(Canvas *c){canvas_=c;};

  //Pen shape
  inline void size(int n){size_=n;build_mask();};
  inline int size()const{return size_;}
  inline void pressure(int n){pressure_=n;build_mask();}
  inline int pressure()const{return pressure_;}
  inline void falloff(int n){falloff_=n;build_mask();}
  inline int falloff()  const {return falloff_;}
  inline void pentype(PenType t){pentype_=t;build_mask();}
  inline PenType pentype()const{return pentype_;}

  //helper functions for draw.h. The data-argument should be this class
  static void pixelsetter(int x,int y,void *data);
  static void slicer(int x1,int x2,int y,void *data);
};

#endif
