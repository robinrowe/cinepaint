/*
  Pattern Input (c) 2002 Yperman Hans
*/
#include <stdlib.h>
#include "w_input_pattern.h"
#include "util.h"
/*
  Format contains:
   ' ': read as much ' ' and '\t' as possible. Zero is allowed
   '%d','%i': read integer
   '%e','%f','%g':read double
   '%[m,M[d':read integer n,with m<=n< M
   '%]m,M[d':                    m< n< M
   '%[m,M]d':                    m<=n<=M
   '%]m,M]d':                    m< n<=M
      for these, you can use %[...]f to read a double
        m and M are a (double or integer) number to describe a minimum or maximum.
	Also allowed is a / to say that the corresponding bound does
	not apply.
      Examples are:
         PATTERN                 ALLOWED           DISALOWED
         %[0,256[f:              0,1,66,255.99     -0.001,256
	 %]0,256]d:              1,4,256           0,3.1415,257
         %[/,256]d:              -1000,256         257,258,....
	 %[0,/]f:                0,0.1,0.9,999     -0.001
         %[/,/]f:(or %f)         any number        any not-number 
   anything else: input string should contain this
   Warning: the %-pattern should be well-formed.
   Example: "<%f,%f,%[0,3]f>" describes a vector <x,y,z> with the last 
   number in [0,3]
 */

void Pattern_Input::check(Fl_Widget *w,void *){
  Pattern_Input *me=(Pattern_Input *)w;
  //me->is_good=true;return;

  const char *p=me->my_pattern;
  const char *s=me->value();
  me->is_good=false;
  for(;;){
    char c=*p++;
    if(c==0){
      me->is_good=(*s==0);
      break;
    }else if(c==' '){
      while(*s==' '||*s=='\t') s++;
    }else if(c=='%'){
      char *theend;
      double min=0,max=0;
      enum {Inclusive,Exclusive,None} mintype=None,maxtype=None;
      if(*p=='['||*p==']'){
	mintype=*p++=='['?Inclusive:Exclusive;
	if(*p=='/'){
	  mintype=None;
	  p++;
	}else{
	  min=strtod(p,&theend);
	  p=theend;
	}
	if(*p++!=',')
	  stop("Invalid pattern");
	if(*p=='/'){
	  maxtype=None;
	  p++;
	}else{
	  max=strtod(p,&theend);
	  p=theend;
	  if(*p++=='[')
	    maxtype=Exclusive;	  
	  else
	    maxtype=Inclusive;
	}
      }
      switch(*p++){
      case 'd':
      case 'i':
	{
	  int n=strtol(s,&theend,10);
	  if(theend==s)
	    goto pattern_bad;
	  if(mintype==Inclusive&&n<min)
	    goto pattern_bad;
	  if(mintype==Exclusive&&n<=min)
	    goto pattern_bad;
	  if(maxtype==Inclusive&&n>max)
	    goto pattern_bad;
	  if(maxtype==Exclusive&&n>=max)
	    goto pattern_bad;
	  s=theend;
	}
	break;
      case 'f':
      case 'g':
      case 'e':
	{
	  double n=strtod(s,&theend);
	  if(theend==s)
	    goto pattern_bad;
	  if(mintype==Inclusive&&n<min)
	    goto pattern_bad;
	  if(mintype==Exclusive&&n<=min)
	    goto pattern_bad;
	  if(maxtype==Inclusive&&n>max)
	    goto pattern_bad;
	  if(maxtype==Exclusive&&n>=max)
	    goto pattern_bad;
	  s=theend;
	}
	break;
      default:
	goto pattern_bad;
      }
    }else if(c!=*s++)
	break;    
  }
  pattern_bad:
  me->good(me->is_good);
  if(me->is_good&&me->callb!=0)
    me->callb(me,me->val);
}

Pattern_Input::Pattern_Input(int x,int y,int w,int h,const char *s)
  :Fl_Input(x,y,w,h,s){
  is_good=true;
  my_pattern=0;
  callb=0;
  val = 0;
  Fl_Input::callback(check,0);
}

Pattern_Input::~Pattern_Input(){
  if(my_pattern!=0)
    free((char *)my_pattern);
}

void Pattern_Input::good(bool b){
  is_good=b;
  if(is_good)
    textcolor(FL_BLACK);
  else
    textcolor(FL_RED);
  redraw();
}


void Pattern_Input::printf(const char *fmt,...){
  va_list v;
  va_start(v,fmt);
  char *s=vmsprintf(fmt,v);
  value(s);
  va_end(v);
  good(true);
}

void Pattern_Input::printf_no_cb(const char *fmt,...){
  va_list v;
  va_start(v,fmt);
  char *s=vmsprintf(fmt,v);
  Fl_Input::value(s);
  va_end(v);
  good(true);
}
