#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
static char debugger[]="/usr/bin/gdb";
static const char *programname;
static int signals[]={SIGABRT,SIGFPE,SIGILL,SIGQUIT,SIGSEGV,0};

/*
  Handle program crash by starting debugger
*/
static void signal_handler(int num){
  //avoid an endless loop
  for(int i=0;signals[i]!=0;i++)
    signal(signals[i],SIG_DFL);

  //print message
  printf("Signal %d caught\n"
	 "Trying to start debugger %s ...\n"
	 "Please type bt<ENTER> and send the author of this program  the result\n\n"
	 ,num,debugger);
  
  pid_t forkno=fork();
  if(forkno<0){
    printf("Could not fork !!\n");
    abort();
  }
  if(forkno>0){
    int status;
    waitpid(forkno,&status,0);
    printf("Error:Forked debugger exited ! \n");
    raise(num);
    abort();
  }

  //we are the child process. Resistance is futile. You will be debugged.
  char s[128];
  sprintf(s,"%d",getppid());
  execl(debugger,debugger,programname,s,0);
  fprintf(stderr,"Could not start debugger");
  _exit(1);
}

/*
  setup the error checker. This returns true if that was possible.
*/
bool setup_errorchecker(const char *name){
  programname=name;
  if(!isatty(0)||!isatty(1))
    return false;
  //register signal handlers
  for(int i=0;signals[i]!=0;i++)
    if(signal(signals[i],signal_handler)==SIG_ERR)
      return false;
  return true;
}
