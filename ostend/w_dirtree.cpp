#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <Fl/Fl.h>
#include <Fl/Fl_Button.h>
#include <Fl/fl_draw.h>
#include <Fl/fl_ask.h>
#include <Fl/fl_xpm_image.h>
#include "util.h"
#include "ui_tools.h"
#include "w_dirtree.h"
//rsr
//#include "images.h"
#include "win_stuff.h"
#include "win_open.h"
#include <unistd.h>

DirTreeBrowser::DirTreeBrowser(int x,int y,int w,int h,char *l)
  :TreeBrowser(x,y,w,h,l){
  dir=0;
  dirchanged=0;
  callback(treebrowsed);
}

DirTreeBrowser::~DirTreeBrowser(){
  dirchanged->unsubscribe(directory_changed,this);
}

void DirTreeBrowser::directory(Directory *d){
  dir=d;
  repopulate();
}

void DirTreeBrowser::directory_changed(void *p,void *p2){  
  DirTreeBrowser *me=(DirTreeBrowser *)p;
  TreeItem *t=(TreeItem *)me->selection();
  if(t->has_childs() || p!=p2){
    t->has_childs(false);
    me->repopulate();
    t->is_open(true);
  }else{
    t->remove_childs();
    t->has_childs(true);
  }
  me->redraw();
}

void DirTreeBrowser::getnodepath(const TreeTextItem *i,LongString& l){
  TreeTextItem *dad=dynamic_cast<TreeTextItem *>(i->up());
  if(dad!=0){
    getnodepath(dad,l);
    if(i->value()[0]!=0)
    l.append('/');
  }
  l.append(i->value());
}

void DirTreeBrowser::treebrowsed(Fl_Widget *w,void *){
  DirTreeBrowser *me=(DirTreeBrowser *)w;
  if(me->selection()==0) return;
  LongString s;
  me->getnodepath((TreeTextItem *)me->selection(),s);
  me->dir->chdir(s.value());
  me->dirchanged->broadcast(me);
}

void DirTreeBrowser::on_dirchange(Broadcaster *b){
  b->subscribe(directory_changed,this);
  dirchanged=b;
}

void DirTreeBrowser::repopulate(){
  //find the treeitem to connect stuff to
  //add subdirs
  TreeItem *node=root();
  TreeTextItem *child,*found=0;
  for(int level=0;;level++){
    //check for mama of this level, add if needed
    const char *thisdir=dir->level(level);
    if(thisdir==0){
      select_only(node);    
      break;
    }
    child=(TreeTextItem *)node->down();
    found=0;
    if(child!=0) 
      do{
	if(strcmp(child->value(),thisdir)==0){
	  found=child;break;
	}
	child=(TreeTextItem *)child->right();
      }while((TreeTextItem *)child!=node->down());

    if(found==0){
      found=new TreeTextItem(thisdir);
      found->has_childs(true);
      node->add(found);
    }
    found->has_childs(false);
    found->is_open(true);

    //populate this level
    if(found->down()==0){
      vector<FileData*> dirdata;
      dir->read(dirdata,"*",level);
      for(int i=0;i<(int)dirdata.size();i++){
	if((dirdata[i]->flags&DFL_DIR)==0)
	  continue;
	
	TreeItem *t=new TreeTextItem(dirdata[i]->name);
	t->has_childs(true);
	found->add(t);
      }
      dir->cleanup(dirdata);
    }
    //go to next level
    node=found;
  }
}

///////////////////////////////////////////////////////////////////////////////
Speedbar::Speedbar(int x,int y,int w,int h,const char *l)
  :Fl_Scroll(x,y,w,h,l){
  type(HORIZONTAL_ALWAYS);
  user_data(this);
  box(FL_ENGRAVED_BOX); //should be done here
//rsr  
  static Fl_XPM_Image dir_xpm("img/dir.xpm");
  static Fl_XPM_Image dir_home_xpm("img/dir_home.xpm");
  static Fl_XPM_Image dir_new_xpm("img/dir_new.xpm");
  static Fl_XPM_Image dir_floppy_xpm("img/dir_floppy.xpm");
  static Fl_XPM_Image dir_cdrom_xpm("img/dir_cdrom.xpm");
  
  //setup default images
  register_image("dir",&dir_xpm);
  register_image("dir-home",&dir_home_xpm);
  register_image("dir-new",&dir_new_xpm);
  register_image("dir-floppy",&dir_floppy_xpm);
  register_image("dir-cdrom",&dir_cdrom_xpm);
  //create some default items
  Item defaults[]={
    {"Home","dir-home","","~",0},
    {"New","dir-new","mkdir $?","$?",0},
    {"Floppy","dir-floppy","","/floppy",0},
    {"CD-ROM","dir-cdrom","","/cdrom",0},
  };

  for(int i=0;i<(int)UBOUND(defaults);i++){
    Item *it=new Item();
    it->name=strdup(defaults[i].name);
    it->icon=strdup(defaults[i].icon);
    it->cmd=strdup(defaults[i].cmd);
    it->dir=strdup(defaults[i].dir);
    it->widget=0;
    build_widget(it,i);
    items.push_back(it);
  }

}

Speedbar::~Speedbar(){
  static const char groupmask []="Button%d";
/* This does not work. Bug in fltrk 1.1.0 RC 5 ??
  while(prefs->groups()>0){
    prefs->deleteGroup(prefs->group(0));
  }
*/
  for(int i=0;;i++){
    char *s=msprintf(groupmask,i);
    if(!prefs->groupExists(s)){
      free(s);
      break;
    }
    prefs->deleteGroup(s);
    free(s);
  }
  for(int i=0;i<(int)items.size();i++){
    Fl_Preferences *item=new Fl_Preferences(*prefs,
					    Fl_Preferences::Name(groupmask,i));
    item->set("Name",items[i]->name);
    item->set("Icon",items[i]->icon);
    item->set("Command",items[i]->cmd);
    item->set("Dir",items[i]->dir);
    delete item;
    items[i]->destroy();    
  }
  delete prefs;
}


/* This is the destructor of an item.  Sort of */
void Speedbar::Item::destroy(){  
#if 0
  free(name);
  free(icon);
  free(cmd);
  free(dir);
#endif
  widget->parent()->remove(widget);
  delete widget;
  delete this; //Is this legal ?
}

void Speedbar::preferences(Fl_Preferences *p){
  prefs=new Fl_Preferences(p,"Speedbar");
  if(prefs->groups()==0) 
    return;

  for(int i=0;i<(int)items.size();i++){
    items[i]->destroy();
  }
  items.clear();
  for(int i=0;;i++){   
    Fl_Preferences *item=new Fl_Preferences(prefs,
					    Fl_Preferences::Name("Button%d",i));
    char *s;
    item->get("Name",s,(char *)0);
    if(s==0) {delete item;break;}
    Item *it=new Item();
    it->name=strdup(s);
    item->get("Icon",s,(char *)0);
    it->icon=strdup(s);
    item->get("Command",s,(char *)0);
    it->cmd=strdup(s);
    item->get("Dir",s,(char *)0);
    it->dir=strdup(s);
    it->widget=0;
    build_widget(it,i);
    items.push_back(it);    
    delete item;
  }
}

void Speedbar::directory(Directory *d){
  dir=d;
}

void Speedbar::on_dirchange(Broadcaster *b){
  dirchanged=b;
}

void Speedbar::build_widget(Item *item,int pos){
  int x=this->x()+Fl::box_dx(box())-xposition(); 
  int y=this->y()+Fl::box_dy(box());
  if(item->widget!=0){
    item->widget->parent()->remove(item->widget);
    delete item->widget;
  }
  begin();
  item->widget=new Fl_Button(x+size*pos,y,size,size,item->name);
  item->widget->callback(button_callback,(void *)item);
  //item->widget->box(FL_ENGRAVED_BOX);
  end();
  item->widget->label(item->name);
  item->widget->image(get_image(item->icon));
  redraw();
}

void Speedbar::button_callback(Fl_Widget *w,void *p){
  Speedbar *me=(Speedbar *)(w->parent()->user_data());
  Item *i=(Item *)p;
  LongString cmd,dir;
  //ask the question
  cmd.append(i->cmd);
  dir.append(i->dir);
  if(strstr(i->cmd,"$?")!=0||strstr(i->dir,"$?")!=0){
    const char *param=fl_input(i->name); //stupid beeping function.replace me.
    if(param==0) return;
    cmd.replace("$?",param);
    dir.replace("$?",param);
  }
  //execute the command
  if(cmd.value()[0]!=0){
    LongString exactcmd;
    exactcmd.append("cd \"");
    exactcmd.append(me->dir->cwd());
    exactcmd.append("\" && ");
    exactcmd.append(cmd.value());
    int n=system(exactcmd.value());
    if(n!=0){
      char *s=msprintf("The command '%s' returned with exit status %d\n"
		       ,exactcmd.value(),n);
      ask("Command error",0,s,"OK");
      delete[] s;
    }
  }
  //chdir to the new directory
  me->dir->chdir(dir.value());
  me->dirchanged->broadcast(me);
}



/*
  return the number of the button that was selected with the mouse.
  return -1 if none left,items.size() if none right
*/
int Speedbar::selected_speedbutton(){
  int x=(Fl::event_x_root()-window()->x()-this->x()
	 +this->xposition())/size;
  printf("Item %d \n",x);
  if(x<0) return -1;
  if(x>(int)items.size()) return int(items.size());
  return x;
}

/*
  Insert a button at the mouse cursor
*/
void Speedbar::insert(){
  int i=selected_speedbutton();
  if(i<0) return;
  Item *it=new Item();
  const char *n=strrchr(dir->cwd(),'/')+1;
  if(n-1==0) 
    n="name";
  it->name=strdup(n);
  it->icon=strdup("dir");
  it->cmd=strdup("");
  it->dir=strdup(dir->cwd());  
  it->widget=0;
  items.insert(items.begin()+i,it);
  for(;i<(int)items.size();i++)
    build_widget(items[i],i);
  edit();
}

/*
  remove the button at the mouse cursor
*/
void Speedbar::removeb(){
  int i=selected_speedbutton();
  if(i<0||i>=(int)items.size()) return;
  items[i]->destroy();
  items.erase(items.begin()+i);
  for(;i<(int)items.size();i++)
    build_widget(items[i],i);
}

/*
  edit the button at the mouse cursor
*/
void Speedbar::edit(){
  int i=selected_speedbutton();
  if(i<0||i>=(int)items.size()) return;
  Win_Open_Helper w(items[i]);
  w.run();
  build_widget(items[i],i);
}

/*
  move the button at the mouse cursor left
*/
void Speedbar::toleft(){
  int i=selected_speedbutton();
  if(i<1||i>=(int)items.size()) return;
  SWAP(Item *,items[i],items[i-1]);
  build_widget(items[i],i);
  build_widget(items[i-1],i-1);
}

/*
  move the button at the mouse cursor right
*/
void Speedbar::toright(){
  int i=selected_speedbutton();
  if(i<0||i>=(int)items.size()-1) return;
  SWAP(Item *,items[i],items[i+1]);
  build_widget(items[i],i);
  build_widget(items[i+1],i+1);
}

///////////////////////////////////////////////////////////////////////////////
FileBrowser::FileBrowser(int x,int y,int w,int h,const char *l)
  :Fl_Browser_(x,y,w,h,l){
  i_height=fl_height();
  file_selection=strdup("*");
  callback(selection_changed,(void *)this);
}

FileBrowser::~FileBrowser(){
  dirchanged->unsubscribe(directory_changed,this);
  filechanged->unsubscribe(file_changed,this);
  new_list();
  dir->cleanup(files);  
  free(file_selection);
}



void FileBrowser::directory_changed(void *p,void *p2){
  FileBrowser *me=(FileBrowser *)p;
  me->load_directory();
  me->redraw();
  file_changed(p,p2);
}

void FileBrowser::file_changed(void *,void *){
}

void FileBrowser::directory(Directory *d){
  dir=d;
  load_directory();
}

void FileBrowser::on_dirchange(Broadcaster *b){
  dirchanged=b;
  dirchanged->subscribe(directory_changed,this);
}

void FileBrowser::on_filechange(Broadcaster *b){
  filechanged=b;
  filechanged->subscribe(file_changed,this);
}

void FileBrowser::load_directory(){
  //load the files
  dir->cleanup(files);
  dir->read(files,file_selection);
  //calculate sizes
  text_width=0;
  for(int i=0;i<(int)files.size();i++){
    int w=(int)fl_width(files[i]->name);
    if(w>text_width)
      text_width=w;
  }
  //flush the browser
  new_list();
}


//Beware!!! As 0 corresponds to nothing, the first element of the list is 1
//Thus, files[0] corresponds with 1, etc
void *FileBrowser::item_first() const{
  if(files.size()>0)
    return (void *)1;
  else
    return 0;

}
#define ptr2int (intptr_t)
#define int2ptr (void*)
void *FileBrowser::item_next(void *x) const{
  intptr_t i=ptr2int(x);
  if(i+1>(int)files.size())
    return 0;
  else
    return int2ptr(i+1);
}

void *FileBrowser::item_prev(void *x) const{
  intptr_t i=ptr2int(x);
  if(i<=0)
    return 0;
  else
    return int2ptr(i-1);
}

int FileBrowser::item_height(void *) const{
  return i_height;
}

int FileBrowser::item_width(void *) const{
  return text_width+icon_width;
}

int FileBrowser::item_quick_height(void *) const{
  return i_height;
}

int FileBrowser::full_height() const{
  int n=i_height*int(files.size());
  if(n<0) return 0;
  return n;
}

int FileBrowser::full_width() const{
  return text_width+icon_width;
}

void FileBrowser::item_draw(void *p,int x,int y,int,int) const{
  intptr_t i=ptr2int(p)-1;
  if(selection()==p)
    fl_color(FL_WHITE);
  else
    fl_color(FL_BLACK);
  fl_draw(files[i]->name,x+icon_width,y+fl_height()-fl_descent());
}


void FileBrowser::selection_changed(Fl_Widget *,void *p){
  FileBrowser *me=(FileBrowser *)p;
  int sel=ptr2int(me->selection());
  if(sel==0)
    me->filechanged->broadcast(0);
  else{
    LongString name;
    name.append(me->dir->cwd());
    name.append('/');
    name.append(me->files[sel-1]->name);
    me->filechanged->broadcast((void *)name.value());
  }
}
///////////////////////////////////////////////////////////////////////////////
FileInputBox::FileInputBox(int x,int y,int w,int h,const char *l)
  :Fl_Input(x,y,w,h,l){
}

FileInputBox::~FileInputBox(){}

void FileInputBox::directory_changed(void *p,void *p2){
  if(p==p2) return;
  FileInputBox *me=(FileInputBox *)p;
  LongString l;
  l.append(me->dir->cwd());
  l.append('/');
  me->value(l.value());
}

void FileInputBox::file_changed(void *p,void *p2){
  FileInputBox *me=(FileInputBox *)p;
  const char *file=(const char *)p2;  
  if(file==0){
    LongString l;
    l.append(me->dir->cwd());
    l.append('/');
    me->value(l.value());
  }else if(strcmp(me->value(),file)!=0)
    me->value(file);
}

void FileInputBox::directory(Directory *d){
  dir=d;
}

void FileInputBox::on_dirchange(Broadcaster *b){
  dirchanged=b;
  dirchanged->subscribe(directory_changed,this);
}

void FileInputBox::on_filechange(Broadcaster *b){
  filechanged=b;
  filechanged->subscribe(file_changed,this);
}

int FileInputBox::handle(int event){
  int n=Fl_Input::handle(event);
  if(event==FL_KEYDOWN){    
    //is the directory changed ?
    int c=Fl::event_key();
    if(c=='/'||c=='/'+FL_KP){      
      dir->chdir(value());
      dirchanged->broadcast(this);
    }
    //is a directory entered
    if(c==FL_Enter||c==FL_KP_Enter){
      if(dir->chdir(value())){
	dirchanged->broadcast(this);
	const char *s=value();
	if(s[strlen(s)]!='/'){
	  LongString l;
	  l.append(dir->cwd());
	  l.append('/');
	  value(l.value());
	}
	return 1;
      }
    }

    //is the filename changed ?
    if(c>32&&c<0xFFFF){
      filechanged->broadcast((void *)value());
    }
  }else if(event==FL_UNFOCUS){
    //do we want tab completion
    int c=Fl::event_key();
    if(c==FL_Tab&&!Fl::event_state(FL_SHIFT|FL_CTRL|FL_ALT|FL_META)){
    //build a list of all possible files
      Fl::focus(this);
      LongString mask;
      const char *v=strrchr(value(),'/');
      if(v!=0)
	mask.append(v+1);
      else
	mask.append(value());
      mask.append('*');
      vector<FileData *> files;
      dir->read(files,mask.value());
      if(files.size()==1){
	if((files[0]->flags&DFL_DIR)!=0){
	  dir->chdir(files[0]->name);
	  LongString l;
	  l.append(dir->cwd());
	  l.append('/');
	  value(l.value());
	  insert_position(strlen(value()));
	  dirchanged->broadcast(this);
	}else{
	  LongString l;
	  l.append(dir->cwd());
	  l.append('/');
	  l.append(files[0]->name);
	  value(l.value());
	  filechanged->broadcast((void *)value());
	}
      }else if(files.size()>=1){
	char *base=strdup(files[0]->name);
	int len=strlen(base);
	for(int i=1;i<(int)files.size();i++){
	  char *file2=files[i]->name;
	  int j;
	  for(j=0;file2[j]!=0&&base[j]==file2[j];j++)
	    ;
	  if(j<len)
	    len=j;
	}
	base[len]=0;
	LongString l;
	l.append(dir->cwd());
	l.append('/');
	l.appendd(base);
	value(l.value());
	insert_position(strlen(value()));
      }
    }

  }
  
  return n;
}
