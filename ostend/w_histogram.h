#ifndef _X_W_HISTOGRAM_H_
#define _X_W_HISTOGRAM_H_
#include <Fl/Fl_Widget.h>
#include "canvas.h"
class W_Histogram:public Fl_Widget{
  static const int buckets=256;
  Canvas *my_canvas;
  uint32 bucket[buckets];
  char my_mode;
  uint32 max;  
 protected:
  void update();
  virtual void draw();
 public:
  W_Histogram(int x,int y,int w,int h,const char *l=0);
  virtual ~W_Histogram();
  void canvas(Canvas *c){my_canvas=c;update();}
  void mode(char m){my_mode=m;update();}
};
#endif
