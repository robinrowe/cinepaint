/*
  Pattern Input (c) 2002 Yperman Hans
*/

#ifndef _X_PATTERN_INPUT_H_
#define _X_PATTERN_INPUT_H_
#include <Fl/Fl_Input.h>
#include <string.h>
class Pattern_Input:public Fl_Input{
  char *my_pattern;
  bool is_good;
  static void check(Fl_Widget *w,void *);

  void *val;
  Fl_Callback *callb;
 public:
  Pattern_Input(int x,int y,int w,int h,const char *s=0);
  virtual ~Pattern_Input();
  void pattern(const char *s){my_pattern=strdup(s);check(this,0);}
  const char *pattern(){return my_pattern;}
  void printf(const char *s,...);
  void printf_no_cb(const char *s,...);
  void value(const char *s){Fl_Input::value(s);check(this,0);}
  void value_no_cb(const char *s){Fl_Input::value(s);good(true);redraw();}
  const char *value(){return Fl_Input::value();}
  bool good(){return is_good;}
  void good(bool b);
  void callback(Fl_Callback *c,void *v){callb=c;val=v;}
  void callback(Fl_Callback *c){callb=c;}
};
#endif
