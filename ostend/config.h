#define PROGRAM_NAME "CineBrush"
#define PACKAGE_NAME "CinePaint"
#define VERSION "0.1"

//choose one of little endian or big endian
#define BYTESEXLE
//#define BYTESEXBE

//choose datatypes
typedef unsigned long long uint64;
typedef unsigned int uint32;
typedef unsigned short int uint16;
typedef unsigned char uint8;

typedef long long int int64;
typedef int int32;
typedef short int int16;
typedef char int8;

typedef unsigned char byte;


//various debug helpers
//#define DUMP_BYTES         //file.cc - read dumps data it reads
//#define TEST_BLIT_SPEED    //w_canvas.cc - keep blitting, say blits/seconde
//#define DEBUG_BLIT_UPDATES //w_canvas.cc - change blit color
