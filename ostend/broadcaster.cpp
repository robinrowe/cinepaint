/*
  Broadcaster (c) 2002 Yperman Hans
*/
#include <string.h>
#include "broadcaster.h"

Broadcaster::Broadcaster(){
  count=0;
}

Broadcaster::~Broadcaster(){
  if(count>0){
    delete[] funcs;
    delete[] params;
  }
}

void Broadcaster::subscribe(BFunc f,void *param){
  BFunc **funcs2=new BFunc*[count+1];
  void **params2=new void*[count+1];
  if(count!=0){
    memcpy(funcs2,funcs,sizeof(funcs[0])*count);
    memcpy(params2,params,sizeof(params[0])*count);
    delete[] funcs;
    delete[] params;
  }
  funcs=funcs2;
  params=params2;
  funcs[count]=f;
  params[count]=param;
  count++;
}

void Broadcaster::unsubscribe(BFunc f,void *param){
  for(int i=0;i<count;){
    if(funcs[i]==f&&params[i]==param){
      memcpy(funcs+i,funcs+i+1,(count-i-1)*sizeof(funcs[0]));
      memcpy(params+i,params+i+1,(count-i-1)*sizeof(params[0]));
      count--;
    }else
      i++;
  }
  if(count<=0){
    delete[] funcs;
    delete[] params;
  }
}

void Broadcaster::broadcast(void *p2){
  for(int i=0;i<count;i++)
    (funcs[i])(params[i],p2);
}

