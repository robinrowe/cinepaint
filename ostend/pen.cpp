//#include <stdio.h>
#include <math.h>
#include "pen.h"
/////Pen///////////////////////////////////////////////////////////////////////

Pen::Pen(){
  size_=1;pressure_=255;falloff_=0;
  pentype_=PEN_DOT;
  mask=new uint8[1];
  build_mask();
}
Pen::~Pen(){
  delete[] mask;
}

void Pen::pixelsetter(int x,int y,void *data){
  //printf("Pset (%d,%d)\n",x,y);
  Pen *p=(Pen *)data;
  p->pset(x,y);
}

void Pen::slicer(int x1,int x2,int y,void *data){
  Pen *p=(Pen *)data;
  unsigned char *s=&p->canvas()->shadow(x1,y);
  unsigned char v=p->pressure();
  memset(s,v,x2-x1+1);
  p->canvas()->dirty(x1,y,x2+1,y);
}

void Pen::pset(int x,int y){
  canvas()->shadow_add_mask(x,y,mask,size());
}

void Pen::build_mask(){
  delete[] mask;
  mask=new uint8[size_*size_];
  uint8 *mask_pixel=mask;
  uint8 base_value=0; //assignment to shut op compiler
  int size2=(size_+1)>>1,size2b=size_>>1;
  for(int y=0;y<size_;y++)
    for(int x=0;x<size_;x++){
      switch(pentype_){
      case PEN_DOT:
	{
	  int dx=size2b-x,dy=size2b-y;
	  int dxy=dx*dx+dy*dy,d;
	  if(dxy<=size2*size2){
	    d=int((size2-sqrt(double(dxy)))*255)/size2;
	    d=(d*pressure_*(255-falloff_)/(255*255))+(pressure_*falloff_/255);
	    base_value=d<0?0:(d>pressure_?pressure_:d);
	  }else
	    base_value=0;;
	}
	break;
      case PEN_SQUARE:
	{
	  int dx=ABS(size2b-x),
	    dy=ABS(size2b-y),
	    d=255-(MAX(dx,dy)<<8)/size2;
	  d=(d*pressure_*(256-falloff_)>>16)+(pressure_*falloff_>>8);
	  base_value=d<0?0:(d>pressure_?pressure_:d);
	}
	break;
      case PEN_SLASH:
	{
	  if(x==y){
	    int dx=ABS(size2b-x),
	      dy=ABS(size2b-y),
	      d=255-(MAX(dx,dy)<<8)/size2;
	    d=(d*pressure_*(256-falloff_)>>16)+(pressure_*falloff_>>8);
	    base_value=d<0?0:(d>pressure_?pressure_:d);
	  }else
	    base_value=0;
	}
	break;
      case PEN_PLUS:
	{
	  if(x==size2b||y==size2b){
	    int dx=ABS(size2b-x),
	      dy=ABS(size2b-y),
	      d=255-(MAX(dx,dy)<<8)/size2;
	    d=(d*pressure_*(256-falloff_)>>16)+(pressure_*falloff_>>8);
	    base_value=d<0?0:(d>pressure_?pressure_:d);
	  }else
	    base_value=0;
	}
	break;
      default:	
	stop("Unfortunately, this program crashed itself "
	     "to heaven: pen type invalid");
      }
      *mask_pixel++=base_value;
    }
}
