/*
  The Infinite Slider (c) 2002 Yperman Hans
*/
//this is for pow10
#define _GNU_SOURCE
//#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <Fl/Fl.h>
#include <Fl/fl_draw.h>
#include "w_infslider.h"
#include "util.h"

Infinite_Slider::Infinite_Slider(int x,int y,int w,int h,const char *s)
  :Fl_Valuator(x,y,w,h,s){
  start=0;size=0;
}

void Infinite_Slider::draw(){
  if(size==0){
    start=minimum();
    size=maximum()-minimum();
  }
  double v=value();
  //draw empty box
  int my_x=x()+Fl::box_dx(box());
  int my_y=y()+Fl::box_dy(box());
  int my_w=w()-Fl::box_dw(box());
  int my_h=h()-Fl::box_dh(box());
  draw_box();
  fl_color(255,255,255);
  fl_rectf(my_x,my_y,my_w-1,my_h-1);

  fl_color(0,0,0);
  fl_push_clip(my_x,my_y,my_w-1,my_h-1);

  //calculate transofmation 
  double n_mul=(my_w-place_l-place_r)/size;
  double n_add=my_x+place_l-start*n_mul;

  //calculate mark positions
  double step=pow10(double (int(log10(fabs(size)))))/2;
  if(size>12*step)
    step*=10;

  double n;  
  for(n=int(start/step)*step;n<=start+size;n+=step){
    int pos=int(n*n_mul+n_add);
    fl_line(pos,my_y,pos,my_y+3);
    fl_line(pos,my_y+my_h-1,pos,my_y+my_h-4);    
    char *s=msprintf("%3.3g",n);
    fl_draw(s,pos-20,my_y+4,40,my_h-8,FL_ALIGN_CENTER,0,0);
    delete[] s;
  }

  step/=10;
  for(n=int(start/step)*step;n<start+size;n+=step){
    int pos=int(n*n_mul+n_add);
    fl_line(pos,my_y,pos,my_y+2);
  }


  //show value()
  if(v<start){
    //what here?
  }else if(v>start+size){
    //what here ?
  }else{
    //Draw value
    fl_color(0,0,255);
    int pos=int(v*n_mul+n_add);
    fl_line(pos,my_y,pos,my_y+my_h-1);
  }

  fl_pop_clip();
}


int Infinite_Slider::handle(int event){
  if(event!=FL_PUSH&&event!=FL_DRAG&&event!=FL_RELEASE)
    return Fl_Widget::handle(event);

  int my_x=x()+Fl::box_dx(box())+place_l;
  int my_w=w()-Fl::box_dw(box());

  //calculate transformation 
  double mul_val=size/double(my_w-place_l-place_r);
  double newval=double(Fl::event_x()-my_x)*mul_val+start;

  static int base_x,base_y,start_x;
  static double base_val,base_start;

  switch(Fl::event_button()){
  case 1:
    if(newval>start+size)
      newval=start+size;
    if(newval<start)
      newval=start;
    value(newval);
    do_callback();
    break;          
  case 2:
    if(event!=FL_PUSH){
      double delta=double(Fl::event_x()-base_x+Fl::event_y()-base_y);
      double newsize=size+delta;
      if(newsize<1)
	newsize=1;
      if(newsize>1e8) //stupid limit to prevent overflows
	newsize=1e8;
      start=(base_start-base_val)/size*newsize+base_val;
      size=newsize;
    }
    else
      start_x=Fl::event_x();
    base_val=double(start_x-my_x)*mul_val+start;
    base_start=start;
    if(minimum()>=0&&start<0)
      start=0;
    break;
  case 3:
    if(event!=FL_PUSH)
      start-=(Fl::event_x()-base_x)*mul_val;
    if(minimum()>=0&&start<0)
      start=0;
    break;
  }
  base_x=Fl::event_x();
  base_y=Fl::event_y();
  redraw();
  return 1;
}

///////////////////////////////////////////////////////////////////////////////
Infinite_Slider_Num::Infinite_Slider_Num(int x,int y,int w,int h,const char *s)
  :Fl_Group(x,y,w,h,s){  
  slider=new Infinite_Slider(x,y,w-52,h);
  slider->callback(my_callback,this);
  input=new Fl_Float_Input(x+w-50,y,50,h);
  input->callback(my_callback,this);
  end();
}

void Infinite_Slider_Num::my_callback(Fl_Widget *w,void *d){
  Infinite_Slider_Num *me=(Infinite_Slider_Num *)d;
  
  if(w==me->input){
    double d;
    if(sscanf(me->input->value(),"%lg",&d)!=1)
      return;
    me->slider->value(d);
  }else{
    char *s=msprintf("%g",me->slider->value());
    me->input->value(s);
    delete[] s;
  }
  me->do_callback();
}
void Infinite_Slider_Num::box(Fl_Boxtype t){
  slider->box(t);
  //input->box(t);
}

Infinite_Slider_Num::~Infinite_Slider_Num(){
}

void Infinite_Slider_Num::maximum(double n){
  slider->maximum(n);
}

void Infinite_Slider_Num::minimum(double n){
  slider->minimum(n);
}

double Infinite_Slider_Num::value(){
  return slider->value();
}

void Infinite_Slider_Num::value(double d){
  slider->value(d);
  char *s=msprintf("%g",d);
  input->value(s);
  delete[] s;
}
