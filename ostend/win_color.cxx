# data file for the Fltk User Interface Designer (fluid)
version 1.0100 
header_name {.h} 
code_name {.cxx}
decl {\#include "ui_tools.h"} {} 

decl {\#include "w_input_pattern.h"} {public
} 

class WinPaint {open : {public MainBase}
} {
  decl {bool busy;} {}
  Function {~WinPaint();} {return_type virtual
  } {
    code {delete window;} {}
  }
  Function {run()} {} {
    code {modal_runner(window,&busy);} {}
  }
  Function {WinPaint()} {open
  } {
    Fl_Window {} {open selected
      xywh {189 294 600 400} visible
    } {
      Fl_Return_Button {} {
        label OK
        xywh {520 12 72 28}
      }
      Fl_Button {} {
        label Cancel
        xywh {520 48 72 28}
      }
    }
  }
} 
