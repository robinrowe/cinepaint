#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdarg.h>
#include <pwd.h>
#include <dirent.h>
#include <Fl/filename.h>
#include "streams.h"
#include "util.h"

///////////////////////////////////////////////////////////////////////////////
StreamError::StreamError(ErrorCreator  *source){
  src=source;
  emsg = 0;
}

StreamError::StreamError(ErrorCreator  *source,const char *msg,...){
  va_list v;
  va_start(v,msg);
  emsg=vmsprintf(msg,v);
  va_end(v);
  src=source;
}

StreamError::~StreamError(){
  free(emsg);
}

StreamError *StreamError::clone(){
 StreamError *s=new StreamError(src);
 s->emsg=strdup(emsg);
 return s;
}
///////////////////////////////////////////////////////////////////////////////
ErrorCreator::ErrorCreator(const char *name)
  :name_(name){
  last_error=0;
  eh=default_error_handler;
}

ErrorCreator::~ErrorCreator(){
  if(last_error!=0)
    delete last_error;
}

void ErrorCreator::handle_error(const char *action,const char *msg,...){
  va_list v;
  va_start(v,msg);
  char *s1=vmsprintf(msg,v);
  va_end(v);
  StreamError *e=new StreamError(this,"Error while %s %s:%s",
				 action,name(),s1);
  free(s1);
  eh(e);
}

void ErrorCreator::handle_error(const char *action){
  StreamError *e=new StreamError(this,"Error while %s %s:%s",
				 action,name(),strerror(errno));
  eh(e);
}

void ErrorCreator::default_error_handler(StreamError *e){
  e->source()->last_error=e;
}

void ErrorCreator::throw_error_handler(StreamError *e) //throw(StreamError *)
{
  e->source()->last_error=e;
  throw e;
}

/////Input streams/////////////////////////////////////////////////////////////
InputStream::InputStream(const char *n)
  :ErrorCreator(n){

}

InputStream::~InputStream(){
}


void InputStream::read(void *buf,int len){
  if(len==0) return;
  if(try_read(buf,len)!=len)
    handle_error("reading","reading past end of file");
  #ifdef DUMP_BYTES
  printf("Read :");
  for(int i=0;i<len;i++)
    printf("%2x ",((char *)buf)[i]);
  printf("\n");
  #endif
}
/////Output Streams////////////////////////////////////////////////////////////
OutputStream::OutputStream(const char *n)
  :ErrorCreator(n){
  user_data = 0;
  pos = 0;
}

OutputStream::~OutputStream(){
}


void OutputStream::write(void *buf,int len){
  if(len==0) return;
  if(try_write(buf,len)!=len)
    handle_error("writing","could not append more data - disk full?");
#ifdef DUMP_BYTES
  printf("Write:");
  for(int i=0;i<len;i++)
    printf("%2x ",((char *)buf)[i]);
  printf("\n");
#endif
}


void OutputStream::printf(const char *s,...){
  va_list v;
  va_start(v,s);
  char *data=vmsprintf(s,v);  
  write(data,int(strlen(data)));
  free(data);
  va_end(v);
}
/////File Input////////////////////////////////////////////////////////////////
FileInputStream::FileInputStream(const char *n)
  :InputStream(n){
  fd=::open(n,O_RDONLY|O_BINARY);
  pos=0;
  if(fd==-1)
    handle_error("opening(r)");
}

FileInputStream::~FileInputStream(){
  close();
}

void FileInputStream::close(){
  if(fd==-1)
    return;
  if(::close(fd)==-1&&!bad())
    handle_error("closing(r)");
}

int FileInputStream::try_read(void *buf,int len){
  int n=::read(fd,buf,len);
  if(n==-1){
    handle_error("reading");
    return 0;
  }
  pos+=n;
  return n;
}


int FileInputStream::position() const{
  return pos;
}

void FileInputStream::position(int new_pos){
  int p=lseek(fd,new_pos,SEEK_SET);
  if(p==-1){
    handle_error("seeking(r)");
    return;
  }
  pos=p;
}
/////File Output////////////////////////////////////////////////////////////////
FileOutputStream::FileOutputStream(const char *n)
  :OutputStream(n){
  //666 is not the number of the beast,it's only rw-acces to u,g,a
  //according to umask
  fd=::open(n,O_WRONLY|O_CREAT|O_TRUNC,00666);
  pos=0;
  if(fd==-1)
    handle_error("opening(w)");
}

FileOutputStream::~FileOutputStream(){
  close();
}

void FileOutputStream::close(){
  if(fd==-1)
    return;
  if(::close(fd)==-1&&!bad())
    handle_error("closing(w)");
}

int FileOutputStream::try_write(void *buf,int len){
  int n=::write(fd,buf,len);
  if(n==-1){
    handle_error("writing");
    return 0;
  }
  pos+=n;
  return n;
}


int FileOutputStream::position() const{
  return pos;
}

void FileOutputStream::position(int new_pos){
  int p=lseek(fd,new_pos,SEEK_SET);
  if(p==-1){
    handle_error("seeking(w)");
    return;
  }
  pos=p;
}
///////////////////////////////////////////////////////////////////////////////

BufferInputStream::BufferInputStream(InputStream *i)
  :InputStream(i->name()){
  buffer=new char[buffer_space];
  buffer_len=0;
  buffer_pos=0;
  buffer_start=0;
  in=i;
}

BufferInputStream::~BufferInputStream(){
  delete in;
  delete buffer;
}


void BufferInputStream::close(){
  in->close();
  handle_error(in->error());
}

int BufferInputStream::try_read(void *out_,int len){
  char *out=(char *)out_;
  if(len==0) return 0;
  //first copy whatever is in the buffer
  int pos=buffer_pos-buffer_start;  
  int got=0;
  if(pos>0&&pos<buffer_len){
    if(len<buffer_len-pos){
      //everything we need is in the buffer
      memcpy(out,buffer+pos,len);
      buffer_pos+=len;
      return len;
    }else{
      //there is usefull data in the buffer, but  not everything
      got=buffer_len-pos;
      memcpy(out,buffer+pos,got);
      buffer_pos+=got;
      out+=got;
      len-=got;     
    }
  }
  in->position(buffer_pos);
  if(len>buffer_space){
    //read some stuff, remember last part in the buffer
    len=in->try_read(out,len);
    //copy last part of it in the buffer (why buffer it, if this happens much ??)
    int l=len>buffer_space?buffer_space:len;
    memcpy(buffer,out+len-l,l);
    handle_error(in->error());
    return got+len;
  }else{
    //read data into buffer
    buffer_len=in->try_read(buffer,buffer_space);
    memcpy(out,buffer,len);
    buffer_start=buffer_pos;
    buffer_pos+=len;
    handle_error(in->error());
    return got+len;
  }
}

int BufferInputStream::position() const{
  return buffer_pos;
}

void BufferInputStream::position(int pos){
  buffer_pos=pos;
}

///////////////////////////////////////////////////////////////////////////////
/*
  Read stuff from *in. tokens is a null-terminated array of strings. if
  something is in it, then CToken_BASE+position is returned, else
  one of the other CTokens comes your way.
  If m==true, then this thing will delete *in while being deleted
*/
CCodeLexer::CCodeLexer(InputStream *in,const char **tokens,bool m)
  :ErrorCreator(in->name()){
  manage_=m;
  ident_tokens=tokens;
  result_buffer=0;
  this->in=in;

  //calculate min_buf_len;
  min_buf_len=2;
  for(num_tokens=0;tokens[num_tokens]!=0;num_tokens++){
    int l=int(strlen(tokens[num_tokens]));
    if(l>min_buf_len)
      min_buf_len=l;
  }
  buffer=new char[min_buf_len+1];
  buf_len=0;

  reserve_token=CToken(0);
  move_buffer(0);
  next(); 
}

CCodeLexer::~CCodeLexer(){
  if(manage_)
    delete in;
  delete[] buffer;
  if(result_buffer!=0)
    delete[] result_buffer;
}

//read a new token
int CCodeLexer::compare(const void *buffer,const void *array){
  char *s=(char *)buffer;
  char *k=*(char **)array;
  while(*k!=0)
    if(*s<*k)
      return 1;
    else if(*s>*k)
      return -1;
    else
      s++,k++;
  return 0;
}

//remove n char's from the buffer
void CCodeLexer::move_buffer(int n){
  buf_len-=n;
  if(buf_len>0)
    memcpy(buffer,buffer+n,buf_len);
  if(buf_len<min_buf_len)
    buf_len+=in->try_read(buffer+buf_len,min_buf_len-buf_len);  
}

CToken CCodeLexer::get_next(){
  if(reserve_token!=0){
    CToken c=reserve_token;
    reserve_token=CToken(0);
    return c;
  }
  if(buf_len==0)
    if(in->bad()){
      handle_error("parsing",in->errormsg());
      return CToken_Error;
    }else
      return CToken_EOF;
  buffer[buf_len]=0;

  //check for a listed token
  void *result=bsearch(buffer,ident_tokens,num_tokens,
		       sizeof(*ident_tokens),compare);
  if(result!=0){
    int r=int((const char **)result-ident_tokens);
    //we found a match - check if there is a better one below it
    do {
      r++;
    }while(ident_tokens[r]!=0&&compare(buffer,&ident_tokens[r])==0);
    move_buffer(int(strlen(ident_tokens[r-1])));
    return CToken(r+CToken_BASE-1);
  }


  CToken c=CToken(buffer[0]);

  //nothing interesting - return this token
  move_buffer(1);
  return c;
}


CToken CCodeLexer::next(){
  for(;;){
    if(bad()) return now=CToken_Error;
    CToken c=get_next();

    //check for whitespace
    if(c==' '||c=='\n'||c=='\t')
      continue;
    
    //check for identifier
    if((c>='a'&&c<='z')||(c>='A'&&c<='Z')||c=='_'||c=='$'){
      int l=0;
      if(result_buffer!=0)
	delete[] result_buffer;
      result_buffer=new char[max_result_len];
      do {
	result_buffer[l++]=char(c);
	if(l>=max_result_len){
	  handle_error("parsing","identifier too long");
	  return now=CToken_Error;
	}	
	for(;;){
	  c=get_next();
	  if(c<CToken_BASE)
	    break;
	  const char *s=ident_tokens[c-CToken_BASE];
	  char d=s[0];
	  if((d>='a'&&d<='z')||(d>='A'&&d<='Z')||(d>='0'&&d<='9')||d=='_'||d=='$'){
	    int len=int(strlen(s));
	    memcpy(result_buffer+l,s,len);
	    l+=len;
	  }	    
	}
      }while((c>='a'&&c<='z')||(c>='A'&&c<='Z')||(c>='0'&&c<='9')||c=='_'||c=='$');
      reserve_token=c;
      result_buffer[l]=0;
      return now=CToken_Ident;
    }

    //check for comment
    if(c=='/'){
      c=get_next();
      if(c=='/'){
	//comment until end of line
	do {
	  c=get_next();	  
	}while(c!='\n'&&c!=CToken_EOF&&c==CToken_Error);
	continue;
      }else if(c=='*'){
	/* comment until */
	do {
	  c=get_next();	  
	  if(c=='*'&&(c=get_next())=='/')
	    break;
	}while(c!=CToken_EOF&&c!=CToken_Error);
	continue;
	
      }else {
	reserve_token=c;
	return now=CToken('/');
      }
    }//no comment

    //check for string
    if(c=='\"'){
      LongString l;
      for(;;){
	char n=0;
	switch(c=get_next()){
	case '\"':
	  if(result_buffer!=0)
	    delete[] result_buffer;
	  result_buffer=l.release();
	  return now=CToken_String;
	case CToken_EOF:
	  handle_error("parsing","String without end");
	case CToken_Error:
	  return now=CToken_Error;
	case '\\':
	  switch(c=get_next()){
	  case 'A':case 'a':n='\a';break;
	  case 'B':case 'b':n='\b';break;
	  case 'E':case 'e':n=27;  break;
	  case 'F':case 'f':n='\f';break;
	  case 'N':case 'n':n='\n';break;
	  case 'R':case 'r':n='\r';break;
	  case 'T':case 't':n='\t';break;
	  case 'V':case 'v':n='\v';break;
	  case 'U':case 'u':
	    handle_error("parsing","Sorry - unicode not yet supported");
	    return now=CToken_Error;
	  case 'X':case 'x':
	    for(int i=0;i<2;i++){
	      c=get_next();
	      int d;
	      if(c>='0'&&c<='9')
		d=c-'0';
	      else if(c>='a'&&c<='f')
		d=c-'a'+10;
	      else if(c>='A'&&c<='F')
		d=c-'A'+10;
	      else{
		handle_error("parsing","Invalid \\xhex data in string");
		return now=CToken_Error;
	      }
	      n=(n<<4)+d;
	    }
	    break;
	  case '\\':
	  case '\"':
	  case '\'':
	    n=c;
	    break;
	  default:
	    handle_error("parsing","Unknown \\? construction in string");
	    return now=CToken_Error;
	  }	  
	  c=CToken(n);
	  //no break
	default:
	  l.append(char(c));
	}
      }
    }
    return now=c;
  }
}

// if get() returns t, eat it and return true; else return false
bool CCodeLexer::have(CToken t){
  if(get()==t){
    next();
    return true;
  }
  return false;
}

// if(!have(x)) error()
void  CCodeLexer::need(CToken t){
  if(get()==t)
    next();
  else{
    char *s=strdup(token_name(get()));
    try{
    handle_error("parsing","needed %s, but found %s",token_name(t),s);
    }catch(...){
      free(s);
      throw;
    }
    free(s);
  }
}

const char *CCodeLexer::token_name(CToken t) const{
  switch(t){
  case CToken_Ident:return "an identifier";
  case CToken_EOF:return "end of file";
  case CToken_Error:return "an error while reading";
  case CToken_String:return "a string";
  case CToken_Integer: return "an integer";
  case CToken_Double: return "a floating point number";
  case CToken_Comment: return "a comment";
  default:
    if(t<256){
      static char buf[]="'?'";
      buf[1]=char(t);
      return buf;
    }else if(t>=CToken_BASE&&t<CToken_BASE+num_tokens){
      return ident_tokens[t-CToken_BASE];
    }else
      return "an invalid token (this can't happen)";
  }
}
///////////////////////////////////////////////////////////////////////////////
//Everywhere, LE stands for little endian (Intel) and BE for big endian
#ifdef BYTESEXLE
#define MAKE_LE16(x) 
#define MAKE_BE16(x) swap16(x)
#define MAKE_LE32(x)
#define MAKE_BE32(x) swap32(x)
#else 
#ifdef BYTESEXBE
#define MAKE_LE16(x) swap16(x)
#define MAKE_BE16(x)
#define MAKE_LE32(x) swap32(x)
#define MAKE_BE32(x)
#else
#error BYTESEX not defined
#endif
#endif
inline static void swap16(uint16& x){
  union {
    uint16 x16;
    uint8 x8[2];
  }xx;
  xx.x16=x;
  SWAP(uint8,xx.x8[0],xx.x8[1]);
}

inline static void swap32(uint32& x){
  union {
    uint32 x32;
    uint8 x8[4];
  }xx;
  xx.x32=x;
  SWAP(uint8,xx.x8[0],xx.x8[3]);
  SWAP(uint8,xx.x8[1],xx.x8[2]);
}


BinaryInputReader::BinaryInputReader(InputStream *i,bool manage)
  :ErrorCreator(i->name()){
  in=i;
  managed=manage;
}

BinaryInputReader::~BinaryInputReader(){
  if(managed)
    delete in;
}

uint8 BinaryInputReader::readbe8(){
  return readle8();
}

uint8 BinaryInputReader::readle8(){
  uint8 d;
  in->read(&d,sizeof(d));
  if(in->bad())
    handle_error(in->error()->clone());
  return d;
}

uint16 BinaryInputReader::readle16(){
  uint16 d;
  in->read(&d,sizeof(d));
  if(in->bad())
    handle_error(in->error()->clone());
  MAKE_LE16(d);
  return d;
}

uint16 BinaryInputReader::readbe16(){
  uint16 d;
  in->read(&d,sizeof(d));
  if(in->bad())
    handle_error(in->error()->clone());
  MAKE_BE16(d);
  return d;
}

uint32 BinaryInputReader::readle32(){
  uint32 d;
  in->read(&d,sizeof(d));
  if(in->bad())
    handle_error(in->error()->clone());
  MAKE_LE32(d);
  return d;
}

uint32 BinaryInputReader::readbe32(){
  uint32 d;
  in->read(&d,sizeof(d));
  if(in->bad())
    handle_error(in->error()->clone());
  MAKE_BE32(d);
  return d;
}

uint8 *BinaryInputReader::readbe8(void *data,int len){
  return readle8(data,len);
}

uint8 *BinaryInputReader::readle8(void *data,int len){
  in->read(data,len);
  if(in->bad())
    handle_error(in->error()->clone());
  return (uint8*)data;
}

uint16 *BinaryInputReader::readle16(void *data,int len){
  in->read(data,len*sizeof(uint16));
  if(in->bad())
    handle_error(in->error()->clone());
  uint16 *result=(uint16 *)data;
  while(len-->0){
    MAKE_LE16(*result);
    result++;
  }
  return (uint16*)data;
}

uint16 *BinaryInputReader::readbe16(void *data,int len){
  in->read(data,len*sizeof(uint16));
  if(in->bad())
    handle_error(in->error()->clone());
  uint16 *result=(uint16 *)data;
  while(len-->0){
    MAKE_BE16(*result);
    result++;
  }
  return (uint16*)data;
}

uint32 *BinaryInputReader::readle32(void *data,int len){
  in->read(data,len*sizeof(uint32));
  if(in->bad())
    handle_error(in->error()->clone());
  uint32 *result=(uint32 *)data;
  while(len-->0){
    MAKE_LE32(*result);
    result++;
  }
  return (uint32*)data;
}

uint32 *BinaryInputReader::readbe32(void *data,int len){
  in->read(data,len*sizeof(uint32));
  if(in->bad())
    handle_error(in->error()->clone());
  uint32 *result=(uint32 *)data;
  while(len-->0){
    MAKE_BE32(*result);
    result++;
  }
  return (uint32*)data;
}
/////Binary Output/////////////////////////////////////////////////////////////
BinaryOutputWriter::BinaryOutputWriter(OutputStream *i,bool manage)
  :ErrorCreator(i->name()){
  out=i;
  managed=manage;
}

BinaryOutputWriter::~BinaryOutputWriter(){
  if(managed)
    delete out;
}

void BinaryOutputWriter::writele8(uint8 d){
  out->write(&d,sizeof(d));
  if(out->bad())
    handle_error(out->error()->clone());
}

void BinaryOutputWriter::writele16(uint16 d){
  MAKE_LE16(d);
  out->write(&d,sizeof(d));
  if(out->bad())
    handle_error(out->error()->clone());
}

void BinaryOutputWriter::writele32(uint32 d){
  MAKE_LE32(d);
  out->write(&d,sizeof(d));
  if(out->bad())
    handle_error(out->error()->clone());
}
  
void BinaryOutputWriter::writebe8(uint8 d){
  writele8(d);
}
void BinaryOutputWriter::writebe16(uint16 d){
  MAKE_BE16(d);
  out->write(&d,sizeof(d));
  if(out->bad())
    handle_error(out->error()->clone());
}
void BinaryOutputWriter::writebe32(uint32 d){
  MAKE_BE32(d);
  out->write(&d,sizeof(d));
  if(out->bad())
    handle_error(out->error()->clone());
}
  
void *BinaryOutputWriter::writele8(uint8 *dataptr,int num_bytes){
  out->write(dataptr,sizeof(*dataptr)*num_bytes);
  if(out->bad())
    handle_error(out->error()->clone());
  return dataptr;
}

void *BinaryOutputWriter::writele16(uint16 *dataptr,int num_words){
  for(int i=num_words;i>0;i--){
    MAKE_LE16(*dataptr);
    dataptr++;
  }
  dataptr-=num_words;
  out->write(dataptr,sizeof(*dataptr)*num_words);
  if(out->bad())
    handle_error(out->error()->clone());
  return dataptr;
}

void *BinaryOutputWriter::writele32(uint32 *dataptr,int num_dwords){
  for(int i=num_dwords;i>0;i--){
    MAKE_LE32(*dataptr);
    dataptr++;
  }
  dataptr-=num_dwords;
  out->write(dataptr,sizeof(*dataptr)*num_dwords);
  if(out->bad())
    handle_error(out->error()->clone());
  return dataptr;
}
void *BinaryOutputWriter::writebe8(uint8 *dataptr,int num_bytes){
  return writele8(dataptr,num_bytes);
}

void *BinaryOutputWriter::writebe16(uint16 *dataptr,int num_words){
  for(int i=num_words;i>0;i--){
    MAKE_BE16(*dataptr);
    dataptr++;
  }
  dataptr-=num_words;
  out->write(dataptr,sizeof(*dataptr)*num_words);
  if(out->bad())
    handle_error(out->error()->clone());
  return dataptr;
}

void *BinaryOutputWriter::writebe32(uint32 *dataptr,int num_dwords){
  for(int i=num_dwords;i>0;i--){
    MAKE_BE32(*dataptr);
    dataptr++;
  }
  dataptr-=num_dwords;
  out->write(dataptr,sizeof(*dataptr)*num_dwords);
  if(out->bad())
    handle_error(out->error()->clone());
  return dataptr;
}
///////////////////////////////////////////////////////////////////////////////
Directory::SubdirData::~SubdirData(){
  free(name);
  delete reader;
}

Directory::SubdirData::SubdirData(const char *s,FSReader *r){
  name=strdup(s);
  reader=r;
}
///////////////////////////////////////////////////////////////////////////////
FSReader::FSReader()
  :ErrorCreator("File system"){
}

FSReader::~FSReader(){
}
///////////////////////////////////////////////////////////////////////////////
FSReader *FSDirRecognize(FSReader *r,const char *subdir){
  FSDirReader *parent=dynamic_cast<FSDirReader *>(r);
  if(parent==0) 
    return 0; //subdir of something else than a dir
  LongString fulldir;
  fulldir.append(parent->abspath());
  fulldir.append('/');
  fulldir.append(subdir);
  //is it a directory ?
  struct stat stats;
  stat(fulldir.result(),&stats);  
  if(!S_ISDIR(stats.st_mode))
    return 0;
  //ok
  return new FSDirReader(fulldir.result());
}


FSDirReader::FSDirReader(const char *src){
#ifdef _WIN32
	source=0;
	mounted=0;
#else
  source=strdup(src);
  //try to mount it
  LongString cmd;
  cmd.append("/bin/mount ");
  cmd.append(src);
  cmd.append("> /dev/zero 2> /dev/zero");
  int n=system(cmd.value());
  mounted=n==0;
#endif
}

FSDirReader::~FSDirReader(){
#ifndef _WIN32
  if(mounted){
    LongString cmd;
    cmd.append("/bin/umount ");
    cmd.append(source);
    int n=system(cmd.value());
    if(n!=0)
      warn("Could not unmount",cmd.value());
  }
#endif
  free(source);
}

void FSDirReader::read(vector<FileData*>& result,const char * mask){
  DIR *dir=opendir(source);
  if(dir==0){
    handle_error("Could not open directory.");
    return ;
  }
  for(;;){
    errno=0;
    dirent *entry=readdir(dir);
    if(entry==0&&errno==0){
      closedir(dir);
      return;
    }
    if(entry==0){
      closedir(dir);
      handle_error("Could not read directory entry.");
      return ;
    }
    if(!fl_filename_match(entry->d_name,mask)||
       strcmp(entry->d_name,".")==0||
       strcmp(entry->d_name,"..")==0)
      continue;
    FileData *fd=new FileData();
    fd->name=strdup(entry->d_name);

    LongString name;
    name.append(source);
    name.append('/');
    name.append(fd->name);

    struct stat stats;
    stat(name.result(),&stats);

    if(S_ISDIR(stats.st_mode))
      fd->flags=DFL_DIR;
    else
      fd->flags=DFL_FILE;
    if(entry->d_name[0]=='.')
      fd->flags|=DFL_HIDDEN;
    fd->atime=stats.st_atime;
    fd->mtime=stats.st_mtime;
    fd->ctime=stats.st_ctime;
    fd->size=stats.st_size;    
    result.push_back(fd);
  }
}

OutputStream* FSDirReader::writeto(const char *s){
    LongString name;
    name.append(source);
    name.append('/');
    name.append(s);
    return new FileOutputStream(name.result());
}

OutputStream* FSDirReader::create(const char *s){
    LongString name;
    name.append(source);
    name.append('/');
    name.append(s);
    return new FileOutputStream(name.result()); 
}

InputStream* FSDirReader::readfrom(const char *s){
    LongString name;
    name.append(source);
    name.append('/');
    name.append(s);
    return new FileInputStream(name.result());
}
///////////////////////////////////////////////////////////////////////////////
Directory::Directory()
  :ErrorCreator("(directory)"){
  my_show_hidden=false;
  my_order_backwards=false;
  my_order=NAME;


  //initialize with the current directory
  char *thisdir;
  int thisdirlen=128;
  for(;;){
    thisdir=(char *)malloc(thisdirlen);
    char *result=getcwd(thisdir,thisdirlen);
    if(result==0&&errno==ERANGE){
      free(thisdir);
      thisdirlen+=128;
      continue;
    }
    if(result!=0)
      break;
    free(thisdir);
    handle_error("Can't go to current directory.");
    return;
  }
  chdir(thisdir);
  free(thisdir);
}

Directory::~Directory(){
  for(int i=0;i<(int)subdir.size();i++)
    delete subdir[i];
}

/*
  change the cwd, relatively tot the curent one.
  return true if path was a valid path.  if false, then the path may be
  changed to the valid part of the given directory
*/
bool Directory::chdir(const char *dir){  
  //split in parts having a '/'
  const char *end;
  if(*dir==0) return false;
  bool b=true;
  do{
    //chdir to the next part
    if(*dir==0)
      break;
    end=strchr(dir,'/');
    if(end==dir){
      b=chdir_part("");
      dir++;
    }else if(end==0){
      b=chdir_part(dir);
    }else {
      char *s=strdup(dir);
      s[end-dir]=0;
      b=chdir_part(s);
      free(s);
      dir=end+1;
    }
  }while(end!=0&&b);

  //rebuild cwd string;
  my_cwd.clear();
  for(int i=0;i<(int)subdir.size();i++){
    my_cwd.append(subdir[i]->name);
    if(i!=(int)subdir.size()-1)
      my_cwd.append("/");
  }
  return b;
}

Recognize recognized[]={
  FSDirRecognize
};


/*
  chdir("/a/b/c"); will call chdir_part("a");chdir_part("b");chdir_part("c");
  return true if path was a valid subdirectory
*/
bool Directory::chdir_part(const char *path){
  if(strcmp(path,".")==0) //cd .
    return true;

  if(strcmp(path,"..")==0){ //cd ..
    delete subdir[subdir.size()-1];
    subdir.pop_back();
    return true;
  }

  if(path[0]==0||subdir.size()==0){ //cd /
    while(subdir.size()!=0){
      delete subdir[subdir.size()-1];
      subdir.pop_back();
    }
    subdir.push_back(new SubdirData("",new FSDirReader("/")));
    return true;
  }

  if(path[0]=='~'){
    char *target=0;
    if(path[1]==0){
      target=getenv("HOME");
    }else{
      passwd *pass=getpwnam(path+1);
      if(pass!=0)
	target=pass->pw_dir;
    }
    if(target!=0)
      return chdir(target);
    return false;
  }
  
  FSReader *d=subdir[subdir.size()-1]->reader;

  for(int i=0;i<(int)UBOUND(recognized);i++){ //cd something
    FSReader* r=recognized[i](d,path);
    if(r==0)
      continue;    
    subdir.push_back(new SubdirData(path,r));
    return true;
  }
  return false;
}

void Directory::cleanup(vector<FileData*>& data){
  for(int i=0;i<(int)data.size();i++){
    free(data[i]->name);
    delete data[i];
  }
  data.clear();
}

void Directory::read(vector<FileData*>& data,const char *mask,int level){
  cleanup(data);
  if(level>=(int)subdir.size())
    return;
  if(level==-1)
    level=int(subdir.size())-1;
  subdir[level]->reader->read(data,mask);

  //remove hidden files
  if(!my_show_hidden)
    for(int i=0;i<(int)data.size();)
      if((data[i]->flags&DFL_HIDDEN)!=0){
	free(data[i]->name);
	delete data[i];
	data.erase(data.begin()+i);
      }else
	i++;

  //sort.  This hsould be done better.
  int l=int(data.size())-1;
  bool changed;
  do{
    changed=false;
    for(int i=0;i<l;i++){
      //default ordering is by name
      bool swapthem=strcasecmp(data[i]->name,data[i+1]->name)>0;
      switch(my_order){
      case NAME:
	break;
      case EXTENSION:
	{
	  char *ext1=strchr(data[i]->name,'.');
	  char *ext2=strchr(data[i]->name,'.');
	  if(ext1==0&&ext2==0)
	    break;
	  if(ext1==0){
	    swapthem=false;
	    break;
	  }
	  if(ext2==0){
	    swapthem=true;
	    break;
	  }
	  int n=strcmp(ext1,ext2);
	  if(n>0) swapthem=true;
	  if(n<0) swapthem=false;
	}
	break;
      case MTIME:
	if(data[i]->mtime<data[i+1]->mtime)
	  swapthem=false;
	else if(data[i]->mtime>data[i+1]->mtime)
	  swapthem=true;
	break;
      case CTIME:
	if(data[i]->ctime<data[i+1]->ctime)
	  swapthem=false;
	else if(data[i]->ctime>data[i+1]->ctime)
	  swapthem=true;
	break;
      case ATIME:
	if(data[i]->atime<data[i+1]->atime)
	  swapthem=false;
	else if(data[i]->atime>data[i+1]->atime)
	  swapthem=true;
	break;
      }
      if(swapthem^my_order_backwards){
	changed=true;
	SWAP(FileData *,data[i],data[i+1]);
      }
    }
  }while(changed);
}
