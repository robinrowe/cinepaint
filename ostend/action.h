#ifndef _X_ACTION_H_
#define _X_ACTION_H_
#include "canvas.h"

//an action returns 0, or an errormessage. We'll get rid of that message
//with delete[].  Don't give anything for x or y. They are there for ActionUndo

//rsr
//typedef char *(Action)(Canvas *,int x=0,int y=0);
typedef char *(Action)(Canvas *,int x,int y);
void resize_fast(Canvas *c,int ex_w,int ex_h,Pixel *from);
void resize_good(Canvas *c,int ex_w,int ex_h,Pixel *from);
char *action_resize(Canvas *c,int x=0,int y=0);
char *action_mirror_lr(Canvas *c,int x=0,int y=0);
char *action_mirror_ud(Canvas *c,int x=0,int y=0);
char *action_mirror_diag(Canvas *c,int x=0,int y=0);
char *action_rotate_90(Canvas *c,int x=0,int y=0);
char *action_rotate_180(Canvas *c,int x=0,int y=0);
char *action_rotate_270(Canvas *c,int x=0,int y=0);
char *action_roll(Canvas *c,int splitx,int splity);

class ActionUndo:public Undo{
  Action *act;
  int arg1,arg2;
 public:
  virtual void undo(Canvas *c);
  ActionUndo(Action *a,int argx=0,int argy=0){act=a;arg1=argx;arg2=argy;}
  virtual ~ActionUndo();
};
#endif
