/*
  canvas (c) 2002 Yperman Hans
*/
#ifndef  _X_IMAGE_H_
#define  _X_IMAGE_H_
#include <stdlib.h>
#include <string.h>
#include "pixel.h"
#include "util.h"
#include "broadcaster.h"

struct  Point{
  int x,y;
  inline Point()
    {   x = y = 0;
    }
  inline Point(int xx,int yy):x(xx),y(yy){};
};
class Canvas;

//Undo remembers how to undo an action
//len should be set to know when to remove undo's from the undo list
class Undo {
 protected:
  int len_;
 public:
  Undo();
  virtual ~Undo();
  int len(){return len_;};
  virtual void undo(Canvas *i)=0;

};

struct Row{
	uint16 start,len;
	Pixel *data;
	Row *next;
};
  
//Undo a shadowbuffer operation.
class ShadowUndo:public Undo {
  int w,h;
  Row **row;
 public:
  ShadowUndo(const Canvas *c);
  virtual ~ShadowUndo();
  virtual void undo(Canvas *i);
};

//Undo an operation that changed the entire image
class ImageUndo:public Undo {
  int w,h;
  Pixel *data;
 public:
  virtual void undo(Canvas *c);
  ImageUndo(int w,int h,Pixel *data);
  virtual ~ImageUndo();
};

//Canvas is the thing we paint on
class Canvas {
  Broadcaster ondelete,onpaint;
  Pixel *data;
  bool changed; //check me if you want to save

  void *parameters;
  int num_parameters;
  //char *my_name,*my_file_name;

  int w,h;
  int undo_count,undo_index;  
  Undo **undo;

  int dirty_x1,dirty_x2,dirty_y1,dirty_y2;
  int orig_x,orig_y;

  //shadow undo
  uint8* shadow_buffer;
  Pixel **original_line;
 protected:
  static void pixelsetter(int x,int y,void *data);
 public:
  Canvas(int w,int h);
  ~Canvas();
  //what's the difference ??
  Pixel *new_image(int w,int h,Pixel *data=0);
  //void image(int w,int h,Pixel *data);  

  void undo_action();
  void add(Undo *u);

  //[]gs] of parameters
  inline int width() const{return w;}
  inline int height()const{return h;}
/*
  inline char *name()const{return my_name;}
  inline void name(const char *s){free(my_name);my_name=strdup(s);
  inline char *filename()const{return my_file_name;}
  inline void filename(const char *s){
    free(my_file_name);
    my_file_name=strdup(s);
  }
*/
  void param(const char *name,const char *value);
  const char *param(const char *name);
  inline const char *paramdefault(const char *name,const char *deflt){
    const char *s=param(name);
    if(s==0)
      return deflt;
    else
      return s;
  }

  inline bool saveme(){return changed;}
  inline void saveme(bool b){changed=b;}
  inline void base(int x,int y){orig_x=x;orig_y=y;}
  inline int base_x() const {return orig_x;}
  inline int base_y() const {return orig_y;}

  inline Broadcaster& on_delete(){return ondelete;}
  inline Broadcaster& on_paint(){return onpaint;}

  //ask the value of a pixel of the image
  //it is guaranteed that pixels within a line have following adresses
  inline Pixel &pixel(int x,int y)const{return data[w*y+x];}
  Pixel *image(){return data;} 

  //lines should be dirty() before adding shadow to them
  //shadow_add_mask does this also, shadow does not
  void shadow_add_mask(int x,int y,uint8 *mask,int size);
  inline uint8 &shadow(int x,int y) const {return shadow_buffer[w*y+x];}
  void del_shadow();
  void undo_shadow();

  //ask the original value of a pixel of the image
  //it is guaranteed that pixels within a row have following adresses  
  inline Pixel &original(int x,int y) const {
    if(original_line[y]==0)
      return data[w*y+x];
    else
      return original_line[y][x];
  }
  void dirty(int x,int y,int x2,int y2);
  void undirty();
  inline void dirty(){dirty(0,0,w,h);}

  inline bool line_changed(int y) const{return original_line[y]!=0;}
  inline void isdirty(int& x1,int& y1,int& x2,int& y2) const{
    x1=dirty_x1;y1=dirty_y1;x2=dirty_x2;y2=dirty_y2;
  }

  //call actibvate before using an image. call deactivate after it
  void activate();
  void deactivate();
};

//Paint defines the thing that is drawn. The foreground color is a Paint
class Paint;
//Pen gives shape and pressure of the Pen
class Pen ;
//Tool is a line, a point, a rectangle,...
class Tool;
#endif
