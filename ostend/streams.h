#ifndef _X_FILESTREAMS_H_
#define _X_FILESTREAMS_H_
#include <stdlib.h>
#include <vector>
#include "time.h"
#include "util.h"

using namespace std;

/////Error handling////////////////////////////////////////////////////////////
class ErrorCreator;
class StreamError{
  char *emsg;
  ErrorCreator *src;  
  StreamError(ErrorCreator  *source);
 public:
  //structors
  StreamError(ErrorCreator  *source,const char *msg,...);
  virtual ~StreamError();  

  //[gs]etters
  inline char *message(){return emsg;}
  inline ErrorCreator *source(){return src;}
  inline void source(ErrorCreator *e){src=e;}

  //workers
  virtual StreamError *clone();
};

typedef void (ErrorHandler)(StreamError *);

class ErrorCreator {
  StreamError *last_error;
  ErrorHandler *eh;
  const char *name_;
 public:
  //structors
  ErrorCreator(const char *name);
  virtual ~ErrorCreator();

  //[gs]etters
  void on_error(ErrorHandler *e){eh=e;}
  ErrorHandler  *on_error() const {return eh;}
  inline bool bad() const {return last_error!=0;}
  inline const char *errormsg() const {return last_error->message();}
  inline StreamError *error(){return last_error;}
  inline const char *name() const {return name_;}

  //workers
  void handle_error(const char *action); //based on errno
  void handle_error(const char *action,const char * message,...);
  inline void handle_error(StreamError *s){s->source(this);eh(s);}

  //default error functions
  static void default_error_handler(StreamError *);
#pragma warning( disable : 4290 )
  static void throw_error_handler(StreamError *);// throw(StreamError *);
};

/////Input streams//////////////////////////////////////////////////////////////
class InputStream:public ErrorCreator{
 protected:
  int pos;
 public:
  //structors
  InputStream(const char *name);
  virtual ~InputStream();
  
  //workers
  void read(void *buf,int exact_len);

  //interface
  virtual int try_read(void *buf,int max_len)=0;
  virtual int position() const =0;
  virtual void position(int p)=0;  
  virtual void close()=0;
  //get one for free
  void *user_data;
};

class FileInputStream:public InputStream{
 protected:
  int fd;
 public:
  //structors
  FileInputStream(const char *name);
  virtual ~FileInputStream();

  //workers
  virtual int try_read(void *buf,int len);
  virtual int position() const;
  virtual void position(int p);
  virtual void close();
};

class BufferInputStream:public InputStream {
 private:
  InputStream *in;
  char *buffer;  
  int buffer_start;
  int buffer_len;    //valid data in buffer
  static const int buffer_space=1600;  //max dataspace before we need to realloc
  int buffer_pos;    //position retuned by next read
 public:
  //structors
  BufferInputStream(InputStream *i);
  virtual ~BufferInputStream();

  //workers
  virtual int try_read(void *buf,int len);
  virtual int position() const;
  virtual void position(int p);
  virtual void close();  
};
/////Output stream ////////////////////////////////////////////////////////////
class OutputStream:public ErrorCreator{
 protected:
  int pos;
 public:
  //structors
  OutputStream(const char *name);
  virtual ~OutputStream();
  
  //workers
  void write(void *buf,int exact_len);
  void printf(const char *s,...);
  //interface
  virtual int try_write(void *buf,int max_len)=0;
  virtual int position() const =0;
  virtual void position(int p)=0;  
  virtual void close()=0;
  //get one for free
  void *user_data;
};

class FileOutputStream:public OutputStream{
 protected:
  int fd;
 public:
  //structors
  FileOutputStream(const char *name);
  virtual ~FileOutputStream();

  //workers
  virtual int try_write(void *buf,int len);
  virtual int position() const;
  virtual void position(int p);
  virtual void close();
};


/////C Code Reader/////////////////////////////////////////////////////////////
enum CToken{
  CToken_First=0, //start of 256 ASCII chars
  CToken_Ident=256,
  CToken_EOF,
  CToken_Error,
  CToken_String,
  CToken_Integer, //not yet
  CToken_Double,  //not yet
  CToken_Comment, //not yet
  CToken_BASE=1024,//User defined start here
  CToken_LAST=65535
};

class CCodeLexer:public ErrorCreator {
 private:
  InputStream *in;
  CToken now,reserve_token;
  bool manage_;
  const char **ident_tokens;
  int num_tokens;
  int min_buf_len,buf_len;
  char *buffer;

  static const int max_result_len=256; //max len of ident
  char *result_buffer;

  CToken get_next();
  static int compare(const void *,const void *);
  void move_buffer(int);
 public:
  //structors
  CCodeLexer(InputStream *i,const char **tokens,bool managed=true);
  virtual ~CCodeLexer();

  //workers
  const char *token_name(CToken) const;

  inline CToken get() const{return now;}
  CToken next();

  //have & need
  bool have(CToken t);
  inline bool have(char c){return have(CToken(c));}
  void need(CToken t);
  inline void need(char c){need (CToken(c));}  

  //parameters of result
  const char *cstring() const {return result_buffer;}
};
///////////////////////////////////////////////////////////////////////////////
class BinaryInputReader:public ErrorCreator {
  InputStream *in;
  bool managed;
 public:

  //structors
  BinaryInputReader(InputStream *i,bool manage=true);
  virtual ~BinaryInputReader();

  //workers
  uint8  readle8() ;
  uint16 readle16();
  uint32 readle32();
  
  uint8  readbe8();
  uint16 readbe16();
  uint32 readbe32();
  
  uint8  *readle8(void *dataptr,int num_bytes);
  uint16 *readle16(void *dataptr,int num_words);
  uint32 *readle32(void *dataptr,int num_dwords);  

  uint8  *readbe8(void *dataptr,int num_bytes);
  uint16 *readbe16(void *dataptr,int num_words);
  uint32 *readbe32(void *dataptr,int num_dwords);  
};

///////////////////////////////////////////////////////////////////////////////
class BinaryOutputWriter:public ErrorCreator {
  OutputStream *out;
  bool managed;
 public:

  //structors
  BinaryOutputWriter(OutputStream *o,bool manage=true);
  virtual ~BinaryOutputWriter();

  //workers, le=little endian, be=big endian
  void writele8(uint8) ;
  void writele16(uint16);
  void writele32(uint32);
  
  void writebe8(uint8);
  void writebe16(uint16);
  void writebe32(uint32);
  
  //in the given pointer, bytes may be swapped as needed.
  void *writele8(uint8 *dataptr,int num_bytes);
  void *writele16(uint16 *dataptr,int num_words);
  void *writele32(uint32 *dataptr,int num_dwords);  

  void *writebe8(uint8 *dataptr,int num_bytes);
  void *writebe16(uint16 *dataptr,int num_words);
  void *writebe32(uint32 *dataptr,int num_dwords);  
};
///////////////////////////////////////////////////////////////////////////////
struct FileData {
  char *name;
  int flags;
  int size;
  time_t atime,mtime,ctime;
  FileData()
  {	memset(this,0,sizeof(*this));
  }
  ~FileData()
  {	delete [] name;
	memset(this,0,sizeof(*this));
  }
};

#define DFL_FILE      1
#define DFL_DIR       2
#define DFL_HIDDEN    4
enum Order{
  NAME,EXTENSION,MTIME,CTIME,ATIME
};

//This represents one open directory
class FSReader:public ErrorCreator {
 private:
  //friend class Directory;
/*  char *datasrc;
  char *workingdir;
  DirectoryReader *parentreader,*childreader;
*/
public:
  FSReader();
  virtual ~FSReader();

  virtual void read(vector<FileData*>& data,const char *mask)=0;
  virtual OutputStream* writeto(const char *s)=0;
  virtual OutputStream* create(const char *s)=0;
  virtual InputStream* readfrom(const char *s)=0; 
};


class FSDirReader:public FSReader{
  char *source;
  bool mounted;
public:
  FSDirReader(const char *src);
  const char *abspath() const{return source;}
  virtual ~FSDirReader();
  virtual void read(vector<FileData*>& data,const char *mask);
  virtual OutputStream* writeto(const char *s);
  virtual OutputStream* create(const char *s);
  virtual InputStream* readfrom(const char *s); 
  //virtual bool isdir(const char *s);
};

typedef FSReader *(*Recognize)(FSReader *d,const char *subdir);

// this one is for the application

class Directory:public ErrorCreator{
  class SubdirData{
  public:
    SubdirData(const char *s,FSReader *r);
    ~SubdirData();
    char *name;
    FSReader *reader;
  };
  vector<SubdirData*> subdir;
  LongString my_cwd;
  bool chdir_part(const char *subdir);
  Order my_order;
  bool my_show_hidden,my_order_backwards;
 public:
  Directory();
  ~Directory();

  //the curent working directory (cwd)
  bool chdir(const char *subdir); //relative to current cwd
  inline void order(Order o){my_order=o;}
  inline void showhidden(bool b){my_show_hidden=b;}
  inline void order_backwards(bool b){my_order_backwards=b;}
  inline bool order_backwards(){return my_order_backwards;}
  const char *cwd(){return my_cwd.value();};

  //levels in the directry tree
  const char *level(int n) const{
    if(n>=(int)subdir.size())
      return 0;
    else
      return subdir[n]->name;
  }
  void cleanup(vector<FileData*>& data);
  void read(vector<FileData*>& data,const char *mask,int level=-1);

  //file access - return 0 if error
  OutputStream* writeto(const char *s);
  OutputStream* create(const char *s);
  InputStream* readfrom(const char *s);
};

#endif

