// file.cpp
// look up file type of image file, image loaders
// bmp dib jfif jpg jpeg png xpm
// these should all be plug-ins

#include <unordered_map>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <setjmp.h>

#if 0
#include <png.h>
#define XMD_H
extern "C" { //What ?? !�)���'�!!! Yaarg!!!! Unbelievable!  
#include <jpeglib.h>
}
#undef XMD_H
#endif

#ifdef _WIN32
#include <malloc.h>
#endif

#include "ui_tools.h"
#include "config.h"
#include "streams.h"
#include "util.h"
#include "file.h"
#include "win_open.h"
///////////////////////////////////////////////////////////////////////////////
Canvas *xpm_loader(InputStream *in){
  Canvas *result=0;
  static const char *tokens[]={"static","char",(char *)0};
  CCodeLexer lex(in,tokens,false);
  try {
    lex.on_error(ErrorCreator::throw_error_handler);
    /*read something like
      static char * whatever [ ] = {
    */
    lex.need(CToken_BASE);
    lex.need(CToken(CToken_BASE+1));
    lex.need('*');
    // we should do lex.need(CToken_Ident);, but some XPMs have
    // - and such in their filename. These are not valid C-source.
    CToken t;
    while(t=lex.get(),t==CToken_Ident||(t<256&&t!=CToken('[')))
      lex.next();

    lex.need('[');
    lex.need(']');
    lex.need('=');
    lex.need('{');
    /*read header info: a string containing the numbers:
      " width height colors cpp [xhot yhot] "
    */
    lex.need(CToken_String);
    int w,h,clr,cpp,xhot,yhot;
    char useless;
    int n=sscanf(lex.cstring()," %d %d %d %d %d %d %c",
		 &w,&h,&clr,&cpp,&xhot,&yhot,&useless);
    if(n!=4&&n!=6){
      in->handle_error("decoding XPM","bad header string");
      return 0;
    }
    if(w<0||h<0||w>65535||h>65535||clr<1||cpp<1||clr>(1<<25)||cpp>4){
      in->handle_error("decoding XPM","invalid value in header");
      return 0;
    }
    //now read colors
    int ch_size=clr*2;
    Pixel *color_hash=new Pixel[ch_size];
    uint32 *cname_hash=new uint32[ch_size];
    memset32(cname_hash,0,ch_size);
    memset32(color_hash,0,ch_size);
    for(int n=0;n<clr;n++){
      lex.need(',');
      lex.need(CToken_String);
      const char *s=lex.cstring();
      //printf("%s\n",s);
      //first get ident
      if(strlen(s)<4){
	delete[] color_hash;delete[] cname_hash;
	in->handle_error("decoding XPM color","color identity too short");
	return 0;
      }
      uint32 color_name=0;
      Pixel color;
      for(int i=0;i<cpp;i++)
	color_name=(color_name<<8)+(*(unsigned const char *)s++);

      for(;;){
	//now, several characters can follow. hope for " c XXXXXX".
	int len;
	char c;
	sscanf(s," %c %n",&c,&len);s+=len;
	if(c=='s'){
	  sscanf(s," %*s%n",&len); s+=len;
	  continue;
	}else if(c!='c'){
	  delete[] color_hash;delete[] cname_hash;
	  in->handle_error("decoding XPM color","color is not of type 'c'");	  
	  return 0;
	}

	//try color None
	sscanf(s,"Non%c",&c);
	sscanf(s,"non%c",&c);
	if(c=='e'){
	  color=0xFFFF00FF;
	  break;
	}
	
	//try 6 and 12 color
	int r,g,b,r2,g2,b2;
	int n=sscanf(s,"#%2x%2x%2x%2x%2x%2x",&r,&g,&b,&r2,&g2,&b2);
	if(n==0){	  
	  delete[] color_hash;delete[] cname_hash;
	  in->handle_error("decoding XPM color",
			   "bad format for %s",lex.cstring());
	  return 0;
	}
	
	if(n>3)
	  color=(r<<RSHIFT)+(b<<GSHIFT)+(g2<<BSHIFT);
	else
	  color=(r<<RSHIFT)+(g<<GSHIFT)+(b<<BSHIFT);
	break;
      }
      //hash that color	
      uint32 d;
      for(d=color_name;cname_hash[d%ch_size]!=0;d++) ;
      color_hash[d%ch_size]=color;
      cname_hash[d%ch_size]=color_name;
    }
    result=new Canvas(w,h);
    for(int y=0;y<h;y++){
      lex.need(',');
      lex.need(CToken_String);
      const char *s=lex.cstring();
      if((int)strlen(s)<cpp*w){
	delete[] color_hash;delete[] cname_hash;
	delete result;
	in->handle_error("decoding XPM data","not enough data for line y=%d",y);
	return 0;	
      }
      for(int x=0;x<w;x++){
	uint32 color_name=0;
	Pixel color;
	for(int i=0;i<cpp;i++)
	  color_name=(color_name<<8)+(*(unsigned const char *)s++);
	color=color_name;
	//dehash that color
	uint32 c;
	for(c=color_name;cname_hash[c%ch_size]!=color_name;c++)
	  if(cname_hash[c%ch_size]==0){
	    delete[] color_hash;delete[] cname_hash;
	    delete result;
	    in->handle_error("decoding XPM data",
			     "unhashed color found at (%d,%d)",x,y);
	    return 0;
	  }
	result->pixel(x,y)=color_hash[c%ch_size];
      }
    }
    delete[] color_hash;
    delete[] cname_hash;
  }catch(StreamError *e){
    if(result!=0)
      delete result;
    in->handle_error(e->clone());
    return 0;
  }
  return result;
}

void xpm_saver(OutputStream *out,Canvas *c){   
  std::unordered_map<Pixel,int> pixelcode;
  vector<Pixel> colormap;
  int base_code=1;
  int w=c->width(),h=c->height();

  //first, calculate an index for each color
  colormap.push_back(0xFF000000); // ' ' is  transparant
  for(int y=0;y<w;y++)
    for(int x=0;x<w;x++){
      Pixel p=c->pixel(x,y);
      //convert transparancy
      if(ADATA(p)>127)
	continue;
      p&=0xFFFFFF;
      //give it an index
      if(pixelcode[p]==0){
	pixelcode[p]=base_code;
	colormap.push_back(p);
	base_code++;
      }
    }
  //colormap will be:
  //transparant -> ' ' (code 0)
  //codes 0 ...  whatever are from ASCII 32 to 124. As the result should be a
  //valid C-string, " is changed to ASCII 125, and \ to ASCII 126. This means there
  //are 124-32+1=93 possible codes for colors. With this in mind, we can
  //calculate the nuber of characters per pixel (cpp).
  // AS 93*93*93*93(=possibilities when cpp=4)>256*256*256+1(possible different number
  // of colors for XPM), cpp=4 is the maximum
  int cpp=1;
  int num_colors=base_code;
  while(base_code>93){
    cpp++;
    base_code/=93;
  }
  if(cpp>4){
    out->handle_error(__FILE__":internal error:cpp>4 is not possible");
    return;
  }
  //write header
  char *name=strdup(out->name());
  int j=0;
  for(int i=0;name[i]!=0;i++){
    if(isalnum(name[i])||name[i]=='_'){
      name[j++]=name[i];
    }else if(name[i]=='/'){
      j=0;
    }else
      name[j++]='_';
  }
  name[j]=0;
  if(j==0){
    free(name);
    name=strdup("image");
  }
  out->printf("/* XPM */\nstatic char* %s[]={\n\"%d %d %d %d\",\n",
	      name,w,h,num_colors,cpp);
  free(name);
  //write colormap
#ifdef _WIN32
  char* buf=(char*) _alloca(5+w*cpp);
#else
  char buf[5+w*cpp];
#endif
  buf[0]=buf[1]=buf[2]=buf[3]=' ';
  buf[cpp]=0;
  out->printf("\"%s c None\",\n",buf);
  for(int i=1;i<num_colors;i++){
    Pixel p=colormap[i];
    int n=i;
    for(int j=0;j<cpp;j++){
      buf[j]=n%93+32;
      n/=93;
      if(buf[j]=='\\')
	buf[j]=125;
      if(buf[j]=='\"')
	buf[j]=126;
    }
    out->printf("\"%s c #%2.2x%2.2x%2.2x\",\n",buf,RDATA(p),GDATA(p),BDATA(p));
  }
  //write image
  buf[0]='\"';
  for(int y=0;y<h;y++){
    char *b=buf+1;
    for(int x=0;x<w;x++){
      Pixel p=c->pixel(x,y);
      int n;
      if(p>=0x80000000)
	n=0;
      else
	n=pixelcode[p&0xFFFFFF];
      //printf("%d ",n);

      for(int j=0;j<cpp;j++){
	*b=n%93+32;
	n/=93;
	if(*b=='\\') *b=125;
	if(*b=='\"') *b=126;
	b++;
      }      
    }
    //now save this row
    out->write(buf,w*cpp+1);
    if(y+1==h)
      out->printf("\"};\n");
    else
      out->printf("\",\n");
  }
  //tadaam.
}

///////////////////////////////////////////////////////////////////////////////

static void bmp_saver(OutputStream *o,Canvas *c){
  BinaryOutputWriter out(o,false);
  uint8 *row=0;
  try{
    out.on_error(ErrorCreator::throw_error_handler);
    //setup header parts we already know
    int bmp_row_len=(3*c->width()+3)&~3;
    out.writele8((uint8 *)"BM",2);
    out.writele32(0);out.writele32(0);out.writele32(0);
    out.writele32(40);out.writele32(c->width());out.writele32(c->height());
    out.writele16(1);out.writele16(24);out.writele32(0);
    out.writele32(bmp_row_len*c->height());
    out.writele32(72);out.writele32(72);
    out.writele32(0);out.writele32(0);
    //write image. Prepare for massive uncompressed data chunk!!
    row=new uchar[bmp_row_len];
    for(int i=0;i<4;i++)
      row[bmp_row_len-4+i]=0;
    int imagedata=o->position();
    for(int y=c->height()-1;y>=0;y--){
      uint8 *r=row;     
      for(int x=0;x<c->width();x++){
	Pixel p=c->pixel(x,y);
	*r++=BDATA(p);*r++=GDATA(p);*r++=RDATA(p);	
      }
      out.writele8(row,bmp_row_len);
    }
    int filesize=o->position();
    o->position(2);
    out.writele32(filesize+3>>2);
    out.writele32(0);
    out.writele32(imagedata);
  }catch(StreamError *e){
    o->handle_error(e->clone());
    if(row!=0)
      delete[] row;
  }

}

static Canvas *bmp_loader(InputStream *i){
  Canvas *result=0;
  Pixel *colormap=0;

  BinaryInputReader in(i,false);
  try{
    in.on_error(ErrorCreator::throw_error_handler);
    if(in.readle8()!='B'||in.readle8()!='M')
      in.handle_error("checking type","This is not a MS DIB/BMP file");
    in.readle32();in.readle32();in.readle32();
    //now is the OS/2 or WIN header. I can only decode WIN
    if(in.readle32()!=40)
      in.handle_error("checking type","This is not the MS windows file type");
    uint32 width=in.readle32(),height=in.readle32();
    if(in.readle16()!=1||width>65535||height>65535)
      in.handle_error("reading header","Invalid value for planes,width or height");
    int bits=in.readle16();
    if(bits!=1&&bits!=4&&bits!=8&&bits!=24)
      in.handle_error("reading header","bits is not 1,4,8 or 24");
    enum {NOCOMP,RLE8,RLE4};
    uint32 compression=in.readle32();
    if(compression>RLE4)
      in.handle_error("reading header","unknown compression type %4x",compression);
    //printf("Compression %d - %d\n",compression,bits);
    in.readle32();in.readle32();in.readle32();
    int colormap_size=in.readle32();in.readle32();
    if(colormap_size==0&&bits!=24)
      colormap_size=1<<bits;

    //read colormap
    if(colormap_size>0)
      colormap=new Pixel[colormap_size];
    for(int i=0;i<colormap_size;i++){
      unsigned int b=in.readle8();
      unsigned int g=in.readle8();
      unsigned int r=in.readle8();
      unsigned int a=in.readle8();
      colormap[i]=ToPixel(r,g,b,a);
    }

    //read image
    result=new Canvas(width,height);
    switch(compression){
    case NOCOMP:
      {
	int bmp_row_len=((bits*width+7)/8+3)&~3;
	uint8 *line=new uint8[bmp_row_len];
	if(bits>8)
	  for(int y=height-1;y>=0;y--){
	    in.readle8(line,bmp_row_len);
	    uint8 *l=line;
	    for(int x=0;x<(int)width;x++){
	      unsigned int b=*l++,g=*l++,r=*l++;
	      result->pixel(x,y)=ToPixel(r,g,b,0);
	    } 
	  }
	else
	  for(int y=height-1;y>=0;y--){
	    in.readle8(line,bmp_row_len);
	    int mask=(1<<bits)-1;
	    int bitshift=8-bits;
	    for(int x=0;x<(int)width;x++){
	      int index=line[x*bits/8];
	      index>>=bitshift;
	      bitshift=(bitshift-bits)&7;
	      result->pixel(x,y)=colormap[index&mask];
	    }
	  }
	delete[] line;
      }
      break;
    case RLE8:
      {
	int x=0,y=height-1;
	for(;;){
	  uchar count=in.readle8();
	  //printf("Count=%d,%d,%d\n",count,x,y);
	  if(count==0){
	    uchar count2=in.readle8();
	    if(count2==0){
	      y--;x=0;
	    }else if(count2==1){
	      break;
	    }else if(count2==2){
	      warn("Hope i understood this part well");
	      x+=in.readle8();
	      y+=in.readle8();
	    }else{
	      int odd_even=count2&1;
	      while(count2-->0)
		result->pixel(x++,y)=colormap[in.readle8()];
	      if(odd_even!=0)
		in.readle8();
	    }
	  }else {
	    uchar data=in.readle8();
	    while(count-->0){
	      result->pixel(x++,y)=colormap[data];
	    }
	  }
	}
      }
      break;
    default:
      in.handle_error("Decoding image","unsupported compression");
    }
  }catch(StreamError *e){
    if(result!=0)
      delete result;
    if(colormap!=0)
      delete colormap;
    i->handle_error(e->clone());
    return 0;
  }
  return result;
}


#if 0
///////////////////////////////////////////////////////////////////////////////
static void pngreadfunc(png_structp pngstruct,png_bytep data,png_size_t length){
  InputStream *i=(InputStream *)png_get_io_ptr(pngstruct);
  i->read(data,int(length));
}

static Canvas *png_loader(InputStream *i){
/*
  //Why does this not work always ????
  static const int header_len=8;
  unsigned char header[header_len];
  i->read(header,header_len);
  if(!png_sig_cmp(header,0,header_len)){
    i->handle_error("Reading png header","File is not a valid .png file");
    return 0;
  }
*/
  png_structp pngstruct=0;
  png_infop pnginfo=0;
  Canvas *canvas=0;
  ErrorHandler *old_eh=i->on_error();
  try {
    i->on_error(ErrorCreator::throw_error_handler);

    //create basic data structures
    pngstruct=png_create_read_struct(PNG_LIBPNG_VER_STRING,0,0,0);
    if(pngstruct==0)
      i->handle_error("Reading png header","Could not create pngstruct ?");
    
    pnginfo=png_create_info_struct(pngstruct);
    if(pnginfo==0)
      i->handle_error("Reading png header","Could not create pnginfo ?");

    //setup error handler
    if(setjmp(png_jmpbuf(pngstruct))){
      png_destroy_read_struct(&pngstruct, &pnginfo,0);
      i->on_error(old_eh);
      i->handle_error("Reading png header","Unknown bad stuff happened");
      return 0;
      if(canvas!=0)
	delete canvas;
    }
    png_set_read_fn(pngstruct,(void *)i,pngreadfunc);
    //ready to read ...
    //png_set_sig_bytes(pngstruct,header_len);
    png_read_png(pngstruct,pnginfo,
		 PNG_TRANSFORM_STRIP_16|PNG_TRANSFORM_PACKING|
		 PNG_TRANSFORM_INVERT_ALPHA,0);
    int w=png_get_image_width(pngstruct,pnginfo);
    int h=png_get_image_height(pngstruct,pnginfo);
    canvas=new Canvas(w,h);
    unsigned char **rowpointers=png_get_rows(pngstruct,pnginfo);
    for(int y=0;y<h;y++){
      unsigned char *from=rowpointers[y];
      Pixel *to=&canvas->pixel(0,y);
      for(int x=0;x<w;x++){ //this could be replaceable by memcpy
	uchar r=*from++;
	uchar g=*from++;
	uchar b=*from++;
	uchar a=*from++;
	*to++=ToPixel(r,g,b,a);
      }
    }
    png_destroy_read_struct(&pngstruct, &pnginfo,0);
    pngstruct=0;pnginfo=0;
    i->on_error(old_eh);
    return canvas;
  }catch(StreamError *e){
    png_destroy_read_struct(&pngstruct, &pnginfo,0);
    if(canvas!=0)
      delete canvas;
    i->on_error(old_eh);
    i->handle_error(e->clone());
    return 0;
  }
}

static void pngwritefunc(png_structp pngstruct,png_bytep data,png_size_t length){
  OutputStream *i=(OutputStream *)png_get_io_ptr(pngstruct);
  i->write(data,int(length));
}

static void pngflushfunc(png_structp){
  //this is easy
}

void png_saver(OutputStream *o,Canvas *canvas){
  png_structp pngstruct=0;
  png_infop pnginfo=0;
  unsigned char **rowpointers=0;
  ErrorHandler *old_eh=o->on_error();

  try {
    o->on_error(ErrorCreator::throw_error_handler);

    //create basic data structures
    pngstruct=png_create_write_struct(PNG_LIBPNG_VER_STRING,0,0,0);
    if(pngstruct==0)
      o->handle_error("Writing png header","Could not create pngstruct ?");
    
    pnginfo=png_create_info_struct(pngstruct);
    if(pnginfo==0)
      o->handle_error("Writing png header","Could not create pnginfo ?");

    //setup error handler
    if(setjmp(png_jmpbuf(pngstruct))){
      png_destroy_write_struct(&pngstruct, &pnginfo);
      o->on_error(old_eh);
      o->handle_error("Writing png header","Unknown bad stuff happened");
      if(canvas!=0)
	delete canvas;
      return;
    }
    png_set_write_fn(pngstruct,(void *)o,pngwritefunc,pngflushfunc);
    //ready to write...
    int w=canvas->width(),h=canvas->height();
    png_set_compression_level(pngstruct,Z_BEST_COMPRESSION); //one word:pentium
    png_set_IHDR(pngstruct,pnginfo,w,h,
		 8,PNG_COLOR_TYPE_RGB_ALPHA,PNG_INTERLACE_ADAM7,
		 PNG_COMPRESSION_TYPE_DEFAULT,PNG_FILTER_TYPE_DEFAULT);
    typedef unsigned char * ucp;
    rowpointers=new ucp[h];
    for(int y=0;y<h;y++)
      rowpointers[y]=(unsigned char *)&canvas->pixel(0,y);
    png_set_rows(pngstruct,pnginfo,(png_byte **)rowpointers);
#ifdef BYTESEXLE
    png_write_png(pngstruct,pnginfo,PNG_TRANSFORM_INVERT_ALPHA|
		  PNG_TRANSFORM_PACKSWAP,0);
#else
    png_write_png(pngstruct,pnginfo,PNG_TRANSFORM_INVERT_ALPHA,0);
#endif
    png_destroy_write_struct(&pngstruct, &pnginfo);
    pngstruct=0;pnginfo=0;
    o->on_error(old_eh);
    delete[] rowpointers;
    return;

  }catch(StreamError *e){
    png_destroy_write_struct(&pngstruct, &pnginfo);
    if(canvas!=0)
      delete canvas;
    if(rowpointers!=0)
      delete[] rowpointers;
    o->on_error(old_eh);
    o->handle_error(e->clone());
    return;
  }
}
///////////////////////////////////////////////////////////////////////////////
static const int source_buffer_size=512;

void jpg_sleep(struct jpeg_decompress_struct *){}

boolean jpg_fill_input_buffer(j_decompress_ptr cinfo){
  //get params
  InputStream *i=(InputStream *)cinfo->client_data;
  size_t& ib=cinfo->src->bytes_in_buffer;
// rsr this was *& before
  JOCTET* nib=(JOCTET *)cinfo->src->next_input_byte;

  //read data
  nib-=source_buffer_size-ib;
  nib=(JOCTET *)i->user_data;

  ib=i->try_read(nib,source_buffer_size);
  return true;
}

void jpg_skip_input_data(j_decompress_ptr cinfo,long num_bytes){
  InputStream *i=(InputStream *)cinfo->client_data;
  size_t& ib=cinfo->src->bytes_in_buffer;
// rsr this was *& before
  JOCTET* nib=(JOCTET *)cinfo->src->next_input_byte;
  if(ib<(unsigned)num_bytes){
    i->position(int(i->position()-ib));
    nib+=ib;
    ib=0;
  }else{
    ib-=num_bytes;
    nib+=num_bytes;
  }
}

struct jpg_error_mgr_ext {
  struct jpeg_error_mgr pub;
  jmp_buf setjmp_buffer;
};

void jpg_error_exit (j_common_ptr cinfo){
  jpg_error_mgr_ext *myerr=(jpg_error_mgr_ext *)cinfo->err;
  longjmp(myerr->setjmp_buffer, 1);
}


static Canvas *jpg_loader(InputStream *i){
  //init basic jpeg structures
  struct jpeg_decompress_struct cinfo;
  struct jpg_error_mgr_ext jerr;
  struct jpeg_source_mgr jsrc;

  Canvas *volatile canvas=0; //volatile, or the compiler complaisn abou setjmp
  JSAMPLE *samples=0;
  JOCTET *input_buffer=new JOCTET[source_buffer_size];
  i->user_data=input_buffer;
  cinfo.err = jpeg_std_error(&jerr.pub);
  jerr.pub.error_exit = jpg_error_exit;
  if (setjmp(jerr.setjmp_buffer)) {
    /* If we get here, the JPEG code has signaled an error.
     * We need to clean up the JPEG object, close the input file, and return.
     */
    jpeg_destroy_decompress(&cinfo);
    if(canvas!=0)
      delete canvas;
    if(samples!=0)
      delete[] samples;
    delete[] input_buffer;
    if(!i->bad())
      i->handle_error("Decoding jpeg data","libjpeg error");
    return 0;
  }
  jpeg_create_decompress(&cinfo);    
  
  //setup data source
  jsrc.init_source=jpg_sleep;
  jsrc.fill_input_buffer=jpg_fill_input_buffer;
  jsrc.skip_input_data=jpg_skip_input_data;
  jsrc.resync_to_restart=jpeg_resync_to_restart;
  jsrc.term_source=jpg_sleep;
  jsrc.next_input_byte=input_buffer+source_buffer_size;
  jsrc.bytes_in_buffer=0;
  cinfo.client_data=i;
  cinfo.src=&jsrc;  
  //read header
  jpeg_read_header(&cinfo, TRUE);
  jpeg_start_decompress(&cinfo);
  canvas=new Canvas(cinfo.output_width,cinfo.output_height);
  samples=new JSAMPLE[cinfo.output_width*4];
  while (cinfo.output_scanline < cinfo.output_height){
    Pixel *p=&canvas->pixel(0,cinfo.output_scanline);
    jpeg_read_scanlines(&cinfo,&samples,1);
    JSAMPLE *s=samples;
    for(int x=0;x<(int)cinfo.output_width;x++){
      uchar r=*s++,g=*s++,b=*s++;
      *p++=ToPixel(r,g,b,0);
    }
  }
  jpeg_finish_decompress(&cinfo);
  jpeg_destroy_decompress(&cinfo);
  delete[] samples;
  delete input_buffer;
  return canvas;
}

static boolean jpg_empty_buffer(j_compress_ptr cinfo){
  //get params
  OutputStream *o=(OutputStream *)cinfo->client_data;
  size_t& ib=cinfo->dest->free_in_buffer;
  //process
  o->write(o->user_data,int(ib=source_buffer_size));
  cinfo->dest->next_output_byte=(JOCTET *)o->user_data;
  return 1==1;
}

static void jpg_flush(j_compress_ptr cinfo){
  //get params
  OutputStream *o=(OutputStream *)cinfo->client_data;
  size_t& ib=cinfo->dest->free_in_buffer;
  //process
  o->write(o->user_data,int(source_buffer_size-ib));
  cinfo->dest->next_output_byte=(JOCTET *)o->user_data;
  return ;
}

static void jpg_sleep2(j_compress_ptr){}

void jpg_saver(OutputStream *o,Canvas *canvas){
  //init basic jpeg structures
  struct jpeg_compress_struct cinfo;
  struct jpg_error_mgr_ext jerr;
  struct jpeg_destination_mgr jdest;

  JSAMPLE *samples=new JSAMPLE[canvas->width()*3];
  JOCTET *output_buffer=new JOCTET[source_buffer_size];
  o->user_data=output_buffer;
  cinfo.err = jpeg_std_error(&jerr.pub);
  jerr.pub.error_exit = jpg_error_exit;
  if (setjmp(jerr.setjmp_buffer)) {
    /* If we get here, the JPEG code has signaled an error.
     * We need to clean up the JPEG object, close the input file, and return.
     */
    jpeg_destroy_compress(&cinfo);
    delete[] samples;
    delete[] output_buffer;
    if(!o->bad())
      o->handle_error("Encoding jpeg data","libjpeg error");
    return;
  }
  jpeg_create_compress(&cinfo);    
  
  //setup data source
  jdest.init_destination=jpg_sleep2;
  jdest.empty_output_buffer=jpg_empty_buffer;
  jdest.term_destination=jpg_flush;
  jdest.next_output_byte=output_buffer;
  jdest.free_in_buffer=source_buffer_size;
  cinfo.client_data=o;
  cinfo.dest=&jdest;

  //setup header
  cinfo.image_width=canvas->width();
  cinfo.image_height=canvas->height();
  cinfo.input_components = 3;
  cinfo.in_color_space = JCS_RGB;
  jpeg_set_defaults(&cinfo);
  jpeg_start_compress(&cinfo,TRUE);
  while (cinfo.next_scanline < cinfo.image_height){    
    Pixel *p=&canvas->pixel(0,cinfo.next_scanline);
    JSAMPLE *s=samples;
    for(int x=0;x<(int)cinfo.image_width;x++){
      *s++=RDATA(*p);*s++=GDATA(*p);*s++=BDATA(*p);
      p++;
    }
    jpeg_write_scanlines(&cinfo,&samples,1);

  }
  jpeg_finish_compress(&cinfo);
  jpeg_destroy_compress(&cinfo);
  delete[] samples;
  delete[] output_buffer;
  return;  
}
#endif
///////////////////////////////////////////////////////////////////////////////
static struct Extension_Struct extension_table[]={
  //extensions should be sorted asciibethically
  {"bmp",bmp_loader,bmp_saver},
  {"dib",bmp_loader,bmp_saver},
#if 0
  {"jfif",jpg_loader,jpg_saver},
  {"jpg",jpg_loader,jpg_saver},
  {"jpeg",jpg_loader,jpg_saver},
  {"png",png_loader,png_saver},
#endif
  {"xpm",xpm_loader,xpm_saver},
  {0,0}
};


int extension_comparator(const void *key,const void *elem){
  return strcmp((char *)key,((Extension_Struct *)elem)->extension);
}
/*
  return a list of valid extensions. The last one is NULL
  destruct with free(extension[i]);delete[] extensions
*/
char **File::extensions(){
  char **list=new char*[UBOUND(extension_table)+1];
  list[UBOUND(extension_table)]=0;
  for(int i=0;i<UBOUND(extension_table);i++)
    list[i]=strdup(extension_table[i].extension);
  return list;
}

//return a ptr to the extension(after the dot) of the file
//!!if type!=0, return strdup(type)
//the string should be delete[]d
const char *File::extension(const char *name,const char *type){
#if 1
	if(!name)
	{	return 0;
	}
	const char* dot=strrchr(name,'.');
	if(!dot)
	{	return 0;
	}
	return dot+1;
#else
  if(type!=0&&strcmp(type,"*")!=0)
    return strdup(type);

  //get the extension
  const char *dotptr=strrchr(name,'.');
  if(dotptr==0)
    return 0;
  char *s=strdup(dotptr+1);
  //convert to lower - if there are non-ascii characters in the extension,
  //we are lost anyway
  char *s2=s;
  while(*s2!=0){
    if(*s2>='A'&&*s2<='Z')
      *s2+='a'-'A';
    s2++;
  }
  return s;
#endif
}

/*
  Open a file. 'type' is the extension, or 0 if the extension of 'name'
  should be taken.  This returns 0 if something went wrong. The errormsg is
  returned in *errmsg
*/
Canvas *File::open(const char *name,const char *type,char **errmsg){  
  const char *dotptr=extension(name,type);
  if(dotptr==0){
    *errmsg=msprintf("File '%s' has no extension -> unknown file type",name);
    return 0;
  }
  //find out what it actually is  
  Extension_Struct *result=LookupExtStruct(dotptr,extension_table);
  if(result==0){
    *errmsg=msprintf("File '%s' has unknown extension",name);
    return 0;
  }

  InputStream *in=new FileInputStream(name);
  if(in->bad()){
    *errmsg=strdup(in->errormsg());
    delete in;
    return 0;
  }
  Canvas *c=result->loader(in); 
  if(c==0){
    *errmsg=strdup(in->errormsg());
    delete in;
    return 0;
  }
  c->param("File",name);
  const char *s=strrchr(name,'/');
  if(s==0)
    c->param("Name",name);
  else
    c->param("Name",s+1);
  delete in;
  *errmsg=0;
  c->saveme(false);
  return c;
}


/*
  Save a file. 'type' is the extension, or 0 if the extension of 'name'
  should be taken.
*/
void File::save(Canvas *c,const char *name,const char *type){  
  const char *dotptr=extension(name,type);
  if(dotptr==0){
    char *s=msprintf("File '%s' has no extension. Unknown file type. Not saved",name);
    ask("Unknown extension",0,s,"OK");
    free(s);
    return;
  }
  //find out what it actually is  
  Extension_Struct *result=(Extension_Struct *)
    bsearch(dotptr,extension_table,
	    UBOUND(extension_table),
	    sizeof(extension_table[0]),
	    extension_comparator);
  free((void*) dotptr);
  if(result==0){
    char *s=msprintf("File '%s' has unknown extension. Not saved",name);
    ask("Unknown extension",0,s,"OK");
    free(s);
    return;
  }
  OutputStream *out=new FileOutputStream(name);
  if(out->bad()){
    ask("Error while opening file",0,out->errormsg(),"OK");
    delete out;
    return;
  }
  result->saver(out,c); 
  if(out->bad()){
    ask("Error while writing to file",0,out->errormsg(),"OK");
    delete out;
    return;
  }
  const char *s=strrchr(name,'/');
  if(s==0)
    c->param("Name",name);
  else
    c->param("Name",s+1);
  c->param("File",name); //this invalidates 'name'

  delete out;
  return;
}
