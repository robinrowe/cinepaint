#ifndef _X_TOOL_H_
#define _X_TOOL_H_
#include "canvas.h"
#include <Fl/Fl_Group.h>
class Tool {
  Canvas *canvas_;
  Pen *pen_;
  Paint *paint_;
 public:
  //[gs]etters
  void setup(Canvas * c,Pen *pen,Paint *paint);
  inline Canvas *canvas()const{return canvas_;}
  inline Pen *pen()const{return pen_;}
  inline Paint *paint()const{return paint_;}

  //interface
  virtual bool on_click(int x,int y)=0;
  virtual bool on_drag(int x,int y);
  virtual bool on_release(int x,int y);
  virtual bool on_move(int x,int y);
  virtual int *on_draw();
  virtual Fl_Group *on_install();
  virtual void on_uninstall();
};

class ToolDot:public Tool{  
  int xold,yold;
  ShadowUndo *myundo;
 public:
  virtual bool on_click(int x,int y);
  virtual bool on_drag(int x,int y);
  virtual bool on_release(int x,int y);
};

class ToolLine:public Tool {
  int xold,yold;
 public:
  virtual bool on_click(int x,int y);
  virtual bool on_drag(int x,int y);
  virtual bool on_release(int x,int y);
};

class ToolRect:public Tool {
  int xold,yold;
 public:
  virtual bool on_click(int x,int y);
  virtual bool on_drag(int x,int y);
  virtual bool on_release(int x,int y);
};

class ToolFRect:public Tool {
  int xold,yold;
 public:
  virtual bool on_click(int x,int y);
  virtual bool on_drag(int x,int y);
  virtual bool on_release(int x,int y);
};

class ToolFill:public Tool{
  Pixel base_pixel;
  int max_pixel_difference;
  static void slicer(int, int, int, void *);
  static bool ptester(int, int, void *);
 public:  
  virtual bool on_click(int x,int y);
};

class ToolRoll:public Tool {
  int splitx,splity;
 public:
  virtual bool on_move(int x,int y);
  virtual int* on_draw();
  virtual bool on_click(int x,int y);
};

class ToolPoly:public Tool {
  int points;
  int *pointx,*pointy;
 public:
  virtual bool on_move(int x,int y);
  virtual int* on_draw();
  virtual bool on_click(int x,int y);
};

class ToolControlPoints:public Tool {
  Point *knot;
  int num_knots;
  int selected_knot;
 public:
  ToolControlPoints();
  inline void knots(Point *knots,int count){
    this->knot=knots;
    this->num_knots=count;
  }
  virtual bool on_drag(int x,int y);
  virtual bool on_click(int x,int y);
};

class ToolCircle:public ToolControlPoints {
 protected:
  friend class PanelToolCircle;
  class PanelToolCircle *child;
  Point control[3]; //0=center,1 and 2=axis
  bool is_ready;
  virtual void drawcircle();
  static void update(Fl_Widget *,void *);
  static void make(Fl_Widget *,void *);
 public:
  ToolCircle();
  virtual int*on_draw();
  virtual bool on_drag(int x,int y);
  virtual bool on_click(int x,int y);
  virtual bool on_release(int x,int y);
  virtual Fl_Group *on_install();
  virtual void on_uninstall();
};

class ToolFCircle:public ToolCircle{
  virtual void drawcircle();
 public:
  ToolFCircle();
};

/*
class ToolWarp:public ToolControlPoints {
 protected:
  //friend class PanelToolWarp;
  //class PanelToolWarp *child;
  Point *knots;
  int grid;
 public:
  ToolWarp();
  virtual int* on_draw();

  virtual bool on_drag(int x,int y);
  virtual bool on_click(int x,int y);
  virtual bool on_release(int x,int y);
  virtual Fl_Group *on_install();
  virtual void on_uninstall();

};
*/
#endif
