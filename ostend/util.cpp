/*
  util.cc (c) 2002 Yperman Hans
*/
//this is for asprintf()
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <errno.h>
#include "util.h"
#include <Fl/Fl.h>
#include <unistd.h>


//append *s to *s2, delete original *s
//*s can be NULL
void strappend(char **s,char *s2){
  if(*s==0){
    *s=strdup(s2);
    return;
  }else{
    int l1=strlen(*s);
    int l2=strlen(s2);
    char *result=new char[l1+l2+1];
    memcpy(result,*s,l1);
    memcpy(result+l1,s2,l2+1);
    delete[] *s;
    *s=result;
    return;
  }
}


//Calling this means the program wants to abort
void stop(const char *s,...){
  va_list v;
  va_start(v,s);
  fprintf(stderr,"Internal error:\n");
  vfprintf(stderr,s,v);
  fprintf(stderr,"\n");
  va_end(v);
  abort();

}

//Calling this means the program wants to warn about internal errors
void warn(char *s,...){
  va_list v;
  va_start(v,s);
  fprintf(stderr,"Warning:Internal error:\n");
  vfprintf(stderr,s,v);
  fprintf(stderr,"\n");
  va_end(v);
}

// rsr: 9/09/05
char *vasprintf256(char** s,const char *fmt,va_list v)
{// rsr: BUG - benign, but could warn on errors
  const size_t size=256;
  char* p=(char*)malloc(size);
  if(!p)
  {  return 0;
  }
  *s=p;
  vsnprintf(p,size-1,fmt,v);
  p[size-1]=char(0);
  return p;
}
#define vasprintf vasprintf256

/*
  it's MUCH handyer to msprintf than to malloc(enough)/sprintf
*/
char *msprintf(const char *fmt,...){
  va_list v;
  va_start(v,fmt);
  char *s;
  vasprintf(&s,fmt,v);
  va_end(v);
  return s;
}

/*
  i am NOT pro VAX VMS.  This buddy means: 
  malloc and sprintf, with a va_list argument.
*/
char *vmsprintf(const char *fmt,va_list v){
  char *s;
  vasprintf(&s,fmt,v);
  return s;
}


/////LongString////////////////////////////////////////////////////////////////
LongString::~LongString(){if(len>0) delete[] data;}
void LongString::append(double d){
  char *s=msprintf("%g",d);
  appendd(s);
}

void LongString::append(char c){
  if(pos+1>=len){
    len+=bloksize;
    char *d2=new char[len];
    if(pos>0){
      memcpy(d2,data,pos);
      delete[] data;
    }
    data=d2;
  }
  data[pos++]=c;
  data[pos]=0;
}


void LongString::append(const char *s){
  int l=strlen(s);
  if(pos+l>=len){
    len+=bloksize+l;
    char *d2=new char[len];
    if(pos>0){
      memcpy(d2,data,pos);
      delete[] data;
    }
    data=d2;
  }
  memcpy(data+pos,s,l+1);
  pos+=l;
}

void LongString::printf(const char *fmt,...){
  va_list v;
  va_start(v,fmt);
  char *s;
  vasprintf(&s,fmt,v);
  va_end(v);
  appendd(s);
}

//replace every occurence of c1 with c2. c2 can be 0. c1 can't
char *replace(char *str,char c1,char c2){
  char *s=str;
  while(*s!=0){
    if(*s==c1)
      *s=c2;
    s++;
  }
  return str;
}

//replace every occurence of 'from' with 'to'
void LongString::replace(const char *from,const char * to){
  char *p;
  while((p=strstr(data,from))!=0){ //We'll probably iterate exactly once
    int len_to=strlen(to);
    int len_from=strlen(from);
    //do we have the place needed to store this ?
    if(pos+len_to-len_from<len){
      //move
      memmove(p+len_to,p+len_from,data+pos-p+1);
      memcpy(p,to,len_to);
    }else{
      //realloc and replace
      char *d2=new char [len+len_to+bloksize];
      if(p!=data)
	memcpy(d2,data,p-data);      
      if(data+pos-p!=0)
	memcpy(d2+(p-data)+len_to,p+len_from,data+pos-p);
      if(len_to!=0)
	memcpy(d2+(p-data),to,len_to);
      delete[] data;
      data=d2;
    }
    pos=pos+len_to-len_from;
    data[pos]=0;
  }
}

/////DynArray//////////////////////////////////////////////////////////////////

typedef void* void_ptr;

DynArray::DynArray(){
  items=0;
  space=space_delta;
  data=new void_ptr[space];
}
DynArray::~DynArray(){
  delete[] data;
}

int DynArray::add(void *ptr){
  if(space==items){
    space+=space_delta;
    void **d2=new void_ptr[space];
    memcpy(d2,data,sizeof(void*)*items);
    delete[] data;
    data=d2;
  }
  data[items]=ptr;
  return items++;
}

int DynArray::insert(int index,void *ptr){
  if(space==items){
    space+=space_delta;
    void **d2=new void_ptr[space];
    memcpy(d2,data,sizeof(void*)*items);
    delete[] data;
    data=d2;
  }
  if(index!=items)
    memmove(data+index,data+index+1,sizeof(void*)*(items-index));
  data[index]=ptr;
  items++;
  return index;
}

void DynArray::remove(int index){
  memmove(data+index+1,data+index,sizeof(void*)*(items-index-1));
  items--;
}

///////////////////////////////////////////////////////////////////////////////
static Program *last_program=0;
Program& Program::get(){

  if(last_program==0)
    stop("Internal error:No program defined");
  return *last_program;
}

Program::Program(int argc,char *argv[]){
  if(last_program!=0)
    stop("Internall error:2 program classes defined");
  last_program=this;
  file_arg=myargc=argc;
  myargv=argv;
  touched=new bool[argc+1];
  for(int i=0;i<argc;i++)
    touched[i]=false;
  //FLTK-specific: parse FLTK args
  for(int i=1;;){
    int oldi=i;
    Fl::arg(argc,argv,i);
    for(;oldi<i;oldi++)
      touched[oldi]=true;
    if(i>=argc)
      break;
    if(strcmp(argv[i],"--")==0){
      file_arg=i;
      touched[i]=true;
      break;
    }
    i++;
  }
  tmpdir=0;
}

Program::~Program(){
  if(tmpdir!=0){
    //Now this is what i call program cleanup
    char *s=msprintf("cd /;rm -R \"%s\"",tmpdir);
    system(s);
    free(s);
    delete[] tmpdir;
    tmpdir=0;
  }
  delete[] touched;    
  last_program=0;
}

bool Program::argument(const char *option){
  return argumentpos(option)!=0;
}

int Program::intargument(const char *option,int defaultval){
  int i=argumentpos(option)+1;
  int n;
  char *tail;
  if(i==1||touched[i]||file_arg<=i||(n=strtol(myargv[i],&tail,10),*tail!=0))
    return defaultval;
  touched[i]=true;
  return n;
}

const char *Program::stringargument(const char* option,const char* defaultval){
  int i=argumentpos(option)+1;
  if(i==1||i>=file_arg)
    return defaultval;
  touched[i]=true;
  return myargv[i];
}

int Program::argumentpos(const char*option){
  for(int i=1;i<file_arg;i++)
    if(strcmp(option,myargv[i])==0){
      touched[i]=true;
      return i;
    }
  return 0;
}

int Program::untouchedargument(){
  for(int i=1;i<myargc;i++)
    if(!touched[i]){
      touched[i]=true;
      return i;
    }
  return 0;
}

const char *Program::argument(int i){
  touched[i]=true;
  return myargv[i];
}
  

/*
  return the name of a  unique file in this program's temporary directory
  Don't worry about already existing files etc ...
*/
const char* Program::tmpfile(){
  abort(); //mkdtmp gives trouble on some systems. 
  return "tmpfile aborted"; // VC++ 8 requires a return statement 
/*
  if(tmpdir==0){
    tmpnum=0;
    LongString tmplate;
    char *tmp=getenv("TMP");
    if(tmp==0) tmp=getenv("tmp");
    if(tmp==0) tmp="/tmp";
    tmplate.append(tmp);
    tmplate.printf("/"INTERNAL_NAME"-%d-XXXXXX",getpid());
    tmpdir=tmplate.release();
    if(mkdtemp(tmpdir)==0)
      stop("Could not create temporary directory %s:%s",
	   tmpdir,strerror(errno));    
    fprintf(stderr,"Temporary direcory:%s\n",tmpdir);
  }
  return msprintf("%s/tmp-%d",tmpdir,tmpnum++);
*/
}

