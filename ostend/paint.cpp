#include <stdio.h>
#include <stdlib.h>
#include "paint.h"
void Paint::reada(int ,int ){}
void Paint::readb(int ,int ){}

////ColorPaint/////////////////////////////////////////////////////////////////
ColorPaint::~ColorPaint(){}

void ColorPaint::unshadow(){
  int x1,y1,x2,y2;
  canvas()->isdirty(x1,y1,x2,y2);
  for(int y=y1;y<=y2;y++){
    const uint8 *shadow=&canvas()->shadow(x1,y);
    Pixel *to=&canvas()->pixel(x1,y);
    Pixel *from=&canvas()->original(x1,y);
    for(int x=x1;x<=x2;x++){
      if(*shadow!=0){
	*to=mix(*shadow,mycolor,*from);
      }
      shadow++;
      to++;
      from++;
    }
  }
}

void ColorPaint::reada(int x,int y){
  mycolor=canvas()->pixel(x,y);
}

////GradientPaint/////////////////////////////////////////////////////////////////
GradientPaint::~GradientPaint(){}
void GradientPaint::unshadow(){
  int dx,dy,addv=0,divv;
  int w=canvas()->width(),h=canvas()->height();
  switch(dir){
  case 0:dx=1;dy=0;divv=h;break;
  case 1:dx=0;dy=1;divv=w;break;
  case 2:dx=dy=1;divv=w+h;break;
  default:dx=1;dy=-1;addv=w;divv=w+h;break;
  }
  int x1,y1,x2,y2;
  canvas()->isdirty(x1,y1,x2,y2);
  for(int y=y1;y<=y2;y++){
    const uint8 *shadow=&canvas()->shadow(x1,y);
    Pixel *to=&canvas()->pixel(x1,y);
    Pixel *from=&canvas()->original(x1,y);
    for(int x=x1;x<=x2;x++){
      if(*shadow!=0){
	Pixel c=mix((dx*y+dy*x+addv)*255/divv,mycolor[1],mycolor[0]);
	*to=mix(*shadow,c,*from);
      }
      shadow++;
      to++;
      from++;
    }
  }
}

////CopyPaint////////////////////////////////////////////////////////////////////
CopyPaint::~CopyPaint(){}
void CopyPaint::canvas_deleted(void *c,void *me){
  ((CopyPaint *)me)->source=(Canvas *)c;
}
void CopyPaint::unshadow(){
  int x1,y1,x2,y2;

  if(source==0) source=canvas();
  int wd=source->width(),hd=source->height();
  canvas()->isdirty(x1,y1,x2,y2);

  //these should always be <=0, we'll do % on them
  int ddx=canvas()->base_x()-dx;
  int ddy=canvas()->base_y()-dy;
  while(ddx>0) ddx-=wd;
  while(ddy>0) ddy-=hd;

  for(int y=y1;y<=y2;y++){
    const uint8 *shadow=&canvas()->shadow(x1,y);
    Pixel *to=&canvas()->pixel(x1,y);
    Pixel *from=&canvas()->original(x1,y);
    Pixel *orig=&canvas()->original(0,(y-ddy)%hd);
    if(source!=canvas())
      orig=&source->pixel(0,(y-ddy)%hd);
    for(int x=x1;x<=x2;x++){
      if(*shadow!=0)
	*to=mix(*shadow,orig[(x-ddx)%wd],*from);
      shadow++;
      to++;
      from++;
    }
  }
}

void CopyPaint::reada(int x,int y){
  dx=x;dy=y;
  source=canvas();
  source->on_delete().subscribe(CopyPaint::canvas_deleted,this);
}

////ConveryPaint/////////////////////////////////////////////////////////////////
ConvertPaint::~ConvertPaint(){}
void ConvertPaint::unshadow(){
  int x1,y1,x2,y2;
  canvas()->isdirty(x1,y1,x2,y2);
  for(int y=y1;y<=y2;y++){
    const uint8 *shadow=&canvas()->shadow(x1,y);
    Pixel *to=&canvas()->pixel(x1,y);
    Pixel *from=&canvas()->original(x1,y);
    for(int x=x1;x<=x2;x++){
      if(*shadow!=0&&*from==mycolor[0])
	*to=mix(*shadow,mycolor[1],*from);

      shadow++;
      to++;
      from++;
    }
  }
}

void ConvertPaint::reada(int x,int y){
  mycolor[1]=canvas()->pixel(x,y);
}

void ConvertPaint::readb(int x,int y){
  mycolor[0]=canvas()->pixel(x,y);
}
////MultiPaint////////////////////////////////////////////////////////////////////
MultiPaint::MultiPaint(Pixel p,Pixel p2){
  mycolor[0]=p;
  mycolor[1]=p2;
  data1=new uint8[255];
  data2=new uint8[255];
  for(int i=0;i<255;i++){
    data1[i]=((uint8)rand());
    data2[i]=((uint8)rand());
  }
};

MultiPaint::~MultiPaint(){
  delete[] data1;
  delete[] data2;
}
void MultiPaint::unshadow(){
  int x1,y1,x2,y2;
  canvas()->isdirty(x1,y1,x2,y2);
  for(int y=y1;y<=y2;y++){
    const uint8 *shadow=&canvas()->shadow(x1,y);
    Pixel *to=&canvas()->pixel(x1,y);
    Pixel *from=&canvas()->original(x1,y);
    int rn=y&0xFF;
    for(int x=x1;x<=x2;x++){
      if(*shadow!=0){
	int kn=x&0xFF;
	int random=data1[kn]^data2[rn^kn];
	Pixel c=mix(random, mycolor[1],mycolor[0]);
	*to=mix(*shadow,c,*from);
      }
      shadow++;
      to++;
      from++;
    }
  }
}

void MultiPaint::reada(int x,int y){
  mycolor[0]=canvas()->pixel(x,y);
}

void MultiPaint::readb(int x,int y){
  mycolor[1]=canvas()->pixel(x,y);
}
