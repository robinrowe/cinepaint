/*
  ui tools (c) 2002 Yperman Hans
*/
#ifndef _X_UI_TOOLS_H_
#define _X_UI_TOOLS_H_
#include <stdlib.h>
#include <Fl/Fl.h>
#include <Fl/Fl_Window.h>
#include <Fl/Fl_Preferences.h>
extern Fl_Preferences *rootprefs;

Fl_Widget *run_modal(Fl_Window *w);
void run_modal_callback(Fl_Widget *wid,void *);

//questions
int ask(const char *title,Fl_Image *img,const char *text,
	const char *btn0=0,const char *btn1=0,const char *btn2=0,bool askask=true);
void ask_init(Fl_Preferences *p);
void ask_exit();


//Fl_Image *get_image(const char **xpmdata);
Fl_Image *get_image(const char *name);
bool register_image(const char *name,Fl_Image *img);
bool register_image(const char *name,const char **xpmdata);
void unregister_images();

static inline int makecol24(int r,int g,int b){
  return r+(g<<8)+(b<<16)+0xFF000000;
}

static inline int makecol24(Fl_Color c){
  uchar r,g,b;
  Fl::get_color(c,r,g,b);
  return makecol24(r,g,b);
}
#endif
