#ifndef _X_W_DIRTREE_H_
#define _X_W_DIRTREE_H_
/*
  Lots of widgets for the file dialog
*/
#include <Fl/Fl_Scroll.h>
#include <Fl/Fl_Button.h>
#include <Fl/Fl_Preferences.h>
#include <Fl/Fl_Input.h>
#include "broadcaster.h"
#include "streams.h"
#include "w_tree.h"

class DirTreeBrowser:public TreeBrowser {
  Directory *dir;
  Broadcaster *dirchanged;
  static void directory_changed(void *p,void *p2);
  static void treebrowsed(Fl_Widget *,void *);
  void repopulate();
  void getnodepath(const TreeTextItem *i,LongString& l);
 public:
  DirTreeBrowser(int x,int y,int w,int h,char *l=0);
  virtual ~DirTreeBrowser();
  void directory(Directory *d);
  void on_dirchange(Broadcaster *b);  
};

class Speedbar:public Fl_Scroll {
 public:
  struct Item {
    const char *name;
    const char *icon;
    const char *cmd;
    const char *dir;
    Fl_Button *widget;
    void destroy();
  };
 private:
  Directory *dir;
  Broadcaster *dirchanged;
  vector<Item *> items;
  Fl_Preferences *prefs;
  static const int size=64; //size of buttons

  //void build_buttons();
  void build_widget(Item *,int);
  static void button_callback(Fl_Widget *w,void *p);
  int selected_speedbutton();

 public:
  //h should be 84, or ethernal uglyness is the fate of you app
  Speedbar(int x,int y,int w,int h,const char *l=0);
  virtual ~Speedbar();
  void preferences(Fl_Preferences *p);
  void directory(Directory *d);
  void on_dirchange(Broadcaster *b);  
  void insert();
  void removeb();
  void edit();
  void toleft();
  void toright();
};

class FileBrowser:public Fl_Browser_{
  Directory *dir;
  Broadcaster *dirchanged,*filechanged;
  vector<FileData *> files;
  int i_height,text_width;
  char *file_selection;
  static const int icon_width=20;
  static void directory_changed(void *p,void *p2);
  static void file_changed(void *p,void *p2);
  static void selection_changed(Fl_Widget *,void *p);
  void  load_directory();

 protected:
  virtual FL_EXPORT void *item_first() const;
  virtual FL_EXPORT void *item_next(void *) const ;
  virtual FL_EXPORT void *item_prev(void *) const ;
  virtual FL_EXPORT int item_height(void *) const;
  virtual FL_EXPORT int item_width(void *) const;
  virtual FL_EXPORT int item_quick_height(void *) const ;
  virtual FL_EXPORT int full_height() const;
  virtual FL_EXPORT int full_width() const;
  virtual FL_EXPORT void item_draw(void *,int,int,int,int) const;
  //virtual FL_EXPORT int handle(int);
 public:
  FileBrowser(int x,int y,int w,int h,const char *l=0);
  virtual ~FileBrowser();

  inline void filesel(const char *ext){
    free(file_selection);
    file_selection=strdup(ext);
  }

  void directory(Directory *d);
  void on_dirchange(Broadcaster *b);
  void on_filechange(Broadcaster *b);
};

class FileInputBox:public Fl_Input {
  Directory *dir;
  Broadcaster *dirchanged,*filechanged;
  static void directory_changed(void *p,void *p2);
  static void file_changed(void *p,void *p2);
 protected:
  int handle(int event);
 public:
  FileInputBox(int x,int y,int w,int h,const char *l=0);
  virtual ~FileInputBox();

  void directory(Directory *d);
  void on_dirchange(Broadcaster *b);
  void on_filechange(Broadcaster *b);
};

#endif
