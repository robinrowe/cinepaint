#ifndef _X_PAINT_H_
#define _X_PAINT_H_
#include "canvas.h"
class Paint {
  Canvas *canvas_;
 public:
  //interface
  virtual void unshadow()=0;
  virtual void reada(int x,int y);
  virtual void readb(int x,int y);
  //Class users should specify a canvas
  inline Canvas *canvas(){return canvas_;};
  inline void canvas(Canvas *c){canvas_=c;}
};

class ColorPaint:public Paint{
  Pixel mycolor;
 public:
  inline ColorPaint(Pixel p){mycolor=p;};
  virtual ~ColorPaint();
  virtual void reada(int x,int y);
  virtual void unshadow();
  inline void color(Pixel c){mycolor=c;}
  inline Pixel color(){return mycolor;}
};

class GradientPaint:public Paint{
  Pixel mycolor[2];
  int dir;
 public:
  inline GradientPaint(Pixel p,Pixel p2,int d){mycolor[0]=p;mycolor[1]=p2;dir=d;};
  virtual ~GradientPaint();
  virtual void unshadow();

  inline void color(int n,Pixel c){mycolor[n]=c;}
  inline Pixel color(int n)const{return mycolor[n];}
  inline int direction()const{return dir;}
  inline void direction(int n){dir=n;}
};

class CopyPaint:public Paint{
  int dx,dy;
  Canvas *source;
 public:
  inline CopyPaint(){dx=dy=30;source=0;};
  virtual ~CopyPaint();
  virtual void unshadow();
  virtual void reada(int x,int y);
  static void canvas_deleted(void *,void *);
};

class ConvertPaint:public Paint{
  Pixel mycolor[2];
 public:
  inline ConvertPaint(Pixel p,Pixel p2){mycolor[0]=p;mycolor[1]=p2;};
  virtual ~ConvertPaint();
  virtual void unshadow();
  virtual void reada(int x,int y);
  virtual void readb(int x,int y);
  inline void color(int n,Pixel c){mycolor[n]=c;}
  inline Pixel color(int n){return mycolor[n];}
};

class MultiPaint:public Paint{
  Pixel mycolor[2];
  uint8* data1,*data2;
 public:
  MultiPaint(Pixel p,Pixel p2);
  virtual ~MultiPaint();
  virtual void unshadow();
  virtual void reada(int x,int y);
  virtual void readb(int x,int y);
  inline void color(int n,Pixel c){mycolor[n]=c;}
  inline Pixel color(int n){return mycolor[n];}
};

#endif
