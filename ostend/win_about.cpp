// generated by Fast Light User Interface Designer (fluid) version 1.0106

#include "win_about.h"
//Program info window
//rsr
//#include "data.h"
#include "ui_tools.h"
#include "util.h"

Win_About::Win_About() {
  Fl_Double_Window* w;
  LongString l;
//rsr
//l.append(about_html);
l.replace("$VERSION$",VERSION);
l.replaced("$FLTKVERSION$",msprintf("fltk v%d.%d.%d",FL_MAJOR_VERSION,FL_MINOR_VERSION,FL_PATCH_VERSION));
  { Fl_Double_Window* o = window = new Fl_Double_Window(475, 385, "About");
    w = o;
    o->callback((Fl_Callback*)run_modal_callback, (void*)(this));
    { Fl_Return_Button* o = okbutton = new Fl_Return_Button(405, 356, 65, 24, "OK");
      o->callback((Fl_Callback*)run_modal_callback);
    }
    { Fl_Help_View* o = new Fl_Help_View(5, 5, 465, 345);
      o->box(FL_ENGRAVED_BOX);
      o->color(FL_BACKGROUND_COLOR);
      o->selection_color(FL_BACKGROUND_COLOR);
      o->labeltype(FL_NORMAL_LABEL);
      o->labelfont(0);
      o->labelsize(14);
      o->labelcolor(FL_BLACK);
      o->align(FL_ALIGN_CENTER);
      o->when(FL_WHEN_RELEASE);
      o->value(l.value());
      o->textsize(o->textsize()+1);
    }
    o->end();
  }
}

Win_About::~Win_About() {
  delete window;
}

void Win_About::run() {
  run_modal(window);
}
