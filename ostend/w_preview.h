#ifndef _X_W_PREVIEW_H_
#define _X_W_PREVIEW_H_
#include <stdlib.h>
#include <vector>
#include <Fl/Fl_Widget.h>
#include <Fl/Fl_Group.h>
#include <Fl/Fl_Scrollbar.h>
#include "canvas.h"
#include "streams.h"
class W_Preview:public Fl_Widget {
 private:
  static const int size=80,text_offset=14,max_name_chars=10,
    border_u=4,border_d=18,border_l=4,border_r=4;
  Fl_Scrollbar *my_scrollbar;
  char *file_selection;
  int sel_start,sel_end;
  struct Previewdata{
    Canvas *image;
    char *name;
    bool selected;
  };
  vector<Previewdata *> images;
  Directory *dir;
  Broadcaster *dirchanged;

  static void directory_changed(void *p,void *p2);
  static void scrolled(Fl_Widget *,void *);

  void load_directory();
  void clean_previews();
 protected:
  virtual int handle(int event);
  virtual void draw();
 public:
  W_Preview(int x,int y,int w,int h,const char *l=0);
  virtual ~W_Preview();
  inline void directory(Directory *d){dir=d;}
  void on_dirchange(Broadcaster *b);
  inline void filesel(const char *ext){
    free(file_selection);
    file_selection=strdup(ext);
  }
  inline void scrollbar(Fl_Scrollbar *b){
    my_scrollbar=b;
    b->callback(scrolled,this);
  }
  const char *selection(int& i);
};
#endif
