#ifndef _X_DRAW_H_
#define _X_DRAW_H_
#include "util.h"
typedef void (PixelSetter)(int x,int y,void *data);
typedef void (SliceSetter)(int x1,int x2,int y,void *data);
typedef bool (PixelTester)(int x,int y,void *data);

void line(int x1,int y1,int x2,int y2,int w,int h,PixelSetter *p,void *data);
void circle(int x,int y,int r,int quadrants,PixelSetter *p,void *data);
int fill(int x,int y,int w,int h,
	  PixelTester *t,void *data0,SliceSetter *s,void *data1);
void conic(int xs,int ys,int xe,int ye,
	  int64 A,int64 B,int64 C,int64 D,int64 E,int64 F,
	   PixelSetter *p,void *data);
void fconic(double A,double B,double C,double D,double E,double F,
	   SliceSetter *s,void *data,int w,int h);

#endif
