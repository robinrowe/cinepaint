#include <string.h>
#include <math.h>
#include "action.h"
#include "canvas.h"
#include "win_stuff.h"
#include "util.h"
/////Action undo///////////////////////////////////////////////////////////////
ActionUndo::~ActionUndo(){}

void ActionUndo::undo(Canvas *c){
  act(c,arg1,arg2);
}
/////Resize////////////////////////////////////////////////////////////////////
void resize_fast(Canvas *c,int ex_w,int ex_h,Pixel *from){
  Pixel *to=&c->pixel(0,0);
  int w=c->width();
  int h=c->height();
  int xa=(1<<16)*ex_w/w;
  for(int y=0;y<h;y++){
    Pixel *from2=from+ex_w*(y*ex_h/h);
    int xx=0;
    for(int x=0;x<w;x++){
      *to++=from2[xx>>16];
      xx+=xa;
    }
  }
  //delete[] from;
}

inline double up(double n){return (double)(int)(n+1)-n;}
inline double down(double n){return n-floor(n);}

static inline void gather_pixel(double& r,double& g,double& b,double& a,
				Pixel *row,double xstart,double xend){

  int xxstart=(int)xstart,xxend=(int)xend;
  row+=xxstart;
  //handle first pixel
  double n=up(xstart);
  Pixel p=*row++;
  r=RDATA(p)*n;g=GDATA(p)*n;b=BDATA(p)*n;a=ADATA(p)*n;
  //handle row
  for(xxstart++;xxstart<xxend;xxstart++){
    p=*row++;
    r+=RDATA(p);g+=GDATA(p);b+=BDATA(p);a+=ADATA(p);    
  }
  //handle last pixel
  n=down(xend);
  p=*row++;
  r+=RDATA(p)*n;g+=GDATA(p)*n;b+=BDATA(p)*n;a+=ADATA(p)*n;

}
void resize_good(Canvas *c,int ex_w,int ex_h,Pixel *from){
  Pixel *to=&c->pixel(0,0);
  int w=c->width();
  int h=c->height();
  for(int y=0;y<h;y++){
    double ystart=y*(ex_h-1)/double(h);
    double yend=(y+1)*(ex_h-1)/double(h);
    int yystart=(int)ystart,yyend=(int)yend;
    for(int x=0;x<w;x++){
      double xstart=x*(ex_w-1)/double(w);
      double xend=(x+1)*(ex_w-1)/double(w);
      int xxstart=(int)xstart,xxend=(int)xend;
      if(yystart==yyend){
	if(xxstart==xxend){
	  //only 1 pixel is needed
	  *to++=from[xxstart+yystart*ex_w];
	}else{
	  //only 1 row is needed
	  double sum=xend-xstart;
	  Pixel *f=from+xxstart+yystart*ex_w;
	  //first pixel
	  double n=up(xstart);

	  Pixel p=*f++;
	  double r=RDATA(p)*n,g=GDATA(p)*n,b=BDATA(p)*n,a=ADATA(p)*n;
	  //row
	  for(xxstart++;xxstart<xxend;xxstart++){
	    p=*f++;
	    r+=RDATA(p);g+=GDATA(p);b+=BDATA(p);a+=ADATA(p);
	  }

	  //last pixel
	  n=xend-floor(xend);
	  p=*f;
	  r+=RDATA(p)*n;g+=GDATA(p)*n;b+=BDATA(p)*n;a+=ADATA(p)*n;

	  *to++=ToPixel(int(r/sum),int(g/sum),int(b/sum),int(a/sum));
	}
      }else{
	if(xxstart==xxend){
	  double sum=yend-ystart;
	  //only 1 column is needed
	  Pixel *f=from+xxstart+yystart*ex_w;
	  //first pixel
	  double n=up(ystart);
	  Pixel p=*f;f+=ex_w;
	  double r=RDATA(p)*n,g=GDATA(p)*n,b=BDATA(p)*n,a=ADATA(p)*n;

	  //column
	  for(int y=yystart+1;y<yyend;y++){
	    p=*f;f+=ex_w;
	    r+=RDATA(p);g+=GDATA(p);b+=BDATA(p);a+=ADATA(p);
	  }

	  //last pixel
	  n=yend-floor(yend);
	  p=*f;
	  r+=RDATA(p)*n;g+=GDATA(p)*n;b+=BDATA(p)*n;a+=ADATA(p)*n;
	  *to++=ToPixel(int(r/sum),int(g/sum),int(b/sum),int(a/sum));
	}else{
	  double r,g,b,a,ra,ga,ba,aa;
	  double sum=(xend-xstart)*(yend-ystart);
	  //add first row
	  double n=up(ystart);
	  gather_pixel(ra,ga,ba,aa,from+yystart*ex_w,xstart,xend);
	  r=ra*n;g=ga*n;b=ba*n;a=aa*n;
	  //add center blok
	  for(int y=yystart+1;y<yyend;y++){
	    gather_pixel(ra,ga,ba,aa,from+(int)y*ex_w,xstart,xend);
	    r+=ra;g+=ga;b+=ba;a+=aa;
	  }

	  //add last row
	  n=yend-floor(yend);
	  gather_pixel(ra,ga,ba,aa,from+yyend*ex_w,xstart,xend);
	  r+=ra*n;g+=ga*n;b+=ba*n;a+=aa*n;
	  *to++=ToPixel(int(r/sum),int(g/sum),int(b/sum),int(a/sum));	  
	}
      }
    }
  }
  //delete[] from;
}

char *action_resize(Canvas *c,int,int){
  Win_Resize *win=new Win_Resize(c);
  if(win->run()){
    int w=win->rewidth();
    int h=win->reheight();
    int ex_w=c->width();
    int ex_h=c->height();
    
    Pixel *ex_data=c->new_image(w,h);
    if(win->fast_method())
      resize_fast(c,ex_w,ex_h,ex_data);
    else
      resize_good(c,ex_w,ex_h,ex_data);
    c->add(new ImageUndo(ex_w,ex_h,ex_data));
  }
  delete win;
  return 0;
}
/////Mirror ///////////////////////////////////////////////////////////////////
char * action_mirror_lr(Canvas *c,int u,int){
  int w=c->width();
  int h=c->height();
  int x2=w>>1;
  for(int y=0;y<h;y++){
    Pixel *row=&c->pixel(0,y);
    for(int x=0;x<x2;x++)      
      SWAP(Pixel,row[x],row[w-1-x]);
  }
  if(u==0)//if we are not undoing
    c->add(new ActionUndo(action_mirror_lr,1,0));
  return 0;
}

char * action_mirror_ud(Canvas *c,int u,int){
  int w=c->width();
  int h=c->height();
  int y2=h>>1;
  for(int y=0;y<y2;y++){
    for(int x=0;x<w;x++)      
      SWAP(Pixel,c->pixel(x,y),c->pixel(x,h-1-y));
  }
  if(u==0)//if we are not undoing
    c->add(new ActionUndo(action_mirror_ud,1,0));
  return 0;
}

char *action_mirror_diag(Canvas *c,int u,int){
  int w=c->width();
  int h=c->height();
  Pixel *ex_image=c->new_image(h,w);
  for(int y=0;y<w;y++){
    Pixel *to=&c->pixel(0,y);
    Pixel *from=ex_image+y;
    for(int x=0;x<h;x++){
      *to++=*from;
      from+=w;
    }
  }   
  delete[] ex_image;
  if(u==0)//if we are not undoing
    c->add(new ActionUndo(action_mirror_diag,1,0));
  return 0;
}

/////Rotate////////////////////////////////////////////////////////////////////
char * action_rotate_90(Canvas *c,int u,int){
  action_mirror_lr(c,1,0);
  action_mirror_diag(c,1,0);
  if(u==0)//if we are not undoing
    c->add(new ActionUndo(action_rotate_270,1,0));  
  return 0;
}

char * action_rotate_270(Canvas *c,int u,int){
  action_mirror_ud(c,1,0);
  action_mirror_diag(c,1,0);
  if(u==0)//if we are not undoing
    c->add(new ActionUndo(action_rotate_90,1,0));
  return 0;
}

char * action_rotate_180(Canvas *c,int u,int){
 action_mirror_ud(c,1,0);
 action_mirror_lr(c,1,0);
 if(u==0)//if we are not undoing
   c->add(new ActionUndo(action_rotate_180,1,0));
 return 0;
}

/////Roll//////////////////////////////////////////////////////////////////////
char *action_roll(Canvas *c,int splitx,int splity){
  int w=c->width();
  int h=c->height();
  splitx%=w;splity%=h;
  if(splitx<0) splitx+=w;
  if(splity<0) splity+=h;
  Pixel *ex_image=c->new_image(w,h);
  for(int y=0;y<h;y++){
    if(w!=splitx)
      memcpy(&c->pixel(0,y),ex_image+((splity+y)%h)*w+splitx,
	     (w-splitx)*sizeof(Pixel));
    if(splitx!=0)
      memcpy(&c->pixel(w-splitx,y),ex_image+((splity+y)%h)*w,
	     splitx*sizeof(Pixel));
  }
  delete[] ex_image;
  return 0;
}

