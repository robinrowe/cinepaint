#include <stdio.h>
#include <math.h>
#include <Fl/Fl_Widget.h>
#include "action.h"
#include "canvas.h"
#include "tool.h"
#include "draw.h"
#include "pen.h"
#include "paint.h"
#include "win_tools.h"
/////Tool//////////////////////////////////////////////////////////////////////
//Connect tool->pen->paint->canvas
void Tool::setup(Canvas * c,Pen *pen,Paint *paint){
  if(paint!=0)
    paint->canvas(c);
  if(pen!=0)
    pen->canvas(c);
  canvas_=c;
  pen_=pen;
  paint_=paint;
}

/*
  returns an array integers, in this format:
    x1,y1,x2,y2 of line 1
    x1,y1,x2,y2 of line 2
    ...
    -1,-1,-1,-1: last line
  the caller will delete[] the array.

  if 0 returned, no lines are drawn.
*/
int *Tool::on_draw(){return 0;}

//these are called at events. return true if you want a redraw
bool Tool::on_drag(int,int){return false;}
bool Tool::on_release(int,int){return false;}
bool Tool::on_move(int,int){return false;}
Fl_Group *Tool::on_install(){return 0;}
void Tool::on_uninstall(){}

////A tool with control points/////////////////////////////////////////////////
ToolControlPoints::ToolControlPoints(){
  num_knots=selected_knot=-1;
}

bool ToolControlPoints::on_click(int x,int y){
  int dx,dy,d,dmin=0xFFFFFFF;
  selected_knot=-1;
  for(int i=0;i<num_knots;i++){
    dx=x-knot[i].x;
    dy=y-knot[i].y;
    d=dx*dx+dy*dy;
    if(d<dmin){
      dmin=d;
      selected_knot=i;
    }
  }
  if(dmin>200)
    selected_knot=-1;
  return true;
}

bool ToolControlPoints::on_drag(int x,int y){
  if(selected_knot<0||selected_knot>=num_knots)
    return false;
  knot[selected_knot].x=x;
  knot[selected_knot].y=y;
  return true;
}
///////////////////////////////////////////////////////////////////////////////
bool ToolDot::on_click(int x,int y){
  xold=x;yold=y;
  pen()->pset(x,y);
  paint()->unshadow();
  return true;
}

bool ToolDot::on_drag(int x,int y){  
  line(xold,yold,x,y,-1,-1,pen()->pixelsetter,pen());
  xold=x;yold=y;
  paint()->unshadow();
  return true;
}

bool ToolDot::on_release(int ,int ){
  canvas()->add(new ShadowUndo(canvas()));
  canvas()->del_shadow();
  return false;
}

/////Line tool/////////////////////////////////////////////////////////////////
bool ToolLine::on_click(int x,int y){
  xold=x;yold=y;
  pen()->pset(x,y);
  paint()->unshadow();
  return true;
}

bool ToolLine::on_drag(int x,int y){
  canvas()->undo_shadow();
  line(xold,yold,x,y,-1,-1,pen()->pixelsetter,pen());
  paint()->unshadow();
  return true;
}

bool ToolLine::on_release(int, int){
  canvas()->add(new ShadowUndo(canvas()));
  canvas()->del_shadow();
  return false;  
}

//////////////////////////////////////////////////////////////////////////////
bool ToolFill::ptester(int x,int y,void *data){
  ToolFill *me=(ToolFill *)data;
  if(me->canvas()->shadow(x,y)>0)
    return false;
  int d=difference(me->canvas()->pixel(x,y),me->base_pixel);
  return d<=me->max_pixel_difference;
}

bool ToolFill::on_click(int x,int y){
  if(pen()->pressure()==0)
    return false;
  if(x<0|y<0||x>=canvas()->width()||y>=canvas()->height())
    return false;
  max_pixel_difference=0;
  base_pixel=canvas()->pixel(x,y);
  fill(x,y,canvas()->width(),canvas()->height(),ptester,this,
       pen()->slicer,pen());
  paint()->unshadow();
  canvas()->add(new ShadowUndo(canvas()));
  canvas()->del_shadow();
  return true;
}

/////Rectangle tool////////////////////////////////////////////////////////////
bool ToolRect::on_click(int x,int y){
  xold=x;yold=y;  
  pen()->pset(x,y);
  paint()->unshadow();
  
  return true;
}

bool ToolRect::on_drag(int x,int y){
  canvas()->undo_shadow();
  if(x==xold||y==yold){
    pen()->pset(x,y);
    paint()->unshadow();
    return true;
  }
  int dx=xold<x?1:-1;
  int xx=xold-dx;
  do {
    xx+=dx;    
    pen()->pset(xx,yold);
    pen()->pset(xx,y);
  }while(xx!=x);

  int dy=yold<y?1:-1;
  int yy=yold;
  do {
    pen()->pset(xold,yy);
    pen()->pset(x,yy);
    yy+=dy;
  }while(yy!=y);
  paint()->unshadow();
  return true;
}

bool ToolRect::on_release(int, int){
  canvas()->add(new ShadowUndo(canvas()));
  canvas()->del_shadow();
  return false;  
}

/////Filled rectangle tool/////////////////////////////////////////////////////
bool ToolFRect::on_click(int x,int y){
  xold=x;yold=y;
  pen()->pset(x,y);
  paint()->unshadow();
  return true;
}

bool ToolFRect::on_drag(int x,int y){
  canvas()->undo_shadow();
  int xx=xold;
  int yy=yold;
  int w=canvas()->width(),h=canvas()->height();
  ORDER(int,xx,x);
  ORDER(int,yy,y);
  CLIP(x,y,0,0,w-1,h-1);
  CLIP(xx,yy,0,0,w-1,h-1);
  canvas()->dirty(xx,yy,x,y);
  for(int i=yy;i<=y;i++){
    memset(&canvas()->shadow(xx,i),pen()->pressure(),x-xx+1);
    pen()->pset(xx,i);
    pen()->pset(x,i);
  }
  for(;xx<x;xx++){
    pen()->pset(xx,yy);
    pen()->pset(xx,y);
  }
  paint()->unshadow();
  return true;
}

bool ToolFRect::on_release(int, int){
  canvas()->add(new ShadowUndo(canvas()));
  canvas()->del_shadow();
  return false;  
}

///////////////////////////////////////////////////////////////////////////////
bool ToolRoll::on_move(int x,int y){
  splitx=x%canvas()->width();splity=y%canvas()->height();
  return true;
}

int* ToolRoll::on_draw(){
  int *p=new int[12];
  p[0]=0;p[1]=splity;p[2]=canvas()->width();p[3]=splity;
  p[4]=splitx;p[5]=0;p[6]=splitx;p[7]=canvas()->height();
  p[8]=p[9]=p[10]=p[11]=-1;
  return p;
}

bool ToolRoll::on_click(int sx,int sy){
  action_roll(canvas(),sx,sy);
  canvas()->add(new ActionUndo(action_roll,canvas()->width()-sx,
			       canvas()->height()-sy));
  return true;
}

/////A Circle drawing tool/////////////////////////////////////////////////////

ToolCircle::ToolCircle(){
  is_ready=false;
  control[0].x=control[0].y=control[1].x=control[1].y=control[2].x=control[2].y=0;
  knots(control,UBOUND(control));
}

bool ToolCircle::on_click(int x,int y){
  if(is_ready)
    return ToolControlPoints::on_click(x,y);
  control[0].x=x;
  control[0].y=y;
  control[1].x=x;
  control[1].y=y;
  control[2].x=x;
  control[2].y=y;
  drawcircle();
  return true;
}

bool ToolCircle::on_drag(int x,int y){
  canvas()->undo_shadow();
  if(is_ready){
    ToolControlPoints::on_drag(x,y);
  }else{
    int dx=x-control[0].x;
    int dy=y-control[0].y;
    int r=(int)sqrt(double(dx*dx+dy*dy));
    control[1].x=control[0].x;
    control[1].y=control[0].y-r;
    control[2].x=control[0].x-r;
    control[2].y=control[0].y;
  }
  drawcircle();
  //update toolbar
  child->val_ox->printf_no_cb("%d",control[0].x);
  child->val_oy->printf_no_cb("%d",control[0].y);
  child->val_px->printf_no_cb("%d",control[1].x);
  child->val_py->printf_no_cb("%d",control[1].y);
  child->val_qx->printf_no_cb("%d",control[2].x);
  child->val_qy->printf_no_cb("%d",control[2].y);  
  return true;
}

bool ToolCircle::on_release(int,int){
  is_ready=true;
  return true;
}

int* ToolCircle::on_draw(){
  if(!is_ready) return 0;
  int *d=new int[12];
  d[0]=d[4]=control[0].x;
  d[1]=d[5]=control[0].y;
  d[2]=control[1].x;
  d[3]=control[1].y;
  d[6]=control[2].x;
  d[7]=control[2].y;
  d[8]=d[9]=d[10]=d[11]=-1;
  return d;
}

void ToolCircle::drawcircle(){
  int64 A,B,C,D,E,F;
  int64 Ox=control[0].x;
  int64 Oy=control[0].y;
  int64 Px=control[1].x-Ox;
  int64 Py=control[1].y-Oy;
  int64 Qx=control[2].x-Ox;
  int64 Qy=control[2].y-Oy;
  int64 det=Px*Qy-Qx*Py;
  //printf("OPQ=%d,%d,%d,%d,%d,%d\n",Ox,Oy,Px,Py,Qx,Qy);
  if(det==0)
    return;
  A=Qy*Qy+Py*Py;
  B=-2*(Px*Py+Qx*Qy);
  C=Qx*Qx+Px*Px;
  D=-2*A*Ox-B*Oy;
  E=-2*C*Oy-B*Ox;
  F=A*Ox*Ox+C*Oy*Oy+B*Ox*Oy-det*det;
  int xs=control[1].x,ys=control[1].y;
  canvas()->undo_shadow();
  conic(xs,ys,xs,ys,A,B,C,D,E,F,pen()->pixelsetter,pen());
  paint()->unshadow();
}

void ToolCircle::update(Fl_Widget *,void *v){
  ToolCircle *me=(ToolCircle *)v;
  sscanf(me->child->val_ox->value(),"%d",&me->control[0].x);
  sscanf(me->child->val_oy->value(),"%d",&me->control[0].y);
  sscanf(me->child->val_px->value(),"%d",&me->control[1].x);
  sscanf(me->child->val_py->value(),"%d",&me->control[1].y);
  sscanf(me->child->val_qx->value(),"%d",&me->control[2].x);
  sscanf(me->child->val_qy->value(),"%d",&me->control[2].y);
  me->drawcircle();
}

void ToolCircle::make(Fl_Widget *,void *v){
  ToolCircle *me=(ToolCircle *)v;
  me->drawcircle();
  me->is_ready=false;
  me->control[0].x=me->control[0].y=
    me->control[1].x=me->control[1].y=
    me->control[2].x=me->control[2].y=0;
  me->canvas()->add(new ShadowUndo(me->canvas()));
  me->canvas()->del_shadow();

}

Fl_Group *ToolCircle::on_install(){
  child=new PanelToolCircle(this);
  update(child->pannel,this);
  return child->pannel;
}

void ToolCircle::on_uninstall(){
  delete child;
  if(canvas()==0) return;
  canvas()->undo_shadow();
  canvas()->del_shadow();
}

///////////////////////////////////////////////////////////////////////////////
ToolFCircle::ToolFCircle(){
}

void ToolFCircle::drawcircle(){
  int64 A,B,C,D,E,F;
  int64 Ox=control[0].x;
  int64 Oy=control[0].y;
  int64 Px=control[1].x-Ox;
  int64 Py=control[1].y-Oy;
  int64 Qx=control[2].x-Ox;
  int64 Qy=control[2].y-Oy;
  int64 det=Px*Qy-Qx*Py;
  //printf("OPQ=%d,%d,%d,%d,%d,%d\n",Ox,Oy,Px,Py,Qx,Qy);
  if(det==0)    
    return;
  A=Qy*Qy+Py*Py;
  B=-2*(Px*Py+Qx*Qy);
  C=Qx*Qx+Px*Px;
  D=-2*A*Ox-B*Oy;
  E=-2*C*Oy-B*Ox;
  F=A*Ox*Ox+C*Oy*Oy+B*Ox*Oy-det*det;
  int xs=control[1].x,ys=control[1].y;
  canvas()->undo_shadow();
  //conic(xs,ys,xs,ys,A,B,C,D,E,F,pen()->pixelsetter,pen());
  fconic(double(A),
	double(B),
	double(C),
	double(D),
	double(E),
	double(F),
	pen()->slicer,
	pen(),
	canvas()->width(),
	canvas()->height());
  conic(xs,ys,xs,ys,
	A,B,C,D,E,F,
	pen()->pixelsetter,pen());

  paint()->unshadow();
}

///////////////////////////////////////////////////////////////////////////////
/*
ToolWarp::ToolWarp(){
  //knots=new 
}
ToolWarp::~ToolWarp(){
  delete[] knots;
}
virtual int* ToolWarp::on_draw(){
}
*/
