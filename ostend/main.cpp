#include <stdlib.h>
#include <stdio.h>
#include <Fl/fl_draw.h>
#include "ui_tools.h"
#include "errorchecker.h"
#include "win_main.h"
#include "win_stuff.h"
Fl_Preferences *rootprefs;
//const char minminversion_msg[]=INTERNAL_NAME " v" VERSION " (c) 2002 Yperman Hans\n";
const char minminhelp_msg[]=
"Give any of the following options.  Unrecognized options or "
"options after -- are handled as file names\n"
"*The normal X and fltk options:\n"
"\t-bg\n"
"\t-bg2\n"
"\t-display\n"
"\t-dnd\n"
"\t-fg\n"
"\t-geometry\n"
"\t-kbd\n"
"\t-nokbd\n"
"\t-name\n"
"\t-scheme\n"
"\t-title\n"
"\t-tooltips\n"
"*help options:\n"
"\t--help\t\tyou are reading it now\n"
"\t--version\t dump 1 line of version info\n";


void init(int argc,char *argv[]){
  //parse basic arguments
  Program *p=new Program(argc,argv);
  if(p->argument("--version")){
    printf(VERSION);
    exit(0);
  }
  if(p->argument("--help")){
    printf(PROGRAM_NAME);
    printf(minminhelp_msg);
    exit(0);
  }

  //setup a font.  Now you can ask(...) things.
  fl_font(FL_TIMES,14);
  //setup SIGSEGV handler 
#ifndef _WIN32
  if(!setup_errorchecker(argv[0]))
    fprintf(stderr,"Could not start error handler.\n"
	    "Please check if stdin & stdout are TTY's");
#endif
  //initialize configuration file
  rootprefs=new Fl_Preferences(Fl_Preferences::USER,
			       PACKAGE_NAME,PROGRAM_NAME);
  rootprefs->set("version",VERSION);

  //init do not ask-questions
  ask_init(new Fl_Preferences(*rootprefs,"questions"));

  //init temp file manager
  //don't init: We don't need it as yet, and it's buggy
  //free(program->tmpfile());
}

void deinit(){
  //Now take the program down
  ask_exit();
  unregister_images();
  delete rootprefs;
  delete &Program::get(); //takes temp files with it
}

int main(int argc,char *argv[]){
  try {
    init(argc,argv);

    MainWindow *w=new MainWindow();
    w->open_argument_files();
    w->run();
    delete w;
    
    deinit();
  }catch(...){
    fprintf(stderr,"Unhandled exception caught !\n");
  }
}
