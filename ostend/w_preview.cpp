#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <Fl/Fl.h>
#include <Fl/fl_draw.h>
#include "w_preview.h"
#include "file.h"
#include "action.h"
#include <unistd.h>

using namespace std;

W_Preview::W_Preview(int x,int y,int w,int h,const char *l)
  :Fl_Widget(x,y,w,h,l){
  file_selection=strdup("*");
}

W_Preview::~W_Preview(){
  clean_previews();
  dirchanged->unsubscribe(directory_changed,this);
  free(file_selection);
}

void W_Preview::on_dirchange(Broadcaster *b){  
  b->subscribe(directory_changed,this);
  dirchanged=b;
}

/////Events////////////////////////////////////////////////////////////////////
int W_Preview::handle(int event){
  int x=this->x()+Fl::box_dx(box());
  int y=this->y()+Fl::box_dy(box());
  int w=this->w()-Fl::box_dw(box());
  int h=this->h()-Fl::box_dh(box());
  int colsz=size+border_l+border_r;
  int rowsz=size+border_u+border_d;
  int ppr=w/colsz,ppc=h/rowsz;//pictures/row & pictures/column
  int imageno=ppr*my_scrollbar->value();  
  if(ppr==0||ppc==0)
    return 0;

  switch(event){
  case FL_PUSH:
    sel_end=sel_start=imageno+(Fl::event_y()-y)/rowsz*ppr+(Fl::event_x()-x)/colsz;
    //no break
  case FL_DRAG:
  case FL_RELEASE:
    //undo everything in the given rectangle
    int dir;
    if(Fl::event_state(FL_CTRL)){
      if(event!=FL_PUSH){
	dir=1;if(sel_end<sel_start) dir=-1;
	for(int i=sel_start;i!=sel_end+dir;i+=dir){
	  if(i>=(int)images.size()||i<0)
	    break;
	  images[i]->selected=!images[i]->selected;
	}
      }
    }else{
      for(int i=0;i<(int)images.size();i++)
	images[i]->selected=false;
    }
    //select new end point
    sel_end=imageno+(Fl::event_y()-y)/rowsz*ppr+(Fl::event_x()-x)/colsz;
    //do everything in the given rectangle
    dir=1;if(sel_end<sel_start) dir=-1;
    for(int i=sel_start;i!=sel_end+dir;i+=dir){
      if(i>=(int)images.size()||i<0)
	break;
      if(Fl::event_state(FL_CTRL))
	images[i]->selected=!images[i]->selected;
      else
	images[i]->selected=true;
    }
    redraw();
    return 1;
  }
  return Fl_Widget::handle(event);
}

void W_Preview::draw(){
  draw_box();
  int x=this->x()+Fl::box_dx(box());
  int y=this->y()+Fl::box_dy(box());
  int w=this->w()-Fl::box_dw(box());
  int h=this->h()-Fl::box_dh(box());
  fl_push_clip(x,y,w,h);
  int colsz=size+border_l+border_r;
  int rowsz=size+border_u+border_d;
  int ppr=w/colsz,ppc=h/rowsz;//pictures/row & pictures/column
  if(ppr==0||ppc==0)
    return;
  int imageno=ppr*my_scrollbar->value();
  int yend=y+h,xend=x+w;  
  int v=my_scrollbar->value();
  if(v*ppr>(int)images.size())
    v=0;
  my_scrollbar->value(v,ppc,0,int(images.size())/ppr+1);
  my_scrollbar->redraw();
  for(int yy=y;yy<yend;yy+=rowsz){
    int box_y1=yy+border_u,box_y2=box_y1+size+text_offset;
    fl_color(FL_DARK3);
    fl_rectf(x,yy,xend-x,border_u-1);
    fl_rectf(x,box_y2+1,xend-x,border_d-text_offset-1);
    for(int xx=x;xx<xend-colsz;xx+=colsz){
      if(imageno>=(int)images.size()){
	fl_color(FL_DARK3);
	fl_rectf(xx,yy,xend-xx,rowsz);	
	fl_rectf(x,yy+rowsz,xend-x,yend-yy+rowsz);
	goto breakloops;
      }
      int box_x1=xx+border_l,box_x2=box_x1+size;
      fl_color(FL_DARK3);
      fl_rectf(xx,yy,border_l,rowsz);
      fl_rectf(box_x2,yy,border_r,rowsz);

      //fl_rectf(box_x1,box_y1,box_x2-box_x1,box_y2-box_y1);
      Canvas *c=images[imageno]->image;
      int w=c->width(),h=c->height();
      if(images[imageno]->selected)
	fl_color(0,0,64);
      else
	fl_color(FL_BACKGROUND_COLOR);

      fl_rectf(box_x1+1,box_y1,(size-w)/2-1,box_y2-box_y1);
      fl_rectf(box_x1+(size+w)/2,box_y1,(size-w-1)/2,box_y2-box_y1);
      fl_rectf(box_x1+1,box_y1,box_x2-box_x1-2,(size-h)/2);
      fl_rectf(box_x1+1,box_y1+(size+h)/2,box_x2-box_x1-2,(size-h+1)/2+text_offset);

      fl_draw_image((const uchar *)(&(c->pixel(0,0))),
		    xx+border_l+(size-w)/2,yy+border_u+(size-h)/2,w,h,4);

      fl_color(FL_WHITE);
      fl_yxline(box_x1,box_y2,box_y1-1,box_x2-1);
      fl_color(FL_BLACK);
      fl_xyline(box_x1-1,box_y2,box_x2-1,box_y1);
      if(images[imageno]->selected)
	fl_color(FL_WHITE);

      char *imgname=images[imageno]->name;
      imgname=strrchr(imgname,'/')+1;      
      if(imgname==0)
	imgname=images[imageno]->name;
      imgname=strdup(imgname);
      int l=strlen(imgname);
      if(l>max_name_chars)
	imgname[max_name_chars]=0;
      fl_draw(imgname,box_x1+1,box_y2-fl_descent()-1);
      free(imgname);
      imageno++;      
    }
  }
 breakloops:
  fl_color(FL_DARK3);
  fl_rectf(x+ppr*colsz,y,xend-(x+ppr*colsz),yend-y);
  fl_pop_clip();
}

void W_Preview::directory_changed(void *p,void *){
  W_Preview *me=(W_Preview *)p;
  me->my_scrollbar->value(0,1,0,1);
  me->load_directory();
  me->redraw();
}

void W_Preview::scrolled(Fl_Widget *,void *p){
  W_Preview *me=(W_Preview *)p;
  me->redraw();
}
///////////////////////////////////////////////////////////////////////////////
void W_Preview::load_directory(){
  clean_previews();
  vector<FileData*> files;
  dir->read(files,file_selection);
  LongString l;
  for(int i=0;i<(int)files.size();i++){
    l.clear();
    l.append(dir->cwd());
    l.append('/');
    l.append(files[i]->name);
    char *err=0;
    Canvas *c=File::open(l.value(),"*",&err);
    if(c==0){
      free(err);
      continue;
    }
    Previewdata *d=new Previewdata;
    if(c->width()>size||c->height()>size){
      int w=c->width(),h=c->height();
      int ex_w=w,ex_h=h;
      if(w>h){
	h=h*size/w;
	w=size;
      }else{
	w=w*size/h;
	h=size;
      }
      //Weird images are made on the world
      if(w==0) w=1;
      if(h==0) h=1;      
      Pixel *ex_data=c->new_image(w,h);
      resize_fast(c,ex_w,ex_h,ex_data);
      delete[] ex_data;
    }
    d->image=c;
    d->name=strdup(l.value());
    d->selected=false;
    images.push_back(d);
  }
  dir->cleanup(files);
}

void W_Preview::clean_previews(){
  while(images.size()>0){
    Previewdata *d=images[images.size()-1];
    free(d->name);
    delete d->image;
    delete d;
    images.pop_back();
  }
}

/*
  start with i=0; keep calling this until the return value is NULL.
  This way, you'll get any selected filename, with fully fledged path.
*/
const char *W_Preview::selection(int& i){
  for(;i<(int)images.size();i++){
    if(images[i]->selected){
      printf("%s\n",images[i]->name);
      return images[i++]->name;
    }
  }
  return 0;
}
