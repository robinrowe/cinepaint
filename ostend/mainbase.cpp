#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <Fl/Fl_Menu_Item.h>
#include "win_paint.h"
#include "mainbase.h"
#include "win_main.h"
#include <unistd.h>

MainBase::MainBase(){
  image=0;
  image_count=image_now=0;
}

MainBase::~MainBase(){
  for(int i=0;i<image_count;i++)
    delete image[i];
  if(image!=0)
    delete[] image;
}

typedef Canvas* Canvas_ptr;

void MainBase::add(Canvas *img){
  Canvas **il=new Canvas_ptr[image_count+1];
  il[image_count]=img;
  if(image!=0){    
    memcpy(il,image,sizeof(image[0])*image_count);
    delete[] image;
  }
  image=il;
  image_count++;
  select_image(image_count-1);
}

void MainBase::remove(Canvas *img){
  int j=0;
  for(int i=0;i<image_count;i++)
    if(image[i]!=img){
      image[j++]=image[i];
    }else
     delete image[i];
  image_count=j;
  select_image(image_count-1);
}

void MainBase::select_image(int n){
  image_now=n;
}

const char* default_width="640";
const char* default_height="480";

void MainBase::add_image(){
  Win_AskSize w;
  w.width->value(default_width);
  w.height->value(default_height);
  for(;;){    
    //ok was pressed
    if(!w.run()||!w.width->good()||!w.height->good())
      continue;
    int wi=strtol(w.width->value(),0,10);
    int he=strtol(w.height->value(),0,10);
    if(wi<=0||he<=0)
      continue;
    add(new Canvas(wi,he));
    return;
  }
}

void MainBase::generate_view_menu(Fl_Menu_Bar *menubar,int view_index,
				  Fl_Callback *c){
  Fl_Menu_Item *menu=(Fl_Menu_Item *)(menubar->menu());
  menu+=view_index;

  //get rid of old menu
  Fl_Menu_Item *oldsubmenu=(Fl_Menu_Item *)menu->user_data();
  if(oldsubmenu!=0){
    for(int i=0;oldsubmenu[i].label()!=0;i++)
      free((char *)oldsubmenu[i].label());
    oldsubmenu--;
    delete[] oldsubmenu;    
  }

  //add new menu
  Fl_Menu_Item *newsubmenu=new Fl_Menu_Item[image_count+2];
  memset(newsubmenu,0,sizeof(Fl_Menu_Item)*(image_count+2));
  newsubmenu++;//bug in fltk setonly
  menu->user_data(newsubmenu);
  menu->flags|=FL_SUBMENU_POINTER;

  //fill with something that makes some sense
  for(intptr_t i=0;i<image_count;i++){
    newsubmenu[i].label(strdup(image[i]->param("Name")));
    newsubmenu[i].flags=FL_MENU_RADIO;
    newsubmenu[i].user_data((void*)(i));
    newsubmenu[i].callback(c);
    if(i==image_now)
      newsubmenu[i].flags|=FL_MENU_VALUE;
  }
}
