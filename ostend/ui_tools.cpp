/*
  ui tools (c) 2002 Yperman Hans  
*/
#include <vector>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <Fl/Fl.h>
#include <Fl/Fl_Window.h>
#include <Fl/Fl_Button.h>
#include <Fl/Fl_Box.h>
#include <Fl/Fl_Round_Button.h>
#include <Fl/Fl_Return_Button.h>
#include <Fl/Fl_Pixmap.h>
#include <Fl/Fl_XPM_Image.h>
#include <Fl/Fl_Preferences.h>
#include <Fl/fl_draw.h>
#include "ui_tools.h"
#include "util.h"

using namespace std;

/////Modal runner v2///////////////////////////////////////////////////////////
static Fl_Widget * modal_widget_closer=0;
Fl_Widget *run_modal(Fl_Window *w){
  Fl_Widget *mw=modal_widget_closer,*mw2;
  modal_widget_closer=0;
  bool vis=(0!=w->visible());
  w->set_modal();
  if(!vis)
    w->show();  
  do{    
    Fl::wait();
  }while(modal_widget_closer==0);
  if(!vis)
    w->hide();
  mw2=modal_widget_closer;
  modal_widget_closer=mw;
  return mw2;
}

void run_modal_callback(Fl_Widget *wid,void *){
  modal_widget_closer=wid;
}
/////Named image cache ////////////////////////////////////////////////////////
static struct NImageCachePtr{
  NImageCachePtr *left,*right;
  const char *name;
  Fl_Image *image;
  ~NImageCachePtr();
} *name_imageroot=0;

//register something, then return true.
//after the call, both name and img should stay readable forever
//name will be left alone, img will be deleted at shutdown time
bool register_image(const char *name,Fl_Image *img){
  NImageCachePtr **p=&name_imageroot;
  while(*p!=0){
    int n=strcmp(name,(*p)->name);
    if(n==0){
      Fl_Image* ptr=(*p)->image;
      delete ptr;
      (*p)->image=img;
      return true;
    }if(n<0)
      p=&((*p)->left);
    else
      p=&((*p)->right);
  }
  *p=new NImageCachePtr();
  (*p)->left=(*p)->right=0;
  (*p)->image=img;
  (*p)->name=name;
  return true;
}

//register name. with a given xpm-string.
//if xpmdata==0, then try to open "name" as an xpm-file. if it's first line
//is "/* XPM */", use that, and crash if it was noit an xpm after all.
//The last 'feature' is there because i can't get fltk to return errormessages
//if something goes wrong

bool register_image(const char *name,const char **xpmdata){
  if(xpmdata!=0)    
    return register_image(name,new Fl_Pixmap(xpmdata));
  FILE *fp=fopen(name,"r");
  if(fp==0) return false;
  char buf[256];fgets(buf,255,fp);fclose(fp);
  char c=0;
  sscanf(buf," /* XPM *%c",&c);
  if(c!='/') return false;
  return register_image(name,new Fl_XPM_Image(name));
}

//unregister all images
void unregister_images(){
  if(name_imageroot!=0)
    delete name_imageroot;
}

NImageCachePtr::~NImageCachePtr(){
  delete image;
  if(left!=0) delete left;
  if(right!=0) delete right;
  
}
//if prev==0, return the asciibethical first registered image.
//else if prev is a registered name. return the next one.
// return 0 if this was the last
char *get_registered_name(const char *prev){
  (void)prev;
  return 0;
}

//return the image registered to this name, or 0 if nothing found
//Probably,this also should be lots faster. If there are more images in the cache
Fl_Image *get_image(const char *name){
  NImageCachePtr *p=name_imageroot;
  while(p!=0){
    int n=strcmp(name,p->name);
    if(n==0)
      return p->image;
    if(n<0)
      p=p->left;
    else
      p=p->right;
  }
  return 0;
}

///////////////////////////////////////////////////////////////////////////////
/*
  Ask a question.
  *Title, the window title, is used to detect if a question is asked again.  It should
  always be the same for the same question.  What i mean is: this is valid:
  >sprintf(buffer,"close image '%s',image->name);
  >ask("Close Image?",buffer, ... );
  This is not:
  >ask(buffer,buffer,...);
  *askask=true-> there will be an Ask Again line.
  *the button order is RIGHT TO LEFT!
  *The question is possibly not asked. (Ask Again might be Never)
  *returned value is:
    0,1,2 -> button 0,1,2 was pressed
    3..9 -> reserved for future buttons.
    10 -> cancelled somehow, e.g. by pressing ESC or closing the window.
*/

static Fl_Preferences *ask_prefs=0;
static vector<char *> notsession;
static vector<int> notsessionvals;

void ask_init(Fl_Preferences *p){
  ask_prefs=p;
}

void ask_exit(){
  for(int i=0;i<(int)notsession.size();i++)
    free(notsession[i]);
  delete ask_prefs;
}

int ask(const char *title,Fl_Image *img,const char *text,
	const char *btn0,const char *btn1,const char *btn2,bool askask){
  int textw=0,texth=0;
  //check if session wants us to ask this. tryed thuis with a map<>, and failed
  //so ...
  {
    for(int i=0;i<(int)notsession.size();i++)
      if(strcmp(title,notsession[i])==0)
	return notsessionvals[i];
  }

  //mangle the title. Fl_prefs is to stupid.
  char *minititle=strdup(title);
  for(char *s=minititle;*s!=0;s++){
    if(!isalnum(*s)&&*s!='_')
      *s='_';
  }


  //check if program wants us to ask this
  {
    int n;
    if(ask_prefs==0)
      warn(__FILE__".ask():ask_prefs=0");
    else if(ask_prefs->get(minititle,n,10)){
      free(minititle);
      return n;
    }
  }
  
  //decide about size
  fl_measure(text,textw,texth,0);
  CLIP(textw,texth,350,16,600,100);
  static const int border=8,askask_height=fl_height(),button_height=24;
  int img_width=64,img_height=64;
  int width=border*3+img_width+textw;
  int height=border*4+askask_height+MAX(texth,img_height)+button_height;
  Fl_Window win(width,height,title);
  win.callback(run_modal_callback);
  win.begin();

  //image and text
  Fl_Box image(border,border,img_width,img_height);
  image.image(img);

  //image.box(FL_UP_BOX);
  Fl_Box textbox(border*2+img_width,border,textw,texth,text);
  //textbox.box(FL_UP_BOX);

  //ask again line
  Fl_Box askagain(border,border*2+img_width,70,
		  askask_height,"Ask again:");
  Fl_Round_Button always(border*2+70,border*2+img_width,70,
			 askask_height,"Always");
  Fl_Round_Button session(border*2+150,border*2+img_width,150,
			  askask_height,"Not in this session");
  Fl_Round_Button never(border*2+300,border*2+img_width,100,
			askask_height,"Never");
  always.type(FL_RADIO_BUTTON);
  session.type(FL_RADIO_BUTTON);
  never.type(FL_RADIO_BUTTON);
  always.setonly();
  if(!askask){
    askagain.hide();
    always.hide();
    session.hide();
    never.hide();
  }

  //button line
  Fl_Button *answer[3];
  int button_y=border*3+askask_height+MAX(texth,img_height);
  int button_x=width-border;
  for(int i=0;i<3;i++){
    const char *t=0;
    switch(i){
    case 0:t=btn0;break;
    case 1:t=btn1;break;
    case 2:t=btn2;break;
    }
    if(t==0){
      answer[i]=0;
      continue;
    }
    int w=(int)fl_width(t)+2*border;
    button_x-=w;
    if(i==0){
      button_x-=16;
      answer[i]=new Fl_Return_Button(button_x,button_y,w+16,button_height,t);
    }else
      answer[i]=new Fl_Button(button_x,button_y,w,button_height,t);

    answer[i]->callback(run_modal_callback);
    button_x-=border;
  }
  win.end();

  //go !!
  Fl_Widget *result=run_modal(&win);

  //We may compare the pointers , but not follow them.
  int a=10;
  if(result==answer[0])
    a=0;
  else if(result==answer[1])
    a=1;
  else if(result==answer[2])
    a=2;

  if(never.value()){
    //if we should never ask this again, store it in the prefs
    ask_prefs->set(minititle,a);
  }else if(session.value()){
    //if we shouldn't ask it again this session, put it in notsession
    notsession.push_back(strdup(title));
    notsessionvals.push_back(a);
  }
  free(minititle);
  return a;
}
