#ifndef _X_MAINBASE_H_
#define _X_MAINBASE_H_
#include <Fl/Fl_Widget.h>
#include <Fl/Fl_Menu_Bar.h>
#include "canvas.h"
class MainBase {
  int image_count,image_now;
  Canvas **image;
 public:
  MainBase();
  virtual ~MainBase();
  void add_image();
  void add(Canvas *);
  void remove(Canvas *);
  void generate_view_menu(Fl_Menu_Bar*bar,int index,Fl_Callback *c);
  virtual void select_image(int index);
  Canvas *selected(){return image_count==0?0:image[image_now];}
};
#endif
