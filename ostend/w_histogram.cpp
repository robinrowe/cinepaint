#include <stdio.h>
#include <Fl/Fl.h>
#include <Fl/fl_draw.h>
#include "w_histogram.h"
#include "util.h"
W_Histogram::W_Histogram(int x,int y,int w,int h,const char *l)
  :Fl_Widget(x,y,w,h,l){
  my_canvas=0;
  my_mode='r';
}

W_Histogram::~W_Histogram(){
}

void W_Histogram::update(){
  memset32(bucket,0,buckets);
  for(int y=0;y<my_canvas->height();y++)
    for(int x=0;x<my_canvas->width();x++){
      Pixel p=my_canvas->pixel(x,y);
      int b;
      int h,s,v,a;
      pixel2hsv(h,s,v,a,p);
      switch(my_mode){
      case 'r':b=RDATA(p);break;
      case 'g':b=GDATA(p);break;
      case 'b':b=BDATA(p);break;
      case 'a':b=ADATA(p);break;
      case 'h':b=h;break;
      case 's':b=s;break;
      case 'v':b=v;break;
      default:b=0;warn("W_Histogram: invalid mode '%c'",my_mode);
      }
      if(b>=buckets||b<0)
	fprintf(stderr,"Internal error in Histogram:\n"
		"Value %d does  not belong to a bucket \n",b);
      else
	bucket[b]++;
    }
  max=1;
  for(int i=0;i<buckets;i++)
    if(bucket[i]>max)
      max=bucket[i];
  redraw();
}

void W_Histogram::draw(){
  draw_box();
  int x=this->x()+Fl::box_dx(box());
  int y=this->y()+Fl::box_dy(box());
  int w=this->w()-Fl::box_dw(box());
  int h=this->h()-Fl::box_dh(box());
  fl_color(FL_BLACK);
  for(int i=0;i<buckets;i++){
    int height=h*bucket[i]/max;
    fl_rectf(x+i*w/buckets,y+h-height,(i+1)*w/buckets-i*w/buckets,height);
  }
}
