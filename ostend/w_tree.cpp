/*
  Tree Widget (c) 2002 Yperman Hans
*/
#include <stdlib.h>
#include <string.h>
#include <Fl/Fl.h>
#include <Fl/fl_draw.h>
#include "w_tree.h"

/////TreeItem class ///////////////////////////////////////////////////////////
TreeItem::TreeItem(){
  parent=child=0;
  prev=next=this;
  flags.open=false;
  flags.active=true;
  flags.has_childs=false; //show as having childs, even if it hasn't
}

TreeItem::~TreeItem(){
  remove_childs();
}

void TreeItem::remove_childs(){
  TreeItem *i=child,*j;
  if(i==0)
    return;
  do{
    j=i->next;
    delete i;
    i=j;
  }while(i!=child);  
  child=0;
}

//Add as last child
TreeItem *TreeItem::add(TreeItem *c){
  c->parent=this;
  if(child==0){
    child=c;
  }else{
    c->prev=child->prev;
    c->next=child;
    c->prev->next=c;
    child->prev=c;
  }
  return c;
}

//Add after given item.
TreeItem *TreeItem::add(TreeItem *after,TreeItem *c){
  c->parent=this;
  if(child==0){
    child=c;
  }else if(after==0){
    c->next=child;
    c->prev=child->prev;
    child=c;
  }else {
    c->next=after->next;
    c->prev=after;
    after->next->prev=c;
    after->next=c;
  }
  return c;
}

//Remove an item. Do this NEVER with the treeroot.
//This thing does not remove it's childs
//It returns this->next, or 0 if it was the last child
TreeItem *TreeItem::remove(){
  TreeItem *result=next;
  if(next!=0&&parent->child==next)
    result=0;
  if(next==this){
    parent->child=0;
  }else{
    if(parent->child==this)
      parent->child=next;
    next->prev=prev;
    prev->next=next;
  }
  prev=next=this;parent=0;
  return result;
}

//These do not exist, but it is handy to make the root a simple TreeItem
void TreeItem::draw(int ,int ,int ,int ){abort();}
int TreeItem::height(){abort();return 666;}
int TreeItem::width(){abort();return 666;}

///// A TreeItem that displays text ///////////////////////////////////////////
TreeTextItem::TreeTextItem(const char *s){
  data=strdup(s);
}

TreeTextItem::~TreeTextItem(){
  free(data);
}

void TreeTextItem::draw(int x,int y,int w,int h){
  fl_push_clip(x,y,w,h);
  //fl_rect(x,y,w,h);
  fl_draw(data,x+2,y,w,h,FL_ALIGN_LEFT);
  fl_pop_clip();
}
int TreeTextItem::height(){
  return fl_height();
}

int TreeTextItem::width(){
  return int(fl_width(data));//Huh? Why whas it a double ?
}

/////The tree browser//////////////////////////////////////////////////////////
TreeBrowser::TreeBrowser(int x,int y,int w,int h,char *l)
  :Fl_Browser_(x,y,w,h,l){
  my_root=new TreeItem();
  my_root->flags.open=true;
}

TreeBrowser::~TreeBrowser(){
  delete my_root;
}

void *TreeBrowser::item_first() const {
  return my_root->child;
}

void *TreeBrowser::item_next(void * p) const{ 
  TreeItem *i=(TreeItem *)p;
  if(i->flags.open&&i->child!=0)
    return (void *)i->child;
  while(i!=my_root&&i->next==i->parent->child)
    i=i->parent;
  if(i==my_root)
    i=0;
  else
    i=i->next;
  return (void *)i;
}

void *TreeBrowser::item_prev(void * p) const{
  TreeItem *i=(TreeItem *)p;
  //if there is nothing before this, return parent
  if(i==my_root->child)
    return 0;
  if(i->parent->child==i)
    return i->parent;
  //go to last child of i->prev
  i=i->prev;
  while(i->child!=0&&i->flags.open)
    i=i->child->prev;
  return (void *)i;
}

int TreeBrowser::item_height(void *p) const{
  TreeItem *i=(TreeItem *)p;
  return i->height();
}

int TreeBrowser::item_width(void *p) const{
  TreeItem *i=(TreeItem *)p;
  return i->width()+square*max_indentation;
}

int TreeBrowser::item_quick_height(void *p) const{
  TreeItem *i=(TreeItem *)p;
  return i->height();
}

int TreeBrowser::handle(int event){  
  int r=Fl_Browser_::handle(event);
  TreeItem *i=(TreeItem *)selection();

  //check if clicked on something that can toggle
  if(event!=FL_PUSH||i==0||i->child==0)
    return r;

  //check if clicked on marker of it
  int w2=border+square;
  TreeItem *j=i,*j2,*end;
  while(j->parent!=0){
    w2+=square;
    j=j->parent;
  }
  if(Fl::event_x()>w2+this->x())
    return r;

  //Open or close it
  if(i->flags.open){
    //remove all childs from screen
    j=i->child;
    i->flags.open=false;
    end=(TreeItem *)item_next(i);
    while(j!=end){
      deleting(j);
      j=(TreeItem *)item_next(j);
    }
  }else{
    //add new children
    end=(TreeItem *)item_next(i);
    i->flags.open=true;
    j=i->child;
    j2=(TreeItem *)item_next(j);
    while(j2!=end){
      inserting(j,j2);
      j=j2;
      j2=(TreeItem *)item_next(j);
    }
  }
  damage(5); //redraw scrollbar & inside
  return r;
}


void TreeBrowser::item_draw(void *p,int x,int y,int w,int h) const{
  TreeItem *i=(TreeItem *)p,*j=i;
  //get indentation
  int w2=border;
  while(j->parent!=0){
    w2+=square;
    j=j->parent;
  }
  fl_color(200,200,200);
  fl_line(x,y+h-1,x+w-1,y+h-1);
  if(i->child!=0||i->flags.has_childs){
    int xr=x+w2-5,yr=y+(h>>1);
    if(i->flags.open){
      fl_color(255,0,0);
      fl_polygon(xr,yr-5,xr+5,yr,xr,yr+5);
      fl_color(0,0,0);
      fl_loop(xr,yr-5,xr+5,yr,xr,yr+5);
    }else{
      fl_color(255,0,0);
      fl_polygon(xr-5,yr-2,xr,yr+3,xr+5,yr-2);
      fl_color(0,0,0);
      fl_loop(xr-5,yr-2,xr,yr+3,xr+5,yr-2);
    }
  }    
  if(selection()==p)
    fl_color(255,255,255);
  else
    fl_color(0,0,0);
  i->draw(x+w2,y,w-w2,h);
}
