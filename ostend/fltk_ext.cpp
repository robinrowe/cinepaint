//Extensions to fltk. You'll have to port anything in here
// (c) 2002 Yperman Hans
#include <Fl/Fl.h>
#include <FL/Fl_Widget.H>
#include <FL/Fl_Window.H>
// rsr
#ifndef _WIN32
#include <X11/Xlib.h>
#include <X11/X.h>
#endif

#include <Fl/x.h>

void fl_set_mouse(int root_x,int root_y){
#ifdef _WIN32
	// BUG
	
#else
  Window rootwin=DefaultRootWindow(fl_display);
  XWarpPointer(fl_display,None,rootwin,0,0,0,0,root_x,root_y);
#endif
}

///////////////////////////////////////////////////////////////////////////////
/* to hide the cursor, you do
   start_hidden_cursor();
   while(???)
   if(cursor_moved)
     move_hidden_cursor();
   end_hidden_cursor();

   As fltk has no way to determine the current cursor, it wil be reset
   to the default cursor after end_hidden_cursor();

*/
static int hidden_x_start,hidden_y_start;
void start_hidden_cursor(Fl_Widget *w){
  Fl::get_mouse(hidden_x_start,hidden_y_start);
  //Fl::grab(w->window());
  w->window()->cursor(FL_CURSOR_NONE);
}

void end_hidden_cursor(Fl_Widget *w){
  //Fl::grab(0);
  w->window()->cursor(FL_CURSOR_DEFAULT);
}

void move_hidden_cursor(int& dx,int& dy){
  int x,y;
  Fl::get_mouse(x,y);  
  dx=x-hidden_x_start;dy=y-hidden_y_start;
  if(dx==0&&dy==0)
    return;
  fl_set_mouse(hidden_x_start,hidden_y_start);
}

