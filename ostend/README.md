# CineBrush

9/6/05

- created splat project based on antipaint GPL
- ren *.cc *.cpp (no compile options otherwise)
- include ..\..\unistd;..\..\fltk
- link ..\..\fltk\vcpp7\release fltk.lib
- antipaint requires fluid, have to stop to build fluid

9/8/05

- Fluid built

9/9/05

- Eliminate all warnings (code changes)
- Replace vasprint with arbitrary char malloc of size 256
- Set to build with RTTI because uses dynamic cast
- Everything builds except still missing libpng and libjpeg

9/13/05

- Built libfltk (non-dll version)
- LINK ..\..\fltk\vcpp7\debug or release with libfltk.lib
- Package libunistd and add to project
- LINK +  OPENGL32.LIB WSOCK32.LIB COMCTL32.LIB 

http://www.fltk.org/links.php?V178
Category:	Wiki/Software/Graphics	Rating:	2.6
Name:	AntiPaint	Popularity:	13%
Version:	0.95	License:	GPL
Author:	Yperman Hans	EMail:	hyperman@eduserv.rug.ac.be
Home Page:	http://studwww.rug.ac.be/~hyperman/antipaint (2670 visits)
Download:	http://studwww.rug.ac.be/~hyperman/antipaint/download.html (578 visits)
Description:
A paint program with some advanced, techie-friendly features.
2006.12.21
