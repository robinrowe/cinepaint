/*
  Broadcaster (c) 2002 Yperman Hans:
    if given a broadcaster, then broadcast(p2) will call every
    (BFunc,param)-colmbination.  If a BFunc is registered twice
    with different parameters, it is called twice.
    unsubscribing should been given the same parameter as subscribe
  
  Altough the implementation is trivial, this class was designed 
  for containing small numbers of functions, say,2-10.

  Depending on nothing else.
*/
#ifndef _X_BROADCASTER_H_
#define _X_BROADCASTER_H_
typedef  void (BFunc)(void *param,void *p2);
class Broadcaster {
  int count;
  BFunc **funcs;
  void **params;
public:
  Broadcaster();
  ~Broadcaster();
  void subscribe(BFunc f,void *param);
  void unsubscribe(BFunc f,void *param);
  void broadcast(void *p2=0);
};
#endif
