/*
  The Infinite Slider (c) 2002 Yperman Hans
*/
#ifndef _X_INFINITE_SLIDER_H_
#define _X_INFINITE_SLIDER_H_
#include <Fl/Fl_Valuator.h>
#include <Fl/Fl_Group.h>
#include <Fl/Fl_Float_Input.h>

class Infinite_Slider:public Fl_Valuator {
  double start,size;
  static const int place_l=5,place_r=10;
protected:
  virtual int handle(int event);
  virtual void draw();
public:
  Infinite_Slider(int x,int y,int w,int h,const char *s=0);
};

class Infinite_Slider_Num:public Fl_Group {
  Infinite_Slider *slider;
  Fl_Float_Input *input;
  static void my_callback(Fl_Widget *,void *);
 public:
  Infinite_Slider_Num(int x,int y,int w,int h,const char *s=0);
  virtual ~Infinite_Slider_Num();
  void box(Fl_Boxtype t);
  void minimum(double n);
  void maximum(double n);
  double value();
  void value(double d);
};

#endif
