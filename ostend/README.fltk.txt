http://www.fltk.org/links.php?V178
Category:	Wiki/Software/Graphics	Rating:	2.6 
Name:	AntiPaint	Popularity:	13%
Version:	0.95	License:	GPL
Author:	Yperman Hans	EMail:	hyperman@eduserv.rug.ac.be
Home Page:	http://studwww.rug.ac.be/~hyperman/antipaint (2670 visits)
Download:	http://studwww.rug.ac.be/~hyperman/antipaint/download.html (578 visits)
Description:	
A paint program with some advanced, techie-friendly features.
2006.12.21