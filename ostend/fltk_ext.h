/*
  Some functions not found in fltk.
  (c) 2002 Yperman Hans
*/
#ifndef _X_FLTK_EXT_H_
#define _X_FLTK_EXT_H_
void fl_set_mouse(int w,int h);

void start_hidden_cursor(Fl_Widget *w);
void end_hidden_cursor(Fl_Widget *w);
void move_hidden_cursor(int& dx,int& dy);
#endif
