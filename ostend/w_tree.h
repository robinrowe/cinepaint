/*
  Tree widget (c) 2002 Yperman Hans
*/
#ifndef _X_TREEWIDGET_H_
#define _X_TREEWIDGET_H_
#include <Fl/Fl_Browser_.h>
class TreeItem {
  friend class TreeBrowser;
protected:
  TreeItem *parent,*child,*prev,*next;
  struct {
    bool open:1;
    bool active:1;
    bool has_childs:1;
  }flags;
public:
  TreeItem();
  virtual ~TreeItem();
  inline void has_childs(bool b){flags.has_childs=b;}
  inline bool has_childs(){return flags.has_childs;}
  inline void is_open(bool b){flags.open=b;}
  inline bool is_open(){return flags.open;}
  
  TreeItem *add(TreeItem *);
  TreeItem *add(TreeItem * after,TreeItem *);
  TreeItem *remove();
  void remove_childs();
  TreeItem *up() const {return parent;}
  TreeItem *down() const {return child;}
  TreeItem *left() const {return prev;}
  TreeItem *right() const {return next;}
  virtual void draw(int x,int y,int w,int h);
  virtual int height();
  virtual int width();
};

class TreeTextItem:public TreeItem{
  char *data;
 public:
  void *user_data;
  TreeTextItem(const char *s);
  inline const char *value() const {return data;}
  virtual ~TreeTextItem();
  virtual void draw(int x,int y,int w,int h);
  virtual int height();
  virtual int width();  
};

class TreeBrowser:public  Fl_Browser_ {
  TreeItem *my_root;
  static const int square=10,border=3,max_indentation=10;
protected:
  virtual FL_EXPORT void *item_first() const;
  virtual FL_EXPORT void *item_next(void *) const ;
  virtual FL_EXPORT void *item_prev(void *) const ;
  virtual FL_EXPORT int item_height(void *) const;
  virtual FL_EXPORT int item_width(void *) const;
  virtual FL_EXPORT int item_quick_height(void *) const ;
  virtual FL_EXPORT void item_draw(void *,int,int,int,int) const;
  virtual FL_EXPORT int handle(int);
public:
  TreeBrowser(int x,int y,int w,int h,char *l=0);
  virtual ~TreeBrowser();
  TreeItem *root(){return my_root;}
};
#endif
