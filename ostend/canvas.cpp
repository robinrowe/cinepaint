/*
  (c) 2002 Yperman Hans
*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "util.h"
#include "draw.h"
#include "canvas.h"
#include "pen.h"
///////////////////////////////////////////////////////////////////////////////
struct Parameter{
  char *name,*value;
};

///////////////////////////////////////////////////////////////////////////////
Pixel hsv2pixel(int h,int s,int v,int a){
  if (s==0)
    return v*0x010101+(a<<ASHIFT);
  else{
    int i=int(h*6/255);
    double f=h*6.0/255.0-(float)i;
    int p1=v*(255-s)/255;
    int p2=int(v*(255-s*f)/255);
    int p3=int(v*(255-s*(1.0-f)));
    switch (i) {
    case 0:return (v<<RSHIFT)+(p3<<GSHIFT)+(p1<<BSHIFT)+(a<<ASHIFT);
    case 1:return (p2<<RSHIFT)+(v<<GSHIFT)+(p1<<BSHIFT)+(a<<ASHIFT);
    case 2:return (p1<<RSHIFT)+(v<<GSHIFT)+(p3<<BSHIFT)+(a<<ASHIFT);
    case 3:return (p1<<RSHIFT)+(p2<<GSHIFT)+(v<<BSHIFT)+(a<<ASHIFT);
    case 4:return (p3<<RSHIFT)+(p1<<GSHIFT)+(v<<BSHIFT)+(a<<ASHIFT);
    case 5:return (v<<RSHIFT)+(p1<<GSHIFT)+(p2<<BSHIFT)+(a<<ASHIFT);
    }
  }
  return 0x666; //only to shut up compiler
}

void pixel2hsv(int &h,int & s,int& v,int& a,Pixel p){
  int r=RDATA(p),g=GDATA(p),b=BDATA(p);
  a=ADATA(p);
  int maxv = r > g ? r : g; if (b > maxv) maxv = b;
  v = maxv;
  if (maxv>0) {
    int minv = r < g ? r : g; if (b < minv) minv = b;
    s =255-255*minv/maxv;
    if (maxv > minv) {
      if (maxv == r) {h = 255/6*(g-b)/(maxv-minv);h&=0xFF;}
      else if (maxv == g) h = 255/3+255/6*(b-r)/(maxv-minv);
      else h = 511/3+255/6*(r-g)/(maxv-minv);
    }else
      h=0;
  }
}
///////////////////////////////////////////////////////////////////////////////
Canvas::Canvas(int w,int h){
  //setup image
  changed=true;
  this->w=w;this->h=h;
  data=new uint32[w*h];
  for(int i=0;i<w*h;i++)
    data[i]=0xFFFFFF;

  //setup UNDO's
  undo_count=10;
  undo=new Undo*[undo_count];
  undo_index=0;
  for(int i=0;i<undo_count;i++)
    undo[i]=0;
  original_line=0;
  dirty_x1=w;dirty_y1=h;dirty_x2=dirty_y2=0;
  //parameters
  num_parameters=1;
  Parameter *p;
  parameters=p=new Parameter[1];
  p->name=strdup("Name");
  p->value=strdup("New Image");
}

Canvas::~Canvas(){
  ondelete.broadcast(this);
  if(original_line!=0)
    deactivate();
  //remove undo's
  for(int i=0;i<undo_count;i++)
    if(undo[i]!=0)
      delete undo[i];
  delete[] undo;

  //remove parameters
  Parameter *p=(Parameter *)parameters;
  for(int i=0;i<num_parameters;i++){
    free(p[i].name);
    free(p[i].value);
  }
  delete[] p;
  delete[] data;
}

/*
  Create a new image of size w*h, The old one is given back as an
  oldw*oldh array of pixels. get rid of it with delete[]. The new image
  is not filled with something usefull,or cleared, or something else fancy.
  Also, the contents of the shadow buffer disappear.
*/
Pixel *Canvas::new_image(int w,int h,Pixel *d){
  bool shadow_active=original_line!=0;
  if(shadow_active)
    deactivate();
  Pixel *old=data;
  this->w=w;this->h=h;
  data=d;
  if(data==0)
    data=new Pixel[w*h];

  if(shadow_active)
    activate();
  return old;
}

/*
  the image may be edited
*/
typedef Pixel* Pixel_ptr;
void Canvas::activate(){
  shadow_buffer=new uint8[w*h+1];
  memset(shadow_buffer,0,w*h+1);
  original_line=new Pixel_ptr[h+1];
  memset(original_line,0,(h+1)*sizeof(Pixel *));
}

/*
  the image will not be edited for now.  Flush some buffers.
*/
void Canvas::deactivate(){
  del_shadow();
  delete[] shadow_buffer;
  delete[] original_line;
  original_line=0;
}

/*
  add undo information on the undo stack. As every Tool should add
  undo information, this is a good place to mark an image as changed
*/

void Canvas::add(Undo *u){
  changed=true;
  int i=undo_index;
  if(undo[i]!=0)
    delete undo[i];
  undo[i]=u;
  undo_index=(i+1)%undo_count;
}

/*
  undo the last action
*/
void Canvas::undo_action(){
  undo_index=undo_index-1;
  if(undo_index<0) undo_index+=undo_count;
  
  if(undo[undo_index]!=0){
    undo[undo_index]->undo(this);
    delete undo[undo_index];
    undo[undo_index]=0;
  }else
    //nothing to undo
    undo_index=(undo_index+1)%undo_count;

}

/*
  mark part of the image as changed. It should be updated on screen
  and/or by a Paint
*/
void Canvas::dirty(int x1,int y1,int x2,int y2){
  CLIP(x1,y1,0,0,w-1,h-1);
  CLIP(x2,y2,0,0,w-1,h-1);
  CLIPUP(dirty_x1,dirty_y1,x1,y1);
  CLIPDOWN(dirty_x2,dirty_y2,x2,y2);
  for(int y=y1;y<=y2;y++)
    if(original_line[y]==0){
      original_line[y]=new Pixel[w];
      memcpy(original_line[y],&pixel(0,y),sizeof(Pixel)*w);
    }
  onpaint.broadcast(this);
}

/*
  Nothing dirty anymore. Called when drawn
*/
void Canvas::undirty(){
  dirty_x1=w;dirty_y1=h;dirty_x2=dirty_y2=0;
}

/*
  erase contents of the shadow buffer
*/
void Canvas::del_shadow(){
  for(int y=0;y<=h;y++)
    if(original_line[y]!=0)
      delete[] original_line[y];  
  memset(shadow_buffer,0,w*h+1);
  memset(original_line,0,(h+1)*sizeof(Pixel *));
}

/*
  place backup data of the shadowed pixels(and some others) back
  in the canvas. Not an undo, but handy if e.g. a line needs to be
  erased and redrawn
*/

void Canvas::undo_shadow(){
  for(int y=0;y<=h;y++){
    if(!line_changed(y))
      continue;
    dirty(0,y,width()-1,y);
    memcpy(&pixel(0,y),original_line[y],sizeof(Pixel)*w);
    delete[] original_line[y];
  }
  memset(shadow_buffer,0,w*h+1);
  memset(original_line,0,(h+1)*sizeof(Pixel *));

}

/*
  add mask[xx][yy] to the size*size points around (x,y) in the 
  shadowbuffer
*/
void Canvas::shadow_add_mask(int x,int y,uint8 *mask,int size){

  dirty(x-size,y-size,x+size,y+size);
  //make (x,y) the coordinates of the upper left, not of the center
  int size2=size>>1;
  x-=size2;y-=size2;

  //trivial rejection
  int w=width(),h=height();
  if(x>=w||y>=h||x+size<0||y+size<0)
    return;

  uint8 *mpointer=mask;
  uint8 *mend=mpointer+size*size;
  int yend=y+size;

  //clip above
  if(y<0){
    mpointer-=y*size;
    y=0;
  }
  //clip below
  if(yend>h){
    mend-=(yend-h)*size;
    yend=h;
  }
  //clip left
  int source_add_line=0;
  int target_add_line=w-size;
  int xlen=size;
  if(x<0){
    mpointer-=x;
    source_add_line=-x; //x is negative
    target_add_line-=x; //here too
    xlen+=x;            //and here too
    x=0;
  }
  //clip right
  if(x-w+xlen>=0){
    int delta=x-w+xlen;
    source_add_line+=delta;
    target_add_line+=delta;
    xlen-=delta;
  }

  uint8 *target=&shadow(x,y);
  while(mpointer<mend){
    uint8 *target_end=target+xlen;
    while(target!=target_end){
      if(*target<*mpointer){
	*target=*mpointer;
      }
      target++;
      mpointer++;
    }
    target+=target_add_line;
    mpointer+=source_add_line;
  }
}

static int param_compare(const void *p1,const void *p2){
  Parameter *param1=(Parameter *)p1;
  Parameter *param2=(Parameter *)p2;
  return strcmp(param1->name,param2->name);
}


/*
  This returns a read-only pointer to a parameter.
  WARNING: it stays only valid until the value of the parameter is changed.
  You can do param(name,param(name)) without running into trouble.
*/
const char *Canvas::param(const char *name){
  Parameter e;
  e.name=(char *)name;  
  void *match=bsearch(&e,parameters,num_parameters,sizeof(Parameter),param_compare);
  if(match==0)
    return 0;
  return ((Parameter *)match)->value;
}

void Canvas::param(const char *name,const char *value){
  //Warning: there should be at least 1 parameter in the list before this works
  //The constructor adds one, so that's all right
  Parameter *list=new Parameter[num_parameters+1];
  Parameter *old=(Parameter *)parameters;
  int i,n;

  //search new element position i, copy elementrs before it
  for(i=0;i<num_parameters;i++){
    n=strcmp(old[i].name,name);
    if(n<0){
      list[i].name=old[i].name;
      list[i].value=old[i].value;      
      continue;
    }else if(n==0){
      list[i].name=old[i].name;
      list[i].value=strdup(value);
      //this must be done last, or param("X",param("X")); doesnt work
      free(old[i].value);
      memcpy(list+i+1,old+i+1,(num_parameters-i-1)*sizeof(list[0]));
      break;
    }else{ 
      //add new element
      list[i].name=strdup(name);
      list[i].value=strdup(value);
      memcpy(list+i+1,old+i,(num_parameters-i)*sizeof(list[0]));
      num_parameters++;
      break;
    }
  }
  delete[] old;
  parameters=list;
}
/////Undo data/////////////////////////////////////////////////////////////////
Undo::Undo(){};
Undo::~Undo(){};
/////Image undo ///////////////////////////////////////////////////////////////
/*
  ImageUndo is a backup of the complete image. Undoing may change
  even width and height.
*/
void ImageUndo::undo(Canvas *c){
  delete[] c->new_image(w,h,data);
  c->dirty(0,0,w,h);
  data=0;
}

/*
  Undo the image to a w*h image with data 'data'. Data is now a
  part of the class, and should not be changed or deleted
*/
ImageUndo::ImageUndo(int w,int h,Pixel *data){
  this->len_=w*h*sizeof(Pixel);
  this->w=w;
  this->h=h;
  this->data=data;
}

ImageUndo::~ImageUndo(){
  if(data!=0)
    delete[] data;
}

/////Canvas shadow buffer system///////////////////////////////////////////////
/*
  Shadowundo backups the pixels marked in the shadowbuffer, thus these
  changed with the Pen
*/
typedef Row* Row_ptr;

ShadowUndo::ShadowUndo(const Canvas *c){
  w=c->width();
  h=c->height();
  row=new Row_ptr[h];
  memset(row,0,sizeof(Row*)*h);
  len_=sizeof(Row)*h;
  for(int y=0;y<h;y++){
    if(!c->line_changed(y))
      continue;
    Pixel *to=&c->pixel(0,y);
    Pixel *from=&c->original(0,y);    
    for(int x=0;x<w;){
      //find a changed pixel
      while(x<w&&to[x]==from[x])
	x++;
      if(x==w)
	break;
      //make a new Row structure with the changes
      int len=0;
      while(x+len<w&&to[x+len]!=from[x+len])
	len++;
      Row *r=new Row();
      r->start=x;
      r->len=len;
      r->data=new Pixel[len];
      memcpy(r->data,from+x,len*sizeof(Pixel));
      r->next=row[y];
      row[y]=r;
      len_+=sizeof(Row)+len*sizeof(Pixel);
      x+=len;
    }
  }
}

void ShadowUndo::undo(Canvas *c){
  if(c->width()!=w||c->height()!=h)
    stop("Internal error: undo data has wrong width or height");
  for(int y=0;y<h;y++){
    for(Row *r=row[y];r!=0;r=r->next)
      memcpy(&c->pixel(r->start,y),r->data,r->len*sizeof(Pixel));    
  }
}

ShadowUndo::~ShadowUndo(){
  for(int y=0;y<h;y++)
    for(Row *r=row[y];r!=0;){
      Row *r2=r->next;
      delete[] r->data;
      delete r;
      r=r2;
    }
  delete[] row;
}
