# Cinebrush/CMakeLists.txt
# Created by Robin Rowe 2023-03-17
# License Open Source GPL

cmake_minimum_required(VERSION 3.8)
set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

project(Cinebrush_Project)
message("Configuring ${CMAKE_PROJECT_NAME}...")

if(NOT WIN32 AND NOT APPLE)
	link_libraries(rt pthread)
endif(NOT WIN32 AND NOT APPLE)

option(SUPPRESS "Enable suppression of code warnings" true)
if(SUPPRESS)
	if(WIN32)
		add_definitions(-D_CRT_SECURE_NO_WARNINGS /wd4244 /wd4305 /wd4018 /wd26451 /wd6031 /wd4267 /wd4996)
	endif(WIN32)
endif(SUPPRESS)

option(UNISTD "Enable libunistd" true)
if(UNISTD)
	if(WIN32)
		set(LIBUNISTD_PATH "/code/github/libunistd")
		include_directories(${LIBUNISTD_PATH}/unistd)
		set (UNISTD_LINK_DIRECTORIES ${LIBUNISTD_PATH}/build/${CMAKE_GENERATOR_PLATFORM}/Release)
		link_directories(UNISTD_LINK_DIRECTORIES)
		link_libraries(libunistd)
		message(UNISTD_LINK_DIRECTORIES = ${UNISTD_LINK_DIRECTORIES})
#/c/code/github/libunistd/build/Win32/Debug/libunistd.lib
	endif(WIN32)
endif(UNISTD)

option(FLTK "Enable FLTK" true)
if(FLTK)
	include(../DownloadProject/DownloadProject.cmake)
	include(../cmake/FLTK.cmake)
	message("FLTK include_directories = ${FLTK_PATH_INC}")
	include_directories(${FLTK_PATH_INC})
endif(FLTK)

project(Cinebrush_Project)

if(WIN32)
	set(EXTRA_LIBS Imm32)
	add_definitions(-D__MINGW32__)
endif(WIN32)


file(STRINGS sources.cmake SOURCES)
add_executable(cinebrush ${SOURCES})

target_link_libraries(cinebrush 	
	${FLTK_IMAGE_LIBRARY} 
	${FLTK_LIBRARY} 
	${EXTRA_LIBS}
)
