#ifndef _X_FILE_H_
#define _X_FILE_H_
#include "canvas.h"
//#include "fstreams.h"

//Given a stream , an imagereader can 
// *) return a Canvas or
// *) return 0. Then the errormessage of the given inputstream notes the problem
typedef Canvas *(ImageReader)(class InputStream *);
typedef void (ImageWriter)(class OutputStream *,class Canvas *);
//struct FileExtension;

struct Extension_Struct{
  const char *extension;
  ImageReader *loader;
  ImageWriter *saver;
};

class File {
  static const char *extension(const char *name,const char *type);
 public:
  static Canvas *open(const char *name,const char *type,char **errmsg);
  static void save(Canvas *s,const char *name,const char *type);
//  static const FileExtension *get_types();
  static char **extensions();
// rsr:
	static Extension_Struct* LookupExtStruct(const char* ext,Extension_Struct* extension_table)
	{	Extension_Struct * ptr=extension_table;
		while(ptr->extension)
		{	if(!strcmp(ext,ptr->extension))
			{	return ptr;
			}
			ptr++;
		}
		return 0;
	}
};
#endif
