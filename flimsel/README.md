#README.md

Robin Rowe 2020-06-17

A minimalistic digitial image browser based on the fltk toolkit
flimsel is a minimalistic digitial image browser based on the fltk toolkit. It displays thumbnails of images given on the command line. The project is intended to be used in digital image processing shell scripts see script in the examples directory. 

flimsel uses the very fast epeg library by The Rasterman (Carsten Haitzler) for JPEG thumbnail generation but falls back to fltk native methods for other image formats. flimsel outputs keybord hotkeys and file names of selected images to stdout. Images whose filenames are written to stdin will be reloaded. 

The flimsel.sh script in the examples directory uses this to implement basic image manipulations on selected images: Ctrl+L rotates the selected images 90 leftCtrl+R rotates the selected images 90 rightCtrl+D moves selected images to the /tmp/flimsel_deleted directory Ctrl+G opens the selected images in gimpCtrl+V opens the selected images in feh 

