//
// Copyright 2006-2012 Johannes Hofmann <Johannes.Hofmann@gmx.de>
//
// This software may be used and distributed according to the terms
// of the GNU General Public License, incorporated herein by reference.

#include <stdio.h>
#include <string.h>
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Shared_Image.H>
#include "Fl_Epeg_Image.H"
#include "Slide.H"

int Slide::max_thumb_w;
int Slide::max_thumb_h;

Slide::Slide(const char *name, int my_x, int my_y, int my_w, int my_h) :
	Fl_Toggle_Button(my_x, my_y, my_w, my_h) {

	file = name;
	box(FL_NO_BOX);
}

Slide::~Slide() {
	file.clear();
}

int
Slide::load() {
	uchar header[64];
	size_t hdrlen;
	FILE *fp;
	Fl_Image *im;

	if ((fp = fl_fopen(file.c_str(), "rb"))) {
		hdrlen = fread(header, 1, sizeof(header), fp);
		fclose(fp);
	} else {
		return 1;
	}

	im = epeg_thumb_check(file.c_str(), header, (int) hdrlen);
	if (!im)
		im = fallback_thumb_check(file.c_str(), header, (int) hdrlen);
	if (!im)
		return 1;

	image(im);
	
	return 0;
}

int
Slide::reload() {
	if (file.c_str() && load() == 0) {
	 	redraw();
		return 0;
	} else {
		return 1;
	}
}

const char *
Slide::filename() {
	return file.c_str();
}

Fl_Image *
Slide::epeg_thumb_check(const char *name, uchar *header, int hdrlen) {
    if (hdrlen >= 4 &&
        memcmp(header, "\377\330\377", 3) == 0 &&
        header[3] >= 0xe0 && header[3] <= 0xef) {
        return new Fl_Epeg_Image(name, Slide::max_thumb_w, Slide::max_thumb_h);
    } else {
        return NULL;
    }
}

// work around protected constructor of Fl_Shared_Image
class AnyImage : public Fl_Shared_Image {
public:
	AnyImage(const char *n, Fl_Image *img = 0) : Fl_Shared_Image(n, img) {};
	~AnyImage() {};
};

Fl_Image *
Slide::fallback_thumb_check(const char *name, uchar *header, int hdrlen) {
	int im_w, im_h, thumb_w, thumb_h;
	AnyImage *tmp = new AnyImage(name);

	if (!tmp)
		return NULL;

	im_w = tmp->w();
	im_h = tmp->h();

	if (im_w <= 0 || im_h <= 0)
		return NULL;

	if (im_w * Slide::max_thumb_h > im_h * Slide::max_thumb_w) {
		thumb_w = Slide::max_thumb_w;
		thumb_h = thumb_w * im_h / im_w;
	} else {
		thumb_h = Slide::max_thumb_h;
		thumb_w = thumb_h * im_w / im_h;
	}

	Fl_Image *ret = tmp->copy(thumb_w, thumb_h);
	delete tmp;

	return ret;
}

int
Slide::handle(int event) {
	if (event == FL_PUSH) {
		if (Fl::event_button() != 1) {
			printf("Mouse%d\t%s\n", Fl::event_button(), file.c_str());
			fflush(stdout);
			return 1;
		}
	} else if (event == FL_DRAG) {
		if (Fl::event_button() != 1)
			return 1;
	}

	return Fl_Toggle_Button::handle(event);
}
	
void 
Slide::init(int _w, int _h) {
	Slide::max_thumb_w = _w;
	Slide::max_thumb_h = _h;
	fl_register_images();
}

void 
Slide::draw_shadow(Fl_Color c, int x, int y, int w, int h) {
	int n = 4;
	for (int i = n; i > 0; i--) {
		fl_color(fl_color_average(c, FL_BLACK, float(0.5 + 0.1 * i)));
		fl_rectf(x - i + 10, y - i + 12, w -n + 2 * i, h - n + 2 * i);
	}
}

void 
Slide::draw() {
	Fl_Color c = value() ? fl_darker(color()) : color();

	fl_color(c);
	fl_rectf(x(), y(), w(), h());

	if (image()) {
		draw_shadow(c,
                    x() + (w() - image()->w()) / 2,
                    y() + (h() - image()->h()) / 2,
                    image()->w(), image()->h());
	}

	Fl_Toggle_Button::draw();
}
