/*
 * This file is taken from epeg library which was part of the enlightenment
 * project (http://www.enlightenment.org).
 * It is included here as epeg is no longer actively maintained.
 */

#include <algorithm>
#include <jerror.h>
#include <utility>
#include "Epeg.h"

#undef min

static const JOCTET fake_EOI[2] = { 0xFF, JPEG_EOI };

/**
 * Open a JPEG image by filename.
 * @param file The file path to open.
 * @return A handle to the opened JPEG file, with the header decoded.
 *
 * This function opens the file indicated by the @p file parameter, and
 * attempts to decode it as a jpeg file. If this failes, NULL is returned.
 * Otherwise a valid handle to an open JPEG file is returned that can be used
 * by other Epeg calls.
 *
 * The @p file must be a pointer to a valid C string, NUL (0 byte) terminated
 * thats is a relative or absolute file path. If not results are not
 * determined.
 *
 * See also: memory_open(), close()
 */
void Epeg::file_open(const char *file)
{	in.file = strdup(file);
	if(!in.file)
	{	return;
	}
	in.f = fopen(in.file, "rb");
	if(!in.f)
	{	close();
		return;
	}
	fstat(fileno(in.f), &(stat_info));
	out.quality = 75;
	open_header();
	return;
}

/**
 * Open a JPEG image stored in memory.
 * @param data A pointer to the memory containing the JPEG data.
 * @param size The size of the memory segment containing the JPEG.
 * @return A handle to the opened JPEG, with the header decoded.
 *
 * This function opens a JPEG file that is stored in memory pointed to by
 * @p data, and that is @p size bytes in size. If successful a valid handle
 * is returned, or on failure NULL is returned.
 *
 * See also: Epeg::file_open(), Epeg::close()
 */
void Epeg::memory_open(unsigned char *data, int size)
{	out.quality = 75;
	in.mem.data = (JOCTET *)data;
	in.mem.size = size;
	in.f = NULL;
	in.w = 0;
	in.h = 0;
	open_header();
	return;
}

/**
 * Return the original JPEG pixel size.
 * @param im A handle to an opened Epeg image.
 * @param w A pointer to the width value in pixels to be filled in.
 * @param h A pointer to the height value in pixels to be filled in.
 *
 * Returns the image size in pixels.
 *
 */
void Epeg::size_get(int *w, int *h)
{	if(w) { *w = in.w; }
	if(h) { *h = in.h; }
}

/**
 * Return the original JPEG pixel color space.
 * @param im A handle to an opened Epeg image.
 * @param space A pointer to the color space value to be filled in.
 *
 * Returns the image color space.
 *
 */
void
Epeg::colorspace_get(int *space)
{	if(space) { *space = color_space; }
}

/**
 * Set the size of the image to decode in pixels.
 * @param im A handle to an opened Epeg image.
 * @param w The width of the image to decode at, in pixels.
 * @param h The height of the image to decode at, in pixels.
 *
 * Sets the size at which to deocode the JPEG image, giving an optimised load
 * that only decodes the pixels needed.
 *
 */
void
Epeg::decode_size_set(int w, int h)
{	if(pixels) { return; }
	if(w < 1) { w = 1; }
	else if(w > in.w) { w = in.w; }
	if(h < 1) { h = 1; }
	else if(h > in.h) { h = in.h; }
	out.box.w = w;
	out.box.h = h;
	out.box.x = 0;
	out.box.y = 0;
}

void Epeg::decode_bounds_set(int x, int y, int w, int h)
{	if(pixels) { return; }
	if(w < 1) { w = 1; }
	else if(w > in.w) { w = in.w; }
	if(h < 1) { h = 1; }
	else if(h > in.h) { h = in.h; }
	out.box.w = w;
	out.box.h = h;
	if(x < 0) { x = 0; }
	if(y < 0) { y = 0; }
	out.box.x = x;
	out.box.y = y;
}

/**
 * Set the colorspace in which to decode the image.
 * @param im A handle to an opened Epeg image.
 * @param colorspace The colorspace to decode the image in.
 *
 * This sets the colorspace to decode the image in. The default is EPEG_YUV8,
 * as this is normally the native colorspace of a JPEG file, avoiding any
 * colorspace conversions for a faster load and/or save.
 */
void Epeg::decode_colorspace_set(Epeg_Colorspace colorspace)
{	if(pixels)
	{	return; 
	}
	if((colorspace < EPEG_GRAY8) || (colorspace > EPEG_CMYK)) 
	{	return; 
	}
	color_space = colorspace;
}

/**
 * Get a segment of decoded pixels from an image.
 * @param im A handle to an opened Epeg image.
 * @param x Rectangle X.
 * @param y Rectangle Y.
 * @param w Rectangle width.
 * @param h Rectangle height.
 * @return Pointer to the top left of the requested pixel block.
 *
 * Return image pixels in the decoded format from the specified location
 * rectangle bounded with the box @p x, @p y @p w X @p y. The pixel block is
 * packed with no row padding, and it organsied from top-left to bottom right,
 * row by row. You must free the pixel block using Epeg::pixels_free() before
 * you close the image handle, and assume the pixels to be read-only memory.
 *
 * On success the pointer is returned, on failure, NULL is returned. Failure
 * may be because the rectangle is out of the bounds of the image, memory
 * allocations failed or the image data cannot be decoded.
 *
 */


const Pix Epeg::pixels_get(int x, int y, int w, int h)
{	int xx, yy, ww, hh, bpp, ox, oy, ow, oh, iw, ih;
	if(!pixels)
	{	if(decode() != 0) { return Pix(); }
	}
	if(!pixels) { return Pix(); }
	if((out.box.w < 1) || (out.box.h < 1)) { return Pix(); }
	if(scale() != 0) { return Pix(); }
	bpp = in.jinfo.output_components;
	iw = out.box.w;
	ih = out.box.h;
	ow = w;
	oh = h;
	ox = 0;
	oy = 0;
	if((x + ow) > iw) { ow = iw - x; }
	if((y + oh) > ih) { oh = ih - y; }
	if(ow < 1) { return Pix(); }
	if(oh < 1) { return Pix(); }
	if(x < 0)
	{	ow += x;
		ox = -x;
	}
	if(y < 0)
	{	oh += y;
		oy = -y;
	}
	if(ow < 1) { return Pix(); }
	if(oh < 1) { return Pix(); }
	ww = x + ox + ow;
	hh = y + oy + oh;
	if(color_space == EPEG_GRAY8)
	{	unsigned char *p;
		Pix pix(w,h,1);
		for(yy = y + oy; yy < hh; yy++)
		{	unsigned char *s;
			s = lines[yy] + ((x + ox) * bpp);
			p = pix + ((((yy - y) * w) + ox));
			for(xx = x + ox; xx < ww; xx++)
			{	p[0] = s[0];
				p++;
				s += bpp;
			}
		}
		return pix;
	}
	else if(color_space == EPEG_YUV8)
	{	unsigned char *p;
		Pix pix(w,h,3);
		for(yy = y + oy; yy < hh; yy++)
		{	unsigned char *s;
			s = lines[yy] + ((x + ox) * bpp);
			p = pix + ((((yy - y) * w) + ox) * 3);
			for(xx = x + ox; xx < ww; xx++)
			{	p[0] = s[0];
				p[1] = s[1];
				p[2] = s[2];
				p += 3;
				s += bpp;
			}
		}
		return pix;
	}
	else if(color_space == EPEG_RGB8)
	{	unsigned char *p;
		Pix pix(w,h,3);
		for(yy = y + oy; yy < hh; yy++)
		{	unsigned char *s;
			s = lines[yy] + ((x + ox) * bpp);
			p = pix + ((((yy - y) * w) + ox) * 3);
			for(xx = x + ox; xx < ww; xx++)
			{	p[0] = s[0];
				p[1] = s[1];
				p[2] = s[2];
				p += 3;
				s += bpp;
			}
		}
		return pix;
	}
	else if(color_space == EPEG_BGR8)
	{	unsigned char *p;
		Pix pix(w,h,3);
		for(yy = y + oy; yy < hh; yy++)
		{	unsigned char *s;
			s = lines[yy] + ((x + ox) * bpp);
			p = pix + ((((yy - y) * w) + ox) * 3);
			for(xx = x + ox; xx < ww; xx++)
			{	p[0] = s[2];
				p[1] = s[1];
				p[2] = s[0];
				p += 3;
				s += bpp;
			}
		}
		return pix;
	}
	else if(color_space == EPEG_RGBA8)
	{	unsigned char *p;
		Pix pix(w,h,4);
		for(yy = y + oy; yy < hh; yy++)
		{	unsigned char *s;
			s = lines[yy] + ((x + ox) * bpp);
			p = pix + ((((yy - y) * w) + ox) * 4);
			for(xx = x + ox; xx < ww; xx++)
			{	p[0] = s[0];
				p[1] = s[1];
				p[2] = s[2];
				p[3] = 0xff;
				p += 4;
				s += bpp;
			}
		}
		return pix;
	}
	else if(color_space == EPEG_BGRA8)
	{	unsigned char *p;
		Pix pix(w,h,4);
		for(yy = y + oy; yy < hh; yy++)
		{	unsigned char *s;
			s = lines[yy] + ((x + ox) * bpp);
			p = pix + ((((yy - y) * w) + ox) * 4);
			for(xx = x + ox; xx < ww; xx++)
			{	p[0] = 0xff;
				p[1] = s[2];
				p[2] = s[1];
				p[3] = s[0];
				p += 4;
				s += bpp;
			}
		}
		return pix;
	}
	else if(color_space == EPEG_ARGB32)
	{	unsigned char *p;
		Pix pix(w,h,4);
		for(yy = y + oy; yy < hh; yy++)
		{	unsigned char *s;
			s = lines[yy] + ((x + ox) * bpp);
			p = pix + ((((yy - y) * w) + ox));
			for(xx = x + ox; xx < ww; xx++)
			{	p[0] = 0xff000000 | (s[0] << 16) | (s[1] << 8) | (s[2]);
				p++;
				s += bpp;
			}
		}
		return pix;
	}
	else if(color_space == EPEG_CMYK)
	{	unsigned char *p;
		Pix pix(w,h,4);
		for(yy = y + oy; yy < hh; yy++)
		{	unsigned char *s;
			s = lines[yy] + ((x + ox) * bpp);
			p = pix + ((((yy - y) * w) + ox) * 4);
			for(xx = x + ox; xx < ww; xx++)
			{	p[0] = s[0];
				p[1] = s[1];
				p[2] = s[2];
				p[3] = 0xff;
				p += 4;
				s += bpp;
			}
		}
		return pix;
	}
	return Pix();
}

/**
 * Get a segment of decoded pixels from an image.
 * @param im A handle to an opened Epeg image.
 * @param x Rectangle X.
 * @param y Rectangle Y.
 * @param w Rectangle width.
 * @param h Rectangle height.
 * @return Pointer to the top left of the requested pixel block.
 *
 * Return image pixels in the decoded format from the specified location
 * rectangle bounded with the box @p x, @p y @p w X @p y. The pixel block is
 * packed with no row padding, and it organsied from top-left to bottom right,
 * row by row. You must free the pixel block using Epeg::pixels_free() before
 * you close the image handle, and assume the pixels to be read-only memory.
 *
 * On success the pointer is returned, on failure, NULL is returned. Failure
 * may be because the rectangle is out of the bounds of the image, memory
 * allocations failed or the image data cannot be decoded.
 *
 */
const Pix
Epeg::pixels_get_as_RGB8(int x, int y, int w, int h)
{	int xx, yy, ww, hh, bpp, ox, oy, ow, oh, iw, ih;
	if(!pixels)
	{	if(decode() != 0) { return Pix(); }
	}
	if(!pixels) { return Pix(); }
	if((out.box.w < 1) || (out.box.h < 1)) { return Pix(); }
	bpp = in.jinfo.output_components;
	iw = out.box.w;
	ih = out.box.h;
	ow = w;
	oh = h;
	ox = 0;
	oy = 0;
	if((x + ow) > iw) { ow = iw - x; }
	if((y + oh) > ih) { oh = ih - y; }
	if(ow < 1) { return Pix(); }
	if(oh < 1) { return Pix(); }
	if(x < 0)
	{	ow += x;
		ox = -x;
	}
	if(y < 0)
	{	oh += y;
		oy = -y;
	}
	if(ow < 1) { return Pix(); }
	if(oh < 1) { return Pix(); }
	ww = x + ox + ow;
	hh = y + oy + oh;
	if(color_space == EPEG_GRAY8)
	{	unsigned char *p;
		Pix pix(w,h,3);
		if(!pix) { return Pix(); }
		for(yy = y + oy; yy < hh; yy++)
		{	unsigned char *s;
			s = lines[yy] + ((x + ox) * bpp);
			p = pix + ((((yy - y) * w) + ox) * 3);
			for(xx = x + ox; xx < ww; xx++)
			{	p[0] = s[0];
				p[1] = s[0];
				p[2] = s[0];
				p += 3;
				s += bpp;
			}
		}
		return pix;
	}
	if(color_space == EPEG_RGB8)
	{	unsigned char *p;
		Pix pix(w,h,3);
		if(!pix) { return Pix(); }
		for(yy = y + oy; yy < hh; yy++)
		{	unsigned char *s;
			s = lines[yy] + ((x + ox) * bpp);
			p = pix + ((((yy - y) * w) + ox) * 3);
			for(xx = x + ox; xx < ww; xx++)
			{	p[0] = s[0];
				p[1] = s[1];
				p[2] = s[2];
				p += 3;
				s += bpp;
			}
		}
		return pix;
	}
	if(color_space == EPEG_CMYK)
	{	unsigned char *p;
		Pix pix(w,h,3);
		if(!pix) { return Pix(); }
		for(yy = y + oy; yy < hh; yy++)
		{	unsigned char *s;
			s = lines[yy] + ((x + ox) * bpp);
			p = pix + ((((yy - y) * w) + ox) * 3);
			for(xx = x + ox; xx < ww; xx++)
			{	p[0] = (unsigned char)(std::min(255, (s[0] * s[3]) / 255));
				p[1] = (unsigned char)(std::min(255, (s[1] * s[3]) / 255));
				p[2] = (unsigned char)(std::min(255, (s[2] * s[3]) / 255));
				p += 3;
				s += bpp;
			}
		}
		return pix;
	}
	return Pix();
}

/**
 * Get the image comment field as a string.
 * @param im A handle to an opened Epeg image.
 * @return A pointer to the loaded image comments.
 *
 * This function returns the comment field as a string (NUL byte terminated)
 * of the loaded image @p im, if there is a comment, or NULL if no comment is
 * saved with the image. Consider the string returned to be read-only.
 *
 */
const char *
Epeg::comment_get()
{	return &in.comment[0];
}

/**
 * Get thumbnail comments of loaded image.
 * @param im A handle to an opened Epeg image.
 * @param info Pointer to a thumbnail info struct to be filled in.
 *
 * This function retrieves thumbnail comments written by Epeg to any saved
 * JPEG files. If no thumbnail comments were saved, the fields will be 0 in
 * the @p info struct on return.
 *
 */
void
Epeg::thumbnail_comments_get(Epeg_Thumbnail_Info *info)
{	if(!info) { return; }
	info->uri = in.thumb_info.uri;
	info->mtime = in.thumb_info.mtime;
	info->w = in.thumb_info.w;
	info->h = in.thumb_info.h;
	info->mimetype = in.thumb_info.mime;
}

/**
 * Set the comment field of the image for saving.
 * @param im A handle to an opened Epeg image.
 * @param comment The comment to set.
 *
 * Set the comment for the image file for when it gets saved. This is a NUL
 * byte terminated C string. If @p comment is NULL the output file will have
 * no comment field.
 *
 * The default comment will be any comment loaded from the input file.
 *
 */
void
Epeg::comment_set(const char *comment)
{	if(out.comment) { free(out.comment); }
	if(!comment) { out.comment = NULL; }
	else { out.comment = strdup(comment); }
}

/**
 * Set the encoding quality of the saved image.
 * @param im A handle to an opened Epeg image.
 * @param quality The quality of encoding from 0 to 100.
 *
 * Set the quality of the output encoded image. Values from 0 to 100
 * inclusive are valid, with 100 being the maximum quality, and 0 being the
 * minimum. If the quality is set equal to or above 90%, the output U and V
 * color planes are encoded at 1:1 with the Y plane.
 *
 * The default quality is 75.
 *
 */
void
Epeg::quality_set(int quality)
{	if(quality < 0) { quality = 0; }
	else if(quality > 100) { quality = 100; }
	out.quality = quality;
}

/**
 * Enable thumbnail comments in saved image.
 * @param im A handle to an opened Epeg image.
 * @param onoff A boolean on and off enabling flag.
 *
 * if @p onoff is 1, the output file will have thumbnail comments added to
 * it, and if it is 0, it will not. The default is 0.
 *
 */
void
Epeg::thumbnail_comments_enable(int onoff)
{	out.thumbnail_info = onoff;
}

/**
 * Set the output file path for the image when saved.
 * @param im A handle to an opened Epeg image.
 * @param file The path to the output file.
 *
 * This sets the output file path name (either a full or relative path name)
 * to where the file will be written when saved. @p file must be a NUL
 * terminated C string conatining the path to the file to be saved to. If it is
 * NULL, the image will not be saved to a file when calling Epeg::encode().
 */
void
Epeg::file_output_set(const char *file)
{	if(out.file) { free(out.file); }
	if(!file) { out.file = NULL; }
	else { out.file = strdup(file); }
}

/**
 * Set the output file to be a block of allocated memory.
 * @param im A handle to an opened Epeg image.
 * @param data A pointer to a pointer to a memory block.
 * @param size A pointer to a counter of the size of the memory block.
 *
 * This sets the output encoding of the image when saved to be allocated
 * memory. After Epeg::close() is called the pointer pointed to by @p data
 * and the integer pointed to by @p size will contain the pointer to the
 * memory block and its size in bytes, respecitvely. The memory block can be
 * freed with the free() function call. If the save fails the pointer to the
 * memory block will be unaffected, as will the size.
 *
 */
void
Epeg::memory_output_set(unsigned char **data, int *size)
{	out.mem.data = *data;
	out.mem.size = *size;
	out.file = NULL;
}

/**
 * This saves the image to its specified destination.
 * @param im A handle to an opened Epeg image.
 *
 * This saves the image @p im to its destination specified by
 * Epeg::file_output_set() or Epeg::memory_output_set(). The image will be
 * encoded at the deoded pixel size, using the quality, comment and thumbnail
 * comment settings set on the image.
 *
 * retval 1 - error scale
 * 2 - error encode
 * 3 - error decode
 * 4 - error decode ( setjmp )
 */
int
Epeg::encode()
{	int ret;
	if((ret = decode()) != 0)
	{ return (ret == 2 ? 4 : 3); }
	if(scale() != 0)
	{ return 1; }
	if(encode() != 0)
	{ return 2; }
	return 0;
}

/**
 * FIXME: Document this
 * @param im A handle to an opened Epeg image.
 *
 * FIXME: Document this.
 */
int
Epeg::trim()
{	if(decode_for_trim() != 0)
	{ return 1; }
	if(trim() != 0)
	{ return 1; }
	if(encode() != 0)
	{ return 1; }
	return 0;
}

/**
 * Close an image handle.
 * @param im A handle to an opened Epeg image.
 *
 * This closes an opened image handle and frees all memory associated with it.
 * It does not free encoded data generated by Epeg::memory_output_set() followed
 * by Epeg::encode() nor does it guarantee to free any data recieved by
 * Epeg::pixels_get(). Once an image handle is closed consider it invalid.
 */
void
Epeg::close()
{
	puts("to-do");
#if 0
	if(!im) { return; }
	if(pixels) { free(pixels); }
	if(lines) { free(lines); }
	if(in.file) { free(in.file); }
	if(!in.file) { free(in.jinfo.src); }
	if(in.f || in.mem.data) { jpeg_destroy_decompress(&(in.jinfo)); }
	if(in.f) { fclose(in.f); }
	if(in.comment) { free(in.comment); }
	if(in.thumb_info.uri) { free(in.thumb_info.uri); }
	if(in.thumb_info.mime) { free(in.thumb_info.mime); }
	if(out.file) { free(out.file); }
	if(!out.file) { free(out.jinfo.dest); }
	if(out.f || in.mem.data) { jpeg_destroy_compress(&(out.jinfo)); }
	if(out.f) { fclose(out.f); }
	if(out.comment) { free(out.comment); }
	free(im);
#endif
}

bool Epeg::open_header()
{	struct jpeg_marker_struct *m;
	struct jpeg_source_mgr *src_mgr = NULL;
	in.jinfo.err = jpeg_std_error(&(jerr.pub));
	jerr.pub.error_exit = fatal_error_handler;
#ifdef NOWARNINGS
	jerr.pub.emit_message = _emit_message;
	jerr.pub.output_message = _output_message;
	jerr.pub.format_message = _format_message;
#endif
	if(setjmp(jerr.setjmp_buffer))
	{	error:
		close();
		return false;
	}
	jpeg_create_decompress(&(in.jinfo));
	jpeg_save_markers(&(in.jinfo), JPEG_APP0 + 7, 1024);
	jpeg_save_markers(&(in.jinfo), JPEG_COM, 65535);
	if(in.f != NULL)
	{	jpeg_stdio_src(&(in.jinfo), in.f);
	}
	else
	{	/* Setup RAM source manager. */
		src_mgr = new jpeg_source_mgr;
		src_mgr->init_source = _jpeg_init_source;
		src_mgr->fill_input_buffer = _jpeg_fill_input_buffer;
		src_mgr->skip_input_data = _jpeg_skip_input_data;
		src_mgr->resync_to_restart = jpeg_resync_to_restart;
		src_mgr->term_source = _jpeg_term_source;
		src_mgr->bytes_in_buffer = in.mem.size;
		src_mgr->next_input_byte = in.mem.data;
		in.jinfo.src = src_mgr;
	}
	jpeg_read_header(&(in.jinfo), TRUE);
	in.w = in.jinfo.image_width;
	in.h = in.jinfo.image_height;
	if(in.w < 1) { goto error; }
	if(in.h < 1) { goto error; }
	out.box.w = in.w;
	out.box.h = in.h;
	color_space = ((in.color_space = in.jinfo.out_color_space) == JCS_GRAYSCALE) ? EPEG_GRAY8 : EPEG_RGB8;
	if(in.color_space == JCS_CMYK) { color_space = EPEG_CMYK; }
	for(m = in.jinfo.marker_list; m; m = m->next)
	{	if(m->marker == JPEG_COM)
		{	in.comment.resize(m->data_length + 1);
			memcpy(&in.comment[0], m->data, m->data_length);
			in.comment[m->data_length] = 0;
			continue;
		}
		if(m->marker != (JPEG_APP0 + 7))
		{	continue;
		}
		if((m->data_length > 7) && (!strncmp((char *)m->data, "Thumb::", 7)))
		{	std::vector<char> v;
			v.resize(m->data_length+1);
			char* p = &v[0];
			memcpy(p, m->data, m->data_length);
			p[m->data_length] = 0;
			char* p2 = strchr(&p[0], '\n');
			if(!p2)
			{	continue;
			}
			p2[0] = 0;
			if(!strcmp(p, "Thumb::URI"))
			{	in.thumb_info.uri = strdup(p2 + 1);
				continue;
			}
			if(!strcmp(p, "Thumb::MTime"))
			{	sscanf(p2 + 1, "%llu", &(in.thumb_info.mtime));
				continue;
			}
			if(!strcmp(p, "Thumb::Image::Width"))
			{	in.thumb_info.w = atoi(p2 + 1);
				continue;
			}
			if(!strcmp(p, "Thumb::Image::Height"))
			{	in.thumb_info.h = atoi(p2 + 1);
				continue;
			}
			if(!strcmp(p, "Thumb::Mimetype"))
			{	in.thumb_info.mime = strdup(p2 + 1);
				continue;
			}
		}
	}
}

/**
 retval 1 - malloc or other
 2 - setjmp error
*/
int Epeg::decode()
{	int scale, scalew, scaleh, y;
	JDIMENSION old_output_scanline = 1;
	if(pixels) 
	{	return 1; 
	}
	if((out.box.w < 1) || (out.box.h < 1)) { return 1; }
	scalew = in.w / out.box.w;
	scaleh = in.h / out.box.h;
	scale = scalew;
	if(scaleh < scalew) { scale = scaleh; }
	if(scale > 8) 
	{	scale = 8; 
	}
	else if(scale < 1) 
	{	scale = 1;
	}
	in.jinfo.scale_num = 1;
	in.jinfo.scale_denom = scale;
	in.jinfo.do_fancy_upsampling = FALSE;
	in.jinfo.do_block_smoothing = FALSE;
	in.jinfo.dct_method = JDCT_IFAST;
	switch(color_space)
	{	case EPEG_GRAY8:
			in.jinfo.out_color_space = JCS_GRAYSCALE;
			in.jinfo.output_components = 1;
			break;
		case EPEG_YUV8:
			in.jinfo.out_color_space = JCS_YCbCr;
			break;
		case EPEG_RGB8:
		case EPEG_BGR8:
		case EPEG_RGBA8:
		case EPEG_BGRA8:
		case EPEG_ARGB32:
			in.jinfo.out_color_space = JCS_RGB;
			break;
		case EPEG_CMYK:
			in.jinfo.out_color_space = JCS_CMYK;
			in.jinfo.output_components = 4;
			break;
		default:
			break;
	}
	out.jinfo.err = jpeg_std_error(&(jerr.pub));
	jerr.pub.error_exit	= fatal_error_handler;
#ifdef NOWARNINGS
	jerr.pub.emit_message = _emit_message;
	jerr.pub.output_message = _output_message;
	jerr.pub.format_message = _format_message;
#endif
	if(setjmp(jerr.setjmp_buffer))
	{ return 2; }
	jpeg_calc_output_dimensions(&(in.jinfo));
	pixels = malloc(in.jinfo.output_width * in.jinfo.output_height * in.jinfo.output_components);
	if(!pixels) { return 1; }
	lines = malloc(in.jinfo.output_height * sizeof(char *));
	if(!lines)
	{	free(pixels);
		pixels = NULL;
		return 1;
	}
	jpeg_start_decompress(&(in.jinfo));
	for(y = 0; y < in.jinfo.output_height; y++)
	{ lines[y] = pixels + (y * in.jinfo.output_components * in.jinfo.output_width); }
	while(in.jinfo.output_scanline < in.jinfo.output_height)
	{	if(old_output_scanline == in.jinfo.output_scanline)
		{	jpeg_abort_decompress(&(in.jinfo));
			return 1;
		}
		old_output_scanline = in.jinfo.output_scanline;
		jpeg_read_scanlines(&(in.jinfo),
		 &(lines[in.jinfo.output_scanline]),
		 in.jinfo.rec_outbuf_height);
	}
	jpeg_finish_decompress(&(in.jinfo));
	return 0;
}

int
Epeg::scale()
{	unsigned char *dst, *row, *src;
	int x, y, w, h, i;
	if((in.w == out.box.w) && (in.h == out.box.h)) { return 0; }
	if(scaled) { return 0; }
	if((out.box.w < 1) || (out.box.h < 1)) { return 0; }
	scaled = 1;
	w = out.box.w;
	h = out.box.h;
	for(y = 0; y < h; y++)
	{	row = pixels + (((y * in.jinfo.output_height) / h) * in.jinfo.output_components * in.jinfo.output_width);
		dst = pixels + (y * in.jinfo.output_components * in.jinfo.output_width);
		for(x = 0; x < out.box.w; x++)
		{	src = row + (((x * in.jinfo.output_width) / w) * in.jinfo.output_components);
			for(i = 0; i < in.jinfo.output_components; i++)
			{ dst[i] = src[i]; }
			dst += in.jinfo.output_components;
		}
	}
	return 0;
}

int
Epeg::decode_for_trim()
{	int		y;
	if(pixels) { return 1; }
	in.jinfo.scale_num = 1;
	in.jinfo.scale_denom = 1;
	in.jinfo.do_fancy_upsampling = FALSE;
	in.jinfo.do_block_smoothing = FALSE;
	in.jinfo.dct_method = JDCT_ISLOW;
	switch(color_space)
	{	case EPEG_GRAY8:
			in.jinfo.out_color_space = JCS_GRAYSCALE;
			in.jinfo.output_components = 1;
			break;
		case EPEG_YUV8:
			in.jinfo.out_color_space = JCS_YCbCr;
			break;
		case EPEG_RGB8:
		case EPEG_BGR8:
		case EPEG_RGBA8:
		case EPEG_BGRA8:
		case EPEG_ARGB32:
			in.jinfo.out_color_space = JCS_RGB;
			break;
		case EPEG_CMYK:
			in.jinfo.out_color_space = JCS_CMYK;
			in.jinfo.output_components = 4;
			break;
		default:
			break;
	}
	out.jinfo.err = jpeg_std_error(&(jerr.pub));
	jerr.pub.error_exit = fatal_error_handler;
#ifdef NOWARNINGS
	jerr.pub.emit_message = _emit_message;
	jerr.pub.output_message = _output_message;
	jerr.pub.format_message = _format_message;
#endif
	if(setjmp(jerr.setjmp_buffer))
	{ return 1; }
	jpeg_calc_output_dimensions(&(in.jinfo));
	pixels = malloc(in.jinfo.output_width * in.jinfo.output_height * in.jinfo.output_components);
	if(!pixels) { return 1; }
	lines = malloc(in.jinfo.output_height * sizeof(char *));
	if(!lines)
	{	free(pixels);
		pixels = NULL;
		return 1;
	}
	jpeg_start_decompress(&(in.jinfo));
	for(y = 0; y < in.jinfo.output_height; y++)
	{ lines[y] = pixels + (y * in.jinfo.output_components * in.jinfo.output_width); }
	while(in.jinfo.output_scanline < in.jinfo.output_height)
		jpeg_read_scanlines(&(in.jinfo),
		 &(lines[in.jinfo.output_scanline]),
		 in.jinfo.rec_outbuf_height);
	jpeg_finish_decompress(&(in.jinfo));
	return 0;
}

int
Epeg::trim()
{	int y, a, b, w, h;
	if((in.w == out.w) && (in.h == out.h)) { return 1; }
	if(scaled) { return 1; }
	scaled = 1;
	w = out.box.w;
	h = out.box.h;
	a = out.box.x;
	b = out.box.y;
	for(y = 0; y < h; y++)
	{ lines[y] = pixels + ((y+b) * in.jinfo.output_components * in.jinfo.output_width) + (a * in.jinfo.output_components); }
	return 0;
}

struct epeg_destination_mgr
{	struct jpeg_destination_mgr dst_mgr;
	Epeg* im;
	Store<unsigned char> buf;
};

int Epeg::encode()
{	epeg_destination_mgr dst_mgr;
	int ok = 0;
	if((out.w < 1) || (out.h < 1)) { return 1; }
	if(out.f) { return 1; }
	if(out.file)
	{	out.f = fopen(out.file, "wb");
		if(!out.f)
		{	error = 1;
			return 1;
		}
	}
	else
	{ out.f = NULL; }
	out.jinfo.err = jpeg_std_error(&(jerr.pub));
	jerr.pub.error_exit = fatal_error_handler;
#ifdef NOWARNINGS
	jerr.pub.emit_message = _emit_message;
	jerr.pub.output_message = _output_message;
	jerr.pub.format_message = _format_message;
#endif
	if(setjmp(jerr.setjmp_buffer))
	{	ok = 1;
		error = 1;
		goto done;
	}
	jpeg_create_compress(&(out.jinfo));
	if(out.f)
	{ jpeg_stdio_dest(&(out.jinfo), out.f); }
	else
	{	out.mem.data = NULL;
		out.mem.size = 0;
		/* Setup RAM destination manager */
		dst_mgr.dst_mgr.init_destination = _jpeg_init_destination;
		dst_mgr.dst_mgr.empty_output_buffer = _jpeg_empty_output_buffer;
		dst_mgr.dst_mgr.term_destination = _jpeg_term_destination;
		dst_mgr.im = this;
		dst_mgr.buf = malloc(65536);
		if(!dst_mgr.buf)
		{	ok = 1;
			error = 1;
			goto done;
		}
		out.jinfo.dest = (struct jpeg_destination_mgr *)dst_mgr;
	}
	out.jinfo.image_width = out.w;
	out.jinfo.image_height = out.h;
	out.jinfo.input_components = in.jinfo.output_components;
	out.jinfo.in_color_space = in.jinfo.out_color_space;
	out.jinfo.dct_method	 = in.jinfo.dct_method;
	jpeg_set_defaults(&(out.jinfo));
	jpeg_set_quality(&(out.jinfo), out.quality, TRUE);
	if(out.quality >= 90)
	{	out.jinfo.comp_info[0].h_samp_factor = 1;
		out.jinfo.comp_info[0].v_samp_factor = 1;
		out.jinfo.comp_info[1].h_samp_factor = 1;
		out.jinfo.comp_info[1].v_samp_factor = 1;
		out.jinfo.comp_info[2].h_samp_factor = 1;
		out.jinfo.comp_info[2].v_samp_factor = 1;
	}
	jpeg_start_compress(&(out.jinfo), TRUE);
	if(out.comment)
	{ jpeg_write_marker(&(out.jinfo), JPEG_COM, out.comment, strlen(out.comment)); }
	if(out.thumbnail_info)
	{	char buf[8192];
		if(in.file)
		{	snprintf(buf, sizeof(buf), "Thumb::URI\nfile://%s", in.file);
			jpeg_write_marker(&(out.jinfo), JPEG_APP0 + 7, buf, strlen(buf));
			snprintf(buf, sizeof(buf), "Thumb::MTime\n%llu", (unsigned long long int)stat_info.st_mtime);
		}
		jpeg_write_marker(&(out.jinfo), JPEG_APP0 + 7, buf, strlen(buf));
		snprintf(buf, sizeof(buf), "Thumb::Image::Width\n%i", in.w);
		jpeg_write_marker(&(out.jinfo), JPEG_APP0 + 7, buf, strlen(buf));
		snprintf(buf, sizeof(buf), "Thumb::Image::Height\n%i", in.h);
		jpeg_write_marker(&(out.jinfo), JPEG_APP0 + 7, buf, strlen(buf));
		snprintf(buf, sizeof(buf), "Thumb::Mimetype\nimage/jpeg");
		jpeg_write_marker(&(out.jinfo), JPEG_APP0 + 7, buf, strlen(buf));
	}
	while(out.jinfo.next_scanline < out.h)
	{ jpeg_write_scanlines(&(out.jinfo), &(lines[out.jinfo.next_scanline]), 1); }
	jpeg_finish_compress(&(out.jinfo));
done:
	if((in.f) || (in.mem.data != NULL)) { jpeg_destroy_decompress(&(in.jinfo)); }
	if((in.f) && (in.file)) { fclose(in.f); }
	out.jinfo.dest = NULL;
	jpeg_destroy_compress(&(out.jinfo));
	if((out.f) && (out.file)) { fclose(out.f); }
	in.f = NULL;
	out.f = NULL;
	return ok;
}

void Epeg::fatal_error_handler(j_common_ptr cinfo)
{	emptr errmgr;
	errmgr = (emptr)cinfo->err;
	longjmp(errmgr->setjmp_buffer, 1);
	return;
}

/* Source manager methods */
METHODDEF(void)
_jpeg_decompress_error_exit(j_common_ptr cinfo)
{
}


METHODDEF(void)
_jpeg_init_source(j_decompress_ptr cinfo)
{
}

METHODDEF(boolean)
_jpeg_fill_input_buffer(j_decompress_ptr cinfo)
{	WARNMS(cinfo, JWRN_JPEG_EOF);
	/* Insert a fake EOI marker */
	cinfo->src->next_input_byte = fake_EOI;
	cinfo->src->bytes_in_buffer = sizeof(fake_EOI);
	return TRUE;
}


METHODDEF(void)
_jpeg_skip_input_data(j_decompress_ptr cinfo, long num_bytes)
{	if(num_bytes > (long)(cinfo)->src->bytes_in_buffer)
	{ ERREXIT(cinfo, 0); }
	(cinfo)->src->next_input_byte += num_bytes;
	(cinfo)->src->bytes_in_buffer -= num_bytes;
}

METHODDEF(void)
_jpeg_term_source(j_decompress_ptr cinfo)
{
}


/* destination manager methods */
METHODDEF(void)
_jpeg_init_destination(j_compress_ptr cinfo)
{	struct epeg_destination_mgr *dst_mgr;
	dst_mgr = (struct epeg_destination_mgr *)cinfo->dest;
	dst_mgr->dst_mgr.free_in_buffer = 65536;
	dst_mgr->dst_mgr.next_output_byte = (JOCTET *)dst_mgr->buf;
}

METHODDEF(boolean)
_jpeg_empty_output_buffer(j_compress_ptr cinfo)
{	struct epeg_destination_mgr *dst_mgr;
	unsigned char *p;
	int psize;
	dst_mgr = (struct epeg_destination_mgr *)cinfo->dest;
	psize = dst_mgr->im->out.mem.size;
	dst_mgr->im->out.mem.size += 65536;
	p = realloc(*(dst_mgr->im->out.mem.data), *(dst_mgr->out.mem.size));
	if(p)
	{	memcpy(p + psize, dst_mgr->buf, 65536);
		dst_mgr->dst_mgr.free_in_buffer = 65536;
		dst_mgr->dst_mgr.next_output_byte = (JOCTET *)dst_mgr->buf;
	}
	else
	{ return FALSE; }
	return TRUE;
}

METHODDEF(void)
_jpeg_term_destination(j_compress_ptr cinfo)
{	struct epeg_destination_mgr *dst_mgr;
	unsigned char *p;
	int psize;
	dst_mgr = (struct epeg_destination_mgr *)cinfo->dest;
	psize = *(dst_mgr->im->out.mem.size);
	*(dst_mgr->im->out.mem.size) += 65536 - dst_mgr->dst_mgr.free_in_buffer;
	p = realloc(*(dst_mgr->im->out.mem.data), *(dst_mgr->im->out.mem.size));
	if(p)
	{	*(dst_mgr->im->out.mem.data) = p;
		memcpy(p + psize, dst_mgr->buf, 65536 - dst_mgr->dst_mgr.free_in_buffer);
	}
}

/* be noisy - not */
METHODDEF(void)
_emit_message(j_common_ptr cinfo, int msg_level)
{
}

METHODDEF(void)
_output_message(j_common_ptr cinfo)
{
}

METHODDEF(void)
_format_message(j_common_ptr cinfo, char * buffer)
{
}
