// Pix.h 
// Created by Robin Rowe 2020-06-18
// License GPL3

#ifndef Pix_h
#define Pix_h

#include <iostream>
#include <memory>

class Pix
{	std::unique_ptr<unsigned char[]> v;
	unsigned size;
	void Move(Pix& p)
	{	v = std::move(p.v);
		size = p.size;
		p.size = 0;
	}
public:
	Pix(Pix& p)
	{	Move(p);
	}
	void operator=(Pix& p)
	{	Move(p);
	}
	~Pix()
	{}
	Pix()
	{}
	Pix(unsigned width,unsigned height,unsigned depth)
	{	size = width*height*depth;
		v = std::make_unique<unsigned char[]>(size);
	}
	operator unsigned char*() const
	{	return v.get();
	}
	bool operator!() const
	{	return 0 == size;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const Pix& pix)
{	return pix.Print(os);
}


inline
std::istream& operator>>(std::istream& is,Pix& pix)
{	return pix.Input(is);
}

#endif
