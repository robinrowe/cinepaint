//
// Copyright 2006-2013 Johannes Hofmann <Johannes.Hofmann@gmx.de>
//
// This software may be used and distributed according to the terms
// of the GNU General Public License, incorporated herein by reference.

#include <pthread.h>
#include "workpool.H"

WorkPool::WorkPool(int numThreads)
{
	pthread_mutex_init(&mutex, NULL);
	pthread_mutex_init(&condMutex, NULL);
	pthread_cond_init(&cond, NULL);

	for (int i = 0; i < numThreads; i++) {
		pthread_t t;
		pthread_create(&t, NULL, thread_main, this);
		threads[i] = t;
	}
}

WorkPool::~WorkPool()
{
	void *res;
	for (int i = 0; i < threads.size(); i++)
		pthread_join(threads[i], &res);
}

#if 0
Queue *
WorkPool::createQueue()
{
	Queue *q = (Queue*) malloc(sizeof(Queue));

	q->head = q->tail = NULL;
	q->pool = this;
	
	return q;
}

void
WorkPool::addQueue(Queue *q)
{
	pthread_mutex_lock(&mutex);
	queues.push_back(q);
	pthread_mutex_unlock(&mutex);
	signal ();
}



void
WorkPool::addTask(Queue *q, action_fn action, void *data)
{
	Task *t = (Task*) malloc (sizeof(Task));

	t->action = action;
	t->data = data;
	t->next = NULL;

	pthread_mutex_lock(&mutex);
	t->prev = q->tail;
	if (!q->head)
		q->head = t;
	if (q->tail)
		q->tail->next = t;
	q->tail = t;
	pthread_mutex_unlock(&mutex);

	signal();
}

void
WorkPool::removeQueues()
{
	pthread_mutex_lock(&mutex);
	queues.clear();
	pthread_mutex_unlock(&mutex);
}

Task*
WorkPool::getTask(Queue *q)
{
	Task *t = q->head;

	if (t) {
		q->head = t->next;
		if (q->head)
			q->head->prev = NULL;
	}

	return t;
}

Task*
WorkPool::getTask()
{
	Task *t = NULL;

	pthread_mutex_lock(&mutex);
	for (int i = 0; i < queues.size(); i++) {
		t = getTask(queues[i]);
	 	if (t)
			break;
	}
	pthread_mutex_unlock(&mutex);

	return t;
}
#endif
void
WorkPool::signal ()
{
	pthread_mutex_lock(&condMutex);
	pthread_cond_broadcast(&cond);
	pthread_mutex_unlock(&condMutex);
}

void
WorkPool::run ()
{
	Task t;
	for (;;) {
		pthread_mutex_lock(&condMutex);
		t = getTask ();
		if (!t)
		{	pthread_cond_wait(&cond, &condMutex);
			pthread_mutex_unlock(&condMutex);			
			continue;
		}
		pthread_mutex_unlock(&condMutex);
		t.action(t.data);
	}
}

void *
WorkPool::thread_main(void *data)
{
	WorkPool *pool = (WorkPool*) data;

	pool->run ();
	return NULL;
}
