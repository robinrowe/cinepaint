// Store.h 
// Created by Robin Rowe 2020-06-20
// License open source MIT

#ifndef Store_h
#define Store_h

#include <iostream>
#include <memory>
#include <memory.h>

template <typename T>
class Store
{	std::unique_ptr<T[]> v;
	unsigned size;
	void Move(Pix& p)
	{	v = std::move(p.v);
		size = p.size;
		p.size = 0;
	}
public:
	~Store()
	{}
	Store()
	{}
	Store(unsigned size)
	:	size(size)
	{	v = std::make_unique<T[]>(size);
	}
	Store(Store& p)
	{	Move(p);
	}
	void operator=(Store& p)
	{	Move(p);
	}
	operator T*()
	{	return v.get();
	}
	operator T*() const
	{	return v.get();
	}
	bool operator!() const
	{	return 0 == size;
	}
	void Open(unsigned size)
	{	this->size = size;
		v = std::make_unique<T[]>(size);
	}
	void Close()
	{	size = 0;
		v.release();
	}
	void Zero()
	{	memset(this,0,sizeof(T)*size);
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

template <typename T>
std::ostream& operator<<(std::ostream& os,const Store<T>& store)
{	return store.Print(os);
}

template <typename T>
std::istream& operator>>(std::istream& is,Store<T>& store)
{	return store.Input(is);
}

#endif
