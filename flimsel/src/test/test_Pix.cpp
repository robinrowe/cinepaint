// test_Pix.cpp 
// Created by Robin Rowe 2020-06-18
// License GPL3

#include <iostream>
#include "../Pix.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing Pix" << endl;
	Pix pix(1,1,1);
	if(!pix)
	{	cout << "Pix failed on operator!" << endl;
		return 1;
	}
	cout << pix << endl;
	cout << "Pix Passed!" << endl;
	return 0;
}
