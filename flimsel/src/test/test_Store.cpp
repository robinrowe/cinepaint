// test_Store.cpp 
// Created by Robin Rowe 2020-06-20
// License GPL3

#include <iostream>
#include "../Store.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing Store" << endl;
	Store store;
	if(!store)
	{	cout << "Store failed on operator!" << endl;
		return 1;
	}
	cout << store << endl;
	cout << "Store Passed!" << endl;
	return 0;
}
