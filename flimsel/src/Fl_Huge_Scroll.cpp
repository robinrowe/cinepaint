//
// Copyright 2006 Johannes Hofmann <Johannes.Hofmann@gmx.de>
//
// This software may be used and distributed according to the terms
// of the GNU General Public License, incorporated herein by reference.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include "Fl_Huge_Scroll.H"

#define std::min(A,B) ((A)<(B)?(A):(B))
    
Fl_Huge_Scroll::Fl_Huge_Scroll(int x, int y, int w, int h) : Fl_Group(x, y, w, h) {
	array = NULL;
	visible_widgets = NULL;
	max_num = num = num_vis =0;
	size_x = size_y = 0;
	x_position = y_position = 0;
	drag_scrolling = 0;
	h_scrollbar = v_scrollbar = NULL;
	resizable(NULL);
}

void
Fl_Huge_Scroll::add(Fl_Widget *_w, int _x, int _y) {
	if (num >= max_num) {
		max_num = max_num + 1000;
		array = (PositionWidget **) 
			realloc(array, sizeof(PositionWidget*) * max_num);
		visible_widgets = (Fl_Widget **)
			realloc(visible_widgets, sizeof(Fl_Widget*) * max_num);
	}

	PositionWidget *p = new PositionWidget();
	p->widget = _w;
	p->x = _x;
	p->y = _y;
	p->widget->position(p->x - x_position + x(), p->y - y_position + y());
	array[num++] = p;

	if (visible(p))
		visible_widgets[num_vis++] = _w;
	else
		p->widget->clear_visible();

	Fl_Group::add(_w);
	set_scrollbar_value();
}

void
Fl_Huge_Scroll::remove(Fl_Widget *_w) {

	for (int i=0; i<num;i++) {
		PositionWidget *p = array[i];
		if (!p) {
			continue;
		} else if (p->widget == _w) {
			delete p;
			array[i] = NULL;
		}
	}

	for (int i=0; i<num_vis;i++)
		if (visible_widgets[i] == _w)
			visible_widgets[i] = NULL;

	Fl_Group::remove(_w);
	update_bbox();
}

void
Fl_Huge_Scroll::update_visibles() {
	num_vis = 0;

	for (int i = 0; i < num; i++) {
		PositionWidget *p = array[i];
		if (!p) {
			continue;
		} else if (visible(p)) {
			p->widget->position(p->x - x_position + x(),
				p->y - y_position + y());
			p->widget->set_visible();
			visible_widgets[num_vis++] = p->widget;
		} else {
			p->widget->clear_visible();
		}
	}
}

void
Fl_Huge_Scroll::position(int px, int py) {
	x_position = px;
	y_position = py;
	size_x = 0;
	size_y = 0;
	update_visibles();
	set_scrollbar_value();	
	damage(FL_DAMAGE_SCROLL);
}

void
Fl_Huge_Scroll::scroll(int dx, int dy) {
	position(xposition() + dx, yposition() + dy);
}

void
Fl_Huge_Scroll::resize(int _x, int _y, int _w, int _h) {
	Fl_Group::resize(_x, _y, _w, _h);
	update_visibles();
	set_scrollbar_value();	
}

int
Fl_Huge_Scroll::handle(int event) {
	switch (event) {
		case FL_PUSH:
			if (Fl::event_button2()) {
				drag_scrolling = 1;
				drag_x = Fl::event_x();
				drag_y = Fl::event_y();
				fl_cursor(FL_CURSOR_MOVE);
				return 1;
			}
			break;
		case FL_DRAG:
			if (Fl::event_button2() && drag_scrolling) {
				scroll(drag_x - Fl::event_x(), drag_y - Fl::event_y());
				drag_x = Fl::event_x();
				drag_y = Fl::event_y();
				if (user_scroll_callback) {
					user_scroll_callback(user_scroll_callback_data);
				}
				return 1;
			}
			break;
		case FL_RELEASE:
			if (drag_scrolling) {
				drag_scrolling = 0;
				fl_cursor(FL_CURSOR_DEFAULT);
				return 1;
			}
			break;
	}

	return Fl_Group::handle(event);
}

int
Fl_Huge_Scroll::visible(PositionWidget *p) {
	return p &&
		p->x - x_position + p->widget->w() >= 0 &&
		p->x - x_position <= w() &&
        p->y - y_position + p->widget->h() >= 0 &&
        p->y - y_position <= h();
}

int
Fl_Huge_Scroll::sizey() {
	update_bbox();
	return size_y;
}

int
Fl_Huge_Scroll::sizex() {
	update_bbox();		
	return size_x;
}

void
Fl_Huge_Scroll::draw_clip(void *v, int X, int Y, int W, int H) {
	Fl_Huge_Scroll *sc = (Fl_Huge_Scroll*) v;	
	fl_clip(X, Y, W, H);
	fl_color(sc->color());
	fl_rectf(X, Y, W, H);	
	for (int i=0; i<sc->num_vis;i++)
		if (sc->visible_widgets[i])
			sc->draw_child(*sc->visible_widgets[i]);

	fl_pop_clip();
}

void
Fl_Huge_Scroll::draw() {
	unsigned char d = damage();

	if (d & FL_DAMAGE_SCROLL) {
		fl_scroll(x(), y(), w(), h(), old_x - x_position, old_y - y_position,
			draw_clip, this);
		d &= ~FL_DAMAGE_SCROLL;
	}

    if (d & FL_DAMAGE_CHILD) {
		fl_clip(x(), y(), w(), h());
		draw_children();
		fl_pop_clip();
        d &= ~FL_DAMAGE_CHILD;
	}

    if (d)
		draw_clip(this, x(), y(), w(), h());

	old_x = x_position;
    old_y = y_position;
}

void
Fl_Huge_Scroll::update_bbox() {
	for (int i=0; i<num;i++) {
		PositionWidget *p = array[i];
		if (p == NULL)
			continue;
		if (p->x + p->widget->w() > size_x)
			size_x = p->x + p->widget->w();
		if (p->y + p->widget->h() > size_y)
			size_y = p->y + p->widget->h();
	}
}

void
Fl_Huge_Scroll::scrollbar_position() {
	int p_x = h_scrollbar?h_scrollbar->value():x_position;
	int p_y = v_scrollbar?v_scrollbar->value():y_position;

	position(p_x, p_y);
	set_scrollbar_value();

	if (user_scroll_callback)
		user_scroll_callback(user_scroll_callback_data);
}

void
Fl_Huge_Scroll::set_scrollbar_value() {
	update_bbox();
	
	if (h_scrollbar) {
		h_scrollbar->value(x_position, w(),
			std::min(0, size_x - w()),
			size_x>w() ? size_x : 2*w() - size_x);
	}

	if (v_scrollbar) {
		v_scrollbar->value(y_position, h(),
			std::min(0, size_y - h()),
			size_y>h() ? size_y : 2*h() - size_y);
	}
}   

void
Fl_Huge_Scroll::scrollbar_cb(Fl_Widget *w, void *d) {
    Fl_Huge_Scroll *scroll = (Fl_Huge_Scroll*) d;

    scroll->scrollbar_position();
}

void
Fl_Huge_Scroll::set_horizontal_scrollbar(Fl_Scrollbar *sb) {
	h_scrollbar = sb;
	h_scrollbar->callback((Fl_Callback*) scrollbar_cb, this);
	set_scrollbar_value();
}

void
Fl_Huge_Scroll::set_vertical_scrollbar(Fl_Scrollbar *sb) {
	v_scrollbar = sb;
	v_scrollbar->callback((Fl_Callback*) scrollbar_cb, this);
	set_scrollbar_value();
}

void
Fl_Huge_Scroll::register_user_scroll_callback(Callback *cb, void *d) {
    user_scroll_callback = cb;
    user_scroll_callback_data = d;
}
