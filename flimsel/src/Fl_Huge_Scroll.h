//
// Copyright 2006 Johannes Hofmann <Johannes.Hofmann@gmx.de>
//
// This software may be used and distributed according to the terms
// of the GNU General Public License, incorporated herein by reference.

#ifndef Fl_Huge_Scroll_H
#define Fl_Huge_Scroll_H

#include <FL/Fl_Group.H>
#include <FL/Fl_Scrollbar.H>

class Fl_Huge_Scroll : public Fl_Group {
	private:
		class PositionWidget {
			public:
			int x, y;
			Fl_Widget *widget;
		};
		typedef void (Callback)(void *d);

		PositionWidget **array;
		Fl_Widget **visible_widgets;
		Fl_Scrollbar *h_scrollbar, *v_scrollbar;
		int num;
		int num_vis;
		int max_num;
		int x_position, y_position;
		int old_x, old_y;
		int size_x, size_y;
		int drag_x, drag_y;
		int drag_scrolling;
		Callback *user_scroll_callback;
		void *user_scroll_callback_data;
		int visible(PositionWidget *p);
		void update_visibles();
		void update_bbox();
		static void draw_clip(void *v, int x, int y, int w, int h);
		void set_scrollbar_value();
		void scrollbar_position();
		static void scrollbar_cb(Fl_Widget *o, void *d);

	public:
		Fl_Huge_Scroll(int x, int y, int w, int h);
		void add(Fl_Widget *w, int x, int y);
		void remove(Fl_Widget *w);
		void position(int x, int y);
		void scroll(int dx, int dy);
		void resize(int x, int y, int w, int h);
		int xposition() {return x_position;};
		int yposition() {return y_position;};
		int sizex();
		int sizey();
		void draw();
		int handle(int event);
		void set_horizontal_scrollbar(Fl_Scrollbar *sb);
		void set_vertical_scrollbar(Fl_Scrollbar *sb);
		void register_user_scroll_callback(Callback *cb, void *d);
};

#endif
