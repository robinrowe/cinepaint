//
// Copyright 2006 Johannes Hofmann <Johannes.Hofmann@gmx.de>
//
// This software may be used and distributed according to the terms
// of the GNU General Public License, incorporated herein by reference.

#ifndef Fl_Epeg_Image_H
#define Fl_Epeg_Image_H

#include <FL/Fl_Image.H>

class Fl_Epeg_Image : public Fl_RGB_Image {

	public:
	Fl_Epeg_Image(const char *filename, int max_w, int max_h);
};

#endif
