#ifndef _EPEG_H
#define _EPEG_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <setjmp.h>
#include <jpeglib.h>
#include <vector>
#include "Pix.h"
#include "config.h"
#include "Store.h"

typedef struct _epeg_error_mgr *emptr;
typedef struct _Epeg_Thumbnail_Info Epeg_Thumbnail_Info;

typedef enum _Epeg_Colorspace
{
	EPEG_GRAY8,
	EPEG_YUV8,
	EPEG_RGB8,
	EPEG_BGR8,
	EPEG_RGBA8,
	EPEG_BGRA8,
	EPEG_ARGB32,
	EPEG_CMYK
} Epeg_Colorspace;

struct _Epeg_Thumbnail_Info
{	char *uri;
	unsigned long long int mtime;
	int w, h;
	char *mimetype;
};

struct _epeg_error_mgr
{	struct jpeg_error_mgr pub;
	jmp_buf setjmp_buffer;
};

struct ThumbInfo
{	char *uri;
	unsigned long long int mtime;
	int w, h;
	char *mime;
};

struct EpegMem
{	JOCTET* data;
	int size;
};

struct EpegIn
{	char *file;
	struct EpegMem mem;
	int w, h;
	std::vector<char> comment;
	FILE *f;
	J_COLOR_SPACE color_space;
	struct jpeg_decompress_struct jinfo;
	struct ThumbInfo thumb_info;
};

struct Box
{	int x;
	int y;
	int w;
	int h;
};

struct EpegOut
{	char *file;
	EpegMem mem;
	Box box;
	char *comment;
	FILE *f;
	struct jpeg_compress_struct jinfo;
	int quality;
	char thumbnail_info : 1;
};

METHODDEF(void) _jpeg_decompress_error_exit(j_common_ptr cinfo);
METHODDEF(void) _jpeg_init_source(j_decompress_ptr cinfo);
METHODDEF(boolean) _jpeg_fill_input_buffer(j_decompress_ptr cinfo);
METHODDEF(void) _jpeg_skip_input_data(j_decompress_ptr cinfo, long num_bytes);
METHODDEF(void) _jpeg_term_source(j_decompress_ptr cinfo);

METHODDEF(void) _jpeg_init_destination(j_compress_ptr cinfo);
METHODDEF(boolean) _jpeg_empty_output_buffer(j_compress_ptr cinfo);
METHODDEF(void) _jpeg_term_destination(j_compress_ptr cinfo);

METHODDEF(void) _emit_message(j_common_ptr cinfo, int msg_level);
METHODDEF(void) _output_message(j_common_ptr cinfo);
METHODDEF(void) _format_message(j_common_ptr cinfo, char * buffer);

struct Epeg
{	struct _epeg_error_mgr jerr;
	struct stat stat_info;
	Store<unsigned char> pixels;
	Store<unsigned char*> lines;
	char scaled : 1;
	int error;
	Epeg_Colorspace color_space;
	struct EpegIn in;
	struct EpegOut out;
	bool open_header();
	int decode();
	int scale();
	int decode_for_trim();
	int trim();
	int encode();
	static void fatal_error_handler(j_common_ptr cinfo);
	void decode_bounds_set(int x, int y, int w, int h);
	const Pix pixels_get_as_RGB8(int x, int y, int w, int h);
public:
	void file_open(const char *file);
	void memory_open(unsigned char *data, int size);
	void size_get( int *w, int *h);
	void decode_size_set(int w, int h);
	void colorspace_get(int *space);
	void decode_colorspace_set(Epeg_Colorspace colorspace);
	const Pix pixels_get(int x, int y, int w, int h);
	const char *comment_get();
	void thumbnail_comments_get(Epeg_Thumbnail_Info *info);
	void comment_set(const char *comment);
	void quality_set(int quality);
	void thumbnail_comments_enable(int onoff);
	void file_output_set(const char *file);
	void memory_output_set(unsigned char **data, int *size);
	void close();
};

#endif

