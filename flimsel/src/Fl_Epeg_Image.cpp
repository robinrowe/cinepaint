//
// Copyright 2006 Johannes Hofmann <Johannes.Hofmann@gmx.de>
//
// This software may be used and distributed according to the terms
// of the GNU General Public License, incorporated herein by reference.

#include <stdio.h>
#include <string.h>
#include "Epeg.h"
#include "Fl_Epeg_Image.H"

Fl_Epeg_Image::Fl_Epeg_Image(const char *filename, int max_w, int max_h) : Fl_RGB_Image(0,0,0) {
	Epeg_Image *im;
	const void *data;
	int im_w, im_h, thumb_w, thumb_h;

	im = epeg_file_open(filename);
	if (!im) {
		return;
	}

	epeg_size_get(im, &im_w, &im_h);

	if (im_w * max_h > im_h * max_w) {
		thumb_w = max_w;
		thumb_h = thumb_w * im_h / im_w;
	} else {
		thumb_h = max_h;
		thumb_w = thumb_h * im_w / im_h;
	}

	epeg_decode_colorspace_set(im, EPEG_RGB8); // force RGB8 for grayscale jpgs
	epeg_decode_size_set(im, thumb_w, thumb_h );

	data = epeg_pixels_get(im, 0, 0, thumb_w, thumb_h);
	if (data) {
		w(thumb_w); 
		h(thumb_h);
		d(3);

		array = new uchar[w() * h() * d()];
		alloc_array = 1;
		memcpy((void*) array, data, h() * w() *d());
	}

	epeg_pixels_free(im, data);
	epeg_close(im);
}
