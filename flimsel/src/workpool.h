#ifndef WORKPOOL_H
#define WORKPOOL_H

#include <pthread.h>
#include <vector>
#include <queue>

typedef void (*action_fn)(void *data);

struct Task {
	action_fn action;
	void *data; 
	Task(action_fn action,void *data)
	:	action(action)
	,	data(data)
	{}
	Task()
	:	action(0)
	,	data(0)
	{}
	bool operator!() const
	{	return 0 == action;
	}
//	Task *prev;
//	Task *next;
};

/*
struct Queue {
	Task *head, *tail;
	WorkPool *pool;
};
*/

class WorkPool {
	private:
		pthread_mutex_t mutex;
		pthread_mutex_t condMutex;
		pthread_cond_t cond;
		std::vector<std::queue<Task> > queues;
		std::vector<pthread_t> threads;

		void signal();
//		Task* getTask(Queue *q);
		Task getTask()
		{	if(!tasks.size())
			{	return Task();
			}
			pthread_mutex_lock(&mutex);
			Task task = tasks.front();
		    tasks.pop();
			pthread_mutex_unlock(&mutex);
			return task;
		}
		void run();
		static void *thread_main(void *data);

	public:
		std::queue<Task> tasks;
		WorkPool(int numThreads);
		~WorkPool();

//		Queue *createQueue ();
//		void addQueue (Queue *q);
		void removeQueues ()
		{	pthread_mutex_lock(&mutex);
			queues.clear();
			pthread_mutex_unlock(&mutex);
		}
		void addTask(action_fn action, void *data)
		{	pthread_mutex_lock(&mutex);
			tasks.emplace(action,data);
			pthread_mutex_unlock(&mutex);
			signal();
		}
};

#endif
