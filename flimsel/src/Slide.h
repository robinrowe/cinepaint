//
// Copyright 2006-2012 Johannes Hofmann <Johannes.Hofmann@gmx.de>
//
// This software may be used and distributed according to the terms
// of the GNU General Public License, incorporated herein by reference.

#ifndef Slide_H
#define Slide_H

#include <string>
#include <FL/Fl_Image.H>
#include <FL/Fl_Toggle_Button.H>

class Slide : public Fl_Toggle_Button {
	private:
		std::string file;
		static int max_thumb_w, max_thumb_h;

		static  Fl_Image *epeg_thumb_check(const char *name,
			uchar *header, int hdrlen);
		static  Fl_Image *fallback_thumb_check(const char *name,
			uchar *header, int hdrlen);
		void draw_shadow(Fl_Color c, int x, int y, int w, int h);

	public:
		Slide(const char *name, int _x, int _y, int _w, int _h);
		~Slide();

		int load();
		int reload();
		const char *filename();
		int handle(int event);
		static void init(int _w, int _h);
		void draw();
};

#endif
