#!/bin/sh

TRASH=$HOME/.flimsel/deleted

TMPDIR=`mktemp -d /tmp/flimselXXXXXX`
PIPE=$TMPDIR/fifo

trap "break" INT TERM EXIT
mkfifo -m 0600 $PIPE
mkdir -p $TRASH

IFS="	"
flimsel "$@" < $PIPE | while read cmd arg; do
	for i in $arg; do
		case $cmd in
			*Ctrl+A)
			jhead -autorot "$i";;

			*Ctrl+L)
			jpegtran -copy all -rotate 270 "$i" > $TMPDIR/tmp.jpg && mv $TMPDIR/tmp.jpg "$i";;
	
			*Ctrl+R)
			jpegtran -copy all -rotate 90 "$i" > $TMPDIR/tmp.jpg && mv $TMPDIR/tmp.jpg "$i";;
	
			*Ctrl+F)
			flimp "$i";;

			*Ctrl+G)
			gimp "$i";;
	
			*Ctrl+D)
			mv "$i" $TRASH;;
		esac
		echo $i
	done

	case $cmd in
		*Mouse3)
		feh -F "$arg"&;;

		*Ctrl+H)
		xmessage -file $0;;

		*Ctrl+X)
                echo -n $arg | xsel -i;;
	esac
done > $PIPE

rm $PIPE
rmdir $TMPDIR
