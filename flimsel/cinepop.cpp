// cinepop.cpp 
// Created by Robin Rowe 2020-06-17
// License GPL3

#include <iostream>
using namespace std;

void Usage()
{	cout << "Usage: cinepop " << endl;
}

enum
{	ok,
	invalid_args

};

int main(int argc,char* argv[])
{	cout << "cinepop starting..." << endl;
	if(argc < 1)
	{	Usage();
		return invalid_args;
	}

	cout << "cinepop done!" << endl;
	return ok;
}
