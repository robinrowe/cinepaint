//
// Copyright 2006-2013 Johannes Hofmann <Johannes.Hofmann@gmx.de>
//
// This software may be used and distributed according to the terms
// of the GNU General Public License, incorporated herein by reference.

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Scrollbar.H>
#include <FL/fl_draw.H>

#include "config.h"
#include "Slide.H"
#include "Fl_Huge_Scroll.H"
#include <vector>
#include "workpool.H"

#define VERSION "0.1"

static int slides_per_row = 4;
static int slide_size = 250;
static int slide_margin = 10;
static int scrollbar_width = 15;

static Fl_Huge_Scroll *sc;
static WorkPool pool(2);
//static std::vector<Queue> queues;

static void
usage() {
	fprintf(stderr,
		"flimsel %s\n"
		"usage: flimsel [-r <slides_per_row>] [-s <slide_size>] <images>\n"
		"   -r <slides_per_row>  number of slides per row (default 4).\n"
		"   -S <slide_size>      slide size in pixels (default 250).\n"
		"   <images>             image files.\n",
		VERSION);
}

static int
key_handler(int e) {
	switch (e) {
		case FL_SHORTCUT:
			int key = Fl::event_key();
			int state = Fl::event_state();

			// quit
			if (Fl::test_shortcut(FL_CTRL | 'q')) {
				exit(0);
			}

			// don't exit on FL_Escape
			if (Fl::test_shortcut(FL_Escape)) {
				return 1;
			}

			// deselect all
			if (Fl::test_shortcut(FL_ALT | FL_CTRL | 'n')) {
				for (int i=0; i<sc->children(); i++) {
					Slide *s = (Slide*) sc->child(i);
					if (!s) {
						continue;
					} else {
						s->value(0);
					} 
				}
				return 1;
			}

			// invert selection
			if (Fl::test_shortcut(FL_ALT | FL_CTRL | 'i')) {
				for (int i=0; i<sc->children(); i++) {
					Slide *s = (Slide*) sc->child(i);
					if (!s) {
						continue;
					} else {
						s->value(!s->value());
					} 
				}
				return 1;
			}

			// range selection
			if (Fl::test_shortcut(FL_ALT | FL_CTRL | 'r')) {
				int i, in_range = 0;

				for (i = 0; i < sc->children(); i++) {
					Slide *s = (Slide*) sc->child(i);
					if (s && s->value())
						break;
				}

				for (int j = sc->children() - 1; j > i; j--) {
                    Slide *s = (Slide*) sc->child(j);
                    if (s && s->value()) 
                       in_range = 1;
					if (s && in_range)
						s->value(1);
                }

				return 1;
			}

			if (key <= 32 || key > 0x7f) {
				return 0;
			}
                 
			printf("%s\t", fl_shortcut_label(key | state));
			for (int i=0; i<sc->children(); i++) {
				Slide *s = (Slide*) sc->child(i);
				if (!s) {
					continue;
				} else if (s->value()) {
					printf("%s\t", s->filename());
				}
			}
			printf("\n");
			fflush(stdout);
			return 1;

			break;
	}
	return 0;
}

static int 
slide_x(int n) {
	return ((n % slides_per_row) * (slide_size + slide_margin)) + slide_margin;
}

static int
slide_y(int n) {
	return ((n / slides_per_row) * (slide_size + slide_margin)) + slide_margin;
}

static void
load_task(void *data)
{
    Slide *s = (Slide *) data;
	if (s) {
		s->load();
		Fl::lock();
		s->redraw();
		Fl::awake();
		Fl::unlock();
	}
}

static Slide*
add_slide(const char *name)
{
	Slide *s = new Slide(name, 0, 0, slide_size, slide_size);
	int row = sc->children() / slides_per_row;
	
	if (row >= queues.size()) {
		Queue *q = pool.createQueue();
		queues[row] = *q;
	}

	sc->add(s, slide_x(sc->children()), slide_y(sc->children()));
	s->redraw();

	pool.addTask(&queues[row], load_task, s);

	return s;
}

static int
update_slide(const char *name) {
	// update existing slide
	for(int i=0; i<sc->children(); i++) {
		Slide *s = (Slide*) sc->child(i);
		if (s && strcmp(s->filename(), name) == 0) {
			if (s->reload() != 0) {
				sc->remove(s);
				delete s;
				sc->redraw();
			}

			return 0;
		}
	}

	// create new slide
	add_slide(name);
	return 0;
}

static void
my_stdin_handler(FL_SOCKET fd, void *d) {
	char buf[1024];
	char *c;

	if (fgets(buf, sizeof(buf), stdin)) {
		if ((c = strchr(buf, '\n')))
			*c = '\0';
		update_slide(buf);
	} else if (feof(stdin)) {
		fprintf(stderr, "EOF on stdin\n");
		clearerr(stdin);
		Fl::remove_fd(0); // avoid busy looping after stdin got EOF
	} else if (ferror(stdin)) {
		fprintf(stderr, "Error on stdin\n");
		exit(1);
	}
}

static void
prioritize_load(void *data)
{
	int from_row = sc->yposition() / (slide_size + slide_margin);
	int visible_rows = sc->h() / (slide_size + slide_margin) + 1;

	if (from_row < 0)
		from_row = 0;

	pool.removeQueues();
	for (int i = from_row;
		i <= from_row + 2 * visible_rows && i < queues.size(); i++) {
		pool.addQueue(&queues[i]);
	}
	for (int i = from_row;
		 i >= 0 && i >= from_row - visible_rows && i < queues.size(); i--) {
		pool.addQueue(&queues[i]);
	}
}

int
main(int argc, char **argv) {
	int c, n, i, err;
	err = 0;
	while ((c = getopt(argc, argv, "?hr:S:s:g:d:f:i:t:b:")) != EOF) {
		switch (c) {  
			case 'r':
				slides_per_row = atoi(optarg);
				break;
			case 'S':
				slide_size = atoi(optarg);
				break;
			case '?':
			case 'h':
				usage();
				exit(0);
			default:
				i = optind - 1;
				n = Fl::arg(argc, argv, i);
				if (n == 0) {
					err++;
				} else {
					optind = i;
				}
				break;
		}
	}

	Fl::add_handler(key_handler);

	Slide::init(slide_size - 25, slide_size - 25);

	Fl_Window window(slides_per_row * (slide_size + slide_margin) + 30, 800);
	window.end();
	sc = new Fl_Huge_Scroll(0, 0, window.w() - scrollbar_width,
		window.h() - scrollbar_width);
	sc->register_user_scroll_callback(prioritize_load, NULL);
	sc->end();
	window.add(sc);
	window.resizable(sc);

	Fl_Scrollbar *sb_v = new Fl_Scrollbar(window.w() - scrollbar_width, 0,
		scrollbar_width, window.h() - scrollbar_width);
	window.add(sb_v);
	sc->set_vertical_scrollbar(sb_v);

	Fl_Scrollbar *sb_h =  new Fl_Scrollbar(0, window.h() - scrollbar_width,
		window.w() - scrollbar_width, scrollbar_width);
	sb_h->type(FL_HORIZONTAL);
	window.add(sb_h);
	sc->set_horizontal_scrollbar(sb_h);

//	pool = new WorkPool (2);
//	queues = new Vector <Queue*> ();

	Fl::add_fd(0, FL_READ, my_stdin_handler);

	window.show(1, argv);

	for (int i=0; i<argc - optind; i++)
		add_slide(argv[i + optind]);

	prioritize_load(NULL);
	
	Fl::lock();
	return Fl::run();
}
